//
//  MacPresentationController.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 21/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

class MacPresentationController: UIPresentationController {
    private var dimmingView: UIView!
    private var direction: MacPresentationDirection
    var popupHeight: CGFloat = 420
    
    
    init(presentedViewController: UIViewController, presentingViewController: UIViewController?,direction: MacPresentationDirection, isBlurredBackground: Bool) {
        self.direction = direction
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        setUpDimmingView(isBlurredBackground: isBlurredBackground)
    }
    
    override var frameOfPresentedViewInContainerView: CGRect {
        var frame: CGRect = .zero
        guard let containerView = containerView else {return frame}
        frame.size = size(forChildContentContainer: presentedViewController, withParentContainerSize: containerView.bounds.size)
        switch direction {
        case .pageSheet,.formSheet:
            frame.origin = CGPoint(x: (containerView.frame.width - frame.size.width)/2, y: (containerView.frame.height - frame.size.height)/2)
        case .top, .topWithCustomHeight:
            frame.origin = CGPoint(x: (containerView.frame.width - frame.size.width)/2, y: 0)
        }
        return frame
    }
    
    override func presentationTransitionWillBegin() {
        containerView?.insertSubview(dimmingView, at: 0)
        dimmingView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmingView.alpha = 1.0
            return
        }
        
        coordinator.animate(alongsideTransition: { (_) in
            self.dimmingView.alpha = 1.0
        })
    }
    
    override func dismissalTransitionWillBegin() {
        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmingView.alpha = 0.0
            return
        }
        
        coordinator.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = 0.0
        })
    }
    
    override func containerViewWillLayoutSubviews() {
        presentedView?.frame = frameOfPresentedViewInContainerView
    }
    
    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        switch direction {
        case .pageSheet:
            return CGSize(width: 500, height: 700)
        case .formSheet:
            return CGSize(width: 540, height: 564)
        case .top:
            return CGSize(width: 1000, height: 800)
        case .topWithCustomHeight:
            return CGSize(width: 600, height: popupHeight)
        }
    }
    
    func setUpDimmingView(isBlurredBackground: Bool) {
        dimmingView = UIView()
        dimmingView.translatesAutoresizingMaskIntoConstraints = false
        dimmingView.alpha = 0.0
        if #available(iOS 13, macCatalyst 13, *), isBlurredBackground {
            let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .systemUltraThinMaterial))
            visualEffectView.bounds = dimmingView.bounds
            visualEffectView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
            dimmingView.addSubview(visualEffectView)
        } else {
            dimmingView.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        }
        
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:)))
        dimmingView.addGestureRecognizer(recognizer)
    }
    
    @objc dynamic func handleTap(recognizer: UITapGestureRecognizer) {
        presentingViewController.dismiss(animated: true)
    }
}
