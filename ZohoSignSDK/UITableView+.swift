//
//  UITableView+.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 24/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

//TableView identifier
public protocol Reuse: class {
    static var reuseIdentifier: String { get }
}

public extension Reuse {
    static var reuseIdentifier: String { return String(describing: Self.self) }
}

public extension UITableView {
    
    func registerReusableCell<T: UITableViewCell>(_: T.Type) {
        register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
    
    func setSeparatorStyle(color : UIColor = ZColor.seperatorColor ){
        separatorColor  = color
        //        separatorStyle  = .singleLine
        //        if SwiftUtils.isMac{
        //            separatorStyle  = .none
        //        }
    }
}

extension UITableViewCell: Reuse {
}

public extension UITableViewCell {
    func setBackgroundView(_ listSelectionColor: UIColor = ZColor.listSelectionColor){
        let selectedBackgroundView = UIView()
        selectedBackgroundView.backgroundColor = listSelectionColor
        self.selectedBackgroundView = selectedBackgroundView
    }
}
