//
//  ZRadioGroupView.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 23/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import Foundation
import AMPopTip

public protocol ZRadioGroupViewDelegate: class {
    func radioGroupDidTappedEditButton(_ radioGroupView: ZRadioGroupView,onRadioItem radioItem: ZRadioGroupItem)
    func radioGroupDidTapped(onRadioGroupView radioGroupView: ZRadioGroupView)
    func radioGroupBeginEditing(onRadioGroupView radioGroupView: ZRadioGroupView)
    func radioGroupEditing(onRadioGroupView radioGroupView: ZRadioGroupView)
    func radioGroupEndEditing(_ radioGroupView: ZRadioGroupView,onRadioItem radioItem: ZRadioGroupItem)
}

public extension ZRadioGroupViewDelegate{
    func radioGroupDidTappedEditButton(_ radioGroupView: ZRadioGroupView,onRadioItem radioItem: ZRadioGroupItem) {}
    func radioGroupDidTapped(onRadioGroupView radioGroupView: ZRadioGroupView) {}
    func radioGroupEditing(onRadioGroupView radioGroupView: ZRadioGroupView) {}
    func radioGroupBeginEditing(onRadioGroupView radioGroupView: ZRadioGroupView) {}
    func radioGroupEndEditing(_ radioGroupView: ZRadioGroupView,onRadioItem radioItem: ZRadioGroupItem) {}
}

public class RadioGroup {
    public var name: String
    public var values: [RadioValues] = []
    public var isOptional: Bool = false
    public var isReadOnly: Bool = false
    public var deletedSubFields: [String] = []
    
    public init(name: String,values: [RadioValues] = []) {
        self.name = name
        self.values = values
    }
    
    public var defaultValueId: String {
        var id: String = ""
        values.forEach { (value) in
            if value.defaultValue {
                id = value.subFieldId ?? ""
            }
        }
        return id
    }
    
    public func deepCopy() -> RadioGroup {
        let mutableDeepCopy = RadioGroup(name: name)
        mutableDeepCopy.values = values
        mutableDeepCopy.isOptional = isOptional
        mutableDeepCopy.isReadOnly = isReadOnly
        return mutableDeepCopy
    }
}



public class ZRadioGroupView : ResizableView, UIGestureRecognizerDelegate{
    
    var radioItems : [ZRadioGroupItem] = []
    private var groupIndicatorView: UIView!
    private lazy var dashedBorder: CAShapeLayer = {
        var dashedBorder = CAShapeLayer()
        dashedBorder.strokeColor = self.grayFieldBorderColor.cgColor
        dashedBorder.lineWidth = 2
        dashedBorder.lineDashPattern = [5, 5]
        dashedBorder.fillColor = nil
        dashedBorder.path = UIBezierPath(rect: groupIndicatorView.bounds).cgPath
        dashedBorder.frame = groupIndicatorView.bounds
        return dashedBorder
    }()
    
    private lazy var previousViewFrame: CGRect = {
        return frame
    }()
    
    private var itemSize: CGFloat = 12 {
        willSet{
            outerMargin = newValue/4
        }
    }
    
    public override var resizeViewMode: ZResizeViewMode {
        didSet {
            setupAppearance()
        }
    }
    
    public override var outerMargin: CGFloat {
        willSet {
            totalMargin = newValue * 2
        }
    }
    
    private var totalItemSize: CGFloat {
        return itemSize + totalMargin
    }
    
    private var groupIndicatorShowTimer: Timer?
    private var popTips: [PopTip] = []
    private var isRadioItemPositionIsReseted = false
    private let itemPadding: CGFloat = 10
    
    var selectedRadioItem: ZRadioGroupItem?
    public weak var delegate: ZRadioGroupViewDelegate?
    
    public init(frame: CGRect,itemDroppedAt location: CGPoint, signField: SignField) {
        super.init(frame: frame)
        self.itemSize = frame.width * 0.015
        self.outerMargin = itemSize/4
        self.totalMargin = outerMargin*2
        self.signField = signField
        setupDefaultAttributes(location: location)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    deinit {
        ZInfo("deinit")
    }
    
    private func setupDefaultAttributes(location: CGPoint,withSize size: CGFloat? = nil) {
        guard let radioGroup = signField.radioGroup else {
            return
        }
        let totalItemCount = CGFloat(radioGroup.values.count)
                
        groupIndicatorView = UIView()
        groupIndicatorView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        groupIndicatorView.isUserInteractionEnabled = false
        groupIndicatorView.layer.addSublayer(dashedBorder)
        addSubview(groupIndicatorView)
        
        var previousViewFrame: CGRect?
        radioGroup.values.enumeratedInPlaceLoop { (index, value) -> RadioValues in
            var mutableradioValue = value
            if mutableradioValue.rect?.isNull ?? true {
                if var previousFrame = previousViewFrame {
                    previousFrame.origin.y += totalItemSize + itemPadding
                    mutableradioValue.rect = previousFrame
                    previousViewFrame = previousFrame
                    //CGRect(x: location.x, y: location.y + (totalItemSize * CGFloat(index)), width: itemSize, height: itemSize)
                } else {
                    previousViewFrame = CGRect(origin: location, size: CGSize(withSide: itemSize))
                    let totalItemLength = (totalItemCount * totalItemSize) + (itemPadding * (totalItemCount-1))
                    previousViewFrame?.origin.y += totalItemLength
                    if previousViewFrame!.isValidFrame(restrictedToBounds: bounds)  {
                        previousViewFrame?.origin.y -= totalItemLength
                    } else {
                        previousViewFrame?.origin.y = bounds.height - totalItemLength
                    }
                    mutableradioValue.rect = previousViewFrame
                }
                 
                /*
                if index > 0 {
                    mutableradioValue.rect?.origin.y += CGFloat(index) * itemPadding
                }
                */
            } else {
                itemSize = mutableradioValue.rect!.height
            }
            
            addRadioItems(radioValue: mutableradioValue,rect: mutableradioValue.actualViewFrameFromRect(outerMargin: outerMargin, size: mutableradioValue.rect!.height + totalMargin), index: index)
            mutableradioValue = calculateNormalisedFrame(mutableradioValue)
            return mutableradioValue
        }
        updateViewFrames()
        setupAppearance()
    }
    
    public func reloadRadioItems() {
        //remove all radioitems from the view
        radioItems.removeAll()
        subviews.forEach({ if $0 is ZRadioGroupItem { $0.removeFromSuperview() } })
        
        //add them again and recompute rect for those items which doesn't contain rect
        var lastRadioItemFrame: CGRect?
        signField.radioGroup!.values.enumeratedInPlaceLoop { (index, value) -> RadioValues in
            var mutableradioValue = value
            var viewRect: CGRect
            
            if mutableradioValue.rect.isNull {
                if let lastRadioItemFrame = lastRadioItemFrame {
                    viewRect =  lastRadioItemFrame.getNearestPossibleFrame(onSide: .all, forSize: lastRadioItemFrame.height, restrictedToBounds: self.frame)
                    mutableradioValue.setRect(fromActualViewFrame: viewRect, outerMargin: outerMargin)
                }else {
                    mutableradioValue.rect = .zero
                    viewRect = .zero
                }
            } else {
                viewRect = mutableradioValue.actualViewFrameFromRect(outerMargin: outerMargin, size: mutableradioValue.rect!.height + totalMargin)
            }
            
            self.addRadioItems(radioValue: mutableradioValue,rect: viewRect, index: index)
            lastRadioItemFrame = viewRect
            return mutableradioValue
        }
        
        updateViewFrames()
        setupAppearance()
    }
    
    private func addRadioItems(radioValue: RadioValues,rect: CGRect, index: Int) {
        let radioItem = ZRadioGroupItem(frame: rect,value: radioValue,outerMargin: self.outerMargin,resizeMode: resizeViewMode)
        radioItem.tag = index
        radioItem.delegate = self
        addSubview(radioItem)
        radioItems.append(radioItem)
    }
    
    private func setupAppearance() {
        switch resizeViewMode {
        case .sentDoc_Create:
            isUserInteractionEnabled = true
            radioItems.forEach{
                $0.resizeMode = resizeViewMode
                $0.backgroundColor = redFieldDarkBgColor.withAlphaComponent(0.8)
            }
        case .receivedDoc_pending:
            isUserInteractionEnabled = !signField.isReadOnly
            radioItems.forEach {
                $0.resizeMode = resizeViewMode
                if signField.isReadOnly {
                    $0.setSelected($0.radioValue.defaultValue)
                    $0.radioButton.outerCircleViewBackgroundColor = .clear
                    $0.backgroundColor = .clear
                    $0.isUserInteractionEnabled = false
                } else {
                    $0.setSelected(false)
                    $0.backgroundColor = redFieldDarkBgColor.withAlphaComponent(0.8)
                    $0.isUserInteractionEnabled = true
                }
                
            }
        case .receivedDoc_preview:
            isUserInteractionEnabled = false
            radioItems.forEach {
                $0.backgroundColor = grayFieldBgColor
                $0.resizeMode = resizeViewMode
            }
        case .receivedDoc_completed:
            isUserInteractionEnabled = false
            radioItems.forEach {
                $0.backgroundColor = .clear
                $0.radioButton.outerCircleViewBackgroundColor = .clear
                $0.resizeMode = resizeViewMode
            }
        default:
            break
        }
    }
    

    public override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for item in radioItems{
            if item.frame.contains(point){
                return true
            }
        }
       
        return false
    }
    
    public override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if clipsToBounds || isHidden || alpha == 0 {
            return nil
        }

        for subview in subviews.reversed() {
            let subPoint = subview.convert(point, from: self)
            if let result = subview.hitTest(subPoint, with: event) {
                return result
            }
        }

        return nil
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        //update the previous frame value and view frames only if it changes.
        if previousViewFrame != frame {
            updateViewFrames(shouldUpdateRadioItemFrames: true)
            previousViewFrame = frame
        }
    }
    
    private func animateDashedBorder() {
        dashedBorder.frame = groupIndicatorView.bounds
        
        let pathAnimation = CABasicAnimation(keyPath: "path")
        pathAnimation.fromValue = dashedBorder.path!
        pathAnimation.duration = 0.2
        pathAnimation.toValue = UIBezierPath(rect: groupIndicatorView.layer.bounds
        ).cgPath
        pathAnimation.fillMode = .both
        pathAnimation.isRemovedOnCompletion = false
        pathAnimation.timingFunction = CAMediaTimingFunction(name: .linear)
        dashedBorder.add(pathAnimation, forKey: nil)
        
    }
    
    //show border view and change border color for items when view is being focused on editor page
    public func setFocused(_ isFocused: Bool) {
        groupIndicatorView.isHidden = !isFocused
        radioItems.forEach{
            $0.layer.borderColor = isFocused ? ZColor.buttonColor.cgColor
                : UIColor.clear.cgColor
            $0.layer.borderWidth = isFocused ? $0.frame.width * 0.05 : 0
        }
        //selectedRadioItem.safelyUnwrap{ setSelected(isFocused, forItem: $0) }
    }
    
    /*
    public func setSelected(_ isSelected: Bool,forItem radioItem: ZRadioGroupItem) {
        if isSelected {
            radioItem.layer.borderWidth = radioItem.frame.width * 0.1
            radioItem.layer.borderColor = ZColor.buttonColor.cgColor
        } else {
            radioItem.layer.borderWidth = 1
        }
    }
    */
    
    //show poptip for radio items when clicked in the field list page
    public override func setIsHighlighted(_ isHighlighted: Bool) {
        groupIndicatorShowTimer?.invalidate()
        guard isHighlighted else {
            dismissPoptip()
            return
        }
        
        dismissPoptip()
        for item in radioItems {
            var highlightMessage = item.radioValue.name
            
            if signField.isReadOnly {
                highlightMessage = String(format: "%@ (%@)",highlightMessage,"sign.mobile.common.readonly".ZSLString )
            }
            if !signField.isMandatory {
                highlightMessage = String(format: "%@ (%@)",highlightMessage,"sign.mobile.common.optional".ZSLString )
            }
            let possibleSides = item.frame.getPossibleSides(increamentedbySize: 0, restrictedToBounds: bounds)
            
            var direction: PopTipDirection = .none
            if possibleSides.contains(.top) {
                direction = .up
            } else if possibleSides.contains(.bottom) {
                direction = .down
            }
            
            let popTip = PopTip()
            popTip.font = ZSFont.regularFont(withSize: 15)
            popTip.shouldDismissOnTap = true
            popTip.shouldDismissOnTapOutside = true
            popTip.shouldDismissOnSwipeOutside = true
            popTip.arrowSize = CGSize(width: 15, height: 10)
            let padding : CGFloat = DeviceType.isIphone ? 5 : 8
            popTip.edgeInsets = .init(top: padding, left: padding, bottom: padding, right: padding)
            
            if resizeViewMode == .sentDoc_Create {
                popTip.bubbleColor = ZColor.bgColorDark
            } else {
                popTip.bubbleColor = ZColor.redColor
                popTip.layer.zPosition = 9999
            }
            popTip.show(text: highlightMessage, direction: direction, maxWidth: 250, in: superview!, from: item.frame, duration: 3)
            groupIndicatorView.isHidden = false
            popTips.append(popTip)
        }
        
        groupIndicatorShowTimer = Timer.scheduledTimer(withTimeInterval: 3.5, repeats: false) { (timer) in
            self.groupIndicatorView.isHidden = true
        }
        
    }
    
    private func dismissPoptip() {
        popTips.forEach({$0.hide(forced: true)})
        popTips.removeAll()
    }
}

extension ZRadioGroupView: ZRadioGroupItemDelegate {
    
    public func didBeginDragging(onItem radioItem: ZRadioGroupItem) {
        bringSubviewToFront(radioItem)
        //selectedRadioItem.safelyUnwrap{ setSelected(false, forItem: $0) }
        selectedRadioItem = radioItem
        setIsHighlighted(false)
        setFocused(true)
        delegate?.radioGroupBeginEditing(onRadioGroupView: self)
    }
    
    public func didChangeDragging(onItem radioItem: ZRadioGroupItem) {
        updateViewFrames()
        signField.radioGroup!.values[radioItem.tag] = radioItem.radioValue
        delegate?.radioGroupEditing(onRadioGroupView: self)
    }
    
    public func didEndDragging(onItem radioItem: ZRadioGroupItem) {
       var mutableRadioItems = radioItems
       mutableRadioItems.remove(element: radioItem)
       let newFrame = isOverLappingFrame(radioItem.frame, in: mutableRadioItems)
       resetPosition(forRadioItem: radioItem, withFrame: newFrame)
    }
    
    public func didTapped(onItem radioItem: ZRadioGroupItem) {
        if resizeViewMode == .receivedDoc_pending {
            radioItem.setSelected(true)
            signField.radioGroup?.values[radioItem.tag] = radioItem.radioValue
            resizeViewMode = .receivedDoc_completed
            delegate?.radioGroupDidTapped(onRadioGroupView: self)
        } else {
            didBeginDragging(onItem: radioItem)
        }
    }
    
    public func didTappedEditButton(onItem radioItem: ZRadioGroupItem) {
        didBeginDragging(onItem: radioItem)
        delegate?.radioGroupDidTappedEditButton(self,onRadioItem: radioItem)
    }
    
    func resetPosition(forRadioItem radioItem: ZRadioGroupItem,withFrame frame: CGRect) {
        UIView.animate(withDuration: 0.2, animations: {
            radioItem.frame = frame
            self.updateViewFrames(shouldAnimateDashedBorders:true)
        }) { _ in
            self.signField.radioGroup!.values[radioItem.tag] = radioItem.radioValue
            self.signField.radioGroup?.values.enumeratedInPlaceLoop({ (_, value) -> RadioValues in
                return self.calculateNormalisedFrame(value)
            })
            self.delegate?.radioGroupEndEditing(self, onRadioItem: radioItem)
        }
    }
    
    private func calculateNormalisedFrame(_ value: RadioValues) -> RadioValues{
        var mutableCopy = value
        mutableCopy.xValue = (mutableCopy.rect!.origin.x / self.frame.width) * 100
        mutableCopy.yValue = (mutableCopy.rect!.origin.y / self.frame.height) * 100
        mutableCopy.width = (mutableCopy.rect!.width / self.frame.width) * 100
        mutableCopy.height = (mutableCopy.rect!.height / self.frame.height) * 100
        return mutableCopy
    }
}


extension ZRadioGroupView {
    func updateViewFrames(shouldUpdateRadioItemFrames: Bool = false, shouldAnimateDashedBorders: Bool = false) {
        var newMaxY: CGFloat = 0
        var newMaxX: CGFloat = 0
        var newMinY = CGFloat.greatestFiniteMagnitude
        var newMinX = CGFloat.greatestFiniteMagnitude
        
        for radioItem in radioItems {
            
            if shouldUpdateRadioItemFrames {
                updateFrame(forRadioItem: radioItem)
            }
            
            let radioItemFrame = radioItem.frame
            newMaxY = max(newMaxY, radioItemFrame.maxY)
            newMinY = min(newMinY, radioItemFrame.minY)
            newMaxX = max(newMaxX, radioItemFrame.maxX)
            newMinX = min(newMinX, radioItemFrame.minX)
        }
        
        groupIndicatorView.frame = CGRect(x: newMinX, y: newMinY, width: newMaxX-newMinX, height: newMaxY-newMinY)
        
        if shouldAnimateDashedBorders {
            animateDashedBorder()
        } else {
            dashedBorder.removeAllAnimations()
            dashedBorder.path = UIBezierPath(rect: groupIndicatorView.bounds).cgPath
            dashedBorder.frame = groupIndicatorView.bounds
        }
    }
    
    private func updateFrame(forRadioItem radioItem: ZRadioGroupItem) {
        radioItem.radioValue.setRect(inBounds: bounds)
        radioItem.frame = radioItem.radioValue.actualViewFrameFromRect(outerMargin: outerMargin, size: radioItem.radioValue.rect!.height + totalMargin)
        signField.radioGroup!.values[radioItem.tag] = radioItem.radioValue
    }
    
    private func isOverLappingFrame(_ frame: CGRect,in radioItems: [ZRadioGroupItem]) -> CGRect {
        var newFrame: CGRect = frame
        for item in radioItems {
            let overLappingSide: OverLappingSide = frame.overLaps(with: item.frame)
            if overLappingSide != .none {
                let possibleFrame = item.frame.getNearestPossibleFrame(onSide: overLappingSide, forSize: item.frame.height, restrictedToBounds: self.frame)
                newFrame = isOverLappingFrame(possibleFrame, in: radioItems)
            }
        }
        return newFrame
    }
}
