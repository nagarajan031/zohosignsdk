//
//  Optional+.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 24/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public extension Optional {
    var isNull: Bool {
        return self == nil
    }
    
    var isNonNull: Bool {
        return self != nil
    }
    
    @discardableResult
    @inlinable func safelyUnwrap(_ completion: (Wrapped) throws -> Void) rethrows -> Bool {
        if let safelyUnwrapped = self {
            try? completion(safelyUnwrapped)
            return true
        }
        return false
    }
    
    @inlinable func safelyUnwrap<T>(_ completion: (T) throws -> Void) rethrows {
        if let safelyUnwrapped = self as? T {
            try? completion(safelyUnwrapped)
        }
    }
    
}
