//
//  CropViewController.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 14/11/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit
import SnapKit
import AMPopTip

class CropViewController: UIViewController {
    private let originalImage: UIImage
    private var filteredImage: UIImage!
    private var backgroundRemovedImage: UIImage? {
        willSet {
            cropview.changeImage = newValue
        }
    }
    public weak var delegate: SignatureCreatorDelegate?
    private let signatureColors: [UIColor] = [.black,.red,.blue,.green]
    private var heightConstraint: Constraint?
    private let stackViewHeight: CGFloat = 3 * 45 + 30
    public var isModalyPresented: Bool = false
    private var selectedButton: ZSButton?
    private let enhanceButtonImages: [(UIImage?,UIImage?)] = [
        (UIImage(named: "empty_background"), ZSImage.tintColorImage(withNamed: "doc_sign", col: ZColor.blackColor)),
        (UIImage.circle(diameter: 32, color: ZColor.whiteColor),ZSImage.tintColorImage(withNamed: "doc_sign", col: ZColor.blackColor)),
        (UIImage.circle(diameter: 32, color: UIColor.yellow),ZSImage.tintColorImage(withNamed: "doc_sign", col: ZColor.buttonColor))
    ]
    
    private lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = 15
        view.distribution = .fillEqually
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var cropview: CropPickerView = {
        let view = CropPickerView()
        view.delegate = self
        view.image = originalImage
        view.scrollMinimumZoomScale = 1
        view.scrollMaximumZoomScale = 5
        view.isCrop = true
        view.backgroundColor = .clear
        view.scrollBackgroundColor = .clear
        view.imageBackgroundColor = .clear
        return view
    }()
    
    
    init(image: UIImage) {
        self.originalImage = image
        self.filteredImage = SwiftUtils.generateContrastImage(5.0, originalImage.image(withBrightness: 0.4) ?? UIImage())
        //SwiftUtils.generateContrastImage(5.0, originalImage.withContrast(1.0, brightness: 0.4))
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let enhanceContainerView = UIView()
        enhanceContainerView.layer.cornerRadius = 30
        enhanceContainerView.clipsToBounds = true
        enhanceContainerView.backgroundColor = UIColor(red: 0.2, green: 0.203, blue: 0.207, alpha: 1)
        enhanceContainerView.addSubviews(stackView)
        view.addSubviews(cropview, enhanceContainerView)
        
        cropview.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(view.snp.topMargin)
        }
        
        enhanceContainerView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-20)
            make.width.equalTo(60)
            make.height.equalTo(stackViewHeight + 20)
        }
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: isModalyPresented ? "sign.mobile.common.cancel".ZSLString : "sign.mobile.common.retake".ZSLString, style: .plain, target: self, action: #selector(retakeClicked))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        navigationItem.rightBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneClicked))
        ]
        
        if !isModalyPresented {
            navigationController?.navigationBar.tintColor = ZColor.whiteColor
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationController?.navigationBar.shadowImage = UIImage()
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationBar.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        }
        
        
        prepareStackView()
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true && !isModalyPresented, animated: false)
    }
    
    @objc func retakeClicked(){
        if isModalyPresented {
            dismiss(animated: true, completion: nil)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func doneClicked(){
        cropview.crop()
    }
    
    private func prepareStackView(){
        for (index,image) in enhanceButtonImages.enumerated() {
            let enhanceButton = ZSButton()
            enhanceButton.layer.cornerRadius = 45/2
            enhanceButton.clipsToBounds = true
            enhanceButton.tag = index
            enhanceButton.setBackgroundImage(image.0, for: .normal)
            enhanceButton.setImage(image.1, for: .normal)
            enhanceButton.addTarget(self, action: #selector(enhanceChanged(_:)), for: .touchUpInside)
            enhanceButton.layer.borderColor = ZColor.buttonColor.cgColor
            if index == enhanceButtonImages.count - 1 {
                enhanceButton.layer.borderWidth = 3
                selectedButton = enhanceButton
            }
            stackView.addArrangedSubview(enhanceButton)
        }
        
        stackView.snp.makeConstraints { (make) in
            make.width.equalTo(45)
            make.center.equalToSuperview()
            make.height.equalTo(stackViewHeight)
        }
    }
    
    @objc private func enhanceChanged(_ sender: ZSButton){
        
        selectedButton?.layer.borderWidth = 0
        sender.layer.borderWidth = 3
        selectedButton = sender
        
        switch sender.tag {
        case 0:
            setImageViewWithEmptyBackground()
        case 1:
            view.backgroundColor = ZColor.bgColorWhite
            cropview.changeImage = self.filteredImage
        case 2:
            view.backgroundColor = ZColor.bgColorWhite
            cropview.changeImage = self.originalImage
        default:
            break
        }
        SwiftUtils.generateImpactFeedBack(for: .medium)
    }
    
    private func setImageViewWithEmptyBackground(){
        if let image = backgroundRemovedImage {
            cropview.changeImage = image
            view.backgroundColor = UIColor(patternImage: UIImage(named: "empty_background")!)
        } else {
            ZSProgressHUD.show(inView: view, status: "sign.mobile.activityIndicator.processing".ZSLString)
            let imageProcessor = ZSSignatureImageProcessor()
            imageProcessor.image = self.filteredImage
            imageProcessor.removeBackground(brightnessFactor: 1.0, thresshold: 100) {[weak self] image in
                ZSProgressHUD.dismiss()
                self?.backgroundRemovedImage = image
                self?.view.backgroundColor = UIColor(patternImage: UIImage(named: "empty_background")!)
                let button = self?.stackView.arrangedSubviews.first
                
                let popTip = PopTip()
                popTip.font = ZSFont.regularFont(withSize: 15)
                popTip.shouldDismissOnTap = true
                popTip.shouldDismissOnTapOutside = true
                popTip.bubbleColor = ZColor.bgColorDark
                popTip.borderWidth = 1
                popTip.borderColor = ZColor.whiteColor.withAlphaComponent(0.2)
                popTip.arrowSize = CGSize(width: 15, height: 10)
                let padding : CGFloat = DeviceType.isIphone ? 5 : 8
                popTip.edgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
                popTip.show(text: "Auto Enhance on", direction: .left, maxWidth: 250, in: self?.view ?? UIView(), from: button?.convert(button?.frame ?? .zero, to: self?.view ?? UIView()) ?? .zero, duration: 5)
            }
        }
    }
    
}

extension CropViewController: CropPickerViewDelegate {
    func cropPickerView(_ cropPickerView: CropPickerView, image: UIImage) {
        //for i in
        delegate?.signatureCreated(signImage: image)
        dismiss(animated: true, completion: nil)
    }
    
    func cropPickerView(_ cropPickerView: CropPickerView, error: Error) {
        showAlert(withMessage: error.localizedDescription)
    }
}
