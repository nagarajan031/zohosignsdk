//
//  ZSPopupMenuWithIcon.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 20/04/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZSPopupMenuItem: NSObject{
    public var icon : UIImage?
    public var name : String?
    public var isDestructive = false

    public var handler:  ((ZSPopupMenuItem) -> ())?
    
    public override init(){
        
    }
    
    public init(icon: UIImage, name: String, handler: ((ZSPopupMenuItem) -> ())? ) {
        self.icon = icon
        self.name = name
        self.handler = handler
    }
}

public class ZSPopupMenuWithIcon: UIView,UIGestureRecognizerDelegate , UITableViewDataSource, UITableViewDelegate,CAAnimationDelegate {
    var menuTableView : UITableView = UITableView()
    public var menuItemArray : [ZSPopupMenuItem] = []
    var alphaView = UIView()
    let bgView = UIView()
    let dummyTopPadding : CGFloat = 20;
    public var bottomPadding : CGFloat = 0
    private var selectedMenuItem: ZSPopupMenuItem?

    public func showPopupInView(_ superView: UIView, fromHeight: CGFloat)
    {
        
        superView.addSubview(self)
        self.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.frame = superView.bounds
        
        alphaView.frame                 =   CGRect(x: 0, y: 0, width: self.frame.size.width , height: fromHeight)
        alphaView.backgroundColor       =    ZColor.bgColorDullBlack
        alphaView.alpha                 =    0
        alphaView.clipsToBounds         =   false;
        alphaView.isUserInteractionEnabled    =   false
        alphaView.autoresizingMask = [.flexibleWidth]
        self.addSubview(alphaView)
        
        let opacityAnimation : CABasicAnimation = CABasicAnimation(keyPath: "opacity");
        opacityAnimation.fromValue = NSNumber(value: 0.0 as Float)
        opacityAnimation.toValue = NSNumber(value: 1.0 as Float)
        opacityAnimation.isRemovedOnCompletion  = false
        opacityAnimation.fillMode = CAMediaTimingFillMode.forwards
        alphaView.layer.add(opacityAnimation, forKey: nil)
        
        let tableHeight : CGFloat = (TableviewCellSize.normal * CGFloat(menuItemArray.count)) + bottomPadding + dummyTopPadding
        
        bgView.frame =  CGRect(x: 0, y: alphaView.frame.size.height - tableHeight, width: self.bounds.size.width, height: tableHeight )
        bgView.clipsToBounds = true
//        bgView.layer.cornerRadius = IS_IPHONE_X ? 10 : 0
        self.addSubview(bgView)
        
        

        menuTableView.frame                             =    CGRect(x: 0, y: tableHeight , width: self.bounds.size.width, height: tableHeight + 50)
        menuTableView.autoresizingMask                  =   .flexibleWidth
        menuTableView.delegate                          =    self
        menuTableView.dataSource                        =    self
        menuTableView.rowHeight                         =    TableviewCellSize.normal
        menuTableView.setSeparatorStyle()
        menuTableView.separatorInset                    =   .zero
        menuTableView.isScrollEnabled                   =   false
        menuTableView.showsVerticalScrollIndicator      =    false
        menuTableView.backgroundColor                   =   ZColor.bgColorWhite;
        menuTableView.clipsToBounds = true
        menuTableView.layer.cornerRadius = iPhoneXCurveRadius
        bgView.addSubview(menuTableView)
        
        UIView.animate(withDuration: 0.35, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: [.curveEaseIn ,.beginFromCurrentState], animations: {
            self.menuTableView.frame                             =    CGRect(x: 0, y: self.dummyTopPadding, width: self.bounds.size.width, height: tableHeight + 50 - self.dummyTopPadding)

        }) { (completed) in
            
        };
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.numberOfTapsRequired = 1
        tapGesture.delegate    =    self
        tapGesture.addTarget(self, action: #selector(self.hideView))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc public func hideView()
    {
        let opacityAnimation : CABasicAnimation = CABasicAnimation(keyPath: "opacity");
        opacityAnimation.fromValue = NSNumber(value: 1.0 as Float)
        opacityAnimation.toValue = NSNumber(value: 0.0 as Float)
        opacityAnimation.delegate    =    self
        opacityAnimation.isRemovedOnCompletion  = false
        opacityAnimation.fillMode = CAMediaTimingFillMode.forwards
        alphaView.layer.add(opacityAnimation, forKey: nil)
        
        
        UIView.animate(withDuration: 0.35, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options:[.curveEaseOut ,.beginFromCurrentState], animations: {
            self.menuTableView.frame                             =    CGRect(x: 0, y: self.menuTableView.frame.size.height , width: self.bounds.size.width, height: self.menuTableView.frame.size.height)
        }) { (completed) in
            if let menu = self.selectedMenuItem, let handler = menu.handler {
                handler(menu)
            }
            self.removeFromSuperview()
            self.isHidden = true
        };
        
    }
    
    
    // MARK: - TableView Data Source
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  menuItemArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let identifier = "Cell"
        
        var cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        if cell == nil {
            cell =  UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: identifier)
        }
        let menuItem =  menuItemArray[indexPath.row]
        
        cell.textLabel?.text            =       menuItem.name
        cell.textLabel?.font            =       ZSFont.listTitleFont()
//        cell.textLabel?.textColor       =       menuItem.isDestructive ? ZColor.redColor :  ZColor.primaryTextColor
        cell.textLabel?.textColor       =       menuItem.isDestructive ? ZColor.redColor :  ZColor.primaryTextColor
        cell.imageView?.image           =       menuItem.icon
//        cell.imageView?.tintColor       =       menuItem.isDestructive ? ZColor.redColor :  ZColor.primaryTextColor
        cell.imageView?.tintColor       =     ZColor.buttonColor//UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.00);
        cell.backgroundColor            =       ZColor.bgColorWhite
        
        let selectedView = UIView()
        selectedView.backgroundColor = ZColor.listSelectionColor;
        cell.selectedBackgroundView = selectedView;
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let menuItem =  menuItemArray[indexPath.row]
        selectedMenuItem = menuItem
        hideView()
        
    }
    
    //MARK : - gesture Recognizer delegate
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view != self)
        {
            return false
        }
        return true
    }
    
    //MARK : - Animation delegate
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
       self.removeFromSuperview()
        self.isHidden = true
    }

}
