//
//  FontManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 03/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public enum TextFonts: String, CaseIterable {
    case roboto = "Roboto"
    case liberationSerif = "Liberation Serif"
    case dejaVuSans = "DejaVu Sans"
    case heuristica = "Heuristica"
    case texGyreCursor = "texgyrecursor-regular"
    static var allCasesRawValue: [String] {
        TextFonts.allCases.map({$0.rawValue})
    }
}




public class FontManager {
    static let shared = FontManager()
    
    let textFontsArray = [["fontFamily":"Roboto",
                           "normal":"Roboto",
                           "italic":"Roboto-Italic",
                           "bold":"Roboto-Bold",
                           "boldItalics":"Roboto-BoldItalic"],
                          
                          ["fontFamily":"Liberation Serif",
                           "normal":"LiberationSerif",
                           "italic":"LiberationSerif-Italic",
                           "bold":"LiberationSerif-Bold",
                           "boldItalics":"LiberationSerif-BoldItalic"],
                          
                          ["fontFamily":"DejaVu Sans",
                           "normal":"DejaVuSans",
                           "italic":"DejaVuSans-Oblique",
                           "bold":"DejaVuSans-Bold",
                           "boldItalics":"DejaVuSans-BoldOblique"],
                          
                          ["fontFamily":"Heuristica",
                           "normal":"Heuristica",
                           "italic":"Heuristica-Italic",
                           "bold":"Heuristica-Bold",
                           "boldItalics":"Heuristica-BoldItalic"],
                          
                          ["fontFamily":"TeXGyreCursor",
                           "normal":"texgyrecursor-Regular",
                           "italic":"texgyrecursor-Italic",
                           "bold":"texgyrecursor-Bold",
                           "boldItalics":"texgyrecursor-BoldItalic"]]
    
    
    func getFontName(withFontFamily fontFamily: String, boldTrait isBold: Bool, italicTrait isItalic: Bool) -> String {

        let filteredFontDict =  textFontsArray.filter { (dict) -> Bool in
            return dict["fontFamily"] == fontFamily
        }.first
        
        
        guard let dict  = filteredFontDict  else{
            return "Helvetica"
        }

        var fontName: String? = nil
        if isBold && isItalic {
            fontName = dict["boldItalics"]
        } else if isItalic {
            fontName = dict["italic"]
        } else if isBold {
            fontName = dict["bold"]
        } else {
            fontName = dict["normal"]
        }
        
        guard let font = fontName,(UIFont(name: font , size: 8) != nil) else{
            return "Helvetica"
        }

       
        return font
    }

    public class func calibaratedServerFontSize(forDeviceFontSize deviceFontSize: Int, pageWidth: CGFloat, imageWidth imgWidth: CGFloat) -> Int {

        let fontRatio = FontManager.fieldFontRatio(pageWidth, imageWidth: imgWidth)


        var fontSize = Int(roundf(Float(CGFloat(deviceFontSize) / fontRatio)))
        if fontSize >= 100 {
            fontSize = 99
        }
        else if fontSize < 5{
            fontSize = 8
        }
        return fontSize
    }

    public class func calibaratedDeviceFontSize(forServerFontSize serverFontSize: String?, pageWidth: CGFloat, imageWidth imgWidth: CGFloat) -> Int {
        guard let serverFontSize = serverFontSize else {
            return 1
        }
        

        let fontRatio = FontManager.fieldFontRatio(pageWidth, imageWidth: imgWidth)


        let fontSize = Int(roundf(Float(CGFloat(Int(serverFontSize) ?? 0) * fontRatio)))
        return fontSize

    }

    class func fieldFontRatio(_ pageWidth: CGFloat, imageWidth imgWidth: CGFloat) -> CGFloat {

        var fontRatio = (pageWidth) / imgWidth

        if DeviceType.isIphone {
            return pageWidth / imgWidth
        }
        if pageWidth < 200 {
            fontRatio = pageWidth / imgWidth
        } else {
            fontRatio = pageWidth / imgWidth
        }

        return fontRatio
    }

    
}
