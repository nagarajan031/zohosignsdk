//
//  ZSOnOffSwitchTableViewCell.swift
//  ZohoSign
//
//  Created by Nagarajan S on 26/04/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZSOnOffSwitchTableViewCell: UITableViewCell {

    public let  onOffSwitch    = UISwitch()

    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    
        textLabel?.textColor = ZColor.primaryTextColor
        
        selectionStyle =   .none
        onOffSwitch.frame           =   CGRect(x: 0, y: 9 , width: 50, height: 32)
        onOffSwitch.tag             =   1;
        accessoryView = onOffSwitch
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    public func setEnable(_ enable : Bool)  {
        onOffSwitch.isEnabled = enable
        
        textLabel?.isUserInteractionEnabled = enable
        textLabel?.textColor    =  (enable ?   ZColor.primaryTextColor : ZColor.primaryTextColor.withAlphaComponent(0.3))
    }
    
    

}
