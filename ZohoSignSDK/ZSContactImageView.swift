//
//  ZSContactImageView.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 22/01/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZSContactImageView: UIImageView {

    
    var twoCharacters: Bool = true
    /// Sets font for text (Default is UIFont(20) **UIFONT not currently supported in @IBInspectable
    public var textFont: UIFont = UIFont.systemFont(ofSize: 22)
    public var fontSize: CGFloat = 22{
        didSet{
            textFont = textFont.withSize(fontSize)
            setStoryboardImage()
        }
    }
    public var labelFont: String?{
        didSet{
            if let font = labelFont{
                if let setFont = UIFont(name: font, size: fontSize){
                    self.textFont = setFont
                    setStoryboardImage()
                }
            }
        }
    }
    /// Sets color of text being displayed, default is white color
    public var textColor: UIColor = UIColor.white{
        didSet{
            setStoryboardImage()
        }
    }
    /// Set text to be displayed in UIImageView, default is "GK".
    public var text: String = "GK"{
        didSet{
            setStoryboardImage()
        }
    }
    public var username: Bool = false{
        didSet{
            setStoryboardImage()
        }
    }
    public var backgroundImage: UIImage? = nil{
        didSet{
            setStoryboardImage()
        }
    }
    /// Returns a circular if set true, default is false
    public var circle: Bool = true{
        didSet{
            if circle{
                
                self.layer.cornerRadius = self.bounds.width / 2
            }else{
                self.layer.cornerRadius = 0
            }
        }
    }
    /// Set background color your imageview
    public var borderColor: UIColor = UIColor.clear{
        didSet{
            setStoryboardImage()
        }
    }
    /// Set background color your imageview
    public var fillColor: UIColor = UIColor.lightGray{
        didSet{
            setStoryboardImage()
        }
    }
    override public var bounds: CGRect {
        didSet {
            setStoryboardImage()
        }
    }
    private func setStoryboardImage(){
        setImageText(text: text, backgroundImage: backgroundImage, username: username, font: textFont, textColor: textColor, fillColor: fillColor, circle: circle)
    }
    public func setImageText(text: String, backgroundImage: UIImage? = nil, username: Bool = false, font: UIFont = UIFont.systemFont(ofSize: 22), textColor: UIColor = UIColor.white, fillColor: UIColor, circle: Bool = true){
        var imgText = text
        if username{
            imgText = getCharactersFromName(text: text)
        }
        let attributes = getAttributedText(text: imgText, color: textColor, textFont: font)
        let attributedText = NSAttributedString(string: imgText, attributes: attributes)
        self.image = createImage(attributedString: attributedText, backgroundImage: backgroundImage, backgroundColor: fillColor)
        
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = self.bounds.width / 2

    }
    //MARK: Private funcs
    
    /**
     Returns an NSAttribute based on the color and font, size is determined by the number of characters in text
     - parameter text:     size is determined by number of characters
     - parameter color:    color for NSForegroundColorAttribute
     - parameter textFont: font for NSFontAttribute
     
     - returns: [String: AnyObject] to be used as an NSAttribute
     */
    func getAttributedText(text: String, color: UIColor, textFont: UIFont) -> [NSAttributedString.Key: AnyObject] {
        let area:CGFloat = self.bounds.width * textFont.pointSize
        let size = sqrt(area / CGFloat(text.count))
        let attribute: [NSAttributedString.Key:AnyObject] = [
            NSAttributedString.Key.foregroundColor: color,
            NSAttributedString.Key.font: textFont.withSize(size)
        ]
        return attribute
    }
    /**
     Renders the text into the ImageView
     
     - parameter attributedString: attributes for the text to be rendered
     - parameter backgroundColor:  background color for UIImageVIew
     
     - returns: an ImageView with text rendered
     */
    
    private func createImage(attributedString: NSAttributedString, backgroundImage: UIImage? = nil, backgroundColor: UIColor) -> UIImage {
        let scale = UIScreen.main.scale
        let bounds = self.bounds
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, scale)
        var context = UIGraphicsGetCurrentContext()
        
        if context == nil {
            let width = Int(self.frame.width)
            let height = Int(self.frame.height)
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            let bytesPerPixel = 4
            let bytesPerRow = bytesPerPixel * width
            let bitsPerComponent = 8
            let bitmapByteCount = bytesPerRow * height
            let dataPointer:UnsafeMutableRawPointer = calloc(bitmapByteCount, 1)!

            let context = CGContext(data: dataPointer, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue|CGBitmapInfo.byteOrder32Big.rawValue)
            UIGraphicsPushContext(context!)
        }
        
        if (circle) {
            let path = CGPath(ellipseIn: self.bounds, transform: nil);
            context!.addPath(path)
            context?.clip()
        }
        if backgroundImage != nil{
            backgroundImage!.draw(in: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        }else{
            context!.setFillColor(backgroundColor.cgColor)
            context!.fill(CGRect(x: 0, y: 0, width: bounds.size.width, height: bounds.size.height))
//            context!.setStrokeColor(ZColor.redColor.cgColor)
//            context!.stroke(CGRect(x: 0, y: 0, width: bounds.size.width, height: bounds.size.height), width: 1)
        }
        let textSize = attributedString.size()
        let rect = CGRect(x: bounds.size.width/2 - textSize.width/2, y: bounds.size.height/2 - textSize.height/2, width: textSize.width, height: textSize.height)
        attributedString.draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!;
    }
    /**
     Retrive initals from a full name
     
     - parameter text: full name
     
     - returns: returns a two character string (first inital and last inital
     */
    func getCharactersFromName(text: String) -> String {
        let username = text.components(separatedBy: " ")
        var initial = String()
        if let initalFirst = username.first?.first{
            initial.append(initalFirst)
        }
        if username.count > 1{
            if let initalSecond = username[1].first {
                initial.append(initalSecond)
            }
        }
        return initial
    }

}
