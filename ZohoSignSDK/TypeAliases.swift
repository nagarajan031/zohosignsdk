//
//  TypeAliases.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 06/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import Foundation
public typealias ResponseDictionary = Dictionary<AnyHashable,Any>
