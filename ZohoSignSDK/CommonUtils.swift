//
//  CommonUtils.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 31/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import Foundation
import SSZipArchive
import AVKit




public class CommonUtils {
    //MARK:- Inits
    public class func signAppSharedGroupID() -> String {
        guard let  sharedGroupId = appBundleIdDictionary()?[ZSSharedGroupID] else  {
            return ""
        }
        
        return sharedGroupId
    }
    
    
    
//
//    class func signAppID() -> String? {
//        let commonDict = self.commonPropertiesDictionary()
//
//        if commonDict != nil && commonDict?[ZSAppID] != nil {
//            return commonDict?[ZSAppID] as? String
//        }
//
//        return ""
//    }
//
    class func signAppSharedKeychainGroupID() -> String {
        let bundlePrefixId = self.bundleSeedID()
        return "\(bundlePrefixId ?? "").com.zoho.sign"
    }

    class func bundleSeedID() -> String {
            let queryLoad: [String: AnyObject] = [
                kSecClass as String: kSecClassGenericPassword,
                kSecAttrAccount as String: "bundleSeedID" as AnyObject,
                kSecAttrService as String: "" as AnyObject,
                kSecReturnAttributes as String: kCFBooleanTrue
            ]

            var result : AnyObject?
            var status = withUnsafeMutablePointer(to: &result) {
                SecItemCopyMatching(queryLoad as CFDictionary, UnsafeMutablePointer($0))
            }

            if status == errSecItemNotFound {
                status = withUnsafeMutablePointer(to: &result) {
                    SecItemAdd(queryLoad as CFDictionary, UnsafeMutablePointer($0))
                }
            }

            if status == noErr {
                if let resultDict = result as? [String: Any], let accessGroup = resultDict[kSecAttrAccessGroup as String] as? String {
                    let components = accessGroup.components(separatedBy: ".")
                    return components.first ?? ""
                }else {
                    return ""
                }
            } else {
                ZError("Error getting bundleSeedID to Keychain")
                return ""
            }
        
//        var query: [AnyHashable : Any]? = nil
//        if let kCFBooleanTrue = kCFBooleanTrue {
//            query = [
//            kSecClass as String : kSecClassGenericPassword as String,
//            kSecAttrAccount : "bundleSeedID",
//            kSecAttrService : "",
//            kSecReturnAttributes : kCFBooleanTrue
//        ]
//        }
//        var result: CFDictionary? = nil
//        var status: OSStatus? = nil
//        if let query = query as? CFDictionary? {
//            status = SecItemCopyMatching(query, &result as? CFTypeRef?)
//        }
//        if status == errSecItemNotFound {
//            if let query = query as? CFDictionary? {
//                status = SecItemAdd(query, &result as? CFTypeRef?)
//            }
//        }
//        if status != errSecSuccess {
//            return nil
//        }
//        let accessGroup = (result as? [AnyHashable : Any])?[kSecAttrAccessGroup as String] as? String
//        let components = accessGroup?.components(separatedBy: ".")
//        let bundleSeedID = ((components as NSArray?)?.objectEnumerator())?.nextObject() as? String
//        return bundleSeedID
    }
//
    
    public class func appBundleIdDictionary() -> [String: String]?{
        let bundleIDs = Bundle.main.infoDictionary?[ZSignAppBundleIDs] as? [String: String]
        
        return bundleIDs
    }
    
   public class func machineName() -> String {
    
      var systemInfo = utsname()
      uname(&systemInfo)
      let machineMirror = Mirror(reflecting: systemInfo.machine)
      return machineMirror.children.reduce("") { identifier, element in
        guard let value = element.value as? Int8, value != 0 else { return identifier }
        return identifier + String(UnicodeScalar(UInt8(value)))
      }
    }

            
    static func getFieldTypeIcon(fieldType:ZSignFieldType, color: UIColor) -> UIImage
    {
        var iconImg = ZSImage.tintColorImage(withNamed: "menu_textfield", col: color)
        switch fieldType {
            case .signature:
                iconImg = ZSImage.tintColorImage(withNamed: "menu_sign", col: color)
            case .initial:
                iconImg = ZSImage.tintColorImage(withNamed: "menu_initial", col: color)
            case .email:
                iconImg = ZSImage.tintColorImage(withNamed: "menu_email", col: color)
            case .name:
                iconImg = ZSImage.tintColorImage(withNamed: "menu_name", col: color)
            case .company:
                iconImg = ZSImage.tintColorImage(withNamed: "menu_companyname", col: color)
            case .jobTitle:
                iconImg = ZSImage.tintColorImage(withNamed: "menu_jobtitle", col: color)
            case .signDate,.date:
                iconImg = ZSImage.tintColorImage(withNamed: "menu_date", col: color)
            case .textBox:
                iconImg = ZSImage.tintColorImage(withNamed: "menu_textfield", col: color)
            case .checkBox:
                iconImg = ZSImage.tintColorImage(withNamed: "menu_checkbox", col: color)
            case .dropDown:
                iconImg = ZSImage.tintColorImage(withNamed: "menu_dropdown", col: color)
            case .attachment:
                iconImg = ZSImage.tintColorImage(withNamed: "menu_attachment", col: color)
            case .radioGroup:
                iconImg = ZSImage.tintColorImage(withNamed: "menu_textfield", col: color)//TODO: change
            case .others:
                iconImg = ZSImage.tintColorImage(withNamed: "menu_textfield", col: color)
        }
        
        return iconImg!
    }
    

    
    //MARK: - File Handling
    public static func mergePDF(listOfFilePaths : [String],outputFileName : String) -> String? {
        if listOfFilePaths.count == 0{
            return nil
        }
        else if (listOfFilePaths.count == 1) {
            return listOfFilePaths.first
        }
        
        var fileName = outputFileName
        if !outputFileName.contains("zip") {
            fileName = String(format: "%@.zip", outputFileName)
        }
        
        let folderPath = ZSFileManager.shared.getSubFolder(type: .documents, name: "tempSharePdf")
        if FileManager.default.fileExists(atPath: folderPath) {
            try? FileManager.default.removeItem(atPath: folderPath)
        }
        
        try? FileManager.default.createDirectory(atPath: folderPath, withIntermediateDirectories: true, attributes: nil)
        
        let pdfPathOutput = String(format: "%@/%@", folderPath,outputFileName)
        
        SSZipArchive.createZipFile(atPath: pdfPathOutput, withFilesAtPaths: listOfFilePaths)
        
        return pdfPathOutput
    }
    
    
    //MARK: - Helpers
    public class func hasUserGrantAccessForCamera(successBlock: @escaping () -> Void, failureBlock: @escaping () -> Void) {

        let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)

        if status == .authorized {
            successBlock()
        } else if status == .denied {
            failureBlock()
        } else if status == .restricted {
            // restricted, normally won't happen
        } else if status == .notDetermined {
            // not determined?!
            //NSLog(@"%@", @"Camera access not determined. Ask for permission.");

            AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                if granted {
                    DispatchQueue.main.async(execute: {
                        successBlock()
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        //                    failureBlock(); //Not needed
                    })
                }
            })
        }

    }
    
    public class func openUrlInBrowser(_ url : URL?){
        guard let url = url else {
            return
        }
        UIApplication.shared.open(url, options:[:], completionHandler: nil)
    }
    
    public class func openUrlStrInBrowser(_ urlStr : String){
        guard let url = URL(string: urlStr) else {
            return
        }
        UIApplication.shared.open(url, options:[:], completionHandler: nil)
    }

}
