//
//  UIImage+.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 24/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public extension UIImage{
    
    func compressSignatureData() -> Data
    {
        var img : UIImage? = self
        var imgData = pngData()
        ZInfo("Size of Image(bytes):%lu",imgData?.count)
        
        var scalledQuality = 1;
        while ((imgData?.count ?? 1) > 400000) {
            img = imageWithSize(size: CGSize(width: size.width/2, height: size.height/2))
            imgData = img?.pngData()
            ZInfo("Size of Image(bytes):%lu",imgData?.count)
            scalledQuality += 1
        }
        
        return imgData ?? Data()
    }


    
    
    class func circle(diameter: CGFloat, color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: diameter,height: diameter), false, 0)
        let ctx = UIGraphicsGetCurrentContext()!
        ctx.saveGState()
        
        let rect = CGRect(x: 0, y: 0, width: diameter, height: diameter)
        ctx.setFillColor(color.cgColor)
        ctx.fillEllipse(in: rect)
        
        ctx.restoreGState()
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return img!
    }
    /// Compress image size
    ///
    /// - Parameters:
    ///     - size: -
    ///
    /// - Returns: the compressed image.
    ///
    func reducedFileSize() -> UIImage{
        if let data = self.jpegData(compressionQuality: 1),data.count > 1000000, let imgData = self.jpegData(compressionQuality: 0.6), let reducedImage = UIImage(data: imgData){
            return reducedImage
        }
        return self
    }
    
    /// Resizes an image to the specified size.
    ///
    /// - Parameters:
    ///     - size: the size we desire to resize the image to.
    ///
    /// - Returns: the resized image.
    ///
    func downsampleImage(size: CGSize) -> UIImage? {
        return UIGraphicsImageRenderer(size: size).image { (context) in
            draw(in: CGRect(origin: .zero, size: size))
        }
        
    }
    
    /// Resizes an image to the specified size and adds an extra transparent margin at all sides of
    /// the image.
    ///
    /// - Parameters:
    ///     - size: the size we desire to resize the image to.
    ///     - extraMargin: the extra transparent margin to add to all sides of the image.
    ///
    /// - Returns: the resized image.  The extra margin is added to the input image size.  So that
    ///         the final image's size will be equal to:
    ///         `CGSize(width: size.width + extraMargin * 2, height: size.height + extraMargin * 2)`
    ///
    
    func imageWithSize(size: CGSize, extraMargin: CGFloat = 0) -> UIImage? {
        
        let imageSize = CGSize(width: size.width + extraMargin * 2, height: size.height + extraMargin * 2)
        
        UIGraphicsBeginImageContextWithOptions(imageSize, false, UIScreen.main.scale);
        let drawingRect = CGRect(x: extraMargin, y: extraMargin, width: size.width, height: size.height)
        draw(in: drawingRect)
        
        let resultingImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return resultingImage
    }
    
    
    /// Resizes an image to the specified size.
    ///
    /// - Parameters:
    ///     - size: the size we desire to resize the image to.
    ///     - roundedRadius: corner radius
    ///
    /// - Returns: the resized image with rounded corners.
    ///
    func imageWithSize(size: CGSize, roundedRadius radius: CGFloat) -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        if let currentContext = UIGraphicsGetCurrentContext() {
            let rect = CGRect(origin: .zero, size: size)
            currentContext.addPath(UIBezierPath(roundedRect: rect,
                           byRoundingCorners: .allCorners,
                           cornerRadii: CGSize(width: radius, height: radius)).cgPath)
            currentContext.clip()
            
            //Don't use CGContextDrawImage, coordinate system origin in UIKit and Core Graphics are vertical oppsite.
            draw(in: rect)
            currentContext.drawPath(using: .fillStroke)
            let roundedCornerImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return roundedCornerImage
        }
        return nil
    }
    
    static func buttonImage(forName name: String) -> UIImage {
        return ZSImage.tintColorImage(withNamed: name, col: ZColor.buttonColor)
    }
    
    
    

    func image(withBrightness brightnessFactor: Float) -> UIImage? {

                
        let inputImage = CIImage(image: self)
    
        let contrastParameters = [
            "inputBrightness": NSNumber(value: brightnessFactor),
            "inputContrast": NSNumber(value: 2.0)
        ]
//        let outputImage = inputImage.applyingFilter("CIColorControls", parameters: parameters)

        guard let outputImage =  inputImage?.applyingFilter("CIColorControls", parameters: contrastParameters) else {
            return nil
        }
        
        let context = CIContext(options: nil)
        
        guard let img = context.createCGImage(outputImage, from: outputImage.extent) else { return nil }
        
        
        return UIImage(cgImage: img)
    }
        
    
    func crop(_ rect: CGRect) -> UIImage? {
        var rect = rect

        rect = CGRect(x: rect.origin.x * scale, y: rect.origin.y * scale, width: rect.size.width * scale, height: rect.size.height * scale)

        let imageRef = cgImage?.cropping(to: rect)
        var result: UIImage? = nil
        if let imageRef = imageRef {
            result = UIImage(cgImage: imageRef, scale: scale, orientation: imageOrientation)
        }
        return result
    }

}

