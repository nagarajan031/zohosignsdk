//
//  SignatureEditorControlsView.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 29/11/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit
import SnapKit

enum SignatureEditorControls {
    case crop(_ shouldShow: Bool)
    case filter(_ shouldShow: Bool)
    case rotate(_ angle: CGFloat)
    
    func isCrop() -> Bool{
        switch self {
        case let .crop(shouldShow):
            return shouldShow
        default:
            return false
        }
    }
    
    func isFilter() -> Bool {
        switch self {
        case let .filter(shouldShow):
            return shouldShow
        default:
            return false
        }
    }
}

protocol SignatureEditorControlsViewDelegate: NSObjectProtocol {
    func signatureEditorControlDidClicked(_ signaturEditorControl: SignatureEditorControls)
}

class SignatureEditorControlsView: UIView {
    
    private lazy var stackView: UIStackView = {
       let view = UIStackView()
        view.axis = DeviceType.isMac ? .horizontal : .vertical
        view.distribution = .fillEqually
        view.spacing = 28
        return view
    }()
    
    private lazy var selectionView: UIView = {
       let view = UIView()
        view.backgroundColor = ZColor.buttonColor
        view.clipsToBounds = true
        view.layer.cornerRadius = 4
        return view
    }()
    
    private var signatureEditorControl: SignatureEditorControls = .crop(false)
    private var editorControlImages: [UIImage?] = [
        ZSImage.tintColorImage(withNamed: "crop", col: ZColor.whiteColor),
        ZSImage.tintColorImage(withNamed: "image_filter", col: ZColor.whiteColor),
        ZSImage.tintColorImage(withNamed: "rotate", col: ZColor.whiteColor)
    ]
    public weak var delegate: SignatureEditorControlsViewDelegate?
    private var selectionViewLeadingConstraint: Constraint?
    private var selectionViewTopConstraint: Constraint?
    public var rotationAngle: CGFloat = 0
    
    init(delegate: SignatureEditorControlsViewDelegate) {
        super.init(frame: .zero)
        self.delegate = delegate
        for (index,image) in editorControlImages.enumerated() {
            let button = ZSButton()
            button.setImage(image?.downsampleImage(size: CGSize(width: 30, height: 30)), for: .normal)
            button.tag = index
            button.addTarget(self, action: #selector(editorControlsClicked(_:)), for: .touchUpInside)
            button.backgroundColor = .clear
            stackView.addArrangedSubview(button)
        }
        
        addSubviews(stackView,selectionView)
        stackView.snp.makeConstraints { (make) in
            if DeviceType.isMac {
                make.centerX.top.equalToSuperview()
                make.height.equalTo(50)
                make.width.equalTo(50 * 3 + 46)
            } else {
                make.centerY.leading.equalToSuperview()
                make.width.equalTo(50)
                make.height.equalTo(50 * 3 + 46)
            }
        }
        selectionView.snp.makeConstraints { (make) in
            make.width.height.equalTo(8)
            if DeviceType.isMac {
                selectionViewLeadingConstraint = make.leading.equalTo(stackView).constraint
                make.top.equalTo(stackView.snp.bottom).offset(-5)
            } else {
                selectionViewTopConstraint = make.top.equalTo(stackView).constraint
                make.leading.equalTo(stackView.snp.trailing).offset(-5)
            }
            
        }
    }
    
    func setBackground(isBackgroundColorNeeded: Bool) {
        backgroundColor = UIColor(white: 0, alpha: isBackgroundColorNeeded ? 0.6 : 0)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        perform({
            let sender = self.stackView.arrangedSubviews[1] as! ZSButton
            self.updateSelectionView(sender)
            self.editorControlsClicked(sender)
        }, afterDelay: 0.5)
        
    }
    
    @objc func editorControlsClicked(_ sender: ZSButton) {
        updateSelectionView(sender)
        switch sender.tag {
        case 0:
            signatureEditorControl = .crop(!signatureEditorControl.isCrop())
            delegate?.signatureEditorControlDidClicked(signatureEditorControl)
        case 1:
            signatureEditorControl = .filter(!signatureEditorControl.isFilter())
            delegate?.signatureEditorControlDidClicked(signatureEditorControl)
        case 2:
            rotationAngle += .pi/2
            rotationAngle.formTruncatingRemainder(dividingBy: .pi*2)
            delegate?.signatureEditorControlDidClicked(.rotate(.pi/2))
        default:
            break
        }
    }
    
    func updateSelectionView(_ sender: ZSButton) {
        if DeviceType.isMac {
            selectionViewLeadingConstraint?.update(offset: sender.center.x - 5)
        } else {
            selectionViewTopConstraint?.update(offset: sender.center.y - 5)
        }
        
        layoutIfNeeded()
    }
}
