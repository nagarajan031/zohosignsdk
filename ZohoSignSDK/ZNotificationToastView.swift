//
//  ZNotificationToastView.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 02/05/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZNotificationToastView: UIView {

//    public static var shared = ZNotificationToastView()

    public var tapHandler : (() -> ())?
    
    public var hideAfter : TimeInterval = 3
    public var actionBtn = ZSButton()
    lazy var msgLabel: UILabel = {
        let label  = UILabel()
        label.textColor = ZColor.whiteColor
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        label.font = ZSFont.regularFontLarge()
        return label
    }()
    
    lazy var titleLabel: UILabel = {
        let label  = UILabel()
        label.textColor = ZColor.whiteColor
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.font = ZSFont.mediumFontLarge()
        return label
    }()
    
    var isActionNeeded = false
    var isAnimationOn = true
    
    var myTimer : Timer?
    
    var viewHeight : CGFloat = 0
    
    let topPadding : CGFloat = 15
    
    @objc public init(title: String,actionTitle: String? = nil,actionTarget : Any?  = nil, action: Selector? = nil) {
        super.init(frame: CGRect.zero)
        msgLabel.text = title
        if let _action = action{
            isActionNeeded = true
            actionBtn.addTarget(actionTarget, action: _action, for: .touchUpInside)
        }
        actionBtn.titleLabel?.font = ZSFont.mediumFontLarge()
        actionBtn.setTitle(actionTitle, for: .normal)
        actionBtn.setTitleColor(UIColor(red:0.32, green:0.52, blue:0.78, alpha:1.00), for: .normal)
//        actionBtn.setTitleColor(ZColor.buttonColor, for: .normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc public func showLoaderMsg(inView: UIView? = nil) {
        hideAfter = 100
        let activityView = UIActivityIndicatorView(style: .white)
        activityView.hidesWhenStopped = true
        activityView.startAnimating()
        
       
        
        addSubview(msgLabel)

        addSubview(activityView)
        activityView.snp.makeConstraints { (make) in
            make.left.equalTo(cellValueLabelX)
            make.centerY.equalTo(msgLabel.snp.centerY)
            make.width.height.equalTo(30)
        }
        
    
        msgLabel.snp.makeConstraints { (make) in
            make.left.equalTo(activityView.snp.right).offset(16)
            make.top.bottom.equalToSuperview().inset(topPadding)
            make.right.equalToSuperview().inset(cellValueLabelX)
        }
        
       
        
        backgroundColor = ZColor.bgColorDark
        
        show(inView: inView)
    }
    
    @objc public func showErrorMsg(inView: UIView? = nil , tapToDismiss : Bool = true) {
        
        addSubview(msgLabel)
        
        let actionRightWidth = isActionNeeded  ? 120 : cellValueLabelX
       
        msgLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cellValueLabelX)
            make.top.bottom.equalToSuperview().inset(topPadding)
            make.right.equalToSuperview().inset(actionRightWidth)
        }
        backgroundColor = UIColor(red:0.79, green:0.35, blue:0.33, alpha:1.00)
        
        actionBtn.contentHorizontalAlignment = .right
        addSubview(actionBtn)
        actionBtn.snp.makeConstraints { (make) in
            make.width.equalTo(actionRightWidth)
            make.top.bottom.equalTo(msgLabel)
            make.right.equalToSuperview().inset(cellValueLabelX)
        }
        
        show(inView: inView)
        
        if tapToDismiss {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissToast))
            self.addGestureRecognizer(tapGesture)
        }
    }
    
    @objc public func showSuccessMsg(inView: UIView? = nil , tapToDismiss : Bool = true) {
        
        addSubview(msgLabel)
        let actionBtnSize = actionBtn.titleLabel!.text?.textSize(withConstrainedSize: CGSize(width: 120, height: frame.height), font: actionBtn.titleLabel?.font ?? ZSFont.bodyFont())
        
        let actionRightWidth = isActionNeeded ?  ((actionBtnSize?.width ?? 0) + 40) :  cellValueLabelX
        
//        let actionRightWidth = isActionNeeded  ? GlobalFunctions.size(forText: actionBtn.titleLabel!.text!, sizeWith: actionBtn.titleLabel?.font, constrainedTo: CGSize(width: 120, height: frame.height) ).width + 40 : cellValueLabelX
       
        msgLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cellValueLabelX)
            make.top.bottom.equalToSuperview().inset(topPadding)
            make.right.equalToSuperview().inset(actionRightWidth)
        }
        backgroundColor = ZColor.bgColorDark
        
        actionBtn.contentHorizontalAlignment = .right
        addSubview(actionBtn)
        actionBtn.snp.makeConstraints { (make) in
            make.width.equalTo(actionRightWidth)
            make.top.bottom.equalTo(msgLabel)
            make.right.equalToSuperview().inset(cellValueLabelX)
        }
        
        show(inView: inView)
        
        if tapToDismiss {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissToast))
            self.addGestureRecognizer(tapGesture)
        }
    }
    
    
    
    
    @objc public func showNotification(msg: String, subtitle :  String, inView: UIView? = nil) {
        
        isActionNeeded = true
        addSubview(titleLabel)
        addSubview(msgLabel)
        
        let actionBtnSize = "sign.mobile.template.viewer".ZSLString.textSize(withConstrainedSize: CGSize(width: 120, height: frame.height), font: actionBtn.titleLabel?.font ?? ZSFont.bodyFont())
               
        let actionRightWidth = isActionNeeded ?  (actionBtnSize.width + 40) :  cellValueLabelX
        
//        let actionRightWidth = isActionNeeded  ? GlobalFunctions.size(forText: "sign.mobile.template.viewer".ZSLString, sizeWith: actionBtn.titleLabel?.font, constrainedTo: CGSize(width: 120, height: frame.height)).width + 40 : cellValueLabelX
        
        
        titleLabel.text = msg
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cellValueLabelX)
            make.top.equalTo(topPadding)
            make.right.equalToSuperview().inset(actionRightWidth)
            make.height.equalTo(20)
        }
        titleLabel.sizeToFit()
        msgLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cellValueLabelX)
            make.top.equalToSuperview().inset(38)
            make.bottom.equalToSuperview().inset(topPadding)
            make.right.equalToSuperview().inset(actionRightWidth)
        }
        msgLabel.textColor = ZColor.whiteColor.withAlphaComponent(0.8)
        msgLabel.text = subtitle
        backgroundColor = ZColor.bgColorDark
        actionBtn.addTarget(self, action: #selector(notificationDidTapped), for: .touchUpInside)
        actionBtn.contentHorizontalAlignment = .right
        addSubview(actionBtn)
        actionBtn.snp.makeConstraints { (make) in
            make.width.equalTo(actionRightWidth)
            make.top.bottom.equalToSuperview()
            make.right.equalToSuperview().inset(cellValueLabelX)
        }
        actionBtn.setTitle("sign.mobile.template.viewer".ZSLString, for: .normal)
        showNotification(inView: inView)

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(notificationDidTapped))
        self.addGestureRecognizer(tapGesture)
        
        let panGesture = UISwipeGestureRecognizer(target: self, action:  #selector(swipeGestureHandling(_:)))
        panGesture.direction  = .up
        self.addGestureRecognizer(panGesture)
    }
    
    func showNotification(inView: UIView? = nil)  {
        var superView = inView
        if inView == nil {
            superView = UIApplication.shared.keyWindow
        }
        guard let view = superView else {
            return
        }
        
        let labelWidth  =  (view.frame.width - 60)
        let size = msgLabel.text?.textSize(withConstrainedSize: CGSize(width: labelWidth, height: 1000), font: ZSFont.regularFontLarge()) ?? CGSize(width: 1, height: 1)
        
        layer.cornerRadius = 5
        view.addSubview(self)
        viewHeight = size.height + 57
        
        self.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.width.equalToSuperview().inset(60).priority(.high)
            make.top.equalTo(-viewHeight)
            make.height.equalTo(viewHeight)
        }
        view.layoutIfNeeded()
        
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.snp.updateConstraints { (make) in
                make.top.equalTo(20)
            }
            view.layoutIfNeeded()
        }) { (finish) in
            
        }
        
        myTimer = Timer.scheduledTimer(withTimeInterval: 50, repeats: false) {[weak self] (timer) in
            self?.dismissNotificationToast()
        }
    }
    
    func show(inView: UIView? = nil)  {
        var superView = inView
        if inView == nil {
            superView = UIApplication.shared.keyWindow
        }
        guard let view = superView else {
            return
        }
        let size = msgLabel.text?.textSize(withConstrainedSize: CGSize(width: view.frame.width - 40, height: 1000), font: ZSFont.regularFontLarge()) ?? CGSize(width: 1, height: 1)
        
        view.addSubview(self)
        viewHeight = size.height + topPadding + topPadding + view.safeAreaInsets.bottom
        
        self.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(viewHeight)
            make.height.equalTo(viewHeight)
        }
        msgLabel.snp.updateConstraints { (make) in
            make.bottom.equalToSuperview().inset(topPadding + view.safeAreaInsets.bottom)
        }
        view.layoutIfNeeded()
        
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.snp.updateConstraints { (make) in
                make.bottom.equalToSuperview()
            }
            view.layoutIfNeeded()
        }) { (finish) in
            
        }
        
        myTimer = Timer.scheduledTimer(withTimeInterval: hideAfter, repeats: false) {[weak self] (timer) in
            self?.dismissToast()
        }
    }
    
    
    //MARK: - Dismiss
    @objc public func dismissToast(){
        myTimer?.invalidate()
        if self.superview == nil {return}
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.snp.updateConstraints { (make) in
                make.bottom.equalToSuperview().offset(self.viewHeight)
            }
            self.superview?.layoutIfNeeded()
        }, completion: { (finifh) in
            self.removeFromSuperview()
        })
    }
    
    @objc public func dismissNotificationToast(){
        myTimer?.invalidate()
        if self.superview == nil {return}
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.snp.updateConstraints { (make) in
                make.top.equalTo(-self.viewHeight)
            }
            self.superview?.layoutIfNeeded()
        }, completion: { (finifh) in
            self.removeFromSuperview()
        })
    }
    
    //MARK: - Gestures
    @objc func notificationDidTapped()  {
        dismissNotificationToast()
        tapHandler?()
    }
    
    @objc func swipeGestureHandling(_ gesture : UISwipeGestureRecognizer ) {
        dismissNotificationToast()
    }

    
}
