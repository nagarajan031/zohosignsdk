//
//  ZSSigningSuccessView.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 08/01/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit
let sharedSuccessMsgView = ZSSuccessMsgView()

public enum SuccessViewType: Int {
    case SelfSigning, RemoteSigning,InpersonSigning, SampleSigning, ExtnSigning, ApproveRequest;
}

public class ZSSuccessMsgView: UIView {

    public var didActionButtonPressed   : (() -> Void)?

    public let actionButton = ZSButton()
    let successMsgLabel =  UILabel()
    var successMessage : String = ""
    var topConstraint = NSLayoutConstraint()
    var bottomConstraint = NSLayoutConstraint()
    
    var titleLableHeightConstraint = NSLayoutConstraint()
    var descLabelHeightConstraint = NSLayoutConstraint()

    var successType : SuccessViewType = .SelfSigning
    public var viewHeight  = 130
    var descTextHeight  = 0

    let actionBtnWidth : CGFloat = 120
    let actionBtnHeight : CGFloat = 42

    public var documentName  = ""
    public var inPersonSignerName  = ""
   public var hostname  = ""
    
    // MARK: - Private
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width:0, height: viewHeight))
        initateSetup()
        return;
    }
   
    required public init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented"); }
    
    // the sharedInstance class method can be reached from ObjC. (From OP's answer.)
    public class func sharedInstance() -> ZSSuccessMsgView {
        return sharedSuccessMsgView
    }
    
    
    
    func signingSuccessSubDetailMsg() -> NSAttributedString {
        let string = NSMutableAttributedString(string: successMessage, attributes:[NSAttributedString.Key.font: ZSFont.bodyFont()])
        
        let minWidth = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
        
        let constraintRect = CGSize(width: minWidth - 46 - actionBtnWidth, height: .greatestFiniteMagnitude)
        let descStrSize = string.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        descTextHeight = Int(descStrSize.height) + 3
        if descTextHeight < Int(actionBtnHeight) {
            descTextHeight = Int(actionBtnHeight);
        }
        return string
    }
    
    func initateSetup()  {
        
        
        self.autoresizingMask      =   [.flexibleWidth, .flexibleHeight]
        self.backgroundColor       =   ZColor.isNightModeOn ? ZColor.bgColorDark : ZColor.buttonColor
        
        
        successMsgLabel.textColor  =   ZColor.whiteColor
        successMsgLabel.attributedText = signingSuccessSubDetailMsg()
        successMsgLabel.textAlignment = .left
        successMsgLabel.numberOfLines  =   0
//        successMsgLabel.font       =   ZSFont.regularFontLarge()
//        successMsgLabel.backgroundColor = ZColor.redColor
        self.addSubview(successMsgLabel)
        
   
        let parentItem : Any = self.safeAreaLayoutGuide
        
        
//        self.layer.shadowColor    =       UIColor.black.cgColor
//        self.layer.shadowOffset   =       CGSize(width: 0, height: -1);
//        self.layer.shadowRadius   =       1
//        self.layer.shadowOpacity  =       0.1
//        self.layer.shadowPath     =       UIBezierPath(rect: self.bounds).cgPath
        
        successMsgLabel.translatesAutoresizingMaskIntoConstraints = false
        let leading = NSLayoutConstraint(item:  successMsgLabel, attribute: .leading, relatedBy: .equal, toItem: parentItem, attribute: .leading, multiplier: 1, constant: 16)
        
        let trailing = NSLayoutConstraint(item:  successMsgLabel, attribute: .trailing, relatedBy: .equal, toItem: parentItem, attribute: .trailing, multiplier: 1, constant: CGFloat(-actionBtnWidth - 30))
        
        let top = NSLayoutConstraint(item:  successMsgLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 14)
        
        descLabelHeightConstraint = NSLayoutConstraint(item:  successMsgLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: CGFloat(descTextHeight))
        
        self.addConstraints([leading, trailing, top, descLabelHeightConstraint])
        
       
        actionButton.titleLabel?.font =   ZSFont.buttonFont()
//        actionButton.setTitleColor(UIColor(red:0.67, green:0.76, blue:0.95, alpha:1.00), for: .normal)
        actionButton.setTitleColor(ZColor.buttonColor, for: .normal)
        actionButton.layer.cornerRadius   =   3
//        actionButton.layer.borderColor = UIColor(white: 1, alpha: 0.8).cgColor
//        actionButton.layer.borderWidth  = 1
        actionButton.contentHorizontalAlignment = .right
        actionButton.backgroundColor = ZColor.whiteColor
        actionButton.addTarget(self, action: #selector(actionButtonPressed), for: .touchUpInside)
        actionButton.semanticContentAttribute = .forceLeftToRight
        actionButton.imageEdgeInsets = UIEdgeInsets.init(top: 7.5, left: 0, bottom: 7.5, right: 2)
        actionButton.titleEdgeInsets  =   UIEdgeInsets.init(top: 0, left: 2, bottom: 0, right: 0)
        actionButton.imageView?.contentMode = .scaleAspectFit
        actionButton.contentHorizontalAlignment = .center
        actionButton.titleLabel?.adjustsFontSizeToFitWidth = true
        self.addSubview(actionButton)

        actionButton.translatesAutoresizingMaskIntoConstraints = false
        let trailing1 = NSLayoutConstraint(item:  actionButton, attribute: .trailing, relatedBy: .equal, toItem: parentItem, attribute: .trailing, multiplier: 1, constant: -16)
        
        let widthConstr = NSLayoutConstraint(item:  actionButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: CGFloat(actionBtnWidth))
        
        let heightConstr = NSLayoutConstraint(item:  actionButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: actionBtnHeight)
        let btnCenterY = NSLayoutConstraint(item:  actionButton, attribute: .centerY, relatedBy: .equal, toItem: successMsgLabel, attribute: .centerY, multiplier: 1, constant:0)
        self.addConstraints([trailing1, widthConstr, heightConstr,btnCenterY])
        
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        descLabelHeightConstraint.constant = CGFloat(descTextHeight);
        self.updateConstraints()
        self.layoutIfNeeded()
    }
    
     public func showSuccessMsg(inView: UIView, type : SuccessViewType) {
        if (self.superview != nil) {self.removeFromSuperview()}
        SwiftUtils.generateNotificationFeedback(for: .success)
        successType = type
        let parentItem : Any = inView.safeAreaLayoutGuide
        
        inView.addSubview(self)
        self.translatesAutoresizingMaskIntoConstraints = false

        switch successType {
        case .SampleSigning:
            successMessage = "sign.mobile.account.signupToProceed".ZSLString
            actionButton.setTitle("sign.mobile.common.signup".ZSLString, for: .normal)
            break
        case .RemoteSigning,.SelfSigning:
            successMessage = "\"\(documentName)\" " +  "sign.mobile.request.signedSuccessfully".ZSLString.lowercased()
            actionButton.setTitle("sign.mobile.common.share".ZSLString, for: .normal)
            break
        case .InpersonSigning:
            successMessage =   String(format: "sign.mobile.inperson.passcontrolMessage".ZSLString, inPersonSignerName,hostname)
            actionButton.setTitle("sign.mobile.inperson.passControl".ZSLString, for: .normal)
            break
        case .ExtnSigning:
            successMessage = "sign.mobile.tooltip.extnSuccessmsg".ZSLString
            actionButton.setTitle("sign.mobile.common.share".ZSLString, for: .normal)
            break
            
      
        case .ApproveRequest:
            successMessage = "\"\(documentName)\" " +  "sign.mobile.request.approvedSuccessfully".ZSLString.lowercased()
            actionButton.setTitle("sign.mobile.common.share".ZSLString, for: .normal)
            break
        }
        

        successMsgLabel.attributedText = signingSuccessSubDetailMsg()
        viewHeight = 28 + descTextHeight
        descLabelHeightConstraint.constant = CGFloat(descTextHeight);
        
        let leading = NSLayoutConstraint(item:  self, attribute: .leading, relatedBy: .equal, toItem: inView, attribute: .leading, multiplier: 1, constant: 0)
        
        let trailing = NSLayoutConstraint(item:  self, attribute: .trailing, relatedBy: .equal, toItem: inView, attribute: .trailing, multiplier: 1, constant: 0)
        
        bottomConstraint = NSLayoutConstraint(item:  self, attribute: .bottom, relatedBy: .equal, toItem: inView, attribute: .bottom, multiplier: 1, constant: CGFloat(viewHeight))
        
        topConstraint = NSLayoutConstraint(item:  self, attribute: .top, relatedBy: .equal, toItem: parentItem, attribute: .bottom, multiplier: 1, constant: 0)
        
        inView.addConstraints([leading, trailing, bottomConstraint, topConstraint])
        
       
        self.updateConstraints()
        self.layoutIfNeeded()

        
        inView.updateConstraints()
        inView.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.topConstraint.constant         =    CGFloat(-self.viewHeight)
            self.bottomConstraint.constant      =   0
            inView.updateConstraints()
            inView.layoutIfNeeded()
            self.layer.shadowPath     =       UIBezierPath(rect: self.bounds).cgPath

        }, completion: nil)
    }
    
    @objc func actionButtonPressed()  {
        didActionButtonPressed?()
    }

    @objc public func dismissView(animated: Bool)
    {
        if self.superview == nil {return;}
        
        UIView.animate(withDuration: (animated ?  0.4 : 0), delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.topConstraint.constant         =    CGFloat(30)
            self.bottomConstraint.constant      =   CGFloat(self.viewHeight)
            self.superview?.updateConstraints()
            self.superview?.layoutIfNeeded()
            self.layer.shadowPath     =       UIBezierPath(rect: self.bounds).cgPath
            
        }, completion: { (finish) in
            self.removeFromSuperview()
        })
    }
}
