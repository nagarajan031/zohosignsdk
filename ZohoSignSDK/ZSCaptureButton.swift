//
//  ZSCaptureButton.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 25/09/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit
let outerWhiteCircleSize: CGFloat = 70
let outterBlackCircleSize: CGFloat = outerWhiteCircleSize - 10
let innerWhiteCircleSize: CGFloat = outterBlackCircleSize - 5


class ZSCaptureButton: UIView {
    
    private lazy var outerWhiteCircleView: UIView = {
       let view = UIView()
        view.backgroundColor = ZColor.whiteColor
        view.layer.cornerRadius = outerWhiteCircleSize/2
        return view
    }()
    
    private lazy var outerBlackCircleView: UIView = {
       let view = UIView()
        view.backgroundColor = ZColor.blackColor
        view.layer.cornerRadius = outterBlackCircleSize/2
        return view
    }()
    
    private lazy var innerWhiteCircleView: UIView = {
       let view = UIView()
        view.backgroundColor = ZColor.whiteColor
        view.layer.cornerRadius = innerWhiteCircleSize/2
        return view
    }()
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    func setupView(){
        addSubview(outerWhiteCircleView)
        outerWhiteCircleView.addSubview(outerBlackCircleView)
        outerBlackCircleView.addSubview(innerWhiteCircleView)
        
        
        outerWhiteCircleView.snp.makeConstraints { (make) in
            make.size.equalTo(outerWhiteCircleSize)
            make.center.equalToSuperview()
        }
        
        outerBlackCircleView.snp.makeConstraints { (make) in
            make.size.equalTo(outterBlackCircleSize)
            make.center.equalToSuperview()
        }
        
        innerWhiteCircleView.snp.makeConstraints { (make) in
            make.size.equalTo(innerWhiteCircleSize)
            make.center.equalToSuperview()
        }
        
        let longpressGestureRecogniser = UILongPressGestureRecognizer(target: self, action: #selector(longPressGesture(gesture:)))
        longpressGestureRecogniser.minimumPressDuration = 0
        innerWhiteCircleView.addGestureRecognizer(longpressGestureRecogniser)
    }
    
    @objc private func longPressGesture(gesture: UILongPressGestureRecognizer){
        switch gesture.state {
        case .began, .possible:
            UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: [.curveEaseIn], animations: {
                self.innerWhiteCircleView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            }, completion: nil)
        case .ended , .cancelled, .failed:
            UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: [.curveEaseIn], animations: {
                self.innerWhiteCircleView.transform = .identity
            }, completion: nil)
        case .changed:
            ()
        @unknown default:
            ()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
