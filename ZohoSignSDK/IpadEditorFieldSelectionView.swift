//
//  IpadEditorFieldSelectionView.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 28/09/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

class ZSFieldSelectionReceipientCell: UITableViewCell {
   
    let imgView = ZSContactImageView()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    deinit {
        ZInfo("deinit")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        if selected{
            imgView.borderColor = ZColor.buttonColor
            imgView.fillColor = ZColor.bgColorWhite
            imgView.textColor = ZColor.buttonColor
        }else{
            imgView.borderColor = ZColor.seperatorColor
            imgView.fillColor = ZColor.seperatorColor
            imgView.textColor = ZColor.secondaryTextColorDark
        }
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textLabel?.frame = CGRect(x: 36, y: textLabel!.frame.origin.y, width: textLabel!.frame.width, height: textLabel!.frame.height)
        detailTextLabel?.frame = CGRect(x: 36, y: detailTextLabel!.frame.origin.y, width: detailTextLabel!.frame.width, height: detailTextLabel!.frame.height)
        imgView.frame   =   CGRect(x: 10, y: 12, width: 16, height: 16)//CGRect(x: 10, y: 12 , width: 20, height: 20)
        

    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        imgView.frame   =   CGRect(x: 10, y: 12, width: 16, height: 16)
        addSubview(imgView)
        
        backgroundColor = ZColor.bgColorGray
        
        let selectedView = UIView()
        selectedView.layer.cornerRadius = 6
        selectedView.layer.borderColor = ZColor.buttonColor.cgColor
        selectedView.layer.borderWidth = 1
        selectedView.backgroundColor = ZColor.bgColorWhite;
        selectedBackgroundView = selectedView;
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        //fatalError("init(coder:) has not been implemented")
    }
    
    func setRecipient(_ recipient : ZSRecipient, isSigningOrderOn : Bool)  {
//        var emailAdded = false
        var recipientName  = recipient.userRole
        if ((recipientName == nil) || (recipientName == "")){
            recipientName = recipient.name
        }
        if ((recipientName == nil) || (recipientName == "")){
            recipientName = recipient.email
//            emailAdded = true
        }
        
        imgView.fillColor = ZColor.seperatorColor
        imgView.textFont = ZSFont.mediumFont(withSize: 9)
        if isSigningOrderOn {
//            imgView.text  =  (recipient.signingOrder <= 0) ? "1" : String(recipient.signingOrder + 1)
            imgView.text  =  (recipient.signingOrder <= 0) ? "1" : String(recipient.signingOrder)
        }else{
            
            if let str = recipientName?.first {
                imgView.text  = String(str).uppercased()
            }else{
                imgView.text  = ""
            }
//            imgView.backgroundImage = UIImage(named: "icn_user_add_me")
        }
        
        imgView.textColor = ZColor.secondaryTextColorDark

        textLabel?.text            =    recipientName?.capitalizedFirst()
        textLabel?.font            =    ZSFont.listTitleFont()
        detailTextLabel?.font      =    ZSFont.listSubTitleFont()
        
        textLabel?.textColor    =       ZColor.primaryTextColor
        detailTextLabel?.textColor =    ZColor.secondaryTextColorDark
        textLabel?.alpha           =   1
        detailTextLabel?.alpha     =    1
        
        if recipient.actionType == ZS_ActionType_Sign{
            
            detailTextLabel?.text   =   "sign.mobile.recipient.signer".ZSLString
        }
        else if recipient.actionType == ZS_ActionType_View{
            detailTextLabel?.text   =   "sign.mobile.recipient.getsACopy".ZSLString
        }
        else if recipient.actionType == ZS_ActionType_Approver{
            detailTextLabel?.text   =   "sign.mobile.recipient.approver".ZSLString
        }
        else if recipient.actionType == ZS_ActionType_InPerson {
            //           let signerType = "sign.mobile.common.inpersonSigner".ZSLString
            detailTextLabel?.text   =   "sign.mobile.common.inpersonSigner".ZSLString
            var inpersonName  =  recipient.userRole
            if ((inpersonName == nil) || (inpersonName == "")){
                inpersonName = recipient.inPersonName
            }
            if ((inpersonName == nil) || (inpersonName == "")){
                inpersonName = recipient.inPersonEmail
            }
            if ((inpersonName == nil) || (inpersonName == "")){
                inpersonName = String(format:"%@: %@","sign.mobile.recipient.host".ZSLString,recipient.name ?? "")
            }
            if ((inpersonName == nil) || (inpersonName == String(format:"%@: ","sign.mobile.recipient.host".ZSLString))){
                inpersonName = String(format:"%@: %@","sign.mobile.recipient.host".ZSLString,recipient.email ?? "")
            }
            textLabel?.text          =    inpersonName
        }
        
        

    }
    
}


protocol IpadEditorFieldSelectionViewDelegate : class
{
    func fieldSelectionViewRecipientDidChange(_ recep : ZSRecipient)
    func fieldSelectionViewFieldDidTapped(_ fieldType : ZSignFieldType)
}

public class IpadEditorFieldSelectionView: UIView {
    
    let tableView = UITableView(frame: CGRect.zero, style: .plain)
    var menuItemArray    =  [SignFieldMenuItem]()
    var receipientsArray    =  [ZSRecipient]()
    var isSignAndSend = false
    var disableFieldSelection = false
    var isSigningOrderOn = false

    let fieldListBgView = UIView()
    public var selectedFieldType  : ZSignFieldType = .others
    
    let attachementField = SignFieldMenuItem(title: "sign.mobile.field.attachment".ZSLString, icon: UIImage(named:"menu_attachment")!, signFieldType: .attachment)

//    public var selectionBlock : fieldDidSelectBlock?

    weak var delegate : IpadEditorFieldSelectionViewDelegate?

    let cellHeight = 50
    
    public func initateSetup() {
        self.backgroundColor  = ZColor.bgColorGray
     

        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = ZColor.bgColorGray
        tableView.rowHeight =   TableviewCellSize.receipientList
        tableView.setSeparatorStyle()
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 0)
        addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
//            make.edges.equalToSuperview()
            make.top.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        
        let signatureField = SignFieldMenuItem(title: "sign.mobile.field.signature".ZSLString, icon: UIImage(named:"menu_sign")!, signFieldType: .signature)
        let initialField = SignFieldMenuItem(title: "sign.mobile.field.initial".ZSLString, icon: UIImage(named:"menu_initial")!, signFieldType: .initial)
        let emailField = SignFieldMenuItem(title: "sign.mobile.field.email".ZSLString, icon: UIImage(named:"menu_email")!, signFieldType: .email)
        let nameField = SignFieldMenuItem(title: "sign.mobile.field.name".ZSLString, icon: UIImage(named:"menu_name")!, signFieldType: .name)
        let companyField = SignFieldMenuItem(title: "sign.mobile.field.company".ZSLString, icon: UIImage(named:"menu_companyname")!, signFieldType: .company)
        let signDateField = SignFieldMenuItem(title: "sign.mobile.field.signDate".ZSLString, icon: UIImage(named:"menu_date")!, signFieldType: .signDate)
        let jobField = SignFieldMenuItem(title: "sign.mobile.field.jobtitle".ZSLString, icon: UIImage(named:"menu_jobtitle")!, signFieldType: .jobTitle)
        let checkboxField = SignFieldMenuItem(title: "sign.mobile.field.checkbox".ZSLString, icon: UIImage(named:"menu_checkbox")!, signFieldType: .checkBox)
        let textField = SignFieldMenuItem(title: "sign.mobile.field.textfield".ZSLString, icon: UIImage(named:"menu_textfield")!, signFieldType: .textBox)
        let dateField = SignFieldMenuItem(title: "sign.mobile.field.date".ZSLString, icon: UIImage(named:"menu_date")!, signFieldType: .date)
        let dropDownField = SignFieldMenuItem(title: "sign.mobile.field.dropDown".ZSLString, icon: UIImage(named:"menu_dropdown")!, signFieldType: .dropDown)
        let radioField = SignFieldMenuItem(title: "sign.mobile.field.radio".ZSLString, icon: UIImage(named:"menu_checkbox")!, signFieldType: .radioGroup)

        menuItemArray.append(contentsOf: [signatureField,initialField,emailField,nameField,signDateField,companyField,jobField,checkboxField,textField,dateField,dropDownField,radioField])
        if isSignAndSend {
            menuItemArray.removeLast()//datefield
            menuItemArray.removeLast()//dropdownfield
            menuItemArray.removeLast()//radioField
        }
        if (UserManager.shared.currentUser?.isAttachmentFeatureAvailable ?? false) && !isSignAndSend{
            menuItemArray.append(attachementField)
        }
       
        let fieldHeight : CGFloat = 48//44
        let labelHeight : CGFloat = 40//44

        
        fieldListBgView.clipsToBounds = true
        fieldListBgView.backgroundColor = ZColor.bgColorGray
        
        if !isSignAndSend {
            let headerLabel = UILabel()
            headerLabel.text = "sign.mobile.request.recipients".ZSLString.uppercased()
            headerLabel.font =   ZSFont.mediumFontLarge()
            headerLabel.textColor = ZColor.tableHeaderTextColor
            headerLabel.frame    =   CGRect(x: 0, y: 0 , width: fieldListBgView.frame.width, height: 56)
            headerLabel.autoresizingMask =   .flexibleWidth
            fieldListBgView.addSubview(headerLabel)
            
            let seperatorLayer1 = UIView(frame: CGRect(x: 0, y: 46, width: fieldListBgView.frame.width, height: 0.6 ))
            seperatorLayer1.autoresizingMask = .flexibleWidth
            seperatorLayer1.backgroundColor = ZColor.seperatorColor
            headerLabel.addSubview(seperatorLayer1)
            tableView.tableHeaderView = headerLabel
        }
        
        
        
        let signatureFieldsLabel = UILabel()
        signatureFieldsLabel.text = "sign.mobile.editor.signatureFields".ZSLString
        signatureFieldsLabel.font =   ZSFont.tableSectionHeaderFont()
        signatureFieldsLabel.textColor = ZColor.tableHeaderTextColor
        signatureFieldsLabel.frame    =   CGRect(x: 0, y: 18 , width: fieldListBgView.frame.width, height: labelHeight)
        signatureFieldsLabel.autoresizingMask =   .flexibleWidth
        fieldListBgView.addSubview(signatureFieldsLabel)
        var yPosition : CGFloat  = labelHeight + 18

        if !isSignAndSend {
            let fieldHeaderLabel = UILabel()
            fieldHeaderLabel.text = "sign.mobile.editor.fieldsList".ZSLString.uppercased()
            fieldHeaderLabel.font =   ZSFont.mediumFontLarge()
            fieldHeaderLabel.textColor = ZColor.tableHeaderTextColor
            fieldHeaderLabel.frame    =   CGRect(x: 0, y: 25 , width: fieldListBgView.frame.width, height: 30)
            fieldHeaderLabel.autoresizingMask =   .flexibleWidth
            fieldListBgView.addSubview(fieldHeaderLabel)
            
            let seperatorLayer = UIView(frame: CGRect(x: 0, y: 30, width: fieldListBgView.frame.width, height: 0.6 ))
            seperatorLayer.autoresizingMask = .flexibleWidth
            seperatorLayer.backgroundColor = ZColor.seperatorColor
            fieldHeaderLabel.addSubview(seperatorLayer)
            signatureFieldsLabel.frame    =   CGRect(x: 0, y: 60 , width: fieldListBgView.frame.width, height: labelHeight)
            yPosition  = labelHeight + 58
        }
        
        for i in 0..<2 {
            let field = menuItemArray[i]
            addFieldButton(i, field: field, yPos: yPosition)
            yPosition += fieldHeight
        }
        
        let userLabel = UILabel()
        userLabel.text = "sign.mobile.editor.signerInformation".ZSLString
        userLabel.font =   ZSFont.tableSectionHeaderFont()
        userLabel.textColor = ZColor.tableHeaderTextColor
        userLabel.clipsToBounds = false
        userLabel.frame    =   CGRect(x: 0, y: yPosition + 2, width: frame.width - 32, height: labelHeight)
        userLabel.adjustsFontSizeToFitWidth = true
        userLabel.autoresizingMask =   .flexibleWidth
        fieldListBgView.addSubview(userLabel)
        yPosition += labelHeight

        for i in 2..<7 {
            let field = menuItemArray[i]
            addFieldButton(i, field: field, yPos: yPosition)
            yPosition += fieldHeight
        }
        
        let additionalLabel = UILabel()
        additionalLabel.text = "sign.mobile.editor.additionalInformation".ZSLString
        additionalLabel.font =   ZSFont.tableSectionHeaderFont()
        additionalLabel.textColor = ZColor.tableHeaderTextColor
        additionalLabel.frame    =   CGRect(x: 0, y: yPosition + 2, width: frame.width - 32, height: labelHeight)
        additionalLabel.adjustsFontSizeToFitWidth = true
        additionalLabel.autoresizingMask =   .flexibleWidth
        fieldListBgView.addSubview(additionalLabel)
        yPosition += labelHeight

        for i in 7..<(menuItemArray.count) {
            let field = menuItemArray[i]
            addFieldButton(i, field: field, yPos: yPosition)
            yPosition += fieldHeight
        }
        
        fieldListBgView.frame  = CGRect(x: 0, y: 0, width: 230, height: yPosition + 30)
        fieldListBgView.backgroundColor = ZColor.bgColorGray
        
        if disableFieldSelection {
            showFieldDisableInfoView(msg: "sign.mobile.recipient.noPermissionToAddFields".ZSLString)
        }else{
            tableView.tableFooterView = fieldListBgView
        }
        
        if receipientsArray.count > 0 {
            tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
            tableView(tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        }
        

    }
    
    func addFieldButton(_ cellIndex : Int , field : SignFieldMenuItem, yPos : CGFloat)  {
        let button      =  UIButton()
        button.frame    =  CGRect(x: 0, y:  yPos, width: 230, height: 40)
        button.addTarget(self, action: #selector(fieldButtonTapped(_:)), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: 60, bottom: 0, right: 0)
        button.backgroundColor =   ZColor.bgColorWhite
        button.contentHorizontalAlignment = .left
        button.tag = (cellIndex )
        button.setTitleColor(ZColor.primaryTextColorLight, for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.titleLabel?.font = ZSFont.regularFont(withSize: 15)
        button.setTitle(field.title, for: .normal)
        button.addDashedBorder(color: ZColor.secondaryTextColorLight)
        
        button.layer.cornerRadius = 4
        button.clipsToBounds = true
        
        let iconView = UIImageView(frame: CGRect(x: 12, y: 0, width: 20, height: 40))
        button.addSubview(iconView)
        iconView.image = field.icon.withRenderingMode(.alwaysTemplate)
        iconView.tintColor = ZColor.primaryTextColor
        iconView.contentMode = .scaleAspectFit
        iconView.contentScaleFactor = 0.4
        fieldListBgView.addSubview(button)
        
        let horiLayer = UIView(frame: CGRect(x: 44, y: 6, width: 0.8, height: 28 ))
        horiLayer.backgroundColor = ZColor.seperatorColor
        button.addSubview(horiLayer)
    }
    
    func showFieldDisableInfoView(msg : String)  {
        let footerView =  UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: 200))
        

        let fieldHeaderLabel = UILabel()
        fieldHeaderLabel.text = "sign.mobile.editor.fieldsList".ZSLString.uppercased()
        fieldHeaderLabel.font =   ZSFont.mediumFontLarge()
        fieldHeaderLabel.textColor = ZColor.tableHeaderTextColor
        footerView.addSubview(fieldHeaderLabel)
        fieldHeaderLabel.snp.makeConstraints { (make) in
            make.left.width.equalToSuperview()
            make.top.equalTo(25)
            make.height.equalTo(30)
        }
        
        let seperatorLayer = UIView()
        seperatorLayer.backgroundColor = ZColor.seperatorColor
        fieldHeaderLabel.addSubview(seperatorLayer)
        
        seperatorLayer.snp.makeConstraints { (make) in
            make.left.width.equalToSuperview()
            make.top.equalTo(30)
            make.height.equalTo(0.6)
        }
        
        let msglabel = UILabel()
        msglabel.text = msg
        msglabel.numberOfLines = 0
        msglabel.font =   ZSFont.regularFontLarge()
        msglabel.textColor = ZColor.secondaryTextColorDark
//        msglabel.frame    =   CGRect(x: 0, y: 66, width: frame.width, height: 120)
//        msglabel.autoresizingMask =   .flexibleWidth
        msglabel.adjustsFontSizeToFitWidth = true
        footerView.addSubview(msglabel)
        
        msglabel.snp.makeConstraints { (make) in
            make.left.width.equalToSuperview()
            make.height.equalTo(150)
            make.top.equalTo(66)
        }
        
        perform({
            msglabel.sizeToFit()
        }, afterDelay: 0)
        tableView.tableFooterView = footerView
        

    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    @objc private func fieldButtonTapped(_ sender : UIButton){
        let menuItem = menuItemArray[sender.tag]
       delegate?.fieldSelectionViewFieldDidTapped(menuItem.signFieldType)
    }
    
    public func viewInTouchLocation(_ point : CGPoint) -> UIView? {
        

        var stackPoint  = tableView.convert(point, to: fieldListBgView)
        stackPoint.y = stackPoint.y + tableView.contentOffset.y

        guard  let view = fieldListBgView.hitTest(stackPoint, with: nil) as? UIButton else {
            return nil
        }

        SwiftUtils.generateImpactFeedBack()

        selectedFieldType = menuItemArray[view.tag].signFieldType
        let imgView  = UIImageView()
        imgView.image = view.asImage()
        imgView.frame = view.frame
        return imgView
    }

}

extension UIView {
    func addDashedBorder(color : UIColor) {
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        if #available(iOS 13.0, *) {
            traitCollection.performAsCurrent {
                shapeLayer.strokeColor = color.cgColor
            }
        } else {
            shapeLayer.strokeColor = color.cgColor
        }
        shapeLayer.lineWidth = 1
        shapeLayer.lineJoin = .round
        shapeLayer.lineDashPattern = [4,4]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 4).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
}

extension IpadEditorFieldSelectionView : UITableViewDelegate{
    
}

extension IpadEditorFieldSelectionView : UITableViewDataSource {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60;
    }

    public func numberOfSections(in tableView: UITableView) -> Int {
        return isSignAndSend ? 0 : 1
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 0) ?  receipientsArray.count : 0
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell"
        
        var cell: ZSFieldSelectionReceipientCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? ZSFieldSelectionReceipientCell
        
        let recipient =  receipientsArray[indexPath.row]
        
        if cell == nil {
            cell =  ZSFieldSelectionReceipientCell(style: .subtitle, reuseIdentifier: identifier)
        }
        
        cell.setRecipient(recipient, isSigningOrderOn: isSigningOrderOn)
        return cell
    }
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let recep = receipientsArray[indexPath.row]
        delegate?.fieldSelectionViewRecipientDidChange(recep)
        
        if disableFieldSelection {return;}
        
      
        updateFieldsListView(recep)
    }
    
    func updateFieldsListView(_ selectedRecep : ZSRecipient)  {
        if (selectedRecep.actionType == ZS_ActionType_View || selectedRecep.actionType == ZS_ActionType_Approver) {
            showFieldDisableInfoView(msg: "sign.mobile.recipient.userIsNotAnIntendedSigner".ZSLString)
        }else{
            tableView.tableFooterView = fieldListBgView
        }
        
        let _index =  menuItemArray.firstIndex { (item) -> Bool in
            return item.signFieldType == .attachment
        }
        
        guard let index = _index, let attachementFieldBtn = fieldListBgView.viewWithTag(index) as? UIButton else {
            return
        }
        
        attachementFieldBtn.isHidden = selectedRecep.isTemplateUser
      
    }
}
