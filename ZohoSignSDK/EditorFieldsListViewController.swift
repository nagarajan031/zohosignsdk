//
//  EditorFieldsListViewController.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 21/09/17.
//  Copyright © 2017 Zoho Corporation. All rights reserved.
//

import UIKit

public protocol EditorFieldsListViewControllerDelegate: NSObjectProtocol {
    func editorFieldsListDidSelectField(_ field : SignField)
    func editorFieldsListDidUpdatedList(_ fieldsList : [SignField])
    func editorFieldsListDidDeleteField(_ field : SignField)
}


public class EditorFieldsListViewController: UITableViewController {

    public var receipientsArray : [ZSRecipient] = []
    var fieldsListArray : [SignField] =  []
    public var pagesArray : [ZSPage] = []
    public var documentsArray : [Documents] = []

    let noDataLabel  = UILabel()
    var isMultipleRecipient  = false
    public var isSampleSigning   = false
    public var isDocumentCreate = false

    var isFieldUpdated : Bool = false
    
    public var editorType :  ZSDocumentCreatorType = .SIGN_AND_SEND
    private var isResetEditorFieldsInProgress = false {
        willSet {
            if #available(iOS 13, *){
                isModalInPresentation = newValue
            }
        }
    }
    private var resetAllBarButtonItem: UIBarButtonItem!
    private var editFieldsBarButtonItem: UIBarButtonItem!
    public weak var delegate: EditorFieldsListViewControllerDelegate?

    //MARK: - Life cycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        ZColor.adjustNavigationBarForTheme(navigationController?.navigationBar)
        self.title         =    "sign.mobile.editor.fieldsList".ZSLString
        
        let doneButton    =  ZSBarButtonItem.barButton(type: .done, target: self, action: #selector(closeHandle))
        self.navigationItem.rightBarButtonItem    =    doneButton
        
        documentsArray =  documentsArray.sorted { $0.documnetOrder < $1.documnetOrder }

        isMultipleRecipient = (receipientsArray.count > 1)
        self.tableView.rowHeight = (isMultipleRecipient) ? 90 :  TableviewCellSize.subtitle
        
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundColor = ZColor.isNightModeOn ? ZColor.bgColorWhite :  ZColor.bgColorGray
        self.tableView.setSeparatorStyle()
//        tableView.separatorStyle = .none

        var i = 0
        for recipient in receipientsArray {
            for field in recipient.draftFieldListArray
            {
                field.recipientName =   recipient.displayRecipientName()
                field.recipientIndex   =   i
                fieldsListArray.append(field)
            }
            i = (i + 1)
        }
        
        self.tableView.reloadData()
        if fieldsListArray.count == 0
        {
            showNoDataView()
        }
        self.preferredContentSize = CGSize(width: 375, height: ((self.tableView.contentSize.height > 0) ?  self.tableView.contentSize.height :  100))

        
        if !isDocumentCreate && SwiftUtils.isIphone {
            self.navigationController?.setToolbarHidden(false, animated: false)
            self.navigationController?.toolbar.barTintColor = ZColor.tabBarColor
            
            let isCompletedFieldsAvailable = self.isCompletedFieldsAvailable()
            resetAllBarButtonItem = UIBarButtonItem(title: "sign.mobile.editor.resetAllFields".ZSLString, style: .plain, target: self, action: #selector(resetAll))
            resetAllBarButtonItem.isEnabled = isCompletedFieldsAvailable
            editFieldsBarButtonItem =  UIBarButtonItem(title: "sign.mobile.common.edit".ZSLString, style: .plain, target: self, action: #selector(editFields))
            editFieldsBarButtonItem.isEnabled = isCompletedFieldsAvailable
            
            toolbarItems = [
                resetAllBarButtonItem,
                UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                editFieldsBarButtonItem
            ]
        }
    }
    
    private func isCompletedFieldsAvailable() -> Bool {
        return fieldsListArray.contains(where: { $0.isCompleted })
    }
    
    @objc private func resetAll() {
        resetAllBarButtonItem.isEnabled = false
        editFieldsBarButtonItem.isEnabled = false
        for var field in fieldsListArray {
            resetField(signField: &field)
        }
        isFieldUpdated = true
        
        tableView.reloadData()
    }
    
    @objc private func editFields() {
        if editFieldsBarButtonItem.title == "sign.mobile.common.done".ZSLString {
            editFieldsBarButtonItem.style = .plain
            editFieldsBarButtonItem.title = "sign.mobile.common.edit".ZSLString
            isResetEditorFieldsInProgress = false
            let isCompletedFieldsAvailable = self.isCompletedFieldsAvailable()
            navigationItem.rightBarButtonItem?.isEnabled = true
            resetAllBarButtonItem.isEnabled = isCompletedFieldsAvailable
            editFieldsBarButtonItem.isEnabled = isCompletedFieldsAvailable
            tableView.allowsSelection = true
        } else {
            editFieldsBarButtonItem.style = .done
            tableView.allowsSelection = false
            navigationItem.rightBarButtonItem?.isEnabled = false
            isResetEditorFieldsInProgress = true
            resetAllBarButtonItem.isEnabled = false
            editFieldsBarButtonItem.title = "sign.mobile.common.done".ZSLString
        }
        
        tableView.reloadData()
    }
    
    func showNoDataView()  {
        noDataLabel.removeFromSuperview()
        noDataLabel.frame   =   CGRect(x: 15, y: 0, width: self.view.frame.size.width - 30, height: 100)
        noDataLabel.textColor    =   ZColor.secondaryTextColorDark
        noDataLabel.font    =   ZSFont.regularFont(withSize: 16)
        noDataLabel.text    =   "sign.mobile.emptyScreen.noFieldsAdded".ZSLString;
        noDataLabel.textAlignment   =   .center
        noDataLabel.center  =   self.view.center
        self.view.addSubview(noDataLabel)
        
        noDataLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalToSuperview().offset(-30)
            make.height.equalTo(60)
        }

    }
    
//    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//
//        coordinator.animate(alongsideTransition: { (context) in
//            self.navigationController?.setNavigationBarHidden(!UIApplication.shared.isSplitOrSlideOver, animated: false)
//        }) { (completed) in
//        }
//    }
//
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.preferredContentSize = CGSize(width: 375, height: ((self.tableView.contentSize.height > 0) ?  self.tableView.contentSize.height :  100))

    }
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func closeHandle() {
        if isFieldUpdated {
            delegate?.editorFieldsListDidUpdatedList(fieldsListArray)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    // MARK: - Table view data source

    override public func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
//        return pagesArray.count
        return (isSampleSigning) ? 1 : documentsArray.count
    }
    
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (fieldsListArray.count == 0) && !isSampleSigning{
         return 0
        }
        let doc  = documentsArray[section]
        let filteredArray = fieldsListArray.filter() {($0.docId == doc.documentId) }
        if filteredArray.count == 0{
            return 0
        }

        return tablePlainHeaderHeight
    }

    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var titleStr = ""


        if (fieldsListArray.count == 0) && !isSampleSigning{
            return nil
        }
        let doc  = documentsArray[section]
        let filteredArray = fieldsListArray.filter() {($0.docId == doc.documentId) }
        if filteredArray.count == 0{
            return nil
        }

        if isSampleSigning {
            titleStr = "Non-Disclosure Agreement"
        }
        else{
            titleStr = documentsArray[section].documentName ?? ""
        }

        let headerView  =  ZTableHeaderView(style: .plain)
        headerView .setTitle(titleStr)
//        if ((section == 2) && !isInPersonSigner) {
//            headerView.setTitleContentOffset(CGPoint(x: cellValueLabelX, y: -20))
//        }
//        headerView.topBorder.isHidden = true
//        headerView.bottomBorder.isHidden = true
        return headerView
    }
   
//    public override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//
//
//
//        if isSampleSigning {
//            return "Non-Disclosure Agreement"
//        }
//        if fieldsListArray.count == 0 {
//            return nil
//        }
//        let doc  = documentsArray[section]
//        let filteredArray = fieldsListArray.filter() {($0.docId == doc.documentId) }
//        return (filteredArray.count == 0) ? nil : documentsArray[section].documentName
//    }

 
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows

        if(isSampleSigning) { return fieldsListArray.count }
        
        let doc  = documentsArray[section]
        let filteredArray = fieldsListArray.filter() {($0.docId == doc.documentId) }
        return filteredArray.count

    }

    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : EditorFieldsListCell? = tableView.dequeueReusableCell(withIdentifier: "Cell") as? EditorFieldsListCell
        if (cell == nil) {
            cell = EditorFieldsListCell(style: .subtitle, reuseIdentifier: "Cell")
            
            
            let selectedView = UIView()
            selectedView.backgroundColor = ZColor.listSelectionColor
            cell?.selectedBackgroundView = selectedView
        }
        
        cell?.backgroundColor = ZColor.bgColorWhite
        cell?.editorType = editorType
//        let page  = pagesArray[indexPath.section]
//        let filteredArray = fieldsListArray.filter() { ($0.pageIndex == (page.pageNumber - 1)) && ($0.docId == page.docId) }

        if(isSampleSigning) {
            let field : SignField = fieldsListArray[indexPath.row]
            cell?.setSignField(field)
            return cell!
        }

        
        let doc  = documentsArray[indexPath.section]
        let filteredArray = fieldsListArray.filter() {($0.docId == doc.documentId) }
        
        let field : SignField = filteredArray[indexPath.row]
        cell?.didShowRecipientName = isMultipleRecipient
        cell?.setSignField(field)
        cell?.updateCell(shouldReset: isResetEditorFieldsInProgress, field: field)
        cell?.resetField = {[weak self] in
            cell?.contentView.alpha = 0.4
            cell?.isUserInteractionEnabled = false
            if let index = self?.fieldsListArray.firstIndex(of: field), field.isMandatory {
                var signField = field
                self?.resetField(signField: &signField)
                self?.fieldsListArray[index] = signField
            }
        }
        return cell!
    }
    
    func resetField(signField: inout SignField) {
        if signField.type == .attachment {
            return
        }
        isFieldUpdated = true
        
        let field = signField

        signField.isCompleted = false
        signField.signImage  = nil
        signField.dateValue =   nil
        signField.isCheckboxTicked = false
        
        if field.type == .checkBox{
            signField.isCheckboxTicked  = field.textValue?.covertToBool() ?? false
            signField.isCompleted = (field.isReadOnly || !field.isMandatory)
        }
        else if signField.isReadOnly{
            signField.isCompleted = true
        }else{
            signField.textValue = ""
        }
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

//        let filteredArray = fieldsListArray.filter() { ($0.pageIndex == (page.pageNumber - 1)) && ($0.docId == page.docId) }
        if(isSampleSigning) {
            let field : SignField = fieldsListArray[indexPath.row]
//            fieldSelectionBlock?(field)
            delegate?.editorFieldsListDidSelectField(field)
        }
        else{
            let doc  = documentsArray[indexPath.section]
            let filteredArray = fieldsListArray.filter() {($0.docId == doc.documentId) }
            
            let field : SignField = filteredArray[indexPath.row]
//            fieldSelectionBlock?(field)
            delegate?.editorFieldsListDidSelectField(field)
        }
        
        self.dismiss(animated: true, completion: nil)
    }

    
    public override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if isResetEditorFieldsInProgress {return UISwipeActionsConfiguration(actions: [])}

        var field : SignField?
        
        if(isSampleSigning) {
            field  = fieldsListArray[indexPath.row]
        }
        else{
            let doc  = documentsArray[indexPath.section]
            let filteredArray = fieldsListArray.filter() {($0.docId == doc.documentId) }
            
            field  = filteredArray[indexPath.row]
        }
        
        guard var signField  = field else {
            return UISwipeActionsConfiguration(actions: [])
        }
        
        if ((!signField.isCompleted || signField.isReadOnly || (signField.type == .attachment)) && !isDocumentCreate) {
            return UISwipeActionsConfiguration(actions: [])
        }
              
        
        weak var weakSelf = self
        
        let deleteAction = UIContextualAction(style: .destructive, title: isDocumentCreate ? "sign.mobile.common.delete".ZSLString : "sign.mobile.common.reset".ZSLString,
                                              handler: { (action, view, completionHandler) in
                                                weakSelf?.resetButtonTapHandler(indexPath: indexPath, signField: &signField)
                                                
                                                completionHandler(true)
        })
        let swipeAction = UISwipeActionsConfiguration(actions: [deleteAction])
//        swipeAction.performsFirstActionWithFullSwipe = false
        return swipeAction
    }
    
    
    func resetButtonTapHandler(indexPath : IndexPath, signField :inout SignField)  {
        
        
        if isDocumentCreate ?? false{
            fieldsListArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            delegate?.editorFieldsListDidDeleteField(signField)
            
            if fieldsListArray.count == 0 {
                showNoDataView()
            }
        }else{
            tableView.reloadRows(at: [indexPath], with: .automatic)
            resetField(signField: &signField)
            fieldsListArray[indexPath.row] = signField
        }
    }
}



#if targetEnvironment(macCatalyst)
@available(iOS 13, *)
extension EditorFieldsListViewController {
    public override func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        
        var field : SignField?
        
        if(isSampleSigning) {
            field  = fieldsListArray[indexPath.row]
        }
        else{
            let doc  = documentsArray[indexPath.section]
            let filteredArray = fieldsListArray.filter() {($0.docId == doc.documentId) }
            
            field  = filteredArray[indexPath.row]
        }
        
        guard var signField  = field else {
            return nil
        }
        
        if ((!signField.isCompleted || signField.isReadOnly || (signField.type == .attachment)) && !isDocumentCreate) {
            return nil
        }
              
        
        weak var weakSelf = self
        
        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { (menuElement) -> UIMenu? in
            let deleteAction = UIAction(title: self.isDocumentCreate ? "sign.mobile.common.delete".ZSLString : "sign.mobile.common.reset".ZSLString, image: nil ,identifier: nil){ action in
                
                weakSelf?.resetButtonTapHandler(indexPath: indexPath, signField: &signField)
            }
            
            return UIMenu(__title: "", image: nil, identifier: nil, children: [deleteAction])
        }
        
    }
    
}

#endif
