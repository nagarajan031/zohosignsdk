//
//  ZSDrawSignatureController.swift
//  ZSDrawSignature
//
//  Created by Nagarajan S on 19/04/17.
//  Copyright © 2017 Zoho Corp. All rights reserved.
//

import UIKit

public class ZSDrawSignatureController: UIViewController {
    deinit {
        ZInfo("draw page deinit")
    }
    public var delegate : SignatureCreatorDelegate?
    //public let signatureView = ZNSketchView();
    public let signatureView = SketchView()

    let blueColorButton = ZSButton()
    let blackColorButton = ZSButton()
    let redColorButton = ZSButton()
    let greenColorButton = ZSButton()
    
    public var cancelBlock: (()->())?
    
    let signatureColors: [UIColor] = [.black, .blue, .red, .green]

    override public var prefersStatusBarHidden: Bool
    {
        return DeviceType.isIphone
    }

    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask
    {
        return .landscape
    }
    
    override public var shouldAutorotate: Bool
    {
        return true
    }
    
    let strokeImage = UIImage(named: "draw_btn_color_stroke")?.withRenderingMode(.alwaysTemplate)

    override public func viewDidLoad() {
        super.viewDidLoad()
        ZColor.adjustNavigationBarForTheme(self.navigationController?.navigationBar)
        navigationController?.navigationBar.shadowImage = UIImage();
        // Do any additional setup after loading the view.
        self.view.backgroundColor   =   ZColor.bgColorWhite
        self.navigationController?.navigationBar.isTranslucent = false
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.title  =   "sign.mobile.profile.drawSignature".ZSLString
        
        // Close button
        self.navigationItem.leftBarButtonItem    = ZSBarButtonItem.barButton(type: .cancel, target: self, action: #selector(dismissVC))
        // Done button
        self.navigationItem.rightBarButtonItem = ZSBarButtonItem.barButton(type: .save, target: self, action: #selector(saveButtonPressed))
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        signatureView.frame            = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - 60)
        signatureView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        signatureView.lineColor         =   UIColor.black
        signatureView.lineAlpha         =   1
        signatureView.style             = .pen
        signatureView.delegate = self
        signatureView.lineWidth         =   DeviceType.isIpad ? 8 :  4
        self.view.addSubview(signatureView)
    
        blackColorButton .addTarget(self, action: #selector(changeSignColor(button:)), for: .touchUpInside)
        blackColorButton.tintColor =  ZColor.isNightModeOn ?  UIColor(white: 0.15, alpha: 1) : signatureColors[0]
        blackColorButton .setImage(UIImage(named: "draw_btn_color")?.withRenderingMode(.alwaysTemplate), for: .normal)
        blackColorButton.tag = 0
        blackColorButton.setBackgroundImage(UIImage(named: "draw_btn_color_stroke"), for: .normal)
        blackColorButton.autoresizingMask =   [.flexibleTopMargin]
        self.view.addSubview(blackColorButton)
        
        blueColorButton .addTarget(self, action: #selector(changeSignColor(button:)), for: .touchUpInside)
        blueColorButton .setImage(UIImage(named: "draw_btn_color")?.withRenderingMode(.alwaysTemplate), for: .normal)
        blueColorButton.tag = 1
        blueColorButton.tintColor =   signatureColors[1]
        blueColorButton.autoresizingMask =   [.flexibleTopMargin]
        self.view.addSubview(blueColorButton)
        
        redColorButton .addTarget(self, action: #selector(changeSignColor(button:)), for: .touchUpInside)
        redColorButton .setImage(UIImage(named: "draw_btn_color")?.withRenderingMode(.alwaysTemplate), for: .normal)
        redColorButton.tag = 2
        redColorButton.tintColor =   signatureColors[2]
        redColorButton.autoresizingMask =   [.flexibleTopMargin]
        self.view.addSubview(redColorButton)
        
        greenColorButton .addTarget(self, action: #selector(changeSignColor(button:)), for: .touchUpInside)
        greenColorButton .setImage(UIImage(named: "draw_btn_color")?.withRenderingMode(.alwaysTemplate), for: .normal)
        greenColorButton.tag = 3
        greenColorButton.tintColor =   signatureColors[3]
        greenColorButton.autoresizingMask =   [.flexibleTopMargin]
        self.view.addSubview(greenColorButton)
        
        let colorStackView = UIStackView(arrangedSubviews: [blackColorButton,blueColorButton,redColorButton,greenColorButton])
        colorStackView.frame =   CGRect(x: 0, y: 0, width: 210, height: 40)
        colorStackView.axis = .horizontal
        colorStackView.distribution = .fillEqually
        colorStackView.spacing  =   10
        colorStackView.backgroundColor = ZColor.bgColorGray
        self.view.addSubview(colorStackView)
        
        var parentItem : Any = self.view.safeAreaLayoutGuide
       
        colorStackView.translatesAutoresizingMaskIntoConstraints = false
        let width3 = NSLayoutConstraint(item: colorStackView, attribute: .width, relatedBy: .equal, toItem:nil, attribute: .notAnAttribute, multiplier: 1, constant: 158)
        let leading3 = NSLayoutConstraint(item: colorStackView, attribute: .leading, relatedBy: .equal, toItem: parentItem, attribute: .leading, multiplier: 1, constant: 16)
        let top3 = NSLayoutConstraint(item: colorStackView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -46)
        let height3 = NSLayoutConstraint(item: colorStackView, attribute: .height, relatedBy: .equal, toItem:nil, attribute: .notAnAttribute, multiplier: 1, constant: 32)
        self.view.addConstraints([width3,leading3,top3,height3])

        let clearSignButton =   ZSButton()
        clearSignButton.addTarget(signatureView, action: #selector(signatureView.clear), for: .touchUpInside)
        if #available(iOS 13, *){
            clearSignButton.setImage(ZSImage.tintColorSystemImage(withNamed: "arrow.clockwise", col: ZColor.primaryTextColor, with: .alwaysTemplate), for: .normal)
        } else {
            clearSignButton.setImage(ZSImage.tintColorImage(withNamed: "icn_reset", col: ZColor.primaryTextColor), for: .normal)
        }

        
        self.view.addSubview(clearSignButton)
        
        clearSignButton.translatesAutoresizingMaskIntoConstraints = false
        let width = NSLayoutConstraint(item: clearSignButton, attribute: .width, relatedBy: .equal, toItem:nil, attribute: .notAnAttribute, multiplier: 1, constant: 32)
        let height = NSLayoutConstraint(item: clearSignButton, attribute: .height, relatedBy: .equal, toItem:nil, attribute: .notAnAttribute, multiplier: 1, constant: 32)
        let trailing = NSLayoutConstraint(item: clearSignButton, attribute: .trailing, relatedBy: .equal, toItem: parentItem, attribute: .trailing, multiplier: 1, constant: -16)
        let top = NSLayoutConstraint(item: clearSignButton, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -46)
        self.view.addConstraints([width,trailing,top,height])
    }
    
    override public func viewDidAppear(_ animated: Bool)  {
        
        super.viewDidAppear(animated)
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: CGPoint(x: 16, y: self.view.frame.size.height - 60))
        linePath.addLine(to: CGPoint(x: self.view.frame.size.width - 16, y: self.view.frame.size.height - 60))
        line.path = linePath.cgPath
        line.strokeColor = ZColor.seperatorColor.cgColor
        line.lineWidth = 0.5
        line.lineJoin = CAShapeLayerLineJoin.round
        line.lineDashPattern = [8, 8]
        self.view.layer.addSublayer(line)
        
    }
    
    @objc public func saveButtonPressed()  {
        signatureView.getSignatureImage()
    }
    
    @objc public func clearSignButtonPressed()  {
        signatureView.clear()
    }
    
    
    @objc public func dismissVC() {
        self.dismiss(animated: true, completion: nil)
        cancelBlock?()
    }
    
    @objc public func changeSignColor(button: UIView){
        
        let color = signatureColors[button.tag]
        guard signatureView.lineColor != color else {return}
        signatureView.redraw(with: color)
        signatureView.lineColor = color
        
        switch color {
        case .black:
            animateImageTransition(buttons: [redColorButton,greenColorButton,blueColorButton], buttonWithStroke: blackColorButton, image: strokeImage)
        case .blue:
            animateImageTransition(buttons: [blackColorButton,redColorButton,greenColorButton], buttonWithStroke: blueColorButton, image: strokeImage)
        case .red:
            animateImageTransition(buttons: [blackColorButton,blueColorButton,greenColorButton], buttonWithStroke: redColorButton, image: strokeImage)
        case .green:
            animateImageTransition(buttons: [blackColorButton,blueColorButton,redColorButton], buttonWithStroke: greenColorButton, image: strokeImage)
        default:
            break
        }
    }
    
    func animateImageTransition(buttons: [UIButton], buttonWithStroke: UIButton, image: UIImage?){
        
        buttons.forEach({ button in
            UIView.transition(with: button, duration: 0.3, options: [.transitionCrossDissolve], animations: {
                button.setBackgroundImage(nil, for: .normal)
            })
        })
        
        UIView.transition(with: buttonWithStroke, duration: 0.3, options: [.transitionCrossDissolve], animations: {
            buttonWithStroke.setBackgroundImage(image, for: .normal)
        })
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ZSDrawSignatureController: SketchViewDelegate {
    
    public func willBeginDrawing(_ view: SketchView) {
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    public func didPerformedAction(_ action: SketchView.Actions) {
        switch action {
        case .clear:
            navigationItem.rightBarButtonItem?.isEnabled = false
        default:
            break //TODO: should handle for redo and undo
        }
    }

    public func imageforDrawnSignature(_ image: UIImage) {
        self.delegate?.signatureCreated(signImage: image)
        self.dismissVC()
    }
}
