//
//  ZTextFieldCell.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 14/09/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZTextFieldCell: UITableViewCell, UITextFieldDelegate {
    
    public var textField = UITextField()
    
    public var isCheckForRegex : Bool = false
    var regexPattern : String?
    public var maxLimit : Int?
    var popupShowInView : UIView?
    var keyString : String = ""
    fileprivate var isShowDeleteButton : Bool = false
    
    fileprivate let deleteButton = ZSButton()

    public var textFieldBeginEditing : (() -> Void)?
    public var textFieldEndEditing : ((String) -> Void)?
    public var textFieldShouldChangeCharactersIn : ((UITextField, String) -> Void)?
//    public var textFieldShouldReturn : (() -> Void)?
    public var deleteButtonTapHandler : (() -> Void)?
    var customLeftView: UIView? = nil

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initial setup
        initialSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        //fatalError("init(coder:) has not been implemented")
    }
    
    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func initialSetup(){
        
        self.contentView.backgroundColor = ZColor.bgColorWhite
        
        textField.backgroundColor = UIColor.clear
        textField.font = ZSFont.bodyFont()
        textField.textColor = ZColor.primaryTextColor
        textField.returnKeyType = .done
        textField.clearButtonMode = .whileEditing
        textField.autocorrectionType = .no
        textField.tintColor = ZColor.tintColor
        textField.delegate = self
        self.contentView.addSubview(textField)
        
        textField.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(cellValueLabelX)
            make.right.equalTo(-12)
        }
        
        self.contentView.addSubview(deleteButton)
        deleteButton.backgroundColor = ZColor.bgColorWhite
        deleteButton.setImage(ZSImage.tintColorImage(withNamed: "field_delete_icon", col: ZColor.redColor), for: .normal)
        deleteButton.addTarget(self, action: #selector(deleteButtonPressed), for: .touchUpInside)
        deleteButton.snp.makeConstraints { (make) in
            make.top.bottom.right.equalToSuperview()
            make.width.equalTo(50)
        }
        deleteButton.isHidden = true
        
    }
    
    public var showDeleteButton: Bool {
        set {
            textField.snp.removeConstraints()
            textField.snp.makeConstraints { (make) in
                make.top.bottom.equalToSuperview()
                make.left.equalTo(cellValueLabelX)
                make.right.equalTo((newValue ? -52 : -12))
            }
            deleteButton.isHidden = !newValue
            isShowDeleteButton = newValue
        }
        get {
            return isShowDeleteButton
        }
    }
    
    func addCustomView(customLeftView: UIView? = nil) {
        self.customLeftView?.removeFromSuperview()
        self.customLeftView = customLeftView

        if let customLeftView = customLeftView {
            contentView.addSubview(customLeftView)
            customLeftView.isUserInteractionEnabled = true
            customLeftView.snp.makeConstraints { (make) in
                make.centerY.leading.equalToSuperview()
                make.width.equalTo(2*cellValueLabelX + 20)
                make.top.bottom.equalToSuperview()
            }
            
            textField.snp.removeConstraints()
            textField.snp.makeConstraints { (make) in
                make.top.bottom.equalToSuperview()
                make.leading.equalTo(customLeftView.snp.trailing)
                make.right.equalTo(-12)
            }
        }
    }
//
//    override public func layoutSubviews() {
//        super.layoutSubviews()
//        if isShowDeleteButton {
//            textField.frame = CGRect(x: cellValueLabelX, y: 0, width: self.bounds.size.width - 82, height: self.bounds.size.height)
//        }
//        else{
//            textField.frame = CGRect(x: cellValueLabelX, y: 0, width: self.bounds.size.width - 32, height: self.bounds.size.height)
//        }
//    }
    
    // MARK: -
    
    @objc public  func setTextField(placeholder :String, value:String?, editable:Bool)
    {
        keyString = placeholder
//        textField.placeholder = placeholder
        textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: ZColor.secondaryTextColorLight])

        textField.text        = value
        
        isUserInteractionEnabled    = editable
        
        if (editable) {
            textField.alpha = 1.0
        }else {
            textField.alpha = 0.5
        }
        self.setNeedsLayout()
    }
    
    @objc public func deleteButtonPressed()
    {
        deleteButtonTapHandler?()
    }
    public func checkForTextFieldRegex(regex pattern:String, showPopInView view:UIView){
        isCheckForRegex = true
        regexPattern = pattern
        popupShowInView = view
    }
    
    public func setTextMaxLimit(_ limit:Int){
        maxLimit = limit
    }
    
    // MARK: -
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        textFieldBeginEditing?()
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        textFieldEndEditing?(textField.text ?? "")
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
                
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let newString = textFieldText.replacingCharacters(in: range, with: string)
        
        var isAllowChar : Bool = true
        if let limit = maxLimit {
            // If existing text is greater than limit, allow delete character
            if ((textField.text?.count)! <= limit) {
                isAllowChar = (newString.count <= limit)
            }
        }
        if (isCheckForRegex && newString.count > 0) {
            if let regex = self.regexPattern {
                let regexPredicate = NSPredicate(format:"SELF MATCHES %@", regex)
                
                if (!regexPredicate.evaluate(with: string)) {
                    // Invalid char
                    // Delete chars, but not add chars
                    if ((textField.text?.count)! < newString.count) {
                        isAllowChar = false
                        let message = "\(self.keyString ) should not contain '\(string)'"

//                        SwiftUtils.showPopTip(withMessage: message, inView: self, onRect: CGRect(x: 80, y: 20, width: 50, height: 50))
                        if let shownInView = self.popupShowInView {
                            SwiftUtils.showPopTip(withMessage: message, inView: shownInView, onRect: self.frame)

                        }
                    }
                }
            }
        }
        
        if (isAllowChar) {
            textFieldShouldChangeCharactersIn?(textField, newString)
//            if (self.textFieldShouldChangeCharactersIn != nil) {
//                self.textFieldShouldChangeCharactersIn!(textField, newString)
//            }
        }
        return isAllowChar
        
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textFieldShouldChangeCharactersIn?(textField, "")
        return true
    }
}
