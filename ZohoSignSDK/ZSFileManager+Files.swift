//
//  ZSFileManager+Files.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 13/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import Foundation

extension ZSFileManager{
    public func writeFile(withFileId fileId: String, fileData: Data, saveCache save: Bool) -> Bool {
    
        let folder = (save) ? createSubFolder(parentFilePath: sharedDirectoryPath, subFolder: FolderNameAllFiles) : createSubFolder(parentFilePath: cacheDirectoryPath, subFolder: UnCachedFileSaveLocation)
        let fileURL =  URL(fileURLWithPath: folder.folderPath).appendingPathComponent("\(fileId).pdf")

        do{
            try fileData.write(to: fileURL)
            return true
        }catch{
            return false
        }
    }

    public func getFile(withFileId fileId: String, isSharedLocation sharedLocation: Bool) -> String? {

        let folder = (sharedLocation) ? createSubFolder(parentFilePath: sharedDirectoryPath, subFolder: FolderNameAllFiles) : createSubFolder(parentFilePath: cacheDirectoryPath, subFolder: UnCachedFileSaveLocation)

        let filePath =  URL(fileURLWithPath: folder.folderPath).appendingPathComponent("\(fileId).pdf").path

        if fileManager.fileExists(atPath: filePath) {
            return filePath
        }
        return nil
    }

}
