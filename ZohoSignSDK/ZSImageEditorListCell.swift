//
//  ZSImageEditorListCell.swift
//  ZohoSign
//
//  Created by Nagarajan S on 16/11/17.
//  Copyright © 2017 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZSImageEditorListCell: UITableViewCell {

    let floatingLabelPaddingY:CGFloat = 12.0
    var floatingImageView : UIImageView!
    var keyLabel         =   UILabel()
    var valueButton      =   ZSButton()
    public var separatorLine    =   UIView()
    public var placeHolderLabel =   UILabel()

    public var buttonPressed : (() -> Void)?
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        //fatalError("init(coder:) has not been implemented")
    }
    
    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func initialSetup(){
        
        backgroundColor = ZColor.bgColorWhite
        
        floatingImageView = UIImageView()
        floatingImageView.backgroundColor = .clear
        self.contentView.addSubview(floatingImageView)
        
        
        keyLabel.backgroundColor = .clear
        keyLabel.font =  ZSFont.createListKeyFontSmall()
        keyLabel.textAlignment = .left
        keyLabel.textColor = ZColor.secondaryTextColorDark
        self.contentView.addSubview(keyLabel)

        
        placeHolderLabel.backgroundColor = .clear
        placeHolderLabel.font =  ZSFont.signatureFont(withSize: 20)
        placeHolderLabel.textAlignment = .center
        placeHolderLabel.textColor = ZColor.secondaryTextColorLight
        placeHolderLabel.numberOfLines = 2
        self.contentView.addSubview(placeHolderLabel)
        
        valueButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        valueButton.titleLabel?.textAlignment = .left
        valueButton.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        valueButton.contentHorizontalAlignment = .left
        valueButton.addTarget(self, action: #selector(popupBtnPressed), for: UIControl.Event.touchUpInside)
        valueButton.imageView?.contentMode  =   .scaleAspectFit
        valueButton.setTitleColor(ZColor.primaryTextColor, for: UIControl.State.normal)
        self.contentView.addSubview(valueButton)
//        valueButton.contentEdgeInsets = UIEdgeInsets(top: 18, left: 25, bottom: 0, right: 0)

        separatorLine.backgroundColor = ZColor.seperatorColor
        self.contentView.addSubview(separatorLine)
        
        selectionStyle = .none
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        floatingImageView.frame = cellIconFrame
        keyLabel.frame = CGRect(x: cellValueLabelX, y: cellPlaceHolderLabelY, width: self.frame.size.width-50, height: 18)
        valueButton.frame = CGRect(x: cellValueLabelX, y: keyLabel.frame.maxY + 3, width: self.frame.size.width-55, height: self.frame.size.height-(keyLabel.frame.maxY + 10))
        separatorLine.frame = CGRect(x: cellValueLabelX, y: self.bounds.size.height - Constants.ZS_SeparatorLine_Height, width: self.bounds.size.width-30, height: Constants.ZS_SeparatorLine_Height)
        placeHolderLabel.frame = CGRect(x: cellValueLabelX, y: cellValueLabelY, width: self.frame.size.width-55, height: self.frame.size.height-cellValueLabelY)
    }
    
    @objc func popupBtnPressed() {
        if (self.buttonPressed != nil) {
            self.buttonPressed!()
        }
    }
    
    public func setButtonValue(key placeholder:String, iconName imageName:String?,mandatory : Bool, value image:UIImage?)
    {
        let attributes : [NSAttributedString.Key : Any]  = [NSAttributedString.Key.foregroundColor : ZColor.secondaryTextColorDark]
        let attributes1 : [NSAttributedString.Key : Any] = [NSAttributedString.Key.foregroundColor : ZColor.redColor]
        let str = NSMutableAttributedString(string: placeholder, attributes: attributes)
        let str1 = NSAttributedString(string: " *", attributes: attributes1)
        if mandatory {
            str.append(str1)
        }
        
        keyLabel.attributedText = str

        if imageName != nil {
            floatingImageView.image = ZSImage.tintColorImage(withNamed: imageName!, col: ZColor.secondaryTextColorDark)
        }
//        valueButton.setBackgroundImage(image, for: .normal)
        valueButton.setImage(image, for: .normal)
    }

}
