//
//  ZImageCell.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 14/09/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

class ZImageCell: UITableViewCell {

    fileprivate var placeHolderBtn = ZSButton()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initial setup
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialSetup(){
        
        let selectedView = UIView()
        selectedView.backgroundColor = ZColor.listSelectionColor
        selectedBackgroundView = selectedView
        
        placeHolderBtn.isUserInteractionEnabled = false
        placeHolderBtn.contentHorizontalAlignment = .center
        placeHolderBtn.titleLabel?.font = ZSFont.regularFont(withSize: 15)
        placeHolderBtn.setTitleColor(ZColor.primaryTextColor, for: .normal)
        placeHolderBtn.imageEdgeInsets = UIEdgeInsets(top: 23, left: -6, bottom: 23, right: 0)
        placeHolderBtn.imageView?.contentMode = .scaleAspectFit
        addSubview(placeHolderBtn)
        
        imageView?.backgroundColor = ZColor.bgColorWhite
        imageView?.contentMode = .scaleAspectFit
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        imageView?.frame = CGRect(x: 16, y: 3, width: frame.size.width - 32, height: frame.size.height - 6)
        placeHolderBtn.frame = CGRect(x: 16, y: 3, width: frame.size.width - 32, height: frame.size.height - 6)
        placeHolderBtn.setImage(ZSImage.tintColorImage(withNamed: "editor_add_fill", col: ZColor.buttonColor), for: .normal)
        
    }
    
    func setPlaceholder(text : String)  {
        placeHolderBtn.setTitle(text, for: .normal)
        placeHolderBtn.isHidden = false
        imageView?.image = nil
    }
    
    func setImage(_ image : UIImage?) {
        if let img = image {
            placeHolderBtn.isHidden = true
            imageView?.image  = img
        }
    }
    

}


