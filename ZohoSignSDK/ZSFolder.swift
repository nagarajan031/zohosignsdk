//
//  ZSFolder.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 24/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public protocol OtherDetailTypeName {
    var name: String! { get set }
}

public extension Array where Element: OtherDetailTypeName {
    mutating func getNames() -> [String] {
        sort()
        return self.map({$0.name.trimmingCharacters(in: .whitespacesAndNewlines)})
    }
    
    mutating func sort() {
        self = count > 1 ? [self[0]] + self[1...].sorted(by: { (type1, type2) -> Bool in
            return type1.name.trimmingCharacters(in: .whitespacesAndNewlines).localizedCaseInsensitiveCompare(type2.name.trimmingCharacters(in: .whitespacesAndNewlines)) == ComparisonResult.orderedAscending
        }) : self
    }
}

public struct ZSFolder: OtherDetailTypeName {
    public var name: String!
    public var folderId : String!
    
    public init(folderId:String,name:String){
         self.folderId = folderId
         self.name = name
     }
    
    public static var emptyFolder = ZSFolder(folderId: EMPTY_FOLDER_ID, name: "sign.mobile.common.none".ZSLString)
}

public struct ZSRequestType: OtherDetailTypeName {
    public var name: String!
    public var typeId : String!
    
    public init(typeId:String,name:String){
         self.typeId = typeId
         self.name = name
     }
    
    public static var emptyRequestType = ZSRequestType(typeId: EMPTY_TYPE_ID, name: "sign.mobile.common.others".ZSLString)
}




public struct DropdownOption {
    var dropdownValue : String!
    var dropdownOptionId : String?
    var dropdownOrder : String?
}
