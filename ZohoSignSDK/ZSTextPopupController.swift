//
//  ZSTextPopupController.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 06/11/17.
//  Copyright © 2017 Zoho Corporation. All rights reserved.
//

import UIKit

@objc public enum ZSTextPopupType: Int {
    case textField
    case textView
}


public class ZSTextPopupController: UIViewController,UITextFieldDelegate, UITextViewDelegate {
    
    var alphaView        =   UIView()
    public let doneBtn          =   ZSButton()
    
    var topConstraint = NSLayoutConstraint()

    var viewHeightConstraint = NSLayoutConstraint()
    var footerTopConstraint = NSLayoutConstraint()

    var noOfShakes = 0
    var direction = 1
        
    public var textPopupType  : ZSTextPopupType = .textField
    
    public var didEndEditingWithText    : ((String) -> Void)!
    public var didCancelled    : (() -> Void)?
    
    public var isTextFieldCanbeEmpty = false
    public var isValidateEmailAddress = false
    public var cancelBlock: (()->())?
    
    public var shouldIgnoreViewDismiss = false
    
//    let popupWidth : CGFloat  = ((DeviceType.isIpad) ? 300 : 280)
    
    private lazy var viewBackground: UIView = {
        let view = UIView()
        view.backgroundColor = ZColor.bgColorGray
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.alpha      =   0
        return view
    }()
    
    private lazy var footerLabel: UILabel = {
        let footerLabel      =   UILabel()
        footerLabel.textColor    =   ZColor.redColor
        footerLabel.numberOfLines    =   2
        footerLabel.font         =   ZSFont.regularFont(withSize: 12)
        footerLabel.text         =   ""
        footerLabel.translatesAutoresizingMaskIntoConstraints = false
        return footerLabel
    }()
    
    @objc public lazy var textField: UITextField = {
        
        let textfield = UITextField()
        textfield.font                 =    ZSFont.textViewFont()// ZSFont.createListValueFontSmall()
        textfield.autocorrectionType   =    .no
        textfield.delegate             =    self
        textfield.text                 =    ""
        textfield.placeholder          =   ""
        textfield.textColor             =   ZColor.primaryTextColor
        textfield.returnKeyType         =   .done
        textfield.enablesReturnKeyAutomatically = true
        textfield.clipsToBounds         =   false
        textfield.adjustsFontSizeToFitWidth = true
        textfield.layer.borderColor = ZColor.seperatorColor.cgColor
        textfield.layer.borderWidth = 0.8;
        textfield.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 0))
        textfield.leftViewMode = .always
        textfield.backgroundColor = ZColor.bgColorWhite
        textfield.tintColor      =   ZColor.tintColor
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
    
    @objc public lazy var textView: UITextView = {
        let textView = UITextView()
        textView.font                 =    ZSFont.textViewFont()//  ZSFont.createListValueFontSmall()
        textView.delegate             =    self
        textView.text                 =    ""
        textView.textColor             =   ZColor.primaryTextColor
        textView.enablesReturnKeyAutomatically = true
        textView.clipsToBounds         =   true
        textView.layer.borderColor = ZColor.seperatorColor.cgColor
        textView.layer.borderWidth = 0.8;
        textView.backgroundColor = ZColor.bgColorWhite
        textView.tintColor      =   ZColor.tintColor
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    @objc public lazy var textViewPlaceHolderLabel: UILabel = {
        let placeHolderLabel = UILabel()
        placeHolderLabel.font         =    ZSFont.textViewFont()//ZSFont.createListValueFontSmall()
        placeHolderLabel.textColor    =   ZColor.secondaryTextColorDark
        placeHolderLabel.backgroundColor = UIColor.clear
//        placeHolderLabel.translatesAutoresizingMaskIntoConstraints = false
        return placeHolderLabel
    }()
    
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit {
        ZInfo("deinit")
    }
    
    public init() {
        super.init(nibName:nil, bundle: nil)
        self.title = title
        
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
    }
    
    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewBackground.layer.shadowPath = UIBezierPath(roundedRect: self.viewBackground.bounds, cornerRadius: 2).cgPath
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor   =   UIColor(white: 0, alpha: 0)
        createBgView()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.perform({
            self.showPopup()
        }, afterDelay: 0)
    }
    
    // MARK: - Private
    
    func createBgView()  {
        alphaView.frame                 =   self.view.bounds
        alphaView.backgroundColor       =   ZColor.bgColorDullBlack
        alphaView.alpha                 =    0
        alphaView.clipsToBounds         =   false;
        alphaView.isUserInteractionEnabled    =   false
        self.view.addSubview(alphaView)
   
        alphaView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.view.addSubview(viewBackground)

        
        viewBackground.translatesAutoresizingMaskIntoConstraints = false
        viewHeightConstraint = NSLayoutConstraint(item: self.viewBackground, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: ((textPopupType == .textField) ?  150 : 260))
        
        let widthConstraint = NSLayoutConstraint(item: self.viewBackground, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: kTextPopup_width)
        
        let xConstraint = NSLayoutConstraint(item:  self.viewBackground, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        
        topConstraint = NSLayoutConstraint(item:  self.viewBackground, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: -90)
        
        self.view.addConstraints([xConstraint, topConstraint,widthConstraint,viewHeightConstraint])
        
        
        let headerTitleView = UILabel()
        headerTitleView.backgroundColor = .clear
        headerTitleView.autoresizingMask = [.flexibleWidth]
        headerTitleView.font = ZSFont.navTitleFont()
        headerTitleView.textColor = ZColor.primaryTextColor
        headerTitleView.textAlignment = .center
        headerTitleView.numberOfLines = 2
        headerTitleView.adjustsFontSizeToFitWidth = true
        headerTitleView.text = title
        viewBackground.addSubview(headerTitleView)
        
        headerTitleView.snp.makeConstraints { (make) in
            make.left.equalTo(cellValueLabelX)
            make.right.equalTo(-cellValueLabelX)
            make.height.equalTo(24)
            make.top.equalTo(16)
        }
        
        
        viewBackground.addSubview(footerLabel)
        footerTopConstraint = NSLayoutConstraint(item: footerLabel, attribute: .top, relatedBy: .equal, toItem: viewBackground, attribute: .bottom, multiplier: 1, constant: -76)
        let centerY2 = NSLayoutConstraint(item: footerLabel, attribute: .centerX, relatedBy: .equal, toItem: viewBackground, attribute: .centerX, multiplier: 1, constant:0)
        let width2 = NSLayoutConstraint(item: footerLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: (kTextPopup_width - 30))
        let height2 = NSLayoutConstraint(item: footerLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
        viewBackground.addConstraints([footerTopConstraint,centerY2,width2,height2])
        
        
        if textPopupType == .textField {
            viewBackground.addSubview(textField)

            let heightConstraint4 = NSLayoutConstraint(item: textField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 32)
            
            let widthConstraint4 =   (NSLayoutConstraint(item: textField, attribute: .width, relatedBy: .equal, toItem: viewBackground, attribute: .width, multiplier: 1.0, constant: -30))
            
            let xConstraint4 = NSLayoutConstraint(item:  textField, attribute: .centerX, relatedBy: .equal, toItem: viewBackground, attribute: .centerX, multiplier: 1, constant: 0)
            
            let yConstraint4 = NSLayoutConstraint(item:  textField, attribute: .top, relatedBy: .equal, toItem: viewBackground, attribute: .top, multiplier: 1, constant: 60)
            
            self.view.addConstraints([xConstraint4, yConstraint4,widthConstraint4,heightConstraint4])
        }
        else{
            viewBackground.addSubview(textView)
            textView.addSubview(textViewPlaceHolderLabel)
            textViewPlaceHolderLabel.frame = CGRect(x: 5, y: 8, width: 280, height: 50)
            textViewPlaceHolderLabel.sizeToFit()

            let heightConstraint4 = NSLayoutConstraint(item: textView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 150)
            
            let widthConstraint4 =   (NSLayoutConstraint(item: textView, attribute: .width, relatedBy: .equal, toItem: viewBackground, attribute: .width, multiplier: 1.0, constant: -30))
            
            let xConstraint4 = NSLayoutConstraint(item:  textView, attribute: .centerX, relatedBy: .equal, toItem: viewBackground, attribute: .centerX, multiplier: 1, constant: 0)
            
            let yConstraint4 = NSLayoutConstraint(item:  textView, attribute: .top, relatedBy: .equal, toItem: viewBackground, attribute: .top, multiplier: 1, constant: 54)
            
            self.view.addConstraints([xConstraint4, yConstraint4,widthConstraint4,heightConstraint4])
            
        }
        
        setupBottomButtons()
        
    }
    
    func setupBottomButtons()  {
        
        let cancelButton = ZSButton()
        cancelButton.backgroundColor = .clear
        cancelButton.titleLabel?.font = ZSFont.popupCancelBtnFont()
        cancelButton.titleLabel?.textAlignment = .center
        cancelButton.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        cancelButton.contentHorizontalAlignment = .center
        cancelButton.contentVerticalAlignment = .center
        cancelButton.layer.borderWidth = 0.8
        cancelButton.layer.borderColor = ZColor.seperatorColor.cgColor
        cancelButton.setTitleColor(ZColor.buttonColor, for: .normal)
        cancelButton.setTitle("sign.mobile.common.cancel".ZSLString, for: UIControl.State.normal)
        cancelButton.addTarget(self, action: #selector(cancelBtnClicked), for: UIControl.Event.touchUpInside)
        
        doneBtn.backgroundColor = .clear
        doneBtn.titleLabel?.font = ZSFont.popupDoneBtnFont()
        doneBtn.titleLabel?.textAlignment = .center
        doneBtn.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        doneBtn.contentHorizontalAlignment = .center
        doneBtn.contentVerticalAlignment = .center
        doneBtn.setTitleColor(ZColor.buttonColor, for: .normal)
        doneBtn.setTitleColor(ZColor.secondaryTextColorDark, for: .disabled)
        doneBtn.setTitle("sign.mobile.common.done".ZSLString, for: UIControl.State.normal)
        doneBtn.layer.borderWidth = 0.8
        doneBtn.layer.borderColor = ZColor.seperatorColor.cgColor
        doneBtn.addTarget(self, action: #selector(doneBtnClicked), for: UIControl.Event.touchUpInside)
        
        
        
        let stackView  = UIStackView()
        stackView.tintColor = ZColor.buttonColor
        stackView.axis = .horizontal
        stackView.spacing = 0.0
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        viewBackground.addSubview(stackView)
        
        stackView.addArrangedSubview(cancelButton)
        stackView.addArrangedSubview(doneBtn)
        
        
        stackView.snp.makeConstraints { (make) in
            make.centerX.width.equalToSuperview()
            make.bottom.equalToSuperview()
            make.height.equalTo(44)
        }
    }
    
    @objc public func showFooterMsg(_ msg: String)
    {
        footerLabel.text = "* " + msg
        footerLabel.sizeToFit()
        
        let labelY : CGFloat  = ((footerLabel.frame.height > 20) ? -86 : -75)
        let popupWidth : CGFloat  = ((footerLabel.frame.height > 20) ? 184 : 166)
        view.removeConstraint(viewHeightConstraint)
        viewBackground.removeConstraint(footerTopConstraint)

        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.9, options: .curveEaseIn, animations: {
            self.viewHeightConstraint = NSLayoutConstraint(item: self.viewBackground, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: popupWidth)
            self.view.addConstraint(self.viewHeightConstraint)
            
            self.footerTopConstraint = NSLayoutConstraint(item: self.footerLabel, attribute: .top, relatedBy: .equal, toItem: self.viewBackground, attribute: .bottom, multiplier: 1, constant: labelY)
            self.viewBackground.addConstraint(self.footerTopConstraint)

        });
    }
    
    @objc public func shake()
    {
        noOfShakes = 0;
        direction = 1;
        
        shakeAnim()
    }
    
    func shakeAnim()
    {
        UIView.animate(withDuration: 0.06, animations: {
            self.viewBackground.transform = CGAffineTransform.init(translationX: CGFloat(4*self.direction), y: 0)
        }) { (finished) in
            if(self.noOfShakes >= 4)
            {
                self.direction = 1;
                self.noOfShakes = 0;
                self.viewBackground.transform = CGAffineTransform.identity
                return;
            }
            
            self.noOfShakes = self.noOfShakes+1
            self.direction = self.direction * -1;
            self.shakeAnim()
        }
        
    }
    
    //MARK:- Show/ hide
    @objc public func cancelBtnClicked()
    {
        hideView(isCompleted: false)
    }
    func showPopup()
    {
        textField.enablesReturnKeyAutomatically =   !isTextFieldCanbeEmpty
        doneBtn.isEnabled  =   (isTextFieldCanbeEmpty || (textField.text!.count > 0));
        viewBackground.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        viewBackground.alpha = 0.0
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.viewBackground.alpha =    1
            self.alphaView.alpha      =    1
            self.viewBackground.transform = CGAffineTransform(scaleX: 0.99, y: 0.99)
        }) { (completed) in
            if self.textPopupType == .textField {
                self.textField.becomeFirstResponder()
            }else{
                self.textView.becomeFirstResponder()
            }
            self.alphaView.alpha      =    1
            UIView.animate(withDuration: 0.05, delay: 0, options: .curveEaseIn, animations: {
                self.viewBackground.transform = CGAffineTransform.identity
            }, completion: { (completed) in
                self.alphaView.alpha      =    1
            })
        }
        
    }
    
    
    
    func hideView(isCompleted : Bool)
    {
        if shouldIgnoreViewDismiss && isCompleted {
            let inputText = ((self.textPopupType == .textField) ? self.textField.text! : self.textView.text!)
            self.didEndEditingWithText?(inputText)
            return
        }
        NotificationCenter.default.removeObserver(self)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.viewBackground.alpha =     0
            self.alphaView.alpha      =     0.2
            self.viewBackground.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }) { (completed) in
            UIView.animate(withDuration: 0.05, delay: 0, options: .curveEaseOut, animations: {
                self.viewBackground.alpha =  0
                self.alphaView.alpha      =  0
                self.viewBackground.transform = CGAffineTransform.identity
            }) { (completed) in
                self.viewBackground.removeFromSuperview()
                self.dismiss(animated: false, completion: {
                    self.cancelBlock?()
                    if isCompleted
                    {
                        let inputText = ((self.textPopupType == .textField) ? self.textField.text! : self.textView.text!)
                        self.didEndEditingWithText?(inputText)
                    }
                    else{
                        self.didCancelled?()
                    }
                })
            }
        }
    }
    
    @objc public func doneBtnClicked()
    {
        if (isValidateEmailAddress) {
            if !SwiftUtils.isValidEmail(textField.text ?? "", showAlertIfFails: false, rootViewController: nil) {
                SwiftUtils.showPopTip(withMessage: "sign.mobile.recipient.invalidEmail".ZSLString, inView: textField, onRect: CGRect(x: 14, y: 0, width: 0, height: 0))
                return;
            }
        }
        
        hideView(isCompleted: true)
    }
    //MARK:- Keyboard Handling
//    @objc func keyboardShown(notification: NSNotification) {
//        let info = notification.userInfo!
//        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
//        ZInfo("keyboardFrame: \(keyboardFrame)")
//        yConstraint.constant = ((-keyboardFrame.size.height/2) + 30)
//
//        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
//            self.view.updateConstraints()
//        }, completion: nil)
//    }


    
    //MARK:- Textfield delegate
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let textFieldText = textField.text as NSString?{
            let newString = textFieldText.replacingCharacters(in: range, with: string)
            let trimmedStrin = newString.trimmingCharacters(in: .whitespaces)
            doneBtn.isEnabled  =   (isTextFieldCanbeEmpty || (trimmedStrin.count > 0));
        }
        
        
        return true;
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        doneBtnClicked()
        return false
    }
    
    //MARK:- Textview delegate
    public func textViewDidChange(_ textView: UITextView) {
        let trimmedStrin = textView.text.trimmingCharacters(in: .whitespaces)
        doneBtn.isEnabled  =   (isTextFieldCanbeEmpty || (trimmedStrin.count > 0));
        textViewPlaceHolderLabel.isHidden = doneBtn.isEnabled;
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var enumCount = 0
        textView.text.enumerateLines { (str, _) in
            enumCount += 1
        }
        if text == "\n" && (enumCount > 9) {
            return false
        }
        return true
    }
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            let topAlign = -90 - (keyboardSize.height/4)
            
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn,.beginFromCurrentState] , animations: {
                self.topConstraint.constant = topAlign
                self.view.layoutIfNeeded()
            }) { (finisf) in
                
            }
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn,.beginFromCurrentState] , animations: {
            self.topConstraint.constant = -90
            self.view.layoutIfNeeded()
        }) { (finisf) in
            
        }
    
    }
}

