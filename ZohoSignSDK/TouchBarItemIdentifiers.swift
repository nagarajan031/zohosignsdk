////
////  TouchBarItemIdentifiers.swift
////  ZohoSignSDK
////
////  Created by somesh-8758 on 15/07/19.
////  Copyright © 2019 Zoho Corporation. All rights reserved.
////
//
//#if targetEnvironment(macCatalyst)
//import AppKit
//
//extension NSTouchBarItem.Identifier {
//    static let doneButtonIdentifier = NSTouchBarItem.Identifier("touchbar.popTextFieldPage.done")
//    static let cancelButtonIdentifier = NSTouchBarItem.Identifier("touchbar.popTextFieldPage.cancel")
//
//
//}
//
//#endif
