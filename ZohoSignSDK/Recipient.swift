//
//  Recipient.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 05/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

enum RecipientAction {
    case sign
    case view
    case inPersonSign
    case Declined
    case Approver
}

public class ZSRecipient {
    public var recipientID: String?
    public var name: String?
    public var email: String?
    public var countryCode: String?
    public var phoneNumber: String?
    public var userRole: String?
    public var actionId: String?
    public var actionType: String?
    public var fields: [Any]?
    public var draftFieldListArray: [SignField] = []
    public var secretCode: String?
    public var deliveryType: String?
 /* 1) ZS_DeliverType_email 2) ZS_DeliverType_offline  3) ZS_DeliverType_sms */
    public var privateMessage: String?
    public var signingOrder = 0
    public var isNeedToSign = true
    public var isVerificationNeeded = false
    public var isTemplateUser = false
    
    public var isSigningCompleted = false

    //Inperson signing
    public var isHost = false
    public var inPersonEmail: String?
    public var inPersonName: String?
    public var inPersonSigningId: String?
    public var inPersonSigningStatus: String?
    public var inPersonSignature: UIImage?
    public var inPersonInitial: UIImage?

    func inpersonFirstName() -> String? {
        if let name = inPersonName {
            let arr = name.components(separatedBy: " ")
            if arr.count > 0 {
                return arr.first
            }
        }
        return inPersonName
    }

    func inpersonLastName() -> String? {
        if let name = inPersonName {
            let arr = name.components(separatedBy: " ")
            if arr.count > 1 {
                return arr.last
            }
        }
        return ""
    }

    func displayRecipientName() -> String {
        var recipientName = userRole
        if (recipientName == nil) || (recipientName == "") {
            recipientName = name
        }
        if (recipientName == nil) || (recipientName == "") {
            recipientName = email
        }
        if (actionType == ZS_ActionType_InPerson) {
            recipientName = inPersonName
            if (recipientName == nil) || (recipientName == "") {
                recipientName = inPersonEmail
            }
            if (recipientName == nil) || (recipientName == "") {
                recipientName = name
            }
            if (recipientName == nil) || (recipientName == "") {
                recipientName = email
            }
        }

        return recipientName ?? ""
    }

    public init() {}
}
