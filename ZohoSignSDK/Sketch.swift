//
//  ZSSketchTools.swift
//  SignatureView
//
//  Created by somesh-8758 on 13/02/20.
//  Copyright © 2020 Somesh-8758. All rights reserved.
//

import UIKit
class Sketch: NSObject {
    
    var tool: SketchTool?
    var type: Sketch.UndoType
    var image: UIImage?
    
    init(tool: SketchTool?, type: Sketch.UndoType) {
        self.tool = tool
        self.type = type
        super.init()
    }
    
    static func sketchObject(with tool: SketchTool) -> Sketch {
        return Sketch(tool: tool, type: .sketchTool)
    }
    
    static func clear() -> Sketch {
        return Sketch(tool: nil, type: .clear)
    }
}

extension Sketch {
    enum UndoType: Int {
        case sketchTool = 0
        case clear
        case image
    }

    enum Style: Int {
        case pencil = 0
        case pen
        case brush
        case eraser
    }
}

struct SketchDefaults {
    static var lineColor: UIColor = .black
    static var lineWidth: CGFloat = 10
    static var lineAlpha: CGFloat = 1.0
    static var imageCacheLimit = 25
    static var undoLimit = 100
}

class SketchPath: NSObject {
    var path: CGMutablePath = .init()
    var point: CGPoint = .zero
    var force: CGFloat = 0
    
    override init() {
        super.init()
    }
    
    convenience init(path: CGMutablePath, force: CGFloat) {
        self.init()
        self.path = path
        self.force = force
    }
    
    convenience init(point: CGPoint, force: CGFloat) {
        self.init()
        self.point = point
        self.force = force
    }
}

class SketchToolIndex: NSObject {
    var index: Int
    var undoType: Sketch.UndoType
    
    init(index: Int, undoType: Sketch.UndoType) {
        self.index = index
        self.undoType = undoType
        super.init()
    }
    
    static func sketchTool(at index: Int) -> SketchToolIndex {
        return SketchToolIndex(index: index, undoType: .sketchTool)
    }
    
    static func imageTool(at index: Int) -> SketchToolIndex {
        return SketchToolIndex(index: index, undoType: .image)
    }
    
    static func clearTool(at index: Int) -> SketchToolIndex {
        return SketchToolIndex(index: index, undoType: .clear)
    }
}
