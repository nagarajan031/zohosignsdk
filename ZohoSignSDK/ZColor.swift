//
//  ZColor.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 08/10/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit


public class ZColor: NSObject {


    public static var isNightModeOn = false
    public static var  bgColorWhite  =   UIColor.white
    public static var  bgColorGray  =   UIColor(red:0.97, green:0.97, blue:0.97, alpha:1.00)
    public static var  bgColorDark  =   UIColor(white: 0, alpha: 0.9)
    public static var  bgColorDullBlack  =   UIColor(white: 0, alpha: 0.4)

    public static var homeBgColor = UIColor(red:0.99, green:0.99, blue:0.99, alpha:1.00)

    public static var  tintColor      =   ZColor.blueColor

//    public static var  navBarColor      =  DeviceType.isIpad ?  ZColor.bgColorGray : ZColor.bgColorGray
    public static var  navBarColor      =   ZColor.bgColorGray

    public static var  listSelectionColor = UIColor(red: 0.86, green: 0.91, blue: 0.97, alpha: 1.00)
    
    //Text colors
    
    public static var  primaryTextColor =  UIColor.black
    public static var  primaryTextColorLight =  UIColor(white: 0, alpha: 0.8)
    public static var  secondaryTextColorDark = UIColor(white: 0, alpha: 0.5)
    public static var  secondaryTextColorLight = UIColor(white: 0, alpha: 0.3)
    public static var  disableColorLight = UIColor(red: 0.82, green: 0.84, blue: 0.89, alpha: 1.00)
    @objc public static var  seperatorColor =   UIColor(red:0.88, green:0.88, blue:0.89, alpha:1.00)
    public static var  seperatorColorLight = UIColor(red: 0.89, green: 0.90, blue: 0.93, alpha: 0.5)
    public static var  ipadListSeperatorColor = UIColor(red: 0.89, green: 0.90, blue: 0.93, alpha: 0.5)

//    public static var  buttonColor = UIColor(red:0.18, green:0.48, blue:0.96, alpha:1.00)//UIColor(red: 0.30, green: 0.58, blue: 0.97, alpha: 1)
    public static var  buttonColor = UIColor(red:0.18, green:0.48, blue:0.96, alpha:1.00)// UIColor(red: 0.30, green: 0.58, blue: 0.97, alpha: 1)

    public static var  tableHeaderTextColor = UIColor(red:0.36, green:0.36, blue:0.36, alpha:1.00)
    public static var  tableHeaderBgColor = UIColor(red:0.89, green:0.90, blue:0.90, alpha:1.00)
    public static var  tabBarColor = UIColor(red: 0.98, green: 0.98, blue: 0.98, alpha: 1)
    public static var  suggestionViewBgColor = UIColor(red: 0.94, green: 0.96, blue: 0.99, alpha: 1)
    public static var  notifyBgColor = UIColor(red:0.18, green:0.16, blue:0.20, alpha:1.00)
    public static var ipadRightSideBarColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.00)

    public static var ipadLeftSideBarColor : UIColor? = ZColor.bgColorWhite

    //status colors
    public static let  draftColor      =   UIColor(red:0.46, green:0.78, blue:0.96, alpha:1.00)
    public static let  inprogressColor  =   UIColor(red:0.95, green:0.60, blue:0.22, alpha:1.00)
    public static let  completedColor =   greenColor
    public static let  expiringColor =   UIColor(red: 0.97, green: 0.58, blue: 0.37, alpha: 1)
    public static let  expiredColor  =   UIColor(red: 0.78, green: 0.33, blue: 0.47, alpha: 1)
    public static let  recalledColor =   UIColor(red: 0.24, green: 0.41, blue: 0.56, alpha: 1)
    public static let  trashColor    =   UIColor(red: 0.63, green: 0.59, blue: 0.59, alpha: 1)
    public static let  declinedColor =   redColor
    
    
    
//custom colors
    public static let  greyColor =   UIColor(red: 0.59, green: 0.62, blue: 0.66, alpha: 1)
    public static let  redColor =  UIColor(red:0.93, green:0.30, blue:0.24, alpha:1.00)// UIColor(red:0.89, green:0.33, blue:0.31, alpha:1.00)
    public static let  greenColor =   UIColor(red:0.46, green:0.84, blue:0.45, alpha:1.00)//UIColor(red: 0.55, green: 0.82, blue: 0.49, alpha: 1)
    public static let  blackColor =   UIColor.black
    public static let  whiteColor =   UIColor.white
    public static let  blueColor =   UIColor(red:0.19, green:0.48, blue:0.96, alpha:1.00)
    public static var  shimmeringGridBackgroundColor = UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 0.5)
    public static var  shimmeringListBackgroundColor = UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 1)
    public static var  shimmeringGridViewColor = UIColor(red: 0.90, green: 0.90, blue: 0.94, alpha: 0.7)
    public static var  shimmeringCollectionAndTableBackgroundColor = UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1)
    public static var  shimmeringCollectionViewColor = UIColor(red: 0.87, green: 0.89, blue: 0.93, alpha: 1)
    public static var  shimmeringDraftBackgroundColor = UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 0.5)
    public static var shimmeringIpadDocslistBackgroundColor = UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 0.5)
    
    //MARK:- Helpers
    public static func hexString(fromColor : UIColor) -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        fromColor.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return String(
            format: "%02X%02X%02X",
            Int(r * 0xff),
            Int(g * 0xff),
            Int(b * 0xff)
        )
    }
    
    public static func color(hexString : String) -> UIColor {
        let scanner = Scanner(string: hexString)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        return  UIColor(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
    
    public static func filterStatusColor(filterKey : String) -> UIColor {
        switch filterKey {
        case STATUS_INPROGRESS:
            return ZColor.inprogressColor
        case STATUS_EXPIRING:
            return ZColor.expiringColor
        case STATUS_COMPLETED:
            return ZColor.completedColor
        case STATUS_DECLINED:
            return ZColor.declinedColor
        case STATUS_EXPIRED:
            return ZColor.expiredColor
        case STATUS_DRAFT:
            return ZColor.draftColor
        case STATUS_DELETED:
            return ZColor.trashColor
        case STATUS_RECALLED:
            return ZColor.recalledColor
        default:
            return ZColor.whiteColor
        }
    }
    
    public static func adjustNavigationBarForTheme(_ navBar : UINavigationBar?) {
        if ZColor.isNightModeOn {
            navBar?.tintColor = ZColor.tintColor
            navBar?.barStyle = .black
        }
    }
    
    //MARK:- Night theme
    public static func changeTheme(isNightMode : Bool) {
        ZColor.isNightModeOn = isNightMode
        if isNightMode {
    
            
            ZColor.bgColorWhite  =   UIColor(red:0.15, green:0.15, blue:0.15, alpha:1.0)
            ZColor.bgColorGray  =   UIColor(red:0.08, green:0.08, blue:0.08, alpha:1.0)
            ZColor.bgColorDark  =   UIColor(white: 0, alpha: 0.9)
            ZColor.bgColorDullBlack  =   UIColor(white: 0, alpha: 0.4)
            
            
            ZColor.tintColor      =   ZColor.whiteColor
            
            ZColor.navBarColor      =   ZColor.bgColorGray
            
            
            ZColor.listSelectionColor = UIColor(red:0.07, green:0.10, blue:0.13, alpha:1.00)
            
            //Text colors
            
            ZColor.primaryTextColor =  UIColor.white
            ZColor.primaryTextColorLight =  UIColor(white: 1, alpha: 0.8)
            ZColor.secondaryTextColorDark = UIColor(white: 1, alpha: 0.5)// UIColor(red: 0.45, green: 0.45, blue: 0.45, alpha: 1.00)
            ZColor.secondaryTextColorLight = UIColor(white: 1, alpha: 0.3)//UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 1.00)
            ZColor.disableColorLight = UIColor(red: 0.82, green: 0.84, blue: 0.89, alpha: 1.00)
            ZColor.seperatorColor = (DeviceType.isIpad) ? UIColor(red:0.22, green:0.26, blue:0.30, alpha:1.00) :  UIColor(red:0.22, green:0.26, blue:0.30, alpha:1.00)
            ZColor.seperatorColorLight = UIColor(red:0.22, green:0.26, blue:0.30, alpha:0.5)
            ZColor.tableHeaderTextColor = UIColor(red:0.36, green:0.36, blue:0.36, alpha:1.00)//UIColor(red: 0.45, green: 0.45, blue: 0.45, alpha: 1)
            ZColor.tabBarColor = UIColor(red: 0.98, green: 0.98, blue: 0.98, alpha: 1)
        }
        else{
            ZColor.bgColorWhite  =   UIColor.white
            ZColor.bgColorGray  =   UIColor(red:0.97, green:0.97, blue:0.97, alpha:1.00)
            ZColor.bgColorDark  =   UIColor(white: 0, alpha: 0.9)
            ZColor.bgColorDullBlack  =   UIColor(white: 0, alpha: 0.4)
            
            
            ZColor.tintColor      =   ZColor.blueColor
            
            ZColor.navBarColor      =   ZColor.bgColorGray
            
            ZColor.listSelectionColor = UIColor(red: 0.86, green: 0.91, blue: 0.97, alpha: 1.00)
            
            //Text colors
            
            ZColor.primaryTextColor =  UIColor.black
            ZColor.primaryTextColorLight =  UIColor(white: 0, alpha: 0.8)
            ZColor.secondaryTextColorDark = UIColor(white: 0, alpha: 0.5)// UIColor(red: 0.45, green: 0.45, blue: 0.45, alpha: 1.00)
            ZColor.secondaryTextColorLight = UIColor(white: 0, alpha: 0.3)//UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 1.00)
            ZColor.disableColorLight = UIColor(red: 0.82, green: 0.84, blue: 0.89, alpha: 1.00)
            ZColor.seperatorColor = (DeviceType.isIpad) ? UIColor(red: 0.80, green: 0.84, blue: 0.86, alpha: 0.8) :  UIColor(red:0.88, green:0.88, blue:0.89, alpha:1.00)
            ZColor.seperatorColorLight = UIColor(red: 0.89, green: 0.90, blue: 0.93, alpha: 0.5)
            ZColor.tableHeaderTextColor = UIColor(red:0.36, green:0.36, blue:0.36, alpha:1.00)//UIColor(red: 0.45, green: 0.45, blue: 0.45, alpha: 1)
            ZColor.tabBarColor = UIColor(red: 0.98, green: 0.98, blue: 0.98, alpha: 1)
            
        }
    }
  
    
    @available(iOS 13.0, *)
    public static func adjustColorsForIos13()  {
        ZColor.bgColorWhite      =  .setAppearance(dark: UIColor.secondarySystemBackground, light:  UIColor.white)
        ZColor.bgColorGray       =   UIColor.systemGroupedBackground//.setAppearance(dark: UIColor.secondarySystemBackground, light:  UIColor.systemGroupedBackground)
        ZColor.homeBgColor       = .setAppearance(dark: UIColor.secondarySystemBackground, light:  UIColor.white)//.setAppearance(dark: UIColor(red:0.08, green:0.08, blue:0.08, alpha:1.0), light: UIColor(red:0.99, green:0.99, blue:0.99, alpha:1.00))

        ZColor.seperatorColor    =  .setAppearance(dark: UIColor.separator, light: UIColor(red:0.88, green:0.88, blue:0.89, alpha:1.00))
        
        ZColor.ipadListSeperatorColor    =  UIColor.setAppearance(dark: UIColor.opaqueSeparator.withAlphaComponent(0.35), light: UIColor.opaqueSeparator.withAlphaComponent(0.24))
        
        ZColor.seperatorColorLight = UIColor.opaqueSeparator//.setAppearance(dark:  UIColor.opaqueSeparator, light: UIColor(red: 0.89, green: 0.90, blue: 0.93, alpha: 0.5))
        ZColor.tintColor        =  .setAppearance(dark: ZColor.whiteColor, light: ZColor.blueColor)
        ZColor.listSelectionColor = .setAppearance(dark: UIColor(red:0.22, green:0.23, blue:0.24, alpha:1.00),light: UIColor(red: 0.86, green: 0.91, blue: 0.97, alpha: 1.00))
        
        
        ZColor.primaryTextColor         =  UIColor.label
        ZColor.primaryTextColorLight    =  UIColor.label
        ZColor.secondaryTextColorDark   =   UIColor.secondaryLabel
        ZColor.secondaryTextColorLight  =   UIColor.tertiaryLabel
        ZColor.disableColorLight        =   UIColor(red: 0.82, green: 0.84, blue: 0.89, alpha: 1.00)
        
        ZColor.tableHeaderTextColor = UIColor(red:0.36, green:0.36, blue:0.36, alpha:1.00)
        ZColor.tabBarColor          = .setAppearance(dark: UIColor.systemGroupedBackground, light: UIColor(red: 0.98, green: 0.98, blue: 0.98, alpha: 1))
        
        ZColor.suggestionViewBgColor     = UIColor.tertiarySystemBackground

        ZColor.ipadRightSideBarColor     = .setAppearance(dark: UIColor.tertiarySystemGroupedBackground, light: UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.00))
        
        
        
        ZColor.shimmeringGridBackgroundColor = .setAppearance(dark: .systemGray5, light: .groupTableViewBackground)
        ZColor.shimmeringListBackgroundColor = .setAppearance(dark: .tertiarySystemBackground, light: UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 1))
        ZColor.shimmeringGridViewColor = .setAppearance(dark: UIColor.systemGray4.withAlphaComponent(0.6) ,light: UIColor(red: 0.90, green: 0.90, blue: 0.94, alpha: 0.9))
        ZColor.shimmeringCollectionViewColor = .setAppearance(dark: .systemGray4, light: UIColor(red: 0.87, green: 0.89, blue: 0.93, alpha: 1))
        ZColor.shimmeringCollectionAndTableBackgroundColor = .setAppearance(dark: .tertiarySystemBackground, light: UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1))
        ZColor.shimmeringDraftBackgroundColor = .setAppearance(dark: .systemGray5 , light: UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 0.5))
        ZColor.shimmeringIpadDocslistBackgroundColor = .setAppearance(dark: .systemGray5, light:#colorLiteral(red: 0.9264881015, green: 0.9266433716, blue: 0.9264676571, alpha: 0.8137929137))
        
        ZColor.tableHeaderBgColor = .setAppearance(dark: UIColor(red:0.19, green:0.20, blue:0.20, alpha:1.00), light: UIColor(red:0.89, green:0.90, blue:0.90, alpha:1.00))
        //Mac
        ZColor.ipadLeftSideBarColor = .setAppearance(dark: UIColor.secondarySystemBackground, light:  UIColor.white)

        if DeviceType.isMac{
            ZColor.ipadLeftSideBarColor = nil
            ZColor.bgColorGray = .setAppearance(dark: UIColor(red:0.16, green:0.16, blue:0.18, alpha:1.00), light:  UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.00))
            ZColor.ipadRightSideBarColor     = .setAppearance(dark: UIColor(red:0.12, green:0.13, blue:0.13, alpha:1.00), light: UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.00))
            ZColor.ipadListSeperatorColor    =  UIColor.setAppearance(dark: UIColor.opaqueSeparator.withAlphaComponent(0.04), light: UIColor.opaqueSeparator.withAlphaComponent(0.24))

        }
    }
}


