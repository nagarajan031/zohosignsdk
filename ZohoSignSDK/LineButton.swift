//Copyright (c) 2018 pikachu987 <pikachu77769@gmail.com>
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

import UIKit

// Called when the button's highlighted is false.
protocol LineButtonDelegate: class {
    func lineButtonUnHighlighted()
    func panGestureTriggered(forButtonType type: ButtonLineType, withGesture gesture: UIPanGestureRecognizer)
    func pinchGestureTriggered(withScale scale: CGFloat)
}

// Side, Edge LineButton
class LineButton: UIButton {
    weak var delegate: LineButtonDelegate?
    
    internal var type: ButtonLineType
    
    var oldDistance: CGFloat = 0
    var isPinchActive = false
    
    public var isGestureActive: Bool = false {
        willSet {
            if !newValue {
                self.delegate?.lineButtonUnHighlighted()
            }
        }
    }
    
    // MARK: Init
    init(_ type: ButtonLineType) {
        self.type = type
        super.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        
        self.setTitle(nil, for: .normal)
        self.translatesAutoresizingMaskIntoConstraints = false
        if type != .center {
            self.widthConstraint(constant: 50)
            self.heightConstraint(constant: 50)
            self.alpha = 0
        }
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureTriggered(_:)))
        addGestureRecognizer(panGesture)
    }
    
    @objc private func panGestureTriggered(_ gesture: UIPanGestureRecognizer) {
        if gesture.numberOfTouches == 2 && type == .center{
            switch gesture.state {
            case .began,.changed:
                isPinchActive = true
                let pointA = gesture.location(ofTouch: 0, in: gesture.view)
                let pointB = gesture.location(ofTouch: 1, in: gesture.view)
                let distance = sqrt(pow(pointA.x - pointB.x, 2) + pow(pointA.y - pointB.y, 2))
                var scale: CGFloat = 1
                let scaleFactor: CGFloat = DeviceType.isMac ? 1.0 : 1.03
                if oldDistance != 0 {
                    scale = distance/oldDistance
                    scale = scale > 1.0 ? scale * scaleFactor : scale/scaleFactor
                }
                
                oldDistance = distance
                delegate?.pinchGestureTriggered(withScale: scale)
            case .ended,.cancelled,.failed:
                oldDistance = 0
                isPinchActive = false
            default:
                break
            }
        } else {
            switch gesture.state {
            case .began:
                isGestureActive = true
                alpha = 0.6
            case .changed:
                if !isPinchActive {
                    delegate?.panGestureTriggered(forButtonType: type, withGesture: gesture)
                }
            case .ended,.failed,.cancelled:
                isGestureActive = false
                alpha = 1.0
                oldDistance = 0
                isPinchActive = false
            default:
                break
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func edgeLine(_ color: UIColor?) {
        self.setImage(self.type.view(color)?.imageWithView?.withRenderingMode(.alwaysOriginal), for: .normal)
    }
}

extension LineButton: UIGestureRecognizerDelegate {
    
    /*
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer is UIPinchGestureRecognizer && otherGestureRecognizer is UIPanGestureRecognizer {
            return true
        }
        return false
    }
    */
    
    /*
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer is UIPinchGestureRecognizer && otherGestureRecognizer is UIPanGestureRecognizer {
            return true
        }
        return false
    }
    */
    
}

enum ButtonLineType {
    case center
    case leftTop, rightTop, leftBottom, rightBottom, top, left, right, bottom
    
    var rotate: CGFloat {
        switch self {
        case .leftTop:
            return 0
        case .rightTop:
            return CGFloat.pi/2
        case .rightBottom:
            return CGFloat.pi
        case .leftBottom:
            return CGFloat.pi/2*3
        case .top:
            return 0
        case .left:
            return CGFloat.pi/2*3
        case .right:
            return CGFloat.pi/2
        case .bottom:
            return CGFloat.pi
        case .center:
            return 0
        }
    }
    
    var yMargin: CGFloat {
        switch self {
        case .rightBottom, .bottom:
            return 1
        default:
            return 0
        }
    }
    
    var xMargin: CGFloat {
        switch self {
        case .leftBottom:
            return 1
        default:
            return 0
        }
    }
    
    func view(_ color: UIColor?) -> UIView? {
        var view: UIView?
        if self == .leftTop || self == .rightTop || self == .leftBottom || self == .rightBottom {
            view = ButtonLineType.EdgeView(self, color: color)
        } else {
            view = ButtonLineType.SideView(self, color: color)
        }
        view?.isOpaque = false
        view?.tintColor = color
        return view
    }
    
    class LineView: UIView {
        var type: ButtonLineType
        var color: UIColor?
        init(_ type: ButtonLineType, color: UIColor?) {
            self.type = type
            self.color = color
            super.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        }
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        func apply(_ path: UIBezierPath) {
            var pathTransform  = CGAffineTransform.identity
            pathTransform = pathTransform.translatedBy(x: 25, y: 25)
            pathTransform = pathTransform.rotated(by: self.type.rotate)
            pathTransform = pathTransform.translatedBy(x: -25 - self.type.xMargin, y: -25 - self.type.yMargin)
            path.apply(pathTransform)
            path.closed()
                .strokeFill(self.color ?? .white)
        }
    }
    
    class EdgeView: LineView {
        override func draw(_ rect: CGRect) {
            let path = UIBezierPath()
                .move(6, 6)
                .line(6, 20)
                .line(8, 20)
                .line(8, 8)
                .line(20, 8)
                .line(20, 6)
                .line(6, 6)
            self.apply(path)
        }
    }
    class SideView: LineView {
        override func draw(_ rect: CGRect) {
            let path = UIBezierPath()
                .move(15, 6)
                .line(35, 6)
                .line(35, 8)
                .line(15, 8)
                .line(15, 6)
            self.apply(path)
        }
    }
}
