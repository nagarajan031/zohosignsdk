//
//  ZRadioButton.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 27/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZRadioButton: UIButton {
    private let outerCircleView = UIView()
    private let innerCicleView = UIView()
    
    var outerCircleViewBackgroundColor: UIColor = .clear {
        didSet {
            setAppearance()
        }
    }
    var borderColor: UIColor = ZColor.blueColor {
        didSet {
            setAppearance()
        }
    }
    var borderWidth: CGFloat {
        return frame.width * 0.1
    }
    
    var innerCicleViewGap: CGFloat {
        return frame.width * 0.1
    }
    
    private var innerCircleViewHeight: CGFloat {
        return frame.height - innerCicleViewGap*2 - borderWidth*2
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        outerCircleView.isUserInteractionEnabled = false
        innerCicleView.clipsToBounds = true
        innerCicleView.isUserInteractionEnabled = false
        innerCicleView.alpha = 0
        
        addSubviews(outerCircleView,innerCicleView)
        
        outerCircleView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        innerCicleView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.width.equalTo(innerCircleViewHeight)
        }
        
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        setAppearance()
    }
    
    public override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setAppearance()
    }
    
    func setAppearance() {
        outerCircleView.backgroundColor = outerCircleViewBackgroundColor
        outerCircleView.layer.borderColor = borderColor.cgColor
        outerCircleView.layer.borderWidth = borderWidth
        innerCicleView.backgroundColor = borderColor
        outerCircleView.clipsToBounds = true
        innerCicleView.frame.size = CGSize(withSide: innerCircleViewHeight)
        innerCicleView.center = outerCircleView.center
        outerCircleView.layer.cornerRadius = frame.height / 2
        innerCicleView.layer.cornerRadius = innerCicleView.frame.height/2
    }
    
    public override var isSelected: Bool {
        willSet {
            UIView.animate(withDuration: 0.3) {
                self.innerCicleView.alpha = newValue.toNumber()
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
