//
//  ZSPersonalDetailController.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 07/08/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit


enum ZSProfileFieldType {
    case textField
    case textbutton
    case imagebutton
}

struct ZSProfileField {
    var key  : String?
    var displayName  : String?
    var value  : Any?
    var type   : ZSProfileFieldType = .textField
    
    var isEditable    = true
    var isModified    = false
    var isMandatory  = false
    var textContentType : UITextContentType?

}



public class ZSPersonalDetailController: UITableViewController {
    deinit {
        ZInfo("Personal page deinit")
    }
    var profileDataArray : [ZSProfileField] = []
    let dateFormatter       =   DateFormatter()
    let currentDate         =   Date()
    var signatureManager  :SignatureManager!
    let dataManager         =   UserRequestManager()
    
    var signatureField        =   ZSProfileField()
    var initialField          =   ZSProfileField()
    var lastNameField         =   ZSProfileField()
    var firstNameField        =   ZSProfileField()
    var emailField            =   ZSProfileField()
    var companyField          =   ZSProfileField()
    var jobTitleField         =   ZSProfileField()
    var dateFormatField       =   ZSProfileField()
    var timeZoneField         =   ZSProfileField()
    
    var isModified    = false

    public var isModalPresent      =   false
    public var isRefreshProfileData =  false
    public var isThirdPartySDK   =  false

    var showUpgradeBtn = false

    var isGuestSign = false
    
    public var upgradeButtonTapHandler : (() -> Void)?
    var slideInTransitioningDelegate: PresentationManager = PresentationManager()

    let currentUser = UserManager.shared.currentUser
    // MARK: - View life cycles

    override public func viewDidLoad() {
        super.viewDidLoad()
        
        ZSignKit.shared.isDynamicFontSupportNeeded = isThirdPartySDK
        
        signatureManager =  SignatureManager(rootViewController: self, signatureType: .signature)
        signatureManager.isThirdPartySDK        =   isRefreshProfileData;
        signatureManager.delegate               =   self
    }

    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParent {
            self.view.endEditing(true)
            saveChanges()
        }
        ZSProgressHUD.dismiss()
        
//        ZSignKit.shared.setNightMode(!ZColor.isNightModeOn)
        super.viewWillDisappear(animated)
    }
    
    
    override public func loadView() {
        super.loadView()
        
        
        self.tableView.backgroundColor  =   ZColor.bgColorGray
        self.tableView.rowHeight        =   60
        self.tableView.tableFooterView  =   UIView()
        tableView.keyboardDismissMode   =  .onDrag
        tableView.setSeparatorStyle()
        
        self.view.backgroundColor  =   ZColor.bgColorGray
        
        tableView.register(ZTextFieldCell.self, forCellReuseIdentifier: "TextCell")
        tableView.register(ZImageCell.self, forCellReuseIdentifier: "ImageEditorCell")
        
        
        self.title      =   "sign.mobile.settings.personalDetails".ZSLString
        
        if isModalPresent {
            self.navigationItem.leftBarButtonItem    =    ZSBarButtonItem.barButton(type: .close, target: self, action: #selector(dismissVC))
        }
        
        if isRefreshProfileData {
            
            ZSProgressHUD.show(inView: (self.view)!)
            
            weak var weakself = self
            dataManager.getCurrentUserDetails(success: { (responseObj) in
                ZSProgressHUD.dismiss()

                guard let responseObject = responseObj as? [String : AnyObject] else {
                        return
                }
                
                if(responseObject["account_id"] != nil){
                    UserManager.shared.updateCurrentUser(dict: responseObject)
                    weakself?.refreshListData()
                }
                
            }) { (error) in
                ZSProgressHUD.dismiss()
                weakself?.handleDataManagererror(error)
            }

        }
        else{
            refreshListData()
//            self.createHeaderView()
        }
    }
    
    
    @objc func dismissVC() {
        
        self.view.endEditing(true)
        saveChanges()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func profileUpdationHandling()
    {
       isModified = true
        UserManager.shared.saveUser(updateInServer: true)
    }
    
    func createHeaderView()  {
        var remainingDoc = ((currentUser?.numberOfTotalDocumentsQouta ?? 0) - (currentUser?.numberOfUsedDocuments ?? 0))
        if (remainingDoc > (currentUser?.numberOfTotalDocumentsQouta ?? 5)){
            remainingDoc = (currentUser?.numberOfTotalDocumentsQouta ?? 5)
        }
        
        if remainingDoc == -1 {
            return
        }
//        remainingDoc = 0
        
        showUpgradeBtn =  (remainingDoc == 0)
        
        var profileHeight : CGFloat = 71
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: CGFloat (profileHeight + 30)))
        
        
        let profileView = UIView(frame: CGRect(x: -1, y: 30, width: view.frame.size.width + 2, height: 71))
        profileView.layer.borderColor = ZColor.seperatorColor.cgColor
        profileView.layer.borderWidth = 0.3
        profileView.backgroundColor = ZColor.bgColorWhite
        headerView.addSubview(profileView)
        
       
        let nameLabel = UILabel(frame: CGRect(x: 20, y: 15, width: view.frame.size.width - 32 , height: 20))
        nameLabel.text = currentUser?.fullName
        nameLabel.textColor = ZColor.primaryTextColor
        nameLabel.font =   ZSFont.titleFont()
        nameLabel.adjustsFontSizeToFitWidth = true
        nameLabel.clipsToBounds = false
        profileView.addSubview(nameLabel)
        
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.top.equalTo(15)
            make.right.equalToSuperview().offset(-122)
            make.height.equalTo(20)
        }
        nameLabel.sizeToFit()
        nameLabel.layoutIfNeeded()

        let countLabel = UILabel(frame: CGRect(x: 20, y: 40, width: view.frame.size.width - 32 , height: 20))
        countLabel.textColor =  ZColor.secondaryTextColorDark
        countLabel.font =   ZSFont.bodyFont()
        countLabel.adjustsFontSizeToFitWidth = true
        profileView.addSubview(countLabel)
        countLabel.clipsToBounds = false
        countLabel.text = String(format: "%d %@", remainingDoc,"sign.mobile.account.documentsLeft".ZSLString.lowercased())
        countLabel.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.top.equalTo(nameLabel.frame.maxY + 3)
            make.right.equalToSuperview().offset(-122)
            make.height.equalTo(18)
        }
        countLabel.sizeToFit()
      
        
        profileHeight = countLabel.frame.maxY + 15
        
        
        let seperatorView = UIView(frame: CGRect(x: 20, y: profileHeight, width: tableView.frame.size.width, height: 0.3))
        seperatorView.backgroundColor = ZColor.seperatorColor
        profileView.addSubview(seperatorView)
              
        headerView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: CGFloat (profileHeight + 30))
        
        tableView.tableHeaderView = headerView

        headerView.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.height.equalTo(profileHeight + 30)
        }
        profileView.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.height.equalTo(profileHeight + 1)
            make.top.equalTo(30)
        }
        

        
        if showUpgradeBtn {
            let updateBtn = ZSButton(frame: CGRect(x:view.frame.width - 118 , y: 91 , width: 100, height: 34))
            updateBtn.setTitle("sign.mobile.iap.upgrade".ZSLString, for: .normal)
            updateBtn.setTitleColor(ZColor.tintColor, for: .normal)
            updateBtn.titleLabel?.font  = ZSFont.regularFontLarge()
            updateBtn.layer.cornerRadius = 4
            updateBtn.autoresizingMask = .flexibleLeftMargin
            updateBtn.layer.borderWidth = 1
            updateBtn.isHidden = !showUpgradeBtn
            updateBtn.layer.borderColor = ZColor.tintColor.cgColor
            updateBtn.addTarget(self, action: #selector(showUpgradeOption), for: .touchUpInside)
            profileView.addSubview(updateBtn)
            
            updateBtn.snp.makeConstraints { (make) in
                make.width.equalTo(100)
//                make.top.equalTo(23)
                make.centerY.equalToSuperview()
                make.right.equalToSuperview().offset(-18)
                make.height.equalTo(35)
            }
        }
      
       
        
       
        
        
        
        
        
        /*if (hideDocCountBtn){
            seperatorView.isHidden = true
            countLabel.isHidden = true
            docCountLabel.isHidden = true
            docLeftLabel.isHidden = true
            profileView.clipsToBounds = true
        }*/
        
    }
    
    @objc public func refreshListData(){
        isGuestSign  = (currentUser?.userType == .guestUser)
        dateFormatter.dateFormat    =   currentUser?.dateFormat

        profileDataArray.removeAll()
        
        
        signatureField.key          =   "signature"
        signatureField.displayName  =   "sign.mobile.field.signature".ZSLString
        signatureField.value        =   currentUser?.getSignatureImage()
        signatureField.type         =   .imagebutton
        

        initialField.key            =   "initial"
        initialField.displayName    =   "sign.mobile.field.initial".ZSLString
        initialField.value          =   currentUser?.getInitialImage()
        initialField.type         =   .imagebutton
        
        firstNameField.key          =   "firstName"
        firstNameField.displayName  =   "sign.mobile.profile.firstName".ZSLString
        firstNameField.value        =   (currentUser?.firstName == nil) ? "-" : currentUser?.firstName
        firstNameField.type         =   .textField
        firstNameField.isEditable   =   (currentUser?.userType != .loggedInSignUser)
        firstNameField.textContentType = .givenName

        lastNameField.key           =   "lastName"
        lastNameField.displayName   =   "sign.mobile.profile.lastName".ZSLString
        lastNameField.value         =   (currentUser?.lastName == nil) ? "-" : currentUser?.lastName
        lastNameField.type         =   .textField
        lastNameField.isEditable   =   (currentUser?.userType != .loggedInSignUser)
        lastNameField.textContentType = .familyName

        emailField.key              =   "email"
        emailField.displayName      =   "sign.mobile.profile.email".ZSLString
        emailField.value            =   (currentUser?.email == nil) ? "-" : currentUser?.email
        emailField.type             =   .textField
        firstNameField.isEditable   =   (currentUser?.userType != .loggedInSignUser)
        emailField.textContentType  =   .emailAddress

        companyField.key            =   "company"
        companyField.displayName    =   "sign.mobile.profile.company".ZSLString
        companyField.value          =   (currentUser?.company == nil) ? "" :  currentUser?.company
        companyField.type         =   .textField
        companyField.textContentType = .organizationName
        
        jobTitleField.key           =   "jobtitle"
        jobTitleField.displayName    =   "sign.mobile.profile.jobTitle".ZSLString
        jobTitleField.value          =   (currentUser?.jobTitle == nil) ? "" :  currentUser?.jobTitle
        jobTitleField.type         =   .textField
        jobTitleField.textContentType    =    .jobTitle
        
        dateFormatField.key         =   "dateformat"
        dateFormatField.displayName  =   "sign.mobile.profile.dateFormat".ZSLString
        dateFormatField.value        =   dateFormatter.string(from:currentDate)
        dateFormatField.type         =   .textbutton
        
        
        timeZoneField.key           =   "timeZone"
        timeZoneField.displayName   =   "sign.mobile.profile.timeZones".ZSLString
        timeZoneField.value         =   currentUser?.timeZone
        timeZoneField.type         =   .textbutton
        timeZoneField.isEditable   =   false
        
        profileDataArray.append(contentsOf: [signatureField,initialField,firstNameField,lastNameField,emailField,companyField,jobTitleField,dateFormatField])
        
//        if (UserManager.shared.currentUser?.userType == ZS_USER_SIGN) {
            profileDataArray.insert(timeZoneField, at: 5)
//        }
        
        self.tableView.reloadData()
        if isThirdPartySDK {
            createHeaderView()
        }
    }
  
    @objc public func saveChanges()
    {
        let filteredArray = profileDataArray.filter{ $0.isModified == true}
        for field in filteredArray
        {
            if (field.key == "firstName")
            { UserManager.shared.currentUser?.firstName    =   field.value as? String}
            else if (field.key == "lastName")
            { UserManager.shared.currentUser?.lastName    =   field.value as? String}
            else if (field.key == "email")
            { UserManager.shared.currentUser?.email    =   field.value as? String}
            else if (field.key == "company")
            { UserManager.shared.currentUser?.company    =   field.value as? String}
            else if (field.key == "jobtitle")
            { UserManager.shared.currentUser?.jobTitle    =   field.value as? String}
            
        }
        
        if ((UserManager.shared.currentUser?.isUserProfileEdited ?? false) || (filteredArray.count > 0)) {
            UserManager.shared.updateProfileInServer()
        }
    }
    
    @objc func showUpgradeOption()  {
        
        upgradeButtonTapHandler?()

    }
    
    func changeDateFormat()
    {
        var array : [String] = []
        
        for dateFormat in DateManager.availableDateFormats()
        {
            dateFormatter.dateFormat = dateFormat as? String
            array.append(dateFormatter.string(from: currentDate))
        }
        
        dateFormatter.dateFormat = UserManager.shared.currentUser?.dateFormat
        let selectedStr = dateFormatter.string(from: currentDate)
        
        let popup = PopupViewController(title: "sign.mobile.profile.dateFormat".ZSLString, menuItems: PopupDataModel.getDataModel(forStrings: array), selectedItem: PopupDataModel(title: selectedStr))
        popup.shouldEnableScroll = true
        popup.completionHandler = { selectedIndex in
            UserManager.shared.currentUser?.dateFormat = DateManager.availableDateFormats()[selectedIndex]
            self.dateFormatter.dateFormat = UserManager.shared.currentUser?.dateFormat
            
            let newDateStr = self.dateFormatter.string(from: self.currentDate)
            
            self.profileDataArray[8].value    =   newDateStr
            self.profileDataArray[8].isModified    =   true

            self.profileUpdationHandling()
            self.tableView.reloadData()
        }
        
      //  let nav = ZNavigationController(rootViewController: popup, supportedOrient: .portrait)
        popup.modalPresentationStyle = .custom
        
        popup.transitioningDelegate = slideInTransitioningDelegate
        self.navigationController?.present(popup, animated: true, completion: nil)
    }
    
    func goToAccountsPage() {
        let url  = URL(string: "https://accounts.zoho.com/home")
        CommonUtils.openUrlInBrowser(url)
    }
    
    // MARK: - Table view data source
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        if profileDataArray.count == 0 {
            return 0
        }
        return 6
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if profileDataArray.count == 0 {
            return 0
        }
        if section == 2 {
            return (isGuestSign) ? 3 : 4
        }
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if ((indexPath.section == 0) || (indexPath.section == 1)) {
            return TableviewCellSize.signature
        }
        if ((indexPath.section == 3) || (indexPath.section == 4)) {
            return TableviewCellSize.normal
        }
        return UITableView.automaticDimension
    }
    

    public override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "sign.mobile.field.signature".ZSLString
        }else if section == 1 {
            return "sign.mobile.field.initial".ZSLString
        }else if section == 3{
            return "sign.mobile.field.company".ZSLString
        }
        else if section == 4{
            return "sign.mobile.field.jobtitle".ZSLString
        }
        else if section == 5{
            return "sign.mobile.profile.dateFormat".ZSLString
        }
        return ""
    }
    
    
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if (indexPath.section == 0 || indexPath.section == 1) {
            let field  = profileDataArray[indexPath.section]

            let expiry_identifier = "ImageEditorCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: expiry_identifier, for: indexPath) as! ZImageCell
            cell.backgroundColor = ZColor.bgColorWhite

           
            cell.setPlaceholder(text: field.displayName ?? "")
            cell.setImage(field.value as? UIImage)
//            cell.imageView?.image = field.value as? UIImage
            return cell
        }
        else if (indexPath.section == 2)
        {
            let field  = profileDataArray[indexPath.row + 2]

            let identifier = "NonEditableCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier)
            if cell == nil {
                cell =  UITableViewCell(style: .value1, reuseIdentifier: identifier)
            }
            cell?.backgroundColor = ZColor.bgColorWhite
            cell?.textLabel?.text = field.displayName
            cell?.detailTextLabel?.text = field.value as? String
            cell?.selectionStyle =   .none
            if !ZSignKit.shared.isDynamicFontSupportNeeded {
                cell?.textLabel?.font             =       ZSFont.createListKeyFont()
                cell?.textLabel?.textColor        =       ZColor.secondaryTextColorDark
                cell?.detailTextLabel?.font       =       ZSFont.createListValueFont()
                cell?.detailTextLabel?.textColor  =       ZColor.primaryTextColor
            }

            return cell!
        }
        else if ((indexPath.section == 3) || (indexPath.section == 4)){
    
            let row    = indexPath.row + indexPath.section + 3
            let field  = profileDataArray[row]
            let expiry_identifier = "TextCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: expiry_identifier, for: indexPath) as! ZTextFieldCell
            cell.setTextField(placeholder: field.displayName ?? "", value: field.value as? String ?? "", editable: field.isEditable)
            cell.backgroundColor = ZColor.bgColorWhite

            cell.maxLimit = kSINGLELINE_MAX_COUNT    // 255
            if (field.key == "email") { cell.textField.keyboardType =   .emailAddress
                cell.textField.autocapitalizationType = .none
                cell.textField.textContentType = .emailAddress
            }
            else {
                cell.textField.textContentType = field.textContentType
                cell.checkForTextFieldRegex(regex: ZS_Regex_ForName, showPopInView: self.view)
            }
            
            if ZSignKit.shared.isDynamicFontSupportNeeded {
                cell.textField.font = UIFont.preferredFont(forTextStyle: .body)
            }
            
            cell.textFieldEndEditing = { [weak self](textStr) -> Void in
                if ((field.key == "email") && !SwiftUtils.isValidEmail(textStr, showAlertIfFails: true, rootViewController: self))
                {
                    return;
                }
                
                self?.profileDataArray[row].isModified    =   true
                self?.profileDataArray[row].value         =   textStr
                
                self?.profileUpdationHandling()
            }
            
            return cell
        }
        
        
        
        let field  = profileDataArray[indexPath.row + 8]

        
        //dummy
        let identifier = "Cell"
        
        var cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        if cell == nil {
            cell =  UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: identifier)
            
            let selectedView = UIView()
            selectedView.backgroundColor = ZColor.listSelectionColor
            cell?.selectedBackgroundView = selectedView
        }
        
        cell.backgroundColor            =       ZColor.bgColorWhite
        cell.textLabel?.text            =       field.value as? String

        if !ZSignKit.shared.isDynamicFontSupportNeeded {
            cell.textLabel?.font            =       ZSFont.createListValueFont()
            cell.textLabel?.textColor       =       ZColor.primaryTextColor
        }
        
        return cell
    }
    
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if(indexPath.section == 0)
        {
            self.signatureManager.signatureType  =   .signature
            let cell = tableView.cellForRow(at: indexPath)
            let rect = CGRect(x: cell?.center.x ?? 0, y: cell?.center.y ?? 0, width: 0, height: 0 )
            self.signatureManager.showSignatureWizard(fromRect: rect)
        }
        else if(indexPath.section == 1)
        {
            self.signatureManager.signatureType  =   .initial
            let cell = tableView.cellForRow(at: indexPath)
            let rect = CGRect(x: cell?.center.x ?? 0, y: cell?.center.y ?? 0, width: 0, height: 0 )
//            self.signatureManager.showSignatureWizard(from: cell)
            self.signatureManager.showSignatureWizard(fromRect: rect)
        }
        else if(indexPath.section == 5)
        {
            self.changeDateFormat()
        }else{
            let alertController = UIAlertController(title:nil, message: "sign.mobile.alert.defaultFieldsCanBeModifiedInAcccountsPage".ZSLString, preferredStyle: .alert)
            var cancelAction = UIAlertAction(title:( DeviceType.isMac ? "sign.mobile.common.cancel".ZSLString :  "sign.mobile.common.ok".ZSLString), style: .cancel, handler: nil)

            alertController.addAction(cancelAction)
            
            if DeviceType.isMac {
                
                let doneAction = UIAlertAction(title: "sign.mobile.alert.visitAccountsPage".ZSLString, style: .default) { (alert) in
                    self.goToAccountsPage()
                }

                alertController.addAction(doneAction)
            }
            


            present(alertController, animated: true, completion: nil)
            
//            showAlert(withMessage: "sign.mobile.alert.defaultFieldsCanBeModifiedInAcccountsPage".ZSLString)
        }

    }

}


//MARK:-
extension ZSPersonalDetailController : SignatureManagerDelegate{
    public func didSaveIntial(initialImage: UIImage) {
        profileDataArray[1].value = initialImage
        tableView.reloadData()
        self.profileUpdationHandling()
    }
    
    public func didSaveSignature(signImage: UIImage) {
        profileDataArray[0].value = signImage
        tableView.reloadData()
        self.profileUpdationHandling()
    }
}
