import UIKit

public class ZSTextField: UITextField {
    
    //MARK:- Views

    let placeHolderLabel : UILabel = {
        let tempPlaceHolder = UILabel()
        tempPlaceHolder.textColor = ZColor.secondaryTextColorLight
        tempPlaceHolder.font = ZSFont.createListKeyFont()
        return tempPlaceHolder
    }()
    
    //MARK:- Private Variables

    private var placeHolderTopAnchor : NSLayoutConstraint?
    private var placeHolderBottomAnchor : NSLayoutConstraint?
    private var cellMultiplier : CGFloat = 1
    private var placeHolderTopAnchorConstant : CGFloat = 28
    private var placeHolderBottomConstant : CGFloat = 45
    let defaultHeight: CGFloat = 60
    
    let leftPadding : CGFloat = 17
    
    //Internal Variables
    
    var cellHeight : CGFloat? {
        willSet{
            cellMultiplier = (newValue!)/defaultHeight
            initalizeTextField()
        }
    }
    
    override public var text: String?{
        willSet {
            if text != "" || text != nil
            {
                if newValue != "" && newValue != nil
                {
                    self.placeHolderTopAnchor?.constant = (cellMultiplier * 0)
                    self.placeHolderBottomAnchor?.constant = (cellMultiplier * 34)
                    self.placeHolderLabel.font = ZSFont.createListKeyFontSmall()
                    self.layoutIfNeeded()
                }
            }
        }
    }
    
    var placeHolderColor : UIColor?{
        willSet {
            placeHolderLabel.textColor = newValue
        }
    }

    //MARK:- Padding Variables

    private lazy var padding : UIEdgeInsets = {
        let tempPadding = UIEdgeInsets(top: (leftPadding * cellMultiplier) , left: 0, bottom: 0, right: 0)
        return tempPadding
    }()
    
    @objc public func setPlaceHolder(_ placeHolder: String , isMandatory : Bool){
        if isMandatory {
            let mutableString = NSMutableAttributedString(string: String(format: "%@ *", placeHolder))
            mutableString.setColorForText("*", with: .red)
            self.placeHolderLabel.attributedText = mutableString
        }else{
            self.placeHolderLabel.text = placeHolder
        }
//        self.placeHolderLabel.text = placeHolder

    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets.init(top: leftPadding, left: 0.5, bottom: 0, right: leftPadding))
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets.init(top: leftPadding, left: 0, bottom: 0, right: leftPadding))
    }
    
    override open func clearButtonRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.size.width - 20, y: 29, width: 20, height: 20)
    }
    
    //MARK:- Intializer Functions

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initalizeTextField()
    }

    required override public init(frame: CGRect) {

        super.init(frame: frame)
        self.frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.size.width, height: (defaultHeight * cellMultiplier))
        initalizeTextField()

    }

    func initalizeTextField(){

        for aConstraint in placeHolderLabel.constraints
        {
            placeHolderLabel.removeConstraint(aConstraint)
        }
        placeHolderLabel.removeFromSuperview()
        
        self.textColor = ZColor.primaryTextColor
        
        self.font = ZSFont.createListValueFont()
        
        placeHolderTopAnchorConstant = (28 * cellMultiplier)
        placeHolderBottomConstant = (45 * cellMultiplier)

        placeHolderLabel.textAlignment = NSTextAlignment.left
        placeHolderLabel.textColor = ZColor.secondaryTextColorDark
        addSubview(placeHolderLabel)
        
        placeHolderLabel.translatesAutoresizingMaskIntoConstraints = false
        placeHolderTopAnchor = placeHolderLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: placeHolderTopAnchorConstant)
        placeHolderTopAnchor?.isActive = true
        placeHolderBottomAnchor = placeHolderLabel.bottomAnchor.constraint(equalTo: self.topAnchor, constant: placeHolderBottomConstant)
        placeHolderBottomAnchor?.isActive = true
        placeHolderLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        placeHolderLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true

    }

    //MARK:- Editing Functions
    
    override open func becomeFirstResponder() -> Bool {
        let result = super.becomeFirstResponder()
        self.textFieldDidBeginEditing()
        return result
    }
    
    override open func resignFirstResponder() -> Bool {
        let result =  super.resignFirstResponder()
        self.textFieldDidEndEditing()
        return result
    }
    
    func textFieldDidBeginEditing()
    {
        if self.text == "" || self.text == nil
        {
            UIView.animate(withDuration: 0.3, animations: {
                self.placeHolderTopAnchor?.constant = (0 * self.cellMultiplier)
                self.placeHolderBottomAnchor?.constant = (34 * self.cellMultiplier)
                self.placeHolderLabel.font = ZSFont.createListKeyFontSmall()
                self.layoutIfNeeded()
            },completion: nil)
        }
    }
    


    func textFieldDidEndEditing()
    {
        if self.text == "" || self.text == nil
        {
            UIView.animate(withDuration: 0.3, animations: {
                self.placeHolderTopAnchor?.constant = self.placeHolderTopAnchorConstant
                self.placeHolderBottomAnchor?.constant = self.placeHolderBottomConstant
                self.placeHolderLabel.font = ZSFont.createListKeyFont()
                self.layoutIfNeeded()
            },completion: nil)
        }
    }
}
