//
//  ZWebviewCell.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 28/08/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation
import WebKit

public class ZWebviewCell: UITableViewCell {
        
    fileprivate let textview = UITextView()
    let keyLabel = UILabel()
    public var urlDidClicked: ((URL) -> Void)?
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initial setup
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        //fatalError("init(coder:) has not been implemented")
    }
    
    
    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func initialSetup(){
        
        keyLabel.font              =       ZSFont.createListKeyFont();
        keyLabel.textColor         =       ZColor.secondaryTextColorDark;
        keyLabel.backgroundColor   =       .clear;
        keyLabel.numberOfLines     =       1;
        self.contentView.addSubview(keyLabel)

        keyLabel.snp.makeConstraints { (make) in
            make.left.equalTo(16)
            make.top.equalTo(13)
            make.right.equalToSuperview().offset(16)
            make.height.equalTo(15)
        }
        
        self.contentView.backgroundColor = ZColor.bgColorWhite
        self.contentView.autoresizingMask = .flexibleHeight
      
        textview.backgroundColor = ZColor.bgColorWhite
        textview.isEditable = false
        textview.isScrollEnabled = false
        textview.textContainerInset = .zero
        textview.dataDetectorTypes = .all
        textview.linkTextAttributes = [
            .foregroundColor: ZColor.blueColor,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        self.contentView.addSubview(textview)
    }

    override public func layoutSubviews() {
        super.layoutSubviews()

        textview.frame = CGRect(x: (detailTextLabel?.frame.origin.x ?? 16) - 5, y: detailTextLabel?.frame.origin.y ?? 34, width: (detailTextLabel?.frame.width ?? self.bounds.size.width)  + 10, height: detailTextLabel?.frame.height ?? self.bounds.size.height)//
        //detailTextLabel?.frame ?? CGRect(x: 11, y: 34, width: self.bounds.size.width - 26, height: self.bounds.size.height - 38)
    }
    
    public func setCellValues(keyString : String? ,htmlValue : NSAttributedString)
    {
        textLabel?.text       =   keyString
        textLabel?.textColor  =   ZColor.secondaryTextColorDark
        textLabel?.font        =   ZSFont.createListKeyFont()
        detailTextLabel?.attributedText  = htmlValue
        detailTextLabel?.numberOfLines = 0
        
        selectionStyle        =   .none
        backgroundColor       =   ZColor.bgColorWhite
        
        detailTextLabel?.isHidden = true
//
//        keyLabel.text = keyString
        textview.attributedText = htmlValue
        textview.delegate = self
        self.setNeedsLayout()
    }
    

    
}

extension ZWebviewCell: UITextViewDelegate {
    public func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        urlDidClicked?(URL)
        return false
    }
}
