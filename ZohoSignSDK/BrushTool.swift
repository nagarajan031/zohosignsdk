//
//  BrushTool.swift
//  SignatureView
//
//  Created by somesh-8758 on 13/02/20.
//  Copyright © 2020 Somesh-8758. All rights reserved.
//

import UIKit

class BrushTool: SketchTool {
    
    private var sketchAngle: CGFloat?
    private var lastLeftPoint: CGPoint?
    private var lastRightPoint: CGPoint?
    
    private var xOffset: CGFloat = 0
    private var yOffset: CGFloat = 0
    
    
    override var _lineWidth: CGFloat {
        willSet {
            xOffset = newValue/2
            yOffset = newValue/2
        }
    }
    
    override func draw() {
        guard let context = UIGraphicsGetCurrentContext() else {return}
        context.setLineCap(.butt)
        context.setFillColor(lineColor.cgColor)
        context.setBlendMode(.multiply)
        context.setAlpha(lineAlpha)
        
        subPaths.forEach { (sketchPath) in
            context.addPath(sketchPath.path)
            context.fillPath()
        }
    }
    
    override func touchesBegin(_ point: CGPoint, with force: CGFloat) {
        counter = 0
        controlPoints[0] = point
    }
    
    @discardableResult
    override func touchesMoved(_ point: CGPoint, with force: CGFloat) -> CGRect {
        var bounds: CGRect = .zero
        if controlPoints[counter].fuzzyEqual(with: point, by: 2.0) {
            return bounds
        }
        
        if sketchAngle.isNull {
            self.calculateSketchAngle(forPreviousPoint: controlPoints[counter], withCurrentPoint: point)
        }
        
        counter+=1
        controlPoints[counter] = point
        
        if counter == 2 {
            bounds = drawPath(withPreviousPreviousPoint: controlPoints[0], previousPoint: controlPoints[1], currentPoint: controlPoints[2], force: force)
            
            (controlPoints[0],controlPoints[1]) = (controlPoints[1],controlPoints[2])
            counter = 1
        }
        
        return bounds
    }
    
    @discardableResult
    override func touchesEnded(_ point: CGPoint, with force: CGFloat) -> CGRect {
        counter = 0
        return .zero
    }
    
    override func showPreview(_ points: [CGPoint]) {
        if points.count > 2 {
            var p2Point = points[0]
            var p1Point = points[1]
            
            self.sketchAngle = 320
            calculateOffset(for: sketchAngle!, withPreviousPoint: p2Point, andCurrentPoint: p1Point)
            
            for point in points[2..<points.count] {
                drawPath(withPreviousPreviousPoint: p2Point, previousPoint: p1Point, currentPoint: point, force: 0.0)
                p2Point = p1Point
                p1Point = point
            }
        }
    }
    
    @discardableResult
    private func drawPath(withPreviousPreviousPoint p2Point: CGPoint, previousPoint p1Point: CGPoint, currentPoint: CGPoint, force: CGFloat) -> CGRect {
        let xOffset,yOffset: CGFloat
        let forceXOffset = force * self.xOffset + self.xOffset
        let forceYOffset = force * self.yOffset + self.yOffset
        
        if let sketchAngle = sketchAngle {
            if (sketchAngle < 90 && sketchAngle >= 0) || (sketchAngle >= 180 && sketchAngle < 270) {
                xOffset = forceXOffset
                yOffset = -forceYOffset
            } else {
                xOffset = forceXOffset
                yOffset = forceYOffset
            }
            
            let subPath = drawQuad(withPreviousPreviousPoint: p2Point, previousPoint: p1Point, currrentPoint: currentPoint, xOffset: xOffset, yOffset: yOffset)
            
            let sketchPath = SketchPath(path: subPath, force: force)
            subPaths.append(sketchPath)
            return subPath.boundingBox
        }
        
        return .zero
    }
    
    private func drawQuad(withPreviousPreviousPoint p2Point: CGPoint, previousPoint p1Point: CGPoint, currrentPoint: CGPoint, xOffset: CGFloat, yOffset: CGFloat) -> CGMutablePath{
        let mid1 = p1Point / p2Point
        let mid2 = currrentPoint / p1Point
        
        let topLeftPoint, topRightPoint: CGPoint
        
        if let lastLeftPoint = lastLeftPoint, let lastRightPoint = lastRightPoint {
            topLeftPoint = lastLeftPoint
            topRightPoint = lastRightPoint
        } else {
            topLeftPoint = CGPoint(x: mid1.x+xOffset, y: mid1.y+yOffset)
            topRightPoint = CGPoint(x: mid1.x-xOffset, y: mid1.y-yOffset)
        }
        
        let controlLeftPoint = CGPoint(x: p1Point.x+xOffset, y: p1Point.y+yOffset)
        let controlRightPoint = CGPoint(x: p1Point.x-xOffset, y: p1Point.y-yOffset)
        
        let bottomLeftPoint = CGPoint(x: mid2.x+xOffset, y: mid2.y+yOffset)
        let bottomRightPoint = CGPoint(x: mid2.x-xOffset, y: mid2.y-yOffset)
        
        lastLeftPoint = bottomLeftPoint
        lastRightPoint = bottomRightPoint
        
        let subPath = CGMutablePath()
        subPath.move(to: topLeftPoint)
        subPath.addQuadCurve(to: bottomLeftPoint, control: controlLeftPoint)
        subPath.addLine(to: bottomRightPoint)
        subPath.addQuadCurve(to: topRightPoint, control: controlRightPoint)
        subPath.addLine(to: topLeftPoint)
        
        return subPath
    }
    
    private func calculateSketchAngle(forPreviousPoint previousPoint: CGPoint, withCurrentPoint currentPoint: CGPoint) {
        
        self.sketchAngle = pointPairToBearingDegrees(previousPoint, endingPoint: currentPoint)
        calculateOffset(for: sketchAngle!, withPreviousPoint: previousPoint, andCurrentPoint: currentPoint)
    }
    
    private func pointPairToBearingDegrees(_ startingPoint: CGPoint, endingPoint: CGPoint) -> CGFloat{
        let originalPoint = endingPoint - startingPoint
        let bearingRadians = atan2(originalPoint.y, originalPoint.x)
        
        var bearingDegrees = bearingRadians.degrees()
        bearingDegrees = bearingDegrees > 0.0 ? bearingDegrees : 360 + bearingDegrees
        return bearingDegrees
    }
    
    private func calculateOffset(for angle: CGFloat, withPreviousPoint point: CGPoint, andCurrentPoint currentPoint: CGPoint) {
        let processedAngle = self.processedAngle(angle)
        var offSet = _lineWidth/2
        var defaultOffset = offSet
        
        while true {
            let xValue = processedAngle * defaultOffset
            let yValue = defaultOffset - xValue
            
            let leftPoint = CGPoint(x: currentPoint.x+xValue, y: currentPoint.y+yValue)
            let rightPoint = CGPoint(x: currentPoint.x-xValue, y: currentPoint.y+yValue)
            
            let distance = leftPoint ~> rightPoint
            if distance > self._lineWidth {
                offSet = defaultOffset
                break
            } else {
                defaultOffset += 0.2
            }
        }
        
        self.xOffset = processedAngle * offSet
        self.yOffset = offSet-self.xOffset
        
    }
    
    func processedAngle(_ angle: CGFloat) -> CGFloat {
        let angle180 = angle.truncatingRemainder(dividingBy: 180)
        let angle90: CGFloat = angle180 > 90 ? 180 - angle180 : angle180
        
        let processedAngle = angle90/90
        return processedAngle
    }
}
