//
//  ZSCoreDataManager+SignRequest.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 02/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import CoreData

extension ZSCoreDataManager {
    
    //MARK: CREATE
    public func addDraftSignRequest(fromDict dict: ResponseDictionary?) -> SignRequest?{
        guard let dict = dict else {return nil}
        var signRequest: SignRequest? = nil
        managedObjContext.performAndWait {
            signRequest = SignRequest(context: managedObjContext)
            updateSignRequest(request: signRequest!, dict: dict)
            save()
        }
        return signRequest!
    }
    
    public func addSignRequests(fromResponseObject object: Any?, forFilter requestFilter: String, shouldRemovePreviousElements shouldRemove: Bool) {
        
        guard let dictArray = object as? [ResponseDictionary] else {return}
        
        managedObjContext.performAndWait {
            let filterFetchRequest: NSFetchRequest<Filter> = Filter.fetchRequest()
            filterFetchRequest.predicate = NSPredicate(format: "isMyReqFilter == %@ && filterKey == %@", NSNumber(value: false), requestFilter)
            
            do {
                let filter = try managedObjContext.fetch(filterFetchRequest).first
                if shouldRemove {
                    filter?.signRequests = nil
                    if SwiftUtils.isIOS11 {
                        save()
                    }
                }
                
                dictArray.forEach { (dict) in
                    guard let requestId: String = dict[keyPath: "request_id"] else {return}
                    
                    let signFetchRequest: NSFetchRequest<SignRequest> = SignRequest.fetchRequest()
                    signFetchRequest.predicate = NSPredicate(format: "requestId == %@", requestId)
                    
                    var signRequest: SignRequest
                    
                    if let request = try? managedObjContext.fetch(signFetchRequest).first{
                        signRequest = request
                    } else {
                        signRequest = SignRequest(context: managedObjContext)
                    }
                    
                    updateSignRequest(request: signRequest, dict: dict, isList: true)
                    filter?.addToSignRequests(signRequest)
                }
                
                save()
            } catch {
                ZError(error)
            }
        }
    }
    
    //MARK: GET
    public func getSignRequest(requestId: String) -> SignRequest? {
        var signReq : SignRequest?
        do {
            let fetchRequest : NSFetchRequest<SignRequest> = SignRequest.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "requestId == %@", requestId)
            
            let fetchedResults = try managedObjContext.fetch(fetchRequest)
            if let aDoc = fetchedResults.first {
                signReq = aDoc
            }else{
                signReq = SignRequest(context: managedObjContext)
            }
            signReq?.requestId = requestId
        }
        catch{ }
        
        return signReq
    }
    
    //MARK: UPDATE
    
    public func updateSignRequest(request : SignRequest,  dict : ResponseDictionary, isList: Bool = false){
        
        let arrayData = NSKeyedArchiver.archivedData(withRootObject: dict["actions"] ?? "")
        request.actions = arrayData

        /*
        if let status = dict["request_status"] as? String {
            if ((request.actions == nil) || (request.status != status) ||
                !isList){
            }
            request.status  = status
        }
        */
        
        request.desc               =   dict[keyPath:"description"] ?? ""
        request.totalPages         =   0
        request.createdTime        =   dict[keyPath:"created_time"] ?? 0
        request.expiryDate         =   dict[keyPath:"expire_by"] ?? 0
        request.isInProgress       =   dict[keyPath:"in_process"] ?? 0
        request.isExpiring         =   dict[keyPath:"is_expiring"] ?? 0
        request.isSequential       =   dict[keyPath:"is_sequential"] ?? 0
        request.isSelfSign         =   dict[keyPath:"self_sign"] ?? 0
        request.isDeletedRequest   =   dict[keyPath:"is_deleted"] ??  false
        request.modifiedTime       =   dict[keyPath:"modified_time"] ?? 0
        request.actionTime         =   dict[keyPath:"action_time"] ?? 0
        request.signSubmissionTime =   dict[keyPath:"sign_submitted_time"] ?? 0
        request.notes              =   dict[keyPath:"notes"] ?? ""
        request.folderId           =   dict[keyPath:"folder_id"]  ?? ""
        request.folderName         =   dict[keyPath:"folder_name"] ?? ""
        request.ownerEmail         =   dict[keyPath:"owner_email"] ?? ""
        request.ownerFirstname     =   dict[keyPath:"owner_first_name"] ?? ""
        request.ownerLastname      =   dict[keyPath:"owner_last_name"] ?? ""
        request.ownerId            =   dict[keyPath:"owner_id"] ?? ""
        request.requestId          =   dict[keyPath:"request_id"] ?? ""
        request.name               =   dict[keyPath:"request_name"] ?? ""
        request.status             =   dict[keyPath:"request_status"] ?? ""
        request.typeId             =   dict[keyPath:"request_type_id"] ?? ""
        request.typeName           =   dict[keyPath:"request_type_name"] ?? ""
        request.signPercentages    =   dict[keyPath:"sign_percentage"] ?? 0
        request.validity           =   dict[keyPath:"validity"] ?? 0
        request.declinedReason     =   dict[keyPath:"decline_reason"] ?? ""
        request.timeToComplete = dict[keyPath: "expiration_days"]
        request.isAutomaticReminderOn = dict[keyPath: "email_reminders"] ?? true
        request.reminderTimePeriod = dict[keyPath: "reminder_period"] ?? 5
        request.documents          =    nil
        
        let num =  DateManager.formatTableviewHeader(request.modifiedTime ?? 0, setTodayasNil: false)
        
        if request.sectionIdentifier != num?.stringValue {
            request.sectionIdentifier = num?.stringValue
        }
        
        var pages: Int16 = 0
        dict[keyPath: "document_ids"].safelyUnwrap { (docDicts: [ResponseDictionary]) in
            docDicts.forEach { (docDict) in
                
                docDict[keyPath: "document_id"].safelyUnwrap { (documentId: String) in
                    let document = addDocument(withDocumentId: documentId, andDataDict: docDict)
                    request.addToDocuments(document)
                    if let imageString = document.thumbnailStr, document.documnetOrder == 0 {
                        request.thumbnailStr = imageString
                        request.thumbnailPath = String(format: "%@/%@.jpg", thumbnailFolderPath, documentId)
                    }
                    pages += document.totalPages
                }
                
            }
        }
    
        request.totalPages = NSNumber(value: pages)
    }
    
    //MARK: DELETE
    func deleteSignRequests(withIds reqIds : String...)  {
        reqIds.forEach { (reqId) in
            let fetchRequest : NSFetchRequest<SignRequest> = SignRequest.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "requestId == %@", reqId)

            do{
               let fetchedResults = try managedObjContext.fetch(fetchRequest)
               for object in fetchedResults {
                   managedObjContext.delete(object)
               }
            }
            catch{ }
        }
        
        coreDataDefault.saveContext()
    }
}
