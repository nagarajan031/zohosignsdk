//
//  ZSBarButtonItem.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 24/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public enum ZSBarButtonType {
    case add
    case back
    case close
    case cancel
    case done
    case edit
    case save
    case send
    case finish
    case share
    case info
    case more
    case filter
    case search
    case next
    case help
    case mail
}



public struct ZSBarButtonItem {
    public static func barButton(type: ZSBarButtonType, target: Any?, action: Selector)-> UIBarButtonItem{
        var barButton: UIBarButtonItem
        switch type {
        case .add:
            barButton = UIBarButtonItem(barButtonSystemItem: .add, target: target, action: action)
        case .edit:
            barButton = UIBarButtonItem(barButtonSystemItem: .edit, target: target, action: action)
        case .close:
            barButton = UIBarButtonItem(title: "sign.mobile.common.close".ZSLString, style: .plain, target: target, action: action)
        case .cancel:
            barButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: target, action: action)
            
        case .done:
            barButton = UIBarButtonItem(barButtonSystemItem: .done, target: target, action: action)
        case .save:
            barButton = UIBarButtonItem(barButtonSystemItem: .save, target: target, action: action)
        case .send:
            barButton = UIBarButtonItem(title: "sign.mobile.common.send".ZSLString, style: .done, target: target, action: action)
        case .finish:
            barButton = UIBarButtonItem(title: "sign.mobile.common.finish".ZSLString, style: .done, target: target, action: action)
        case .share:
            barButton = UIBarButtonItem(barButtonSystemItem: .action, target: target, action: action)
        case .filter:
            
            var icon  = UIImage(named: "nav-icn-filter")
            if #available(iOS 13.0, *) {
                icon = UIImage(systemName: "line.horizontal.3.decrease.circle")
            }
            barButton =  UIBarButtonItem(image: icon?.withRenderingMode(.alwaysTemplate), style: .plain, target: target, action: action)
        case .info:
            barButton = UIBarButtonItem(image: UIImage(named: "info_icon")?.withRenderingMode(.alwaysTemplate), style: .plain, target: target, action: action)
        case .more:
            let menuButton = ZSButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            menuButton.backgroundColor = UIColor.clear
            menuButton.setImage(UIImage(named: "more_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
            menuButton.tintColor = ZColor.buttonColor
            menuButton.imageEdgeInsets = UIEdgeInsets(top: 6, left: 12, bottom: 6, right: 0)
            menuButton.addTarget(target, action: action, for: .touchUpInside)
            barButton = UIBarButtonItem(customView: menuButton)
        case .search:
            barButton = UIBarButtonItem(barButtonSystemItem: .search, target: target, action: action)
        case .next:
            barButton = UIBarButtonItem(title: "sign.mobile.common.next".ZSLString, style: .done, target: target, action: action)
        case .back:
            barButton = UIBarButtonItem(image: UIImage(named: "back_icon")?.withRenderingMode(.alwaysTemplate), style: .plain, target: target, action: action)
        case .help:
            barButton = UIBarButtonItem(title: "sign.mobile.common.help".ZSLString, style: .done, target: target, action: action)
        case .mail:
            barButton = UIBarButtonItem(image: ZSImage.buttonImage(named: "swipe_mail"), style: .done, target: target, action: action)
        }
        

            
        return barButton
        
    }
   
}
