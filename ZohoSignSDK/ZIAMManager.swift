//
//  ZIAMManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 19/09/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

import SSOKit

public typealias getIAMTokenSuccessBlock = (String?) -> Void
public typealias getIAMTokenErrorBlock = (Error?) -> Void
public typealias getIAMPofilePicSuccessBlock = (UIImage?) -> Void
public typealias getIAMLogoutSuccessBlock = () -> Void

public class ZIAMManager: NSObject {

    //MARK:- Inits
    public class func initate(window: UIWindow?) {
        let appGroup = CommonUtils.signAppSharedGroupID()
        var oAuthScopes: [AnyHashable] = []
        var oAuthClientId = ""
        var oAuthAppName = ""
        var oAuthUrlScheme = ""
        var oAuthUrlParams = ""
        var oAuthLoginMode: SSOBuildType = SSOBuildType.Live_SSO
        let userDefaults = UserDefaults.standard

        if !ZSignKit.shared.isAppExtension {
            if !userDefaults.bool(forKey: "appfirstlaunch") {
                ZSSOKit.clearSSODetailsForFirstLaunch()
                userDefaults.set(true, forKey: "appfirstlaunch")
                userDefaults.synchronize()
            }
        }

        oAuthScopes = ["ZohoSign.documents.ALL",
                        "ZohoSign.account.ALL",
                        "ZohoSign.setup.ALL",
                        "ZohoSign.templates.ALL",
                        "ZohoSign.reports.ALL",
                        "ZohoPC.files.ALL",
                        "ZohoPayments.MobileSubscription.READ",
                        "ZohoPayments.MobileSubscription.CREATE",
                        "ZohoPayments.MobileSubscription.UPDATE"]

        oAuthAppName = "Zoho Sign iOS"
        oAuthUrlScheme = "zohosign"
        oAuthUrlParams = ""





        switch ZSignKit.shared.signKitType {
            case .localZoho:
                //                #if IAAPSME_ACCOUNT
                //                oAuthClientId  =  @"1002.IUVDIE8692YH30994P95BD6SQHE313";
                //                #else
                oAuthClientId = "1002.T778UB2USSFP985379YUQ8RIGWZ0C4"
                //                #endif
                oAuthLoginMode = SSOBuildType.Local_SSO_Development
            case .live,.pre:
                oAuthLoginMode = SSOBuildType.Live_SSO
                //                #if IAAPSME_ACCOUNT
                //                oAuthClientId  =  @"1002.E87SHUMQ3GOA497633OS37YA4KKZ6W";
                //                #else
                oAuthClientId = "1002.UPI7PEXOV4KO64029RQAJYVXBUU1U3"
            //                #endif
            case .CSEZ:
                oAuthLoginMode = SSOBuildType.CSEZ_SSO_Dev
                //                #if IAAPSME_ACCOUNT
                //                oAuthClientId  =  "1002.T778UB2USSFP985379YUQ8RIGWZ0C4";
                //                #else
                oAuthClientId = "1002.4RPJWWCWHXC369092JK0O7QT335KFR"
                //                #endif
                /*oAuthScopes  =  ["nishanthzohosign.documents.ALL",
                 "nishanthzohosign.account.ALL",
                 "nishanthzohosign.setup.ALL",
                 "nishanthzohosign.reports.ALL"];*/
                oAuthScopes  =  ["centoszohosign.documents.ALL",
                "centoszohosign.account.ALL",
                "centoszohosign.setup.ALL",
                "centoszohosign.reports.ALL",
                "centoszohosign.templates.ALL"];
                /*
                oAuthScopes  =  ["vageezohosign.documents.ALL",
                                 "vageezohosign.account.ALL",
                                 "vageezohosign.setup.ALL",
                                 "vageezohosign.reports.ALL",
                                 "vageezohosign.templates.ALL"];
                */
            default:
                break
        }
        
        ZSSOKit.setHavingAppExtensionWithAppGroup(appGroup)
        
        if (ZSignKit.shared.isAppExtension) {
            ZSSOKit.initWithClientID(oAuthClientId,
                                     scope: oAuthScopes,
                                     urlScheme: oAuthUrlScheme,
                                     buildType: oAuthLoginMode)

        }
        else{
            ZSSOKit.initWithClientID(oAuthClientId,
                                     scope: oAuthScopes,
                                     urlScheme: oAuthUrlScheme,
                                     mainWindow: window,
                                     buildType: oAuthLoginMode)
            
            /*if(![userDefaults boolForKey:@"scopeEnhanced"]){
                [ZSSOKit  enhanceScopes:^(NSString *accessToken, NSError *error) {
                    [userDefaults setBool:YES forKey:@"scopeEnhanced"];
                    [userDefaults synchronize];
                }];
            }*/
        }
        ZSSOKit.setHavingAppExtensionWithAppGroup(appGroup)
        
        #if targetEnvironment(macCatalyst)
        ZIAMUtil.shared()?.shoulduseASWebAuthenticationSession = true
        #endif

    }

    class func getCurrentLoggedInUser() -> ZSSOUser? {
        return ZSSOKit.getCurrentUser()
    }

    public class func loggedInUserBaseDomain() -> String? {
        if ZSSOKit.getCurrentUser() == nil {
            return "zoho.com"
        }

        let dict = ZSSOKit.getDCLInfoForCurrentUser()
        
        guard let baseDomain = dict?["basedomain"] as? String else{
            return "zoho.com"
        }

        return baseDomain
    }

    public class func isUserLoggedIn() -> Bool {
        return ZSSOKit.isUserSignedIn()
    }
    

    public class func transformedURLString(_ urlStr: String) -> String {
        let transformedUrlStr = ZSSOKit.getTransformedURLString(forURL: urlStr)
        return transformedUrlStr?.replacingOccurrences(of: "https://-", with: "https://") ?? urlStr
    }

    //MARK:- Signup
    public class func getIAMTokenAfterSignup(_ successBlock:@escaping getIAMTokenSuccessBlock,errorBlock: @escaping getIAMTokenErrorBlock) {
        ZIAMManager.logout {
            ZSSOKit.presentSignUpViewController { (accessToken, error) in
                DispatchQueue.main.async {
                    guard let token  = accessToken, (error == nil) else{
                        errorBlock(error)
                        return
                    }
                    successBlock(token)
                }
            }
        }
    }

    //MARK:- Login
    public class func getIAMTokenWithNativeSign(_ successBlock:@escaping  getIAMTokenSuccessBlock, error errorBlock:@escaping getIAMTokenErrorBlock) {
//        ZSSOKit.observeSIWAAuthticationStateHavingCallback { (error) in
//            if let err = error as NSError?{
//                if err.code == k_SSONativeSIWAAuthStateCredentialRevoked{
//                    
//                }
//            }
//            
//        }
        
        ZIAMManager.logout {
            ZSSOKit.presentNativeSignIn { (accessToken, error) in
                DispatchQueue.main.async {
                    guard let token  = accessToken, (error == nil) else{
                        errorBlock(error)
                        return
                    }
                    successBlock(token)
                }
            }
        }
    }

    public class func getIAMTokenAfterLogin(_ successBlock:@escaping getIAMTokenSuccessBlock, error errorBlock:@escaping getIAMTokenErrorBlock) {
        ZIAMManager.logout {
            ZSSOKit.presentInitialViewController {  (accessToken, error) in
                DispatchQueue.main.async {
                    
                    guard let token  = accessToken, (error == nil) else{
                        errorBlock(error)
                        return
                    }
                    successBlock(token)
                }
           }
        }
    }

    //MARK:- Get Token
    public class func getIAMAccessToken(_ successBlock:@escaping getIAMTokenSuccessBlock, error errorBlock:@escaping getIAMTokenErrorBlock) {
        if !ZSSOKit.isUserSignedIn() {
            let newError = NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse, userInfo: nil)
            errorBlock(newError)
            return
        }
        
        ZSSOKit.getOAuth2Token { (accessToken, error) in
            DispatchQueue.main.async {
                guard let token  = accessToken else{
                    errorBlock(error)
                    return
                }
                successBlock(token)
            }
        }
        
    }

    public class func getIAMAccessTokenForExtn(_ successBlock:@escaping getIAMTokenSuccessBlock, error errorBlock:@escaping getIAMTokenErrorBlock) {
        getIAMAccessToken(successBlock, error: errorBlock)
    }

    //MARK:- Profile info
    public class func getIAMProfileImage(imgSize : CGSize? , _ successBlock:@escaping getIAMPofilePicSuccessBlock) {
        DispatchQueue.main.async(execute: {
            
            guard let imageData = ZSSOKit.getCurrentUser()?.profile.profileImageData, var image = UIImage(data: imageData) else {
                successBlock(UIImage(named: "no-user"))
                return
            }
            
            

            if let size = imgSize{
                image = image.downsampleImage(size: size) ?? UIImage(named: "no-user")!
            }
//            var isStockImage = false
//
//            let profile = user?.profile
//
//            var image: UIImage? = nil
//
//            if (profile?.profileImageData == nil) || !(profile?.profileImageData is Data) {
//                //ProfilePhoto
//                image = UIImage(named: "no-user")
//                isStockImage = true
//            } else {
//                if let profileImageData = profile?.profileImageData {
//                    image = UIImage(data: profileImageData)
//                }
//                if image == nil {
//                    image = UIImage(named: "no-user")
//                    isStockImage = true
//                }
//            }
//
            successBlock(image)
            
        })
    }

    //MARK:- Logout
    public class func logout(_ successBlock:@escaping getIAMLogoutSuccessBlock) {
        if !ZSSOKit.isUserSignedIn(){
            successBlock()
            return
        }
        
        ZSSOKit.revokeAccessToken { (_error) in
            DispatchQueue.main.async {
                if let error = _error{
                    ZSignKit.shared.trackError(error)
                    ZSSOKit.clearSSODetailsForFirstLaunch()
                }
                successBlock()
            }
        }
    }
}
