//
//  ZSFileManager+Attachment.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 13/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import Foundation

extension ZSFileManager{
    public func attachmentFilePath(attachmentId: String) -> (filePath : String,fileAlreadyAvailable : Bool) {
        let fm = FileManager.default
        let attachmentspath = temporaryDirectoryPath.appending("AllAttachments")
        let filePath = attachmentspath.appendingFormat("/%@.pdf", attachmentId)
        return (filePath, fm.fileExists(atPath: filePath))
    }
    
    public func saveAttachment(attachmentId: String, data: Data) -> (filePath : String,fileAlreadyAvailable : Bool) {
        
        let attachmentfolder = createSubFolder(parentFilePath: temporaryDirectoryPath, subFolder: "AllAttachments")
        
        let filePath = attachmentfolder.folderPath.appendingFormat("/%@.pdf", attachmentId)
        
        if fileManager.fileExists(atPath: filePath){
            try? fileManager.removeItem(atPath: filePath)
        }
        try? data.write(to: URL(fileURLWithPath: filePath))
        return (filePath, fileManager.fileExists(atPath: filePath))
    }
}
