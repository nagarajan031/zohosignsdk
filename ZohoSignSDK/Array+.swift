//
//  Array+.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 24/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public extension Array {
    @inlinable public func enumeratedMap<T>(_ transform: (Index,Element) throws -> T)  rethrows -> [T] {
        var index = 0
        var result: [T] = []
        for element in self {
            try? result.append(transform(index, element))
            index += 1
        }
        return result
    }
    
    @inlinable func enumeratedForEach(_ body: (Index, Element) throws -> Void) rethrows{
        var index: Int = 0
        for element in self {
            try? body(index, element)
            index += 1
        }
    }
    
    @inlinable mutating func enumeratedInPlaceLoop(_ body: (Index, Element) throws -> Element) rethrows {
        var index: Int = 0
        for element in self {
            try? self[index] = body(index, element)
            index += 1
        }
    }
    
    subscript (safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
}

public extension Array where Element: Equatable {
    
    @discardableResult
    mutating func remove(element: Element) -> Element? {
        var indexOfElementToBeRemoved: Int?
        
        enumeratedForEach { (index, item) in
            if item == element {
                indexOfElementToBeRemoved = index
            }
        }
        
        if let index = indexOfElementToBeRemoved {
            return remove(at: index)
        }
        
        return nil
    }
    
    mutating func remove(collection elements: Element...) {
        elements.forEach({remove(element: $0)})
    }
    
    mutating func remove(arrayOf elements: [Element]) {
        elements.forEach({remove(element: $0)})
    }
    
}

public extension Array where Element: LosslessStringConvertible {
    func asString() -> [String] {
        return map{(String($0))}
    }
}
