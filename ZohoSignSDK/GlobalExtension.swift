//
//  GlobalExtension.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 19/07/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

extension NSAttributedString {
    public func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
    
    public func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.width)
    }
    
    public convenience init(htmlString html: String) throws {
           try self.init(data: Data(html.utf8), options: [
               .documentType: NSAttributedString.DocumentType.html,
               .characterEncoding: String.Encoding.utf8.rawValue
           ], documentAttributes: nil)
       }
}

extension NSMutableAttributedString {
    public func setColorForText(_ textToFind: String?, with color: UIColor) {
        let range:NSRange?
        if let text = textToFind {
            range = self.mutableString.range(of: text, options: .caseInsensitive)
        }else{
            range = NSMakeRange(0, self.length)
        }
        if range!.location != NSNotFound {
            addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range!)        }
    }
    
    public static func setColorForText(_ textToFind: String,in string: String, with color: UIColor) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.setColorForText(textToFind, with: color)
        return attributedString
    }
}

extension UIDevice{
    public static var isPortrait : Bool{
        return UIApplication.shared.statusBarOrientation.isPortrait
    }
}

extension UIApplication {
    public var isSplitOrSlideOver: Bool {
        if DeviceType.isIphone || DeviceType.isMac{
            return false
        }
        guard let w = self.delegate?.window, let window = w else { return false }
        return !window.frame.equalTo(window.screen.bounds)
    }
}

extension Date{
    func toMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
