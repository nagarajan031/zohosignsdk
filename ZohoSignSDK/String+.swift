//
//  String+.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 24/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public extension String {
    var ZSLString: String {
        let bundle = Bundle(for: UserManager.self)
        return NSLocalizedString(self, tableName: "ZSignLocalizable", bundle: bundle, value: "", comment: "")
    }
    
    // for Single Line
    func SizeOf_String( font: UIFont) -> CGSize {
        if (self.isEmpty) { return CGSize.zero }
        let fontAttribute = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttribute)
        
        let finalSize = CGSize(width: ceil(size.width), height: ceil(size.height))
        return finalSize;
    }
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }
    // for Multi Line
    func textSize(withConstrainedSize constraintSize: CGSize, font: UIFont) -> CGSize {
        //let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        if (self.isEmpty) { return CGSize.zero }
        let boundingBox = self.boundingRect(with: constraintSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        //let finalSize = CGSize(width: ceil(boundingBox.width), height: ceil(boundingBox.height))
        let finalSize = CGSize(width: boundingBox.width, height: boundingBox.height)
        return finalSize
    }
    func capitalizedFirst() -> String {
        if isEmpty {return ""}
        else if count == 1 {
            return self.uppercased()
        }
        
        return (NSString(string: self).substring(to: 1).uppercased() + NSString(string: self).substring(from: 1))
    }
    
   
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    var isEmail: Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }
    
    public func urlEncodedWithAlphaNumericSet() -> String {
       return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) ??  self
    }
    
    
    public func covertToBool() -> Bool{
        return ((self == "true") || (self == "1")) ? true : false
    }
    /*func getCountryCode(nationalNumber : String) -> String{
        let numbers =  self.components(separatedBy:CharacterSet.decimalDigits.inverted).joined()
        var countryCode = numbers.replacingOccurrences(of: nationalNumber, with: "")
        if (countryCode.isEmpty) {countryCode = "1"}
        return ("+" +  countryCode)
    }*/
}
