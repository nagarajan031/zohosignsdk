//
//  iPadDocumentEditor.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 27/09/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//
//
//                       _oo0oo_
//                      o8888888o
//                      88" . "88
//                      (| -_- |)
//                      0\  =  /0
//                    ___/`---'\___
//                  .' \\|     |// '.
//                 / \\|||  :  |||// \
//                / _||||| -:- |||||- \
//               |   | \\\  -  /// |   |
//               | \_|  ''\---/''  |_/ |
//               \  .-\__  '-'  ___/-. /
//             ___'. .'  /--.--\  `. .'___
//          ."" '<  `.___\_<|>_/___.' >' "".
//         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
//         \  \ `_.   \_ __\ /__ _/   .-` /  /
//     =====`-.____`.___ \_____/___.-`___.-'=====
//                       `=---='
//
//
//     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


import UIKit
import PDFKit

@available(iOS 11.0, *)
public class iPadDocumentEditor: UIViewController, UIPopoverPresentationControllerDelegate {

    fileprivate let contentScrollView = UIScrollView()
    fileprivate var bgScrollView  = UIScrollView()
    
    let dataParser = EditorDataParser()
    
    public var isSampleSigning = false
    public var isFromCreatePage = false
    public var isFromActionExtn  = false
    public var isViewMode = false
    fileprivate var ignoreDidScrollHandling = false
    public var isThirdPartySignSDK = false
    var isRightDetailViewHidden = false

    public var extenstionContext : NSExtensionContext?

   
    public var refRequest : SignRequest?
    public var refTemplate : Template?

//    var contentViews : [Int : DocumentEditorBaseView] = [:]
//    var fieldsListArray : NSMutableArray = []
//    var recipientsArray : [Recipient] = []
//    var deletedFieldsListArray : NSMutableArray = []
//    var selectedRecipient : Recipient!
    
    
    private let shareDocumentManager = ShareDocumentManager()
    
   

    var isPageRotating = false
    
    let fieldSelectionView = IpadEditorFieldSelectionView()
    public var createRequestType : ZSDocumentCreatorType = .SIGN_AND_SEND
    
    let yPadding :CGFloat =  10
    let viewPadding : CGFloat =  20
   
    public var sampleSigningSuccessHandler : (() -> Void)?

    var totalEditorHeight : CGFloat = 0
    
    let dummyBgView = UIView()
    
    var lastScrollPoint = CGPoint.zero
    
    var beginningPoint = CGPoint.zero
    var beginningCenter = CGPoint.zero
    var tempPanView   : UIView?
    
    fileprivate var maximumPages = 0
    
    fileprivate var minimumZoomScale : CGFloat = 1
    
    fileprivate var lastScrolledY : CGFloat = 0
    fileprivate var selectedEditorView : DocumentEditorBaseView?
    
    let fieldCountBtn = ZSBadgeButton()
    
    
    var notif : ZNotificationToastView?



    private lazy var layout : UICollectionViewFlowLayout = {
        return SwiftUtils.pdfThumbanailLayout(showHeader: true)
    }()
    
    private lazy var thumbnailView : ZSPDFThumbnailView = {
        let thumbnailView = ZSPDFThumbnailView(frame: CGRect.zero, collectionViewLayout: layout)
        thumbnailView.translatesAutoresizingMaskIntoConstraints = false
        thumbnailView.backgroundColor   =   ZColor.bgColorGray
        thumbnailView.clipsToBounds    =   false;
        thumbnailView.delegate = self
        thumbnailView.thumbnailDelegate = self
        return thumbnailView
    }()

    //MARK:- Inits
    
    @objc public init(request: SignRequest) {
        super.init(nibName: nil, bundle: nil)
        self.refRequest    =   request
    }
    
    @objc public init(template: Template) {
        super.init(nibName: nil, bundle: nil)
        self.refTemplate    =   template
    }
    
    @objc public init(isSampleSigning: Bool) {
        super.init(nibName: nil, bundle: nil)
        self.isSampleSigning    =   isSampleSigning
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- view life cycles
    deinit {
        ZInfo("deinit")
    }
    public override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .all
    }
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarBorderColor()

        ZColor.adjustNavigationBarForTheme(self.navigationController?.navigationBar)
        self.navigationController?.navigationBar.barTintColor = ZColor.navBarColor
        
        view.backgroundColor = ZColor.bgColorGray
        self.navigationItem.leftBarButtonItem = ZSBarButtonItem.barButton(type: .cancel, target: self, action: #selector(cancelButtonTapped))

        dataParser.delegate   = self
        dataParser.refRequest = refRequest
        dataParser.refTemplate = refTemplate
        dataParser.isFromCreatePage = isFromCreatePage
        dataParser.createReqType = createRequestType
        dataParser.isSelfSigningRequest = refRequest?.isSelfSign == 1
        
        addBarButtons()
        
        self.view.addSubview(thumbnailView)
        thumbnailView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.height.equalToSuperview()
            make.left.equalToSuperview()
            make.width.equalTo(ipadThumbnailViewWidth)
        }
        
        view.addSubview(bgScrollView)
        bgScrollView.backgroundColor = ZColor.bgColorGray
        bgScrollView.frame  = CGRect(x: ipadThumbnailViewWidth, y: 0, width: view.frame.width - (ipadThumbnailViewWidth + editorFieldSelectionViewWidth), height: view.frame.height)
        bgScrollView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        //        bgScrollView.frame  = CGRect(x: 0, y: 0, width: view.frame.width - 220, height: view.frame.height)
        bgScrollView.maximumZoomScale    =   4.0;
        bgScrollView.minimumZoomScale    =   minimumZoomScale;
        bgScrollView.delegate        =   self
        bgScrollView.alpha          =   0
        
//        dummyBgView.frame = bgScrollView.frame
//        view.insertSubview(dummyBgView, belowSubview: bgScrollView)
//        dummyBgView.layer.bounds = bgScrollView.bounds
//        dummyBgView.layer.shadowColor = UIColor.black.cgColor
//        dummyBgView.layer.shadowOpacity = 0.2
//        dummyBgView.layer.shadowOffset = CGSize.zero
//        dummyBgView.layer.shadowRadius = 3
//        dummyBgView.layer.shadowPath = UIBezierPath(rect: dummyBgView.bounds).cgPath
//        dummyBgView.clipsToBounds = false
//
//        dummyBgView.snp.makeConstraints { (make) in
//            make.edges.equalTo(bgScrollView)
//        }
//
//
//        dummyBgView.alpha = 0
        
        bgScrollView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
        contentScrollView.clipsToBounds = false
        bgScrollView.addSubview(contentScrollView)

        maximumPages = refRequest?.totalPages as? Int ?? 1
        
        dataParser.pageWidth = bgScrollView.frame.width

        if isSampleSigning {
            self.navigationItem.rightBarButtonItem    =  ZSBarButtonItem.barButton(type: .finish, target: self, action: #selector(doneBtnAction))
//            parseSampleDocFiles()
            dataParser.parseSampleDocFiles(thumbnailView: &thumbnailView)
            dataParser.currentPage = dataParser.pagesArray.first
                    
            
        }else{
            dataParser.getRequestTypes()
            dataParser.downloadDocument()
        }
        
        self.navigationItem.rightBarButtonItem?.isEnabled = false

        let panGesture = UIPanGestureRecognizer()
        panGesture.addTarget(self, action: #selector(handlePan(gesture:)))
        panGesture.delegate  = self
        view.addGestureRecognizer(panGesture)
    }
    
   
    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.isPageRotating = true
                
        lastScrolledY = bgScrollView.contentOffset.y / bgScrollView.zoomScale
        
        dataParser.contentViews.keys.forEach { (key) in
            let editorView = dataParser.contentViews[key]
            if(editorView?.isEditingMode ?? false){
                editorView?.endEditing()
                bgScrollView.isScrollEnabled = true
            }
            if(editorView?.frame.contains(bgScrollView.contentOffset) ?? false){
                lastScrolledY = editorView?.convert(bgScrollView.contentOffset, from: contentScrollView).y ?? 0
                selectedEditorView = editorView
                lastScrolledY = (lastScrolledY / (editorView?.frame.height ?? 1))
            }
        }
   
        coordinator.animate(alongsideTransition: { (context) in
            
        }) { (context) in
            self.isPageRotating  = false
        }
        
    }
    
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if(isPageRotating){
            self.updateZoomScale()
            self.updateContentViews()
        }
       
        dummyBgView.layer.shadowPath = UIBezierPath(rect: dummyBgView.bounds).cgPath
    }
    
    //MARK: - Local methods
    
    func addBarButtons()  {
        
        var doneBtnItem : UIBarButtonItem?
        if let request =  refRequest {
            let barbuttonType : ZSBarButtonType = (request.isSelfSign?.intValue == 1) ? .finish : .send
            doneBtnItem    =  ZSBarButtonItem.barButton(type: barbuttonType, target: self, action: #selector(doneBtnAction))
        }
        else if(refTemplate != nil){
            doneBtnItem    =  ZSBarButtonItem.barButton(type: .save, target: self, action: #selector(saveDraft))
            isViewMode = !(UserManager.shared.currentUser?.isAdmin ?? false)
        }
        
        doneBtnItem?.isEnabled  =   (refRequest?.isSelfSign == 0)
        
     
        
        let flexiBtn = UIBarButtonItem(title: "  ", style: .plain, target: nil, action: nil)
        
        fieldCountBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        fieldCountBtn.setImage(ZSImage.tintColorImage(withNamed: "editor_fields_list", col: ZColor.buttonColor), for: .normal)
        fieldCountBtn.addTarget(self, action: #selector(showAllFields(_:)), for: .touchUpInside)
        fieldCountBtn.setBadge("0")
        
        let badgeItem = UIBarButtonItem(customView: fieldCountBtn)
        
        
        
        if(refTemplate != nil){
            self.navigationItem.rightBarButtonItems = [doneBtnItem] as? [UIBarButtonItem]
        }
        else{

            self.navigationItem.rightBarButtonItems = [doneBtnItem,flexiBtn,badgeItem] as? [UIBarButtonItem]
        }
        
        if isViewMode {
            self.navigationItem.leftBarButtonItem =  ZSBarButtonItem.barButton(type: .close, target: self, action: #selector(cancelButtonTapped))
            self.navigationItem.rightBarButtonItem = nil
        }
                
    }
    

    @objc public func saveDraft()  {
        dataParser.pageWidth = bgScrollView.frame.width
        dataParser.saveDraft()
    }
    
    
    @objc public func dismissVC(){
        dataParser.cancelAllTasks()
        
        if (isFromActionExtn) {
            ZSignKit.shared.trackExtnSession(isStopped: true)
            self.extenstionContext?.completeRequest(returningItems: nil, completionHandler: nil)
        }
        else{
            dismissViewController(isSuccessAnimation: true)
        }
    }
    
    
    
    @objc public func cancelButtonTapped()  {
        endEditing()
//        if (!isSigningCompleted && isThirdPartySignSDK) {
//            ZSignKit.shared.zSignFailureHandler()
//        }
        
        if (isFromCreatePage) {
            
            if (isFromActionExtn) {
                
                ZSignKit.shared.trackExtnSession(isStopped: true)

                self.extenstionContext?.completeRequest(returningItems: nil, completionHandler: nil)
            }
            else if(dataParser.isSigningCompleted){
                dismissViewController(isSuccessAnimation: true)
            }
            else {
                saveDraft()
            }
            
            return;
        }
        if (isSampleSigning || dataParser.isSigningCompleted || (refTemplate != nil)) {
            dismissViewController(isSuccessAnimation: true)

            return;
        }
        if (dataParser.isFieldsEdited) {
            var msg : String? = nil
            if DeviceType.isMac {
                msg = "sign.mobile.alert.saveChanges".ZSLString
            }

            let changesActionSheet  =  UIAlertController(title: nil, message:msg, preferredStyle: SwiftUtils.actionSheetStyle())
            
            let cancelAction  = UIAlertAction(title: "sign.mobile.request.list.discardChanges".ZSLString, style: .destructive, handler: {[weak self] (alert) in
                ZSAnalytics.shared.track(forEvent: .documentEditor( .discardChanges))
                self?.dismissViewController(isSuccessAnimation: false)
            })
            changesActionSheet.addAction(cancelAction)
            
            let saveAction  = UIAlertAction(title: "sign.mobile.request.saveChanges".ZSLString, style: .default, handler: {[weak self] (alert) in
                self?.saveDraft()
               })
            changesActionSheet.addAction(saveAction)

        
            if let popoverController = changesActionSheet.popoverPresentationController {
                popoverController.barButtonItem  =  self.navigationItem.leftBarButtonItem
            }

            present(changesActionSheet, animated: true, completion: nil)
            
        }else{
            dismissViewController(isSuccessAnimation: false)
        }
    }
    
    func showFinalPreview()   {
        dataParser.showFinalPreview()
    }
    
  
    func endEditing()
    {
        bgScrollView.isScrollEnabled = false
        bgScrollView.isScrollEnabled = true
        dataParser.endEditing()
    }
    
    @objc public func doneBtnAction(){
        SwiftUtils.dismissPoptip()
        endEditing()


        if isSampleSigning {
            dataParser.isSigningCompleted = true
            //adjustViewFrames()
            updateZoomScale()
            contentScrollView.isUserInteractionEnabled = false
            dataParser.showFinalPreview()
            ZSAnalytics.shared.track(forEvent: .documentEditor(.sampleSigning))
            ZSProgressHUD.show(inView: self.navigationController!.view, status: "sign.mobile.activityIndicator.signingDoc".ZSLString)
            self.perform({[weak self] in
                ZSProgressHUD.showSuccess(inView: self?.navigationController!.view ?? UIView(), status: "sign.mobile.request.signedSuccessfully".ZSLString)
                
                self?.sampleSigningSuccessHandling()
            }, afterDelay: 2)
            
            self.navigationItem.rightBarButtonItem  =  ZSBarButtonItem.barButton(type: .done, target: self, action: #selector(dismissVC))
            self.navigationItem.leftBarButtonItem  =   nil;
            
            return
        }
        dataParser.submitRequestToServer()
    }
    
    
    func updateTitleView(title : String, color : UIColor)  {
        if (isThirdPartySignSDK || isSampleSigning){return}
        let titleBtn = SwiftUtils.createButtonWithLeftIcon()
        titleBtn.frame = CGRect(x: 50, y: 0, width: ((self.view.frame.width) - 140), height: 44)
        titleBtn.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        titleBtn.setTitle(title, for: .normal)
        titleBtn.setImage(UIImage.circle(diameter: 10, color: color), for: .normal)
        
        self.navigationItem.titleView = titleBtn
    }
    
    func updateContentViews(){


        isRightDetailViewHidden = dataParser.isSigningCompleted || (self.view.frame.width <= 400)
        
        if (self.view.frame.width <= 600) || isPortrait() ||  UIApplication.shared.isSplitOrSlideOver{
            let widthMargin : CGFloat = isRightDetailViewHidden ? 16 : editorFieldSelectionViewWidth;


            self.bgScrollView.frame  = CGRect(x: 20, y: 0, width: self.view.frame.width - widthMargin - 16, height: self.view.frame.height)
//            let hideResizeBtn  = (isPortrait() || UIApplication.shared.isSplitOrSlideOver)
//            self.toolbarView.showResizeButton(!hideResizeBtn)
//            self.toolbarView.isHidden = false
            self.thumbnailView.isHidden = true
        }
        else{
//            let widthMargin : CGFloat = toolbarView.isRightDetailViewHidden ? editorThumbnailViewWidth : (editorThumbnailViewWidth + editorFieldSelectionViewWidth);
            let widthMargin : CGFloat = isRightDetailViewHidden ? ipadThumbnailViewWidth : (ipadThumbnailViewWidth + editorFieldSelectionViewWidth);

            self.bgScrollView.frame  = CGRect(x: ipadThumbnailViewWidth, y: 0, width: self.view.frame.width - widthMargin, height: self.view.frame.height)
            self.thumbnailView.isHidden = false

        }
        
        
        
        bgScrollView.zoomScale = 1.0

//        bgScrollView.backgroundColor = ZColor.greenColor
        adjustViewFrames()
        
//        contentScrollView.backgroundColor = ZColor.redColor
        var originY  : CGFloat = 0
        dataParser.pagesArray.forEach { (page) in
            let editorView = dataParser.contentViews[page.originalPageNumber]
            editorView?.frame = CGRect(x: 0, y: originY, width: bgScrollView.frame.width, height: page.calibratedPageHeight(bgScrollView.frame.width));
            originY = (editorView?.frame.maxY ?? 0) + yPadding
            
            let filteredResults = dataParser.fieldsListArray.filter { (field) -> Bool in
                
                return ((field.pageIndex == editorView?.pageIndex) && (field.docId == editorView?.docId))
            }
            
           
            let editorWidth = editorView?.bounds.width ?? 0
            let editorHeight = editorView?.bounds.height ?? 0
            
            
            filteredResults.forEach({ (field) in
                if let index = dataParser.fieldsListArray.lastIndex(of: field){
                    dataParser.fieldsListArray[index].currentTextProperty.fontSize = FontManager.calibaratedDeviceFontSize(forServerFontSize: String(field.serverFontSize), pageWidth: editorWidth, imageWidth: editorView?.pdfPageSize.width ?? 1)
                    dataParser.fieldsListArray[index].frame = CGRect(x: (field.xValue / 100) * editorWidth, y: (field.yValue / 100) * editorHeight, width: (field.width / 100) * (editorWidth), height: (field.height / 100) * editorHeight)
                }
            })
            editorView?.clearAllSignFields()
            editorView?.preFillSignFields(filteredResults)
//            editorView?.updateFieldFramesForViewRotation(filteredResults)

        }
        var topY =  lastScrolledY
        if let editorView = selectedEditorView{
            topY = (lastScrolledY * editorView.frame.height) + (editorView.frame.minY)
        }
        
        if (topY <= 0) { topY = 0}
        else if((bgScrollView.contentSize.height - topY) < bgScrollView.frame.height){
            topY = bgScrollView.contentSize.height - bgScrollView.frame.height
        }
        bgScrollView.contentOffset =  CGPoint(x: 0, y: topY)
        self.updateZoomScale()
        contentScrollView.isHidden = false

    }


    func  adjustViewFrames()  {
        updateTotalHeight()
//        contentScrollView.snp.removeConstraints()
//        contentScrollView.snp.makeConstraints { (make) in
//            make.left.width.equalToSuperview()
//            make.top.equalTo(viewPadding)
//            make.height.equalTo(totalEditorHeight + 90)
//        }
        contentScrollView.frame = CGRect(x: 0, y: viewPadding, width: bgScrollView.frame.width , height: totalEditorHeight + 90)
        bgScrollView.contentSize = CGSize(width: bgScrollView.frame.width, height: totalEditorHeight + 90)

//        contentScrollView.frame = CGRect(x: viewPadding, y: viewPadding, width: bgScrollView.bounds.width - 40, height: totalEditorHeight + 40)
//        bgScrollView.contentSize = CGSize(width: contentScrollView.bounds.width, height: totalEditorHeight + 20)
    }
    
    func updateTotalHeight(){
        var height = -yPadding
        dataParser.pagesArray.forEach { (page) in
            height += page.calibratedPageHeight(bgScrollView.frame.width)
            height += yPadding
        }
        
        totalEditorHeight = height
    }
    
    func updateZoomScale()
    {
        let editorView = dataParser.contentViews[1]

        if (isPortrait()){
            minimumZoomScale = 1
        }
        else if let height = editorView?.frame.height{
            minimumZoomScale = (self.bgScrollView.frame.height - 40) / height
            if(minimumZoomScale > 1)    {
                minimumZoomScale = 1
            }
        }
        
        
        bgScrollView.minimumZoomScale = minimumZoomScale
        bgScrollView.setZoomScale(minimumZoomScale, animated: false)
    }
    
    func selectSiginingReceipient(_ recep: ZSRecipient)
    {
        dataParser.changeCurrentRecipient(recep)
    }

    
    func addContentViews(){
         dataParser.pagesArray.forEach { (page) in
                    
                    var originY : CGFloat = 0
                    if(page.originalPageNumber > 1){
                        let editorView = dataParser.contentViews[page.originalPageNumber-1]
                        originY = (editorView?.frame.maxY ?? 0) + yPadding
                    }
                    let viewWidth =  contentScrollView.frame.width
                    let viewRect = CGRect(x: 0.0, y: originY, width: viewWidth, height: page.calibratedPageHeight(viewWidth))
                    let contentView = DocumentEditorBaseView(frame: viewRect)
                    contentView.isThirdPartySDK =   isThirdPartySignSDK
                    let pdfPageImg = UIImage(contentsOfFile: page.filePath!)
                    let downsampledImg = pdfPageImg?.downsampleImage(size: viewRect.size)
        //            let modifyImg = UIImage(contentsOfFile: page.filePath!)?.downsampleImage(withSize: viewRect.size)
        //            contentView.image           =   modifyImg
        //            contentView.image           =   UIImage(contentsOfFile: page.filePath!)
                    contentView.image           =   downsampledImg
                    contentView.pdfPageSize     =   pdfPageImg?.size ?? view.frame.size
                    contentView.delegate        =   dataParser
                    contentView.docId           =   page.docId
                    if(isSampleSigning){ contentView.currentInpersonSigner   =   dataParser.sampleSigner;}
                    contentView.isTemplateDocumentFields    =   dataParser.selectedRecipient.isTemplateUser;
                    contentView.docOrder        =  page.docOrder
                    contentView.pageIndex       =  page.pageNumber - 1;
                    //    contentView.pageNumber      =   (int)pageNumber - 1;
            contentView.currentEditingType =  ((createRequestType ==  .SEND_FOR_SIGNATURE) || (createRequestType == .TEMPLATE)) ?  .sentDoc_Create : .sentDoc_selfSign;
        //            contentView.fieldsListArray =  NSMutableArray(array: fieldsListArray)
        //            contentView.fieldsListArray =  dataParser.fieldsListArray
                    contentView.isPreviewMode   =   (!contentScrollView.isUserInteractionEnabled || isViewMode);
                    contentView.isFinalPreviewMode  =   dataParser.isSigningCompleted;
                    contentView.superVC       =   self.navigationController;
        //            contentView.deletedFieldsListArray = dataParser.deletedFieldsListArray
                    contentView.isUserInteractionEnabled  =   true;
                    contentView.isSampleSigning      =  isSampleSigning;
                    
                    contentView.layer.borderColor = ZColor.seperatorColor.cgColor
                    contentView.layer.borderWidth = 2
                    
                    
                    dataParser.contentViews[page.originalPageNumber] = contentView
                    contentScrollView.addSubview(contentView)
                    
                    var filteredResults = dataParser.fieldsListArray.filter { (field) -> Bool in
                        return ((field.pageIndex == contentView.pageIndex) && (field.docId == contentView.docId))
                    }
                    
                    if (filteredResults.count > 0){
                        contentView.preFillSignFields(filteredResults)
                    }
                }
        
    }
    
    func addFieldSelectionView() {
        fieldSelectionView.receipientsArray = dataParser.recipientsArray
        fieldSelectionView.isSigningOrderOn  = (refRequest?.isSequential == 1)
        //        view.addSubview(fieldSelectionView)
        view.insertSubview(fieldSelectionView, at: 0)
        fieldSelectionView.frame  = CGRect(x: view.frame.width - editorFieldSelectionViewWidth, y: 0, width: editorFieldSelectionViewWidth, height: view.frame.height)
        fieldSelectionView.autoresizingMask = [.flexibleLeftMargin,.flexibleHeight]
        fieldSelectionView.delegate = self
        fieldSelectionView.disableFieldSelection = isViewMode
        fieldSelectionView.isSignAndSend = (createRequestType == .SIGN_AND_SEND)
        fieldSelectionView.initateSetup()
        
    }

    
    func goToPage(docIndex: Int, pageIndex : Int)  {
        if((pageIndex == (dataParser.currentPage.pageNumber-1)) && (docIndex == (dataParser.currentPage.docOrder))){
            return
        }
        
        let filteredResults = dataParser.pagesArray.filter { (page) -> Bool in
            return ((page.pageNumber == pageIndex+1) && (page.docOrder == docIndex))
        }
        guard let page = filteredResults.first else {
            return
        }
        
        ignoreDidScrollHandling = true
        dataParser.currentPage = page
        
        let editorView = dataParser.contentViews[page.originalPageNumber]
        var offsetY = editorView?.frame.origin.y ?? 0
        
        offsetY = offsetY * bgScrollView.zoomScale
        if ((bgScrollView.contentSize.height - offsetY) < bgScrollView.frame.height){
            offsetY = bgScrollView.contentSize.height - bgScrollView.frame.height
        }
        
        bgScrollView.setContentOffset(CGPoint(x: 0, y: offsetY), animated: true)
        self.perform({ [weak self] in
            self?.ignoreDidScrollHandling = false
        }, afterDelay: 0.3)
    }
    
//    func addSignField(tapPoint: CGPoint, fieldType: signFieldType, isQuickAdd: Bool, pangesture: UIPanGestureRecognizer?)  {
//        GlobalFunctions.makeDeviceVibrate()
//        SwiftUtils.dismissPoptip()
//
//        dataParser.contentViews.forEach { (key, value) in
//            if( value.isEditingMode){
//                value.endEditing()
//                bgScrollView.isScrollEnabled = true
//            }
//        }
//
//        dataParser.contentViews.forEach { (key, editorView) in
//            if(editorView.frame.contains(tapPoint)){
//                var point = CGPoint(x: editorView.bounds.width/2, y: editorView.bounds.height/2)
//               editorView.isUserInteractionEnabled = true
//                if(isQuickAdd){
//                    editorView.addSignField(point: point, fieldType: fieldType)
//                    return
//                }else if let gesture = pangesture{
//                    point = gesture.location(in: editorView)
//                }else{
//                    point = bgScrollView.convert(tapPoint, to: editorView)
//                }
//                editorView.addSignField(point: point, fieldType: fieldType)
//            }
//        }
//    }

    @objc func showAllFields(_ fieldBtn : UIButton) {
        bgScrollView.setContentOffset(bgScrollView.contentOffset, animated: false)
        
        let recipient = ZSRecipient();
        recipient.name  =   UserManager.shared.currentUser?.fullName;
        recipient.draftFieldListArray = dataParser.fieldsListArray;
        
        
        let fieldsListView = EditorFieldsListViewController(style: .plain)
        fieldsListView.pagesArray = dataParser.pagesArray
        fieldsListView.delegate = self
        fieldsListView.isDocumentCreate = true
        var docs : [Documents] = []
        refRequest?.documents?.forEach({ (doc) in
            docs.append(doc)
        })
        fieldsListView.documentsArray = docs
        
        weak var weakself = self
        fieldsListView.receipientsArray = [recipient]
        
        fieldsListView.modalPresentationStyle = .popover
        let popover = fieldsListView.popoverPresentationController
        popover?.delegate = self
        popover?.sourceView = fieldBtn
        popover?.sourceRect = fieldBtn.bounds
        present(fieldsListView, animated: true, completion: nil)
        
    }
    
    @objc func showSearchBtnTapped(_ sender : UIBarButtonItem)  {
        let searchViewController = ZSPDFSearchViewController()
        searchViewController.pdfDocument = thumbnailView.documentsArray.first
        searchViewController.delegate = self
        
        searchViewController.modalPresentationStyle = .popover
        if let popoverVC = searchViewController.popoverPresentationController{
            popoverVC.permittedArrowDirections = [.down]
            popoverVC.barButtonItem = sender
        }
        self.present(searchViewController, animated: true, completion:nil)
    }
    
    func goToField(_ field : SignField) {
        let filteredPages = dataParser.pagesArray.filter { (page) -> Bool in
            return ((page.pageNumber == field.pageIndex+1) && (page.docId == field.docId))
        }
        
        guard let selectedPage = filteredPages.first else { return }
        
        let contentView = dataParser.contentViews[selectedPage.originalPageNumber]
        let contentY = (contentView?.frame.origin.y ?? 0)
        
        var offsetY = (contentY + field.frame.origin.y - 150);
        
        offsetY = (offsetY * bgScrollView.zoomScale)
        
        if ((bgScrollView.contentSize.height -  offsetY) < bgScrollView.frame.height ) {
            offsetY = bgScrollView.contentSize.height - bgScrollView.frame.size.height;
        }
        if offsetY < 0 { offsetY = 0 }
        
        bgScrollView.setContentOffset(CGPoint(x: 0, y: offsetY), animated: true)
        perform({
            self.highlightField(field)
        }, afterDelay: 0.3)
        perform({
            self.thumbnailView.goToPage(indexPath: IndexPath(row: selectedPage.pageNumber-1, section: selectedPage.docOrder), animated: false)
        }, afterDelay: 0.4)

    }
    
    @objc func handlePan(gesture : UIPanGestureRecognizer) {
        if (dataParser.isSigningCompleted) {return;}

        let touchLocation = gesture.location(in: self.view)
        switch gesture.state {
        case .began:
            tempPanView?.removeFromSuperview()

            beginningPoint = touchLocation;
            beginningCenter = touchLocation;
            bgScrollView.isUserInteractionEnabled = false;
            let convertedPoint = view.convert(touchLocation, to: fieldSelectionView)
            tempPanView = fieldSelectionView.viewInTouchLocation(convertedPoint)
            tempPanView?.alpha = 0.8
            if tempPanView != nil{
                view.addSubview(tempPanView!)
                tempPanView!.center = beginningPoint;
            }
            fieldSelectionView.isUserInteractionEnabled = false;
            break;
        case .changed:
            tempPanView?.center = touchLocation;
            break;
        case .ended:
            bgScrollView.isUserInteractionEnabled = true;
            fieldSelectionView.isUserInteractionEnabled = true;
            
            if tempPanView == nil{
                tempPanView?.removeFromSuperview()
                return
            }
           
            
            tempPanView?.center = touchLocation;
            
            let point  = gesture.location(in: contentScrollView)
            var zoomedPoint = point
            if (bgScrollView.zoomScale < 1){
                zoomedPoint =  CGPoint(x: point.x * bgScrollView.zoomScale, y: point.y * bgScrollView.zoomScale)
                zoomedPoint.x +=  contentScrollView.frame.origin.x
            }
            
            if(contentScrollView.frame.contains(zoomedPoint)){
//                addSignField(tapPoint: point, fieldType: fieldSelectionView.selectedFieldType, isQuickAdd: false, pangesture: gesture)
                dataParser.addSignField(tapPoint: point, fieldType: fieldSelectionView.selectedFieldType, isQuickAdd: false, pangesture: gesture, inView: &bgScrollView)
            }

            UIView.animate(withDuration: 0.1, animations: {
                self.tempPanView?.transform = CGAffineTransform.init(scaleX: 0.3, y: 0.3)
            }) { (finished) in
                self.tempPanView?.removeFromSuperview()
//                self.tempPanView = nil;
            }
           
            break;
        default:
            break;
        }
    }
    

//MARK: - Signing success handling

    func documentSigningSuccessHandling() {
        
        
        dataParser.isSigningCompleted = true
        isRightDetailViewHidden = true
        
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItems = nil
        self.navigationItem.rightBarButtonItem = ZSBarButtonItem.barButton(type: .done, target: self, action: #selector(dismissVC))

        let widthMargin : CGFloat =  ipadThumbnailViewWidth;


        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.bgScrollView.frame  = CGRect(x: widthMargin, y: 0, width: self.view.frame.width - widthMargin - 20, height: self.view.frame.height)
            self.updateContentViews()
        }, completion: nil)
        
        if (isThirdPartySignSDK) {
            ZSProgressHUD.showSuccess(inView: self.navigationController!.view, status: "sign.mobile.request.signedSuccessfully".ZSLString)
            
            let notif = ZNotificationToastView(title: "sign.mobile.tooltip.extnSuccessmsg".ZSLString)
            notif.showSuccessMsg()
            
        }else{
            let msg = "\"\(title ?? "")\" " +  "sign.mobile.request.signedSuccessfully".ZSLString.lowercased()
            
            updateTitleView(title: title ?? "", color: ZColor.completedColor)
            notif = ZNotificationToastView(title:msg, actionTitle: DeviceType.isMac ? "sign.mobile.common.export".ZSLString :  "sign.mobile.common.share".ZSLString, actionTarget: self, action: #selector(shareDocument))
            notif?.hideAfter = 1000
            notif?.showSuccessMsg(inView: self.view, tapToDismiss: false)
            
            NotificationCenter.default.post(name: NSNotification.Name(kSignRequestListRefreshNotification), object: refRequest, userInfo:[kNotificationUserInfoSignRequest : ZSAlertType.SentReq_Created])
            
            ZSProgressHUD.dismiss()
        }
        
        dataParser.currentPage = dataParser.pagesArray.first
        bgScrollView.setContentOffset(CGPoint.zero, animated: true)
        showFinalPreview()

        self.perform({
            UIView.animate(withDuration: 0.3, animations: {
                self.contentScrollView.center = CGPoint(x: (self.bgScrollView.frame.width / 2), y: self.contentScrollView.center.y)
            }, completion: { (finish) in
                self.fieldSelectionView.isHidden = true
            })
           
        }, afterDelay: 0)
        
    }
    
    @objc func shareDocument() {
        guard let actionBtn = notif?.actionBtn else{
            return
        }
        dataParser.shareDocument(fromButton: actionBtn)
    }
    
    func setSampleSuccessHandler(handler: @escaping () -> ()) {
        sampleSigningSuccessHandler = handler
    }
    
    
    func sampleSigningSuccessHandling(){
        //        bgScrollView.backgroundColor = ZColor.redColor
        isRightDetailViewHidden = true
//        let widthMargin : CGFloat = toolbarView.isMaximimumSizeSelected ? 0 : editorThumbnailViewWidth;
//        bgScrollView.snp.removeConstraints()
//        bgScrollView.snp.makeConstraints { (make) in
//            make.left.equalTo(thumbnailView.snp.right)
//            make.top.bottom.height.equalToSuperview()
//            make.right.equalToSuperview().offset(-cellValueLabelY)
//        }
//
//        ZSNotificationToastView.showToast(type: .success, title: "sign.mobile.account.signupToProceed".ZSLString, inView: self.view, actionTitle: "sign.mobile.common.signup".ZSLString){[weak self] in
//            self?.sampleSigningSuccessHandler?()
//        }
        
        notif = ZNotificationToastView(title:"sign.mobile.account.signupToProceed".ZSLString, actionTitle: "sign.mobile.common.signup".ZSLString, actionTarget: self, action: #selector(sampleSignSuccessAction))
        notif?.hideAfter = 100
        notif?.showSuccessMsg(inView: self.view, tapToDismiss: false)
        
        let widthMargin : CGFloat =  ipadThumbnailViewWidth;
        
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .beginFromCurrentState, animations: {
//            self.bgScrollView.frame  = CGRect(x: widthMargin, y: 0, width: self.view.frame.width - widthMargin - 20, height: self.view.frame.height)
            self.updateContentViews()
        }, completion: nil)
//        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
//            self.bgScrollView.frame  = CGRect(x: widthMargin, y: 0, width: self.view.frame.width - widthMargin - 20, height: self.view.frame.height)
//            self.updateContentViews()
//
//        }) { (finished) in
//
//        }
        dataParser.currentPage = dataParser.pagesArray.first
        
        bgScrollView.setContentOffset(CGPoint.zero, animated: true)
        
        self.perform({
            UIView.animate(withDuration: 0.3, animations: {
                self.contentScrollView.center = CGPoint(x: (self.bgScrollView.frame.width / 2), y: self.contentScrollView.center.y)
            }, completion: { (finish) in
                self.fieldSelectionView.isHidden = true
            })
            
        }, afterDelay: 0)
    }
    
    @objc func sampleSignSuccessAction()  {
        self.sampleSigningSuccessHandler?()
    }
    
}

// MARK:- Scrollview Delegate
@available(iOS 11.0, *)
extension iPadDocumentEditor : UIScrollViewDelegate{
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        if (scrollView == bgScrollView) {
            return contentScrollView;
        }else{
            return nil;
        }
    }

    public func scrollViewDidZoom(_ scrollView: UIScrollView) {
//        if (scrollView == bgScrollView) {
//            bgScrollView.contentSize   =    CGSize(width: contentScrollView.frame.size.width,height : totalEditorHeight * bgScrollView.zoomScale)
//        }
        if scrollView.zoomScale > 1 {
//            if (scrollView == bgScrollView) {
//                 bgScrollView.contentSize   =    CGSize(width: contentScrollView.frame.size.width,height : totalEditorHeight * bgScrollView.zoomScale)
//            }
//            contentScrollView.center = CGPoint(x: (bgScrollView.frame.width / 2), y: contentScrollView.center.y)

        }
        else{
//            contentScrollView.center = CGPoint(x: bgScrollView.frame.width / 2, y: contentScrollView.center.y)
            contentScrollView.center = CGPoint(x: (bgScrollView.frame.width / 2), y: contentScrollView.center.y)

//            if  isPortrait() {
//                contentScrollView.center = CGPoint(x: bgScrollView.frame.width / 2, y: contentScrollView.center.y)
//            }else{
//                contentScrollView.center = CGPoint(x: bgScrollView.frame.width / 2, y: contentScrollView.center.y)
//            }
        }
       
    }
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {

        let currentEditorView = dataParser.contentViews[dataParser.currentPage.originalPageNumber]
        
        let touchLocation = scrollView.panGestureRecognizer.location(in: contentScrollView)
        if let selectedFieldView = currentEditorView?.currentSelectedView{
            let newPoint = contentScrollView.convert(touchLocation, to: currentEditorView)
            var viewFrame: CGRect = .zero
            if let resizeView = selectedFieldView as? ZResizeView {
                viewFrame = resizeView.frame
            } else if let radioGroupView = selectedFieldView as? ZRadioGroupView{
                radioGroupView.selectedRadioItem.safelyUnwrap {
                    viewFrame = $0.convert($0.dragView.frame, to: radioGroupView)
                }
            }
            let touched = viewFrame.contains(newPoint)
            if(touched){
                scrollView.isScrollEnabled = false
                scrollView.isScrollEnabled = true
                ZInfo("touched")
                return;
            }
        }
        
        lastScrollPoint = scrollView.contentOffset
       
        
    }
    
    func highlightField(_ field : SignField){
        if  let _refSignView  = field.parentView {
            _refSignView.setIsHighlighted(true)
        }
    }

    
    
    public func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        if scale  < minimumZoomScale {
            bgScrollView.setZoomScale(minimumZoomScale, animated: true)
        }
    }
    
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if ignoreDidScrollHandling {return}
        if (scrollView != bgScrollView) {return;}
        
        let yVal = scrollView.contentOffset.y
        
        if (lastScrollPoint.y > scrollView.contentOffset.y) { //    // move down
            if (yVal >= (totalEditorHeight - bgScrollView.frame.size.height)) {
                dataParser.currentPage = dataParser.pagesArray.last;
            }
            else if (dataParser.currentPage.originalPageNumber > 1){
                let prevEditorView = dataParser.contentViews[dataParser.currentPage.originalPageNumber-1];
        
                let diff = ((yVal / bgScrollView.zoomScale) - prevEditorView!.frame.origin.y);
        
                if (diff < 0) {
                    dataParser.currentPage = dataParser.pagesArray[dataParser.currentPage.originalPageNumber-2]
                }
            }
         }
        else if (lastScrollPoint.y < scrollView.contentOffset.y) {
        
            // move up
            if (yVal <= 30) { dataParser.currentPage = dataParser.pagesArray.first
            }
            else if (dataParser.currentPage.originalPageNumber < dataParser.pagesArray.count) {
                let nextEditorView = dataParser.contentViews[dataParser.currentPage.originalPageNumber+1]
            
                let diff = (nextEditorView!.frame.origin.y - (yVal / bgScrollView.zoomScale))
            
                if (diff < (bgScrollView.frame.size.height/2)) {
                  dataParser.currentPage = dataParser.pagesArray[dataParser.currentPage.originalPageNumber];
                }
        
            }
        }
        
        thumbnailView.goToPage(indexPath: IndexPath(row: dataParser.currentPage.pageNumber-1, section: dataParser.currentPage.docOrder), animated: true)
        lastScrollPoint = scrollView.contentOffset;
    }

}

// MARK:- Gesture Delegate
@available(iOS 11.0, *)
extension iPadDocumentEditor : UIGestureRecognizerDelegate{
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
            if isViewMode { return false}
        else if let panGesture = gestureRecognizer as? UIPanGestureRecognizer{
            let touchPoint = gestureRecognizer.location(in: fieldSelectionView)
            return ((touchPoint.x > 0) && (touchPoint.y > 0) && panGesture.view != bgScrollView)
        }
        return !bgScrollView.isHidden
    }
}
// MARK:- UICollectionView Delegate
@available(iOS 11.0, *)
extension iPadDocumentEditor : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.goToPage(docIndex: indexPath.section, pageIndex: indexPath.row)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
        return SwiftUtils.thumbnailPageSize(pagesArray: dataParser.pagesArray, indexPath: indexPath)
    }
}

extension iPadDocumentEditor : ZSPDFThumbnailViewDelegate{
    public func pdfThumbnailViewDidSelectHeader(section: Int) {
        goToPage(docIndex: section, pageIndex: 0)
    }
}

// MARK:- PDF Search extension

extension iPadDocumentEditor : ZSPDFSearchViewControllerDelegate {
    
    public func searchTableViewController(_ searchTableViewController: ZSPDFSearchViewController, didSelectSerchResult selection: PDFSelection) {
        selection.color = UIColor.yellow
        
    }
    
    
}



//MARK:- Field Selector delegate
@available(iOS 11.0, *)
extension iPadDocumentEditor : IpadEditorFieldSelectionViewDelegate{
  
    func fieldSelectionViewRecipientDidChange(_ recep: ZSRecipient) {
        selectSiginingReceipient(recep)
    }
    
    public func fieldSelectionViewFieldDidTapped(_ fieldType: ZSignFieldType) {
        let editorView = dataParser.contentViews[dataParser.currentPage.originalPageNumber]
        editorView?.addSignField(point: CGPoint(x: editorView!.bounds.width/2, y: editorView!.bounds.height/2), fieldType: fieldType)
//        addSignField(tapPoint:CGPoint(x: editorView!.bounds.width/2, y: editorView.bounds.height/2), fieldType: fieldType, isQuickAdd: true, pangesture: nil)
    }
    
    
    //MARK:- Modal transition helper
    public func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .fullScreen
    }

    public func presentationController(_ controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
        return UINavigationController(rootViewController: controller.presentedViewController)
    }
}


//MARK: - Dataparser Delegate

extension iPadDocumentEditor : DocumentCreateEditorDelegate{
    
    func editorDataParserParsingCompleted() {
        dataParser.resetAllFields()
        if dataParser.recipientsArray.count > 1 {
//            createRecipientSelectionForMultipleRecipients()
        }
    
        for receipient in dataParser.recipientsArray {
            if(receipient.isNeedToSign){
                selectSiginingReceipient(receipient)
                break;
            }
        }
        
        
        self.navigationItem.rightBarButtonItem?.isEnabled  = ((dataParser.fieldsListArray.count > 0) || (dataParser.selectedRecipient.actionType == ZS_ActionType_Approver))
        
        adjustViewFrames()
        addFieldSelectionView()
        addContentViews()
        thumbnailView.documentsArray = dataParser.documentsArray
        thumbnailView.reloadData()
        thumbnailView.goToPage(indexPath: IndexPath(item: 0, section: 0), animated: false)
        
      

        perform({ [weak self] in
            self?.updateContentViews()
            self?.bgScrollView.alpha  = 1
            }, afterDelay: 0)
    }
    
    func editorDataParserParsingFailed() {
//        dismissVC()
    }
    
    func editorDataParserSaveDraftCompleted() {
        dismissVC()
    }
    
    func editorDataParserRequestCreated() {
        if !dataParser.isSelfSigningRequest {
            dismissVC()
        }
        documentSigningSuccessHandling()
        /*
        updateZoomScale()
        
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItems = nil
        self.navigationItem.rightBarButtonItem = ZSBarButtonItem.barButton(type: .done, target: self, action: #selector(dismissVC))
        
        
        if (isThirdPartySignSDK) {
            ZSProgressHUD.showSuccess(inView: self.navigationController!.view, status: "sign.mobile.request.signedSuccessfully".ZSLString)
            
            let notif = ZNotificationToastView(title: "sign.mobile.tooltip.extnSuccessmsg".ZSLString)
            notif.showSuccessMsg()
            
        }else{
            showSuccessNotification()
            
            
            ZSProgressHUD.dismiss()
        }
        
        dataParser.currentPage = dataParser.pagesArray.first
        bgScrollView.setContentOffset(CGPoint.zero, animated: true)
        dataParser.showFinalPreview()
                
        self.perform({
            UIView.animate(withDuration: 0.3, animations: {
                self.contentScrollView.center = CGPoint(x: (self.bgScrollView.frame.width / 2), y: self.contentScrollView.center.y)
            }, completion: { (finish) in
                self.fieldSelectionView.isHidden = true
            })
            
        }, afterDelay: 0)*/
    }
    
    func editorDataParserTemplateCreated(_ template: Template?) {
        dismissVC()
    }
    
    func editorDataParserRequestUpdateFailed() {
//        dismissVC()
    }
    
    func shareButtonClicked()  {
        dataParser.shareDocument(fromButton: notif?.actionBtn)
    }
    
    func showSuccessNotification() {

       /* if !isThirdPartySignSDK {
            successMsgView = ZSSuccessMsgView.sharedInstance()
            successMsgView.documentName = title ?? ""
            successMsgView.showSuccessMsg(inView: view, type: (isFromActionExtn ? .ExtnSigning : .SelfSigning))

            weak var _self = self
            successMsgView.didActionButtonPressed = {
                _self?.shareButtonClicked()
            }

            currentBottomBarHeight = Int(successMsgView.viewHeight)
        } else {
            currentBottomBarHeight = 0
        }
        bgScrollView.contentSize = CGSize(width: contentScrollView.frame.size.width, height: contentScrollView.frame.size.height + CGFloat(currentBottomBarHeight))*/
    }

    
    
    func editorDataParserFieldsUpdated(_ fieldsListArray: [SignField]) {
        navigationItem.rightBarButtonItem?.isEnabled = (fieldsListArray.count > 0)
        fieldCountBtn.setBadge(String(format: "%i", fieldsListArray.count))
    }
       
    

}


extension iPadDocumentEditor : EditorFieldsListViewControllerDelegate{
    public func editorFieldsListDidDeleteField(_ field: SignField) {
        field.parentView?.removeFromSuperview()
        dataParser.editorBaseViewFieldDeleted(editorView: nil, field: field)
    }
    
    public func editorFieldsListDidSelectField(_ field: SignField) {
        goToField(field)
    }
    
    public func editorFieldsListDidUpdatedList(_ fieldsList: [SignField]) {
        
    }
}
