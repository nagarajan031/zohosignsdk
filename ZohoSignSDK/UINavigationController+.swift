//
//  UINavigationController+.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 24/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

extension UINavigationController {
    var borderHeight: CGFloat {
        return 0.4
    }
    
    public func setNavigationBarBorderColor(_ color:UIColor = ZColor.secondaryTextColorLight) {
//        UINavigationController.changeNavBarShadowImageForTraitChanges = true
        navigationBar.tag = 100
        if  #available(iOS 13, *), traitCollection.userInterfaceStyle == .dark {
            self.navigationBar.shadowImage = color.as1ptImage(height: borderHeight)
        } else {
            self.navigationBar.shadowImage = nil
        }
    }
    open override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
//        if !UINavigationController.changeNavBarShadowImageForTraitChanges { return }
        if navigationBar.tag != 100 { return }
        
        if #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection), traitCollection.userInterfaceStyle == .dark{
            self.navigationBar.shadowImage = ZColor.secondaryTextColorLight.as1ptImage(height: borderHeight)
        } else {
            self.navigationBar.shadowImage = nil
        }

    }
}

fileprivate extension UIColor {

    /// Converts this `UIColor` instance to a 1x1 `UIImage` instance and returns it.
    ///
    /// - Returns: `self` as a 1x1 `UIImage`.
    fileprivate func as1ptImage(height : CGFloat = 1.0) -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: height))
        setFill()
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: height))
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }
}
