//
//  ZSignSelectController.swift
//  ZohoSignSDK
//
//  Created by sudhan-6859 on 17/09/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import Foundation
import SnapKit

class ZSignSelectView: UIButton {
    let textLabel = UILabel()
    private let imgView = UIImageView()
    
    let placeHolderLabel = UILabel()
    
    func initateSetup()  {
        imgView.image = ZSImage.tintColorImage(withNamed: "icn_success", col: ZColor.buttonColor)
        addSubview(imgView)
        imgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.width.equalTo(26)
            make.height.equalTo(26)
        }
     
        placeHolderLabel.textAlignment = .center
        placeHolderLabel.textColor = UIColor(white: 0, alpha: 0.1)
        addSubview(placeHolderLabel)
        
        placeHolderLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20)
            make.bottom.equalToSuperview().offset(-20)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
        }
        
        textLabel.adjustsFontSizeToFitWidth = true
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.textAlignment = .center
        addSubview(textLabel)
        textLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20)
            make.bottom.equalToSuperview().offset(-20)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
        }
        
        backgroundColor = ZColor.bgColorWhite
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0.0)
        layer.shadowRadius = 3.0
        layer.cornerRadius = 3
        layer.shadowOpacity = 0.4
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
        
       

    }
    
    func setSelect(_ select : Bool)  {
        imgView.isHidden = !select
    }
    
    func setText(_ txt : String)  {
        textLabel.text = txt
        placeHolderLabel.isHidden = !txt.isEmpty
    }
    
    
}

public class ZSignSelectController : UITableViewController {
    deinit {
        ZInfo("Select style page deinit")
    }
    let textField = UITextField()
    private let fontListArray : [UIFont?] = [UIFont(name: "Pretty-Pen-Regular", size: 60),UIFont(name: "FancySignature", size: 40),UIFont(name: "SweetlyBroken", size: 78)]
    @objc public  var signatureText : String = ""
    private var selectedIndexPath : IndexPath = IndexPath(row: 0, section: 0)
    
    private let signType : ZSignFieldSignatureType
    public var delegate : SignatureCreatorDelegate?
    
    private let scrollview = UIScrollView()
    
    private var style1 : ZSignSelectView!
    private var style2 : ZSignSelectView!
    private var style3 : ZSignSelectView!
    
    let angleRatio: CGFloat = 0.3
    let rotationX: CGFloat = -1
    let rotationY: CGFloat = 0
    let rotationZ: CGFloat = 0
//    let translateX: CGFloat = 0.045
    let translateX: CGFloat = 0.020
    let translateY: CGFloat = 0

    let headerView = UIView()
    
    public var cancelBlock: (()->())?
    let tapToChooseLabel = UILabel()

   public required init(signType: ZSignFieldSignatureType) {
        self.signType = signType
        super.init(style: .grouped)
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {

        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Local methods
    
    @objc public func cancelButtonPressed(){
        self.dismiss(animated: true){[weak self] in
            #if targetEnvironment(macCatalyst)
            self?.cancelBlock?()
            #endif
        }
    }
    
    @objc public func doneButtonPressed(){
        saveSignature()
        self.perform({[weak self] in
            self?.cancelButtonPressed()
        }, afterDelay: 0.5)
    }
    
    @objc private func saveSignature(){
        if let image = self.image(fromText: self.textField.text ?? ""){
            self.delegate?.signatureCreated(signImage: image)
        }
    }
    
    private func initialString(forName name: String?) -> String {
        var initialChars = ""
        let wordsArr = name?.components(separatedBy: CharacterSet.whitespaces) ?? []
        for i in wordsArr{
            if let _i = i.first{
                initialChars.append(_i)
            }
        }
        if initialChars.count == 0 {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            initialChars = "sign.mobile.profile.yourInitial".ZSLString
        }
        return initialChars
    }

    
    private func image(fromText text: String) -> UIImage? {
        let tempFontArray  : [UIFont?] = [UIFont(name: "Pretty-Pen-Regular", size: 34),UIFont(name: "FancySignature", size: 22),UIFont(name: "SweetlyBroken", size: 48)]

        
        let expectedLabelSize: CGRect = text.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: 30), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: tempFontArray[selectedIndexPath.row] as Any], context: nil)

        let styleWidth: CGFloat = expectedLabelSize.size.width

        let styleHeight: CGFloat = expectedLabelSize.size.height
        let label = UILabel()
        label.frame = CGRect(x: -100, y: -100, width: (styleWidth) + 20, height: styleHeight)

        if let font = tempFontArray[selectedIndexPath.row] {
            label.font = font
        }
        label.text = text
        label.textColor = ZColor.blackColor
        label.textAlignment = .center
        label.backgroundColor = UIColor.clear
        view.insertSubview(label, at: 0)


        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, UIScreen.main.scale)

        label.drawHierarchy(in: label.bounds, afterScreenUpdates: true)
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    private func initialSetup(){
        
        self.navigationItem.leftBarButtonItem = ZSBarButtonItem.barButton(type: .cancel, target: self, action: #selector(cancelButtonPressed))
        self.navigationItem.rightBarButtonItem = ZSBarButtonItem.barButton(type: .done, target: self, action: #selector(doneButtonPressed))
    }
    
    override public func loadView() {
        super.loadView()
        
        headerView.frame = CGRect(x: 0, y: 0, width:  view.frame.width, height: 370)
        headerView.autoresizingMask = .flexibleWidth
        tableView.tableHeaderView = headerView
        self.view.backgroundColor = ZColor.bgColorGray
        
        if view.frame.width > 1000{
            scrollview.frame  = CGRect(x: 100, y: 35, width: view.frame.width - 200, height: 180)
        }else{
            scrollview.frame  = CGRect(x: 30, y: 35, width: view.frame.width - 60, height: 180)
        }
        scrollview.isPagingEnabled = true
        scrollview.showsHorizontalScrollIndicator = false
        scrollview.backgroundColor = ZColor.bgColorGray
        scrollview.layer.cornerRadius = 5
        scrollview.clipsToBounds = false
        scrollview.delegate = self
        scrollview.contentSize = CGSize(width: (scrollview.frame.width * CGFloat (fontListArray.count)), height: 180)
        headerView.addSubview(scrollview)
        
        tapToChooseLabel.translatesAutoresizingMaskIntoConstraints = false
        tapToChooseLabel.text = "sign.mobile.settings.chooseSignatureStyle".ZSLString + " (1 / 3)"
        tapToChooseLabel.textAlignment = .center
        tapToChooseLabel.font = ZSFont.regularFont(withSize: 13)
        tapToChooseLabel.textColor = ZColor.secondaryTextColorDark
        tapToChooseLabel.alpha = 0.9
        headerView.addSubview(tapToChooseLabel)
        
        tapToChooseLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalTo(scrollview.snp.bottom).offset(18)
        }
        
        
        let nameLabel = UILabel()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.font = ZSFont.tableSectionHeaderFont()
        nameLabel.textColor = ZColor.tableHeaderTextColor
        
        headerView.addSubview(nameLabel)
        
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(cellValueLabelX)
            make.right.equalToSuperview()
            make.top.equalTo(tapToChooseLabel.snp.bottom).offset(35)
        }
        
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        style1 = ZSignSelectView(frame: CGRect(x:0, y: 0, width: scrollview.frame.width, height: 180))
        style2 = ZSignSelectView(frame: CGRect(x: scrollview.frame.width , y: 0, width: scrollview.frame.width, height: 180))
        style3 = ZSignSelectView(frame: CGRect(x: 2*(scrollview.frame.width), y: 0, width: scrollview.frame.width , height: 180))
        scrollview.addSubview(style1)
        scrollview.addSubview(style2)
        scrollview.addSubview(style3)
        
        style1.initateSetup()
        style2.initateSetup()
        style3.initateSetup()
        
        var signatureStr  = signatureText;
        if signatureStr.isEmpty {
            if let fullName = UserManager.shared.currentUser?.fullName {
                signatureStr = fullName
            }else{
                signatureStr = ZS_DEFAULT_SAMPLE_SIGN_USERNAME
            }
        }
        
        
        switch signType{
        case .signature:
            self.navigationItem.title = "sign.mobile.settings.enterSignature".ZSLString
            signatureText = signatureStr
            nameLabel.text = "sign.mobile.field.name".ZSLString.uppercased()
            style1.placeHolderLabel.text = "sign.mobile.field.signature".ZSLString
            style2.placeHolderLabel.text = "sign.mobile.field.signature".ZSLString
            style3.placeHolderLabel.text = "sign.mobile.field.signature".ZSLString
            
        case .initial:
            self.navigationItem.title = "sign.mobile.settings.enterInitial".ZSLString
            nameLabel.text = "sign.mobile.field.initial".ZSLString.uppercased()
            signatureText = initialString(forName: signatureStr)
            style1.placeHolderLabel.text = "sign.mobile.field.initial".ZSLString
            style2.placeHolderLabel.text = "sign.mobile.field.initial".ZSLString
            style3.placeHolderLabel.text = "sign.mobile.field.initial".ZSLString
        default:
            break
        }
        
        
        style1.textLabel.font = fontListArray[0]
        style2.textLabel.font = fontListArray[1]
        style3.textLabel.font = fontListArray[2]
        
        style1.placeHolderLabel.font = fontListArray[0]
        style2.placeHolderLabel.font = fontListArray[1]
        style3.placeHolderLabel.font = fontListArray[2]
        
        style1.tag = 0
        style2.tag = 1
        style3.tag = 2
        
        style1.setText(signatureText)
        style2.setText(signatureText)
        style3.setText(signatureText)
        
        textField.text = signatureText
        textField.delegate = self
        textField.textColor = ZColor.primaryTextColor
        textField.backgroundColor = ZColor.bgColorWhite
        textField.returnKeyType = .done
        textField.clearButtonMode = .whileEditing
        textField.autocorrectionType = .no
        textField.layer.borderColor = ZColor.seperatorColor.cgColor
        textField.layer.borderWidth = 0.3
        
        headerView.addSubview(textField)
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: cellValueLabelX, height: 30))
        textField.leftViewMode = .always
        
        textField.snp.makeConstraints { (make) in
            make.top.equalTo(nameLabel.snp.bottom).offset(4)
            make.left.equalToSuperview().offset(-1)
            make.right.equalToSuperview().offset(0)
            make.height.equalTo(50)
        }
        
        adjustViewFrame()
        
        style1.addTarget(self, action: #selector(changeStyle(_:)), for: .touchUpInside)
        style2.addTarget(self, action: #selector(changeStyle(_:)), for: .touchUpInside)
        style3.addTarget(self, action: #selector(changeStyle(_:)), for: .touchUpInside)
        
        style1.setSelect(selectedIndexPath.row == 0)
        style2.setSelect(selectedIndexPath.row == 1)
        style3.setSelect(selectedIndexPath.row == 2)

    }
    @objc func changeStyle(_ sender : UIButton)  {
        selectedIndexPath = IndexPath(row: sender.tag, section: 0)
        style1.setSelect(selectedIndexPath.row == 0)
        style2.setSelect(selectedIndexPath.row == 1)
        style3.setSelect(selectedIndexPath.row == 2)
        let selectedIndexStr = "\(selectedIndexPath.row + 1)"
        tapToChooseLabel.text = "sign.mobile.settings.chooseSignatureStyle".ZSLString + " (\(selectedIndexStr) / 3)"

    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if view.frame.width > 1000{
            scrollview.frame  = CGRect(x: 100, y: 35, width: view.frame.width - 200, height: 180)
        }else{
            scrollview.frame  = CGRect(x: 30, y: 35, width: view.frame.width - 60, height: 180)
        }
       
        scrollview.contentSize = CGSize(width: (scrollview.frame.width * CGFloat (fontListArray.count)), height: 180)
        
        style1.frame = CGRect(x:0, y: 0, width: scrollview.frame.width, height: 180)
        style2.frame = CGRect(x: (scrollview.frame.width), y: 0, width: scrollview.frame.width , height: 180)
        style3.frame = CGRect(x: 2*(scrollview.frame.width), y: 0, width: scrollview.frame.width , height: 180)
        
        style1.layer.shadowPath = UIBezierPath(roundedRect: style1.bounds, cornerRadius: style1.layer.cornerRadius).cgPath
        style2.layer.shadowPath = UIBezierPath(roundedRect: style2.bounds, cornerRadius: style2.layer.cornerRadius).cgPath
        style3.layer.shadowPath = UIBezierPath(roundedRect: style3.bounds, cornerRadius: style3.layer.cornerRadius).cgPath

    }
    
    // MARK: UIScrollViewDelegate
    
    public override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if decelerate == false {
            let currentPage = scrollView.currentPage
            tapToChooseLabel.text = "sign.mobile.settings.chooseSignatureStyle".ZSLString + " (\(currentPage) / 3)"
        }
    }
    
    public override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage = scrollView.currentPage
         tapToChooseLabel.text = "sign.mobile.settings.chooseSignatureStyle".ZSLString + " (\(currentPage) / 3)"
    }
    

    public override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        adjustViewFrame()
    }
    func adjustViewFrame()  {
        let contentOffsetX = scrollview.contentOffset.x
        
        for view in scrollview.subviews {
            
            let t1 = view.layer.transform // Hack for avoid visual bug
            view.layer.transform = CATransform3DIdentity
            
            var distanceFromCenterX = view.frame.origin.x - contentOffsetX
            
            view.layer.transform = t1
            
            distanceFromCenterX = distanceFromCenterX * 100 / scrollview.frame.width
            
            let angle = distanceFromCenterX * self.angleRatio
            
            let offset = distanceFromCenterX;
            let fabOffset = abs(offset);
            let translateX = (scrollview.frame.width * self.translateX) * offset / 100
            let translateY = ((scrollview.frame.width * self.translateY) * (fabOffset / 100))
            let t = CATransform3DMakeTranslation(translateX, translateY, 0)
            
            view.layer.transform = CATransform3DRotate(t, (angle * .pi / 180), self.rotationX, self.rotationY, self.rotationZ)
        }
    }

}

extension ZSignSelectController : UITextFieldDelegate{
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        signatureText = textFieldText.replacingCharacters(in: range, with: string)
//        style1.textLabel.text = signatureText
//        style2.textLabel.text = signatureText
//        style3.textLabel.text = signatureText
        style1.setText(signatureText)
        style2.setText(signatureText)
        style3.setText(signatureText)
        navigationItem.rightBarButtonItem?.isEnabled = !signatureText.isEmpty

        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        style1.setText("")
        style2.setText("")
        style3.setText("")
        navigationItem.rightBarButtonItem?.isEnabled = false
        return true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return false
    }
    
}


extension UIScrollView {
   var currentPage: Int {
      return Int((self.contentOffset.x + (0.5*self.frame.size.width))/self.frame.width)+1
   }
}
