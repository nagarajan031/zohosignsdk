//
//  ZSFont.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 16/10/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZSFont : NSObject {
    
    public class func navTitleFont() -> UIFont {
        return ZSFont.headlineFont()// self.semiBoldFont(withSize: 17)
    }
    
    public class func barButtonFont() -> UIFont {
        return self.mediumFont(withSize: 17)
    }
    
    public class func createListKeyFont() -> UIFont {
        return UIFont.preferredFont(forTextStyle: .subheadline)
    }
    
    public class func createListValueFont() -> UIFont {
        return  UIFont.preferredFont(forTextStyle: .subheadline)
    }

    public class func createListKeyFontSmall() -> UIFont {
        return self.regularFont(withSize: 13)
    }

    public class func createListValueFontSmall() -> UIFont {
        return UIFont.preferredFont(forTextStyle: .footnote)
    }

    public class func listTitleFont() -> UIFont {
        return  UIFont.preferredFont(forTextStyle: .body)
    }
    
    public class func listSubTitleFont() -> UIFont {
        return UIFont.preferredFont(forTextStyle: .caption1)
    }
      
    public class func textViewFont() -> UIFont {
        return UIFont.preferredFont(forTextStyle: .callout)
    }
    
    public class func listTitleFontSmall() -> UIFont {
        return  self.regularFont(withSize: 14)
    }
    
  
    public class func buttonTitleFont() -> UIFont {
        return UIFont.preferredFont(forTextStyle: .headline)
    }
    
    public class func regularFontLarge() -> UIFont {
        return self.regularFont(withSize: 17)
    }
    
    public class func listSwipeActionButtonFont() -> UIFont {
        return self.regularFont(withSize: 13)
    }
    
    public class func tableSectionHeaderFont() -> UIFont {
        return self.mediumFont(withSize: 14)// self.regularFont(withSize: 14)
    }
    
    public class func toolTipFont() -> UIFont {
        return self.mediumFont(withSize: 12)
    }
    
    public class func mediumFontLarge() -> UIFont {
        return self.mediumFont(withSize: 17)
    }
    
    public class func popupCancelBtnFont() -> UIFont {
        return UIFont.preferredFont(forTextStyle: .body)
    }
    
    public class func popupDoneBtnFont() -> UIFont {
        return UIFont.preferredFont(forTextStyle: .headline)
    }
    
    public class func italicFont(withSize size: CGFloat) -> UIFont {
        return UIFont.italicSystemFont(ofSize: size)
    }
    
    public class func regularFont(withSize size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: .regular)
    }
    
    public class func mediumFont(withSize size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: .medium)
    }
    
    public class func semiBoldFont(withSize size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: .semibold)
    }
    
    @objc public class func boldFont(withSize size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: .bold)
    }
    
    public class func heavyFont(withSize size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: .heavy)
    }
    
    public class func lightFont(withSize size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: .light)
    }
    
    public class func signatureFont(withSize size: CGFloat) -> UIFont {
        return UIFont(name: "Yesteryear-Regular", size: size) ?? UIFont(name: "Zapfino", size: size)!
    }
    
    
    //Dynamic Font
    public class func buttonFont() -> UIFont {
        return UIFont.preferredFont(forTextStyle: .headline)
    }
    
    public class func titleFont() -> UIFont {
        return UIFont.preferredFont(forTextStyle: .title3)
    }
    
    public class func bodyFont() -> UIFont {
        return UIFont.preferredFont(forTextStyle: .body)
    }
    
    public class func headlineFont() -> UIFont {
        return UIFont.preferredFont(forTextStyle: .headline)
    }
    
    public class func subHeadlineFont() -> UIFont {
        return UIFont.preferredFont(forTextStyle: .subheadline)
    }
    
    public class func footnoteFont() -> UIFont {
        return UIFont.preferredFont(forTextStyle: .footnote)//self.regularFont(withSize: 13)
    }
    
    public class func calloutFont() -> UIFont {
        return UIFont.preferredFont(forTextStyle: .callout)
    }
    
    
}
