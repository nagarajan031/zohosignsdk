//
//  ZFileTypeViewController.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 15/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

public enum ZSFileType {
    case folderType
    case documentType
}

public protocol ZSFileTypeViewControllerDelegate: NSObjectProtocol {
    func addButtonDidClicked(forType type: ZSFileType)
    func menuItemDidClicked(forType type: ZSFileType,withIndex index: Int)
}

public class ZSFileTypeViewController: SearchResultsTableViewController {
    
    let type: ZSFileType
    var menuItems: [String] = [] {
        didSet {
            tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        }
    }
    var filteredMenuItems: [String] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    var selectedIndex: Int
    var addCellTitle: String!
    public weak var delegate: ZSFileTypeViewControllerDelegate?
    
    public init(type: ZSFileType, menuItems: [String], selectedIndex: Int) {
        self.type = type
        self.menuItems = menuItems
        self.selectedIndex = selectedIndex
        super.init(style: .grouped)
    }
    
    public convenience init(type: ZSFileType, menuItems: [String], selectedItem: String) {
        self.init(type: type, menuItems: menuItems, selectedIndex: menuItems.firstIndex(of: selectedItem) ?? 0)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerReusableCell(UITableViewCell.self)
        
        switch type {
        case .documentType:
            addCellTitle = "Add Document Type"
            title = "sign.mobile.request.requestType".ZSLString
        case .folderType:
            addCellTitle = "Add Folder"
            title = "sign.mobile.request.folder".ZSLString
        }
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonClicked))
        
        
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let addButtonItem
            = UIBarButtonItem(title: addCellTitle, style: .plain, target: self, action: #selector(addButtonClicked))
        addButtonItem.setTitleTextAttributes([NSAttributedString.Key.font: ZSFont.mediumFont(withSize: 17), NSAttributedString.Key.foregroundColor: ZColor.buttonColor], for: .normal)
        navigationController?.isToolbarHidden = false
       // navigationController?.toolbar.backgroundColor = ZColor.bgColorGray
        navigationController?.setToolbarHidden(false, animated: false)
        toolbarItems = [flexibleSpace,addButtonItem,flexibleSpace]
    }
    
    @objc func cancelButtonClicked(){
        searchController.isActive = false
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func addButtonClicked(){
        delegate?.addButtonDidClicked(forType: type)
    }
    
    public func updateMenuItems(menuItems: [String], selectedItem: String? = nil) {
        if let selectedItem = selectedItem, let selectedIndex = menuItems.firstIndex(of: selectedItem) {
            self.selectedIndex = selectedIndex
        }
        self.menuItems = menuItems
    }
    
    // MARK: - Table view data source
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching ? filteredMenuItems.count : menuItems.count
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath)
        cell.setBackgroundView()
      //  if indexPath.section == 0 && !isSearching {
//            let imageView = cell.imageView
//            let image = UIImage(named: "icn_plus")!
//            imageView?.image = image
//            imageView?.layer.borderColor = ZColor.buttonColor.cgColor
//            imageView?.layer.borderWidth = 1
//            imageView?.layer.cornerRadius = image.size.height/2
//            cell.textLabel?.text = addCellTitle
//            cell.textLabel?.textColor = ZColor.buttonColor
      //  } else {
            cell.textLabel?.font = ZSFont.regularFontLarge()
            cell.textLabel?.text = isSearching ? filteredMenuItems[indexPath.row] : menuItems[indexPath.row]
            cell.imageView?.image = nil
            cell.textLabel?.textColor = ZColor.primaryTextColor

            if selectedIndex == indexPath.row && !isSearching{
                cell.accessoryType = .checkmark
                //cell.textLabel?.textColor = ZColor.buttonColor
            } else {
                cell.accessoryType = .none
            }
            
      //  }
        return cell
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        //if indexPath.section == 0 && !isSearching{
         //   delegate?.addButtonDidClicked(forType: type)
       // } else {
            var index = indexPath.row
            if isSearching {
                let item = filteredMenuItems[indexPath.row]
                index = menuItems.firstIndex(of: item)!
            }
            delegate?.menuItemDidClicked(forType: type, withIndex: index)
            cancelButtonClicked()
       // }
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return TableviewCellSize.normal
    }
    
    public override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
}


extension ZSFileTypeViewController {
    public override func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else {return}
        filteredMenuItems = menuItems.filter{$0.range(of: searchText, options: .caseInsensitive) != nil}
    }
    
    public func willPresentSearchController(_ searchController: UISearchController) {
        searchController.searchBar.text = ""
        filteredMenuItems = menuItems
    }
}
