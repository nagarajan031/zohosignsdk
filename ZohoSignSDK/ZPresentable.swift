//
//  ZPresentable.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 10/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public protocol ZPresentable {
    var direction: PresentationDirection { get }
    var popupHeight: CGFloat { get }
    var dimmingViewAlpha: CGFloat { get }
    var navBar: PresentationNavBar { get }
    var isBlurredDimmingView: Bool { get }
    var shouldApplyCornerRadius: Bool { get }
    var cornerRadius: CGFloat { get }
    var transitionAnimationDuration: TimeInterval { get }
}

public extension ZPresentable where Self: UIViewController{
    var direction: PresentationDirection {
        return .bottom
    }
    
    var popupHeight: CGFloat {
        return 420
    }
    
    var dimmingViewAlpha: CGFloat {
        return 0.5
    }
    
    var navBar: PresentationNavBar {
        return PresentationNavBar()
    }
    
    var isBlurredDimmingView: Bool {
        return false
    }
    
    var shouldApplyCornerRadius: Bool {
        return true
    }
    
    var cornerRadius: CGFloat {
        return 10
    }
    
    var transitionAnimationDuration: TimeInterval {
        let totalScreenHeight = UIScreen.main.bounds.height
        let viewYOrigin = (recursiveSuperView(forView: view) ?? view).frame.origin.y
        let distanceToBeAnimated = totalScreenHeight - viewYOrigin
        let animationFactor = distanceToBeAnimated / 250
        
        return TimeInterval((animationFactor < 250 ? 1 : animationFactor) * 0.3)
    }
}

extension ZPresentable {
    public var bottomMargin: CGFloat {
        return DeviceType.isIphoneXAndAbove ? 34 : 10
    }
    
    public func recursiveSuperView(forView view: UIView) -> UIView?{
        return !(view.superview is DragPresentationView) ? recursiveSuperView(forView: view.superview!) : view.superview
    }
}
