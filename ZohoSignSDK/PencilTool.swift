//
//  PencilTool.swift
//  SignatureView
//
//  Created by somesh-8758 on 13/02/20.
//  Copyright © 2020 Somesh-8758. All rights reserved.
//

import UIKit

class PencilTool: SketchTool {
    var points: [SketchPath]
    private var allPoints: [SketchPath]
    private var images: [UIImage]
    
    override var lineColor: UIColor {
        willSet {
            setupImages()
        }
    }
    
    override init() {
        allPoints = .init()
        points = .init()
        images = .init()
        super.init()
        lineCapStyle = .round
        setupImages()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw() {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.setBlendMode(.multiply)
        
        let inset = _lineWidth/2
        for (index,sketchPath) in points.enumerated() { //TODO: change to enumerated for each
            let force = sketchPath.force * 0.7 + 0.3
            context.setAlpha(force)
            
            let sketchPoint = sketchPath.point
            let r = index%8
            let maskedImage = images[r]
            context.draw(maskedImage.cgImage!, in: CGRect(x: sketchPoint.x-inset, y: sketchPoint.y-inset, width: _lineWidth, height: _lineWidth))
        }
    }
    
    override func touchesBegin(_ point: CGPoint, with force: CGFloat) {
        counter = 0
        controlPoints.insert(point, at: counter)
    }
    
    @discardableResult
    override func touchesMoved(_ point: CGPoint, with force: CGFloat) -> CGRect {
        var bounds: CGRect = .zero
        if controlPoints[counter].fuzzyEqual(with: point, by: 1.0) {
            return bounds
        }
        
        counter += 1
        controlPoints.insert(point, at: counter)
        
        if counter == 2 {
            let mid1 = controlPoints[1] / controlPoints[0]
            let mid2 = controlPoints[2] / controlPoints[1]
            
            var distance = lenthBezier(mid1, controlPoints[1], mid2)
            distance = distance * (2/_lineWidth) * 2
            
            for i in 0..<Int(distance) {
                let t = CGFloat(i)/distance
                let newPoint = BezierTool.normalisedPoint(mid1, controlPoints[1], mid2, withConstant: t)
                
                let sketchPath = SketchPath(point: newPoint, force: force)
                points.append(sketchPath)
            }
            
            let subPath = CGMutablePath()
            subPath.move(to: mid1)
            subPath.addQuadCurve(to: mid2, control: controlPoints[1])
            bounds = subPath.boundingBox
            
            (controlPoints[0],controlPoints[1]) = (controlPoints[1],controlPoints[2])
            counter = 1
        }
        
        return bounds
    }
    
    @discardableResult
    override func touchesEnded(_ point: CGPoint, with force: CGFloat) -> CGRect {
        var bounds: CGRect = .zero
        if counter == 0 {
            let sketchPath = SketchPath(point: point, force: force)
            points.append(sketchPath)
            
            let subPath = CGMutablePath()
            subPath.move(to: controlPoints[0])
            subPath.addLine(to: point)
            
            bounds = subPath.boundingBox
        } else {
            bounds = touchesMoved(point, with: force)
        }
        counter = 0
        return bounds
    }
    
    override func showPreview(_ points: [CGPoint]) {
        if points.count > 2 {
            let previousPoint = points[0]
            touchesBegin(previousPoint, with: 0.0)
            for point in points[1..<points.count] {
                touchesMoved(point, with: 0.0)
            }
        }
    }
    
    func removePoints() {
        if allPoints.count > 0 {
            allPoints.append(contentsOf: points)
            points = allPoints
        }
        allPoints.removeAll()
    }
    
    func resetPoints() {
        allPoints.append(contentsOf: points)
        points.removeAll()
    }
    
    private func setupImages() {
        images.removeAll()
        let imageName: String = _lineWidth < 8 ? "pencil_stroke_small" : "pencil_stroke"
        if let coloredImage = UIImage(named: imageName)?.colored(with: lineColor) {
            images.append(coloredImage)
            let rotationAngle: [CGFloat] = [45,90,135,180,225,270,315]
            for angle in rotationAngle {
                if let rotatedImage = coloredImage.rotate(to: angle) {
                    images.append(rotatedImage)
                }
            }
        }
    }
    
    private func lenthBezier(_ midPoint1: CGPoint,_ p1Point: CGPoint,_ midPoint2: CGPoint) -> CGFloat {
        var prevPoint: CGPoint = .zero
        var distance: CGFloat = 0
        for i in 1..<10 {
            let t: CGFloat = CGFloat(i/10)
            let currentPoint = BezierTool.normalisedPoint(midPoint1, p1Point, midPoint2, withConstant: t)
            distance += prevPoint ~> currentPoint
            prevPoint = currentPoint
        }
        return distance
    }
    
    private func normalisedPoint(_ point1: CGPoint,_ point2: CGPoint,_ point3: CGPoint,withConstant t: CGFloat) -> CGPoint {
        return (point1 * pow(1-t, 2.0)) +
        (point2 * (2.0*(1-t) * t)) +
        (point3 * (t*t))
    }
}

extension UIImage {
    
    func colored(with color: UIColor) -> UIImage? {
        guard let maskedImage = cgImage,
            let context = UIGraphicsGetCurrentContext()
            else { return nil }
        
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContext(rect.size)
        
        context.clip(to: rect, mask: maskedImage)
        context.setFillColor(color.cgColor)
        context.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func rotate(to angle: CGFloat) -> UIImage? {
        guard let maskedImage = cgImage,
        let context = UIGraphicsGetCurrentContext()
        else { return nil }
        
        let rotatedViewBox = UIView(frame: CGRect(origin: .zero, size: size))
        let transform = CGAffineTransform(rotationAngle: angle.rad())
        rotatedViewBox.transform = transform
        let rotatedViewSize = rotatedViewBox.frame.size
        
        UIGraphicsBeginImageContext(rotatedViewSize)
        
        context.translateBy(x: rotatedViewSize.width/2, y: rotatedViewSize.height/2)
        context.rotate(by: angle.rad())
        context.scaleBy(x: 1.0, y: -1.0)
        context.draw(maskedImage, in: CGRect(x: -size.width/2, y: -size.height/2, width: size.width, height: size.height))
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}
