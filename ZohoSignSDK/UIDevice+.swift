//
//  UIDevice+.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 03/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import Foundation
import LocalAuthentication
import AudioUnit

public extension UIDevice{
    
    class func isTouchIDAvailable() -> Bool {
        var laContext = LAContext()

        var error: NSError?
        let hasTouchId = laContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)
        return hasTouchId
    }
    
    
    class func isCameraAvailable() -> Bool {
        #if targetEnvironment(macCatalyst)
        return false
        #else
        return UIImagePickerController.isSourceTypeAvailable(.camera)
        #endif
    }
    
   
}
