//
//  PenTool.swift
//  SignatureView
//
//  Created by somesh-8758 on 13/02/20.
//  Copyright © 2020 Somesh-8758. All rights reserved.
//

import UIKit

class PenTool: SketchTool {
    
    override func draw() {
        guard let context = UIGraphicsGetCurrentContext() else {return}
        context.addPath(path)
        context.setLineCap(.round)
        context.setStrokeColor(self.lineColor.cgColor)
        context.setBlendMode(.normal)
        context.setAlpha(lineAlpha)
        
        subPaths.forEach { (sketchPath) in
            let subPath = sketchPath.path
            let lineWidthWithForce = (sketchPath.force * _lineWidth) + _lineWidth
            context.setLineWidth(lineWidthWithForce)
            context.addPath(subPath)
            context.strokePath()
        }
    }
    
    override func touchesBegin(_ point: CGPoint, with force: CGFloat) {
        counter = 0
        controlPoints[0] = point
    }
    
    @discardableResult
    override func touchesMoved(_ point: CGPoint, with force: CGFloat) -> CGRect{
        var bounds: CGRect = .zero
        if controlPoints[counter].fuzzyEqual(with: point, by: 1.0) {
            return bounds
        }
        counter += 1
        controlPoints[counter] = point
        
        if counter == 2 {
            let mid1 = controlPoints[1]/controlPoints[0]
            let mid2 = controlPoints[2]/controlPoints[1]
            let subPath = CGMutablePath()
            subPath.move(to: mid1)
            subPath.addQuadCurve(to: mid2, control: controlPoints[1])
            bounds = subPath.boundingBox
            
            let sketchPath = SketchPath(path: subPath, force: force)
            subPaths.append(sketchPath)
            
            // swap points and get ready to handle the next segment
            (controlPoints[0] , controlPoints[1]) = (controlPoints[1] , controlPoints[2])
            counter = 1
        }
        
        return bounds
    }
    
    @discardableResult
    override func touchesEnded(_ point: CGPoint, with force: CGFloat) -> CGRect {
        var bounds: CGRect = .zero
        
        if counter == 0 {
            let subPath = CGMutablePath()
            subPath.move(to: controlPoints[0])
            subPath.addLine(to: point)
            bounds = subPath.boundingBox
            
            let sketchPath = SketchPath(path: subPath, force: force)
            subPaths.append(sketchPath)
        } else {
            bounds = self.touchesMoved(point, with: force)
        }
        
        counter = 0
        return bounds
    }
    
    override func showPreview(_ points: [CGPoint]) {
        if points.count > 1 {
            let previousPoint = points[0]
            touchesBegin(previousPoint, with: 0.0)
            
            for point in points[1..<points.count] {
                touchesMoved(point, with: 0.0)
            }
        }
    }
}
