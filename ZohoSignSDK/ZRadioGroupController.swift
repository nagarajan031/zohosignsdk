//
//  ZRadioGroupController.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 24/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import UIKit

protocol ZRadioGroupControllerdelegate: NSObjectProtocol {
    func radioGroupCreated(withValue radiogroup: RadioGroup)
    func radioGroupUpdated(associatedWith view: ZRadioGroupView)
    func deleteRadiogroup(associatedWith view: ZRadioGroupView)
}

class ZRadioGroupController: UITableViewController {
    
    private var radioGroup: RadioGroup!
    private var isEditMode: Bool = false
    private var isValuesChanged: Bool = false
    weak var associatedView: ZRadioGroupView?
    public weak var delegate: ZRadioGroupControllerdelegate?
    private var selectedIndexPath: IndexPath?
    
    override init(style: UITableView.Style) {
        super.init(style: style)
    }
    
    convenience init(radioGroup: RadioGroup){
        self.init(style: .grouped)
        self.radioGroup = radioGroup.deepCopy()
        isEditMode = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "sign.mobile.request.radioGroup".ZSLString
        view.backgroundColor = ZColor.bgColorGray
        navigationController?.view.backgroundColor = ZColor.bgColorGray
        navigationController?.navigationBar.barTintColor = ZColor.bgColorGray
        
        if DeviceType.isIpad {
            preferredContentSize = CGSize(width: 340, height: 500)
        } else {
            navigationItem.leftBarButtonItem = ZSBarButtonItem.barButton(type: .cancel, target: self, action: #selector(cancelButtonPressed))
        }

        
        tableView.rowHeight = TableviewCellSize.normal
        tableView.registerReusableCell(ZTextFieldCell.self)
        tableView.registerReusableCell(UITableViewCell.self)
        tableView.showsVerticalScrollIndicator = false
        if radioGroup.isNull {
            let radio1 = String(format: "%@1", "sign.mobile.field.radio".ZSLString)
            let radio2 = String(format: "%@2", "sign.mobile.field.radio".ZSLString)

            radioGroup = RadioGroup(name: "sign.mobile.request.radioGroup".ZSLString,values: [RadioValues(name: radio1),RadioValues(name: radio2)])
        }
        
        navigationItem.rightBarButtonItem = ZSBarButtonItem.barButton(type: .done, target: self, action: #selector(doneButtonPressed))
        updateBarButton()
    }
    
    @objc private func cancelButtonPressed() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func doneButtonPressed() {
        guard !radioGroup.name.isEmpty else {
            //TODO: Show error
            return
        }
        for value in radioGroup.values {
            if value.name.isEmpty {
                //TODO: show some error
                return
            }
        }
        
        if isEditMode {
            if isValuesChanged, let associatedView = associatedView {
                associatedView.signField.radioGroup = radioGroup
                associatedView.reloadRadioItems()
                delegate?.radioGroupUpdated(associatedWith: associatedView)
            }
        } else {
            delegate?.radioGroupCreated(withValue: radioGroup)
        }
        
        cancelButtonPressed()
    }
    
    @objc private func addButtonPressed() {
        isValuesChanged = true
        radioGroup.values.append(RadioValues(name: "Radio\(radioGroup.values.count+1)"))
        tableView.insertRows(at: [IndexPath(row: radioGroup.values.count-1, section: 1)], with: .automatic)
        updateBarButton()
    }
    
    @objc private func switchValueChanged(_ sender: UISwitch) {
        isValuesChanged = true
        if sender.tag == 0 {
            radioGroup.isOptional = sender.isOn
        } else {
            radioGroup.isReadOnly = sender.isOn
        }
    }
    
    private func updateBarButton() {
        navigationItem.rightBarButtonItem?.isEnabled = (radioGroup.values.count > 0)
    }
}

extension ZRadioGroupController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return isEditMode ? 4 : 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0,3:
            return 1
        case 1:
            return radioGroup.values.count
        case 2:
            return 2
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "sign.mobile.field.name".ZSLString
        case 1:
            return "RadioGroup values" //TODO: localise
        case 2:
            return " "
        default:
            return nil
        }
    }
    
    public override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 0,2:
            return 20
        case 1:
            return 50
        case 3:
            return 40
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 1 {
            let sectionFooterView  =  UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
            sectionFooterView.backgroundColor = ZColor.bgColorWhite
            
            let addOptionButton = ZSButton()
            addOptionButton.frame = CGRect(x: 18, y: 0, width: tableView.frame.height - 32, height: 50)
            addOptionButton.titleLabel?.font = ZSFont.mediumFontLarge()
            addOptionButton.contentHorizontalAlignment = .left
            addOptionButton.addTarget(self, action: #selector(addButtonPressed), for: .touchUpInside)
            addOptionButton.setImage(ZSImage.tintColorImage(withNamed: "plus_icon", col: ZColor.buttonColor), for: UIControl.State.normal)
            addOptionButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
            addOptionButton.setTitleColor(ZColor.buttonColor, for: .normal)
            addOptionButton.setTitle("sign.mobile.field.dropdown.addOption".ZSLString, for: .normal)
            sectionFooterView.addSubview(addOptionButton)
            return sectionFooterView
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        weak var weakSelf = self
        
        switch indexPath.section {
        case 0:
            let cell: ZTextFieldCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.selectionStyle = .none
            cell.setTextField(placeholder: "sign.mobile.request.enterYourText".ZSLString, value: radioGroup.name, editable: true)
            cell.textFieldShouldChangeCharactersIn = {(_ , inputText) in
                weakSelf?.isValuesChanged = true
                weakSelf?.radioGroup.name = inputText
            }
            cell.showDeleteButton = false
            cell.maxLimit = kSINGLELINE_MAX_COUNT
            cell.checkForTextFieldRegex(regex: ZS_Regex_ForName, showPopInView: self.view)
            cell.addCustomView() //fix for tableview reloading issues
            return cell
        case 1:
            let cell: ZTextFieldCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.selectionStyle = .none
            let radioItemName = String(format: "%@%i", "sign.mobile.field.radio".ZSLString,indexPath.row+1)

            cell.setTextField(placeholder: radioItemName, value: radioGroup.values[indexPath.row].name, editable: true)
            cell.showDeleteButton = true
            cell.checkForTextFieldRegex(regex: ZS_Regex_ForName, showPopInView: self.view)
            cell.maxLimit = kSINGLELINE_MAX_COUNT
            cell.deleteButtonTapHandler = {
                weakSelf?.isValuesChanged = true
                let value = weakSelf?.radioGroup.values[indexPath.row]
                value?.subFieldId.safelyUnwrap({ (subFieldId) in
                    weakSelf?.radioGroup.deletedSubFields.append(subFieldId)
                })
                weakSelf?.radioGroup.values.remove(at: indexPath.row)
                weakSelf?.tableView.reloadSections(IndexSet(integer:1), with: .automatic)
                weakSelf?.updateBarButton()
            }
            cell.textFieldShouldChangeCharactersIn = {_,text in
                weakSelf?.isValuesChanged = true
                weakSelf?.radioGroup.values[indexPath.row].name = text
            }
            
            
            let customView = UIButton()
            customView.tag = indexPath.row
            customView.addTarget(self, action: #selector(radioButtonValueChanged(_:)), for: .touchUpInside)
            
            let radioButtonView = ZRadioButton(frame: CGRect(origin: .zero, size: CGSize(width: 20, height: 20)))
            radioButtonView.isUserInteractionEnabled = false
            customView.addSubview(radioButtonView)
            radioButtonView.snp.makeConstraints { (make) in
                make.centerY.equalToSuperview()
                make.leading.equalTo(cellValueLabelX+2)
                make.height.width.equalTo(20)
            }
            
            radioButtonView.isSelected = radioGroup.values[indexPath.row].defaultValue
            if radioButtonView.isSelected {
                selectedIndexPath = indexPath
            }
            
            cell.addCustomView(customLeftView: customView)
            
            return cell
        case 2,3:
            let cell: UITableViewCell = tableView.dequeueReusableCell(indexPath: indexPath)
            
            cell.backgroundColor = ZColor.bgColorWhite
            if indexPath.section == 2 {
                let uiSwitch = UISwitch()
                uiSwitch.tag = indexPath.row
                uiSwitch.addTarget(self, action: #selector(switchValueChanged(_:)), for: .valueChanged)
                uiSwitch.onTintColor = ZColor.buttonColor
                cell.accessoryView = uiSwitch
                cell.textLabel?.textAlignment = .left
                cell.textLabel?.textColor = ZColor.primaryTextColor
                cell.selectionStyle = .none
                if indexPath.row == 0 {
                    uiSwitch.isOn = radioGroup.isOptional
                    cell.textLabel?.text = "sign.mobile.common.optional".ZSLString
                } else {
                    uiSwitch.isOn = radioGroup.isReadOnly
                    cell.textLabel?.text = "sign.mobile.common.readonly".ZSLString
                }
            } else {
                cell.textLabel?.text = "sign.mobile.common.delete".ZSLString
                cell.textLabel?.textColor = ZColor.redColor
                cell.textLabel?.textAlignment = .center
                cell.accessoryView = nil
                cell.selectionStyle = .default
                cell.setBackgroundView()
            }
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 3 {
            associatedView.safelyUnwrap { delegate?.deleteRadiogroup(associatedWith: $0) }
        }
        
    }

    @objc private func radioButtonValueChanged(_ sender: UIButton) {
        let radioButton = sender.subviews.first! as! ZRadioButton
        let index = sender.tag
        isValuesChanged = true
        selectedIndexPath.safelyUnwrap { (selectedIndexPath) in
            radioGroup.values[selectedIndexPath.row].defaultValue = false
            tableView.reloadRows(at: [selectedIndexPath], with: .automatic)
        }
        
        radioButton.isSelected.toggle()
        radioGroup.values[index].defaultValue = radioButton.isSelected
        
        if radioButton.isSelected {
            self.selectedIndexPath = IndexPath(row: index, section: 1)
        } else {
            self.selectedIndexPath = nil
        }
        
    }
}

extension ZRadioGroupController: ZPresentable { }
