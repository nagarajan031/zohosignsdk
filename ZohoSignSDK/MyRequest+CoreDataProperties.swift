//
//  MyRequest+CoreDataProperties.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 10/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//
//

import Foundation
import CoreData


extension MyRequest {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MyRequest> {
        return NSFetchRequest<MyRequest>(entityName: "MyRequest")
    }

    @NSManaged public var actionId: String?
    @NSManaged public var actions: Data?
    @NSManaged public var actionStatus: String?
    @NSManaged public var actionType: String?
    @NSManaged public var desc: String?
    @NSManaged public var expireByTime: NSNumber?
    @NSManaged public var inpersonSigners: Data?
    @NSManaged public var isAutoFillViaSiriSelected: Bool
    @NSManaged public var isBlocked: Bool
    @NSManaged public var isDeletedRequest: Bool
    @NSManaged public var isExpiring: NSNumber?
    @NSManaged public var isExpired: NSNumber?
    @NSManaged public var isReviewCompleted: Bool
    @NSManaged public var isVerifyRecipient: NSNumber?
    @NSManaged public var myRequestId: String?
    @NSManaged public var myRequestStatus: String?
    @NSManaged public var name: String?
    @NSManaged public var notes: String?
    @NSManaged public var ownerName: String?
    @NSManaged public var recipientEmail: String?
    @NSManaged public var recipientName: String?
    @NSManaged public var recipientPhoneNumber: String?
    @NSManaged public var requestedTime: NSNumber?
    @NSManaged public var requesterEmail: String?
    @NSManaged public var requesterName: String?
    @NSManaged public var requestId: String?
    @NSManaged public var requestStatus: String?
    @NSManaged public var sectionIdentifierRequestedTime: Int64
    @NSManaged public var signId: String?
    @NSManaged public var statelessAuthCode: String?
    @NSManaged public var signerSettings: Data?
    @NSManaged public var thumbnailPath: String?
    @NSManaged public var thumbnailStr: String?
    @NSManaged public var totalPages: NSNumber?
    @NSManaged public var typeName: String?
    @NSManaged public var validity: Int64
    @NSManaged public var verificationType: String?
    @NSManaged public var documents: Set<Documents>?
    @NSManaged public var filters: Set<Filter>?

}

// MARK: Generated accessors for documents
extension MyRequest {

    @objc(addDocumentsObject:)
    @NSManaged public func addToDocuments(_ value: Documents)

    @objc(removeDocumentsObject:)
    @NSManaged public func removeFromDocuments(_ value: Documents)

    @objc(addDocuments:)
    @NSManaged public func addToDocuments(_ values: NSSet)

    @objc(removeDocuments:)
    @NSManaged public func removeFromDocuments(_ values: NSSet)

}

// MARK: Generated accessors for filters
extension MyRequest {

    @objc(addFiltersObject:)
    @NSManaged public func addToFilters(_ value: Filter)

    @objc(removeFiltersObject:)
    @NSManaged public func removeFromFilters(_ value: Filter)

    @objc(addFilters:)
    @NSManaged public func addToFilters(_ values: NSSet)

    @objc(removeFilters:)
    @NSManaged public func removeFromFilters(_ values: NSSet)

}
