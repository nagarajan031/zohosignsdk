//
//  UIViewController+.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 24/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public extension UIViewController{
    func showAlert(withMessage message: String) {
        let alertController = UIAlertController(title:nil, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "sign.mobile.common.ok".ZSLString, style: .cancel, handler: nil)
        
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func isPortrait() -> Bool {
        if DeviceType.isMac {return false}
        let orientation = UIDevice.current.orientation
        switch orientation {
        case .portrait, .portraitUpsideDown:
            return true
        case .landscapeLeft, .landscapeRight:
            return false
        default: // unknown or faceUp or FaceDown
            return view.frame.width < view.frame.height
        }
    }
    
    func showActivityHUD()
    {
        ZSProgressHUD.show(inView: view)
        //        ZSProgressHUD.show(inView: self.view)
    }
    
    func dismissActivityHUD()  {
        ZSProgressHUD.dismiss()
        //        ZSProgressHUD.dismiss()
    }
    
    func handleDataManagererror(_ error: ZSError?)  {
        //        ZSProgressHUD.dismiss()
        ZSProgressHUD.dismiss()
        if (error?.errorType == .noNetworkError) {
            ZSNotify.sharedNotify.showNoNetworkAlert(error?.localizedDescription ?? "sign.mobile.alert.noInternetConnection".ZSLString)
            return
        }
       
        if ((error?.errorType == .noDataError) || (error?.errorType == .timeOut) || (error?.errorType == .cancel)) {return}
        
        let alertController = UIAlertController(title:nil, message: error?.localizedDescription, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "sign.mobile.common.ok".ZSLString, style: .cancel, handler: nil)
        
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    
    func dismissViewController(isSuccessAnimation : Bool)  {
        if #available(iOS 13, *) {
            parent?.view.window?.dismiss(with: isSuccessAnimation ? .commit : .decline, on: self)
        }
        self.dismiss(animated: true, completion: nil)
    }

    
    func trackEventTouchbar(for action: String) {
        ZSignKit.shared.trackEvent("TouchBar - \(String(describing: self)) - action \(action)")
    }
}
