//
//  ZSButton.swift
//  Action extn
//
//  Created by Nagarajan S on 19/01/17.
//  Copyright © 2017 Zoho. All rights reserved.
//

import UIKit

open class ZSButton: UIButton {
    
    required public init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
//        if #available(iOS 13.0, *) {
//            if UITraitCollection.current.userInterfaceStyle == .dark{
//
//            }
//        }
        
        self.alpha  =   self.isHighlighted ? 0.5 : 1;
        if !isEnabled { self.alpha  =   0.4;}
    }
    
    
    public func setShadow(cornerRadius: CGFloat = 3)  {
        self.layer.cornerRadius =  cornerRadius
        if #available(iOS 13, *){
            traitCollection.performAsCurrent {
                self.layer.shadowColor = ZColor.greyColor.cgColor
            }
        } else {
            self.layer.shadowColor = ZColor.greyColor.cgColor
        }
        self.layer.shadowOffset = CGSize(width: 0, height: 0 )
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity =   0.5
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius).cgPath
    }
    
    public override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection){
            traitCollection.performAsCurrent {
                self.layer.shadowColor = ZColor.greyColor.cgColor
            }
        }
    }
}


public class ZSActionButton: UIButton {
    
    public var bgColor : UIColor?
    public var highlightedBgColor : UIColor?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundColor  =   self.isHighlighted ? highlightedBgColor : bgColor;
        if !isEnabled { self.alpha  =   0.4;}
    }
}
