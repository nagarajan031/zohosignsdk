//
//  ZSFileManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 24/07/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation
public enum FileManagerFolderType {
    case cache
    case temporary
    case documents
    case shared
}


public class ZSFileManager : NSObject{
    let fileManager = FileManager.default
    @objc public static let shared = ZSFileManager()

    public lazy var sharedDirectoryPath : String = {
        
        var sharedUrl = fileManager.containerURL(forSecurityApplicationGroupIdentifier: CommonUtils.signAppSharedGroupID())
        
        var resourceValues = URLResourceValues()
        resourceValues.isExcludedFromBackup = true
        try? sharedUrl?.setResourceValues(resourceValues)
        
        return sharedUrl?.path ?? documentsDirectoryPath
    }()
    
    public lazy var temporaryDirectoryPath : String = {
        return NSTemporaryDirectory()
   }()
    
    public lazy var documentsDirectoryPath : String = {
       return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
   }()
    
    public lazy var cacheDirectoryPath : String = {
       return NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first!
   }()
    
    
    public func folderPath(_ type : FileManagerFolderType) -> String {
        switch type {
            case .cache:
                return cacheDirectoryPath
            case .temporary:
                return temporaryDirectoryPath
            case .documents:
                return documentsDirectoryPath
            case .shared:
                return sharedDirectoryPath
        }
    }
    
    public func getSubFolder(type : FileManagerFolderType,name : String ) -> String {
        var topFolder = cacheDirectoryPath
        switch type {
            case .cache:
                topFolder =  cacheDirectoryPath
            case .temporary:
                topFolder =  temporaryDirectoryPath
            case .documents:
                topFolder =  documentsDirectoryPath
            case .shared:
                topFolder =  sharedDirectoryPath
        }
        
        return String(format: "%@/%@", topFolder,name)
    }
    
    public func clearAllTemporaryCaches()  {
    
        let cachePath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        if let cacheDir = cachePath.first{
            let downloadsDir = String(format: "%@/downloads", cacheDir)
            clearAllCache(atDirectory: downloadsDir)
            
            let uncacheLocation = String(format: "%@/%@", cacheDir,UnCachedFileSaveLocation)
            clearAllCache(atDirectory: uncacheLocation)
        }
    
        clearAllCache(atDirectory: temporaryDirectoryPath)
    }
    
    public func clearAllLocalDownloadedFiles() {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths.first
        clearAllCache(atDirectory: documentsDirectory)


        let cachePath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        let cacheDir = cachePath.first
        clearAllCache(atDirectory: cacheDir)

        clearAllCache(atDirectory: sharedDirectoryPath)

        clearAllCache(atDirectory: temporaryDirectoryPath)
    }

    public func clearAllCache(atDirectory directory: String?) {

        var files: [String]? = nil
        //do {
            files = try? fileManager.contentsOfDirectory(atPath: directory ?? "")
            for filename in files ?? [] {
                if (filename == "db") || filename.contains(".sqlite") {
                    continue
                }

                try? fileManager.removeItem(atPath: directory?.appendingFormat("/%@", filename) ?? "")
            }
//        }catch {
//            ZError("Unexpected error: \(error)")
//        }

       
    }
    
    public func removeSqLiteFile(_ sqliteFileName: String) throws {
        let filePath = String(format: "%@/%@", documentsDirectoryPath, sqliteFileName)
        try fileManager.removeItem(atPath: filePath)
    }
    
    //MARK: - GET Files & Folders
    @objc public func getFolderPathDirectory(withName folderName: String?, isCacheDirectory isCacheDir: Bool) -> String? {
        var error: Error?
        let paths = NSSearchPathForDirectoriesInDomains((isCacheDir) ? .cachesDirectory : .documentDirectory, .userDomainMask, true)

        let documentsDirectory = paths.first

        
        if folderName == nil {
            return documentsDirectory
        }
        
        guard let dataPath = documentsDirectory?.appendingFormat("/%@", folderName!) else {
            return documentsDirectory
        }

        var fileURL = URL(fileURLWithPath: dataPath)
        var resourceValues:URLResourceValues = URLResourceValues()
        resourceValues.isExcludedFromBackup = true

        
        do {
    
            if !fileManager.fileExists(atPath: dataPath) {
                try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: true, attributes: [
                FileAttributeKey.protectionKey: FileProtectionType.complete
                ])
            }
            try fileURL.setResourceValues(resourceValues)
        }
        catch {
            ZError("Unexpected error: \(error)")
        }
    

        return dataPath
    }

    public func writePdfToTempDir(_ data: Data,_ filename: String) -> String{
        
        let fileName = filename + ".pdf"
        let newFilePathForPDF = temporaryDirectoryPath.appending(fileName)
        do {
            if fileManager.fileExists(atPath: newFilePathForPDF) {
                try fileManager.removeItem(atPath: newFilePathForPDF)
            }
            do {
                try data.write(to: URL(fileURLWithPath: newFilePathForPDF))
                return newFilePathForPDF
            } catch{
                ZInfo(error)
            }
        }
        catch{
            ZInfo("error")
        }
        return ""
    }
   
    
    //MARK: - Helpers
    func createSubFolder(parentFilePath : String, subFolder : String) -> (folderPath : String,folderExists : Bool) {
        
        let folderPath = parentFilePath.appending("/" + subFolder)
        
        if !fileManager.fileExists(atPath: folderPath) {
            try? fileManager.createDirectory(atPath: folderPath, withIntermediateDirectories: true, attributes: [FileAttributeKey.protectionKey: FileProtectionType.complete])
        }
        
        return (folderPath, fileManager.fileExists(atPath: folderPath))
    }
}
