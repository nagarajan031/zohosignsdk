//
//  ZSImage.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 26/09/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZSImage: UIImage {
    @objc public static func buttonImage(named: String) -> UIImage!{
        if let image = super.init(named: named) {
            return tintColorImage(image, col: ZColor.buttonColor)
        }
        return UIImage()
    }
    
    @objc public static func tintColorImage(_ img: UIImage?, col: UIColor) -> UIImage!{
        if let img = img {
            return drawImage(img, color: col, withRendringMode: .alwaysTemplate)
        }
        return UIImage()
    }
    
    @objc public static func tintColorImage(withNamed name:String, col color: UIColor) -> UIImage!{
        if let image = super.init(named: name) {
            return tintColorImage(image, col: color)
        }
        return UIImage()
    }
    
    @objc public static func tintColorSystemImage(withNamed name: String, col color: UIColor, with mode: UIImage.RenderingMode) -> UIImage! {
        if #available(iOS 13, *),
            let image = super.init(systemName: name) {
            return drawImage(image,color: color, withRendringMode: mode)
        }
        return UIImage()
    }
    
    @objc public static func image(fromColor color: UIColor, inRect rect: CGRect) -> UIImage!{
        UIGraphicsBeginImageContext(rect.size);
        let context = UIGraphicsGetCurrentContext();
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image;
    }
    
    private static func drawImage(_ image: UIImage,color: UIColor ,withRendringMode mode: UIImage.RenderingMode) -> UIImage!{
        var newImage: UIImage! = image.withRenderingMode(mode)
        UIGraphicsBeginImageContextWithOptions(image.size, false, newImage.scale)
        color.set()
        newImage?.draw(in: CGRect(origin: .zero, size: CGSize(width: image.size.width, height: newImage.size.height)))
        newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
}

extension UIImage {
    public static var scanImage: UIImage = {
        return getImageFrom(appImageName: "action_scan", systemImageName: "doc.text.viewfinder")
    }()
    
    public static var zohoDocsImage: UIImage = {
        return getImageFrom(appImageName: "action_zoho", systemImageName: "z.square")
    }()
    
    public static var photoGalleryImage: UIImage = {
        return getImageFrom(appImageName: "action_photo", systemImageName: "photo")
    }()
    
    public static var browseImage: UIImage = {
        return getImageFrom(appImageName: "action_localfile", systemImageName: "folder")
    }()
    
    public static var infoImage: UIImage = {
        return getImageFrom(appImageName: "settings_about", systemImageName: "info.circle", tintColor: ZColor.primaryTextColor)
    }()
    
    public static var starImage: UIImage = {
        return getImageFrom(appImageName: "settings_rate", systemImageName: "star", tintColor: ZColor.primaryTextColor)
    }()
    
    public static var powerImage: UIImage = {
        return getImageFrom(appImageName: "settings_signout", systemImageName: "power", tintColor: ZColor.redColor)
    }()
    
    public static var addImage: UIImage = {
        return getImageFrom(appImageName: "create_add", systemImageName: "plus", tintColor: ZColor.greyColor)
    }()
    
    public static var moreImage: UIImage = {
        return getImageFrom(appImageName: "create_more", systemImageName: "ellipsis", tintColor: ZColor.greyColor)
    }()
    
    public static var moveImage: UIImage = {
        return getImageFrom(appImageName: "move_icon", systemImageName: "line.horizontal.3")
    }()
    
    public static func getImageFrom(appImageName appImage: String, systemImageName systemImage: String,tintColor color: UIColor = ZColor.buttonColor) -> UIImage{
        if #available(iOS 13, *){
            return ZSImage.tintColorSystemImage(withNamed: systemImage, col: color, with: .alwaysTemplate)
        } else {
            return ZSImage.tintColorImage(withNamed: appImage, col: color)
        }
    }
}

