//
//  LogHelper.swift
//  ZohoSocial
//
//  Created by Akaash Dev on 24/10/17.
//  Copyright © 2017 Akaash Dev. All rights reserved.
//

private func printArrayOfMessages(messages:Array<Any>)
{
    for message in messages {
        
        if message is Array<Any> {
            
            printArrayOfMessages(messages: message as! Array)
            continue
            
        }
        
        print(message, separator: " ", terminator: " ")
        
    }
    
    print("", separator: "", terminator: "\n")
}

private func ZLog(_ logType:String = "🖌", messages: Any..., classname: String = " ", function: String = #function, line:Int = #line) {

    #if DEBUG
    return
    #endif
    
    #if !DEBUG
        return
    #endif

    let fileName = classname.components(separatedBy: "/").last ?? ""
    DispatchQueue.global(qos: .background).async {
        print(logType, fileName, ":", function, "in line:",line, "-", separator: " ", terminator: " ")
    }
    
    printArrayOfMessages(messages: messages)
}

public func ZError(_ messages: Any..., classname: String = #file, function: String = #function, line:Int = #line) {
    
    ZLog("❌", messages: messages, classname: classname, function: function, line: line)
    
}

public func ZWarning(_ messages: Any..., classname: String = #file, function: String = #function, line:Int = #line) {
    
    ZLog("⚠️", messages: messages, classname: classname, function: function, line: line)
    
}

public func ZInfo(_ messages: Any..., classname: String = #file, function: String = #function, line:Int = #line) {
    
    ZLog("ℹ️", messages: messages, classname: classname, function: function, line: line)
    
}
