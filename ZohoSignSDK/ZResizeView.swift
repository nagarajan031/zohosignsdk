//
//  ZResizeView.swift
//  SignEditor
//
//  Created by Nagarajan S on 25/04/19.
//  Copyright © 2019 Naga. All rights reserved.
//

import UIKit
import SnapKit
import MenuItemKit

public protocol ZResizeViewDelegate : class{
    func resizeViewDidBeginEditing(_ resizeView : ZResizeView)
    func resizeViewDidChange(_ resizeView : ZResizeView)
    func resizeViewDidChangeTextProperties(_ resizeView : ZResizeView)
    func resizeViewDidEndEditing(_ resizeView : ZResizeView)
    func resizeViewDeleteButtonTapped(_ resizeView : ZResizeView)
    func resizeViewDidTapped(_ resizeView : ZResizeView)
    func resizeViewDidEditButtonTapped(_ resizeView : ZResizeView)
    func resizeViewDidChangeSubtype(type : ZSignFieldSubType,resizeView : ZResizeView)
}

public class ZResizeView: ResizableView {
    
    var touchLocation = CGPoint.zero
    var initialTouchLocation = CGPoint.zero
    
    var beginningPoint = CGPoint.zero
    var beginningCenter = CGPoint.zero
    
    var beginFrame = CGRect.zero
//    let toolTip =

    let contentBgView = UIView()
    let resizeViewRight = UILabel()
    let resizeViewLeft  = UILabel()
    

    let dashedBorder = CAShapeLayer()
    
    let valueTextLabel      =  UILabel()
    let fieldNameTextField  =  UITextField()
    let signatureImageView  =  UIImageView()
    let dropdownIconView    =  UIImageView()
    let iconView            =  UIImageView()
    
    let slideInTransitioningDelegate = PresentationManager()
    
//    public var resizeViewMode : ZSResizeViewMode = .sentDoc_selfSign
    
    var isEditingMode = false
    public var isSampleSigning = false

    weak public var delegate : ZResizeViewDelegate?
    
    public var minWidth : CGFloat = 0
    public var minHeight : CGFloat = 0
    
    private var lastFontSize  = 0
    var initialSizeRatio : CGFloat = 1

    var textFieldRatio : CGFloat = 1

    var pdfPageSize  = CGSize.zero
    
    //MARK:- init

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupDefaultAttributes()
    }
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    deinit {
        ZInfo("deinit")
    }
    
    //MARK:- setup
    func setupDefaultAttributes() {
        let menuController = UIMenuController.shared
        if #available(iOS 13.0, *) {
            menuController.hideMenu()
        } else {
            // Fallback on earlier versions
            menuController.setMenuVisible(true, animated: false)
            menuController.setMenuVisible(false, animated: false)
        }
       
        //singleTap, move gesture
        let moveGesture = UIPanGestureRecognizer(target: self, action: #selector(moveGesture(_:)))
        moveGesture.delegate    =   self;
        addGestureRecognizer(moveGesture)

        let singleTap = UITapGestureRecognizer(target: self, action: #selector(contentTapped(_:)))
        singleTap.delegate    =   self;
        addGestureRecognizer(singleTap)
        
        if #available(iOS 13.0, *) {
            if (SwiftUtils.isMac) {
                addInteraction(UIContextMenuInteraction(delegate: self))
            }
        }

        
        contentBgView.backgroundColor = UIColor.clear
        contentBgView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(contentBgView)
        contentBgView.frame = bounds.insetBy(dx: outerMargin, dy: outerMargin)
        

     
        
        valueTextLabel.backgroundColor = UIColor.clear
        valueTextLabel.textColor = UIColor.black
        valueTextLabel.text = "";
        valueTextLabel.isUserInteractionEnabled   =   false
        valueTextLabel.clipsToBounds    =   true
        valueTextLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(valueTextLabel)
        valueTextLabel.frame = CGRect(x: outerMargin, y: 500, width: frame.size.width - totalMargin, height: frame.size.height - totalMargin)
        valueTextLabel.isHidden = true
        
       
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: valueTextLabel.frame.height))
        leftView.autoresizingMask = .flexibleHeight
        
        iconView.frame = CGRect(x: 0, y: 0, width: 15, height: valueTextLabel.frame.size.height)
        iconView.isUserInteractionEnabled = false;
        iconView.contentMode  = .scaleAspectFit;
        iconView.autoresizingMask = .flexibleHeight
        iconView.backgroundColor    =   grayFieldBgColor
        iconView.layer.zPosition    =   1001;
        leftView.addSubview(iconView)
        
//        iconView.snp.makeConstraints { (make) in
//            make.left.top.equalTo(0)
//            make.width.equalTo(15)
//            make.height.equalToSuperview()
//        }
        
        fieldNameTextField.frame = CGRect(x: outerMargin, y: outerMargin, width: bounds.width - totalMargin, height: bounds.height - totalMargin)
        fieldNameTextField.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        fieldNameTextField.backgroundColor = .clear
        fieldNameTextField.textColor = .black
        fieldNameTextField.text = "";
        fieldNameTextField.isUserInteractionEnabled   =   false
        fieldNameTextField.clipsToBounds    =   false
        fieldNameTextField.leftView = leftView
        fieldNameTextField.leftViewMode = .always
        addSubview(fieldNameTextField)
        
        
        dropdownIconView.frame = CGRect(x: bounds.size.width - resizeableView_TotalMargin - 3, y: resizeableView_OuterMargin, width: 15, height: contentBgView.frame.size.height)
        dropdownIconView.isUserInteractionEnabled = false
        dropdownIconView.image = ZSImage.tintColorImage(withNamed: "editor_field_next", col: ZColor.whiteColor)
        dropdownIconView.contentMode = .scaleAspectFit
        dropdownIconView.autoresizingMask = [.flexibleLeftMargin, .flexibleHeight]
        dropdownIconView.backgroundColor = redFieldBgColor
        dropdownIconView.layer.zPosition = 1002
        addSubview(dropdownIconView)
        
        
        let strokeTextAttributes = [NSAttributedString.Key.strokeColor : UIColor.white,
                                    NSAttributedString.Key.strokeWidth : -2] as [NSAttributedString.Key : Any]
        addSubview(resizeViewLeft)
        resizeViewLeft.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview().offset(-3)
            make.centerX.equalTo(12)
            make.width.height.equalTo(28)
        }
        resizeViewLeft.textAlignment   =   .center
        resizeViewLeft.isUserInteractionEnabled = true
        resizeViewLeft.textColor = UIColor.red
        resizeViewLeft.layer.zPosition = 1003;
        resizeViewLeft.attributedText =  NSAttributedString(string: "•", attributes: strokeTextAttributes)
        resizeViewLeft.font = UIFont.systemFont(ofSize: 40, weight: .bold);

        addSubview(resizeViewRight)
        resizeViewRight.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview().offset(-3)
            make.trailing.equalToSuperview().offset(2)
            make.width.height.equalTo(28)
        }
        resizeViewRight.textAlignment   =   .center
        resizeViewRight.isUserInteractionEnabled = true
        resizeViewRight.textColor = UIColor.red
        resizeViewRight.layer.zPosition = 1003;
        resizeViewRight.attributedText =  NSAttributedString(string: "•", attributes: strokeTextAttributes)
        resizeViewRight.font = UIFont.systemFont(ofSize: 40, weight: .bold);

        
        let leftResizeGesture =  UIPanGestureRecognizer(target: self, action: #selector(resizeViewPanGesture(_:)))
        leftResizeGesture.delegate    =   self;
        resizeViewLeft.addGestureRecognizer(leftResizeGesture)
        
        let rightResizeGesture =  UIPanGestureRecognizer(target: self, action: #selector(resizeViewPanGesture(_:)))
        rightResizeGesture.delegate    =   self;
        resizeViewRight.addGestureRecognizer(rightResizeGesture)
        
        contentBgView.addSubview(signatureImageView)
        signatureImageView.isUserInteractionEnabled = false
        signatureImageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        dashedBorder.strokeColor = grayFieldBgColor.cgColor;
        dashedBorder.lineWidth =  0.6;
        dashedBorder.lineDashPattern = [5, 5]
        dashedBorder.fillColor = nil
        dashedBorder.zPosition = 1000;
        contentBgView.layer.addSublayer(dashedBorder)
        dashedBorder.path = UIBezierPath(rect: contentBgView.bounds).cgPath
        dashedBorder.frame = contentBgView.bounds;
        
        initialSizeRatio = frame.width / frame.height
        
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        dashedBorder.path = UIBezierPath(rect: contentBgView.bounds).cgPath
        dashedBorder.frame = contentBgView.bounds;
    }
    
    func setSignField(field : SignField)  {
        signField = field

        valueTextLabel.textColor    = UIColor.black
        valueTextLabel.attributedText = signField.getTextWithFieldParams(for: signField.fieldName)
        dropdownIconView.isHidden = true
        valueTextLabel.numberOfLines = 1;
        fieldNameTextField.attributedText = valueTextLabel.attributedText;
        
        if (signField.isCompleted) {
            fieldNameTextField.leftViewMode = .never;
        }else{
            fieldNameTextField.leftViewMode = .always
        }
        
        switch (signField.type) {
        case .signature:
            iconView.image = ZSImage.tintColorImage(withNamed: "doc_sign", col: ZColor.whiteColor)
            if let img = signField?.signImage , signField?.isCompleted ?? false{
                setFieldSignature(img)
            }
            break;
        case .initial:
            iconView.image = ZSImage.tintColorImage(withNamed: "doc_initial", col: ZColor.whiteColor)
            if let img = signField?.signImage , signField?.isCompleted ?? false{
                setFieldSignature(img)
            }
            break;
        case .email:
            iconView.image = ZSImage.tintColorImage(withNamed: "doc_email", col: ZColor.whiteColor)
            if let textVal = signField?.textValue , signField?.isCompleted ?? false{
                setFieldValue(textVal)
            }
            break;
        case .name:
            iconView.image = ZSImage.tintColorImage(withNamed: "doc_fullname", col: ZColor.whiteColor)
            if let textVal = signField?.textValue , signField?.isCompleted ?? false{
                setFieldValue(textVal)
            }
            break;
        case .company:
            iconView.image = ZSImage.tintColorImage(withNamed: "doc_company", col: ZColor.whiteColor)
            if let textVal = signField?.textValue , signField?.isCompleted ?? false{
                setFieldValue(textVal)
            }
            break;
        case .jobTitle:
            iconView.image = ZSImage.tintColorImage(withNamed: "profile_job_title", col: ZColor.whiteColor)
            if let textVal = signField?.textValue , signField?.isCompleted ?? false{
                setFieldValue(textVal)
            }
            break;
        case .date,.signDate:
            iconView.image = ZSImage.tintColorImage(withNamed: "doc_calender", col: ZColor.whiteColor)
            if let textVal = signField?.dateValue , signField?.isCompleted ?? false{
                setFieldValue(textVal)
            }
            break;
        case .checkBox:
            iconView.image = ZSImage.tintColorImage(withNamed: "doc_calender", col: ZColor.whiteColor)
            setFieldCheckboxValue(signField.isCheckboxTicked)
            self.isUserInteractionEnabled = !signField.isReadOnly;
            
            break;
        case .textBox:
            iconView.image = ZSImage.tintColorImage(withNamed: "doc_text", col: ZColor.whiteColor)
            
            if let textVal = signField?.textValue , signField?.isCompleted ?? false{
                setFieldValue(textVal)
            }
            break;
        case .dropDown:
            iconView.image = ZSImage.tintColorImage(withNamed: "nav_dropdown_icon", col: ZColor.whiteColor)
            
            var fieldname = signField.fieldName;
            if let textVal = signField?.textValue , signField?.isCompleted ?? false{
                fieldname = textVal
            }
            else if let textVal = signField?.defaultValue{
                fieldname = textVal
            }
            
            self.isUserInteractionEnabled = !signField.isReadOnly;
            
            //                [self setDropdownFieldValue:fieldname];
            //                self.userInteractionEnabled = !_signField.isReadOnly;
            
            break
        case .attachment:
            updateAttachmentFieldValue()
            isUserInteractionEnabled = true
            break
            
        default:
            iconView.image = ZSImage.tintColorImage(withNamed: "doc_text", col: ZColor.whiteColor)
            
            if let textVal = signField?.textValue , signField?.isCompleted ?? false{
                setFieldValue(textVal)
            }
            break;
        }
        
        adjustTextFieldWidth()
    }
             
    override public var resizeViewMode : ZResizeViewMode {
        didSet{

            var _resizeViewMode = resizeViewMode
            if signField.isReadOnly && (resizeViewMode != .sentDoc_Create) {
                _resizeViewMode = .receivedDoc_completed
            }

            //resizeViewMode = resizeViewMode    // Skipping redundant initializing to itself
            resizeViewRight.isHidden = true
            resizeViewLeft.isHidden = true

            switch _resizeViewMode {
                case .sentDoc_selfSign:
                    resizeViewRight.isHidden = !isEditingMode
                    resizeViewLeft.isHidden = !isEditingMode

                    isUserInteractionEnabled = true

                    dashedBorder.strokeColor = greenFieldBorderColor.cgColor
                    iconView.backgroundColor = greenFieldBgColor
                    contentBgView.backgroundColor = greenFieldBgColor
                    resizeViewLeft.textColor = greenFieldBorderColor
                    resizeViewRight.textColor = greenFieldBorderColor
                case .receivedDoc_preview:
                    dashedBorder.strokeColor = grayFieldBorderColor.cgColor
                    iconView.backgroundColor = grayFieldBorderColor
                    dropdownIconView.backgroundColor = grayFieldBgColor
                    contentBgView.backgroundColor = grayFieldBgColor
                    isUserInteractionEnabled = false
                case .sentDoc_Create, .receivedDoc_pending:
                    dashedBorder.strokeColor = redFieldBorderColor.cgColor
                    iconView.backgroundColor = redFieldDarkBgColor
                    dropdownIconView.backgroundColor = redFieldDarkBgColor
                    contentBgView.backgroundColor = redFieldBgColor
                    isUserInteractionEnabled = true

                    fieldNameTextField.leftViewMode = .always

                    clipsToBounds = resizeViewMode == .sentDoc_Create
                    resizeViewLeft.textColor = redFieldBorderColor
                    resizeViewRight.textColor = redFieldBorderColor
                case .receivedDoc_completed:
                    dashedBorder.strokeColor = UIColor.clear.cgColor
                    iconView.backgroundColor = UIColor.clear
                    contentBgView.backgroundColor = UIColor.clear
                    dropdownIconView.backgroundColor = UIColor.clear
                    self.isUserInteractionEnabled = (((signField.type == .checkBox) || (signField.type == .dropDown) || (signField.type == .attachment)) && !signField.isReadOnly);
                    dropdownIconView.isHidden = true
                default:
                    break
            }
            
            
            if (signField.type == .checkBox) {
                contentBgView.backgroundColor =  UIColor.clear
                self.clipsToBounds = false;
            }
            else if (signField.type == .attachment) {
                updateAttachmentFieldValue()
            }
            else{
                self.clipsToBounds = (_resizeViewMode == .sentDoc_selfSign);
            }
        }
    }
    
   
    @objc func resizeViewPanGesture(_ recognizer : UIPanGestureRecognizer) {
        let isLeftSwipeView = recognizer.view == resizeViewLeft
        
        touchLocation = recognizer.location(in: self.superview)
        
        switch recognizer.state {
        case .began:
            initialTouchLocation = touchLocation
            beginFrame = frame
            break
        case .changed:
            if (resizeViewMode == .sentDoc_Create) {
                resizeHandlingForCreateRequestFields(isLeftSwipeView)
            }
            else{
                resizeHandlingForOtherFields(isLeftSwipeView)
            }
            
            delegate?.resizeViewDidChange(self)
            break
        case .ended:
             signField.frame =  CGRect(x: self.frame.origin.x + outerMargin, y: self.frame.origin.y + outerMargin, width: self.frame.size.width - totalMargin, height: self.frame.size.height - totalMargin)
             delegate?.resizeViewDidEndEditing(self)
            break
        default:
            break
        }
        
    }

    
    func showEditingHandles()  {
        if isDisableResizing {return;}
        
        isEditingMode = true
        
        resizeViewLeft.isHidden = false
        resizeViewRight.isHidden = false
        
        if (signField.isOtherRecipientField) {
            self.isUserInteractionEnabled = false
        }else{
            self.isUserInteractionEnabled = ((resizeViewMode == .sentDoc_selfSign) || (resizeViewMode == .receivedDoc_pending) || (resizeViewMode == .sentDoc_Create))
        }
    }

    override public func hideEditingHandles()  {
        isEditingMode = false
        
        resizeViewLeft.isHidden = true
        resizeViewRight.isHidden = true
        if (signField.isOtherRecipientField) {
            self.isUserInteractionEnabled = false
        }else{
            self.isUserInteractionEnabled = ((resizeViewMode == .sentDoc_selfSign) || (resizeViewMode == .sentDoc_Create))
        }
        resignFirstResponder()
        hideMenuController()
        
    }
    
    //MARK: - Menu controller
    
    func showMenuController() {

        if SwiftUtils.isMac {
            return
        }

        if (resizeViewMode == .receivedDoc_preview) || (resizeViewMode == .receivedDoc_pending) || (resizeViewMode == .receivedDoc_completed) {
            return
        }
        if signField.isOtherRecipientField {
            return
        }
        hideMenuController()
        ZSPopTip.dismissPoptip()

        weak var _self = self
//        let colorMenu = UIMenuItem(title: "sign.mobile.fields.changeColor".ZSLString, image: ZSImage.tintColorImage(withNamed: "field_color_icon", col: ZColor.whiteColor)) { (item) in
//            _self?.showColorsMenu()
//        }
        let fontAttrributesMenu = UIMenuItem(title: "sign.mobile.fields.formatting".ZSLString, image: ZSImage.tintColorImage(withNamed: "field_font_icon", col: ZColor.whiteColor)) { (item) in
            _self?.showFontPropertiesView()
        }
        
        let dateAttrributesMenu = UIMenuItem(title: "sign.mobile.profile.dateFormat".ZSLString, image: ZSImage.tintColorImage(withNamed: "field_calander_icon", col: ZColor.whiteColor)) { (item) in
            _self?.showDatePropertiesView()
        }
        
        
        let editMenu =  UIMenuItem(title: "sign.mobile.common.edit".ZSLString, image: nil) { (item) in
            _self?.delegate?.resizeViewDidEditButtonTapped(self)
        }
        
        
        let firstNameMenu = UIMenuItem(title: "sign.mobile.field.firstname".ZSLString) { (menu) in
            if let _self = _self{
                _self.delegate?.resizeViewDidChangeSubtype(type: .firstName, resizeView: _self)
            }
        }
        
        
        let lastNameNameMenu = UIMenuItem(title: "sign.mobile.field.lastname".ZSLString) { (menu) in
            if let _self = _self{
                _self.delegate?.resizeViewDidChangeSubtype(type: .lastName, resizeView: _self)
            }
        }
        
        
        let fullNameMenu = UIMenuItem(title: "sign.mobile.field.fullname".ZSLString) { (menu) in
            if let _self = _self{
                _self.delegate?.resizeViewDidChangeSubtype(type: .fullName, resizeView: _self)
            }
        }
        
        let optionalMenu = UIMenuItem(title: "sign.mobile.common.optional".ZSLString) { (menu) in
            _self?.signField.isMandatory = false
        }
        
        let mandatoryMenu = UIMenuItem(title: "sign.mobile.common.mandatory".ZSLString) { (menu) in
            _self?.signField.isMandatory = false
        }
        
        
        let checkMenu = UIMenuItem(title: "sign.mobile.fields.checkbox.checked".ZSLString, image: ZSImage.tintColorImage(withNamed: "field_check_icon", col: ZColor.whiteColor)) { (item) in
            _self?.setFieldCheckboxValue(true)
        }
        //        UIMenuItem.mik_init(withTitle: ZSLString("sign.mobile.fields.checkbox.checked"), image: ZSImage.tintColorImage(withNamed: "field_check_icon", col: ZColor.white()), action: { item in
        //                self.fieldCheckboxValue = true
        //            })
        
        let unCheckMenu = UIMenuItem(title: "sign.mobile.fields.checkbox.unchecked".ZSLString, image: ZSImage.tintColorImage(withNamed: "field_uncheck_icon", col: ZColor.whiteColor)) { (item) in
            _self?.setFieldCheckboxValue(false)
        }
        
        
        let readOnlyMenu = UIMenuItem(title: "sign.mobile.common.readonly".ZSLString) { menu in
            _self?.signField.isReadOnly = true
            if _self?.signField.type == .checkBox {
                _self?.setFieldCheckboxValue(true)
            }
        }
        
        let readWriteMenu = UIMenuItem(title: "sign.mobile.common.readAndWrite".ZSLString) { (menu) in
            _self?.signField.isReadOnly = false
        }
        
        
        
        let deleteMenu = UIMenuItem(title: "sign.mobile.common.delete".ZSLString, image: ZSImage.tintColorImage(withNamed: "field_delete_icon", col: ZColor.whiteColor))  { (item) in
            _self?.deleteButtonClicked()
        }
        
        hideMenuController()
        var menuArray : [UIMenuItem] = [];
        switch (signField.type) {
        case .signature,.initial,.attachment:
            if (resizeViewMode == .sentDoc_Create) {
                menuArray.append(signField.isMandatory ? optionalMenu : mandatoryMenu)
            }
            
            menuArray.append(deleteMenu)
            break;
        case .email,.jobTitle,.company:
//            menuArray.append(contentsOf: [fontAttrributesMenu,colorMenu,deleteMenu])
            menuArray.append(contentsOf: [fontAttrributesMenu,deleteMenu])
            break;
        case .name:
            
            var isLastNameAvailable =  true
            if (isSampleSigning) {
                isLastNameAvailable = true;
            }
            else if ((resizeViewMode == .sentDoc_selfSign) && (UserManager.shared.currentUser?.lastName?.isEmpty ?? true ||  UserManager.shared.currentUser?.lastName == " ")) {
                isLastNameAvailable = false;
            }
            
            
            if (signField.subType == .firstName) {
//                menuArray.append(contentsOf: [fullNameMenu,fontAttrributesMenu,colorMenu,deleteMenu])
                menuArray.append(contentsOf: [fullNameMenu,fontAttrributesMenu,deleteMenu])
                
                if (isLastNameAvailable) {
                    menuArray.insert(lastNameNameMenu, at: 1)
                }
                
            }else if (signField.subType == .lastName) {
//                menuArray.append(contentsOf: [fullNameMenu,firstNameMenu,fontAttrributesMenu,colorMenu,deleteMenu])
                menuArray.append(contentsOf: [fullNameMenu,firstNameMenu,fontAttrributesMenu,deleteMenu])

            }else{
//                menuArray.append(contentsOf: [firstNameMenu,fontAttrributesMenu,colorMenu,deleteMenu])
                menuArray.append(contentsOf: [firstNameMenu,fontAttrributesMenu,deleteMenu])

                if (isLastNameAvailable) {
                    menuArray.insert(lastNameNameMenu, at: 1)
                }
            }
            break;
        case .signDate:
//            menuArray.append(contentsOf: [fontAttrributesMenu,dateAttrributesMenu,colorMenu,deleteMenu])
            menuArray.append(contentsOf: [fontAttrributesMenu,dateAttrributesMenu,deleteMenu])

            break;
        case .date:
//            menuArray.append(contentsOf: [fontAttrributesMenu,dateAttrributesMenu,colorMenu,signField.isMandatory ? optionalMenu : mandatoryMenu,deleteMenu])
            menuArray.append(contentsOf: [fontAttrributesMenu,dateAttrributesMenu,signField.isMandatory ? optionalMenu : mandatoryMenu,deleteMenu])
            
            break;
        case .checkBox:
            if (resizeViewMode == .sentDoc_Create) {
                menuArray.append(contentsOf: [(signField.isCheckboxTicked ? unCheckMenu : checkMenu),((signField.isMandatory) ? optionalMenu : mandatoryMenu),((signField.isReadOnly) ? readWriteMenu : readOnlyMenu)])

            }
            menuArray.append(deleteMenu)
            break;
        case .textBox:
            
            if (resizeViewMode == .sentDoc_Create) {
//                menuArray.append(contentsOf: [fontAttrributesMenu,colorMenu,((signField.isMandatory) ? optionalMenu : mandatoryMenu),((signField.isReadOnly) ? readWriteMenu : readOnlyMenu),deleteMenu])
                menuArray.append(contentsOf: [fontAttrributesMenu,((signField.isMandatory) ? optionalMenu : mandatoryMenu),((signField.isReadOnly) ? readWriteMenu : readOnlyMenu),deleteMenu])

            }else{
//                menuArray.append(contentsOf:[fontAttrributesMenu,colorMenu,deleteMenu])
                menuArray.append(contentsOf:[fontAttrributesMenu,deleteMenu])
            }
            
            
            break;
        case .dropDown:
//            menuArray.append(contentsOf: [editMenu,fontAttrributesMenu,colorMenu,((signField.isMandatory) ? optionalMenu : mandatoryMenu),((signField.isReadOnly) ? readWriteMenu : readOnlyMenu),deleteMenu])
            menuArray.append(contentsOf: [editMenu,fontAttrributesMenu,((signField.isMandatory) ? optionalMenu : mandatoryMenu),((signField.isReadOnly) ? readWriteMenu : readOnlyMenu),deleteMenu])

            break;
        default:
//            menuArray.append(contentsOf: [fontAttrributesMenu,colorMenu,deleteMenu])
            menuArray.append(contentsOf: [fontAttrributesMenu,deleteMenu])
            break;
        }
        if (UIFont(name: "Roboto", size: 14) == nil) {
            menuArray.removeAll { (item) -> Bool in
                return item == fontAttrributesMenu
            }
        }
        
        UIMenuController.shared.menuItems = menuArray
        UIMenuController.shared.update()
//          [menu setMenuItems:menuArray];
//          [menu update];
        
        
//        UIMenuController.shared.menuItems = [editMenu,deleteMenu]
        if #available(iOS 13.0, *) {
            UIMenuController.shared.showMenu(from: self, rect: bounds.insetBy(dx: outerMargin, dy: outerMargin))
        } else {
            // Fallback on earlier versions
            UIMenuController.shared.setTargetRect(bounds.insetBy(dx: outerMargin, dy: outerMargin), in: self)
            UIMenuController.shared.setMenuVisible(true, animated: true)
        }
    }
    
    func hideMenuController()  {
        if #available(iOS 13.0, *) {
            UIMenuController.shared.hideMenu()
        } else {
            // Fallback on earlier versions
            UIMenuController.shared.setMenuVisible(false, animated: true)
        }
    }
    
    
    @objc public func showFontPropertiesView()  {
        let textPropertiesController = TextPropertiesViewController(selectedTextProperty: signField.currentTextProperty)
        textPropertiesController.delegate = self
        if DeviceType.isIphone {
            textPropertiesController.modalPresentationStyle = .custom
            textPropertiesController.transitioningDelegate = slideInTransitioningDelegate
        }else{
            textPropertiesController.modalPresentationStyle = .popover
            textPropertiesController.popoverPresentationController?.sourceView = self
            textPropertiesController.popoverPresentationController?.sourceRect = self.bounds
            
        }
        
       
        parentViewController?.present(textPropertiesController, animated: true, completion: nil)
        
    }
    
    public func showDatePropertiesView()  {
        var formats : [String] = []
        let dateformatter = DateFormatter()
        let dateformats = (signField.type == .date) ? DateManager.customDateFormats() : DateManager.availableDateFormats()
        dateformats.forEach { (format) in
            dateformatter.dateFormat = format
            formats.append(dateformatter.string(from: Date()))
        }
        
        let popupController = PopupViewController(title: "sign.mobile.profile.dateFormat".ZSLString, menuItems: PopupDataModel.getDataModel(forStrings: formats), selectedItem: PopupDataModel(title: ""))
        popupController.completionHandler = { [weak self](index) in
            self?.signField.dateFormat = dateformats[index]
            dateformatter.dateFormat  = dateformats[index]
            self?.signField.dateValue = dateformatter.string(from: Date())
            self?.signField.textValue = self?.signField.dateValue
            self?.updateFieldName()
        }
        

        
        
        if DeviceType.isIphone {
            popupController.modalPresentationStyle = .custom
            popupController.transitioningDelegate = slideInTransitioningDelegate
            parentViewController?.present(popupController, animated: true, completion: nil)
        } else {
            let nav = UINavigationController(rootViewController: popupController)
            nav.modalPresentationStyle = .popover
            nav.popoverPresentationController?.sourceRect = self.frame
            nav.popoverPresentationController?.sourceView = self
            nav.popoverPresentationController?.permittedArrowDirections = [.down, .up]
            parentViewController?.present(nav, animated: true, completion: nil)
        }
    }

    @objc public  func deleteButtonClicked()  {
        hideMenuController()
        delegate?.resizeViewDeleteButtonTapped(self)
    }
    
    override public func setIsHighlighted(_ isHighlighted: Bool) {
        if !isHighlighted {
            ZSPopTip.dismissPoptip()
            return
        }
        guard let imgView = (superview) as? UIImageView else {
            return
        }

        var highLightMsg  = signField.fieldName ?? ""
        

        if resizeViewMode == .receivedDoc_pending {
            if signField.isCompleted || signField.isReadOnly {
                //skip
            } else if signField.type == .checkBox {
                highLightMsg = String(format:"sign.mobile.fields.selectTheCheckbox".ZSLString, signField.fieldName?.lowercased() ?? "")
            } else if signField.type == .dropDown {
                highLightMsg = String(format: "sign.mobile.fields.chooseAnOption".ZSLString, signField.fieldName?.lowercased() ?? "")
            } else if signField.type == .attachment {
                highLightMsg = "sign.mobile.fields.uploadAttachment".ZSLString
            } else if (signField.type == .textBox) || (signField.type == .date) || (signField.type == .signDate) {
                highLightMsg = String(format:"sign.mobile.fields.enterValue".ZSLString, signField.fieldName?.lowercased() ?? "")
            } else {
                highLightMsg = String(format:"sign.mobile.fields.enterYourValue".ZSLString, signField.fieldName?.lowercased() ?? "")
            }
        }

        if signField.isReadOnly {
            highLightMsg = String(format: "%@ (%@)",highLightMsg,"sign.mobile.common.readonly".ZSLString )
        }
        if !signField.isMandatory {
            highLightMsg = String(format: "%@ (%@)",highLightMsg,"sign.mobile.common.optional".ZSLString )
        }
        var directon = 1
        if frame.origin.y < 20 {
            directon = 2
        }


        if resizeViewMode == .sentDoc_Create || resizeViewMode == .sentDoc_selfSign {
            ZSPopTip.showInfoTip(withMessage: highLightMsg, inView: imgView, onRect: frame, direction: directon)
        } else {
            ZSPopTip.showMessage(highLightMsg, inView: imgView, onRect: frame, direction: directon)
        }
    }


    // MARK: - Gestures
    

    
    func resizeHandlingForOtherFields(_ isLeftSwipeView : Bool)  {
        let diff = isLeftSwipeView ? (initialTouchLocation.x - touchLocation.x):  (touchLocation.x - initialTouchLocation.x);
        
        
        var newbounds = beginFrame;
        if (isLeftSwipeView) {
            newbounds.origin.x = newbounds.origin.x - diff;
        }
        newbounds.size.width  = beginFrame.size.width + diff;
        if (signField.type == .signature) {
            let signatureRatio  =  SignatureManager.signatureImageRatio(false)
            newbounds.size.height  =  (newbounds.size.width - totalMargin) * signatureRatio;
            newbounds.size.height += totalMargin;
            
            if (newbounds.size.width < minWidth) {
                newbounds.size.width = minWidth;
                newbounds.size.width = minHeight;
            }
            if (newbounds.size.height < minHeight) {
                newbounds.size.height = minHeight;
                newbounds.size.width = minWidth;
            }
            
        }
        else if  (signField.type == .initial){
            let signatureRatio  =  SignatureManager.initialImageRatio(false)
            newbounds.size.height  =  (newbounds.size.width - totalMargin) * signatureRatio;
            newbounds.size.height += totalMargin;
            
            if (newbounds.size.width < minWidth) {
                newbounds.size.width = minWidth;
                newbounds.size.width = minHeight;
            }
            if (newbounds.size.height < minHeight) {
                newbounds.size.height = minHeight;
                newbounds.size.width = minWidth;
            }
        }
        else if (signField.type == .checkBox){
            let signatureRatio : CGFloat = 1.0;
            newbounds.size.height  =  (newbounds.size.width - totalMargin) * signatureRatio;
            newbounds.size.height += totalMargin;

            if (newbounds.size.width < minWidth) {
                newbounds.size.width = minWidth;
                newbounds.size.width = minHeight;
            }
            if (newbounds.size.height < minHeight) {
                newbounds.size.height = minHeight;
                newbounds.size.width = minWidth;
            }
            
        }
        else{
            newbounds.size.height  = newbounds.size.width / initialSizeRatio;
            
            newbounds.size.width = (newbounds.size.width < minWidth) ?  minWidth :  newbounds.size.width;
            newbounds.size.height = (newbounds.size.height < minHeight) ?  minHeight :  newbounds.size.height;
            if ((newbounds.size.width <= minWidth) && (newbounds.size.height <= minHeight)) {
                return;
            }
            
            
            if (!isResizeFrameValid(newbounds)) {
                return;
            }
            
            let labelBounds =  newbounds.insetBy(dx: outerMargin , dy: outerMargin)

            
            valueTextLabel.attributedText = signField.adjustedAttributedString(valueTextLabel.attributedText?.string, toFillRect: labelBounds)

            guard let strSize  = valueTextLabel.attributedText?.size() ,let imgView = superview as? UIImageView else {
                return
            }
            
            if (strSize.width > labelBounds.width || strSize.height > labelBounds.height) {
                return;
            }

            
            signField.currentTextProperty.fontSize = Int(valueTextLabel.font?.pointSize ?? 1)
            
            signField.serverFontSize = FontManager.calibaratedServerFontSize(forDeviceFontSize: signField.currentTextProperty.fontSize, pageWidth: imgView.frame.width, imageWidth: pdfPageSize.width)
            
            lastFontSize = Int(signField.currentTextProperty.fontSize)
            
            if ((signField.type != .signature) && (signField.type != .initial)) {
                let labelSize =  valueTextLabel.attributedText?.size()
                newbounds.size.height = ((labelSize?.height ?? 0)  * textFieldRatio) + totalMargin + 2
            }
            
        }
        
        if (!isResizeFrameValid(newbounds)) {
            return;
        }
        self.frame = newbounds;
        contentBgView.frame       =  CGRect(x: outerMargin, y: outerMargin, width: bounds.width - totalMargin, height: bounds.height - totalMargin)
        
        adjustTextFieldWidth()
        
        dashedBorder.path = UIBezierPath(rect: contentBgView.bounds).cgPath
        dashedBorder.frame = contentBgView.bounds;
    }
    
    func resizeHandlingForCreateRequestFields(_ isLeftSwipeView : Bool)  {
        let diffX = isLeftSwipeView ? (initialTouchLocation.x - touchLocation.x):  (touchLocation.x - initialTouchLocation.x);
        let diffY =  touchLocation.y - initialTouchLocation.y;
        
        var newbounds = beginFrame;
        if beginFrame.origin.x < 0 {
            beginFrame.size.width -= beginFrame.origin.x
            beginFrame.origin.x = 0
        }
        if beginFrame.origin.y < 0 {
            beginFrame.size.height -= beginFrame.origin.y
            beginFrame.origin.y = 0
        }
        
        if (isLeftSwipeView) {
            newbounds.origin.x = newbounds.origin.x - diffX;
        }
        newbounds.size.width  = beginFrame.size.width + diffX;
        
        if (signField.type == .checkBox){
            if (newbounds.size.width < minWidth) {
                newbounds.size.width = minWidth;
            }
            newbounds.size.height  = newbounds.size.width;

        }else{
            newbounds.size.height  = beginFrame.size.height + diffY;

            newbounds.size.width = (newbounds.size.width < minWidth) ?  minWidth :  newbounds.size.width;
            newbounds.size.height = (newbounds.size.height < minHeight) ?  minHeight :  newbounds.size.height;
        }
        if (!isResizeFrameValid(newbounds)) {
            return;
        }

        self.frame = newbounds
        
        adjustTextFieldWidth()
      

//        dashedBorder.path = [UIBezierPath bezierPathWithRect:contentBgView.bounds].CGPath;
//        dashedBorder.frame = contentBgView.bounds;

//        checkboxBgBorder.frame = CGRectMake(-5, -5, contentBgView.frame.size.width+10, contentBgView.frame.size.height+10);
//        checkboxBgBorder.path = [UIBezierPath bezierPathWithRect:checkboxBgBorder.bounds].CGPath;
//        checkboxBgBorder.hidden = YES;
    }

    func isResizeFrameValid(_ newBounds : CGRect) -> Bool {
        if (newBounds.minX < 0) {//left small
            return false;
        }
        else if (newBounds.maxX > (self.superview?.frame.width ?? 1)){ //right small
            return false;
        }
        else if (newBounds.maxY > (self.superview?.frame.height ?? 1)){ //right small
            return false;
        }
        return true
    }
    
    func adjustTextFieldWidth()  {
        if (resizeViewMode == .receivedDoc_completed) {
            valueTextLabel.frame = CGRect(x: outerMargin, y: outerMargin, width: 1100, height: frame.size.height - totalMargin)
        }else{
            valueTextLabel.frame = CGRect(x: outerMargin, y: outerMargin, width: self.frame.size.width - totalMargin, height: frame.size.height - totalMargin)
        }
        adjustTextValues()
    }
    
    func adjustTextValues(){
        fieldNameTextField.attributedText = valueTextLabel.attributedText;
        
        if ((signField.type == .dropDown) && (resizeViewMode != .receivedDoc_completed)) {
        }
        else {
            valueTextLabel.sizeToFit()
        }
        fieldNameTextField.frame = contentBgView.frame;
    }

    public func viewRotationHandling() {
        var fieldValue = signField.fieldName
        if (resizeViewMode != .sentDoc_Create) && (resizeViewMode != .receivedDoc_preview) && (signField?.textValue != nil) {
            fieldValue = signField.textValue
        }

        valueTextLabel.attributedText = signField.getTextWithFieldParams(for: valueTextLabel.attributedText?.string)
        adjustTextValues()
    }

    
    public func adjustFrames() {
    
        if (signField.type != .checkBox)
        {
            var newbounds = self.bounds;
            
            guard let trimmedString = valueTextLabel.attributedText?.string.trimmingCharacters(in: .whitespaces), !trimmedString.isEmpty, let textSize = valueTextLabel.attributedText?.size() else {
                return
            }
                        
            

            if (resizeViewMode == .sentDoc_Create)
            {
                if (textSize.width > (newbounds.size.width - totalMargin - iconView.frame.size.width)) {
                    newbounds.size.width  = textSize.width + totalMargin +  10 + iconView.frame.size.width ;
                }
                if  (textSize.height > (newbounds.size.height - totalMargin))
                {
                    newbounds.size.height = textSize.height + totalMargin + 10;
                }
            }
            else if ((resizeViewMode == .sentDoc_selfSign) || resizeViewMode == .receivedDoc_completed)
            {

            }

            self.bounds = newbounds
        }
        
        
        initialSizeRatio  =  self.bounds.width / self.bounds.height;
        
        adjustTextFieldWidth()
        
//        dashedBorder.path = [UIBezierPath bezierPathWithRect:contentBgView.bounds].CGPath;
//        dashedBorder.frame = contentBgView.bounds;
//        checkboxBgBorder.frame = CGRectMake(-5, -5, contentBgView.frame.size.width+10, contentBgView.frame.size.height+10);
//        checkboxBgBorder.path = [UIBezierPath bezierPathWithRect:checkboxBgBorder.bounds].CGPath;
        
        
//        if (signField.type == .checkBox) {
//            signatureImageView.frame = contentBgView.bounds;
//        }
        
        delegate?.resizeViewDidEndEditing(self)
    }
    
    
   func estimatedCenter() -> CGPoint {
        var newCenter: CGPoint
        var newCenterX: CGFloat = beginningCenter.x + (touchLocation.x - beginningPoint.x)
        var newCenterY: CGFloat = beginningCenter.y + (touchLocation.y - beginningPoint.y)
        
        let superviewBounds = superview?.bounds ?? bounds
        if newCenterX - 0.5 * frame.width <= 0 {
            newCenterX = frame.width / 2 // self.center.x;
        } else if (newCenterX + 0.5 * frame.width) >= superviewBounds.width {
            newCenterX = superviewBounds.width - frame.width / 2
        }
        if newCenterY - 0.5 * frame.height <= 0 {
            newCenterY = frame.height / 2
        } else if newCenterY + 0.5 * frame.height >= superviewBounds.height {
            newCenterY = superviewBounds.height - frame.height / 2
        }

        newCenter = CGPoint(x: newCenterX, y: newCenterY)
        return newCenter
    }

    
    //MARK: - Set fields
    func updateFieldName()  {
        var fieldValue = signField.fieldName;
        if  let val = signField?.textValue ,(resizeViewMode != .sentDoc_Create) {
            fieldValue = val
        }
        
        valueTextLabel.numberOfLines = fieldValue?.components(separatedBy: CharacterSet.newlines).count ?? 1

        valueTextLabel.attributedText = signField.getTextWithFieldParams(for: fieldValue)
        adjustTextValues()
        updateFieldValueWithFontOption()
    }
    
    func updateFieldValueWithFontOption()  {
        valueTextLabel.attributedText = signField.getTextWithFieldParams(for: valueTextLabel.attributedText?.string)


        if resizeViewMode == .sentDoc_selfSign {

            initialSizeRatio = bounds.size.width / bounds.size.height

            var newbounds = frame
            let labelSize = valueTextLabel.attributedText?.size()

            newbounds.size.height = ((labelSize?.height ?? 0.0) * textFieldRatio) + resizeableView_TotalMargin + 2

            newbounds.size.width = newbounds.size.height * initialSizeRatio
            let labelWidth = (labelSize?.width ?? 0.0) + resizeableView_TotalMargin
            if newbounds.size.width < labelWidth {
                newbounds.size.width = labelWidth
            }
            frame = newbounds
        }

        initialSizeRatio = bounds.size.width / bounds.size.height

        contentBgView.frame = CGRect(x: resizeableView_OuterMargin, y: resizeableView_OuterMargin, width: bounds.size.width - resizeableView_TotalMargin, height: bounds.size.height - resizeableView_TotalMargin)
        
        signField.serverFontSize = FontManager.calibaratedServerFontSize(forDeviceFontSize: signField.currentTextProperty.fontSize, pageWidth: superview?.frame.width ?? 1, imageWidth: pdfPageSize.width)

        dashedBorder.path = UIBezierPath(rect: contentBgView.bounds).cgPath
        dashedBorder.frame = contentBgView.bounds
        adjustTextFieldWidth()

    }
    
    func setFieldCheckboxValue(_ value: Bool) {
        fieldNameTextField.isHidden = true
        fieldNameTextField.isHidden = true
        signatureImageView.isHidden = false
        valueTextLabel.isHidden = true
        resizeViewRight.isHidden = true
        resizeViewLeft.isHidden = true
        signField.isCheckboxTicked = value
        signatureImageView.image = (signField.isCheckboxTicked) ? UIImage(named: "checkbox-in") : UIImage(named: "checkbox-out")
    }

    public func setFieldSignature(_ signature: UIImage?) {
        fieldNameTextField.isHidden = true
        fieldNameTextField.isHidden = true
        signField.signImage = signature
        signatureImageView.image = signature
        signatureImageView.isHidden = false
        valueTextLabel.isHidden = true
        if resizeViewMode != .sentDoc_selfSign {
            let signatureRatio = (signature?.size.width ?? 0.0) / (signature?.size.height ?? 0.0)

            signatureImageView.frame = CGRect(x: signatureImageView.frame.origin.x, y: signatureImageView.frame.origin.y, width: signatureRatio * contentBgView.bounds.size.height, height: contentBgView.bounds.size.height)
        }
    }

    
    func setFieldValue(_ textval : String?)  {
        fieldNameTextField.isHidden   = true

        iconView.isHidden             =   true;
        signatureImageView.isHidden   =   true;
        valueTextLabel.isHidden         =   false;
        valueTextLabel.attributedText   =   signField.getTextWithFieldParams(for: textval)
        
        valueTextLabel.numberOfLines = textval?.components(separatedBy: CharacterSet.newlines).count ?? 1
        adjustTextValues()
    }

    
    public func updateAttachmentFieldValue() {

        iconView.image = UIImage(named: "menu_attachment")
        if signField.isCompleted {
            fieldNameTextField.isHidden = true
            signatureImageView.isHidden = false
            signatureImageView.image = UIImage(named: "menu_attachment")
            signatureImageView.contentMode = .scaleAspectFit
            contentBgView.backgroundColor = ZColor.greenColor.withAlphaComponent(0.7)
            dashedBorder.strokeColor = UIColor.clear.cgColor
        } else {
            fieldNameTextField.isHidden = false
            signatureImageView.isHidden = true
            contentBgView.backgroundColor = (resizeViewMode == .receivedDoc_preview) ? grayFieldBgColor : redFieldBgColor
            dashedBorder.strokeColor = (resizeViewMode == .receivedDoc_preview) ? grayFieldBorderColor.cgColor : redFieldBorderColor.cgColor
        }
    }

    func setDropdownFieldValue(_ value: String?) {
        fieldNameTextField.isHidden = true
        signatureImageView.isHidden = true
        valueTextLabel.isHidden = false
        dropdownIconView.isHidden = resizeViewMode == .receivedDoc_completed // _signField.isCompleted;
        valueTextLabel.numberOfLines = 1

        valueTextLabel.attributedText = signField.getTextWithFieldParams(for: value)
        adjustTextValues()
    }

       
}

extension ZResizeView : UIGestureRecognizerDelegate{
    public override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if (gestureRecognizer is UITapGestureRecognizer) {
            
            if (signField.type == .checkBox || signField.type == .dropDown || signField.type == .attachment) && !signField.isReadOnly && (resizeViewMode != .sentDoc_selfSign) && (resizeViewMode != .sentDoc_Create) {
                return true
            } else if resizeViewMode == .receivedDoc_pending {
                return true
            }
            return false
        } else {
            return (resizeViewMode == .sentDoc_selfSign) || resizeViewMode == .sentDoc_Create
        }
        return true
    }
    
    @objc func contentTapped(_ tapGesture: UITapGestureRecognizer) {
        if resizeViewMode == .receivedDoc_preview {
            return
        }
        if (delegate is UIView) {
            let superview = delegate as? UIView
            if let parentViewController = superview?.parentViewController {
                NSObject.cancelPreviousPerformRequests(withTarget: parentViewController)
            }
        } else if (delegate is UIViewController) {
            NSObject.cancelPreviousPerformRequests(withTarget: delegate)
        }

        ZSPopTip.dismissPoptip()
        delegate?.resizeViewDidTapped(self)
        hideMenuController()
    }
    
    @objc func moveGesture(_ panGesture: UIPanGestureRecognizer){
        touchLocation = panGesture.location(in: superview)
        if panGesture.state == .began {
            beginningPoint = touchLocation
            beginningCenter = center
            hideMenuController()
            showEditingHandles()
            self.center = estimatedCenter()

            delegate?.resizeViewDidBeginEditing(self)
        } else if panGesture.state == .changed {
            self.center = estimatedCenter()
            dashedBorder.path = UIBezierPath(rect: contentBgView.bounds).cgPath
            dashedBorder.frame = contentBgView.bounds
            
            delegate?.resizeViewDidChange(self)
        } else if panGesture.state == .ended {
            self.center = estimatedCenter()

            dashedBorder.path = UIBezierPath(rect: contentBgView.bounds).cgPath
            dashedBorder.frame = contentBgView.bounds

            signField.frame = CGRect(x: frame.origin.x + resizeableView_OuterMargin, y: frame.origin.y + resizeableView_OuterMargin, width: frame.size.width - resizeableView_TotalMargin, height: frame.size.height - resizeableView_TotalMargin)

            delegate?.resizeViewDidEndEditing(self)
        }

    }
    

    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if resizeViewMode == .sentDoc_Create || resizeViewMode == .sentDoc_selfSign {
            delegate?.resizeViewDidBeginEditing(self)
        }
    }

    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        showMenuController()
    }
}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
}

extension ZResizeView: TextPropertiesViewControllerDelegate {
    public func textPropertyDidChanged(_ textPropertry: TextProperty) {
        signField.currentTextProperty = textPropertry
        updateFieldValueWithFontOption()
        delegate?.resizeViewDidChangeTextProperties(self)
    }
}
