//
//  CGSize+.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 09/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import Foundation

public extension CGSize {
    //for mac
    static let sizeForFormSheet = CGSize(width: 540, height: 564)
    static let sizeForPageSheet = CGSize(width: 500, height: 700)
    static let sizeForTopSheet = CGSize(width: 1000, height: 800)
    
    //for all
    init(withSide side: CGFloat) {
        self.init(width: side, height: side)
    }
    
    public static func a4Size(_ width: CGFloat) -> CGSize{
        return CGSize(width: width, height: width * 1.414)
    }
}
