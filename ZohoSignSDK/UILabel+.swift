//
//  UILabel+.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 24/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public extension UILabel {
    
    func autoresize() {
        if let textNSString: NSString = self.text as NSString? {
            let rect = textNSString.boundingRect(with: CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude),
                                                 options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                 attributes: [NSAttributedString.Key.font: self.font],
                                                 context: nil)
            self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.size.width, height: rect.height)
        }
    }
    
    func animate(fontSize: CGFloat, duration: TimeInterval) {
        let startTransform = transform
        let oldFrame = frame
        var newFrame = oldFrame
        let scaleRatio = fontSize / font.pointSize
        
        newFrame.size.width *= scaleRatio
        newFrame.size.height *= scaleRatio
        newFrame.origin.x = oldFrame.origin.x - (newFrame.size.width - oldFrame.size.width) * 0.5
        newFrame.origin.y = oldFrame.origin.y - (newFrame.size.height - oldFrame.size.height) * 0.5
        frame = newFrame
        
        font = font.withSize(fontSize)
        
        transform = CGAffineTransform.init(scaleX: 1 / scaleRatio, y: 1 / scaleRatio);
        layoutIfNeeded()
        
        UIView.animate(withDuration: duration, animations: {
            self.transform = startTransform
            newFrame = self.frame
        }) { (Bool) in
            self.frame = newFrame
        }
    }
    
    func addTextWithImage(image: UIImage, imageBehindText: Bool = false) {
        guard let text = self.text else {
            return;
        }
        let lAttachment = NSTextAttachment()
        lAttachment.image = image
        
        let lFontSize = round(self.font.pointSize * 1.32)
        let lRatio = image.size.width / image.size.height
        
        lAttachment.bounds = CGRect(x: 0, y: ((self.font.capHeight - lFontSize) / 2).rounded(), width: lRatio * lFontSize, height: lFontSize)
        
        let lAttachmentString = NSAttributedString(attachment: lAttachment)
        let lStrLabelText: NSMutableAttributedString
        
        if imageBehindText {
            lStrLabelText = NSMutableAttributedString(string: text + "  ")
            lStrLabelText.append(lAttachmentString)
        } else {
            lStrLabelText = NSMutableAttributedString(attributedString: lAttachmentString)
            lStrLabelText.append(NSMutableAttributedString(string: "  " + text))
        }
        
        self.attributedText = lStrLabelText
    }
    
    public func setErrorMsgProperties(msg : String){
        self.textAlignment    =   .center;
        self.numberOfLines    =   0
        self.lineBreakMode = .byWordWrapping
        
        let attributedString = NSMutableAttributedString(string: msg)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2
        paragraphStyle.alignment = .center
        paragraphStyle.lineBreakMode = .byTruncatingMiddle
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        self.attributedText = attributedString;
        self.font         =   ZSFont.regularFontLarge()
        self.textColor    =   ZColor.secondaryTextColorDark
    }
}
