//
//  UIWindow+.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 24/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public extension UIWindow {
    @available(iOS 13,*)
    func dismiss(with windowDismissalAnimation: UIWindowScene.DismissalAnimation,on viewController: UIViewController){
        guard let session = windowScene?.session,
            (rootViewController as? UINavigationController)?.topViewController == viewController
            else {return}
        dismissWindow(with: windowDismissalAnimation,session:  session)
    }
    
    func topViewController() ->  UIViewController? {
         return getTopViewController(vc: rootViewController)
     }
     
    private func getTopViewController(vc : UIViewController?) -> UIViewController? {
         if let rootVc = vc?.presentedViewController as? UINavigationController{
             return  getTopViewController(vc: rootVc.viewControllers.last)
         }
         else if let rootVc = vc?.presentedViewController{
             return getTopViewController(vc: rootVc)
         }
         return vc
     }
}
