//
//  ZSAnalyticsEventsType.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 03/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation
public enum ZSAnalyticsEventType {
    case none
    case login(_ login: LoginEventType)
    case demoVideo(_ demoVideo: DemoVideoEventType)
    case siriShortcuts(_ siriShortcuts: SiriShortcutsEventType)
    case inAppPurchase(_ inAppPurchase: InAppPurchaseEventType)
    case userRequest(_ userRequest: UserRequestEventType)
    case shareManager(_ shareManager: ShareManagerEventType)
    case rateOurApp(_ rateOurApp: RateOurAppEventType)
    case fileRequest(_ fileRequest: FileRequestsEventType)
    case iPhoneDocsList(_ iPhoneDocsList: iPhoneDocsListEventType)
    case guestSign(_ guestSign: GuestSignEventType)
    case notification(_ notification: NotificationEventType)
    case manageRecipient(_ manageRecipient: ManageRecipientEventType)
    case templateRequest(_ templateRequest: TemplateRequestEventType)
    case termsAndConditions(_ termsAndCondition: TermsAndConditionEventType)
    case documentPicker(_ documentPicker: DocumentPickerEventType)
    case iPadDocumentSigning(_ ipadDocumentSigning: iPadDocumentSigningEventType)
    case recipientRequest(_ recipientRequest: RecipientRequestEventType)
    case documentEditor(_ documentEditor: DocumentEditorEventType)
    case receivedDocuments(_ receivedDocuments: ReceivedDocumentsEventType)
    case iPadDocsList(_ iPadDocsList: iPadDocsListEventType)
    case sentDocuments(_ sentDocumentsType: SentDocumentsEventType)
    case myRequest(_ myRequest: MyRequestEventType)
    case iapSuccess(_ iapSuccess: IAPSuccessEventType)
    case iapDetail(_ iapDetail: IAPDetailEventType)
    case iapActivated(_ iapActivated: IAPActivatedEventType)
    case signatureManager(_ signatureManager: SignatureManagerEventType)
}

public enum LoginEventType {
    case successSignUser
    case successGuestUser
    case logoutSuccess
    case signInWithAppleIpad
    case signInWithAppleIphone
}

public enum DemoVideoEventType {
    case play
    case pause
    case fastForward
    case fastBackward
    case stalled
    case finished
}

public enum SiriShortcutsEventType {
    case requestId
    case pendingDocuments
    case inprogressDocuments
    case completedDocuments
    case expiringDocuments
    case receivedDocuments
}

public enum InAppPurchaseEventType {
    case initiated
    case failure
    case restoreButtonTapped
}

public enum UserRequestEventType {
    case accountCreated
    case userUpdated
}

public enum ShareManagerEventType {
    case ios
    case mac
}

public enum RateOurAppEventType {
    case Ipad
    case Iphone
    case surveySkipped
}

public enum FileRequestsEventType {
    case uploadFromZohoDocs
    case fileMaxSizeReached
    case viewHostedFiles
    case fileDeleted
    case completionCertDownload
}

public enum iPhoneDocsListEventType {
    case sentRequest
    case template
    case receivedRequest
    case searchListTemplate
    case searchListReceivedRequest
    case searchListSentRequest
    case iapUpgradeTemplate
    case loadMore
}

public enum GuestSignEventType {
    case declined
    case successInperson
    case mailed
    case successRemote
    case Assigned
}


public enum NotificationEventType {
    case receivedRequest
    case sentRequest
}

public enum ManageRecipientEventType {
    case addMe
}

public enum TermsAndConditionEventType {
    case hostSignerIntructions
    case hostSignerTerms
    case remoteSignerTerms
}

public enum TemplateRequestEventType {
    case deleted
    case created
    case updated
    case quickSend
    case use
}

public enum DocumentPickerEventType {
    case photos
    case zohoDocs
    case defaultBrowser
    case docScanner
    case fileSizeMaxReached
    case docScannerFinished
    case docScannerCancelled
}

public enum iPadDocumentSigningEventType {
    case shareDocument
    case autoFill
}

public enum RecipientRequestEventType {
    case newRecipientAdded
}

public enum DocumentEditorEventType {
    case sampleSigning
    case shareDocument
    case updateDraft
    case saveAsDraft
    case discardChanges
}


public enum ReceivedDocumentsEventType {
    case printDetailPage
    case shareDetailPage
}

public enum iPadDocsListEventType {
    case listLayout
    case gridLayout
    case loadMore
    case sentRequest
    case receivedRequest
    case template
    case searchListSentRequest
    case searchListReceivedRequest
    case searchListTemplate
}

public enum SentDocumentsEventType {
    case newDocumentTypeCreated
    case newFolderCreated
    case printDocumentDetailPage
    case sentRequestCreated
    case sentRequestReminded
    case sentRequestDeleted
    case sentRequestUnblocked
    case sentRequestRecalled
    case sentRequestMailed
    case sentRequestRestored
    case requestCreatedSendForSign
    case cloneDocument
    case requestCreatedSelfSign
    case requestUpdated
    case shareDocumentDetailPage
}

public enum MyRequestEventType {
    case deleted
    case mailed
    case restored
}

public enum IAPSuccessEventType {
    case professionalYearly
    case professionalMonthly
    case standardYearly
    case standardMonthly
}

public enum IAPDetailEventType {
    case professionalYearly
    case professionalMonthly
    case standardYearly
    case standardMonthly
}

public enum IAPActivatedEventType {
    case professionalYearly
    case professionalMonthly
    case standardYearly
    case standardMonthly
}

public enum SignatureManagerEventType {
    case initial
    case signature
}
