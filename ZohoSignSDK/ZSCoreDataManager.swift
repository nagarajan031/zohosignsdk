//
//  ZSCoreDataManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 29/08/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit
import CoreData

public class ZSCoreDataManager: NSObject {

    @objc public static let shared = ZSCoreDataManager()
    public let managedObjContext : NSManagedObjectContext
    let coreDataDefault: ZSCoreDataDefault
    
    let  thumbnailFolderPath =  ZSFileManager.shared.getFolderPathDirectory(withName: "downloads/thumbnail", isCacheDirectory: true) ?? ZSFileManager.shared.cacheDirectoryPath
    
    private override init() {
        coreDataDefault = ZSCoreDataDefault.shared
        managedObjContext = coreDataDefault.managedObjectContext

        super.init()
    }
    
    //MARK: COMMON
    public func save() {
        coreDataDefault.saveContext()
    }
    
    public func saveContentToDB() {
        coreDataDefault.saveChangesToDB()
    }
    
    public func truncateDB() {
        purgeAllData()
        /*
        do {
            try ZSFileManager.shared.removeSqLiteFile(ZSCoreDataSQLFileName)
        } catch {
            purgeAllData()
        }
        */
    }
    
    public func purgeAllData() {
        deletAllObjects(forEntities:
            Filter.self,
            MyRequest.self,
            SignRequest.self,
            Template.self,
            Documents.self,
            ZDocsFile.self,
            Attachment.self,
            TemplateFilter.self, shouldSave: false)
        saveContentToDB()
    }
    
    internal func fetchData<T: NSManagedObject>(withSortDescriptors sortDescriptors: [NSSortDescriptor]? = nil, forPredicate predicate: NSPredicate? = nil) -> [T]?
        {
        var fetchedObjects: [T]? = []
        managedObjContext.performAndWait {
            let fetchRequest: NSFetchRequest<NSFetchRequestResult> = T.fetchRequest()
            fetchRequest.predicate = predicate
            sortDescriptors.safelyUnwrap({ fetchRequest.sortDescriptors = $0 })
            do {
                fetchedObjects = try managedObjContext.fetch(fetchRequest) as? [T]
            }catch {
                ZError(error.localizedDescription)
            }
        }
        return fetchedObjects
    }

    //MARK: - Search
    public func searchFilter() -> Filter {
        var filterObj : Filter!
        do {
             let fetchRequest : NSFetchRequest<Filter> = Filter.fetchRequest()
             fetchRequest.predicate = NSPredicate(format: "filterKey == %@", REQUEST_FILTER_SEARCH)

            
            let fetchedResults = try managedObjContext.fetch(fetchRequest)
            if let _filterObj = fetchedResults.first {
                 filterObj = _filterObj
            }else{
                filterObj = Filter(context: managedObjContext)
                filterObj.filterName         =       "Search";
                filterObj.filterKey          =       REQUEST_FILTER_SEARCH;
                filterObj.displayOrder       =       3000;
                filterObj.isMyReqFilter    =        false;
            }
            
        }
        catch{ }
        
       return filterObj
        
    }
    
    public func clearSearchFilter() {
        searchFilter().myRequests = nil
        searchFilter().signRequests = nil
        save()
    }
    
    //MARK:- GET
    
    func addDocument(withDocumentId docId: String, andDataDict dict: ResponseDictionary) -> Documents {
        let fetchRequest : NSFetchRequest<Documents> = Documents.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "documentId == %@", docId)
        
        let doc: Documents
        if let aDoc = try? managedObjContext.fetch(fetchRequest).first {
            doc = aDoc
        }else{
            doc = Documents(context: managedObjContext)
        }
        
        doc.documentId = docId
        doc.documentName = dict[keyPath:"document_name"]
        dict[keyPath:"document_order"].safelyUnwrap { (documentOrder: String) in
            doc.documnetOrder = Int16(documentOrder) ?? 0
        }
        doc.thumbnailStr = dict[keyPath:"image_string"]
        doc.totalPages = dict[keyPath:"total_pages"] ?? 0
        if let sizeStr = dict["document_size"] as? String {
            var size = Int64(sizeStr ) ?? 0
            if size < 0 {
                size = 0
            }
            doc.fileSize = size
        }
        return doc
    }
    
    //MARK: UPDATE
    public func updateAndFetchDocumentFilters(isMyRequest: Bool) -> [Filter] {
        let predicate = NSPredicate(format: "isMyReqFilter == %@ && (filterName != %@)", NSNumber(booleanLiteral: isMyRequest), REQUEST_FILTER_SEARCH)
        
        guard let filters: [Filter] = fetchData(forPredicate: predicate) else { return []}
        
        guard filters.count == 0 else {
            return filters.sorted { $0.displayOrder!.intValue < $1.displayOrder!.intValue }
        }
        
        var filtersArray: [Filter] = []
        managedObjContext.performAndWait {
            
            let signRequestFilterKeyArray = [
                STATUS_ALL,
                STATUS_INPROGRESS,
                STATUS_COMPLETED,
                STATUS_DECLINED,
                STATUS_DRAFT,
                STATUS_EXPIRED,
                STATUS_DELETED
            ]
            
            var signRequestFilterNameArray = [
                "sign.mobile.request.status.completed",
                "sign.mobile.request.status.declined",
                "sign.mobile.request.status.draft",
                "sign.mobile.request.status.expired",
                "sign.mobile.request.status.trash"
            ]
            
            let myRequestFilterKeyArray = [
                STATUS_ALL,
                STATUS_SIGN_PENDING,
                STATUS_SIGN_SIGNED,
                STATUS_SIGN_DELETED
            ]
            
            var myRequestFilterNameArray = [
                "sign.mobile.request.status.completed",
                "sign.mobile.request.status.trash"
            ]
            
            if DeviceType.isIpad {
                signRequestFilterNameArray.insert(contentsOf: ["sign.mobile.request.sentRequests",
                "sign.mobile.request.outForSignature"], at: 0)
                myRequestFilterNameArray.insert(contentsOf: ["sign.mobile.request.receivedRequests",
                "sign.mobile.request.needsYourSignature"], at: 0)
            } else {
                signRequestFilterNameArray.insert(contentsOf: ["sign.mobile.request.status.all",
                "sign.mobile.request.status.inprogress"], at: 0)
                myRequestFilterNameArray.insert(contentsOf: ["sign.mobile.request.status.all",
                "sign.mobile.request.status.pending"], at: 0)
            }
            
            if isMyRequest {
                myRequestFilterKeyArray.enumeratedForEach { (index, key) -> (Void) in
                    filtersArray.append(createFilter(key, withIndex: index, filterName: myRequestFilterNameArray[index], isMyRequest: isMyRequest))
                }
            } else {
                signRequestFilterKeyArray.enumeratedForEach { (index, key) -> (Void) in
                    filtersArray.append(createFilter(key, withIndex: index, filterName: signRequestFilterNameArray[index], isMyRequest: isMyRequest))
                }
            }
            
            save()
        }
        
        return filtersArray
    }
    
    @discardableResult
    public func updateAndFetchTemplateFilters() -> [TemplateFilter] {
        guard let filters: [TemplateFilter] = fetchData() else {return []}
        
        guard filters.count == 0 else {
            return filters.sorted { $0.displayOrder!.intValue < $1.displayOrder!.intValue }
        }
        
        var filterArray: [TemplateFilter] = []
        managedObjContext.performAndWait {
            let templateFilterKeyArray = [
                STATUS_ALL,
                STATUS_DELETED,
                "Search"
            ]
            
            let templateFilterNameArray = [
                "sign.mobile.request.status.all",
                "sign.mobile.request.status.trash",
                "Search"
            ]
            
            templateFilterKeyArray.enumeratedForEach { (index, key) in
                let filter = TemplateFilter(context: managedObjContext)
                filter.filterName = templateFilterNameArray[index]
                filter.filterKey = key
                filter.displayOrder = NSNumber(value: index)
                filterArray.append(filter)
            }
            
            save()
        }
        return filterArray
    }
    
    private func createFilter(_ key: String, withIndex index: Int, filterName: String,isMyRequest: Bool) -> Filter {
        let filter = Filter(context: managedObjContext)
        filter.filterName = filterName
        filter.filterKey = key
        filter.displayOrder = NSNumber(integerLiteral: index)
        filter.isMyReqFilter = NSNumber(booleanLiteral: isMyRequest)
        return filter
    }
    
    //MARK: - DELETE Objects
    @objc public func deleteCoredataObjects(_ objects : [NSManagedObject], saveAfter : Bool = true)  {
        for obj in objects{
            managedObjContext.delete(obj)
        }
        
        if saveAfter {
            coreDataDefault.saveContext()
        }
    }
    
    @objc public func deleteCoredataObjectsWithIds(_ objectIds : [NSManagedObjectID], saveAfter : Bool = true)  {
        for objID in objectIds{
            let obj = managedObjContext.object(with: objID)
            managedObjContext.delete(obj)
        }
        
        if saveAfter {
            coreDataDefault.saveContext()
        }
    }

    public func deletAllObjects(forEntities entities: NSManagedObject.Type..., shouldSave: Bool) {
        entities.forEach { (entity) in
            managedObjContext.performAndWait {
                let fetchRequest = entity.fetchRequest()
                fetchRequest.includesPropertyValues = true
                if let entities: [NSManagedObject] = try? managedObjContext.fetch(fetchRequest) as? [NSManagedObject]{
                    entities.forEach({managedObjContext.delete($0)})
                }
                if shouldSave {
                    save()
                }
            }
        }
        
    }
    
}


public extension NSManagedObject {
    
    /*
    convenience init(context: NSManagedObjectContext) {
        let name = String(describing: type(of: self))
        let entity = NSEntityDescription.entity(forEntityName: name, in: context)!
        self.init(entity: entity, insertInto: context)
    }
    */
    
}
