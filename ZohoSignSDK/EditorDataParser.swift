//
//  EditorDataParser.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 25/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

protocol DocumentCreateEditorDelegate {
    func editorDataParserParsingCompleted()
    func editorDataParserParsingFailed()
    
    func editorDataParserSaveDraftCompleted()
    func editorDataParserRequestCreated()
    func editorDataParserTemplateCreated(_ template : Template?)

    func editorDataParserFieldsUpdated(_ fieldsListArray : [SignField])

    func editorDataParserRequestUpdateFailed()
}

class EditorDataParser: NSObject {
    fileprivate let templateDataManager = TemplateRequestManager()
    fileprivate let requestDataManager = SentDocumentsRequestManager()
    fileprivate let fileDatamanager = FileRequestManager()
    fileprivate let downloadProgressView = ZSDownloadProgressView()
    
    public var refRequest : SignRequest?
    
    var isSelfSigningRequest = false
    var isFieldsEdited = false

    public var refTemplate : Template?
    
    var fieldTypesArray : [ZSFieldType] = []
    var deletedFieldIdsArray : [String] = []
    var fieldsListArray : [SignField] = []

    var contentViews : [Int : DocumentEditorBaseView] = [:]

    var documentDownloader : ZSDocumentDownloader?
    
    weak var delegate : (UIViewController & DocumentCreateEditorDelegate)?
    var recipientsArray : [ZSRecipient] = []
    var pagesArray : [ZSPage] = []
    var currentPage : ZSPage!

    var pageWidth : CGFloat = 0

    public var sampleSigner : ZSRecipient?
    public var selectedRecipient : ZSRecipient!

    var isSigningCompleted = false
    var isThirdPartySignSDK = false
    
    public var isFromCreatePage = false
    
    public var createReqType : ZSDocumentCreatorType = .SIGN_AND_SEND

    var documentsArray : [ZSPDFDocument] = []

    //Download & share doc
    private let shareDocumentManager = ShareDocumentManager()
    private var isDocDownloadInProgress = false
    private let fileDataManager = FileRequestManager()
    
    let transitionManager = PresentationManager()
    
    let fileManager  = ZSFileManager()

    //MARK: - Helpers & Getters
    func getCurrentEditorView(_ diff : Int = 0) -> DocumentEditorBaseView?  {
        return contentViews[currentPage.originalPageNumber + diff]
    }
    
    func getPreviousEditorView() -> DocumentEditorBaseView?  {
        return contentViews[currentPage.originalPageNumber - 1]
    }
    
    //MARK: - Parser
    func parseSampleDocFiles( thumbnailView : inout ZSPDFThumbnailView){
        
        let tempDirectoryURL = NSURL.fileURL(withPath: NSTemporaryDirectory(), isDirectory: true)

        let page = ZSPage()
        page.docOrder   =    0
        page.docName    = "Non-Disclosure Agreement"
        page.pageNumber =   1
        page.originalPageNumber = 1
        page.filePath   =   tempDirectoryURL.appendingPathComponent("sample_0.jpg").path
        page.docId      =   "1";
        pagesArray.append(page)
        
        let page1 = ZSPage()
        page1.docOrder   =   0;
        page1.docName   =   "Non-Disclosure Agreement"
        page1.pageNumber =   2;
        page1.originalPageNumber = 2;
        page1.filePath   =   tempDirectoryURL.appendingPathComponent("sample_1.jpg").path
        page1.docId      =   "1";
        pagesArray.append(page1)
        

        sampleSigner = ZSRecipient()
        sampleSigner?.email = "charles@bridgeford.com"
        sampleSigner?.name   =  ZS_DEFAULT_SAMPLE_SIGN_USERNAME;
        sampleSigner?.inPersonName = ZS_DEFAULT_SAMPLE_SIGN_USERNAME;
        recipientsArray.append(sampleSigner!)
        selectedRecipient = sampleSigner;

        let pdfDocument = ZSPDFDocument(url: Bundle.main.url(forResource: "sample", withExtension: "pdf")!)
        pdfDocument?.title = "Non-Disclosure Agreement"
        documentsArray = [pdfDocument!]

        
        for index in 0...pdfDocument!.pageCount{
            let page = pdfDocument!.page(at: index)
            let pageRect = page?.bounds(for: .artBox)
            
            let imgFilePath =  tempDirectoryURL.appendingPathComponent("sample_\(index).jpg")//String(format: "%@/%i.jpg",toFolder, index)
            
            if let image = page?.thumbnail(of: (pageRect?.size)!, for: .artBox){
                let imgData = image.pngData()
                
                do{
                    try imgData?.write(to: imgFilePath, options: .atomic)
                }catch{}
            }
        }
        page.calculatePageSize()
        page1.calculatePageSize()

        generateFieldsArray()
              
    }
    

    func saveDraft() {

        let tempFields = fieldsListArray.filter { (field) -> Bool in
            return field.isOtherRecipientField == false
        }

        if selectedRecipient != nil {
            selectedRecipient.draftFieldListArray.removeAll()
            selectedRecipient.draftFieldListArray.append(contentsOf: tempFields)
        }
        
        
        guard let dict = validateSignFieldValues() else{
            delegate?.editorDataParserRequestUpdateFailed()
            return
        }

        weak var weakself = self

        if let template = refTemplate{
            ZSProgressHUD.showActivity(inView: (delegate!.navigationController?.view)!, status: "sign.mobile.activityIndicator.saveTemplate".ZSLString)
            templateDataManager.updateTemplate(id: template.templateId!, params: dict, success: {[weak self] (success) in
                weakself?.delegate?.editorDataParserSaveDraftCompleted()
                let notif = ZNotificationToastView(title: "sign.mobile.notify.templateSaved".ZSLString)
                notif.showSuccessMsg()
            }) {[weak self] (error) in
                weakself?.delegate?.handleDataManagererror(error)
                weakself?.delegate?.editorDataParserRequestUpdateFailed()
            }
        }
        else if let request = refRequest{
            ZSAnalytics.shared.track(forEvent: .documentEditor(isFromCreatePage ? .saveAsDraft : .updateDraft))

            ZSProgressHUD.showActivity(inView: (delegate!.navigationController?.view)!, status: "sign.mobile.activityIndicator.savedraft".ZSLString)

            requestDataManager.updateDraftRequest(reqID: request.requestId!, params: dict, success: { (response) in
                
                if weakself?.isFromCreatePage ?? false{
                    NotificationCenter.default.post(name: NSNotification.Name(kSignRequestListRefreshNotification), object: request, userInfo:[kNotificationUserInfoSignRequest : ZSAlertType.SentReq_Drafted])
                }else{
                    NotificationCenter.default.post(name: NSNotification.Name(kSignRequestListRefreshNotification), object: request, userInfo:[kNotificationUserInfoSignRequest : ZSAlertType.SentReq_Updated])
                }

                if (UserManager.shared.currentUser?.isUserProfileEdited ?? false){
                    UserManager.shared.updateProfileInServer()
                }
                
                weakself?.delegate?.editorDataParserSaveDraftCompleted()

            }) { (error) in
                weakself?.delegate?.handleDataManagererror(error)
                weakself?.delegate?.editorDataParserRequestUpdateFailed()
            }
        }
    }
    
    func submitRequestToServer() {
        if selectedRecipient == nil{
            return
        }
        
        
        selectedRecipient.draftFieldListArray.removeAll()
        let tempFields = fieldsListArray.filter { (field) -> Bool in
            return field.isOtherRecipientField == false
        }
        
        selectedRecipient.draftFieldListArray.append(contentsOf: tempFields)
        
        guard let dict = validateSignFieldValues(isCompleted: true) else {
            return
        }
        
        if refRequest?.isSelfSign?.intValue == 1 {
            ZSProgressHUD.showActivity(inView: (delegate?.navigationController?.view)!, status: "sign.mobile.activityIndicator.signingDoc".ZSLString)
            
            weak var weakself = self
            requestDataManager.submitSelfSignRequest(reqID: refRequest!.requestId!, params: dict, success: { (success) in
                
                NotificationCenter.default.post(name: NSNotification.Name(kSignRequestListRefreshNotification), object: weakself?.refRequest, userInfo:[kNotificationUserInfoSignRequest : ZSAlertType.SentReq_Created])

                
                if (UserManager.shared.currentUser?.isUserProfileEdited ?? false){
                    UserManager.shared.updateProfileInServer()
                }
                weakself?.isSigningCompleted = true
                weakself?.delegate?.editorDataParserRequestCreated()
            }) { (error) in
                weakself?.delegate?.handleDataManagererror(error)
            }
            
        }
        else{
            var signersArray : [ZSRecipient] = []
            recipientsArray.forEach { (recep) in
                if(recep.isNeedToSign || (recep.actionType == ZS_ActionType_Approver)){
                    signersArray.append(recep)
                }
            }
           let confirmationController = ZRequestDetailsConfirmationController(menuItems: signersArray)
            
            weak var weakself = self
            let reqId = refRequest?.requestId ?? ""
            
            
            confirmationController.confirmDetailsHandler = {(signImmediately) in
                ZSProgressHUD.showActivity(inView: (weakself?.delegate?.navigationController?.view)!, status: "sign.mobile.activityIndicator.submittingDoc".ZSLString)
                
                weakself?.requestDataManager.submitRequest(reqID: reqId, params: dict, success: { (responseObj) in
                    NotificationCenter.default.post(name: NSNotification.Name(kSignRequestListRefreshNotification), object: weakself?.refRequest, userInfo:[kNotificationUserInfoSignRequest : ZSAlertType.SentReq_Created])
                    
                    if (UserManager.shared.currentUser?.isUserProfileEdited ?? false){
                        UserManager.shared.updateProfileInServer()
                    }
                    
                    ZSProgressHUD.dismiss()
                    weakself?.delegate?.editorDataParserRequestCreated()
                    
                    if signImmediately, let respDict = responseObj as? [AnyHashable : Any], let reqDict = respDict["requests"] as? [AnyHashable : Any] {
                        let request : MyRequest? = ZSCoreDataManager.shared.addMyRequest(with: reqDict)
                        request?.isReviewCompleted = true
                        NotificationCenter.default.post(name: .loadMyRequest, object: request)
                    }
                    
                }, failure: { (error) in
                    weakself?.delegate?.handleDataManagererror(error)
                })
            }
            
            if DeviceType.isIphone {
                confirmationController.modalPresentationStyle = .custom
                confirmationController.transitioningDelegate = transitionManager
                delegate?.present(confirmationController, animated: true, completion: nil)
            }
            else{
                let nav = UINavigationController(rootViewController: confirmationController)
                nav.navigationBar.barTintColor = ZColor.bgColorWhite
                nav.modalPresentationStyle = .popover
                nav.popoverPresentationController?.barButtonItem = delegate?.navigationItem.rightBarButtonItem
                delegate?.present(nav, animated: true, completion: nil)
            }
            
            delegate?.navigationItem.rightBarButtonItem?.isEnabled = true
        }

    }
    
    func resetAllFields()  {
        fieldsListArray.removeAll()
        
        recipientsArray.forEach { (receipient) in
            for i in 0..<receipient.draftFieldListArray.count {
                receipient.draftFieldListArray[i].isOtherRecipientField = (receipient.actionId != selectedRecipient.actionId)
                fieldsListArray.append(receipient.draftFieldListArray[i])
            }
        }
    }
    
    func changeCurrentRecipient(_ recep : ZSRecipient)  {
        selectedRecipient.draftFieldListArray.removeAll()
        let tempFields = fieldsListArray.filter { (field) -> Bool in
            return field.isOtherRecipientField == false
        }
        
        selectedRecipient.draftFieldListArray.append(contentsOf: tempFields)
        
        selectedRecipient = recep

        resetAllFields()
        
        contentViews.forEach { (key, editorView) in
            editorView.isTemplateDocumentFields = selectedRecipient.isTemplateUser
            editorView.clearAllSignFields()
            
            let filteredResults = fieldsListArray.filter { (field) -> Bool in
                return ((field.pageIndex == editorView.pageIndex) && (field.docId == editorView.docId))
            }
            
            if (filteredResults.count > 0) {
                editorView.preFillSignFields(filteredResults)
            }
        }
    }

    func shareDocument(fromButton btn: UIButton?)  {
        ZSAnalytics.shared.track(forEvent: .documentEditor(.shareDocument))
        
        if let refRequest = refRequest, let reqId = refRequest.requestId {
            
            if fileManager.isFileAvailableForRequest(requestId: reqId, isSentRequests: true, isCache: true){
                guard let actionBtn = btn else{
                    return
                }
                
                let filepathArray = ZSFileManager.shared.getSavedFilesPathAfterDecryption(forRequestId: refRequest.requestId, isSentRequests: true, saveCache: (refRequest.status !=  STATUS_INPROGRESS))
                guard let file = filepathArray as? [String] else {
                    return
                }
                
                guard let filePath = CommonUtils.mergePDF(listOfFilePaths: file, outputFileName: refRequest.name!) else {
                    return
                }
                
                shareDocumentManager.url = URL(fileURLWithPath: filePath)
                shareDocumentManager.uti = filePath.contains(".zip") ? "com.pkware.zip-archive" : "com.adobe.pdf"
                shareDocumentManager.rootVc = delegate
                shareDocumentManager.presentOptionsMenu(from: actionBtn.bounds, in: actionBtn, animated: true)
                
            } else {
                if isDocDownloadInProgress {
                    return
                }
                isDocDownloadInProgress = true
                ZSProgressHUD.show(inView: (delegate?.navigationController?.view)!, status: "sign.mobile.activityIndicator.preparingDocument".ZSLString)
                weak var weakSelf = self
                
                fileDataManager.downloadFilesForSentRequest(requestID: refRequest.requestId!, saveChace: true, returnAsPDF: false, progress: { (progress) in
                    
                }, success: { (filePaths) in
                    weakSelf?.isDocDownloadInProgress = false
                    ZSProgressHUD.dismiss()
                    weakSelf?.shareDocument(fromButton: btn)
                }) { (error) in
                    weakSelf?.delegate?.handleDataManagererror(error)
                }
            }
        }
               
    }
    
    func addSignField(tapPoint: CGPoint, fieldType: ZSignFieldType, isQuickAdd: Bool, pangesture: UIPanGestureRecognizer?, inView bgScrollView : inout UIScrollView)  {
        SwiftUtils.generateImpactFeedBack()
        SwiftUtils.dismissPoptip()
        
        contentViews.forEach { (key, value) in
            if( value.isEditingMode){
                value.endEditing()
                bgScrollView.isScrollEnabled = true
            }
        }
       
        contentViews.forEach { (key, editorView) in
            if(editorView.frame.contains(tapPoint)){
                var point = CGPoint(x: editorView.bounds.width/2, y: editorView.bounds.height/2)
               editorView.isUserInteractionEnabled = true
                if(isQuickAdd){
                    editorView.addSignField(point: point, fieldType: fieldType)
                    return
                }else if let gesture = pangesture{
                    point = gesture.location(in: editorView)
                }else{
                    point = bgScrollView.convert(tapPoint, to: editorView)
                }
                editorView.addSignField(point: point, fieldType: fieldType)
            }
        }
    }
    
    //MARK: - Data Manager
    func getRequestTypes()  {
        requestDataManager.getRequestFieldTypes(success: {[weak self] (array) in
            self?.fieldTypesArray = array as! [ZSFieldType]
        }) {[weak self] (error) in
            let archievedObj = UserDefaults.standard.object(forKey: kUserDefaultkeyFieldType) as? Data ?? Data()
            if let array = try? JSONDecoder().decode([ZSFieldType].self, from: archievedObj) as? [ZSFieldType] {
                self?.fieldTypesArray.append(contentsOf: array)
            }
        }
    }
    
    
    func downloadDocument()  {
        if let req = refRequest {
            documentDownloader = ZSDocumentDownloader(sentRequest: req)
//            updateTitleView(title: req.name ?? "", color: ZColor.draftColor)
            delegate?.title = req.name
        }else if let temp = refTemplate{
            documentDownloader = ZSDocumentDownloader(template: temp)
            delegate?.title = temp.name
        }
        
        weak var weakSelf = self;
        
        documentDownloader?.completionHandler = { _pagesArray in
            weakSelf?.downloadProgressView.dismiss()
            weakSelf?.generatePagesArray(pages: _pagesArray)
        }
        
        documentDownloader?.downloadProgress = {(currentDownloadIndex, totalPages, downloadProgress) in
            if let dView = weakSelf?.delegate?.view{
                if totalPages > 1{
                    let str = String(format: "sign.mobile.document.downloading".ZSLString, currentDownloadIndex,totalPages)
                    weakSelf?.downloadProgressView.titleLabel.text = str
                }
                if (!(weakSelf?.downloadProgressView.isDescendant(of: dView) ?? true)) {
                    weakSelf?.downloadProgressView.show(inView: dView, progress: downloadProgress)
                }
                else{
                    weakSelf?.downloadProgressView.setProgress(downloadProgress)
                }
            }
        }
        documentDownloader?.downloadErrorHandler = {error in
            weakSelf?.delegate?.handleDataManagererror(error)
        }
        
        documentDownloader?.startDownloadingDocument()
    }
    
    func generatePagesArray(pages : [ZSPage]){
        if pages.count == 0 {
            delegate?.editorDataParserParsingFailed()
            return;
        }
        
       let sortedArry =  pages.sorted { (page1, page2) -> Bool in
            return page2.docOrder > page1.docOrder
        }
        
        pagesArray = sortedArry

        currentPage = sortedArry.first
        
        let fileManager = ZSFileManager()

        if let reqDocs = refRequest?.documents{
            let sortedArray =  reqDocs.sorted(by: { (doc1, doc2) -> Bool in
                return doc1.documnetOrder <  doc2.documnetOrder
            })
            sortedArray.forEach({ (doc) in
                let filepath = fileManager.getFile(withFileId: doc.documentId!, isSharedLocation: false)
                if let pdf = ZSPDFDocument(url: URL(fileURLWithPath: filepath!)){
                    pdf.title = doc.documentName ?? ""
                    documentsArray.append(pdf)
                }
            })
        }
        else if let templateDocs = refTemplate?.documents{
            let sortedArray =  templateDocs.sorted(by: { (doc1, doc2) -> Bool in
                return doc1.documnetOrder <  doc2.documnetOrder
            })
            
            sortedArray.forEach({ (doc) in
                let filepath = fileManager.getFile(withFileId:doc.documentId!, isSharedLocation: false)
                if let pdf = ZSPDFDocument(url: URL(fileURLWithPath: filepath!)){
                    pdf.title = doc.documentName ?? ""
                    documentsArray.append(pdf)
                }
                
            })
        }
        
//        thumbnailView.documentsArray = docsArray
//        thumbnailView.reloadData()
//
        generateFieldsArray()
    }
    
    func generateFieldsArray(){
        var actionArray : [[String : Any]] = []
        if let dict =  NSKeyedUnarchiver.unarchiveObject(with: refRequest?.actions ?? Data()) as? [[String : Any]]  {
            actionArray = dict
        }
        else if let template =  refTemplate, let docFields = template.documentFields{
            if let tempArray = NSKeyedUnarchiver.unarchiveObject(with:docFields) as? [[String : Any]] {
                var dict = tempArray.first
                dict?["role"] = "sign.mobile.create.prefillByYou".ZSLString
                dict?["action_type"] = "SIGN"
                dict?["is_Template_User"] = "1"
                actionArray.append(dict!)
            }
            if let dict =  NSKeyedUnarchiver.unarchiveObject(with: template.actions ?? Data()) as? [[String : Any]]  {
                actionArray.append(contentsOf: dict)
            }
        }
        
        actionArray.forEach { (dict) in
            
            let recipient    =   ZSRecipient()
            recipient.recipientID   =   dict[keyPath:"recipient_id"]
            recipient.email         =   dict[keyPath:"recipient_email"]
            recipient.name          =   dict[keyPath:"recipient_name"]
            if ((refTemplate) != nil) {
                if let role = dict["role"] as? String{
                    recipient.name          =   role
                }else{
                    recipient.name          =   recipient.email
                }
            }
            if let actionType = dict["action_type"]  as? String{
                recipient.isNeedToSign  =  ((actionType == "SIGN") || (actionType == "INPERSONSIGN"))
            }else{
                recipient.isNeedToSign  =  false
            }
            recipient.isTemplateUser    = ((dict["is_Template_User"] as? String) == "1");
            recipient.fields        =   dict[keyPath:"fields"]
            recipient.actionId      =   dict[keyPath:"action_id"]
            recipient.actionType    =   dict[keyPath:"action_type"]
            recipient.deliveryType  =   dict[keyPath:"verification_type"]
            recipient.secretCode    =   dict[keyPath:"verification_code"]
            recipient.isVerificationNeeded =   ((dict["verify_recipient"] as? Int) == 1)
            recipient.signingOrder  =   dict[keyPath: "signing_order"] ?? 0
            recipient.countryCode   =  dict[keyPath:"recipient_countrycode"]
            recipient.phoneNumber   =  dict[keyPath:"recipient_phonenumber"]
            recipient.privateMessage  =  dict[keyPath:"private_notes"]
            recipient.isHost  =  ((dict["ishost"] as? Int) == 1);
            recipient.inPersonName      =   dict[keyPath:"in_person_name"]
            recipient.inPersonEmail     =   dict[keyPath:"in_person_email"]
            recipient.userRole      =      dict[keyPath:"role"]

            var receipientFieldsArray : [SignField] = []
            if let fieldsArray = recipient.fields as? [[String : Any]]{
                fieldsArray.forEach({ (fieldDict) in
                    var field = SignField()
                    
                    if let fieldType = fieldDict["field_type_name"] as? String{
                            switch fieldType{
                            case "Signature":
                                field.type = .signature
                                if  (fieldDict["field_value"] != nil){
                                    field.signImage = UserManager.shared.currentUser?.getSignatureImage()
                                }else{field.signImage = nil}
                                break
                            case "Initial":
                                field.type = .initial
                                if  (fieldDict["field_value"] != nil){
                                    field.signImage = UserManager.shared.currentUser?.getInitialImage()
                                }else{field.signImage = nil}
                                break
                            case "Name":
                                field.type = .name
                                break
                            case "Company":
                                field.type = .company
                                break
                            case "Email":
                                field.type = .email
                                break
                            case "Date":
                                field.type = .signDate
    
                                if  let dateFormat =  fieldDict["date_format"]  as? String{
                                    field.dateFormat = dateFormat
                                }else{field.dateFormat = UserManager.shared.currentUser?.dateFormat}
                                
                                field.dateValue = DateManager.convertLongToDateString(longValue: fieldDict["field_value"] as? String, dateFormat: field.dateFormat)
                                break
                            case "Textfield":
                                field.type = .textBox
                                break
                            case "Jobtitle":
                                field.type = .jobTitle
                                break
                            case "Checkbox":
                                field.type = .checkBox
                                field.isReadOnly = (fieldDict[keyPath:"is_read_only"]  ?? 0) == 1
                                field.isCheckboxTicked = ((fieldDict[keyPath:"default_value"] ?? 0) == 1)
                                break
                            case "CustomDate":
                                field.type = .date
                                
                                if  let dateFormat =  fieldDict["date_format"]  as? String{
                                    field.dateFormat = dateFormat
                                }else{field.dateFormat = UserManager.shared.currentUser?.dateFormat}
                                
                                field.dateValue = DateManager.convertLongToDateString(longValue: fieldDict["field_value"] as? String, dateFormat: field.dateFormat)
                                break
                            case "Dropdown":
                                field.type = .dropDown
                                if let fieldDicts = fieldDict["dropdown_values"] as? [AnyHashable]{
                                    field.dropdownFields = fieldDicts
                                }
                                if let defaultValue = fieldDict["default_value"] as? String{
                                    field.defaultValue = defaultValue
                                }
                                break
                            case "Attachment":
                                field.type = .attachment
                                break
                            case "Radiogroup":
                                field.type = .radioGroup
                                field.fieldLabel = fieldDict[keyPath: "Radiogroup"]
                                field.fieldName = fieldDict[keyPath:"field_name"]
                                field.defaultValue = fieldDict[keyPath: "default_value"]
                                field.isReadOnly = fieldDict[keyPath: "is_read_only"] ?? false
                                field.isMandatory = fieldDict[keyPath:"is_mandatory"] ?? false
                                field.pageIndex = fieldDict[keyPath:"page_no"] ?? 0
                                fieldDict["sub_fields"].safelyUnwrap { (subFieldsDict: [ResponseDictionary]) in
                                    let radioGroup: RadioGroup = RadioGroup(name: field.fieldName!)
                                    radioGroup.isReadOnly = field.isReadOnly
                                    radioGroup.isOptional = !field.isMandatory
                                    subFieldsDict.forEach { (subFieldDict) in
                                        var value : RadioValues = RadioValues(name: subFieldDict[keyPath: "sub_field_name"] ?? "")
                                        value.subFieldId = subFieldDict[keyPath: "sub_field_id"]
                                        value.xValue = subFieldDict[keyPath: "x_value"] ?? 0
                                        value.yValue = subFieldDict[keyPath: "y_value"] ?? 0
                                        value.width = subFieldDict[keyPath: "width"] ?? 0
                                        value.height = subFieldDict[keyPath: "height"] ?? 0
                                        value.pageNo = subFieldDict[keyPath: "page_no"]
                                        value.defaultValue = subFieldDict[keyPath: "default_value"] ?? false
                                        radioGroup.values.append(value)
                                    }
                                    field.radioGroup = radioGroup
                                }
                            default:
                                return
                            }
                        
//                        let pageWidth = bgScrollView.frame.width - 20
                        let page = pagesArray.first
                        field.isCompleted = (fieldDict["field_value"] != nil)
                        if let textPropDict = fieldDict["text_property"] as? [String : Any]{
                            
                            let fontFamily : String  =  textPropDict[keyPath:"font"] ?? "Roboto"
                            field.serverFontSize     = textPropDict[keyPath:"font_size"] ?? 14

                            let fontSize  : Int    =  FontManager.calibaratedDeviceFontSize(forServerFontSize: String(field.serverFontSize), pageWidth: pageWidth, imageWidth: page?.width ?? pageWidth)
                            let textColor     =  ZColor.color(hexString: textPropDict[keyPath:"font_color"] ?? "FFFFFF")
                            let isBold   : Bool    = textPropDict[keyPath:"is_bold"] ?? false
                            let isItalic : Bool     = textPropDict[keyPath:"is_italic"] ?? false
                            let isUnderline : Bool   = textPropDict[keyPath:"is_underline"] ?? false
                            
                            field.currentTextProperty = TextProperty(fontColor: textColor, fontFamily: fontFamily, fontSize: fontSize, isBold: isBold, isItalics: isItalic, isUnderline: isUnderline)
                            field.isReadOnly    = textPropDict[keyPath:"is_read_only"] ?? false
                        }
//                        else{
//                            field.fontFamily    =   "Roboto";
//                            field.fontSize    =   8;
//                        }
                        
                        field.fieldId   = fieldDict[keyPath:"field_id"]
                        field.fieldName = fieldDict[keyPath:"field_name"]
                        field.isMandatory = fieldDict[keyPath:"is_mandatory"] ?? false
                        field.pageIndex = fieldDict[keyPath:"page_no"] ?? 0
                        field.docId         =   fieldDict[keyPath:"document_id"]
                        field.fieldTypeName = fieldType
                        if field.dateValue != nil{
                            field.textValue = field.dateValue
                        }else{
                            field.textValue = fieldDict[keyPath:"field_value"]
                        }
                        field.xValue        =   fieldDict[keyPath:"x_value"] ?? 0
                        field.yValue        =   fieldDict[keyPath:"y_value"] ?? 0
                        field.width         =   fieldDict[keyPath:"width"] ?? 0
                        field.height        =   fieldDict[keyPath:"height"] ?? 0

                        
                    }
                    receipientFieldsArray.append(field)
                })
            }
            
            recipient.draftFieldListArray  = receipientFieldsArray
            recipientsArray.append(recipient)
            
        }
        
        selectedRecipient = recipientsArray.first
        if selectedRecipient == nil {
            selectedRecipient = ZSRecipient()
        }
        
        delegate?.editorDataParserParsingCompleted()
        
//        perform({
//            self.fileDatamanager.rotateFilePage(reqId:self.refRequest!.requestId ?? "",  fileId: self.refRequest?.documents?.first?.documentId ?? "", pageNo: 0, success: { (respo) in
//
//            }) { (error) in
//
//            }
//        }, afterDelay: 2)
//        selectedRecipient = recipientsArray.first
//        if selectedRecipient == nil {
//            selectedRecipient = Recipient()
//        }
//
//        resetAllFields()
//        for receipient in recipientsArray {
//            if(receipient.isNeedToSign){
//                selectSiginingReceipient(receipient)
//                break;
//            }
//        }
//
//
//        self.navigationItem.rightBarButtonItem?.isEnabled  = ((fieldsListArray.count > 0) || (selectedRecipient.actionType == ZS_ActionType_Approver))
//
//        adjustViewFrames()
//        addFieldSelectionView()
//        addContentViews()
//        thumbnailView.goToPage(indexPath: IndexPath(item: 0, section: 0), animated: false)
//        fieldCountBtn.setBadge(String(format: "%i", fieldsListArray.count))
//
//        contentScrollView.isHidden = true
//        perform({ [weak self] in
//            self?.updateContentViews()
//        }, afterDelay: 0)
    }
    
    //MARK: - Data Parser
    func validateSignFieldValues(isCompleted : Bool = false) -> [String : Any]?  {
        let firstDoc = refRequest?.documents?.first //this is for fail safe case
        
        var requestDict : [String : Any] = [:]
        
        var emptyFieldRecipients : [String] = []
        if isCompleted {
            for recipient in recipientsArray{
                if(recipient.actionType == ZS_ActionType_InPerson && (recipient.draftFieldListArray.count == 0)){
                    emptyFieldRecipients.append(recipient.inPersonName ?? "")
                }
                else if(recipient.isNeedToSign && (recipient.draftFieldListArray.count == 0)){
                    emptyFieldRecipients.append(recipient.email ?? "")
                }
            }
            
        }
        if emptyFieldRecipients.count == 1 {
            if refRequest?.isSelfSign == 1{
                delegate?.showAlert(withMessage: String(format: "sign.mobile.request.alert.addAtleastOneField".ZSLString, emptyFieldRecipients.first ?? ""))
            }else{
                delegate?.showAlert(withMessage: String(format: "sign.mobile.recipient.addAlteastOneField".ZSLString, emptyFieldRecipients.first ?? ""))
            }
            return nil
            
        }else if emptyFieldRecipients.count > 1 {
            delegate?.showAlert(withMessage: String(format: "sign.mobile.recipient.addAlteastOneFieldForFollowingRecipients".ZSLString, emptyFieldRecipients.joined(separator: ",\n ")))
            return nil
        }
        
        
        
        if let request = refRequest{
            requestDict["request_name"]  = request.name
        }
        
        var actionArr : [[String : Any]] = []
        var documentsFieldsArray: [[String : Any]] = []
        
        for recipient in recipientsArray{
            var tempDict : [String : Any] = [:]
            tempDict["action_id"]          =  recipient.actionId
            tempDict["verify_recipient"]   =  "false"
            tempDict["action_type"]        =  recipient.actionType
            tempDict["signing_order"]      =  NSNumber(value: recipient.signingOrder)
            tempDict["recipient_phonenumber"] =  recipient.phoneNumber
            tempDict["recipient_countrycode"] =  recipient.countryCode
            
            if(recipient.isVerificationNeeded){
                tempDict["verify_recipient"]      =  "true"
                if let code =  recipient.secretCode {
                    tempDict["verification_code"] =  code
                }
                tempDict["verification_type"] =  recipient.deliveryType;
            }
            
            if(recipient.isHost){
                tempDict["ishost"]   =  "true";
                tempDict["in_person_name"]   =  recipient.inPersonName;
                tempDict["in_person_email"]  =  recipient.inPersonEmail;
            }
            
            var imageArr : [[String : Any]] = []
            var textArr : [[String : Any]] = []
            var dateArr : [[String : Any]] = []
            var checkboxArr : [[String : Any]] = []
            var dropDownArr : [[String : Any]] = []
            var attachmentsArr : [[String : Any]] = []
            var radioGroupArr: [[String:Any]] = []
            
              
            for i in 0..<recipient.draftFieldListArray.count {

                var signfield =  recipient.draftFieldListArray[i]
                
                var fieldType = fieldTypesArray.filter({ (type) -> Bool in
                    return (type.fieldType == signfield.type)
                }).first
                
                if(fieldType == nil){
                    fieldType = fieldTypesArray.filter({ (type) -> Bool in
                        return (type.fieldType == .textBox)
                    }).first
                }
                if (fieldType == nil) {
                    continue
                }
                
                let page = pagesArray.filter { (_page) -> Bool in
                    return _page.docId == signfield.docId
                }.first
                
                signfield.xValue        =   (signfield.frame.origin.x / pageWidth) * 100;
                signfield.yValue        =   (signfield.frame.origin.y  / (page?.calibratedPageHeight(pageWidth) ?? 1)) * 100;
                signfield.width         =   (signfield.frame.size.width  / pageWidth) * 100;
                signfield.height        =   (signfield.frame.size.height / (page?.calibratedPageHeight(pageWidth) ?? 1)) * 100;
                
                
                var fieldDict : [String : Any] = [:]
                fieldDict["field_type_id"] = fieldType?.typeId
                fieldDict["width"] = String(format: "%.5f", signfield.width)
                fieldDict["height"] = String(format: "%.5f", signfield.height)
                fieldDict["x_value"] = String(format: "%.5f", signfield.xValue)
                fieldDict["y_value"] = String(format: "%.5f", signfield.yValue)
                fieldDict["page_no"] = String(format: "%d", signfield.pageIndex)
                fieldDict["is_mandatory"] = signfield.isMandatory ? "true" :"false"
                fieldDict["field_name"] = signfield.fieldName
                
                if ((signfield.fieldId) != nil) {fieldDict["field_id"] = signfield.fieldId}
                
                fieldDict["document_id"] = signfield.docId ?? firstDoc?.documentId
                
                if createReqType == .SIGN_AND_SEND{
                    if let dateVal = signfield.dateValue, !dateVal.isEmpty{
                        fieldDict["field_value"] = DateManager.convertStringToLong(stringValue: signfield.dateValue, dateFormat: signfield.dateFormat)
                        if (signfield.dateFormat != nil) {
                            fieldDict["date_format"] = signfield.dateFormat
                        }
                    }
                    else if (signfield.signImage != nil){
                        
                    }
                    else if let textVal = signfield.textValue, !textVal.isEmpty{
                        fieldDict["field_value"] = signfield.textValue
                        
                        if (signfield.type == .name) {
                            fieldDict["name_format"] = signfield.nameFormat()
                        }
                    }
                }
                if ((signfield.type  == .signature) || (signfield.type  == .initial))
                {
                    imageArr.append(fieldDict)
                }
                else if (signfield.type  == .attachment)
                {
                    attachmentsArr.append(fieldDict)
                }
                else if (signfield.type == .checkBox)
                {
                    fieldDict["is_read_only"] = signfield.isReadOnly ? "true" : "false"
                    fieldDict["default_value"] = signfield.isCheckboxTicked ? "true" : "false"
                    fieldDict["field_value"] = signfield.isCheckboxTicked ? "true" : "false"
                    
                    checkboxArr.append(fieldDict)
                }
                else if signfield.type == .radioGroup {
                    guard let radioGroup = signfield.radioGroup else {continue}
                    
                    fieldDict["field_name"]  = "Radiogroup"
                    fieldDict["is_mandatory"] = !radioGroup.isOptional
                    fieldDict["is_read_only"] = radioGroup.isReadOnly
                    fieldDict["deleted_sub_fields"] = radioGroup.deletedSubFields
                    var subFieldsArr: [[String:Any]] = []
                    radioGroup.values.enumeratedInPlaceLoop { (_, value) -> RadioValues in
                        var subField:[String:Any] = [:]
                        var mutableCopy = value
                       
                        mutableCopy.xValue        =   (mutableCopy.rect!.origin.x / pageWidth) * 100
                        mutableCopy.yValue        =   (mutableCopy.rect!.origin.y  / (page?.calibratedPageHeight(pageWidth) ?? 1)) * 100
                        mutableCopy.width         =   (mutableCopy.rect!.size.width  / pageWidth) * 100;
                        mutableCopy.height        =   (mutableCopy.rect!.size.height / (page?.calibratedPageHeight(pageWidth) ?? 1)) * 100
                        
                        subField["sub_field_name"] = mutableCopy.name
                        
                        subField["x_value"] = String(format: "%.5f", mutableCopy.xValue)
                        subField["y_value"] = String(format: "%.5f", mutableCopy.yValue)
                        subField["width"] = String(format: "%.5f", mutableCopy.width)
                        subField["height"] = String(format: "%.5f", mutableCopy.height)
                        subField["page_no"] = String(format: "%d", signfield.pageIndex)
                        subField["default_value"] = mutableCopy.defaultValue
                        
                        mutableCopy.subFieldId.safelyUnwrap{ subField["sub_field_id"] = $0 }
                        
                        subFieldsArr.append(subField)
                        return mutableCopy
                    }
                    fieldDict.removeValue(forKey: "x_value")
                    fieldDict.removeValue(forKey: "y_value")
                    fieldDict.removeValue(forKey: "width")
                    fieldDict.removeValue(forKey: "height")
                    fieldDict["sub_fields"] = subFieldsArr
                    radioGroupArr.append(fieldDict)
                }
                else
                {
                    let page = pagesArray.first
                    let textPropDict : [String : String] =
                        ["font":signfield.currentTextProperty.fontFamily,
                         "font_color":ZColor.hexString(fromColor: signfield.currentTextProperty.fontColor),
                         "font_size": String(signfield.serverFontSize),
                         "is_bold":(signfield.currentTextProperty.isBold) ? "true" :"false",
                         "is_italic":(signfield.currentTextProperty.isItalics) ? "true" :"false",
                         "is_underline":(signfield.currentTextProperty.isUnderline) ? "true" :"false",
                         "is_read_only":(signfield.isReadOnly) ? "true" :"false"]
                    
                    if (signfield.isReadOnly) {
                        fieldDict["default_value"] = signfield.fieldName
                    }
                    fieldDict["text_property"] = textPropDict
                    
                    if ((signfield.type == .signDate) || (signfield.type == .date)) {
                        fieldDict["date_format"] = (signfield.dateFormat == nil) ? UserManager.shared.currentUser?.dateFormat : signfield.dateFormat
                        
                        dateArr.append(fieldDict)
                    }
                    else if (signfield.type == .dropDown) {
                        fieldDict["dropdown_values"] = signfield.dropdownFields
                        
                        if (signfield.defaultValue != nil){
                            fieldDict["default_value"] = signfield.defaultValue
                        }
                        
                        if (signfield.isReadOnly && (signfield.defaultValue == nil)) {
                            delegate?.showAlert(withMessage: String(format: "sign.mobile.fields.provideDefaultValueForDropdownField".ZSLString, signfield.fieldName ?? ""))
                            return nil
                        }
                        
                        dropDownArr.append(fieldDict)
                    }
                    else{
                        if (signfield.type == .name) {
                            fieldDict["name_format"] = signfield.nameFormat()
                        }
                        textArr.append(fieldDict)
                    }
                }
            }
            
            tempDict["fields"] = ["text_fields" : textArr,
                                  "image_fields" : imageArr,
                                  "check_boxes" : checkboxArr,
                                  "date_fields" : dateArr,
                                  "dropdown_fields" : dropDownArr,
                                  "file_fields" : attachmentsArr,
                                  "radio_groups" : radioGroupArr]
            tempDict["deleted_fields"] = deletedFieldIdsArray
            
            if (recipient.isTemplateUser) {
                let doc = refTemplate?.documents?.first
                tempDict.removeValue(forKey: "verify_recipient")
                tempDict.removeValue(forKey: "signing_order")
                tempDict.removeValue(forKey: "action_type")
                tempDict["document_id"]     =  doc?.documentId
                documentsFieldsArray.append(tempDict)
            }else{
                actionArr.append(tempDict)
            }
            
        }
        if(refRequest?.isSelfSign?.intValue == 1){
            requestDict["self_sign"] = "true"
        }
        requestDict["actions"] = actionArr
        
        if(refTemplate != nil){
            requestDict["document_fields"] = documentsFieldsArray
        }
        return requestDict
    }
    
    func showFinalPreview()  {
        contentViews.forEach { (key, contentView) in
            contentView.isFinalPreviewMode = true
        }
    }
    
    //MARK: - End Editing
    
    func endEditing()  {
        ZSPopTip.dismissPoptip()
    
        contentViews.forEach { (key, value) in
            value.endEditing()
        }
    }
    
    func cancelBtnTapped()  {
        
        endEditing()
        
        if !isSigningCompleted && isThirdPartySignSDK {
            
//            ZSignKit.shared.zSignFailureHandler()
        }
    }
    
    func cancelAllTasks() {
        endEditing()
        requestDataManager.cancelAllTasks()
        templateDataManager.cancelAllTasks()
        fileDatamanager.cancelAllTasks()

        endEditing()
        if !isThirdPartySignSDK {
            documentDownloader?.cancelDownload()
        }

    }
    
    
}



//MARK: - Editor base Delegate
extension EditorDataParser : DocumentEditorBaseViewDelegate{
    
    
    func editorBaseViewFieldAdded(editorView: DocumentEditorBaseView, field: SignField) {
        if let index = fieldsListArray.lastIndex(of: field){
            fieldsListArray.remove(at: index)
        }
        fieldsListArray.append(field)
        delegate?.editorDataParserFieldsUpdated(fieldsListArray)
    }
    
    func editorBaseViewFieldDeleted(editorView: DocumentEditorBaseView?, field: SignField) {
        isFieldsEdited  =   true;

        if let index = fieldsListArray.lastIndex(of: field){
            fieldsListArray.remove(at: index)
        }
        if let fieldId = field.fieldId {
            deletedFieldIdsArray.append(fieldId)
        }
        delegate?.editorDataParserFieldsUpdated(fieldsListArray)
    }
    
    func editorBaseViewFieldDetailsUpdated(editorView: DocumentEditorBaseView, field: SignField)
    {
        if let index = fieldsListArray.lastIndex(of: field){
            fieldsListArray[index] = field
        }
        delegate?.editorDataParserFieldsUpdated(fieldsListArray)
    }
        
    
    
    func editorBaseViewSignatureButtonTapped(editorView: DocumentEditorBaseView, signView: ZResizeView, type: ZSignFieldSignatureType) {
        
    }
    
  
    func editorBaseViewBeginEditing(editorView: DocumentEditorBaseView, field: SignField) {
        isFieldsEdited  =   true;
        delegate?.navigationItem.rightBarButtonItem?.isEnabled  =  (fieldsListArray.count  > 0)
    }
    
    
    public func editorBaseViewFieldValueChanged(editorView: DocumentEditorBaseView, field: SignField) {
        
    }
    
    func editorBaseViewAttachmentFieldTapped(editorView: DocumentEditorBaseView, signView: ZResizeView) {
        
    }
    
}
