//
//  EraserTool.swift
//  SignatureView
//
//  Created by somesh-8758 on 13/02/20.
//  Copyright © 2020 Somesh-8758. All rights reserved.
//

import UIKit

class EraserTool: SketchTool {
    
    override func draw() {
        guard let context = UIGraphicsGetCurrentContext() else {return}
        context.setShouldAntialias(false)
        context.saveGState()
        context.addPath(path)
        context.setLineCap(.round)
        context.setLineWidth(_lineWidth)
        context.setBlendMode(.clear)
        context.strokePath()
        context.restoreGState()
    }
    
    func addPath(previousPreviousPoint p2Point: CGPoint,previousPoint p1Point: CGPoint,currentPoint: CGPoint) -> CGRect {
        let mid1 = p1Point / p2Point
        let mid2 = currentPoint / p1Point
        
        let subpath = CGMutablePath()
        subpath.move(to: mid1)
        subpath.addQuadCurve(to: mid2, control: p1Point)
        let bounds = subpath.boundingBox
        
        subpath.addPath(path)
        return bounds
    }
    
}
