//
//  ZSSingleSelectionContollerTableViewController.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 12/12/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

class ZSSingleSelectionContoller: UITableViewController {

    var optionsArray : [String] = []
    var selectedValue  : String = ""
    public var selectedValueDidChange : ((Int) -> Void)?

    private var selectedIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.rowHeight = TableviewCellSize.normal
        view.backgroundColor = ZColor.bgColorGray
        tableView.backgroundColor = ZColor.bgColorGray

        selectedIndex = optionsArray.firstIndex(of: selectedValue) ?? 0
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return optionsArray.count
    }

    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        
        cell.textLabel?.text        =   optionsArray[indexPath.row]
        cell.textLabel?.font        =   ZSFont.regularFontLarge()
        
        if indexPath.row == selectedIndex {
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        //Selected bgview
        let selectedView = UIView()
        selectedView.backgroundColor = ZColor.listSelectionColor;
        cell.selectedBackgroundView = selectedView;
        
        cell.isUserInteractionEnabled = !(cell.textLabel?.text?.isEmpty ?? false)
        cell.alpha = cell.isUserInteractionEnabled ? 1 : 0.5
        cell.backgroundColor = ZColor.bgColorWhite
        return cell
    }
  
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedValue = optionsArray[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        
        let oldIndex  = NSIndexPath(row: selectedIndex, section: 0)
        if let oldCell = tableView.cellForRow(at: oldIndex as IndexPath){
            oldCell.accessoryType = .none
        }
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
        selectedIndex = indexPath.row
        
        selectedValueDidChange?(indexPath.row)
        navigationController?.popViewController(animated: true)
    }
    
    deinit {
        ZInfo("deinit")
    }
}
