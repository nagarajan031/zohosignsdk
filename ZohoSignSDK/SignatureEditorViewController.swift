//
//  SignatureEditorViewController.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 29/11/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit
import SnapKit

class SignatureEditorViewController: UIViewController {
    
    private lazy var cropview: CropPickerView = {
        let view = CropPickerView()
        view.delegate = self
        view.image = image
        view.scrollMinimumZoomScale = 1
        view.scrollMaximumZoomScale = 5
        view.isCrop = false
        view.backgroundColor = .clear
        view.scrollBackgroundColor = .clear
        view.imageBackgroundColor = .clear
        return view
    }()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private let image: UIImage
    private var signatureFiltersView: SignatureFiltersView!
    private var signatureEditorControlView: SignatureEditorControlsView!
    
    private var filtersViewLeadingContraint: Constraint?
    private var filtersViewTopConstraint: Constraint?
    private var cropviewTrailingConstraint: Constraint?
    private var cropViewBottomContraint: Constraint?
    
    public weak var delegate: SignatureCreatorDelegate?
    public var isModalyPresented: Bool = false
    private var signatureEditorControlViewSize = CGSize(width: 50, height: 50 * 3 + 36)
    
    init(image: UIImage) {
        self.image = image
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ZColor.blackColor
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: isModalyPresented ? "sign.mobile.common.cancel".ZSLString : "sign.mobile.common.retake".ZSLString, style: .plain, target: self, action: #selector(retakeClicked))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneClicked))
        
        if !isModalyPresented {
            navigationController?.navigationBar.tintColor = ZColor.whiteColor
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationController?.navigationBar.shadowImage = UIImage()
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationBar.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        }
        
        signatureFiltersView = SignatureFiltersView(withOriginalImage: image, delegate: self)
        signatureFiltersView.alpha = 0
        signatureEditorControlView = SignatureEditorControlsView(delegate: self)
        
        view.addSubviews(cropview,signatureFiltersView,signatureEditorControlView)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        cropview.snp.removeConstraints()
        signatureEditorControlView.snp.removeConstraints()
        signatureFiltersView.snp.removeConstraints()
        
        cropview.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.topMargin)
            
            if DeviceType.isMac {
                make.trailing.equalToSuperview()
                make.leading.equalTo(view)
                cropViewBottomContraint = make.bottom.equalTo(signatureEditorControlView.snp.top).offset(-20).constraint
            } else {
                make.leading.equalTo(view.snp.leadingMargin)
                make.bottom.equalToSuperview()
                cropviewTrailingConstraint = make.trailing.equalTo(signatureEditorControlView.snp.leading).offset(-20).constraint
            }
        }
        
        signatureEditorControlView.snp.makeConstraints { (make) in
            if DeviceType.isMac {
                make.centerX.width.bottom.equalToSuperview()
                make.height.equalTo(signatureEditorControlViewSize.width + 22)
            } else {
                make.trailing.height.equalToSuperview()
                make.centerY.equalToSuperview().offset((navigationController?.navigationBar.frame.height ?? 0)/2)
                make.width.equalTo(signatureEditorControlViewSize.width + 22)
            }
        }
        
        signatureFiltersView.snp.makeConstraints { (make) in
            if DeviceType.isMac {
                make.centerX.equalToSuperview()
                filtersViewTopConstraint = make.top.equalTo(signatureEditorControlView).constraint
                make.height.equalTo(200)
                make.width.equalTo(600)
            } else {
                make.centerY.equalTo(signatureEditorControlView)
                filtersViewLeadingContraint = make.leading.equalTo(signatureEditorControlView).constraint
                make.width.equalTo(100)
                make.height.equalToSuperview().offset(-((navigationController?.navigationBar.frame.height ?? 0)/2)).multipliedBy(0.8)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true && !isModalyPresented, animated: false)
    }
    
    @objc func retakeClicked(){
        if isModalyPresented {
            dismiss(animated: true, completion: nil)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func doneClicked(){
        cropview.crop()
    }

}


extension SignatureEditorViewController: CropPickerViewDelegate {
    func cropPickerView(_ cropPickerView: CropPickerView, image: UIImage) {
        self.delegate?.signatureCreated(signImage: image)
        dismiss(animated: true)
    }
    
    func cropPickerView(_ cropPickerView: CropPickerView, error: Error) {
        showAlert(withMessage: error.localizedDescription)
    }
}

extension SignatureEditorViewController: SignatureFiltersViewDelegate {
    func imageDidSelected(_ image: UIImage,withFilteredType type: SignatureFilters) {
        cropview.imageBackgroundColor = type == .backgroundRemoved ? UIColor(patternImage: UIImage(named: "empty_background")!) : .clear
        cropview.changeImage = image.rotate(radians: Float(signatureEditorControlView.rotationAngle))
    }
}

extension SignatureEditorViewController: SignatureEditorControlsViewDelegate {
    func signatureEditorControlDidClicked(_ signaturEditorControl: SignatureEditorControls) {
        switch signaturEditorControl {
        case let .crop(shouldShow):
            showOrHideFiltersView(false)
            cropview.isCrop = shouldShow
            signatureEditorControlView.setBackground(isBackgroundColorNeeded: true)
        case let .filter(shouldShow):
            cropview.isCrop = false
            showOrHideFiltersView(shouldShow)
            signatureEditorControlView.setBackground(isBackgroundColorNeeded: true)
        case let .rotate(angle):
            let imageToRotate = cropview.image
            let rotatedImage = imageToRotate?.rotate(radians: Float(angle))
            cropview.changeImage = rotatedImage
            break
        default:
            break
        }
    }
    
    func showOrHideFiltersView(_ shouldShow: Bool){
        if DeviceType.isMac {
            cropViewBottomContraint?.update(offset: shouldShow ? -230: 0)
            filtersViewTopConstraint?.update(offset: shouldShow ? -210 : 0)
        } else {
            filtersViewLeadingContraint?.update(offset: shouldShow ? -110 : 0)
            cropviewTrailingConstraint?.update(offset: shouldShow ? -130 : 0)
        }
        
        UIView.animate(withDuration: 0.3) {
            self.signatureFiltersView.alpha = shouldShow.toNumber()
            self.view.layoutIfNeeded()
        }
    }
}

public extension Bool {
    func toNumber() -> CGFloat{
        return self ? 1 : 0
    }
}

