//
//  CGRect+.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 29/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import Foundation

public extension CGRect {
    func overLaps(with otherViewFrame: CGRect) -> Bool {
        
        let isBottomLeftOverLaping = otherViewFrame.maxY > minY && otherViewFrame.minX < maxX && minX < otherViewFrame.minX && maxY > otherViewFrame.maxY
        
        let isTopLeftOverLaping = otherViewFrame.minX < maxX && otherViewFrame.minY < maxY && otherViewFrame.minX > minX && otherViewFrame.minY > minY
        
        let isTopRightOverLaping = otherViewFrame.minY < maxY && minX < otherViewFrame.maxX && maxX > otherViewFrame.maxX && minY < otherViewFrame.minY
        
        let isBottomRightOverLaping = otherViewFrame.maxY > minY && minX < otherViewFrame.maxX && otherViewFrame.maxX < maxX && otherViewFrame.maxY < maxY
        
        let isEqual = equalTo(otherViewFrame)
        
        return isTopLeftOverLaping || isBottomLeftOverLaping || isTopRightOverLaping || isBottomRightOverLaping || isEqual || intersects(otherViewFrame)
    }
    
    func isValidFrame(restrictedToBounds bounds: CGRect) -> Bool {
        if maxX > bounds.maxX {
            return false
        } else if maxY > bounds.maxY {
            return false
        } else if minX < bounds.minX {
            return false
        } else if minY < bounds.minY {
            return false
        }
        return true
    }
    
    func overLaps(with otherViewFrame: CGRect) -> OverLappingSide {
        if otherViewFrame.minX.rounded() == minX.rounded() && otherViewFrame.maxX.rounded() == maxX.rounded() && otherViewFrame.maxY > minY && otherViewFrame.maxY < maxY{
            return .bottom
        }
        
        if otherViewFrame.minY.rounded() == minY.rounded() && otherViewFrame.maxY.rounded() == maxY.rounded() && otherViewFrame.minX < maxX && otherViewFrame.maxX > maxX{
            return .left
        }
        
        if otherViewFrame.minY < maxY && otherViewFrame.minX.rounded() == minX.rounded() && otherViewFrame.maxX.rounded() == maxX.rounded() && otherViewFrame.minY > minY{
            return .top
        }
        
        if otherViewFrame.minY.rounded() == minY.rounded() && otherViewFrame.maxY.rounded() == maxY.rounded() && otherViewFrame.maxX > minX && otherViewFrame.maxX < maxX{
            return .right
        }
        
        if otherViewFrame.maxY > minY && otherViewFrame.minX < maxX && minX < otherViewFrame.minX && maxY > otherViewFrame.maxY {
            return .bottomLeft
        }
        
        if otherViewFrame.minX < maxX && otherViewFrame.minY < maxY && otherViewFrame.minX > minX && otherViewFrame.minY > minY {
            return .topLeft
        }
        
        if otherViewFrame.minY < maxY && minX < otherViewFrame.maxX && maxX > otherViewFrame.maxX && minY < otherViewFrame.minY{
            return .topRight
        }
        
        if otherViewFrame.maxY > minY && minX < otherViewFrame.maxX && otherViewFrame.maxX < maxX && otherViewFrame.maxY < maxY{
            return .bottomRight
        }
    
        if otherViewFrame == self {
            return .all
        }
        
        return .none
    }
    
    func getPossibleSides(increamentedbySize size: CGFloat ,restrictedToBounds bounds: CGRect) -> [OverLappingSide] {
        var possibleSides: [OverLappingSide] = []
        if maxY + size <= bounds.maxY && maxX <= bounds.maxX{
            possibleSides.append(.bottom)
        }
        
        if maxX + size <= bounds.maxX && maxY <= bounds.maxY{
            possibleSides.append(.right)
            if minY - size >= bounds.minY {
                possibleSides.append(.topRight)
            }
            if maxX <= bounds.maxX {
                possibleSides.append(.bottomRight)
            }
        }
        
        if minY - size >= bounds.minY && minX >= bounds.minX{
            possibleSides.append(.top)
        }
        
        if minX - size >= bounds.minX && minY >= bounds.minY{
            if minY - size >= bounds.minY {
                possibleSides.append(.topLeft)
            }
            possibleSides.append(.left)
            if maxY + size <= bounds.maxY {
                possibleSides.append(.bottomLeft)
            }
        }
        
        return possibleSides
    }
    
    func getNearestPossibleFrame(onSide side: OverLappingSide, forSize size: CGFloat, restrictedToBounds bounds: CGRect) -> CGRect {
        var newFrame = self
        
        let possibleSides = getPossibleSides(increamentedbySize: size, restrictedToBounds: bounds)

        switch side {
        case .all,.bottom:
            if possibleSides.contains(.bottom) {
                newFrame.origin.y += size
                return newFrame
            }
        case .top:
            if possibleSides.contains(.top) {
                newFrame.origin.y -= size
                return newFrame
            }
        case .left:
            if possibleSides.contains(.left) {
                newFrame.origin.x -= size
                return newFrame
            }
        case .right:
            if possibleSides.contains(.right) {
                newFrame.origin.x += size
                return newFrame
            }
        case .bottomRight:
            if possibleSides.contains(.bottomRight) {
                newFrame.origin.y += size
                newFrame.origin.x += size
                return newFrame
            }
        case .bottomLeft:
            if possibleSides.contains(.bottomLeft) {
                newFrame.origin.y += size
                newFrame.origin.x -= size
                return newFrame
            }
        case .topRight:
            if possibleSides.contains(.topRight) {
                newFrame.origin.y -= size
                newFrame.origin.x += size
                return newFrame
            }
        case .topLeft:
            if possibleSides.contains(.topLeft) {
                newFrame.origin.y -= size
                newFrame.origin.x -= size
                return newFrame
            }
        default:
            return self
        }
        return getNearestPossibleFrame(onSide: possibleSides.first!,forSize: size, restrictedToBounds: bounds)
    }
}
