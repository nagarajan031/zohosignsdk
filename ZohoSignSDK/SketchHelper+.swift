//
//  SketchHelper+.swift
//  SignatureView
//
//  Created by somesh-8758 on 13/02/20.
//  Copyright © 2020 Somesh-8758. All rights reserved.
//

import CoreGraphics

infix operator ~>

extension CGPoint {
    static func / (lhs: Self, rhs: Self) -> Self {
        return .init(x: (lhs.x + rhs.x) / 2, y: (lhs.y + rhs.y) / 2)
    }

    static func + (lhs: Self, rhs: Self) -> Self {
        return .init(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
    }
    
    static func + (lhs: Self, rhs: CGFloat) -> Self {
        return .init(x: lhs.x+rhs, y: lhs.y+rhs)
    }
    
    static func - (lhs: Self, rhs: Self) -> Self {
        return .init(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
    }
    
    static func - (lhs: Self, rhs: CGFloat) -> Self {
        return .init(x: lhs.x-rhs, y: lhs.y-rhs)
    }
    
    static func * (lhs: Self, rhs: CGFloat) -> Self {
        return .init(x: lhs.x*rhs, y: lhs.y*rhs)
    }
    
    static func ~> (lhs: Self, rhs: Self) -> CGFloat {
        let dif = lhs - rhs
        return sqrt(dif.x*dif.x + dif.y*dif.y)
    }
    
    func fuzzyEqual(with point: Self,by distance: CGFloat) -> Bool{
        return (x-distance <= point.x) &&
            (point.x <= x+distance) &&
            (y+distance <= point.y) &&
            (point.y <= y+distance)
    }
}

extension CGFloat {
    func rad() -> Self {
        return (self * CGFloat.pi) / 180
    }
    
    func degrees() -> Self {
        return self * (180 / CGFloat.pi)
    }
}
