//
//  ZTableHeaderView.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 18/09/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public enum TableHeaderStyle {
    case plain
    case largePlain
    case grouped
}

public let   tableGroupedHeaderHeight : CGFloat    =      40
public let   tablePlainHeaderHeight  : CGFloat     =     36
public let   tablePlainHeaderHeightLarge  : CGFloat     =     60


public class ZTableHeaderView: UIView {

    var style : TableHeaderStyle  = .plain
    public let titleLabel = UILabel()
    public let bottomBorder = UIView()
    public let topBorder    = UIView()
    
    public convenience init(style : TableHeaderStyle) {
        self.init()
        self.style = style
        initateSetup()
    }
    
    func initateSetup(){
        
        autoresizingMask    =   .flexibleWidth
        backgroundColor     =   ZColor.bgColorGray
        
        titleLabel.autoresizingMask =   .flexibleWidth
        addSubview(titleLabel)
        
        topBorder.frame =   CGRect(x: 0, y: 0, width: frame.width, height: 0.4)
        topBorder.autoresizingMask = .flexibleWidth
        topBorder.backgroundColor = ZColor.seperatorColor
        addSubview(topBorder)
        
        bottomBorder.frame =   CGRect(x: 0, y: frame.height - 0.3, width: frame.width, height: 0.4)
        bottomBorder.autoresizingMask = [.flexibleTopMargin,.flexibleWidth]
        bottomBorder.backgroundColor = ZColor.seperatorColor
        addSubview(bottomBorder)
        
        setHeaderStyle(style)
    }
    
     func setHeaderStyle(_ style : TableHeaderStyle)  {
        self.style = style
        switch style {
        case .plain:
            backgroundColor = ZColor.tableHeaderBgColor
            frame   =   CGRect(x: 0, y: 0, width: 320, height: tablePlainHeaderHeight)
            titleLabel.font =   ZSFont.headlineFont()
            titleLabel.textColor =  ZColor.tableHeaderTextColor
            if #available(iOS 13, *) {
                titleLabel.textColor =  .setAppearance(dark: UIColor(white: 1, alpha: 0.8), light: UIColor(red:0.36, green:0.36, blue:0.36, alpha:0.8))
            }

            titleLabel.frame    =   CGRect(x: cellValueLabelX, y: 0, width: frame.width - 32, height: tablePlainHeaderHeight)
            topBorder.isHidden  =   false
            bottomBorder.isHidden  =   false
            titleLabel.text =   titleLabel.text?.capitalizedFirst()
            break
        case .largePlain:
            frame   =   CGRect(x: 0, y: 0, width: 320, height: tablePlainHeaderHeightLarge)
            titleLabel.font =   ZSFont.tableSectionHeaderFont()
            titleLabel.textColor = ZColor.tableHeaderTextColor
            titleLabel.frame    =   CGRect(x: cellValueLabelX, y: 30, width: frame.width - 32, height: tablePlainHeaderHeightLarge - 30)
            topBorder.isHidden  =   false
            bottomBorder.isHidden  =   false
            titleLabel.text =   titleLabel.text?.uppercased()
            break
        case .grouped:
            frame   =   CGRect(x: 0, y: 0, width: 320, height: tableGroupedHeaderHeight)
            titleLabel.font =   ZSFont.titleFont()
            titleLabel.textColor = ZColor.primaryTextColor
            titleLabel.frame    =   CGRect(x: cellValueLabelX, y: 20, width: frame.width - 32, height: tableGroupedHeaderHeight - 6)
            topBorder.isHidden  =   true
            bottomBorder.isHidden  =   true
            titleLabel.text =   titleLabel.text?.uppercased()
            backgroundColor = ZColor.bgColorWhite
            break
       
        }
    }
    
    public func setTitle(_ title: String)  {
        if (style == .grouped || style == .plain ){
//            let attributedString = NSMutableAttributedString(string: title)
            titleLabel.text = title
        }else{
            titleLabel.text = title.uppercased()
        }
    }
    
    public func setTitleContentOffset(_ offset  : CGPoint)
    {
        titleLabel.frame = CGRect(x: offset.x, y: offset.y, width: frame.width - offset.x - 10, height: frame.size.height - offset.y)
    }
    
//    -(void) showBottomBorder:(BOOL) show
//    {
//    bottomBorderLabel.hidden    =   !show;
//    }
//
//    -(void) showTopBorder:(BOOL) show
//    {
//    topBorderLabel.hidden    =   !show;
//
//    }

}
