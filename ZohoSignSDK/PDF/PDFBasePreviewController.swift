//
//  PDFBasePreviewController.swift
//  ZohoSignSDK
//
//  Created by sudhan-6859 on 24/09/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import Foundation
import PDFKit
import SnapKit
import Combine

public let kPDFThumbnailWidth : CGFloat = 170

open class PDFBasePreviewController : UIViewController {
    
    var isScrollDisable = false
    
    public var shouldEnableTouchBar: Bool = true
    
    public let shareDocumentManager = ShareDocumentManager()

    public var hideToolbarView : Bool = false
    
    private lazy var layout : UICollectionViewFlowLayout = {
        return SwiftUtils.pdfThumbanailLayout(showHeader: true)
    }()
    
    public lazy var pdfView : PDFView = {
        let pdfView = PDFView()
        pdfView.backgroundColor = ZColor.bgColorGray
        pdfView.pageBreakMargins = UIEdgeInsets(top: cellValueLabelY, left: 1, bottom: 0, right: 1)
        pdfView.clipsToBounds = true
        return pdfView
    }()
    
    var annotations : [PDFAnnotation] = []

    public lazy var thumbnailView : ZSPDFThumbnailView = {
        let thumbnailView = ZSPDFThumbnailView(frame: CGRect.zero, collectionViewLayout: layout)
        thumbnailView.translatesAutoresizingMaskIntoConstraints = false
        thumbnailView.backgroundColor   =   ZColor.bgColorGray;
        thumbnailView.clipsToBounds    =   true;
        thumbnailView.delegate         = self
        thumbnailView.thumbnailDelegate = self
        return thumbnailView
    }()
    
    private lazy var toolBarView : ZSPDFDocsListToolbar = {
        let toolBar = ZSPDFDocsListToolbar(documents: pdfDocuments)
        toolBar.delegate = self
        return toolBar
    }()

    private var currentIndex :  IndexPath = IndexPath(item: 0, section: 0){
        willSet{
//            thumbnailView.selectedIndexPath = newValue
//            if let cell = thumbnailView.cellForItem(at: currentIndex),let newCell = thumbnailView.cellForItem(at: newValue){
//                thumbnailView.reloadItems(at: [currentIndex,newValue])
//                cell.isSelected = false
//                newCell.isSelected = true
//            }
        }
        didSet{
            toolBarView.changeDocumentIndex(currentIndex.section+1)
//            if(currentIndex.section != oldValue.section){
//            toolBarView.updateCurrentPage(currentIndex.row+1, noOfPages: pdfDocuments[currentIndex.section].pageCount)
//            }else{
//                toolBarView.updateCurrentPage(currentIndex.row+1)
//            }
        }
    }
    
    private var orgPageIndex : Int = 1
    public var isHideThumbnailView = false

    private var widthConstraint : Constraint?
    public var zsPdfView: ZSPDFView!
    
    public var fileURLPath = [String]() {
        didSet{
            annotations.removeAll()
            pdfDocuments.removeAll()
            for i in fileURLPath{
                let url =  URL(fileURLWithPath: i)
                if let doc = ZSPDFDocument(url: url){
                    doc.title = FileManager.default.displayName(atPath: i)
                    pdfDocuments.append(doc)
                    
                    
                    for i in 0..<doc.pageCount{
                        let page = doc.page(at: i)
                        if let annots = page?.annotations{
                            annotations.append(contentsOf: annots)
                        }
                    }
                }
            }
            
            if pdfDocuments.count == 0{return}
            if DeviceType.isMac {
                let images = convertPDFToImages(documents: pdfDocuments)
                zsPdfView = ZSPDFView(images: images)
                view.addSubview(zsPdfView)
                zsPdfView.snp.makeConstraints { (make) in
                    make.right.top.bottom.equalToSuperview()
                    make.left.equalTo(isHideThumbnailView ? 0 : kPDFThumbnailWidth)
                }
                
                #if targetEnvironment(macCatalyst)
                NotificationCenter.Publisher(center: .default, name: .pdfViewCellChanged)
                    .receive(on: DispatchQueue.main)
                    .compactMap({$0.object as? IndexPath})
                    .sink(receiveValue: {[weak self] in
                        self?.currentIndex = $0
                        self?.thumbnailView.goToPage(indexPath: $0, animated: true)
                    })
                #endif

            }else{
                pdfView.document = pdfDocuments[currentIndex.section]
            }
            toolBarView.documents = pdfDocuments
            toolBarView.changeDocumentIndex(1)
            parseDocumentPagesForRequest()
        }
    }
    

    
    public var pdfDocuments = [ZSPDFDocument]()
    
    //MARK:- View Cycle
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(thumbnailView)
        
        thumbnailView.frame = CGRect(x: 0, y: 0, width: kPDFThumbnailWidth, height: self.view.frame.height)
        
//        thumbnailView.snp.makeConstraints { (make) in
//            make.top.equalToSuperview()
//            make.height.equalToSuperview()
//            make.left.equalToSuperview()
//            widthConstraint = make.width.equalTo(kPDFThumbnailWidth).constraint
//        }
        
        self.view.backgroundColor =  ZColor.bgColorGray

        self.view.addSubview(pdfView)
        
        pdfView.clipsToBounds = false
        pdfView.delegate = self

        pdfView.frame = CGRect(x: kPDFThumbnailWidth, y: 0, width: self.view.frame.width - kPDFThumbnailWidth, height: self.view.frame.height)
//        pdfView.alpha = 0

        

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandle))
        pdfView.addGestureRecognizer(tapGesture)
//        ZSProgressHUD.shared.showHUD(inView: self.view)
        
        if isHideThumbnailView{
            hideThumbnailView()
        }
        
        showToolbarView()
        
       
    }
    
    override open func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        adjustPdfViewScaleFactor()
    }
    
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(handlePageChange(notification:)), name: Notification.Name.PDFViewPageChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleDocumentChange(notification:)), name: NSNotification.Name.PDFViewDocumentChanged, object: nil)
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !DeviceType.isMac {
            pdfView.goToPage(index: 0)
        }
    }
    
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.PDFViewPageChanged, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.PDFViewDocumentChanged, object: nil)
    }
    
    
    //MARK: - Local methods
    func convertPDFToImages(documents:[ZSPDFDocument]) -> [ZSPDF] {
        var images: [ZSPDF] = []
        
        documents.forEach { (pdfdocument) in
            var imagesArray: [UIImage] = []
            for index in 0...(pdfdocument.pageCount-1){
                let page = pdfdocument.page(at: index)
                let pageRect = page?.bounds(for: .artBox)
                let pdfImgSize  = CGSize(width: pageRect!.size.width * 2, height: pageRect!.size.height * 2)

                if var image = page?.thumbnail(of: pdfImgSize, for: .artBox){
                   /* if let rotation = page?.rotation, rotation > 0, SwiftUtils.isIOS13AndAbove, !DeviceType.isMac {
                        image = image.rotate(radians: (Float(rotation) * (.pi / 180)))
                    }*/
                    imagesArray.append(image)
                }
            }
            let zsPDF = ZSPDF(title: pdfdocument.title, images: imagesArray)
            images.append(zsPDF)
        }
        
        return images
    }

    @objc private func tapGestureHandle(){
        
        if self.toolBarView.isHidden {
            UIView.animate(withDuration: 0.3, animations: {
                self.toolBarView.alpha = 1
            }) { (finish) in
                self.toolBarView.isHidden = false
            }
        }else{
            UIView.animate(withDuration: 0.3, animations: {
                self.toolBarView.alpha = 0
            }) { (finish) in
                self.toolBarView.isHidden = true
            }
        }
    }
    
    @objc public func hideThumbnailView() {
        adjustPdfViewScaleFactor()
        thumbnailView.isHidden = true
    }
    
    @objc public func showThumbnailView() {
        adjustPdfViewScaleFactor()
        thumbnailView.isHidden = false
    }
    
    @objc public func showToolbarView(){
        if !DeviceType.isIpadOnly() || hideToolbarView{
            return
        }
        
        self.view.addSubview(toolBarView)

        toolBarView.snp.makeConstraints { (make) in
            make.height.equalTo(44)
            make.width.equalTo(240)
            make.centerX.equalTo(pdfView)
            make.bottom.equalTo(view).offset(-20)
        }
        toolBarView.alpha = 0
    }
    
    func adjustPdfViewScaleFactor()  {
//        return
     
       
       if ((self.view.frame.width <= 600) ||  isPortrait()  || UIApplication.shared.isSplitOrSlideOver || isHideThumbnailView){
//            let widthMargin : CGFloat = toolBarView.isRightDetailViewHidden ? 20 : editorFieldSelectionViewWidth;
//            self.bgScrollView.frame  = CGRect(x: 20, y: 0, width: self.view.frame.width - widthMargin - 20, height: self.view.frame.height)
        
            self.thumbnailView.frame = CGRect(x: -kPDFThumbnailWidth, y: 0, width: kPDFThumbnailWidth, height: self.view.frame.height)
//            self.pdfView.frame = CGRect(x: 20, y: 0, width: self.view.frame.width - 40, height: self.view.frame.height)
            self.pdfView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width , height: self.view.frame.height)

//            let hideResizeBtn  = (isPortrait() || UIApplication.shared.isSplitOrSlideOver)
            self.toolBarView.isHidden = false
        }
        else{
            self.thumbnailView.frame = CGRect(x: 0, y: 0, width: kPDFThumbnailWidth, height: self.view.frame.height)
            self.pdfView.frame = CGRect(x: kPDFThumbnailWidth, y: 0, width: self.view.frame.width - kPDFThumbnailWidth, height: self.view.frame.height)

//            let widthMargin : CGFloat = toolbarView.isRightDetailViewHidden ? editorThumbnailViewWidth : (editorThumbnailViewWidth + editorFieldSelectionViewWidth);
//            self.bgScrollView.frame  = CGRect(x: editorThumbnailViewWidth, y: 0, width: self.view.frame.width - widthMargin - 20, height: self.view.frame.height)
            self.toolBarView.isHidden = false
//            showThumbnailView()
        }
        
        var minScale : CGFloat = 1
      
//        if toolBarView.isMaximimumSizeSelected{
//            minScale = pdfView.scaleFactorForSizeToFit
//        }
//        else
            if (pdfView.scaleFactorForSizeToFit < 1) {
            minScale = pdfView.scaleFactorForSizeToFit
        }
    
        pdfView.minScaleFactor = minScale
        pdfView.scaleFactor = minScale
        pdfView.maxScaleFactor = 3
        
    }
    
  
    func parseDocumentPagesForRequest()  {
        DispatchQueue.main.async {
            self.thumbnailView.documentsArray = self.pdfDocuments;
            self.thumbnailView.reloadData()
            if self.pdfDocuments.count > 0{
                self.thumbnailView.goToPage(indexPath: IndexPath(row: 0, section: 0), animated: false)
            }
            self.toolBarView.alpha = 1
        }
    }
    

    @objc func pdfViewDidChangePage(){
        pdfView.clearSelection()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.PDFViewPageChanged, object: nil)
    }
    
   
    
    @objc private func handlePageChange(notification: Notification)
    {
        if isScrollDisable {
            return;
        }
        currentIndex = IndexPath(row: pdfView.currentPageIndex() ?? 0, section: currentIndex.section)
        thumbnailView.goToPage(indexPath: currentIndex, animated: true)
    }

    @objc private func handleDocumentChange(notification: Notification){
        if let index = pdfDocuments.enumerated().first(where: {$0.element == pdfView.document}){
            currentIndex.section = index.offset
            currentIndex.row = 0
            thumbnailView.goToPage(indexPath: currentIndex, animated: true)
        }
    }
    
    @objc public func showNextDocument(){
        if pdfDocuments.count  > (currentIndex.section+1){
            pdfView.document = pdfDocuments[currentIndex.section+1]
            adjustPdfViewScaleFactor()
        }
       
    }
    
    @objc public func showPrevDocument(){
        if currentIndex.section-1 >= 0{
            pdfView.document = pdfDocuments[currentIndex.section-1]
            adjustPdfViewScaleFactor()
        }
        
    }
    
}

extension PDFBasePreviewController : ZSPDFThumbnailViewDelegate{
    public func pdfThumbnailViewDidSelectHeader(section: Int) {
        if(currentIndex.section != section){
            if DeviceType.isMac {
                zsPdfView.moveToPage(indexPath: IndexPath(row: 0, section: section))
                currentIndex.section = section
            } else {
                adjustPdfViewScaleFactor()
                pdfView.document = pdfDocuments[section]
                pdfView.goToPage(index: 0)
            }
        }
    }
}

@available(iOS 11.0,*)
extension PDFBasePreviewController : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        ZInfo(indexPath)

        
        if(currentIndex.row != indexPath.row){
            self.isScrollDisable = true
            if DeviceType.isMac {
                zsPdfView.moveToPage(indexPath: indexPath)
            } else {
                pdfView.goToPage(index: indexPath.row)
            }
        }
        
        currentIndex = indexPath
        self.perform({
            self.isScrollDisable = false
        }, afterDelay: 0.3)
    }
  
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return pdfDocuments[indexPath.section].thumbnailPageSize()
    }
}

extension PDFBasePreviewController : ZSPDFDocsListToolbarDelegate{
    func toolbarDidChangeDocument(docIndex: Int) {
        if(currentIndex.section != docIndex){
            thumbnailView.reloadData()
            thumbnailView.collectionViewLayout.invalidateLayout()
            if DeviceType.isMac {
                thumbnailView.goToPage(indexPath: IndexPath(row: 0, section: docIndex), animated: false)
                zsPdfView.moveToPage(indexPath: IndexPath(row: 0, section: docIndex))
                currentIndex.section = docIndex
            } else {
                pdfView.document = pdfDocuments[docIndex]
                adjustPdfViewScaleFactor()
                pdfView.goToPage(index: 0)
            }
        }
    }
    
}

extension PDFBasePreviewController : ZPDFOutlineViewControllerDelegate {
    
    func outlineTableViewController(_ outlineTableViewController: ZPDFOutlineViewController, didSelectOutline outline: PDFOutline) {
        if let actiongoto = outline.action as? PDFActionGoTo {
            pdfView.go(to: actiongoto.destination)
        }
    }
    
}

extension PDFBasePreviewController : ZSPDFSearchViewControllerDelegate {
    
    public func searchTableViewController(_ searchTableViewController: ZSPDFSearchViewController, didSelectSerchResult selection: PDFSelection) {
        selection.color = UIColor.yellow
        pdfView.currentSelection = selection
        pdfView.go(to: selection)
        NotificationCenter.default.addObserver(self, selector: #selector(pdfViewDidChangePage), name: NSNotification.Name.PDFViewPageChanged, object: nil)
    }
    
}

extension PDFBasePreviewController : UITableViewDelegate,UITableViewDataSource{
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return pdfDocuments.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ZPDFViewCell = tableView.dequeueReusableCell(withIdentifier: "ZPDFViewCell", for: indexPath) as! ZPDFViewCell
        cell.setDocument(pdfDocuments[indexPath.section])
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (tableView.cellForRow(at: indexPath) as? ZPDFViewCell)?.getHeight() ?? UITableView.automaticDimension
    }
    
}

public class ZPDFViewCell : UITableViewCell{
    private let pdfView = PDFView(frame: CGRect.zero)
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialSetup(){
        pdfView.displayMode =  PDFDisplayMode.singlePageContinuous
//        pdfView.translatesAutoresizingMaskIntoConstraints = false
        pdfView.minScaleFactor = pdfView.scaleFactorForSizeToFit
        pdfView.maxScaleFactor = 3
        pdfView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
//        pdfView.autoScales  =  true
        pdfView.clipsToBounds   = false
//        pdfView.isUserInteractionEnabled = false
        self.contentView.addSubview(pdfView)
        self.selectionStyle = .none
//        pdfView.snp.makeConstraints { (make) in
//            make.edges.equalToSuperview()
//            make.width.height.equalToSuperview()
//        }
    }
    
    public func setDocument(_ doc : PDFDocument){
        pdfView.document = doc
    }
    
    public func getHeight() -> CGFloat{
        return (pdfView.subviews[0] as? UIScrollView)?.contentSize.height ?? 0
    }
}

extension PDFView {

    func goToPage(index : Int){
        if let document = self.document,
            let page = document.page(at: index)
        {
            self.go(to: page)
//            let pageBounds = page.bounds(for: self.displayBox)
//            self.go(to: CGRect(x: 0, y: pageBounds.height, width: 1.0, height: 1.0), on: page)
        }
    }
    
    func currentPageIndex() -> Int?{
        if let page = currentPage{
            return document?.index(for: page)
        }
        return nil
    }
    
}


extension PDFBasePreviewController : UIScrollViewDelegate, PDFViewDelegate{
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
  
}
