//
//  ZSPDFThumbnailView.swift
//  ZohoSignSDK
//
//  Created by sudhan-6859 on 28/09/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import Foundation
import PDFKit
import SnapKit

@objc public protocol ZSPDFThumbnailViewDelegate
{
    func pdfThumbnailViewDidSelectHeader(section: Int)
}


@available(iOS 11, *)
public class ZSPDFThumbnailView: UICollectionView , UICollectionViewDataSource {
    
    public var documentsArray : [ZSPDFDocument] = []
    fileprivate let cellIdentifier = "ZSPDFThumbnailViewCell"
    var selectedIndexPath : IndexPath = IndexPath(item: 0, section: -1)
//    public var isCollapsed = false
    @objc public weak var  thumbnailDelegate : ZSPDFThumbnailViewDelegate?
    
    override public init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        initateSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func initateSetup()  {
        self.dataSource = self
        self.register(ZSPDFThumbnailCell.self, forCellWithReuseIdentifier: cellIdentifier);
        self.register(ZSPDFThumbnailHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "ZSPDFThumbnailHeaderView")
        addBorder()
        
    }
    
    @objc public func goToPage(indexPath :  IndexPath, animated : Bool){
//        ZInfo(indexPath)

        if documentsArray.count == 0 { return}
        
        if selectedIndexPath.section != indexPath.section {
//            reloadData()
            let indexArray =  (selectedIndexPath.section < 0) ? [indexPath.section] :  [selectedIndexPath.section,indexPath.section]
            selectedIndexPath = indexPath
            reloadSections(IndexSet(indexArray))
        }else{
            selectedIndexPath = indexPath
        }
        if(documentsArray.count > 0 && (selectedIndexPath.section >= 0)){
            selectItem(at: indexPath, animated: false, scrollPosition: .centeredVertically)
        }
        
       /* if selectedIndexPath.section != indexPath.section {
            reloadData()
            selectedIndexPath = indexPath
            if(documentsArray.count > 0){
                selectItem(at: indexPath, animated: false, scrollPosition: .centeredVertically)
            }
        }else{
            selectedIndexPath = IndexPath(row: 0, section: -1)
            reloadData()
        }*/
    }
    
    @objc public func headerBtnTapped(section : Int){
        if selectedIndexPath.section == section {
            let indexArray =    [selectedIndexPath.section]

            selectedIndexPath = IndexPath(row: 0, section: -1)
//            reloadData()
            reloadSections(IndexSet(indexArray))
        }else{
            goToPage(indexPath: IndexPath(row: 0, section: section), animated: true)
        }
        thumbnailDelegate?.pdfThumbnailViewDidSelectHeader(section: section)
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return documentsArray.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let document   = documentsArray[indexPath.section]
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "ZSPDFThumbnailHeaderView", for: indexPath) as? ZSPDFThumbnailHeaderView
        
        headerView?.addBorder()
//        headerView?.backgroundColor = ZColor.redColor
        headerView?.setTitle(document.title, docIndex: indexPath.section, numberOfPages: document.pageCount, isExpanded: indexPath.section == selectedIndexPath.section)
//        if documentsArray.count <= 1 {
//            headerView?.hideExpandBtn()
//        }
        
        weak var weakself = self
        headerView?.completionHandler = {
            weakself?.headerBtnTapped(section: indexPath.section)
        }
        return headerView!
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(selectedIndexPath.section == section){
            
            return documentsArray[section].pageCount
        }else{
            return 0
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =
            collectionView.dequeueReusableCell(
                withReuseIdentifier: self.cellIdentifier,
                for: indexPath) as? ZSPDFThumbnailCell
        cell?.page = documentsArray[indexPath.section].page(at: indexPath.row)
        
//        cell.isSelected = (selectedIndexPath == indexPath)
        return cell!
    }
    
    
}



//MARK:  - Thumbcollection Cell
@available(iOS 11,*)
class ZSPDFThumbnailCell: UICollectionViewCell {
    
    private let thumbImgView  = UIImageView()
    private let pageNumLabel  = UILabel()
    
    var page : PDFPage!{
        didSet{
            var image = self.page.thumbnail(of: .a4Size(120), for: .artBox)
            /*if let rotation = page?.rotation, rotation > 0, SwiftUtils.isIOS13AndAbove, !DeviceType.isMac {
                image = image.rotate(radians: (Float(rotation) * (.pi / 180)))
            }
            */
            thumbImgView.image = image // a4 size
            pageNumLabel.text = self.page.label
        }
    }
    
    override var isSelected: Bool{
        didSet{
            if(isSelected){
                thumbImgView.layer.borderWidth = 1
                thumbImgView.layer.borderColor = ZColor.buttonColor.cgColor
//                thumbImgView.layer.borderWidth = 2
//                thumbImgView.layer.shadowColor  =   ZColor.blackColor.cgColor
//                self.alpha = 1
            }else{
//                thumbImgView.layer.borderWidth = 0.5
//                thumbImgView.layer.shadowColor  =   ZColor.blackColor.cgColor
//                self.alpha = 0.9
                thumbImgView.layer.borderWidth = 0.5
                thumbImgView.layer.borderColor = ZColor.seperatorColor.cgColor
            }

        }
        
        
       
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        thumbImgView.layer.shadowPath   =   UIBezierPath(rect: thumbImgView.bounds).cgPath
        thumbImgView.layer.shadowColor  =   UIColor.black.cgColor
    }
    
    private func initialSetup(){
        
//        thumbImgView.frame  =  CGRect(x: 0, y: 15, width: bounds.size.width, height: bounds.size.height - 55);
        thumbImgView.autoresizingMask   =   [.flexibleWidth,.flexibleHeight]
        thumbImgView.layer.shadowColor  =   UIColor.black.cgColor
        thumbImgView.layer.shadowColor  =   ZColor.blackColor.cgColor
        thumbImgView.layer.shadowOpacity  = 0.2
        thumbImgView.layer.shadowOffset =   CGSize(width: 0, height: 0)
        thumbImgView.layer.shadowRadius =  3
        thumbImgView.contentMode = .scaleToFill
        thumbImgView.clipsToBounds = false
        thumbImgView.backgroundColor = ZColor.bgColorGray
        self.contentView.addSubview(thumbImgView)
        thumbImgView.layer.borderWidth = 0.5
        thumbImgView.layer.borderColor = ZColor.seperatorColor.cgColor
        
        thumbImgView.layer.shadowPath   =   UIBezierPath(rect: thumbImgView.bounds).cgPath

        
//        pageNumLabel.frame    =   CGRect(x: 0, y: 0, width: 30, height: 18)
        pageNumLabel.backgroundColor    =   UIColor.clear
        pageNumLabel.textColor     =    ZColor.secondaryTextColorDark
        pageNumLabel.font          =    ZSFont.mediumFont(withSize: 13)
//        pageNumLabel.layer.cornerRadius =   3;
        pageNumLabel.textAlignment  =   .center
        pageNumLabel.clipsToBounds  =   true
        self.contentView.addSubview(pageNumLabel)
//        pageNumLabel.center =   CGPoint(x: thumbImgView.center.x, y: self.frame.size.height - 16)
        thumbImgView.snp.makeConstraints { (make) in
            make.left.width.equalToSuperview()
            make.top.equalTo(15)
            make.height.equalToSuperview().inset(25)
        }
        
        
        pageNumLabel.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(thumbImgView.snp.bottom)
            make.bottom.equalToSuperview().inset(5)
        }
        layoutIfNeeded()
        
    }
    
    
    
    //MARK: Deinitializers
    deinit {
        ZInfo("deinit")
    }
//    override var isHighlighted: Bool
//        {
//        didSet{
//            if(isHighlighted == true){
//                self.thumbImgView.alpha  =   0.5
//            }else{
//                self.thumbImgView.alpha  =   1
//            }
//        }
//    }
}

class ZSPDFThumbnailHeaderView : UICollectionReusableView{
    private let titleButton              =   ZSButton()
    let titleNameLabel = UILabel()
    let pagesCountLabel = UILabel()

    var completionHandler : (() -> ())?

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialSetup(){
        backgroundColor = ZColor.ipadRightSideBarColor
        
        titleButton.titleLabel?.font = ZSFont.regularFont(withSize: 14)
        
        titleButton.setTitleColor(ZColor.primaryTextColor, for: .normal)
        titleButton.titleLabel?.textAlignment    =   .left
        titleButton.translatesAutoresizingMaskIntoConstraints = false
        titleButton.contentVerticalAlignment = .center
        titleButton.contentHorizontalAlignment = .right
        titleButton.titleLabel?.numberOfLines = 2
        titleButton.clipsToBounds = true
        titleButton.semanticContentAttribute  = .forceRightToLeft
        
        self.addSubview(titleButton)
        titleButton.snp.makeConstraints { (make) in
            make.leading.equalTo(10)
            make.trailing.equalToSuperview().inset(10)
            make.top.equalTo(0)
            make.bottom.equalToSuperview()
        }
        titleButton.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
        
        titleNameLabel.font = ZSFont.mediumFont(withSize: 15)
        titleButton.addSubview(titleNameLabel)
//        titleNameLabel.backgroundColor = ZColor.redColor
        titleNameLabel.numberOfLines = 1
//        titleNameLabel.snp.makeConstraints { (make) in
//            make.left.equalTo(25)
//            make.top.equalTo(5)
//            make.right.equalToSuperview().inset(5)
//            make.height.equalTo(30)
//        }
        titleNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(8)
            make.top.equalTo(5)
            make.right.equalToSuperview().inset(25)
            make.height.equalTo(30)
        }
        
        pagesCountLabel.font = ZSFont.regularFont(withSize: 13)
        pagesCountLabel.textColor = ZColor.secondaryTextColorDark
        titleButton.addSubview(pagesCountLabel)
        pagesCountLabel.numberOfLines = 1
//        pagesCountLabel.snp.makeConstraints { (make) in
//            make.left.equalTo(25)
//            make.top.equalTo(28)
//            make.right.equalToSuperview().inset(5)
//            make.height.equalTo(20)
//        }
        pagesCountLabel.snp.makeConstraints { (make) in
            make.left.equalTo(8)
            make.top.equalTo(28)
            make.right.equalTo(titleNameLabel.snp.right)
            make.height.equalTo(20)
        }
    }
    
    @objc private func buttonClicked(){
        completionHandler?()
    }
    
    func hideExpandBtn() {
        titleNameLabel.snp.updateConstraints { (make) in
           make.right.equalToSuperview().inset(8)
        }
        titleButton.imageView?.isHidden = true
    }
    
    func setTitle(_ titleStr: String?,docIndex : Int, numberOfPages : Int,isExpanded : Bool){
        let docuIndex = String(format: "%i", docIndex + 1)
        let titStr = titleStr?.replacingOccurrences(of: docuIndex + "_", with: "")

        pagesCountLabel.text = String(format: "sign.mobile.document.pagesCount".ZSLString, numberOfPages)
        
//        titleButton.setTitle(titleStr, for: .normal)
        titleNameLabel.text = titStr
//        titleButton.setTitle(String(format: "%@. %@",docuIndex, titStr ?? ""), for: .normal)
        /*if(isExpanded){
            titleButton.setTitleColor(ZColor.primaryTextColor, for: .normal)
            titleButton.setImage(ZSImage.tintColorImage(withNamed: "thumbnailCollapse", col: ZColor.secondaryTextColorDark)?.imageWithSize(size: CGSize(width: 18, height: 18)), for: .normal)
            backgroundColor = ZColor.listSelectionColor
        }else{
            titleButton.imageView?.transform = CGAffineTransform(rotationAngle: 0)
            titleButton.setTitleColor(ZColor.secondaryTextColorDark, for: .normal)
            titleButton.setImage(ZSImage.tintColorImage(withNamed: "thumbnailExpand", col: ZColor.secondaryTextColorDark).imageWithSize(size: CGSize(width: 18, height: 18)), for: .normal)
            backgroundColor = ZColor.ipadRightSideBarColor
        }*/
        if(isExpanded){
            titleButton.imageView?.transform = CGAffineTransform(rotationAngle: 90*CGFloat.pi/180)
            titleButton.setTitleColor(ZColor.primaryTextColor, for: .normal)
            titleButton.setImage(ZSImage.tintColorImage(withNamed: "detailarrow", col: ZColor.primaryTextColor), for: .normal)
            backgroundColor = ZColor.listSelectionColor
        }else{
            titleButton.imageView?.transform = CGAffineTransform(rotationAngle: 0)
            titleButton.setTitleColor(ZColor.secondaryTextColorDark, for: .normal)
            titleButton.setImage(ZSImage.tintColorImage(withNamed: "detailarrow", col: ZColor.secondaryTextColorDark), for: .normal)
            backgroundColor = ZColor.ipadRightSideBarColor
        }
    }
    
}
