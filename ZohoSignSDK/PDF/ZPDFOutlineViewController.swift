//
//  ZPDFOutlineViewController.swift
//  ZohoSignSDK
//
//  Created by sudhan-6859 on 15/10/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit
import PDFKit

@available(iOS 11.0, *)
protocol ZPDFOutlineViewControllerDelegate: class{
    func outlineTableViewController(_ outlineTableViewController: ZPDFOutlineViewController, didSelectOutline outline: PDFOutline)
}

@available(iOS 11.0, *)
class ZPDFOutlineViewController: UITableViewController {
    
    private struct WrappedBundleImage: _ExpressibleByImageLiteral {
        let image: UIImage
        
        init(imageLiteralResourceName name: String) {
            let bundle = Bundle(for: ZPDFOutlineViewController.classForCoder())
            image = UIImage(named: name, in: bundle, compatibleWith: nil)!
        }
    }
    
    let outlineTableViewCellReuseIdentifier = "ZPDFOutlineTableViewCell"
    
    open var pdfOutlineRoot: PDFOutline? {
        didSet{
            for index in 0...(pdfOutlineRoot?.numberOfChildren)!-1 {
                let pdfOutline = pdfOutlineRoot?.child(at: index)
                pdfOutline?.isOpen = false
                data.append(pdfOutline!)
            }
            tableView.reloadData()
        }
    }
    
    var data = [PDFOutline]()
    
    weak var delegate: ZPDFOutlineViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.cellLayoutMarginsFollowReadableWidth = false
        tableView.rowHeight = UITableView.automaticDimension;
        tableView.estimatedRowHeight = 44.0;
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(closeBtnClick))
        
        tableView.register(ZPDFOutlineTableViewCell.classForCoder(), forCellReuseIdentifier: outlineTableViewCellReuseIdentifier)
    }
    
    @objc func closeBtnClick(sender: UIBarButtonItem) {
        dismiss(animated: false, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: outlineTableViewCellReuseIdentifier, for: indexPath) as! ZPDFOutlineTableViewCell
        
        let outline = data[indexPath.row];
        
        cell.outlineTextLabel.text = outline.label
        cell.pageLabel.text = outline.destination?.page?.label
        
        if outline.numberOfChildren > 0 {
            cell.openButton.setImage(((outline.isOpen ? #imageLiteral(resourceName: "arrow_down") : #imageLiteral(resourceName: "arrow_right")) as WrappedBundleImage).image, for: .normal)
            cell.openButton.isEnabled = true
        } else {
            cell.openButton.setImage(nil, for: .normal)
            cell.openButton.isEnabled = false
        }
        
        cell.openButtonClick = {[weak self] (sender)-> Void in
            if outline.numberOfChildren > 0 {
                if sender.isSelected {
                    outline.isOpen = true
                    self?.insertChildren(parent: outline)
                } else {
                    outline.isOpen = false
                    self?.removeChildren(parent: outline)
                }
                tableView.reloadData()
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        let outline = data[indexPath.row];
        let depth = findDepth(outline: outline)
        return depth;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let outline = data[indexPath.row]
        delegate?.outlineTableViewController(self, didSelectOutline: outline)
        dismiss(animated: false, completion: nil)
    }
    
    func findDepth(outline: PDFOutline) -> Int {
        var depth: Int = -1
        var tmp = outline
        while (tmp.parent != nil) {
            depth = depth + 1
            tmp = tmp.parent!
        }
        return depth
    }
    
    func insertChildren(parent: PDFOutline) {
        var tmpData: [PDFOutline] = []
        let baseIndex = self.data.firstIndex(of: parent)
        for index in 0..<parent.numberOfChildren {
            if let pdfOutline = parent.child(at: index) {
                pdfOutline.isOpen = false
                tmpData.append(pdfOutline)
            }
        }
        self.data.insert(contentsOf: tmpData, at:baseIndex! + 1)
    }
    
    func removeChildren(parent: PDFOutline) {
        if parent.numberOfChildren <= 0 {
            return
        }
        
        for index in 0..<parent.numberOfChildren {
            if let node = parent.child(at: index) {
                
                if node.numberOfChildren > 0 {
                    removeChildren(parent: node)
                    
                    // remove self
                    if let i = data.firstIndex(of: node) {
                        data.remove(at: i)
                    }
                } else {
                    if self.data.contains(node) {
                        if let i = data.firstIndex(of: node) {
                            data.remove(at: i)
                        }
                    }
                }
            }
        }
    }
    
}

class ZPDFOutlineTableViewCell: UITableViewCell {
    
    lazy var openButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(openButtonClick(_:)), for: .touchUpInside)
        button.backgroundColor = .clear
        
        return button
    }()
    
    lazy var outlineTextLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        
        return label
    }()
    
    lazy var pageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .right
        label.backgroundColor = .clear
        
        return label
    }()
    
    var openButtonClick:((_ sender: UIButton) -> Void)?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        [self.openButton, self.outlineTextLabel, self.pageLabel].forEach({ self.contentView.addSubview($0) })
        
        NSLayoutConstraint.activate (
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[openButton(==44)]-[outlineTextLabel]-[pageLabel]-|", options: [], metrics: nil, views: ["openButton": self.openButton, "outlineTextLabel": self.outlineTextLabel, "pageLabel": self.pageLabel]) +
                NSLayoutConstraint.constraints(withVisualFormat: "V:|[openButton(==44)]|", options: [], metrics: nil, views: ["openButton": self.openButton])
        )
        
        self.contentView.addConstraint(NSLayoutConstraint(item: self.outlineTextLabel, attribute: .height, relatedBy: .equal, toItem: self.contentView, attribute: .height, multiplier: 1, constant: 0) )
        self.contentView.addConstraint(NSLayoutConstraint(item: self.pageLabel, attribute: .height, relatedBy: .equal, toItem: self.contentView, attribute: .height, multiplier: 1, constant: 0) )
        
        indentationWidth = 16.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if indentationLevel == 0 {
            outlineTextLabel.font = UIFont.systemFont(ofSize: 17)
            pageLabel.font = UIFont.systemFont(ofSize: 17)
        } else {
            outlineTextLabel.font = UIFont.systemFont(ofSize: 15)
            pageLabel.font = UIFont.systemFont(ofSize: 15)
        }
        
        self.contentView.layoutMargins.left = CGFloat(self.indentationLevel) * self.indentationWidth
        self.contentView.layoutIfNeeded()
    }
    
    @IBAction func openButtonClick(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        openButtonClick?(sender)
    }
    
}
