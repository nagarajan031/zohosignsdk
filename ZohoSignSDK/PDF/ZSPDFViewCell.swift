//
//  ZSPDFViewCell.swift
//  PDFView
//
//  Created by somesh-8758 on 01/08/19.
//  Copyright © 2019 Somesh-8758. All rights reserved.
//

import UIKit
import SnapKit

class ZSPDFViewCell: UITableViewCell {
    
    static let cellLeadingSpacing: CGFloat = 20
    static let cellTopSpacing: CGFloat = 20
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = ZColor.bgColorGray
        contentView.addSubview(pdfImageView)
        pdfImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(ZSPDFViewCell.cellTopSpacing)
            make.bottom.equalToSuperview()
            make.trailing.equalToSuperview().offset(-1 * ZSPDFViewCell.cellLeadingSpacing)
            make.leading.equalToSuperview().offset(ZSPDFViewCell.cellLeadingSpacing)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private lazy var pdfImageView: UIImageView = {
       let view = UIImageView()
        view.addBorder(width: 1.8)
        return view
    }()
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {                pdfImageView.addBorder(width: 1)
            }
        }
    }
    
    
    func setupCell(filePath: String){
        contentView.addSubview(pdfImageView)
        pdfImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().offset(-10)
            make.trailing.equalToSuperview().offset(-20)
            make.leading.equalToSuperview().offset(20)
        }
        guard let image = UIImage(contentsOfFile: filePath) else {return}
        pdfImageView.image = image
    }
    
    func setupCell(data: Data) {
        contentView.addSubview(pdfImageView)
        pdfImageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        guard let image = UIImage(data: data) else {return}
        pdfImageView.image = image
    }
    
    func setupCell(image: UIImage) {
        pdfImageView.image = image
//        pdfImageView.image = image.imageWithSize(size: pdfImageView.frame.size)
    }
    deinit {
        ZInfo("deinit called")
    }
}
