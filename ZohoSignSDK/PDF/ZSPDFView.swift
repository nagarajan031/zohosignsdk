//
//  ZSPDFView.swift
//  PDFView
//
//  Created by somesh-8758 on 01/08/19.
//  Copyright © 2019 Somesh-8758. All rights reserved.
//

import UIKit

public struct ZSPDF {
    var title: String
    var images: [UIImage]
}

public class ZSPDFView: UIView {
    var zsPdfs: [ZSPDF]!
    
    private lazy var tableView: UITableView = {
        let view = UITableView()
        view.separatorStyle = .none
        view.allowsSelection = false
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    var ignoreDidScroll: Bool = false
    
    init(images: [ZSPDF]) {
        super.init(frame: .zero)
        self.zsPdfs = images
        setupView()
    }
    
    func setupView(){
        addSubview(tableView)
        tableView.backgroundColor = ZColor.bgColorGray
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: ZSPDFViewCell.cellTopSpacing))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setImageData(images: [ZSPDF]){
        self.zsPdfs = images
        tableView.reloadData()
    }
}

//MARK: TABLEVIEW DATA-SOURCE
extension ZSPDFView: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return zsPdfs[section].images.count
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return zsPdfs.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: ZSPDFViewCell! = tableView.dequeueReusableCell(withIdentifier: String(describing: ZSPDFViewCell.self)) as? ZSPDFViewCell
        if cell == nil {
            cell = ZSPDFViewCell(style: .default, reuseIdentifier: String(describing: ZSPDFViewCell.self))
        }
        cell.setupCell(image: zsPdfs[indexPath.section].images[indexPath.row])
        return cell
    }

}


//MARK: TABLEVIEW DELEGATES
extension ZSPDFView: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let image = zsPdfs[indexPath.section].images[indexPath.row]
        let tableViewWidth = tableView.bounds.width - (ZSPDFViewCell.cellLeadingSpacing * 2)
        let imageHeight = image.size.height
        let imageWidth = image.size.width
        return tableViewWidth * (imageHeight/imageWidth)
    }
    
//    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = ZTableHeaderView(style: .plain)
//        headerView.titleLabel.textAlignment = .center
//        headerView.titleLabel.textColor = ZColor.primaryTextColor
//        headerView.titleLabel.text = zsPdfs[section].title
//        return headerView
//    }
//
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
}

//MARK: SCROLLVIEW DELEGATE
extension ZSPDFView: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let firstVisibleCellIndexPath = getFirstVisibleIndexpath() {
            NotificationCenter.default.post(name: .pdfViewCellChanged, object: firstVisibleCellIndexPath)
        }
    }
    
    func getFirstVisibleIndexpath() -> IndexPath? {
        if let indexPaths = tableView.indexPathsForVisibleRows, !ignoreDidScroll {
            for firstVisibleCellIndexPath in indexPaths {
                if let cell = tableView.cellForRow(at: firstVisibleCellIndexPath) {
                    let convertedRect = tableView.convert(cell.frame, to: self)
                    if convertedRect.origin.y + cell.frame.height/2 > 0 {
                        return firstVisibleCellIndexPath
                    }
                }
            }
        }
        return nil
    }
    
}

//MSRK: UTIL METHODS
public extension ZSPDFView {
    func gotoNextPage(){
        if let firstVisibleCellIndexpath = getFirstVisibleIndexpath(){
            
            let currentSectionIndexpath = IndexPath(row: firstVisibleCellIndexpath.row + 1, section: firstVisibleCellIndexpath.section)
            let nextSectionIndexPath = IndexPath(row: 0, section: firstVisibleCellIndexpath.section + 1)
            
            if firstVisibleCellIndexpath.row + 1 <= tableView.numberOfRows(inSection: firstVisibleCellIndexpath.section) - 1{
                moveToPage(indexPath: IndexPath(row: firstVisibleCellIndexpath.row + 1, section: firstVisibleCellIndexpath.section), shouldCallPageChangedelegate: true)
            } else if firstVisibleCellIndexpath.section + 1 <= tableView.numberOfSections - 1{
                moveToPage(indexPath: IndexPath(row: 0, section: firstVisibleCellIndexpath.section + 1), shouldCallPageChangedelegate: true)
            }
        }
    }
    
    func gotoPreviousPage(){
        if let firstVisibleCellIndexpath = getFirstVisibleIndexpath(){
            let zsPdf = zsPdfs[firstVisibleCellIndexpath.section]
            
            let currentSectionIndexpath = IndexPath(row: firstVisibleCellIndexpath.row - 1, section: firstVisibleCellIndexpath.section)
            
            if firstVisibleCellIndexpath.row - 1 >= 0 {
                moveToPage(indexPath: IndexPath(row: firstVisibleCellIndexpath.row - 1, section: firstVisibleCellIndexpath.section), shouldCallPageChangedelegate: true)
            } else if firstVisibleCellIndexpath.section - 1 >= 0 {
                let previousFile = zsPdfs[firstVisibleCellIndexpath.section - 1]
                if firstVisibleCellIndexpath.section - 1 >= 0 {
                    moveToPage(indexPath: IndexPath(row: previousFile.images.count - 1, section: firstVisibleCellIndexpath.section - 1), shouldCallPageChangedelegate: true)
                }
            }
        }
    }
    
    func gotoFirstPage(isCurrentSection: Bool = false){
        let indexPath: IndexPath!
        if isCurrentSection,let firstVisibleIndexpath = getFirstVisibleIndexpath() {
            indexPath = IndexPath(row: 0, section: firstVisibleIndexpath.section)
        } else {
            indexPath = IndexPath(row: 0, section: 0)
        }
        moveToPage(indexPath: indexPath, shouldCallPageChangedelegate: true)
    }
    
    func gotoLastPage(isCurrentSection: Bool = false){
        let indexPath: IndexPath!
        if isCurrentSection,let firstVisibleIndexpath = getFirstVisibleIndexpath() {
            let row = zsPdfs[firstVisibleIndexpath.section].images.count - 1
            indexPath = IndexPath(row: row, section: firstVisibleIndexpath.section)
        } else {
            indexPath = IndexPath(row: zsPdfs.last!.images.count - 1 , section: zsPdfs.count - 1)
        }
        moveToPage(indexPath: indexPath, shouldCallPageChangedelegate: true)
    }
    
    func moveToPage(indexPath: IndexPath, shouldCallPageChangedelegate: Bool = false) {
        ignoreDidScroll = true
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        if shouldCallPageChangedelegate {
            NotificationCenter.default.post(name: .pdfViewCellChanged, object: indexPath)
        }
        perform({[weak self] in
            self?.ignoreDidScroll = false
        }, afterDelay: 0.3)
    }
}
