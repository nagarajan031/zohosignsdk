//
//  ZPDFSearchViewController.swift
//  ZohoSignSDK
//
//  Created by sudhan-6859 on 16/10/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit
import PDFKit

@available(iOS 11.0, *)
@objc public protocol ZSPDFSearchViewControllerDelegate: class {
    func searchTableViewController(_ searchTableViewController: ZSPDFSearchViewController, didSelectSerchResult selection: PDFSelection)
    
    @objc optional func searchTableViewControllerWillDismiss()
}

@available(iOS 11.0, *)
public class ZSPDFSearchViewController: UITableViewController {
    
    let searchTableViewCellReuseIdentifier = "ZPDFSearchTableViewCell"
    
    fileprivate lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        searchBar.searchBarStyle = .minimal
        
        return searchBar
    }()
    
    var searchResults = [PDFSelection]()
    
    open var pdfDocument: PDFDocument?
    public weak var delegate: ZSPDFSearchViewControllerDelegate?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.cellLayoutMarginsFollowReadableWidth = false
        
        navigationItem.titleView = searchBar
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonClicked))
        
        tableView.register(ZSPDFSearchTableViewCell.classForCoder(), forCellReuseIdentifier: searchTableViewCellReuseIdentifier)
        preferredContentSize = CGSize(width: 370, height: 1)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchBar.becomeFirstResponder()
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func cancelButtonClicked(sender: UIBarButtonItem) {
        cancelPressed()
    }
    
    fileprivate func cancelPressed() {
        pdfDocument?.cancelFindString()
        dismiss(animated: true, completion: nil)
    }
    
    override public func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        pdfDocument?.cancelFindString()
        super.dismiss(animated: flag, completion: completion)
    }
    
    // MARK: - UITableViewDataSource
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return searchResults.count
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: searchTableViewCellReuseIdentifier, for: indexPath) as! ZSPDFSearchTableViewCell
        
        let selection = searchResults[indexPath.row]
        let page = selection.pages[0]
        let outline = pdfDocument?.outlineItem(for: selection)
        
        let outlintstr = outline?.label ?? ""
        let pagestr = page.label ?? ""
        let txt = outlintstr + " \(NSLocalizedString("Page", comment: "To be translated")):  " + pagestr
        cell.destinationLabel.text = txt
        
        let extendSelection = selection.copy() as! PDFSelection
        extendSelection.extend(atStart: 10)
        extendSelection.extend(atEnd: 90)
        extendSelection.extendForLineBoundaries()
        
        let range = (extendSelection.string! as NSString).range(of: selection.string!, options: .caseInsensitive)
        let attrstr = NSMutableAttributedString(string: extendSelection.string!)
        attrstr.addAttribute(.backgroundColor, value: UIColor.yellow, range: range)
        
        cell.resultTextLabel.attributedText = attrstr
        
        return cell
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selection = searchResults[indexPath.row]
        
        delegate?.searchTableViewController(self, didSelectSerchResult: selection)
        dismiss(animated: false, completion: nil)
    }
    
    override public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
}

@available(iOS 11.0, *)
extension ZSPDFSearchViewController: UISearchBarDelegate {
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        cancelPressed()
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count < 2 {
            return
        }
        
        searchResults.removeAll()
        tableView.reloadData()
        if let pdfDocument = pdfDocument {
            pdfDocument.cancelFindString()
            pdfDocument.delegate = self
            pdfDocument.beginFindString(searchText, withOptions: .caseInsensitive)
        }
    }
    
}

@available(iOS 11.0, *)
extension ZSPDFSearchViewController: PDFDocumentDelegate {
    
    public func didMatchString(_ instance: PDFSelection) {
        searchResults.append(instance)
        tableView.reloadData()
        preferredContentSize = CGSize(width: 370, height: 1000)
    }
    
}


class ZSPDFSearchTableViewCell: UITableViewCell {
    
    lazy var destinationLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .right
        label.backgroundColor = UIColor.clear
        label.font = UIFont.boldSystemFont(ofSize: 17.0)
        
        return label
    }()
    
    lazy var resultTextLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.backgroundColor = UIColor.clear
        label.font = UIFont.systemFont(ofSize: 17.0)
        
        return label
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        [self.destinationLabel, self.resultTextLabel].forEach({ self.contentView.addSubview($0) })
        
        NSLayoutConstraint.activate (
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[destinationLabel]-|", options: [], metrics: nil, views: ["destinationLabel": self.destinationLabel]) +
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-[resultTextLabel]-|", options: [], metrics: nil, views: ["resultTextLabel": self.resultTextLabel]) +
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-[destinationLabel]-[resultTextLabel]-|", options: [], metrics: nil, views: ["destinationLabel": self.destinationLabel, "resultTextLabel": self.resultTextLabel])
        )
        
    }
    
}
