//
//  ZSPDFDocsListController.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 31/07/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

class ZSPDFDocsListController: UITableViewController {
    public var didSelectDocHandler : ((_ withDocIndex: Int) -> Void)?

    var documents : [ZSPDFDocument] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.setSeparatorStyle()
        tableView.separatorInset = UIEdgeInsets.zero
        navigationItem.leftBarButtonItem = ZSBarButtonItem.barButton(type: .cancel, target: self, action: #selector(cancelBtnPressed))        
        preferredContentSize = CGSize(width: 320, height: TableviewCellSize.subtitle * CGFloat(documents.count))
    }
    
    @objc func cancelBtnPressed(){
        dismiss(animated: true, completion: nil)
    }
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documents.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return TableviewCellSize.subtitle
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell"
        
        var cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        if cell == nil {
            cell =  UITableViewCell(style: .subtitle, reuseIdentifier: identifier)
            let selectedView = UIView()
            selectedView.backgroundColor = ZColor.listSelectionColor;
            cell.selectedBackgroundView = selectedView;
        }

        let doc = documents[indexPath.row]
        let docIndex =  String(format: "%i_", (indexPath.row + 1))
        let titStr =  doc.title.replacingOccurrences(of: docIndex, with: "")

    
        cell.textLabel?.text = titStr
        cell.detailTextLabel?.text =  String(format: "sign.mobile.document.pagesCount".ZSLString, doc.pageCount)
        cell.detailTextLabel?.textColor = ZColor.secondaryTextColorDark
        cell.imageView?.image = doc.page(at: 0)?.thumbnail(of: CGSize(width: 36, height: 50), for: .artBox)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
        didSelectDocHandler?(indexPath.row)
        cancelBtnPressed()
    }

    

}
