//
//  ZSPDFDocsListToolbar.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 31/07/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation
protocol ZSPDFDocsListToolbarDelegate: class {
    func toolbarDidChangeDocument(docIndex : Int)
}

class ZSPDFDocsListToolbar : UIView, UIPopoverPresentationControllerDelegate {
    public var documents : [ZSPDFDocument] = []
    public weak var delegate: ZSPDFDocsListToolbarDelegate?

    private lazy var listBtn : ZSButton = {
        let listBtn = ZSButton()
        listBtn.setImage(ZSImage.tintColorImage(withNamed: "editor_list_view", col: ZColor.whiteColor), for: .normal)
        listBtn.addTarget(self, action: #selector(listBtnTapped), for: .touchUpInside)
        return listBtn
    }()
    
    private lazy var currentIndexLabel : UILabel = {
        let indexLabel = UILabel()
        indexLabel.textColor = ZColor.whiteColor
        indexLabel.font = ZSFont.regularFont(withSize: 16)
        indexLabel.textAlignment = .center
        indexLabel.adjustsFontSizeToFitWidth = true
        return indexLabel
    }()
    
    
    public convenience init(documents : [ZSPDFDocument]) {
        self.init()
    
        self.documents = documents
        initateSetup()
    }
    
    func initateSetup() {
        backgroundColor = UIColor(white: 0, alpha: 0.75)
        layer.cornerRadius = 6
        clipsToBounds =  true
        
        addSubview(listBtn)
        addSubview(currentIndexLabel)

        listBtn.snp.makeConstraints { (make) in
//            make.left.top.bottom.equalToSuperview()
//
            make.top.bottom.equalToSuperview()
            make.width.equalTo(44)
            make.right.equalToSuperview()
        }
        
        currentIndexLabel.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(5)
            make.right.equalToSuperview().inset(40)
        }
        currentIndexLabel.text = String(format: "sign.mobile.viewer.showingCurrentItem".ZSLString, 1,1)
    }
    
    public func changeDocumentIndex(_ index : Int)  {
        UIView.animate(withDuration: 0.3) {
            self.currentIndexLabel.isHidden = false
            self.currentIndexLabel.text = String(format: "sign.mobile.viewer.showingCurrentItem".ZSLString, index, self.documents.count)
        }
    }
    
    @objc func listBtnTapped()  {
        let docsListController = ZSPDFDocsListController()
        docsListController.documents = documents
        docsListController.didSelectDocHandler = {[weak self]index in
            self?.delegate?.toolbarDidChangeDocument(docIndex: index)
        }
//        let nav = UINavigationController(rootViewController: docsListController)
        docsListController.modalPresentationStyle = .popover
        let popover = docsListController.popoverPresentationController
        popover?.delegate = self
        popover?.sourceView = listBtn
        popover?.sourceRect = listBtn.bounds
        if let _delegate = delegate as? UIViewController {
             _delegate.present(docsListController, animated: true, completion: nil)
        }
    }
    
    
    //MARK:- Modal transition helper
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .fullScreen
    }

    func presentationController(_ controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
        return UINavigationController(rootViewController: controller.presentedViewController)
    }

}
