//
//  ZSPDFDocument.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 31/12/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit
import PDFKit
import MobileCoreServices

public final class ZSPDFDocument: PDFDocument,Codable,NSItemProviderReading,NSItemProviderWriting  {
    public static var readableTypeIdentifiersForItemProvider: [String]{
        get{
            return [(kUTTypeData as String)]
        }
    }
    
    public static func object(withItemProviderData data: Data, typeIdentifier: String) throws -> ZSPDFDocument {
        //Here we decode the object back to it's class representation and return it
        let subject = ZSPDFDocument()
        subject.data = data
        return subject
        
    }
    
    public static var writableTypeIdentifiersForItemProvider: [String]{
        get{
            return [(kUTTypeData as String)]
        }
    }
    
    public func loadData(withTypeIdentifier typeIdentifier: String, forItemProviderCompletionHandler completionHandler: @escaping (Data?, Error?) -> Void) -> Progress? {
        let progress = Progress(totalUnitCount: 100)
        do {
            //Here the object is encoded to a JSON data object and sent to the completion handler
            let data = try JSONEncoder().encode(self)
            progress.completedUnitCount = 100
            completionHandler(data, nil)
        } catch {
            completionHandler(nil, error)
        }
        return progress
        
    }
//
//    public lazy var bounds : CGRect  = {
//        return (page(at: 0)?.bounds(for: .artBox) ?? CGRect.zero)
//    }()
    
    public func thumbnailPageSize() -> CGSize{
        let width : CGFloat = 120//120
        var a4Ratio : CGFloat = 1.414
        var height = width*a4Ratio + 55
           
       guard let page = page(at: 0) else {
           return CGSize(width: width, height: height)
       }
        
                
        
//        if page.rotation > 0, SwiftUtils.isIOS13AndAbove {
//            if ((page.rotation / 90) % 2) == 1{
//                return CGSize(width: width, height: height)
//            }
//        }
        
        let bounds  = page.bounds(for: .artBox)
        a4Ratio = (bounds.height / bounds.width)
        height = width*a4Ratio + 55
        return CGSize(width: width, height: height)
    }
    
    public var title : String = ""
    public var data: Data? = nil
    
    public var isAttachment = false
    public var attachmentOwner : String = ""
}


extension ZSPDFDocument {
    public static func getPopupDataModel(forPdfDocuments documents: [ZSPDFDocument]) -> [PopupDataModel]{
         return documents.enumeratedMap { (index, document) -> PopupDataModel in
             let docIndex = String(format: "%i_", index + 1)
             let title = document.title.replacingOccurrences(of: docIndex, with: "")
             let subTitle = String(format: "sign.mobile.document.pagesCount".ZSLString, document.pageCount)
             let image = document.page(at: 0)?.thumbnail(of: CGSize(width: 36, height: 50), for: .artBox)
             return PopupDataModel(title: title, subtitle: subTitle, image: image)
         }
     }
}
