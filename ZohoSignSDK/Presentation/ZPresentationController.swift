//
//  ZPresentationController.swift
//  ZohoSign
//
//  Created by Nagarajan S on 12/09/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

class Interactor: UIPercentDrivenInteractiveTransition {
    var hasStarted = false
    var shouldFinish = false
}

class ZPresentationController: UIPresentationController {

    // MARK: - Properties
    fileprivate var dimmingView: UIView!
    private var direction: PresentationDirection
    public var popupHeight : CGFloat = 420
    var interactor: Interactor!
    private var dimmingViewAlpha: CGFloat
    
    private lazy var dragIndicatorView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 5))
        view.layer.cornerRadius = 2.5
        view.clipsToBounds = true
        view.backgroundColor = .setColor(dark: ZColor.secondaryTextColorDark, light: ZColor.secondaryTextColorLight)
        return view
    }()

    override var frameOfPresentedViewInContainerView: CGRect {
        var frame: CGRect = .zero
        frame.size = size(forChildContentContainer: presentedViewController, withParentContainerSize: containerView!.bounds.size)
        switch direction {
//        case .right:
//            frame.origin.x = containerView!.frame.width*(0.06)
        case .bottom:
            frame.origin.y = containerView!.frame.height*(0.06)
        case .bottomWithCustomHeight:
            frame.origin.y = containerView!.frame.height - popupHeight
        default:
            frame.origin = .zero
        }
        return frame
    }
    
    // MARK: - Initializers
    init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?, direction: PresentationDirection,isBlurredBackground: Bool, dimmingViewAlpha: CGFloat) {
        self.dimmingViewAlpha = dimmingViewAlpha
        self.direction = direction
        
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        setupDimmingView(isBlurredBackground: isBlurredBackground)
        if direction == .bottomWithCustomHeight {
            interactor = Interactor()
            let pangesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(sender:)))
            presentedViewController.view.addGestureRecognizer(pangesture)
        }
    }
    
    @objc func handlePanGesture(sender: UIPanGestureRecognizer) {
        let percentThreshold:CGFloat = 0.3
        let view: UIView! = presentedViewController.view
        // convert y-position to downward pull progress (percentage)
        let translation = sender.translation(in: view)
        let verticalMovement = translation.y / view.bounds.height
        let downwardMovement = fmaxf(Float(verticalMovement), 0.0)
        let downwardMovementPercent = fminf(downwardMovement, 1.0)
        let progress = CGFloat(downwardMovementPercent)
        guard let interactor = interactor else { return }
        
        switch sender.state {
        case .began:
            interactor.hasStarted = true
            presentedViewController.dismiss(animated: true)
        case .changed:
            interactor.shouldFinish = progress > percentThreshold
            interactor.update(progress)
        case .cancelled:
            interactor.hasStarted = false
            interactor.cancel()
        case .ended:
            interactor.hasStarted = false
            interactor.shouldFinish
                ? interactor.finish()
                : interactor.cancel()
        default:
            break
        }
    }
    
    override func presentationTransitionWillBegin() {
        containerView?.insertSubview(dimmingView, at: 0)
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|[dimmingView]|", options: [], metrics: nil, views: ["dimmingView": dimmingView]))
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|[dimmingView]|", options: [], metrics: nil, views: ["dimmingView": dimmingView]))
        
        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmingView.alpha = 1.0
            return
        }
        
        coordinator.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = 1.0
        })
        
        /*
        if DeviceType.isIphone, let nav = presentedViewController as? UINavigationController,let viewController = nav.viewControllers.first {
            let dummyView = UIView(frame: CGRect(x: 0, y: 0, width: nav.navigationBar.frame.width, height: nav.navigationBar.frame.height))
            dummyView.addSubview(dragIndicatorView)
            dragIndicatorView.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(10)
                make.centerX.equalToSuperview()
                make.height.equalTo(5)
                make.width.equalTo(50)
            }
            viewController.navigationItem.titleView = dummyView
        }
        */
    }
    
    override func dismissalTransitionWillBegin() {
        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmingView.alpha = 0.0
            return
        }
        
        coordinator.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = 0.0
        })
    }
    
    override func containerViewWillLayoutSubviews() {
        presentedView?.frame = frameOfPresentedViewInContainerView
    }
    
    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        switch direction {
//        case .left, .right:
//            return CGSize(width: parentSize.width*(0.8), height: parentSize.height)
//        case .top:
//            return CGSize(width: parentSize.width, height: parentSize.height*(0.94))
        case .bottom:
            return CGSize(width: parentSize.width, height: parentSize.height*(0.94))
        case .bottomWithCustomHeight:
            return CGSize(width: parentSize.width, height: popupHeight)
        case .macTopSheet:
            return CGSize(width: 600, height: popupHeight)
        default:
            return .zero
        }
    }
}

// MARK: - Private
private extension ZPresentationController {
    
    func setupDimmingView(isBlurredBackground: Bool) {
        dimmingView = UIView()
        dimmingView.translatesAutoresizingMaskIntoConstraints = false
        dimmingView.alpha = 0.0
        if #available(iOS 13, macCatalyst 13, *), isBlurredBackground {
            let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .systemUltraThinMaterialLight))
            visualEffectView.bounds = dimmingView.bounds
            visualEffectView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
            dimmingView.addSubview(visualEffectView)
        } else {
            dimmingView.backgroundColor = UIColor(white: 0.0, alpha: dimmingViewAlpha)
        }
        
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:)))
        dimmingView.addGestureRecognizer(recognizer)
    }
    
    @objc dynamic func handleTap(recognizer: UITapGestureRecognizer) {
        if direction == .bottomWithCustomHeight {
            presentingViewController.dismiss(animated: true)
        }
    }
}

class DragPresentationView: UIView {
    init(with presentedView: UIView, frame: CGRect) {
        super.init(frame: frame)
        addSubview(presentedView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
