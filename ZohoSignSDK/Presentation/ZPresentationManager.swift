//
//  ZPresentationManager.swift
//  ZohoSign
//
//  Created by Nagarajan S on 12/09/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit



public class ZPresentationManager: NSObject {

    public var popupHeight : CGFloat = 420
    public var isBottomPopupWithCustomHeight : Bool = false //to use .bottomWithCustomHeight direction it in Objc
    // MARK: - Properties
    public var direction = PresentationDirection.bottom
    public var disableCompactHeight = true
    public var shouldApplyCornerRadius = true
    public var isBlurredBackground = false
    public var dimmingViewAlpha: CGFloat = 0.5
    var presentationController: ZPresentationController!
}

// MARK: - UIViewControllerTransitioningDelegate
extension ZPresentationManager: UIViewControllerTransitioningDelegate {
    
    
    public func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        if isBottomPopupWithCustomHeight {
            direction = .bottomWithCustomHeight
        }
        presentationController = ZPresentationController(presentedViewController: presented, presenting: presenting, direction: direction, isBlurredBackground: isBlurredBackground,dimmingViewAlpha: dimmingViewAlpha)
        presentationController.popupHeight = popupHeight
        presentationController.delegate = self
        return presentationController
    }
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ZPresentationAnimator(direction: direction, isPresentation: true,shouldApplyCornerRadius: shouldApplyCornerRadius)
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ZPresentationAnimator(direction: direction, isPresentation: false, shouldApplyCornerRadius: shouldApplyCornerRadius)
    }
    
    public func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return direction == .bottomWithCustomHeight && presentationController.interactor.hasStarted ? presentationController.interactor : nil
    }
}

// MARK: - UIAdaptivePresentationControllerDelegate
extension ZPresentationManager: UIAdaptivePresentationControllerDelegate {
    
    public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        if traitCollection.verticalSizeClass == .compact && disableCompactHeight {
            return .overFullScreen
        } else {
            return .none
        }
    }
    
    public func presentationController(_ controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
        guard case(.overFullScreen) = style else { return nil }
        
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RotateViewController")
    }
}
