//
//  ZPresentationAnimator.swift
//  ZohoSign
//
//  Created by Nagarajan S on 12/09/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

class ZPresentationAnimator: NSObject {

    // MARK: - Properties
    let direction: PresentationDirection
    let isPresentation: Bool
    let shouldApplyCornerRadius: Bool
    
    // MARK: - Initializers
    init(direction: PresentationDirection, isPresentation: Bool, shouldApplyCornerRadius: Bool) {
        self.direction = direction
        self.isPresentation = isPresentation
        self.shouldApplyCornerRadius = shouldApplyCornerRadius
        super.init()
    }
}

// MARK: - UIViewControllerAnimatedTransitioning
extension ZPresentationAnimator: UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let key = isPresentation ? UITransitionContextViewControllerKey.to : UITransitionContextViewControllerKey.from
        let controller = transitionContext.viewController(forKey: key)!
        
        if isPresentation {
            transitionContext.containerView.addSubview(controller.view)
        }
        
        var presentedFrame = transitionContext.finalFrame(for: controller)
        var dismissedFrame = presentedFrame
        switch direction {
//        case .left:
//            dismissedFrame.origin.x = -presentedFrame.width
//        case .right:
//            dismissedFrame.origin.x = transitionContext.containerView.frame.size.width
//        case .top:
//            dismissedFrame.origin.y = -presentedFrame.height
        case .bottom:
            dismissedFrame.origin.y = transitionContext.containerView.frame.size.height
            controller.view.layer.cornerRadius = 10
            controller.view.clipsToBounds   =   true

        case .bottomWithCustomHeight:
            dismissedFrame.origin.y = transitionContext.containerView.frame.size.height
            if shouldApplyCornerRadius {
                controller.view.layer.cornerRadius = 10
                controller.view.clipsToBounds   =   true
            }

        case .macTopSheet:
            controller.view.layer.cornerRadius = 5

            let orginX = (transitionContext.containerView.frame.size.width - 600) / 2
            presentedFrame.origin.x = orginX
            dismissedFrame.origin.x = orginX
            dismissedFrame.origin.y = -transitionContext.containerView.frame.size.height
            controller.view.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.top.equalToSuperview()
                make.width.equalTo(600)
                make.height.equalTo(dismissedFrame.size.height)
            }
            controller.view.layer.shadowColor = UIColor.black.cgColor
            controller.view.layer.shadowOffset = CGSize(width: 0, height: 0.0)
            controller.view.layer.shadowRadius = 5.0
            controller.view.layer.shadowOpacity = 0.5
            controller.view.layer.shadowPath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: presentedFrame.width, height: presentedFrame.height), cornerRadius: 5).cgPath
        default:
            break
        }
        
        let initialFrame = isPresentation ? dismissedFrame : presentedFrame
        let finalFrame = isPresentation ? presentedFrame : dismissedFrame
        
        let animationDuration = transitionDuration(using: transitionContext)
        controller.view.frame = initialFrame
        
        UIView.animate(withDuration: animationDuration, animations: {
            controller.view.frame = finalFrame
        }) { finished in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}

extension UIView {
    var presentationContainerView: UIView? {
        return subviews.first(where: { view -> Bool in
            view is DragPresentationView
        })
    }
}
