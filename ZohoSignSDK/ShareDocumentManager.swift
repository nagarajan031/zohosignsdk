//
//  ShareDocumentManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 17/07/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

public class ShareDocumentManager: NSObject {

    @objc public weak var rootVc : UIViewController?
    
    @objc public let shareText = "sign.mobile.request.shareDocument".ZSLString
    #if !targetEnvironment(macCatalyst)
    public let documentInteractionController = UIDocumentInteractionController()
    #endif
    public var fileName: String?
    public var url: URL?
    public var uti : String?
    

    @objc public func presentOptionsMenu(from rect: CGRect, in view: UIView, animated: Bool)
    {
        #if !targetEnvironment(macCatalyst)
        ZSAnalytics.shared.track(forEvent: .shareManager(.ios))
        documentInteractionController.url = url
        documentInteractionController.uti = uti
        documentInteractionController.presentOptionsMenu(from: rect, in: view, animated: animated)
        #else
           presentExportOptionInMac()
        #endif
    }
    
    @objc open func presentOptionsMenu(from item: UIBarButtonItem, animated: Bool)
    {
        #if !targetEnvironment(macCatalyst)
        ZSAnalytics.shared.track(forEvent: .shareManager(.ios))
        documentInteractionController.url = url
        documentInteractionController.uti = uti
        documentInteractionController.presentOptionsMenu(from: item, animated: animated)
        #else
        presentExportOptionInMac()
        #endif
    }
    
    
    /*
    func presentOpenInMenu(from rect: CGRect, in view: UIView, animated: Bool)
    {
        #if !targetEnvironment(macCatalyst)
        ZSAnalytics.shared.track(forEvent: .shareManager(.ios))
        documentInteractionController.url = url
        documentInteractionController.uti = uti
        documentInteractionController.presentOpenInMenu(from: rect, in: view, animated: true)
        #else
        presentExportOptionInMac()
        #endif
    }
    
    open func presentOpenInMenu(from item: UIBarButtonItem, animated: Bool){

        #if !targetEnvironment(macCatalyst)
        ZSAnalytics.shared.track(forEvent: .shareManager(.ios))
        documentInteractionController.url = url
        documentInteractionController.uti = uti
        documentInteractionController.presentOpenInMenu(from: item, animated: animated)
        #else
        presentExportOptionInMac()
        #endif
    }
    */

    fileprivate func presentExportOptionInMac()  {
        ZSAnalytics.shared.track(forEvent: .shareManager(.mac))
        let documentPickerController  = UIDocumentPickerViewController(url: url!, in: .exportToService)
        documentPickerController.modalPresentationStyle = .overCurrentContext
        rootVc?.present(documentPickerController, animated: true, completion: nil)
    }
}


extension ShareDocumentManager : UIDocumentInteractionControllerDelegate{
    public func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return rootVc!
    }
}
