//
//  MacPresentationAnimator.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 21/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit
class MacPresentationAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    private let direction: MacPresentationDirection
    private let isPresentation: Bool
    
    init(direction: MacPresentationDirection, isPresentation: Bool) {
        self.direction = direction
        self.isPresentation = isPresentation
        super.init()
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let key = isPresentation ? UITransitionContextViewControllerKey.to : UITransitionContextViewControllerKey.from
        guard let controller = transitionContext.viewController(forKey: key) else {return}
        
        if isPresentation {
            transitionContext.containerView.addSubview(controller.view)
        }
        
        var presentedFrame = transitionContext.finalFrame(for: controller)
        var dismissedFrame = presentedFrame
        
        controller.view.layer.cornerRadius = 10
        controller.view.clipsToBounds = true
        
        switch direction {
        case .formSheet,.pageSheet:
            dismissedFrame.origin.y = transitionContext.containerView.frame.height
        case .top, .topWithCustomHeight:
            controller.view.layer.cornerRadius = 5
            dismissedFrame.origin.y = -transitionContext.containerView.frame.height
            controller.view.layer.shadowColor = UIColor.black.cgColor
            controller.view.layer.shadowOffset = CGSize(width: 0, height: 0.0)
            controller.view.layer.shadowRadius = 5.0
            controller.view.layer.shadowOpacity = 0.5
            controller.view.layer.shadowPath = UIBezierPath(roundedRect: CGRect(origin: .zero, size: presentedFrame.size), cornerRadius: 5).cgPath
        }
        
        let initialFrame = isPresentation ? dismissedFrame : presentedFrame
        let finalFrame = isPresentation ? presentedFrame : dismissedFrame
        
        let animationDuration =  transitionDuration(using: transitionContext)
        controller.view.frame = initialFrame
        
        UIView.animate(withDuration: animationDuration, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
             controller.view.frame = finalFrame
        }) { (finished) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
        
//        UIView.animate(withDuration: animationDuration, animations: {
//            controller.view.frame = finalFrame
//        }) { finished in
//            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
//        }
    }
}
