//
//  ZTextViewCell.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 18/09/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZTextViewCell: UITableViewCell, UITextViewDelegate {

    public let textView = UITextView()
    
    public var textViewBeginEditing : (() -> Void)?
    public var textViewEndEditing : ((String) -> Void)?
    public var textValueChanged : ((String, CGFloat) -> Void)?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initial setup
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        //fatalError("init(coder:) has not been implemented")
    }
    
    
    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func initialSetup(){
        
        self.contentView.backgroundColor = ZColor.bgColorWhite
        self.contentView.autoresizingMask = .flexibleHeight
      
        textView.backgroundColor = ZColor.bgColorWhite
        textView.font = ZSFont.createListValueFont()
        textView.textColor = ZColor.primaryTextColor
        textView.delegate = self
//        textView.isScrollEnabled = false
        textView.textContainer.lineFragmentPadding = 0
        self.contentView.addSubview(textView)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        textView.frame = CGRect(x: cellValueLabelX, y: 0, width: self.bounds.size.width - 32, height: self.bounds.size.height )
    }
    
    public func setTextView(valueText:String, editable:Bool)
    {
       
        textView.text = valueText
        
        if (editable) {
            textView.isUserInteractionEnabled = true
            self.contentView.alpha = 1.0
        }else {
            textView.isUserInteractionEnabled = false
            self.contentView.alpha = 0.5
        }
        
        self.setNeedsLayout()
    }
    
    // MARK: - Delegate
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if (self.textViewBeginEditing != nil) {
            self.textViewBeginEditing!()
        }
    }
   
    public func textViewDidEndEditing(_ textView: UITextView) {
        if (self.textViewEndEditing != nil) {
            self.textViewEndEditing!(textView.text!)
        }
    }
    
  
    public func textViewDidChange(_ textView: UITextView) {
        if (self.textValueChanged != nil) {
            self.textValueChanged!(textView.text, 100)
        }
        
    }
    
    
//    func textViewHeightForAttributedText(attText text:NSAttributedString, andWidth width:CGFloat) -> CGSize {
//        let tview = UITextView()
//        tview.attributedText = text
//        let size = tview.sizeThatFits(CGSize(width: width, height: .greatestFiniteMagnitude))
//        return size
//    }
//
}
