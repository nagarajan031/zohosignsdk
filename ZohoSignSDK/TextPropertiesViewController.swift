//
//  TextPropertiesViewController.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 27/11/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

public struct TextProperty {
    public var fontColor: UIColor
    public var fontFamily: String
    public var fontSize: Int
    public var isBold: Bool = false
    public var isItalics: Bool = false
    public var isUnderline: Bool = false
    
}

public protocol TextPropertiesViewControllerDelegate: NSObjectProtocol {
    func textPropertyDidChanged(_ textPropertry: TextProperty)
}

public class TextPropertiesViewController: UIViewController {
    
    public var selectedTextProperty: TextProperty {
        willSet {
            if !isFromTouchBar {
                NotificationCenter.default.post(name: .textPropertyChanged , object: newValue)
            }
            delegate?.textPropertyDidChanged(newValue)
            isFromTouchBar = false
        }
    }
    
    private let fontStyle = [
        "B",
        "I"
    ]
    
    private let fontSizeTitles = [
        "A-",
        "A+"
    ]
    
    public let colorsArray =  [
        UIColor.black,
        UIColor(red: 0.27, green: 0.70, blue: 0.33, alpha: 1),
        UIColor(red: 0.99, green: 0.59, blue: 0.15, alpha: 1),
        UIColor(red: 0.98, green: 0.33, blue: 0.34, alpha: 1),
        UIColor(red: 0.3255, green: 0.5059, blue: 0.8588, alpha: 1)
    ];
    
    public var fontSelectionView: FontSelectionView!
    
    private var closeButton: ZSButton = {
        let view = ZSButton()
        view.imageView?.contentMode = .scaleToFill
        view.contentMode = .right
        view.contentHorizontalAlignment = .right
        view.setImage(UIImage(named: "create_close")?.downsampleImage(size: CGSize(width: 20, height: 20))?.withRenderingMode(.alwaysTemplate), for: .normal)
        view.tintColor = ZColor.secondaryTextColorDark
        view.backgroundColor = ZColor.bgColorGray
        return view
    }()
    
    private lazy var fontsScrollViewSelectionView:  UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        view.backgroundColor = ZColor.buttonColor
        view.clipsToBounds = true
        return view
    }()
    
    public lazy var fontStyleStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .fillEqually
        view.spacing = 2
        return view
    }()
    
    private var selectedLabelColor: UIColor{
        return .setColor(dark: ZColor.primaryTextColor, light: ZColor.whiteColor)
    }
    
    private var unSelectedLabelColor: UIColor {
        return ZColor.primaryTextColor
    }
    
    public var colorSelectionStackView: UIStackView!
    public var fontSizeStackView: UIStackView!
    
    private let scrollViewContentheight: CGFloat = 55
    public let maxFontSize =  24
    public let minFontSize = 8
    
    public var isFromTouchBar = false
    public weak var delegate: TextPropertiesViewControllerDelegate?
    
    private var showdowColor: UIColor {
        if #available(iOS 13, *){
            return .setAppearance(dark: .secondarySystemBackground, light: .systemGroupedBackground)
        } else {
            return ZColor.bgColorGray
        }

    }
    
    init(selectedTextProperty: TextProperty) {
        self.selectedTextProperty = selectedTextProperty
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        fontSelectionView = FontSelectionView(selectedFontFamily: selectedTextProperty.fontFamily)
        fontSelectionView.delegate = self
        view.backgroundColor = ZColor.bgColorGray
        
        colorSelectionStackView = fontStyleStackView.copy()
        fontSizeStackView = fontStyleStackView.copy()
        
        if DeviceType.isIpad {
            preferredContentSize = CGSize(width: 375, height: 220)
        }

        ///font style selection view
        for (index,style) in fontStyle.enumerated() {
            let button = ZSButton()
            button.setTitle(style, for: .normal)
            button.backgroundColor = ZColor.bgColorWhite
            button.setTitleColor(ZColor.primaryTextColor, for: .normal)
            button.tag = index
            button.addTarget(self, action: #selector(fontStyleClicked(_:)), for: .touchUpInside)
            fontStyleStackView.addArrangedSubview(button)
            
            if index == 0 {
                button.backgroundColor = selectedTextProperty.isBold ? ZColor.buttonColor : ZColor.bgColorWhite
                button.titleLabel?.font = UIFont(name: "Menlo-Bold", size: 18)
            } else {
                button.backgroundColor = selectedTextProperty.isItalics ? ZColor.buttonColor : ZColor.bgColorWhite
                button.titleLabel?.font = UIFont(name: "Menlo-BoldItalic", size: 18)
            }
            
        }
        
        let fontStyleStackViewContainer = UIView()
        fontStyleStackViewContainer.layer.cornerRadius = 10
        fontStyleStackViewContainer.clipsToBounds = true
        fontStyleStackViewContainer.addSubview(fontStyleStackView)
        fontStyleStackView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        ///font size selection view
        for (index,fontSizeTitle) in fontSizeTitles.enumerated() {
            let button = ZSButton()
            let attributedText = NSMutableAttributedString(string: fontSizeTitle, attributes: [.font: ZSFont.mediumFont(withSize: index == 0 ? 14 : 17)])
            attributedText.setAttributes([.font: ZSFont.regularFont(withSize: 10),.baselineOffset:10], range: NSRange(location: 1, length: 1))
            button.setAttributedTitle(attributedText, for: .normal)
            button.addTarget(self, action: #selector(fontSizeChanged(_:)), for: .touchUpInside)
            button.tag = index
            button.backgroundColor = ZColor.bgColorWhite
            if index == 0, selectedTextProperty.fontSize <= minFontSize {
                button.isEnabled = false
            } else if index == 1, selectedTextProperty.fontSize >= maxFontSize {
                button.isEnabled = false
            }
            fontSizeStackView.addArrangedSubview(button)
        }
        
        let fontSizeStackViewContainer = UIView()
        fontSizeStackViewContainer.layer.cornerRadius = 10
        fontSizeStackViewContainer.clipsToBounds = true
        fontSizeStackViewContainer.addSubview(fontSizeStackView)
        fontSizeStackView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        ///font colors selection view
        for (index,color) in colorsArray.enumerated() {
            let button = ZSButton()
            button.setImage(UIImage.circle(diameter: 30, color: color), for: .normal)
            button.imageView?.layer.cornerRadius = 15
            button.imageView?.layer.borderWidth = 0.5
            button.imageView?.layer.borderColor = ZColor.whiteColor.cgColor
            button.backgroundColor = ZColor.bgColorWhite
            button.tag = index
            button.addTarget(self, action: #selector(colorChanged(_:)), for: .touchUpInside)
            colorSelectionStackView.addArrangedSubview(button)
            if selectedTextProperty.fontColor == color {
                button.backgroundColor = ZColor.buttonColor
                button.setTitleColor(selectedLabelColor, for: .normal)
            }
        }
        
        let colorSelectionContainerView = UIView()
        colorSelectionContainerView.layer.cornerRadius = 10
        colorSelectionContainerView.clipsToBounds = true
        colorSelectionContainerView.addSubview(colorSelectionStackView)
        colorSelectionStackView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        view.addSubviews(fontSelectionView , fontStyleStackViewContainer, fontSizeStackViewContainer, colorSelectionContainerView)
        
        fontSelectionView.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.topMargin).offset(DeviceType.isIphone ? 0 : 20)
            make.leading.equalToSuperview().offset(22)
            make.trailing.equalToSuperview().offset(-22)
            make.height.equalTo(scrollViewContentheight)
        }
        
        /*
        closeButton.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-22)
            make.height.equalTo(scrollViewContentheight)
            make.width.equalTo(30)
            make.centerY.equalTo(fontSelectionView)
        }
        */
        
        fontStyleStackViewContainer.snp.makeConstraints { (make) in
            make.top.equalTo(fontSelectionView.snp.bottom).offset(10)
            make.width.equalTo(150)
            make.height.equalTo(50)
            make.leading.equalTo(fontSelectionView)
        }
        
        fontSizeStackViewContainer.snp.makeConstraints { (make) in
            make.trailing.equalTo(fontSelectionView)
            make.size.top.equalTo(fontStyleStackViewContainer)
        }
        
        colorSelectionContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(fontStyleStackView.snp.bottom).offset(10)
            make.leading.trailing.equalTo(fontSelectionView)
            make.height.equalTo(50)
        }
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        closeButton.addTarget(self, action: #selector(closeButtonClicked), for: .touchUpInside)
        fontSelectionView.isViewAppeared = true
    }
    
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection){
            autoreleasepool { () -> Void in
                let isBoldButton = fontStyleStackView.arrangedSubviews.first as? ZSButton
                isBoldButton?.setTitleColor(selectedTextProperty.isBold ? selectedLabelColor : unSelectedLabelColor, for: .normal)
                let isItalicButton = fontStyleStackView.arrangedSubviews.last as? ZSButton
                isItalicButton?.setTitleColor(selectedTextProperty.isItalics ? selectedLabelColor : unSelectedLabelColor, for: .normal)
            }
        }
    }
    
    @objc public func colorChanged(_ sender: ZSButton) {
        colorSelectionStackView.arrangedSubviews.filter({$0.tag == colorsArray.firstIndex(of: selectedTextProperty.fontColor) ?? 0}).first?.backgroundColor = ZColor.bgColorWhite
        sender.backgroundColor = ZColor.buttonColor
        selectedTextProperty.fontColor = colorsArray[sender.tag]
    }
    
    @objc public func fontStyleClicked(_ sender: ZSButton){
        switch sender.tag {
        case 0:
            selectedTextProperty.isBold = !selectedTextProperty.isBold
            if selectedTextProperty.isBold {
                sender.backgroundColor = ZColor.buttonColor
                sender.setTitleColor(selectedLabelColor, for: .normal)
            } else {
                sender.backgroundColor = ZColor.bgColorWhite
                sender.setTitleColor(unSelectedLabelColor, for: .normal)
            }
        case 1:
            selectedTextProperty.isItalics = !selectedTextProperty.isItalics
            if selectedTextProperty.isItalics {
                sender.backgroundColor = ZColor.buttonColor
                sender.setTitleColor(selectedLabelColor, for: .normal)
            } else {
                sender.backgroundColor = ZColor.bgColorWhite
                sender.setTitleColor(unSelectedLabelColor, for: .normal)
            }
        default:
            break
        }
    }
    
    @objc public func fontSizeChanged(_ sender: ZSButton){
        switch sender.tag {
        case 0:
            if selectedTextProperty.fontSize > minFontSize {
                selectedTextProperty.fontSize -= 1
                sender.isEnabled = !(selectedTextProperty.fontSize == minFontSize)
                (fontSizeStackView.arrangedSubviews.last as? ZSButton)?.isEnabled = true
            }
        case 1:
            if selectedTextProperty.fontSize < maxFontSize {
                selectedTextProperty.fontSize += 1
                sender.isEnabled = !(selectedTextProperty.fontSize == maxFontSize)
                (fontSizeStackView.arrangedSubviews.first as? ZSButton)?.isEnabled = true
            }
        default:
            break
        }
    }
    
    @objc func closeButtonClicked(){
        dismiss(animated: true, completion: nil)
    }
}

extension TextPropertiesViewController: FontSelectionViewDelegate {
    public func fontFamilyDidChanged(withFontFamily fontFamily: String) {
        selectedTextProperty.fontFamily = fontFamily
    }
}

extension TextPropertiesViewController: ZPresentable {
    public var direction: PresentationDirection{
        return .bottomWithCustomHeight
    }
    
    public var shouldApplyCornerRadius: Bool {
        return false
    }
    
    public var dimmingViewAlpha: CGFloat {
        return 0.3
    }
    
    public var navBar: PresentationNavBar {
        return PresentationNavBar(size: PresentationBarSize.noTitleBig, backgroundColor: ZColor.bgColorGray)
    }
    
    public var popupHeight: CGFloat {
        return 200 + bottomMargin
    }
}
