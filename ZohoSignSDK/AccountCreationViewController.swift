//
//  AccountCreationViewController.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 30/07/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class AccountCreationViewController: UIViewController {

    let profileImgView = UIImageView()
    let accCreateView = UIView()
    let accountNameTextfield = ZSTextField()
    let createButton = ZSButton()
    let dataManger = UserRequestManager()
    

    public var proceedButtonTapHandler : (() -> Void)?
    
   
    var title_font = UIFont.preferredFont(forTextStyle: .headline)
    var desc_font = UIFont.preferredFont(forTextStyle: .callout)
    var btnTitle_font = UIFont.preferredFont(forTextStyle: .body)
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = ZColor.bgColorWhite
        
        navigationController?.navigationBar.barTintColor = ZColor.bgColorWhite

        noAccountHandling()
        navigationItem.leftBarButtonItem = ZSBarButtonItem.barButton(type: .cancel, target: self, action: #selector(dismissVC))
        //orgTextField.setPlaceHolder("Organization Name", isMandatory: false)
        // Do any additional setup after loading the view.
    }
    
    func noAccountHandling()  {
        profileImgView.alpha = 0
        accCreateView.alpha = 1
        
        title = "sign.mobile.account.registerOrg".ZSLString

        //        accCreateView.backgroundColor = ZColor.redColor
        view.addSubview(accCreateView)
        if DeviceType.isIpad {
//
//            title_font = ZSFont.mediumFont(withSize: 22)
//            desc_font = ZSFont.regularFont(withSize: 17)
//            btnTitle_font = ZSFont.mediumFont(withSize: 18)
            
            accCreateView.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.top.equalToSuperview().offset(160)
                make.bottom.equalToSuperview().inset(60)
                make.width.equalTo(460) //TODO: change width
            }
        }else{
            accCreateView.snp.makeConstraints { (make) in
                make.center.equalToSuperview()
                make.top.equalTo(view.snp.topMargin).offset(50)
                make.bottom.equalTo(view.snp.bottomMargin).offset(-10)
                make.width.equalToSuperview().inset(32)
            }
        }
        
        
        let titleLabel = UILabel()
        accCreateView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
        }
        titleLabel.font = title_font
        titleLabel.textAlignment = .left
        titleLabel.textColor = ZColor.primaryTextColor
        titleLabel.text =  "sign.mobile.accountCreate.alreadyhaveATeamAccount".ZSLString
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.sizeToFit()
        
        let descLabel = UILabel()
        accCreateView.addSubview(descLabel)
        descLabel.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom).offset(13)
        }
        descLabel.numberOfLines = 0
        descLabel.attributedText = attributedTextForDesc(withText: "sign.mobile.accountCreate.contactAdmin".ZSLString, align: .left)
        
        
        descLabel.sizeToFit()
        
        let  orTitleLabel = UILabel()
        accCreateView.addSubview(orTitleLabel)
        orTitleLabel.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(descLabel.snp.bottom).offset(55)
        }
        orTitleLabel.adjustsFontSizeToFitWidth = true
        orTitleLabel.font = title_font
        orTitleLabel.textAlignment = .left
        orTitleLabel.textColor = ZColor.primaryTextColor
        orTitleLabel.text =  "sign.mobile.accountCreate.newToZohoSign".ZSLString
        orTitleLabel.sizeToFit()
        
        
        accCreateView.addSubview(accountNameTextfield)
        accountNameTextfield.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(42)
            make.top.equalTo(orTitleLabel.snp.bottom).offset(4)
        }
        
        accountNameTextfield.placeholder        =   "sign.mobile.accountCreate.organisationName".ZSLString
        accountNameTextfield.font               =   desc_font
        accountNameTextfield.textColor          =   ZColor.primaryTextColor;
        accountNameTextfield.autocorrectionType =   .no
        accountNameTextfield.layer.cornerRadius =   3;
        accountNameTextfield.delegate           =   self;
        accountNameTextfield.returnKeyType      =   .done
        accountNameTextfield.enablesReturnKeyAutomatically  =   true
        accountNameTextfield.autocapitalizationType =   .words
        
        //        accountNameTextfield.addBorders(edges: [.bottom], color: ZColor.seperatorColor, inset: 0, thickness: 0.7)
        let borderView = UIView()
        borderView.backgroundColor = ZColor.seperatorColor
        accCreateView.addSubview(borderView)
        borderView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(accountNameTextfield.snp.bottom).offset(5)
            make.height.equalTo(0.7)
        }
        
        
        let descLabel1 = UILabel()
        accCreateView.addSubview(descLabel1)
        descLabel1.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(accountNameTextfield.snp.bottom).offset(20)
        }
        descLabel1.textAlignment = .left
        descLabel1.textColor = ZColor.primaryTextColorLight
        descLabel1.text =  "sign.mobile.accountCreate.orgNameDescription".ZSLString
        descLabel1.font = desc_font
        descLabel1.numberOfLines = 0
        
        descLabel1.sizeToFit()
        
        
        accCreateView.addSubview(createButton)
        createButton.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(45)
            make.top.equalTo(descLabel1.snp.bottom).offset(22)
        }
        
        createButton.backgroundColor      =   ZColor.buttonColor
        createButton.layer.cornerRadius   =   4;
        createButton.isEnabled  =   false
        createButton.titleLabel?.font      =   btnTitle_font
        createButton.setTitle("sign.mobile.iap.proceed".ZSLString, for: .normal)
        createButton.addTarget(self, action: #selector(createAccButtonClicked), for: .touchUpInside)
        
    }

    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    func attributedTextForDesc(withText text:String, align: NSTextAlignment) -> NSAttributedString {
        let descAttributedStr = NSMutableAttributedString(string: text)
        descAttributedStr.addAttribute(NSAttributedString.Key.font, value: desc_font, range: NSRange(location: 0, length: text.count))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        paragraphStyle.alignment = align
        descAttributedStr.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: text.count))
        
        return descAttributedStr
    }
    
    @objc func createAccButtonClicked()  {
        view.endEditing(true)
        ZSProgressHUD.show(inView: (self.navigationController?.view)!,status: "sign.mobile.account.creatingAccount".ZSLString)

         weak var weakself  = self
        dataManger.addAccount(orgName: accountNameTextfield.text!, adKeyword: nil, adGroup: nil, adCampaign: nil, success: { (success) in
            weakself?.proceedButtonTapHandler?()
            weakself?.dismiss(animated: true, completion: nil)
            ZSProgressHUD.dismiss()
        }) { (error) in
            weakself?.handleDataManagererror(error)
        }
    }

    
    @objc func dismissVC()
    {
        ZSProgressHUD.dismiss()
        ZSignKit.shared.clearLoggedInUserDetails()
        dismiss(animated: true, completion: nil)
    }
}


extension AccountCreationViewController : UITextFieldDelegate{
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let newText = textFieldText.replacingCharacters(in: range, with: string)
        
        var isAllowChar = true;
        let regexPredicate = NSPredicate(format: "SELF MATCHES %@", ZS_Regex_ForName)
        if (!regexPredicate.evaluate(with: newText)) {
            // Invalid char
            // Delete chars, but not add chars
            if ((textField.text?.count ?? 0) < newText.count) {
                isAllowChar = false;
                let msg =  String(format: "sign.mobile.common.characternotAllowed".ZSLString, string)
                SwiftUtils.showPopTip(withMessage: msg, inView: accCreateView, onRect: accountNameTextfield.frame)
            }
        }
        
        let accNameStr =  newText.trimmingCharacters(in: .whitespacesAndNewlines);
        
        createButton.isEnabled        =   accNameStr.count > 0;
        
        return isAllowChar
    }
    
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        createAccButtonClicked()
        return false
    }
}

