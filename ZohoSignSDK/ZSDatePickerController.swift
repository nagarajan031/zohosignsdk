//
//  ZSDatePickerController.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 23/11/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZSDatePickerController: UIViewController {
    var datePickerView   =  UIDatePicker()
    let calendarTitleLabel = UILabel()

    public var completionHandler : ((_ withDate: Date?) -> Void)?

    var dateToSelect :  Date?

    
    @objc public init(title: String?, selectedDate : Date? ) {
        dateToSelect = selectedDate
        super.init(nibName: nil, bundle: nil)
        self.title = title
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
 
    override public func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ZColor.bgColorGray
        navigationController?.view.backgroundColor = ZColor.bgColorGray
        navigationController?.navigationBar.barTintColor = ZColor.bgColorGray
        navigationController?.navigationBar.isTranslucent = false
        
        ZColor.adjustNavigationBarForTheme(navigationController?.navigationBar)
        
        if DeviceType.isIpad {
            preferredContentSize = CGSize(width: 340, height: 260)
        }
        
        datePickerView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 216)
        datePickerView.autoresizingMask =       [.flexibleWidth]
        datePickerView.backgroundColor = ZColor.bgColorGray
        datePickerView.datePickerMode = .date
        if let date = dateToSelect{
            datePickerView.date = date
        }
//        datepickerView.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        view.addSubview(datePickerView)
        
        
        navigationItem.rightBarButtonItem = ZSBarButtonItem.barButton(type: .done, target: self, action: #selector(doneButtonPressed))
    }
    
    @objc func doneButtonPressed(){
        completionHandler?(datePickerView.date)
        dismiss(animated: true, completion: nil)
    }

}

extension ZSDatePickerController: ZPresentable {
    public var direction: PresentationDirection{
        return .bottomWithCustomHeight
    }
    
    public var navBar: PresentationNavBar{
        return PresentationNavBar(size: PresentationBarSize.noTitleSmall, backgroundColor: ZColor.bgColorGray)
    }
    
    public var popupHeight: CGFloat {
        return 216 + ZS_NavbarHeight + bottomMargin + navBar.size
    }
}
