//
//  ZSRequestParsingUtil.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 02/02/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public typealias ActionDataParsingCompletionHandler = (_ receipientsArray: [ZSRecipient], _ isRecipientFieldsAvailable : Bool) -> ()
public typealias DocumentsToFilesCompletionHandler = (_ fileItems: [ZFileItem]) -> ()


public class ZSRequestParsingUtil: NSObject {
    let sentRequestDataManager = SentDocumentsRequestManager()
    let receivedRequestDataManager = ReceivedDocumentsRequestManager()
    let templateRequestDataManager = TemplateRequestManager()

    public func updateSentRequestDetails(request: SignRequest, successBlck : @escaping (() -> Void), failureBlck : @escaping ((ZSError?) -> Void)){
        sentRequestDataManager.cancelAllTasks()
        sentRequestDataManager.getSignRequestDetails(request.requestId!, success: { (data) in
            data.safelyUnwrap { (dict: ResponseDictionary) in
                ZSCoreDataManager.shared.updateSignRequest(request: request, dict: dict, isList: true)
            }

            successBlck()
        }) { (error) in
            failureBlck(error)
        }
    }

    public class func parseActionDataToRequesterList(_ actionData : Data? , completionHandler: ActionDataParsingCompletionHandler){
        var recipientsArray : [ZSRecipient]   =   []
        var isRecipientFieldsAvailable      =   false
        var recipientFieldCount             =   0
        var signerRecipientCount            =   0
        
        if (actionData == nil){
            completionHandler(recipientsArray, isRecipientFieldsAvailable);
            return
        }
        
        if let objects = NSKeyedUnarchiver.unarchiveObject(with: actionData!) as? [[String : Any]] {
            for dict in objects
            {
                let recipient = ZSRecipient()
                if let name = dict["recipient_name"] as? String {
                    recipient.name = name
                }
                if let email = dict["recipient_email"] as? String {
                    recipient.email = email
                }
                if let userRole = dict["role"] as? String {
                    recipient.userRole = userRole
                }
                
                recipient.actionId = dict["action_id"] as? String
                recipient.countryCode = dict["recipient_countrycode"] as? String
                recipient.phoneNumber = dict["recipient_phonenumber"] as? String

                let actionTypeString = dict["action_type"] as! String
                let actionStatusString : String = (dict[keyPath: "action_status"] ?? "mailed").lowercased()

                if (actionTypeString == "SIGN") {
                    recipient.actionType = ZS_ActionType_Sign
                    recipient.isSigningCompleted =  (actionStatusString == STATUS_SIGN_SIGNED)
                }else if(actionTypeString == "APPROVER"){
                    recipient.actionType = ZS_ActionType_Approver
                    recipient.isSigningCompleted =  (actionStatusString == STATUS_SIGN_APPROVED)
                }else if (actionTypeString == "INPERSONSIGN") {
                    recipient.actionType = ZS_ActionType_InPerson
                    recipient.isSigningCompleted =  (actionStatusString == STATUS_SIGN_SIGNED)
                }else {
                    recipient.actionType = ZS_ActionType_View
                    recipient.isSigningCompleted =  (actionStatusString == STATUS_SIGN_VIEWED)
                }
                recipient.isVerificationNeeded = (dict["verify_recipient"] as! Int == 1)
                if let recpDeliveryType = dict["verification_type"] as? String {
                    recipient.deliveryType = recpDeliveryType
                }
                if let recpVerifyCode = dict["verification_code"] as? String{
                    recipient.secretCode = recpVerifyCode
                }
                if let pvtMsg = dict["private_notes"] as? String {
                    recipient.privateMessage = pvtMsg
                }
                if let ishost = dict["ishost"] as? Int{
                    if (ishost == 1){
                        recipient.isHost = true
                        if let inPersonEmail = dict["in_person_email"] as? String {
                            recipient.inPersonEmail = inPersonEmail
                        }
                        if let inPersonName = dict["in_person_name"] as? String {
                            recipient.inPersonName = inPersonName
                        }
                    }
                }

                if let signingOrder = dict["signing_order"] as? Int {
                    recipient.signingOrder = signingOrder
                }
                
                // Fields & signer recipient check
                if let fields = dict["fields"] as? NSArray {
                    if(fields.count > 0) {
                        recipientFieldCount += 1
                    }
                }
                if (recipient.actionType == ZS_ActionType_Sign || recipient.actionType == ZS_ActionType_InPerson) {
                    signerRecipientCount += 1
                }
                
                recipientsArray.append(recipient)
                
            }
            // Check if all signing recipient has fields in the doc
            isRecipientFieldsAvailable = (signerRecipientCount == recipientFieldCount)
        }
        completionHandler(recipientsArray, isRecipientFieldsAvailable)
    }
    
    public class func parseDocumentsToFileItems(_ documents : Set<Documents>? , completionHandler: DocumentsToFilesCompletionHandler){
        var fileItems : [ZFileItem]   =   []
        
        if (documents == nil) {
            completionHandler(fileItems)
            return
        }
        
        if let sortedArray = documents?.sorted(by: { $0.documnetOrder < $1.documnetOrder }){
            for doc in sortedArray
            {
                let file = ZFileItem()
                file.fileName =  doc.documentName
                file.fileID   =  doc.documentId
                file.fileSize =  Int32(doc.fileSize)
                if let thumbnailStr = doc.thumbnailStr{
                    file.thumbnailImage = SwiftUtils.decodeBase64ToImage(encodeStr: thumbnailStr)
                }else{
                    file.thumbnailImage = UIImage(named:"list_bg")
                }
                fileItems.append(file)
            }
        }
        completionHandler(fileItems)
    }
    
    //MARK: - Request Parsing Handler
    public func deleteSentRequest(request: SignRequest, successBlck : @escaping (() -> Void), failureBlck : @escaping ((ZSError?) -> Void))  {
        sentRequestDataManager.deleteRequest(reqID: request.requestId!, success: { (success) in
            successBlck()
        }, failure: failureBlck)
    }
    
    public func restoreSentRequest(request: SignRequest, successBlck : @escaping (() -> Void), failureBlck : @escaping ((ZSError?) -> Void))  {
        sentRequestDataManager.restoreRequest(reqID: request.requestId!, success: { (success) in
            successBlck()
        }, failure: failureBlck)
    }
    
    public func deleteRecievedRequest(request: MyRequest, successBlck : @escaping (() -> Void), failureBlck : @escaping ((ZSError?) -> Void))  {
        receivedRequestDataManager.deleteRequest(reqId: request.myRequestId!, success: { (success) in
            successBlck()
        }, failure: failureBlck)
    }
    
    public func restoreRecivedRequest(request: MyRequest, successBlck : @escaping (() -> Void), failureBlck : @escaping ((ZSError?) -> Void))  {
        receivedRequestDataManager.restoreRequest(reqId: request.myRequestId!, success: { (success) in
            successBlck()
        }, failure: failureBlck)
    }
    
    
    public func deleteTemplate(template: Template, successBlck : @escaping (() -> Void), failureBlck : @escaping ((ZSError?) -> Void))  {
        templateRequestDataManager.deleteTemplate(id: template.templateId!, success: { (success) in
            successBlck()
        }, failure: failureBlck)
    }
    
    public func mailMyRequest(request: MyRequest, successBlck : @escaping (() -> Void), failureBlck : @escaping ((ZSError?) -> Void))  {
        receivedRequestDataManager.mailDocument(reqId: request.myRequestId!, success: { (success) in
            successBlck()
        }, failure: failureBlck)
    }
    
    
    //MARK: - Helpers
    public class func parseRequestNotesToHtml(_ noteString : String?) -> NSAttributedString {
        var attrStr = NSMutableAttributedString()
        do{
            let notesStr = DeviceType.isMac ?  noteString?.replacingOccurrences(from: "<img", to: "img>", with: "") : noteString
            
            attrStr = try NSMutableAttributedString(htmlString: notesStr ?? "-")
            attrStr.setFontFace(font: ZSFont.createListValueFont() , color: ZColor.primaryTextColor)
        }catch let ex{
            
        }
        return attrStr
    }
}

fileprivate extension String
{
    private func regExprOfDetectingStringsBetween(str1: String, str2: String) -> String {
        return "(?:\(str1))(.*?)(?:\(str2))"
    }
    
    func replacingOccurrences(from subString1: String, to subString2: String, with replacement: String) -> String {
        let regExpr = regExprOfDetectingStringsBetween(str1: subString1, str2: subString2)
        return replacingOccurrences(of: regExpr, with: replacement, options: .regularExpression)
    }

}


fileprivate extension NSMutableAttributedString {
    func setFontFace(font: UIFont, color: UIColor? = nil) {
        beginEditing()
        self.enumerateAttribute(
            .font,
            in: NSRange(location: 0, length: self.length)
        ) { (value, range, stop) in

            if let f = value as? UIFont,
              let newFontDescriptor = f.fontDescriptor
                .withFamily(font.familyName)
                .withSymbolicTraits(f.fontDescriptor.symbolicTraits) {

                let newFont = UIFont(
                    descriptor: newFontDescriptor,
                    size: font.pointSize
                )
                removeAttribute(.font, range: range)
                addAttribute(.font, value: newFont, range: range)
                if let color = color {
                    removeAttribute(
                        .foregroundColor,
                        range: range
                    )
                    addAttribute(
                        .foregroundColor,
                        value: color,
                        range: range
                    )
                }
            }
        }
        endEditing()
    }
}
