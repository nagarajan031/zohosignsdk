//
//  Notification.Name+.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 24/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

// Definition:
public extension Notification.Name {
    static let loginSuccess = Notification.Name("kNotificationLogin")
    static let logoutSuccess = Notification.Name("kNotificationLogout")
    static let loadAppForSignUser = Notification.Name("kNotificationLoadAppForSignUser")
    
    static let purchaseIAPCompletionHandling = Notification.Name("kNotificationPurchaseIAPCompletionHandling")
    
    static let loadMyRequest = Notification.Name("kNotificationLoadMyRequest")
    static let viewDismissedInCustomPresentation = Notification.Name("viewDismissedInCustomPresentation")
    
    //    static let toastAlert = Notification.Name("kNotificationAlertToast")
    //    static let toastError = Notification.Name("kNotificationErrorToast")
    //    static let toastLoading = Notification.Name("kNotificationLoadingToast")
    
    //combine notifications
    static let textPopControllerTextFieldChanged = Notification.Name("textPopControllerTextFieldChanged")
    static let pdfViewCellChanged = Notification.Name("pdfViewCellChanged")
    static let textPropertyChanged = Notification.Name("textPropertiesChanged")
}
