//
//  UserManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 31/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

let kUserDefaults_UserDetails = "UserDefaults_UserDetails"
//let kUserProfileUpdationNotification     = "UserProfileUpdationNotification"

public class UserManager: NSObject {
    public static let shared = UserManager()
    
    public var isUserExists = false
    let dataManager = UserRequestManager()
    
    public var currentUser : UserDetails?
    
    public func initialSetup()  {
        
        if (!ZSignKit.shared.isAppExtension) {
            // restore user detail from keychain
            let userDefaults = UserDefaults.standard
            if(!userDefaults.bool(forKey: "appfirstlaunchUserClear")){
                deleteCurrentUser()
                userDefaults.bool(forKey: "appfirstlaunchUserClear")
                userDefaults.synchronize()
            }
        }
        
        
        if let data  = KeychainManager.load(key: kUserDefaults_UserDetails) as? Data{
            let decoder = JSONDecoder()
            let user = try? decoder.decode(UserDetails.self, from: data)
            isUserExists = true
            currentUser = user
            ZSURLSession.shared.changeBaseDomainForGuest(baseDomain: currentUser?.urlBaseDomain, isUserLoggedIn: true)
        }
        
//        createUser()
//        saveUser(updateInServer: true)
    }
    
 
    func createUser()  {
        currentUser = nil
        
        currentUser = UserDetails()
        currentUser?.dateFormat = "dd MMM yyyy"
    }
    
    public func createSignUser(withBaseDomain domain: String?) {
        createUser()

        currentUser?.userName = ""
        currentUser?.userType = .loggedInSignUser
        currentUser?.urlBaseDomain = domain
        currentUser?.isRetainSigningOrderByDefault = true
        currentUser?.addMeAcionType = ZS_ActionType_View

        isUserExists = true
        ZSCoreDataManager.shared.truncateDB()
        saveUser(updateInServer: false)
    }

    public func createGuestSignUser(withBaseDomain domain: String?) {
        createUser()

        currentUser?.userName = "Guest"
        currentUser?.userType = .guestUser
        currentUser?.urlBaseDomain = domain
        currentUser?.dateFormat = "dd MMM yyyy"

        isUserExists = true
        
        ZSCoreDataManager.shared.truncateDB()
        saveUser(updateInServer: false)
    }

    
    
    public func deleteCurrentUser()  {
        KeychainManager.remove(key: kUserDefaults_UserDetails)
        
        isUserExists = false
        currentUser = nil
    }
    
    public func saveUser(updateInServer : Bool)
    {
        currentUser?.isUserProfileEdited    =   updateInServer;
        let encoder = JSONEncoder()
        guard let data = try? encoder.encode(currentUser) else {
            return
        }
        
        KeychainManager.save(key: kUserDefaults_UserDetails, data: data)
    }
    
    //MARK:- Update in server
    
    public func fetchCurrentUserDetailsFromServer() {
        weak var _self = self
        dataManager.getCurrentUserDetails(success: { (data) in
            if let dict = data as?  [String : AnyObject]{
                _self?.updateCurrentUser(dict: dict)
            }
            _self?.updateAccountSettingsFromServer()
        }) { (error) in
            
        }
    }

    public func updateProfileInServer()  {
        if currentUser == nil {
            return
        } else if currentUser?.userType == .guestUser {
            saveUser(updateInServer: false)
            return
        }

        var paramDict: [String : String] = [:]
        paramDict["company"] = currentUser?.company ?? ""
        paramDict["job_title"] = currentUser?.jobTitle ?? ""
        paramDict["date_format"] = currentUser?.dateFormat ?? ""

        if !ZSignKit.shared.isAppExtension {

            dataManager.updateUserProfileDetails(params: paramDict, progress: { (progress) in
                
            }, success: {[weak self] (responseObj) in
                self?.currentUser?.isUserProfileEdited = false
                self?.saveUser(updateInServer: false)
                NotificationCenter.default.post(name: NSNotification.Name(kUserProfileUpdationNotification), object: nil)
            }) {[weak self] (error) in
                self?.currentUser?.isUserProfileEdited = false
            }
         
        }
        
    }
    func updateAccountSettingsFromServer() {
        weak var _self = self

        dataManager.getUserAccountSettings(success: { (data) in
            if let dict = data as? [AnyHashable : Any]{
                _self?.updateAccountSettings(dict: dict)
            }

        }) { (error) in
            
        }
    }

    func updateAccountSettings(dict: [AnyHashable : Any]) {
        if (currentUser == nil) {
            return
        }
        guard let defaultSettingsDict = dict["request_default"] as? [AnyHashable : Any] else {
            return
        }

        
        currentUser?.accountSetting?.isAuthenticationModeMandatory = defaultSettingsDict[keyPath:"authentication_mandatory"] ?? false
        currentUser?.accountSetting?.isAuthenticationModeEmailEnabled = defaultSettingsDict[keyPath:"authentication_email"] ?? true
        currentUser?.accountSetting?.isAuthenticationModeSmsEnabled = defaultSettingsDict[keyPath:"authentication_sms"] ?? true
        currentUser?.accountSetting?.isAuthenticationModeOfflineEnabled = defaultSettingsDict[keyPath:"authentication_offline"] ?? true
        currentUser?.accountSetting?.expirationDays = defaultSettingsDict[keyPath:"expiration_days"] ?? "30"
        
        if let reminderSettings: [AnyHashable:Any] = dict[keyPath: "reminders_settings"] {
            currentUser?.accountSetting?.isAutomaticReminderOn = reminderSettings[keyPath: "email_reminders"] ?? true
            currentUser?.accountSetting?.reminderTimePeriod = reminderSettings[keyPath: "reminder_period"] ?? 2
        }
        
        saveUser(updateInServer: false)
    }

  
    public func updateCurrentUser(dict: [String : AnyObject]) {
        if currentUser == nil {
            return
        }

        
        currentUser?.firstName =  dict[keyPath:"first_name"]
        currentUser?.lastName = dict[keyPath:"last_name"]
        if let firstname =  currentUser?.firstName {
            if let lastname = currentUser?.lastName, !lastname.isEmpty {
                currentUser?.fullName =  "\(firstname) \(lastname)"
            }else{
                currentUser?.fullName = firstname
            }
        }

        currentUser?.userType = .loggedInSignUser
        currentUser?.customId = dict[keyPath:"ZSOID"]
        currentUser?.ZUID = dict[keyPath:"ZUID"]
        currentUser?.accountID = dict[keyPath:"account_id"]
        currentUser?.isAdmin = dict[keyPath:"is_admin"] ?? false
        currentUser?.timeZone = dict[keyPath:"time_zone"]
        currentUser?.email = dict[keyPath:"user_email"]
        currentUser?.userID = dict[keyPath:"user_id"]
        currentUser?.licenseType = dict[keyPath:"license_type"]
        if currentUser?.licenseType == nil {
            currentUser?.licenseType = "Free"
        }

        if let availableFeatures = dict["features"] as? [AnyHashable : Any] {
            currentUser?.isApproverFeatureAvailable = availableFeatures[keyPath: "approver"] ?? false
            currentUser?.isInPersonSigningAvailable = availableFeatures[keyPath: "in_person_signing"] ?? false
            currentUser?.isTemplatesAvailable = availableFeatures[keyPath: "templates"] ?? false
            currentUser?.isBulkSendAvailable = availableFeatures[keyPath: "bulk_send"] ?? false
            currentUser?.isReportsAvailable = availableFeatures[keyPath: "reports"] ?? false
            currentUser?.isAttachmentFeatureAvailable = availableFeatures[keyPath:"attachment_field"] ?? false
        }else{
            currentUser?.isApproverFeatureAvailable =  false
            currentUser?.isInPersonSigningAvailable = false
            currentUser?.isTemplatesAvailable =  false
            currentUser?.isBulkSendAvailable = false
            currentUser?.isReportsAvailable =  false
            currentUser?.isAttachmentFeatureAvailable = false
        }
        
        

        if  let profileDict = dict["profile_details"] as? [AnyHashable : Any] {
            currentUser?.company = profileDict[keyPath:"company"]
            currentUser?.jobTitle = profileDict[keyPath:"job_title"]
            if let image = SwiftUtils.decodeBase64ToImage(encodeStr: profileDict[keyPath:"signature"]){
                currentUser?.saveSignatureImage(image)
            }
            if let image = SwiftUtils.decodeBase64ToImage(encodeStr: profileDict[keyPath:"initial"]){
                currentUser?.saveinitialImage(image)
            }
            currentUser?.dateFormat = profileDict[keyPath:"date_format"] ?? DateManager.availableDateFormats()[2]
            currentUser?.timeZone = profileDict[keyPath:"time_zone"] ?? TimeZone.current.identifier
        }


        if currentUser?.accountSetting == nil {
            //Default Account settings fix
            currentUser?.accountSetting = AccountSettings()
        }

        currentUser?.numberOfUsedDocuments = dict[keyPath:"documents_used"]
        currentUser?.numberOfTotalDocumentsQouta = dict[keyPath:"no_of_documents"]


        isUserExists = true

        saveUser(updateInServer: false)
    }
    
    public func saveEditedDict(_ dict: [AnyHashable : Any]?, forRequest requestId: String) {
        
        guard let user = currentUser, user.userType == .loggedInSignUser else {
            return
        }

        var reqDict : [AnyHashable : Any] = NSKeyedUnarchiver.unarchiveObject(with: currentUser?.temporaryEditedRequestFields ?? Data()) as? [AnyHashable : Any] ?? [:]
        reqDict[requestId] = dict

        currentUser?.temporaryEditedRequestFields = NSKeyedArchiver.archivedData(withRootObject: reqDict)
        
        saveUser(updateInServer: false)
    }

    public func getSavedFieldsDict(forRequest requestId: String) -> Any? {
        guard let reqDict = NSKeyedUnarchiver.unarchiveObject(with: currentUser?.temporaryEditedRequestFields ?? Data()) as? [AnyHashable : Any] else {
            return nil
        }
        
        return reqDict[requestId]
    }
    
}

