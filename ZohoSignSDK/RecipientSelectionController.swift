//
//  RecipientSelectionController.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 06/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

//
//  ZSRequesterSelectionController.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 26/02/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit


class RecipientSelectionController: UIViewController , UITableViewDataSource, UITableViewDelegate {
    
    var menuTableView : UITableView = UITableView()
    var menuItemArray : [ZSRecipient] = []
    var selectedRecipient : ZSRecipient?

    var selectedIndex           = -1
    var isTemplateSelectionList = false
    
    public var didChangeItemHandler : ((Int) -> Void)?
    
    
    public init(title: String , menuItems: [ZSRecipient] , selectedItem: ZSRecipient) {
        super.init(nibName: nil, bundle: nil)
        self.title = title
        self.menuItemArray  =   menuItems
        self.selectedRecipient    =   selectedItem
        initateSetup()
    }
    
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
 
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Private
    
    func initateSetup()  {
       
//        let numberOfRows : CGFloat = ((menuItemArray.count > 6) ? 6 : CGFloat(menuItemArray.count))
//
//        var tableHeight : CGFloat = (TableviewCellSize.receipientList *  numberOfRows ) + kNavbarHeight + ((menuItemArray.count > 5) ? 15 : 0)
    
        
      
        menuTableView.frame                             =    view.bounds
        menuTableView.autoresizingMask                  =   .flexibleWidth
        menuTableView.delegate                          =    self
        menuTableView.dataSource                        =    self
        menuTableView.rowHeight                         =    TableviewCellSize.receipientList
        menuTableView.setSeparatorStyle()
        menuTableView.showsVerticalScrollIndicator      =    false
        menuTableView.backgroundColor                   =   ZColor.bgColorWhite;
        view.addSubview(menuTableView)
        
//        menuTableView.tableHeaderView    =    headerView
        
    }
//
    func getViewControllerHeight() -> CGFloat {
        return (TableviewCellSize.receipientList * CGFloat(menuItemArray.count)) + (isTemplateSelectionList ? -15 : 0)
    }

    
    // MARK: - TableView Data Source
    public func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return menuItemArray.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let recipient =  menuItemArray[indexPath.row]
        if (recipient.isTemplateUser) { return 60;}
        
        return TableviewCellSize.receipientList
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let identifier = "Cell"
        
        var cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        let recipient =  menuItemArray[indexPath.row]

        if cell == nil {
            cell =  UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: identifier)

            
            //Selected bgview
            let selectedView = UIView()
            selectedView.backgroundColor = ZColor.listSelectionColor;
            cell.selectedBackgroundView = selectedView;
        }
        var recipientName  = recipient.userRole
        if ((recipientName == nil) || (recipientName == "")){
            if let recepName = recipient.name, !recepName.isEmpty{
                recipientName = String(format: "%@ (%@)", recepName,recipient.email ?? "")
            }else{
                recipientName = recipient.email
            }
        }
        
        
        cell.textLabel?.text            =    recipientName
        cell.textLabel?.font            =    ZSFont.listTitleFont()
        cell.detailTextLabel?.font      =    ZSFont.listSubTitleFont()

        cell.textLabel?.textColor    =       ZColor.primaryTextColor
        cell.detailTextLabel?.textColor =    ZColor.secondaryTextColorDark
        
      
        if recipient.isTemplateUser {
            isTemplateSelectionList = true
            if (selectedRecipient?.isTemplateUser == true){
                cell.backgroundColor  = ZColor.listSelectionColor.withAlphaComponent(0.4)
            }
            return cell;
        }
        
        
        cell.contentView.alpha = 1
        cell.isUserInteractionEnabled  = true
        switch recipient.actionType {
            case ZS_ActionType_Sign:
                cell.detailTextLabel?.text =  "sign.mobile.recipient.signer".ZSLString
            case ZS_ActionType_Approver:
                cell.detailTextLabel?.text =  "sign.mobile.recipient.approver".ZSLString
                cell.contentView.alpha = 0.5
                cell.isUserInteractionEnabled  = false
            case ZS_ActionType_InPerson:
                cell.detailTextLabel?.text =  "sign.mobile.common.inpersonSigner".ZSLString
                var inpersonName  =  recipient.userRole
                if ((inpersonName == nil) || (inpersonName == "")){
                    inpersonName = recipient.inPersonName
                }
                if ((inpersonName == nil) || (inpersonName == "")){
                    inpersonName = recipient.inPersonEmail
                }
                if ((inpersonName == nil) || (inpersonName == "")){
                    inpersonName = String(format:"%@: %@","sign.mobile.recipient.host".ZSLString,recipient.name ?? "")
                }
                if ((inpersonName == nil) || (inpersonName == String(format:"%@: ","sign.mobile.recipient.host".ZSLString))){
                    inpersonName = String(format:"%@: %@","sign.mobile.recipient.host".ZSLString,recipient.email ?? "")
                }
                cell.textLabel?.text          =    inpersonName
            default:
                cell.detailTextLabel?.text =  "sign.mobile.recipient.getsACopy".ZSLString
                cell.contentView.alpha = 0.5
                cell.isUserInteractionEnabled  = false
                break
        }
        
        
        if ((selectedRecipient?.actionId != nil) && (selectedRecipient?.actionId == recipient.actionId))
        {
            cell.accessoryType = .checkmark
            cell.backgroundColor  = ZColor.listSelectionColor.withAlphaComponent(0.4)
        }
        else
        {
            cell.accessoryType = .none
            cell.backgroundColor  = ZColor.bgColorWhite
        }
        
      
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedIndex != indexPath.row {
            selectedIndex = indexPath.row
        }
        didChangeItemHandler?(selectedIndex)
        dismiss(animated: true, completion: nil)
    }
    
    
}


extension RecipientSelectionController: ZPresentable {
    var navBar: PresentationNavBar{
        return PresentationNavBar(size: PresentationBarSize.withTitleBig, title: title, titleAlignment: .center)
    }
    
    var popupHeight: CGFloat{
        return getViewControllerHeight() + navBar.size + bottomMargin
    }
    
    var direction: PresentationDirection{
        return .bottomWithCustomHeight
    }
}
