//
//  ZSCoreDataManager+Template.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 02/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import CoreData

extension ZSCoreDataManager {
    
    //MARK: CREATE
    public func addDraftTemplate(fromDict dict: ResponseDictionary) -> Template? {
        var aTemplate: Template? = nil
        managedObjContext.performAndWait({
            aTemplate = Template(context: managedObjContext)
            updateTemplate(aTemplate!, dict: dict, isFromList: false)
            save()
        })

        return aTemplate
    }
    
    public func addTemplatesList(fromResponseObject object: Any?, shouldRemovePreviousElements shouldRemove: Bool) {
        
        guard let dictArray = object as? [ResponseDictionary] else {return}
        
        var templateIdSet = Set<String>()
        
        managedObjContext.performAndWait {
            dictArray.forEach { (dict) in
                guard let templateId: String = dict[keyPath: "template_id"] else {return}
                
                let templateFetchRequest: NSFetchRequest<Template> = Template.fetchRequest()
                templateFetchRequest.predicate = NSPredicate(format: "templateId == %@", templateId)
                templateIdSet.insert(templateId)
                
                let template: Template
                
                if let request = try? managedObjContext.fetch(templateFetchRequest).first {
                    template = request
                } else {
                    template = Template(context: managedObjContext)
                }
                
                updateTemplate(template, dict: dict, isFromList: true)
             }
            
            if shouldRemove {
                let removePredicate = NSPredicate(format: "(NOT templateId IN %@)", templateIdSet)
                
                fetchData(withSortDescriptors: nil, forPredicate: removePredicate).safelyUnwrap { (templates: [Template]) in
                    templates.forEach{ managedObjContext.delete($0) }
                }
            }
            
            save()
        }
    }
    
    public func addSearchTemplates(fromResponseObject object: Any?) {
        
        guard let dictArray = object as? [ResponseDictionary] else {return}
        
        managedObjContext.performAndWait {
            let filterFetchRequest: NSFetchRequest<TemplateFilter> = TemplateFilter.fetchRequest()
            filterFetchRequest.predicate = NSPredicate(format: "filterKey == %@", "Search")
            
            do {
                let filter = try managedObjContext.fetch(filterFetchRequest).first
                filter?.templates = nil
                
                dictArray.forEach { (dict) in
                    guard let templateId: String = dict[keyPath: "template_id"] else {return}
                    
                    let templateFetchRequest: NSFetchRequest<Template> = Template.fetchRequest()
                    templateFetchRequest.predicate = NSPredicate(format: "templateId == %@", templateId)
                    
                    let template: Template
                    if let request = try? managedObjContext.fetch(templateFetchRequest).first {
                        template = request
                    } else {
                        template = Template(context: managedObjContext)
                    }
                    updateTemplate(template, dict: dict, isFromList: true)
                    filter?.addToTemplates(template)
                }
                save()
            }catch {
                ZError(error.localizedDescription)
            }
        }
    }
    
    //MARK: GET
    public func getTemplate(templateId: String) -> Template? {
        var aTemplate : Template?
        do {
            let fetchRequest : NSFetchRequest<Template> = Template.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "templateId == %@", templateId)
            
            let fetchedResults = try managedObjContext.fetch(fetchRequest)
            if let aDoc = fetchedResults.first {
                aTemplate = aDoc
            }else{
                aTemplate = Template(context: managedObjContext)
            }
            aTemplate?.templateId = templateId
        }
        catch{ }
        
        return aTemplate
    }
    
    //MARK: UPDATE
    func updateTemplate(withId templateId: String, fromDict dict: ResponseDictionary) {
        managedObjContext.performAndWait {
            let templateFetchRequest: NSFetchRequest<Template> = Template.fetchRequest()
            templateFetchRequest.predicate = NSPredicate(format: "templateId == %@", templateId)
            
            let template: Template
            if let request = try? managedObjContext.fetch(templateFetchRequest).first {
                template = request
            } else {
                template = Template(context: managedObjContext)
            }
            
            updateTemplate(template, dict: dict, isFromList: false)
        }
    }
    
    func updateTemplate(_ template : Template, dict : ResponseDictionary, isFromList : Bool = false)  {
        let arrayData = NSKeyedArchiver.archivedData(withRootObject: dict["actions"])
        template.actions = arrayData

        if let fieldsDict = dict["document_fields"] {
            let documentFields = NSKeyedArchiver.archivedData(withRootObject: fieldsDict)
            template.documentFields = documentFields
        }

        template.createdTime    = dict[keyPath: "created_time"]
        template.desc           = dict[keyPath: "description"]
        template.expiryDate     = dict[keyPath: "expire_by"]
        template.modifiedTime   = dict[keyPath: "modified_time"]
        template.actionTime     = dict[keyPath: "action_time"] ?? 0
        template.notes          = dict[keyPath: "notes"]
        template.folderId       = dict[keyPath: "folder_id"]
        template.folderName     = dict[keyPath: "folder_name"]
        template.ownerEmail     = dict[keyPath:"owner_email"]
        template.ownerFirstname = dict[keyPath:"owner_first_name"]
        template.ownerLastname  = dict[keyPath:"owner_last_name"]
        template.ownerId        = dict[keyPath:"owner_id"]
        template.templateId     = dict[keyPath:"template_id"]
        template.name           = dict[keyPath:"template_name"]
        template.typeId         = dict[keyPath:"request_type_id"]
        template.typeName       = dict[keyPath:"request_type_name"]
        template.validity       = dict[keyPath:"validity"]
        template.isSequential   = dict[keyPath:"is_sequential"] ?? false
        template.timeToComplete = dict[keyPath:"expiration_days"]
        template.isAutomaticReminderOn = dict[keyPath: "email_reminders"] ?? true
        template.reminderTimePeriod = dict[keyPath: "reminder_period"] ?? 5
        
        DateManager.formatTableviewHeader(template.modifiedTime, setTodayasNil: false).safelyUnwrap { (num) in
            if template.sectionIdentifier != num.stringValue {
                template.sectionIdentifier = num.stringValue
            }
        }

        template.documents = nil
        if let  docDictArr = dict["document_ids"] as? [ResponseDictionary]  {
            docDictArr.forEach { (docDict) in
                if let docId = docDict["document_id"] as? String{
                    let doc = addDocument(withDocumentId: docId, andDataDict: docDict)
                    template.addToDocuments(doc)
                    if let imageStr = doc.thumbnailStr, doc.documnetOrder == 0 {
                        template.thumbnailPath = thumbnailFolderPath + docId + ".jpg"
                        template.thumbnailStr = imageStr
                    }
                }
            }
        }
    }
    
    //MARK: DELETE
    func deleteTemplate(withIds templateIds : String...)  {
        templateIds.forEach { (templateId) in
            let fetchRequest : NSFetchRequest<Template> = Template.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "templateId == %@", templateId)

            do{
               let fetchedResults = try managedObjContext.fetch(fetchRequest)
               for object in fetchedResults {
                   managedObjContext.delete(object)
               }
            }
            catch{ }
        }
        
        coreDataDefault.saveContext()
    }
}
