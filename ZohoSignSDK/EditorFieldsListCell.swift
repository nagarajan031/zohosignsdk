//
//  EditorFieldsListCell.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 21/09/17.
//  Copyright © 2017 Zoho Corporation. All rights reserved.
//

import UIKit
import SnapKit

class EditorFieldsListCell: UITableViewCell {

    var fieldNameLabel       = UILabel()
    var recipientfieldNameLabel    = UILabel()
    var pageNumLabel    = UILabel()
    var iconImageView   = UIImageView()
    
    let resetBtnWidth = 50
    
    private lazy var resetButton: ZSButton = {
        let view = ZSButton()
        view.setImage(ZSImage.tintColorImage(withNamed: "close_icon", col: ZColor.whiteColor), for: .normal)
        view.backgroundColor = ZColor.redColor
//        view.setTitleColor(ZColor.whiteColor, for: .normal)
//        view.setTitle("Reset", for: .normal)
//        view.titleEdgeInsets = UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 2)
        view.addTarget(target: self, selector: #selector(resetButtonClicked(_:)))
//        view.layer.cornerRadius = 35/2
        view.clipsToBounds = true
        view.contentMode = .center
        view.alpha = 0
        view.isHidden = true
        return view
    }()
    
    private var resetButtonTrailingConstraint: Constraint?
    var didShowRecipientName  = false
    
    var resetField: (()->())?
    var editorType :  ZSDocumentCreatorType = .SIGN_AND_SEND
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        iconImageView.backgroundColor = .clear
        iconImageView.frame = CGRect(x: 15, y: 10, width: 22, height: 22);
        iconImageView.contentMode  =   .scaleToFill
        self.contentView.addSubview(iconImageView)
        
        fieldNameLabel.frame = CGRect(x: 42, y: 11, width: self.bounds.size.width - 32, height: 20)
        fieldNameLabel.font  =   ZSFont.listTitleFont()
        fieldNameLabel.textColor  =  ZColor.primaryTextColor
        contentView.addSubview(fieldNameLabel)
        
        pageNumLabel.frame  = CGRect(x: 42, y: self.bounds.size.height - 30, width: self.bounds.size.width - 32, height: 20)
        pageNumLabel.autoresizingMask = [.flexibleWidth,.flexibleTopMargin]
        pageNumLabel.font   =   ZSFont.listSubTitleFont()
        pageNumLabel.textColor  =  ZColor.secondaryTextColorDark
        contentView.addSubview(pageNumLabel)
        
        recipientfieldNameLabel.frame = CGRect(x: 42, y: self.bounds.size.height - 52, width: self.bounds.size.width - 32, height: 20)
        recipientfieldNameLabel.autoresizingMask = [.flexibleWidth,.flexibleTopMargin]
        recipientfieldNameLabel.font   =   ZSFont.listSubTitleFont()
        recipientfieldNameLabel.isHidden    =   true
        recipientfieldNameLabel.textColor  =  ZColor.secondaryTextColorDark
        contentView.addSubview(recipientfieldNameLabel)
        
        contentView.addSubview(resetButton)
        resetButton.snp.makeConstraints { (make) in
            resetButtonTrailingConstraint = make.trailing.equalToSuperview().offset(resetBtnWidth).constraint
            make.centerY.equalToSuperview()
            make.width.equalTo(resetBtnWidth)
            make.height.equalToSuperview()
        }

//        let separatorLine = UIView()
//        separatorLine.frame = CGRect(x: 42, y: self.bounds.size.height - 0.5, width: self.bounds.size.width-15, height: 0.5)
//        separatorLine.autoresizingMask  =   [.flexibleTopMargin,.flexibleWidth]
//        separatorLine.backgroundColor = ZColor.seperatorColor
//        self.contentView.addSubview(separatorLine)
        
        let selectedView = UIView()
        selectedView.backgroundColor = ZColor.listSelectionColor
        self.selectedBackgroundView = selectedView
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        self.imageView?.transform = CGAffineTransform.init(scaleX: 0.8, y: 0.8)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    public func updateCell(shouldReset: Bool, field: SignField){
        if shouldReset {
            if (field.isCompleted && !field.isReadOnly && (field.type != .attachment)){
                accessoryType = .none
                addResetButton()
            } else {
                contentView.alpha = 0.4
                isUserInteractionEnabled = false
            }
        } else {
            removeResetButton(){[weak self] in
                self?.contentView.alpha = 1
                self?.isUserInteractionEnabled = true
                if field.isCompleted {
                    self?.accessoryType = .checkmark
                }
            }
        }
    }
    
    public func addResetButton(){
        resetButton.isHidden = false
        resetButtonTrailingConstraint?.update(offset:0)
        UIView.animate(withDuration: 0.3) {
            self.resetButton.alpha = 1
            self.contentView.layoutIfNeeded()
        }
    }
    
    public func removeResetButton(completion: @escaping ()->()){
        resetButtonTrailingConstraint?.update(offset: resetBtnWidth)
        UIView.animate(withDuration: 0.3, animations: {
            self.resetButton.alpha = 0
            self.contentView.layoutIfNeeded()
        }) { (_) in
            self.resetButton.isHidden = true
            completion()
        }
    }
    
    @objc private func resetButtonClicked(_ sender: ZSButton){
        removeResetButton {}
        resetField?()
    }
    
    func setSignField(_ field:SignField)
    {
        recipientfieldNameLabel.isHidden    =   !didShowRecipientName
        fieldNameLabel.text  =   field.fieldName
        
        let fieldName = field.fieldName ?? ""
        
        accessoryType = .none

        if (editorType != .SIGN_AND_SEND) {
        
            if field.isReadOnly{
                fieldNameLabel.text  =  String(format: "%@ (%@)", fieldName,"sign.mobile.common.readonly".ZSLString)
            }
            else if !field.isMandatory{
                fieldNameLabel.text  =  String(format: "%@ (%@)", fieldName,"sign.mobile.common.optional".ZSLString)
            }
            
            /*
            if field.isCompleted{
                accessoryType = .checkmark
            }
            */
        }
        pageNumLabel.text  =  String(format: "%@: %@", "sign.mobile.common.page".ZSLString,String(field.pageIndex+1))
        if field.recipientName == "sign.mobile.create.prefillByYou".ZSLString {
            recipientfieldNameLabel.text  =  field.recipientName
        }else{
            recipientfieldNameLabel.text  =  String(format:"%@ %@","sign.mobile.common.mailTo".ZSLString,field.recipientName ?? "")
        }
        iconImageView.image = CommonUtils.getFieldTypeIcon(fieldType: field.type ?? .textBox, color: ZColor.secondaryTextColorDark)
    }
}
