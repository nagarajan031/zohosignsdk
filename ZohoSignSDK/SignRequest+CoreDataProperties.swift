//
//  SignRequest+CoreDataProperties.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 10/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//
//

import Foundation
import CoreData


extension SignRequest {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SignRequest> {
        return NSFetchRequest<SignRequest>(entityName: "SignRequest")
    }

    @NSManaged public var actions: Data?
    @NSManaged public var actionTime: Int64
    @NSManaged public var createdTime: NSNumber?
    @NSManaged public var declinedReason: String?
    @NSManaged public var desc: String?
    @NSManaged public var expiryDate: NSNumber?
    @NSManaged public var folderId: String?
    @NSManaged public var folderName: String?
    @NSManaged public var isAutomaticReminderOn: Bool
    @NSManaged public var isDeletedRequest: Bool
    @NSManaged public var isExpiring: NSNumber?
    @NSManaged public var isInProgress: NSNumber?
    @NSManaged public var isSelfSign: NSNumber?
    @NSManaged public var isSequential: NSNumber?
    @NSManaged public var modifiedTime: NSNumber?
    @NSManaged public var name: String?
    @NSManaged public var notes: String?
    @NSManaged public var ownerEmail: String?
    @NSManaged public var ownerFirstname: String?
    @NSManaged public var ownerId: String?
    @NSManaged public var ownerLastname: String?
    @NSManaged public var reminderTimePeriod: Int64
    @NSManaged public var requestId: String?
    @NSManaged public var sectionIdentifier: String?
    @NSManaged public var signPercentages: NSNumber?
    @NSManaged public var signSubmissionTime: Int64
    @NSManaged public var status: String?
    @NSManaged public var thumbnailPath: String?
    @NSManaged public var thumbnailStr: String?
    @NSManaged public var timeToComplete: String?
    @NSManaged public var totalPages: NSNumber?
    @NSManaged public var typeId: String?
    @NSManaged public var typeName: String?
    @NSManaged public var validity: NSNumber?
    @NSManaged public var documents: Set<Documents>?
    @NSManaged public var filters: Set<Filter>?

}

// MARK: Generated accessors for documents
extension SignRequest {

    @objc(addDocumentsObject:)
    @NSManaged public func addToDocuments(_ value: Documents)

    @objc(removeDocumentsObject:)
    @NSManaged public func removeFromDocuments(_ value: Documents)

    @objc(addDocuments:)
    @NSManaged public func addToDocuments(_ values: NSSet)

    @objc(removeDocuments:)
    @NSManaged public func removeFromDocuments(_ values: NSSet)

}

// MARK: Generated accessors for filters
extension SignRequest {

    @objc(addFiltersObject:)
    @NSManaged public func addToFilters(_ value: Filter)

    @objc(removeFiltersObject:)
    @NSManaged public func removeFromFilters(_ value: Filter)

    @objc(addFilters:)
    @NSManaged public func addToFilters(_ values: NSSet)

    @objc(removeFilters:)
    @NSManaged public func removeFromFilters(_ values: NSSet)

}
