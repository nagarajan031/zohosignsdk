//
//  ZSSignCaptureController.swift
//  CameraDemo
//
//  Created by somesh-pt2434 on 27/02/19.
//  Copyright © 2019 somesh-pt2434. All rights reserved.
//

import UIKit
import AVFoundation
import SnapKit

enum FlashMode {
    case on
    case off
}

@available(macCatalyst, unavailable)
public class ZSSignCaptureController: UIViewController {
    deinit {
        ZInfo("capture page deinit")
    }
    private lazy var cameraPreview: UIView = {
       let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height))
        return view
    }()
    
//    private lazy var roundedRectView: UIView = {
//       let view = UIView()
//        view.layer.borderColor = UIColor(white: 1, alpha: 0.5).cgColor
//        view.layer.borderWidth = 1
//        view.layer.cornerRadius = 5
//        return view
//    }()
    
    private lazy var controlsView: UIView = {
       let view = UIView()
        return view
    }()
    
//    private lazy var instructionLabel: UILabel = {
//      let view = UILabel()
//        view.font = ZSFont.regularFont(withSize: 15)
//        view.text = "sign.mobile.sign.capture.instructions".ZSLString
//        view.textColor = .white
//        view.backgroundColor = UIColor(white: 0, alpha: 0.5)
//        view.textAlignment = .center
//        view.clipsToBounds = true
//        view.layer.cornerRadius = 5
//        view.sizeToFit()
//        return view
//    }()
    
    private lazy var flashButton: UIButton = {
       let view = UIButton()
        view.setImage(UIImage(named: "icn_flash_off"), for: .normal)
        view.imageView?.alpha = 0.5
        view.layer.cornerRadius = 20
        view.backgroundColor = ZColor.bgColorDullBlack
        view.isHidden = true
        return view
    }()
    
    private lazy var closeButton: UIButton = {
       let view = UIButton()
        view.setImage(UIImage(named: "icn_close_camera"), for: .normal)
        view.layer.cornerRadius = 20
        view.backgroundColor = ZColor.bgColorDullBlack
        view.isHidden = true
        return view
    }()
    
    private lazy var blurView: UIVisualEffectView = {
        let blurEffect: UIBlurEffect
        if #available(iOS 13, *){
            blurEffect = UIBlurEffect(style: .systemUltraThinMaterial)
        } else {
            blurEffect = UIBlurEffect(style: .prominent)
        }

       let view = UIVisualEffectView(effect: blurEffect)
        view.frame = self.view.bounds
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        return view
    }()
    
    private var captureButtin: ZSCaptureButton = ZSCaptureButton(frame: CGRect.zero)
    private var captureSession: AVCaptureSession!
    private var cameraPreviewLayer: AVCaptureVideoPreviewLayer!
    private var stillImageOutput: AVCapturePhotoOutput!
    private var captureDevice: AVCaptureDevice!
    @objc public weak var delegate : SignatureCreatorDelegate?
    private var flashmode: FlashMode = .off
    
    let minimumZoom: CGFloat = 1.0
    let maximumZoom: CGFloat = 3.0
    var lastZoomFactor: CGFloat = 1.0
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: VIEW LIFECYCLE
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = ZColor.blackColor
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        var pinchView = UIView()

        view.addSubview(cameraPreview)
        view.addSubview(controlsView)
        view.addSubview(pinchView)
        //view.addSubview(roundedRectView)
       // view.addSubview(instructionLabel)

        controlsView.addSubview(captureButtin)
        controlsView.addSubview(flashButton)
        controlsView.addSubview(closeButton)

//        let instructionLabelSize = GlobalFunctions.size(forText: instructionLabel.text, sizeWith: instructionLabel.font, constrainedTo: self.view.bounds.size)
        
        pinchView.snp.makeConstraints { (make) in
            make.leading.bottom.top.equalToSuperview()
            make.trailing.equalTo(controlsView.snp.leading)
        }
        
        controlsView.snp.makeConstraints { (make) in
            make.centerY.height.equalToSuperview()
            make.width.equalTo(outerWhiteCircleSize + 20 + 10)
            make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing)
        }
        
        captureButtin.snp.makeConstraints { (make) in
            make.size.equalTo(outerWhiteCircleSize)
            make.center.equalToSuperview()
        }
        
        /*
        roundedRectView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading).offset(40)
            make.trailing.equalTo(controlsView.snp.leading).offset(-20)
            make.height.equalTo(roundedRectView.snp.width).multipliedBy(CGFloat(SIGNATURE_RATIO_HEIGHT) / CGFloat(SIGNATURE_RATIO_WIDTH))
        }
        
        instructionLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(roundedRectView)
            make.bottom.equalTo(roundedRectView.snp.top).offset(-24)
            make.width.equalTo(instructionLabelSize.width + 30)
            make.height.equalTo(instructionLabelSize.height + 16)
        }
        */
        
        flashButton.snp.makeConstraints { (make) in
            make.top.equalTo(captureButtin.snp.bottom).offset(70)
            make.centerX.equalToSuperview()
            make.size.equalTo(40)
        }

        closeButton.snp.makeConstraints { (make) in
            make.bottom.equalTo(captureButtin.snp.top).offset(-70)
            make.centerX.equalToSuperview()
            make.size.equalTo(flashButton)
        }
        
        let captureButtonTapGesture = UITapGestureRecognizer(target: self, action: #selector(captureImage))
        captureButtonTapGesture.delegate = self
        captureButtin.addGestureRecognizer(captureButtonTapGesture)
        
        let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(pinch(_:)))
        pinchView.addGestureRecognizer(pinchGestureRecognizer)
        //roundedRectView.addGestureRecognizer(pinchGestureRecognizer)
        
        flashButton.addTarget(self, action: #selector(flashButtonClicked), for: .touchUpInside)
        closeButton.addTarget(self, action: #selector(closeButtonClicked), for: .touchUpInside)
        
        self.view.addSubview(blurView)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 0.4, animations: {
            self.blurView.alpha = 0
        }) { (finished) in
            self.blurView.removeFromSuperview()
            guard let captureDevice = AVCaptureDevice.default(for: .video) else{
                ZInfo("Unable to acess camera")
                return
            }
            self.captureDevice = captureDevice
            self.lastZoomFactor = 1.0
            
            do{
                let input = try AVCaptureDeviceInput(device: self.captureDevice)
                
                if !captureDevice.hasTorch {
                    self.flashButton.isHidden = true
                    self.closeButton.snp.remakeConstraints({ (make) in
                        make.size.equalTo(self.flashButton)
                        make.centerX.equalToSuperview()
                        make.top.equalToSuperview().offset(50)
                    })
                } else {
                    self.flashButton.isHidden = false
                }
                self.closeButton.isHidden = false
                self.captureSession  = AVCaptureSession()
                self.stillImageOutput = AVCapturePhotoOutput()
                if self.captureSession.canAddInput(input) && self.captureSession.canAddOutput(self.stillImageOutput) {
                    self.captureSession.addInput(input)
                    self.captureSession.addOutput(self.stillImageOutput)
                }
                
                self.cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
                self.cameraPreviewLayer.videoGravity = .resizeAspectFill
                self.cameraPreviewLayer.frame = self.view.layer.bounds
                self.cameraPreviewLayer.connection?.videoOrientation = self.getVideoCaptureOrientation()
                self.cameraPreview.layer.addSublayer(self.cameraPreviewLayer)
                DispatchQueue.global(qos: .userInitiated).async {[weak self] in
                    self?.stillImageOutput.isHighResolutionCaptureEnabled = true
                    self?.captureSession.startRunning()
                    DispatchQueue.main.async {
                        self?.cameraPreviewLayer.frame = self?.view.bounds ?? .zero
                        self?.toggleFlash(self?.flashmode ?? .off)
                    }
                }
            }catch {
                ZInfo(error.localizedDescription)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(appEntersbackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appEntersforeground), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appResignActive), name: UIApplication.willResignActiveNotification, object: nil)
        addObserverForAVCaptureSessionWasInterrupted()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard captureDevice != nil else {
            return
        }
        do{
            try captureDevice.lockForConfiguration()
            captureDevice.videoZoomFactor = 1.0
            captureDevice.focusMode = .continuousAutoFocus
            captureDevice.unlockForConfiguration()
        }catch {
            
        }
        captureSession.stopRunning()
        toggleFlash(.off)
        NotificationCenter.default.removeObserver(self)
        
        blurView.alpha = 1
        self.view.addSubview(blurView)
    }
    
    public override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if cameraPreviewLayer != nil {
            cameraPreviewLayer.frame = self.view.bounds
            cameraPreviewLayer.connection?.videoOrientation = getVideoCaptureOrientation()
        }
        
    }
    
    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if DeviceType.isIphone {
            blurView.alpha = 1
            blurView.backgroundColor = .clear
            self.view.addSubview(self.blurView)
            
            coordinator.animate(alongsideTransition: {[weak self] (transition) in
                if self?.cameraPreviewLayer != nil  {
                    self?.cameraPreviewLayer.connection?.videoOrientation = self?.getVideoCaptureOrientation() ?? .portrait
                }
            }) {[weak self] (context) in
                UIView.animate(withDuration: 0.3, animations: {
                    self?.blurView.alpha = 0
                }, completion: { (finished) in
                    self?.blurView.removeFromSuperview()
                })
            }
        }
    }
   
    @objc func appEntersbackground(){
        toggleFlash(.off)
    }
    
    @objc func appEntersforeground(){
        toggleFlash(flashmode)
    }
    
    @objc func appResignActive(){
        toggleFlash(.off)
    }
    
    
    //MARK: ONLICK FUNCTIONS
    @objc private func flashButtonClicked(){
        SwiftUtils.generateNotificationFeedback(for: .success)
        if flashmode == .on {
            toggleFlash(.off)
            self.flashmode = .off
        } else {
            toggleFlash(.on)
            self.flashmode = .on
        }
    }
    
    @objc private func closeButtonClicked(){
        self.view.addSubview(blurView)
        blurView.alpha = 1
        blurView.backgroundColor = .clear
        self.dismiss(animated: true, completion: nil)
    }
    
    func toggleFlash(_ flashmode: FlashMode) {
        guard self.captureDevice != nil else {
            return
        }
        guard captureDevice.hasTorch else { return }
        
        do {
            try captureDevice.lockForConfiguration()
            if flashmode == .on {
                captureDevice.torchMode = .on
                flashButton.setImage(UIImage(named: "icn_flash_on"), for: .normal)
                flashButton.imageView?.alpha = 1
            } else {
                captureDevice.torchMode = .off
                flashButton.setImage(UIImage(named: "icn_flash_off"), for: .normal)
                flashButton.imageView?.alpha = 0.5
            }
            
            captureDevice.unlockForConfiguration()
        } catch {
            ZInfo(error)
        }
    }
    
    func addObserverForAVCaptureSessionWasInterrupted(){
        NotificationCenter.default.addObserver(forName: NSNotification.Name.AVCaptureSessionWasInterrupted, object: nil, queue: .main) {[weak self] (notification) in
            guard let userInfo = notification.userInfo else
            {
                return
            }
            if let interruptionReason = userInfo[AVCaptureSessionInterruptionReasonKey], Int(truncating: interruptionReason as! NSNumber) == AVCaptureSession.InterruptionReason.videoDeviceNotAvailableWithMultipleForegroundApps.rawValue
            {
                self?.showAlert(withMessage: "sign.mobile.alert.unableToUseCamerainMultiscreenMode".ZSLString)
            }
        }
    }
    
    
    //MARK: GESTURE RECOGNISER FUNCTIONS
    @objc private func captureImage() {
        var photoSettings: AVCapturePhotoSettings //= AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        photoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey : stillImageOutput.availablePhotoCodecTypes.contains(.hevc) ? .hevc : AVVideoCodecType.jpeg])
        photoSettings.isAutoStillImageStabilizationEnabled = true
        photoSettings.isHighResolutionPhotoEnabled = true
        // photoSettings.flashMode = flashmode == .on ? .on : .off
        if let photoOutputConnection = stillImageOutput.connection(with: .video) {
            photoOutputConnection.videoOrientation = getVideoCaptureOrientation()
        }
        
        stillImageOutput.capturePhoto(with: photoSettings, delegate: self)
    }

    
    @objc func pinch(_ pinch: UIPinchGestureRecognizer) {

        func minMaxZoom(_ factor: CGFloat) -> CGFloat {
            return min(min(max(factor, minimumZoom), maximumZoom), captureDevice.activeFormat.videoMaxZoomFactor)
        }

        func update(scale factor: CGFloat) {
            do {
                try captureDevice.lockForConfiguration()
                defer { captureDevice.unlockForConfiguration() }
                captureDevice.videoZoomFactor = factor
            } catch {
                ZInfo("\(error.localizedDescription)")
            }
        }

        let newScaleFactor = minMaxZoom(pinch.scale * lastZoomFactor)

        switch pinch.state {
        case .began: fallthrough
        case .changed: update(scale: newScaleFactor)
        case .ended:
            lastZoomFactor = minMaxZoom(newScaleFactor)
            update(scale: lastZoomFactor)
        default: break
        }
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        let touchPoint = touches.first!
        let screenSize = cameraPreview.bounds.size
        let focusPoint = CGPoint(x: touchPoint.location(in: cameraPreview).y / screenSize.height, y: 1.0 - touchPoint.location(in: cameraPreview).x / screenSize.width)
        
        if captureDevice != nil {
            do{
                try  captureDevice.lockForConfiguration()
                if captureDevice.isFocusPointOfInterestSupported {
                    captureDevice.focusPointOfInterest = focusPoint
                    captureDevice.focusMode = .continuousAutoFocus
                } else if captureDevice.isExposurePointOfInterestSupported {
                    captureDevice.exposurePointOfInterest = focusPoint
                    captureDevice.exposureMode = .continuousAutoExposure
                }
                captureDevice.unlockForConfiguration()
            }catch {
                ZInfo(error.localizedDescription)
            }
            
        }
    }
}

@available(macCatalyst, unavailable)
extension ZSSignCaptureController: AVCapturePhotoCaptureDelegate {
    public func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        captureSession.stopRunning()
        guard error == nil else {
            ZInfo("Fail to capture photo: \(String(describing: error))")
            return
        }
        guard let imageData = photo.fileDataRepresentation() else {
            ZInfo("Fail to convert pixel buffer")
            return
        }
        let orgImage : UIImage = UIImage(data: imageData)!
        
        let cropController = SignatureEditorViewController(image: orgImage)
        cropController.delegate = self
        self.navigationController?.pushViewController(cropController, animated: true)
      
    }
}

extension UIImage {
    var noir: UIImage? {
        let context = CIContext(options: nil)
        guard let currentFilter = CIFilter(name: "CIPhotoEffectNoir") else { return nil }
        currentFilter.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        if let output = currentFilter.outputImage,
            let cgImage = context.createCGImage(output, from: output.extent) {
            return UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
        }
        return nil
    }
}

@available(macCatalyst, unavailable)
extension ZSSignCaptureController {
    func getVideoCaptureOrientation() ->AVCaptureVideoOrientation {
        switch UIApplication.shared.statusBarOrientation {
        case .landscapeLeft:
            return .landscapeLeft
        case .landscapeRight:
            return .landscapeRight
        case .portrait:
            return .portrait
        case .portraitUpsideDown:
            return .portraitUpsideDown
        case .unknown:
            return .landscapeRight
        @unknown default:
            return .portrait
        }
    }
}

@available(macCatalyst, unavailable)
extension ZSSignCaptureController: UIGestureRecognizerDelegate{
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension UIImage {
    func rotate(radians: Float) -> UIImage {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()
        
        // Move origin to middle
        context?.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context?.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if let newImage = newImage {
            return newImage
        }
        return self
    }
    
    func getOrientationFixedUIImage() -> UIImage{
        switch UIApplication.shared.statusBarOrientation {
        case .portrait, .portraitUpsideDown, .landscapeLeft:
            return self.rotate(radians: .pi * 2)
        default:
            return self
        }
    }
}

extension UIImageView {
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

/*
@available(macCatalyst, unavailable)
extension ZSSignCaptureController: TOCropViewControllerDelegate {
    public func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        ZSProgressHUD.show(inView: cropViewController.view, status: "sign.mobile.activityIndicator.processing".ZSLString)
        let imageProcessor = ZSSignatureImageProcessor()
        imageProcessor.image = SwiftUtils.generateContrastImage(2.0, image)
        imageProcessor.removeBackground(brightnessFactor: 1.0, thresshold: DeviceType.isIpad ? 70  : 40) {[weak self] image in
            self?.delegate?.signatureCreated(forImage: image)
            cropViewController.dismiss(animated: true){
                ZSProgressHUD.dismiss()
            }
        }
    }
}
*/

@available(macCatalyst, unavailable)
extension ZSSignCaptureController: SignatureCreatorDelegate {
    public func signatureCreated(signImage: UIImage) {
        delegate?.signatureCreated(signImage: signImage)
        dismiss(animated: true, completion: nil)
    }
}
