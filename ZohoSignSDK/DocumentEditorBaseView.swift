//
//  DocumentEditorView.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 30/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

let resizeableView_OuterMargin : CGFloat = 12
let  resizeableView_TotalMargin : CGFloat =  24

public protocol DocumentEditorBaseViewDelegate : class{
    func editorBaseViewFieldAdded(editorView: DocumentEditorBaseView, field: SignField)
    func editorBaseViewFieldDeleted(editorView: DocumentEditorBaseView?, field: SignField)
    
    func editorBaseViewFieldDetailsUpdated(editorView: DocumentEditorBaseView, field: SignField)
    func editorBaseViewBeginEditing(editorView: DocumentEditorBaseView, field: SignField)
    func editorBaseViewFieldValueChanged(editorView: DocumentEditorBaseView, field: SignField)

    
    func editorBaseViewSignatureButtonTapped(editorView: DocumentEditorBaseView,signView: ZResizeView, type: ZSignFieldSignatureType)
    func editorBaseViewAttachmentFieldTapped(editorView: DocumentEditorBaseView, signView: ZResizeView)
}

public class DocumentEditorBaseView: UIImageView {

    //MARK: - variables
    let  transitionManager = PresentationManager()
//    weak var fieldsListArray : [SignField]!
//    var deletedFieldsListArray : [SignField]!
    
    public var currentEditingType : ZSDocumentViewMode = .receivedDoc_view
    var isSampleSigning : Bool = false
    
    public var docId : String!
    public var pageIndex : Int = 0
    public var docOrder : Int = 0
    
    public var isEditingMode : Bool = false
    var selectedField : SignField?
    var currentSelectedView : ResizableView?
    
    var isThirdPartySDK = false
    var isTemplateDocumentFields = false
    public var currentInpersonSigner : ZSRecipient?

    public weak var delegate : DocumentEditorBaseViewDelegate?
    public weak var superVC : UIViewController?
    
    let slideInTransitioningDelegate = ZPresentationManager()


    var lastTouchPoint : CGPoint = .zero
    public var pdfPageSize  = CGSize.zero

    
    lazy var signatureManager : SignatureManager = {
        let manager = SignatureManager(rootViewController: nil, signatureType: .signature)
        manager.delegate = self
        return manager
    }()
    
    lazy var dateFieldFormatter : DateFormatter = {
        let formatter          = DateFormatter()
        formatter.dateFormat = UserManager.shared.currentUser?.dateFormat
        formatter.locale = Locale.init(identifier: "en")
        return formatter
    }()
    
    lazy var horizontalGridLinesLayer : CAShapeLayer = {
        let horizontalGridLinesLayer = CAShapeLayer()
        layer.addSublayer(horizontalGridLinesLayer)
        horizontalGridLinesLayer.opacity = 0
        horizontalGridLinesLayer.strokeColor = UIColor.lightGray.cgColor
        horizontalGridLinesLayer.lineWidth = 0.5
        horizontalGridLinesLayer.fillColor = nil
        horizontalGridLinesLayer.zPosition = 1000
        horizontalGridLinesLayer.lineDashPattern = [NSNumber(value: 7), NSNumber(value: 3)] // 7 is the length of dash, 3 is length of the gap.
        return horizontalGridLinesLayer
    }()
    
    lazy var verticalGridLinesLayer : CAShapeLayer = {
        layer.borderColor = ZColor.seperatorColor.cgColor
        layer.borderWidth = 0.5


        let verticalGridLinesLayer = CAShapeLayer()
        verticalGridLinesLayer.strokeColor = UIColor.lightGray.cgColor
        verticalGridLinesLayer.lineWidth = 0.5
        verticalGridLinesLayer.fillColor = nil
        verticalGridLinesLayer.zPosition = 1001
        verticalGridLinesLayer.lineDashPattern = [NSNumber(value: 7), NSNumber(value: 3)] // 7 is the length of dash, 3 is length of the gap.
        layer.addSublayer(verticalGridLinesLayer)
        verticalGridLinesLayer.opacity = 0
        return verticalGridLinesLayer
    }()

    public var isPreviewMode: Bool = false{
        didSet {
            for subView in subviews {
                if let resizeView = subView as? ResizableView{
                    resizeView.resizeViewMode = (isPreviewMode) ? .receivedDoc_preview : ((resizeView.signField.isCompleted) ? .receivedDoc_completed : .receivedDoc_pending)
                }
//                if !(subView is ZResizeView) {
//
//                    continue
//                }
//
//                subView.resizeViewMode = (isPreviewMode) ? .receivedDoc_preview : ((subView.signField.isCompleted) ? .receivedDoc_completed : .receivedDoc_pending)
            }
        }
    }
    
    public var isFinalPreviewMode: Bool = false{
        didSet {
            subviews.forEach { (subview) in
                guard let resizeView = subview as? ResizableView else {
                    return
                }
                
                resizeView.signField.isReadOnly = true
                resizeView.resizeViewMode = (isFinalPreviewMode) ? .receivedDoc_completed : ((resizeView.signField.isCompleted) ? .receivedDoc_completed : .receivedDoc_pending)
                resizeView.isHidden = (!resizeView.signField.isCompleted && (resizeView.signField.type != .checkBox))
            }
            
        }
    }
    
    public var IsInpersonSigning: Bool = false{
        didSet {
            signatureManager.isTemporary            =   IsInpersonSigning;
        }
    }
    
    //MARK: - Inits
    public override init(frame: CGRect) {
        super.init(frame: frame)

        // Initialization code


        initialSetup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    
    deinit {
        ZInfo("deinit")
    }
//    func setIsFinalPreviewMode(_ isFinalPreviewMode: Bool) {
//        self.isFinalPreviewMode = isFinalPreviewMode
//
//        for subView in subviews {
//            if !(subView is ZSResizeView) {
//                continue
//            }
//
//            subView.signField.isReadOnly() = true
//            subView.resizeViewMode = (isFinalPreviewMode) ? .receivedDoc_completed : ((subView.signField.isCompleted) ? .receivedDoc_completed : .receivedDoc_pending)
//            subView.hidden = (!subView.signField.isCompleted && (subView.signField.type != .checkBox))
//        }
//    }
    

    func initialSetup()  {
        
    }
    
    //MARK: - Add Sign Fields
    func addSignField(fieldType: ZSignFieldType, at point: CGPoint, name fieldName: String?, fieldValue value: Any?, dropDownOptions options: [AnyHashable]?, radioGroup: RadioGroup? = nil) {
        var newPoint = point
        if newPoint.x < 50 {
            newPoint.x = 50
            // left x
        }
        if newPoint.y < 25 {
            newPoint.y = 25
            // top y
        }
        if newPoint.x > (frame.size.width - 50) {
            newPoint.x = frame.size.width - 50
            //right x
        }
        if newPoint.y > (frame.size.height - 25) {
            newPoint.y = frame.size.height - 25
            //bottom
        }
        
        
        var fieldWidth: CGFloat = 0.15 * frame.size.width
        var fieldHeight: CGFloat = 0.015 * frame.size.height
        if DeviceType.isIphone {
            fieldWidth = (DeviceType.isIphone5Orless) ? 64 : 80
            fieldHeight = (DeviceType.isIphone5Orless) ? 12.8 : 16
        }
        
        
        var field = SignField()
        field.type = fieldType
        field.isMandatory = true
        field.isReadOnly = false
        field.docId = docId
        field.pageIndex = pageIndex
        
        
        switch fieldType {
        case .signature:
                field.fieldName = "sign.mobile.field.signature".ZSLString
                if let imgVal = value as? UIImage {
                    field.signImage = imgVal
                }
                let signatureRatio = SignatureManager.signatureImageRatio((currentEditingType != .sentDoc_selfSign))
//                GlobalFunctions.signatureImageRatio((currentEditingType != .sentDoc_selfSign))
                
                fieldHeight = fieldWidth * signatureRatio
            case .email:
                field.fieldName     =   "sign.mobile.field.email".ZSLString
                fieldWidth          =   0.23 * frame.width;
                if (DeviceType.isIphone) {
                    fieldWidth          =   (DeviceType.isIphone5Orless) ? 120 : 150;
                }
                field.textValue = value as? String ?? ""
        case .initial:
                field.fieldName = "sign.mobile.field.initial".ZSLString
                if let imgVal = value as? UIImage {
                    field.signImage = imgVal
                }
                let initialRatio = SignatureManager.initialImageRatio((currentEditingType != .sentDoc_selfSign))
                if initialRatio > 0.8 {
                    fieldWidth = 40
                }
                fieldHeight = fieldWidth * initialRatio
            case .name:
                field.fieldName = "sign.mobile.field.fullname".ZSLString
                field.subType = .fullName
                
                field.textValue = value as? String ?? ""

            case .company:
                field.fieldName = "sign.mobile.field.company".ZSLString
                field.textValue = value as? String ?? ""

        case .signDate:
                field.fieldName = "sign.mobile.field.signDate".ZSLString
                field.dateFormat = UserManager.shared.currentUser?.dateFormat
                if let stringVal = value as? String  {
                    field.dateValue = stringVal
                    field.textValue = stringVal
                }
        case .jobTitle:
                field.fieldName = "sign.mobile.field.jobtitle".ZSLString
                field.textValue = value as? String ?? ""

        case .checkBox:
                field.fieldName = "sign.mobile.field.checkbox".ZSLString
                field.isCheckboxTicked = isSampleSigning || currentEditingType == .sentDoc_selfSign
                field.isMandatory = false
                field.textValue  = ""
        case .textBox:
                field.fieldName = fieldName
                field.textValue = value as? String ?? ""

        case .date:
                field.fieldName = "sign.mobile.field.date".ZSLString
                field.dateFormat = "dd MMM yyyy"
                if let format =  UserManager.shared.currentUser?.dateFormat{
                    field.dateFormat = format
                }
                if let stringVal = value as? String  {
                    field.dateValue = stringVal
                    field.textValue = stringVal
                }
        case .dropDown:
                field.fieldName = fieldName
                if let stringVal = value as? String  {
                    field.defaultValue = stringVal
                }
                field.dropdownFields = options
            case .attachment:
                field.fieldName = "sign.mobile.field.attachment".ZSLString
      
        case .radioGroup:
                field.fieldName = "Radiogroup"
                field.isMandatory = !(radioGroup?.isOptional ?? false)
                field.isReadOnly = radioGroup?.isReadOnly ?? false
                field.radioGroup = radioGroup
            default:
                break
        }
        
        if value != nil {
            field.isCompleted = true
        }
        let defaultFontSize  = SwiftUtils.defaultFieldFontSize(pageWidth: frame.size.width)
        
        if fieldType != .dropDown  {
            field.fieldTypeName = "sign.mobile.field.dropDown".ZSLString
        }else{
            field.fieldTypeName = fieldName
        }
        
        let fontName = (UIFont(name: "Roboto", size: 8) != nil) ? "Roboto" : "Helvetica"
        var textSize: CGSize? = nil
        if let font = UIFont(name: fontName, size: defaultFontSize), let taxtVal = field.textValue {
            textSize = taxtVal.size(withAttributes: [
                NSAttributedString.Key.font: font
            ])
        }
        
        if currentEditingType == .sentDoc_Create {
            if let font = UIFont(name: fontName, size: defaultFontSize) {
                textSize = field.fieldName?.size(withAttributes: [
                    NSAttributedString.Key.font: font
                ])
            }
            fieldWidth = (fieldWidth > 100) ? fieldWidth : (100)
        }
        if (fieldType == .signature) || (fieldType == .initial) {
            if let signImg =  field.signImage {
                fieldWidth = (signImg.size.width / signImg.size.height) * fieldHeight
            }
        } else if (currentEditingType != .sentDoc_Create) && ((field.textValue?.count ?? 0) > 2) {
            fieldWidth = ((frame.size.width - 10) > ((textSize?.width ?? 0.0) + 5)) ? ((textSize?.width ?? 0.0) + 5) : (frame.size.width - 10)
            
            fieldHeight = (textSize?.height ?? 0.0) + 1
        }
        
        if fieldType == .checkBox {
            fieldWidth = 0.015 * frame.size.width
            fieldHeight = 0.015 * frame.size.height
            if DeviceType.isIphone {
                fieldWidth = (DeviceType.isIphone5Orless) ? 12.8 : 16
                fieldHeight = (DeviceType.isIphone5Orless) ? 12.8 : 16
            }
        }
        
        setMinSize(&field)
        
        field.currentTextProperty.fontColor = UIColor.black
        field.currentTextProperty.fontFamily = "Roboto"
        field.currentTextProperty.fontSize = Int(defaultFontSize)
        field.currentTextProperty.isBold = false
        field.currentTextProperty.isItalics = false
        field.currentTextProperty.isUnderline = false
        
        field.addedTimeStamp = Date().toMillis()
        
        let fontRatio = FontManager.fieldFontRatio(frame.size.width, imageWidth: pdfPageSize.width)
        let serverFont = Int32(roundf(Float(defaultFontSize / fontRatio)))
        
        field.serverFontSize = Int(serverFont)
        
        if fieldType != .radioGroup {
            field.frame = CGRect(x: newPoint.x - (fieldWidth / 2), y: newPoint.y - 7, width: fieldWidth, height: fieldHeight)
            field.xValue = (field.frame.origin.x / frame.size.width) * 100
            field.yValue = (field.frame.origin.y / frame.size.height) * 100
            field.width = (fieldWidth / frame.size.width) * 100
            field.height = (fieldHeight / frame.size.height) * 100
            
            if field.frame.minX <= 0 {
                field.frame = CGRect(x: 0, y: field.frame.origin.y, width: field.frame.size.width, height: field.frame.size.height)
            }
            ZInfo("%f", fieldHeight)
            ZInfo("%f", fieldWidth)
            addSignField(field, makeFocus: true)
        } else {
            renderRadioGroupView(atPoint: point, withSignField: field,makeFocus: true)
        }
        
        
//        fieldsListArray.append(field)
//        delegate?.editorBaseViewFieldAdded(editorView: self, field: field)
        
    }
    
    private func renderRadioGroupView(atPoint point: CGPoint, withSignField signField: SignField,makeFocus: Bool = false) {
        let radioGroupView = ZRadioGroupView(frame: bounds, itemDroppedAt: point, signField: signField)
        radioGroupView.delegate = self
        radioGroupView.currentEditingType = currentEditingType
        radioGroupView.setResizeViewMode(isPreviewMode: isPreviewMode, isFinalPreviewMode: isFinalPreviewMode)
        radioGroupView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(radioGroupView)
        if signField.isOtherRecipientField {
            radioGroupView.alpha = 0.3
            radioGroupView.isUserInteractionEnabled = false
        }
        //radioGroupView.reloadRadioItems()
        signField.parentView = radioGroupView
        if makeFocus {
            focusSubView(radioGroupView)
        }
        radioGroupView.setFocused(makeFocus)
        self.delegate?.editorBaseViewFieldAdded(editorView: self, field: signField)

        perform({
            if radioGroupView.resizeViewMode == .receivedDoc_preview || radioGroupView.resizeViewMode == .receivedDoc_pending {
                radioGroupView.updateViewFrames(shouldUpdateRadioItemFrames: true)
            }
        }, afterDelay: 0.1)
        
    }
    
    func addSignField(point: CGPoint, fieldType: ZSignFieldType) {
        lastTouchPoint = point
        switch fieldType {
            case .signature:
                addSignatureField()
            case .initial:
                addInitialField()
            case .email:
                addEmailField()
            case .name:
                addFullnameField()
            case .company:
                addCompanyField()
            case .signDate:
                addSignDateField()
            case .jobTitle:
                addJobTitleField()
            case .checkBox:
                addCheckboxItem()
            case .textBox:
                addTextField()
            case .date:
                addCustomDateField()
            case .dropDown:
                addDropdownField()
            case .attachment:
                addAttachmentField()
            case .radioGroup:
                addRadioGroupField()
            default:
                break
        }
    }

    fileprivate func addSignField(_ field: SignField, makeFocus: Bool) {
        if currentEditingType == .receivedDoc_view {
            return
        }

        var field = field
        
        let userResizableView = ZResizeView(frame: CGRect(x: field.frame.origin.x - resizeableView_OuterMargin, y: field.frame.origin.y - resizeableView_OuterMargin, width: field.frame.width + resizeableView_TotalMargin, height: field.frame.height + resizeableView_TotalMargin))

        
//        let userResizableView = ZSResizeView(frame: CGRect(x: field.frame.origin.x - resizeableView_OuterMargin, y: field.frame.origin.y - resizeableView_OuterMargin, width: field.frame.width + resizeableView_TotalMargin, height: field.frame.height + resizeableView_TotalMargin))

        userResizableView.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin, .flexibleWidth, .flexibleHeight]

        userResizableView.currentEditingType = currentEditingType
        userResizableView.isSampleSigning = isSampleSigning
        userResizableView.pdfPageSize = pdfPageSize
        field.parentView = userResizableView

        userResizableView.delegate = self
        if field.minWidth <= 0 {
            setMinSize(&field)
        }


        userResizableView.isDisableResizing = (currentEditingType != .sentDoc_Create) && (currentEditingType != .sentDoc_selfSign)
        if field.type == .attachment {
            userResizableView.isDisableResizing = true
        }
        addSubview(userResizableView)
//        userResizableView.signField = field
        userResizableView.setSignField(field: field)
        userResizableView.setResizeViewMode(isPreviewMode: isPreviewMode, isFinalPreviewMode: isFinalPreviewMode)
    /*
        var resizeMode : ZSResizeViewMode = .sentDoc_selfSign
        if currentEditingType == .sentDoc_Create {
            if isPreviewMode {
                resizeMode = .receivedDoc_preview
            } else {
                resizeMode = .sentDoc_Create
            }
        } else if (currentEditingType == .sentDoc_selfSign) && !isFinalPreviewMode {
            resizeMode = .sentDoc_selfSign
        } else if isFinalPreviewMode || (currentEditingType == .receivedDoc_view) {
            resizeMode = .receivedDoc_completed
        } else if currentEditingType == .receivedDoc_sign {
            if isPreviewMode {
                resizeMode = .receivedDoc_preview
            } else if field.isCompleted {
                resizeMode = .receivedDoc_completed
            } else {
                resizeMode = .receivedDoc_pending
            }
        }
    
        userResizableView.resizeViewMode = resizeMode
    */


        if field.isOtherRecipientField {
            userResizableView.isUserInteractionEnabled = false
            userResizableView.alpha = 0.3
        }

        if makeFocus {
            focusSubView(userResizableView)
        }

        userResizableView.minWidth = field.minWidth + resizeableView_TotalMargin
        userResizableView.minHeight = field.minHeight + resizeableView_TotalMargin
        
        delegate?.editorBaseViewFieldAdded(editorView: self, field: field)
    }
    
    
     public func preFillSignFields(_ fields : [SignField]) {
        for i in 0..<fields.count {
            var field = fields[i]
            if field.serverFontSize <= 0{
                field.serverFontSize = 8
            }
            field.currentTextProperty.fontSize = FontManager.calibaratedDeviceFontSize(forServerFontSize: String(field.serverFontSize), pageWidth: bounds.width, imageWidth: pdfPageSize.width)

            field.frame =  normalisedFrame(x: field.xValue, y: field.yValue, width: field.width, height: field.height)//CGRect(x: ((field.xValue / 100) * bounds.width), y: (field.yValue / 100) * bounds.height, width: (field.width / 100) * (bounds.width), height: (field.height / 100) * bounds.height)
            
            if field.type == .radioGroup {
                field.radioGroup?.values.enumeratedInPlaceLoop({ (_, value) -> RadioValues in
                    var mutableCopy = value
                    mutableCopy.setRect(inBounds: bounds)
                    return mutableCopy
                })
                renderRadioGroupView(atPoint: lastTouchPoint, withSignField: field)
            } else {
                addSignField(field, makeFocus: false)
            }
        }
        
     }
    
    func normalisedFrame(x: CGFloat,y: CGFloat,width: CGFloat,height: CGFloat) -> CGRect {
        return CGRect(x: ((x / 100) * bounds.width), y: (y / 100) * bounds.height, width: (width / 100) * (bounds.width), height: (height / 100) * bounds.height)
    }
    
    public func clearAllSignFields()
    {
        subviews.forEach { (subview) in
            if subview is ZResizeView{
                subview.removeFromSuperview()
            } else if subview is ZRadioGroupView {
                subview.removeFromSuperview()
            }
        }
    }
    
    public func updateFieldFramesForViewRotation(_ fieldsListArr : [SignField])  {
        fieldsListArr.forEach { (field) in
            if let resizeview = field.parentView as? ZResizeView{
                resizeview.textFieldRatio = 1
                
                resizeview.signField.currentTextProperty.fontSize = FontManager.calibaratedDeviceFontSize(forServerFontSize: String(resizeview.signField.serverFontSize), pageWidth: superview?.frame.width ?? 1, imageWidth: pdfPageSize.width)
                
                if UIApplication.shared.statusBarOrientation.isLandscape {
                    let fullScreenRect = UIScreen.main.bounds
                    resizeview.textFieldRatio = fullScreenRect.size.width / fullScreenRect.size.height
                }

                resizeview.minWidth = (resizeview.signField.minWidth * resizeview.textFieldRatio) + resizeableView_TotalMargin
                resizeview.minHeight = (resizeview.signField.minHeight * resizeview.textFieldRatio) + resizeableView_TotalMargin

                resizeview.frame = CGRect(x: field.frame.origin.x - resizeableView_OuterMargin, y: field.frame.origin.y - resizeableView_OuterMargin, width: field.frame.width + resizeableView_TotalMargin, height: field.frame.height + resizeableView_TotalMargin)
                resizeview.adjustFrames()
                resizeview.viewRotationHandling()
                
            }
            
            
        }
        
//        if let resizeview = subview as? ZResizeView{
//            resizeview.textFieldRatio = 1
//
//            resizeview.signField.currentTextProperty.fontSize = FontManager.calibaratedDeviceFontSize(forServerFontSize: String(resizeview.signField.serverFontSize), pageWidth: superview?.frame.width ?? 1, imageWidth: pdfPageSize.width)
//
//            if UIApplication.shared.statusBarOrientation.isLandscape {
//                let fullScreenRect = UIScreen.main.bounds
//                resizeview.textFieldRatio = fullScreenRect.size.width / fullScreenRect.size.height
//            }
//
//            resizeview.minWidth = (resizeview.signField.minWidth * resizeview.textFieldRatio) + resizeableView_TotalMargin
//            resizeview.minHeight = (resizeview.signField.minHeight * resizeview.textFieldRatio) + resizeableView_TotalMargin
//
//            resizeview.frame = CGRect(x: resizeview.signField.frame.origin.x - resizeableView_OuterMargin, y: resizeview.signField.frame.origin.y - resizeableView_OuterMargin, width: resizeview.signField.frame.width + resizeableView_TotalMargin, height: resizeview.signField.frame.height + resizeableView_TotalMargin)
//            resizeview.adjustFrames()
//            resizeview.viewRotationHandling()
//        }
//
//        subviews.forEach { (subview) in
//            if let resizeview = subview as? ZResizeView{
//                resizeview.textFieldRatio = 1
//
//                resizeview.signField.currentTextProperty.fontSize = FontManager.calibaratedDeviceFontSize(forServerFontSize: String(resizeview.signField.serverFontSize), pageWidth: superview?.frame.width ?? 1, imageWidth: pdfPageSize.width)
//
//                if UIApplication.shared.statusBarOrientation.isLandscape {
//                    let fullScreenRect = UIScreen.main.bounds
//                    resizeview.textFieldRatio = fullScreenRect.size.width / fullScreenRect.size.height
//                }
//
//                resizeview.minWidth = (resizeview.signField.minWidth * resizeview.textFieldRatio) + resizeableView_TotalMargin
//                resizeview.minHeight = (resizeview.signField.minHeight * resizeview.textFieldRatio) + resizeableView_TotalMargin
//
//                resizeview.frame = CGRect(x: resizeview.signField.frame.origin.x - resizeableView_OuterMargin, y: resizeview.signField.frame.origin.y - resizeableView_OuterMargin, width: resizeview.signField.frame.width + resizeableView_TotalMargin, height: resizeview.signField.frame.height + resizeableView_TotalMargin)
//                resizeview.adjustFrames()
//                resizeview.viewRotationHandling()
//            }
//        }
    }
    
    //MARK: - Add Sign action Fields Extn
    func addSignatureField() {
        if currentEditingType == .sentDoc_Create {
            addSignField(fieldType: .signature, at: lastTouchPoint, name: "", fieldValue: nil, dropDownOptions: nil)
        } else if currentEditingType == .sentDoc_selfSign {
            if let inPersonSignature = currentInpersonSigner?.inPersonSignature {
                addSignField(fieldType: .signature, at: lastTouchPoint, name: "", fieldValue: inPersonSignature, dropDownOptions: nil)
            } else if let signImage =  UserManager.shared.currentUser?.getSignatureImage() {
                addSignField(fieldType: .signature, at: lastTouchPoint, name: "", fieldValue: signImage, dropDownOptions: nil)
            }
            else{
                addSignButtonClick()
            }
        }
    }
    
    func addSignButtonClick() {

         guard let superVc =  superview?.superview?.superview?.next as? UIViewController else {
             return
         }
        
         let convertedPoint = superVc.view.convert(lastTouchPoint, from: self)
         signatureManager.rootViewController = superVc
         signatureManager.isThirdPartySDK      =  isThirdPartySDK;
        signatureManager.signatureType      =   .signature;
         signatureManager.showSignatureWizard(fromRect: CGRect(x: convertedPoint.x, y: convertedPoint.y, width: 1, height: 1))
     }

    func addInitialField() {
        if currentEditingType == .sentDoc_Create {
            addSignField(fieldType: .initial, at: lastTouchPoint, name: "", fieldValue: nil, dropDownOptions: nil)
        } else if currentEditingType == .sentDoc_selfSign {
            if let inPersonInitial = currentInpersonSigner?.inPersonInitial {
                addSignField(fieldType: .initial, at: lastTouchPoint, name: "", fieldValue: inPersonInitial, dropDownOptions: nil)
            } else if let initalImage =  UserManager.shared.currentUser?.getInitialImage() {
                addSignField(fieldType: .initial, at: lastTouchPoint, name: "", fieldValue: initalImage, dropDownOptions: nil)
            }
            else{
                addInitialButtonClick()
            }
        }
    }
    
    func addInitialButtonClick() {

        guard let superVc =  superview?.superview?.superview?.next as? UIViewController else {
            return
        }
       
        let convertedPoint = superVc.view.convert(lastTouchPoint, from: self)
        signatureManager.rootViewController = superVc
        signatureManager.isThirdPartySDK      =  isThirdPartySDK;
        signatureManager.signatureType      =   .initial
        signatureManager.showSignatureWizard(fromRect: CGRect(x: convertedPoint.x, y: convertedPoint.y, width: 1, height: 1))
    }

    func addEmailField() {
        if isSampleSigning {
            addSignField(fieldType: .email, at: lastTouchPoint, name: "", fieldValue: currentInpersonSigner?.email, dropDownOptions: nil)
            return
        }
        if currentEditingType == .sentDoc_Create {
            addSignField(fieldType: .email, at: lastTouchPoint, name: "", fieldValue: nil, dropDownOptions: nil)
        } else if currentEditingType == .sentDoc_selfSign {
            if let email = UserManager.shared.currentUser?.email {
                addSignField(fieldType: .email,at: lastTouchPoint, name: "", fieldValue: email, dropDownOptions: nil)
            } else {
                let textPopup = ZSTextPopupController()
                textPopup.isTextFieldCanbeEmpty = false
                textPopup.title = "sign.mobile.field.email".ZSLString
                textPopup.textField.text = ""
                textPopup.isValidateEmailAddress = true
                textPopup.textField.keyboardType = .emailAddress
                textPopup.textField.autocapitalizationType = .none

                weak var _self = self
                textPopup.didEndEditingWithText = { inputText in
                    _self?.textPopupDoneWithText(inputText, fieldType: .email)
                }

                textPopup.cancelBlock = {
                    _self?.superVC?.becomeFirstResponder()
                }
                superVC?.present(textPopup, animated: true)
            }
        }
    }
    
    

    func addFullnameField() {
        if isSampleSigning {
            addSignField(fieldType: .name, at: lastTouchPoint, name: "", fieldValue: currentInpersonSigner?.name, dropDownOptions: nil)
            return
        }
        if currentEditingType == .sentDoc_Create {
            addSignField(fieldType: .name, at: lastTouchPoint, name: "", fieldValue: nil, dropDownOptions: nil)
        } else if currentEditingType == .sentDoc_selfSign {
            if let fullName = UserManager.shared.currentUser?.fullName,!fullName.isEmpty  {
                addSignField(fieldType: .name,at: lastTouchPoint, name: "", fieldValue: fullName, dropDownOptions: nil)
            } else {
                let textPopup = ZSTextPopupController()
                textPopup.isTextFieldCanbeEmpty = false
                textPopup.title = "sign.mobile.field.fullname".ZSLString
                textPopup.textField.text = ""
                textPopup.textField.keyboardType = .phonePad
                textPopup.textField.autocapitalizationType = .sentences

                weak var _self = self
                textPopup.didEndEditingWithText = { inputText in
                    _self?.textPopupDoneWithText(inputText, fieldType: .name)
                }

                textPopup.cancelBlock = {
                    _self?.superVC?.becomeFirstResponder()
                }
                superVC?.present(textPopup, animated: true)
            }
        }
    }
    
    func addCompanyField() {
        if isSampleSigning {
            addSignField(fieldType: .company, at: lastTouchPoint, name: "", fieldValue: "Bridgeford Food Corporation", dropDownOptions: nil)
            return
        }
        if currentEditingType == .sentDoc_Create {
            addSignField(fieldType: .company, at: lastTouchPoint, name: "", fieldValue: nil, dropDownOptions: nil)
        } else if currentEditingType == .sentDoc_selfSign {
            if let company = UserManager.shared.currentUser?.company,company.isEmpty  {
                addSignField(fieldType: .company,at: lastTouchPoint, name: "", fieldValue: company, dropDownOptions: nil)
            } else {
                let textPopup = ZSTextPopupController()
                textPopup.isTextFieldCanbeEmpty = false
                textPopup.title = "sign.mobile.field.company".ZSLString
                textPopup.textField.text = ""
                textPopup.textField.keyboardType = .phonePad
                textPopup.textField.autocapitalizationType = .sentences

                weak var _self = self
                textPopup.didEndEditingWithText = { inputText in
                    _self?.textPopupDoneWithText(inputText, fieldType: .company)
                }

                textPopup.cancelBlock = {
                    _self?.superVC?.becomeFirstResponder()
                }
                superVC?.present(textPopup, animated: true)
            }
        }
    }
    
    func addJobTitleField() {
        
        if isSampleSigning {
            addSignField(fieldType: .jobTitle, at: lastTouchPoint, name: "", fieldValue: "VP Sales & Marketing", dropDownOptions: nil)
            return
        }
        if currentEditingType == .sentDoc_Create {
            addSignField(fieldType: .jobTitle, at: lastTouchPoint, name: "", fieldValue: nil, dropDownOptions: nil)
        } else if currentEditingType == .sentDoc_selfSign {
            if let company = UserManager.shared.currentUser?.company,company.isEmpty  {
                addSignField(fieldType: .jobTitle,at: lastTouchPoint, name: "", fieldValue: company, dropDownOptions: nil)
            } else {
                let textPopup = ZSTextPopupController()
                textPopup.isTextFieldCanbeEmpty = false
                textPopup.title = "sign.mobile.field.jobtitle".ZSLString
                textPopup.textField.text = ""
                textPopup.textField.keyboardType = .phonePad
                textPopup.textField.autocapitalizationType = .sentences

                weak var _self = self
                textPopup.didEndEditingWithText = { inputText in
                    _self?.textPopupDoneWithText(inputText, fieldType: .jobTitle)
                }

                textPopup.cancelBlock = {
                    _self?.superVC?.becomeFirstResponder()
                }
                superVC?.present(textPopup, animated: true)
            }
        }
    }
    
    func addCustomDateField() {
        addSignField(fieldType: .date, at: lastTouchPoint, name: "", fieldValue: nil, dropDownOptions: nil)
    }
    
    func addTextField()
    {
        let textPopup = ZSTextPopupController()
        textPopup.isTextFieldCanbeEmpty = false
        textPopup.title = "sign.mobile.field.textfield".ZSLString
        textPopup.textField.text = ""
        if currentEditingType == .sentDoc_selfSign {
            textPopup.textPopupType = .textView
        }
        textPopup.textField.keyboardType = .phonePad
        textPopup.textField.autocapitalizationType = .sentences
        
        weak var _self = self
        textPopup.didEndEditingWithText = { inputText in
            _self?.textPopupDoneWithText(inputText, fieldType: .textBox)
        }
    
        textPopup.cancelBlock = {
            _self?.superVC?.becomeFirstResponder()
        }
        superVC?.present(textPopup, animated: true)
    }
    
    func addSignDateField()  {
        if (isSampleSigning) {
            dateFieldFormatter.dateFormat = DATE_ONLY_FORMAT_MONTH_FIRST
            let str = dateFieldFormatter.string(from: Date())
            addSignField(fieldType: .signDate, at: lastTouchPoint, name: "", fieldValue: str, dropDownOptions: nil)
            return;
        }
        if (currentEditingType == .sentDoc_Create) {
            addSignField(fieldType: .signDate, at: lastTouchPoint, name: "", fieldValue: nil, dropDownOptions: nil)
        }
        else if (currentEditingType == .sentDoc_selfSign)
        {
            if let dateFormat = UserManager.shared.currentUser?.dateFormat{
                dateFieldFormatter.dateFormat = dateFormat
            }else{
                dateFieldFormatter.dateFormat = DATE_ONLY_FORMAT_MONTH_FIRST
            }
            dateFieldFormatter.dateFormat = UserManager.shared.currentUser?.dateFormat
            
            let str = dateFieldFormatter.string(from: Date())
            addSignField(fieldType: .signDate, at: lastTouchPoint, name: "", fieldValue: str, dropDownOptions: nil)
        }
    }
    
    func addDropdownField()  {
        let dropDown = ZSDropdownController(style: .grouped)
        weak var _self = self;
        dropDown.completionHandler = {(fieldName,options, selectedValue) in
            var optionsArr: [AnyHashable] = []
            options.forEach { (opt) in
                optionsArr.append(["dropdown_order": opt.dropdownOrder,
                                   "dropdown_value": opt.dropdownValue])
            }
            _self?.addSignField(fieldType: .dropDown, at: _self?.lastTouchPoint ?? CGPoint.zero, name: fieldName, fieldValue: selectedValue, dropDownOptions : optionsArr)
        }

        let nav = UINavigationController(rootViewController: dropDown)
        
        if DeviceType.isIpad {
            nav.modalPresentationStyle = .popover
            nav.popoverPresentationController?.sourceRect = CGRect(x: lastTouchPoint.x, y: lastTouchPoint.y, width: 0, height: 0)
            nav.popoverPresentationController?.sourceView = self
            nav.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        } else if !SwiftUtils.isIOS13AndAbove{
            nav.modalPresentationStyle = .custom
            nav.transitioningDelegate = transitionManager
        }
        superVC?.present(nav, animated: true)

    }
            
    func addCheckboxItem()  {
        addSignField(fieldType: .checkBox, at: lastTouchPoint, name: "", fieldValue: nil, dropDownOptions: nil)
    }

    func addAttachmentField(){
        addSignField(fieldType: .attachment, at: lastTouchPoint, name: "", fieldValue: nil, dropDownOptions: nil)
    }
    
    func addRadioGroupField(withRadioGroupView radioGroupView: ZRadioGroupView? = nil){
//        addSignField(fieldType: .radioGroup, at: lastTouchPoint, name: "", fieldValue: nil, dropDownOptions: nil)
        var sourceFrame = CGRect(origin: lastTouchPoint, size: .zero)
        let radioGroupController: ZRadioGroupController
        if let radioGroupView = radioGroupView {
            radioGroupController = ZRadioGroupController(radioGroup: radioGroupView.signField.radioGroup!)
            radioGroupController.associatedView = radioGroupView
            sourceFrame = radioGroupView.selectedRadioItem!.frame
        } else {
            radioGroupController = ZRadioGroupController(style: .grouped)
        }
        
        radioGroupController.delegate = self
        
        let nav = ZNavigationController(rootViewController: radioGroupController, supportedOrient: .portrait)
        
        if DeviceType.isIpad {
            nav.modalPresentationStyle = .popover
            nav.popoverPresentationController?.sourceRect = sourceFrame
            nav.popoverPresentationController?.sourceView = self
            nav.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        } else if SwiftUtils.isIOS13AndAbove {
            nav.modalPresentationStyle = .custom
            nav.transitioningDelegate = transitionManager
        }
        superVC?.present(nav, animated: true, completion: nil)
    }
    
    func textFieldButtonClicked(_ resizeView: ZResizeView) {
        currentSelectedView = resizeView
        
        var isAlreadyHaveValue = false
        var text: String?
        switch resizeView.signField.type {
            case .company:
                if let company = UserManager.shared.currentUser?.company, !company.isEmpty {
                    text = company
                    isAlreadyHaveValue = true
                }
            case .email:
                if let email = UserManager.shared.currentUser?.email, !email.isEmpty {
                    text = email
                    isAlreadyHaveValue = true
                }
            case .name:
                if let fullName = UserManager.shared.currentUser?.fullName, !fullName.isEmpty {
                    if resizeView.signField.subType == .firstName {
                        text = UserManager.shared.currentUser?.firstName
                    } else if resizeView.signField.subType == .lastName {
                        text = UserManager.shared.currentUser?.lastName
                    } else {
                        text = fullName
                    }
                    
                    isAlreadyHaveValue = true
                }
            case .jobTitle:
                if let jobTitle = UserManager.shared.currentUser?.jobTitle, !jobTitle.isEmpty {
                    text = jobTitle
                    isAlreadyHaveValue = true
                }
            default:
                break
        }
        if IsInpersonSigning {
            isAlreadyHaveValue = false
            //to omit existing field values of host signer
        }
        
        if isAlreadyHaveValue {
            endEditing(true)
            resizeView.signField.textValue = text
            valueChangeHandling(field: &resizeView.signField, markComplete: true)
            resizeView.setFieldValue(text ?? "")
            resizeView.resizeViewMode = .receivedDoc_completed
            if resizeView.textFieldRatio > 1.00{
                resizeView.adjustFrames()
            }
        }
        else{
            let textPopup = ZSTextPopupController()
            textPopup.isTextFieldCanbeEmpty = false
            if resizeView.signField.type == .textBox {
                textPopup.textPopupType = .textView
                textPopup.textView.text = resizeView.signField.textValue
            }

            textPopup.title = resizeView.signField.fieldName
            textPopup.textField.text = resizeView.signField.textValue
            textPopup.isValidateEmailAddress = resizeView.signField.type == .email
            textPopup.textField.keyboardType = (resizeView.signField.type == .email) ? .emailAddress : .namePhonePad
            textPopup.textField.autocapitalizationType = (resizeView.signField.type == .email) ? .none : .sentences

            weak var _self = self
            textPopup.didEndEditingWithText = { inputText in
                _self?.textPopupDoneWithText(inputText, fieldType: resizeView.signField.type)
            }
            if IsInpersonSigning {
                if resizeView.signField.type == .email {
                    textPopup.textField.text = currentInpersonSigner?.inPersonEmail
                } else if resizeView.signField.type == .name {
                    if resizeView.signField.subType == .firstName {
                        textPopup.textField.text = currentInpersonSigner?.inpersonFirstName()
                    } else if resizeView.signField.subType == .lastName {
                        textPopup.textField.text = currentInpersonSigner?.inpersonLastName()
                        textPopup.isTextFieldCanbeEmpty = true
                    } else {
                        textPopup.textField.text = currentInpersonSigner?.inPersonName
                    }
                } else {
                    textPopup.textField.text = ""
                }
            }
            textPopup.cancelBlock = {
                _self?.superVC?.becomeFirstResponder()
            }
            superVC?.present(textPopup, animated: true)

        }
    }
    
    //MARK: - Sign Field Tap Action for guest
    func signButtonClicked(_ resizeView: ZResizeView, type: ZSignFieldSignatureType) {
        endEditing(true)
        currentSelectedView = resizeView

        if type == .signature {
            if let signatureImg = UserManager.shared.currentUser?.getSignatureImage(), !IsInpersonSigning{
                resizeView.setFieldSignature(signatureImg)
                resizeView.resizeViewMode = .receivedDoc_completed
                valueChangeHandling(field: &resizeView.signField, markComplete: true)
                return
            }
            else if let signatureImg = currentInpersonSigner?.inPersonSignature{
                resizeView.setFieldSignature(signatureImg)
                valueChangeHandling(field:  &resizeView.signField, markComplete: true)
                resizeView.resizeViewMode = .receivedDoc_completed
                return
            }
        }
        else if type == .initial {
            if let initialImg = UserManager.shared.currentUser?.getInitialImage(), !IsInpersonSigning{
                resizeView.setFieldSignature(initialImg)
                resizeView.resizeViewMode = .receivedDoc_completed
                valueChangeHandling(field: &resizeView.signField, markComplete: true)
                return
            }
            else if let initialImg = currentInpersonSigner?.inPersonInitial{
                resizeView.setFieldSignature(initialImg)
                valueChangeHandling(field: &resizeView.signField, markComplete: true)
                resizeView.resizeViewMode = .receivedDoc_completed
                return
            }
        }
        
        //else handling
        currentSelectedView.safelyUnwrap { (resizeView: ZResizeView) in
            delegate?.editorBaseViewSignatureButtonTapped(editorView: self, signView: resizeView, type: type)
        }
    }

    func signDateButtonClicked(_ resizeView: ZResizeView) {
        endEditing(true)

        dateFieldFormatter.dateFormat = resizeView.signField.dateFormat ?? UserManager.shared.currentUser?.dateFormat
        
        let str = dateFieldFormatter.string(from: Date())
        resizeView.setFieldValue(str)
        resizeView.signField.dateValue = str
        valueChangeHandling(field: &resizeView.signField, markComplete: true)
        resizeView.resizeViewMode = .receivedDoc_completed
        currentSelectedView = resizeView
        
        if ((resizeView.textFieldRatio ?? 1) > CGFloat(1.0)){
            resizeView.adjustFrames()
        }
    }
    
    func dropDownFieldClicked(_ resizeView: ZResizeView) {
        endEditing(true)

        var optionsArray: [String] = []
        if let dropdownFields = resizeView.signField.dropdownFields {
            for dict in dropdownFields {
                guard let dict = dict as? [AnyHashable : Any] else {
                    continue
                }
                optionsArray.append(dict[keyPath: "dropdown_value"] ?? "")
            }
        }

        var selectedval = resizeView.signField.defaultValue as? String
        if let textVal = resizeView.signField.textValue, !textVal.isEmpty{
            selectedval = textVal
        }

        weak var _self = self

        let popVC = PopupViewController(title: resizeView.signField.fieldName ?? "Dropdown", menuItems: PopupDataModel.getDataModel(forStrings: optionsArray), selectedItem: PopupDataModel(title: selectedval ?? ""))
        popVC.completionHandler = { selectedIndex in
            var selectedval = optionsArray[selectedIndex] as? String
            resizeView.setFieldValue(selectedval)
            resizeView.signField.textValue = selectedval
            resizeView.resizeViewMode = .receivedDoc_completed
            _self?.valueChangeHandling(field: &resizeView.signField, markComplete: true)
        }

        let nav = ZNavigationController(rootViewController: popVC, supportedOrient: DeviceType.isIphone ? .portrait : .all)

        if DeviceType.isIphone {
            popVC.modalPresentationStyle = .custom
            popVC.transitioningDelegate = transitionManager
            superVC?.present(popVC, animated: true, completion: nil)
        } else {
            nav.modalPresentationStyle = .popover
            nav.popoverPresentationController?.sourceRect = resizeView.frame
            nav.popoverPresentationController?.sourceView = self
            nav.popoverPresentationController?.permittedArrowDirections = [.down, .up]
            superVC?.present(nav, animated: true, completion: nil)
        }
        
    }
    
    func dateButtonClicked(_ resizeView: ZResizeView) {
        endEditing(true)

        dateFieldFormatter.dateFormat = resizeView.signField.dateFormat ?? UserManager.shared.currentUser?.dateFormat

        let datePicker = ZSDatePickerController(title: "sign.mobile.request.selectedDate".ZSLString, selectedDate: Date())
        weak var _self = self
        datePicker.completionHandler = { selectedDate in
            var str: String? = nil
            if let selectedDate = selectedDate {
                str = self.dateFieldFormatter.string(from: selectedDate)
            }
            resizeView.signField.dateValue = str
            resizeView.setFieldValue(str)
            resizeView.resizeViewMode = .receivedDoc_completed

            _self?.valueChangeHandling(field: &resizeView.signField, markComplete: true)
        }
        let nav = UINavigationController(rootViewController: datePicker)
        nav.modalPresentationStyle = .custom
        nav.transitioningDelegate = transitionManager
        if DeviceType.isIpad {
            nav.modalPresentationStyle = .popover
            nav.popoverPresentationController?.sourceRect = resizeView.frame
            nav.popoverPresentationController?.sourceView = self
            nav.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }
        superVC?.present(nav, animated: true)
    }

    func checkboxButtonClicked(_ resizeView: ZResizeView) {
        endEditing(true)
        resizeView.setFieldCheckboxValue(!resizeView.signField.isCheckboxTicked)
        var fieldCompleted = true
        if resizeView.signField.isMandatory != nil {
            fieldCompleted = resizeView.signField.isCheckboxTicked ?? false
        }
        valueChangeHandling(field: &resizeView.signField, markComplete: fieldCompleted)
        resizeView.resizeViewMode = .receivedDoc_completed
    }


    //MARK: - Helpers
    
    func setMinSize(_ field: inout SignField) {
        if currentEditingType == .receivedDoc_sign {
            return
        }
        
        var fieldWidth: CGFloat = 0.12 * frame.size.width
        var fieldHeight: CGFloat = 0.012 * frame.size.height
        
        switch field.type {
            case .signature:
                fieldWidth = 0.095 * frame.size.width
                let signatureRatio = SignatureManager.signatureImageRatio((currentEditingType != .sentDoc_selfSign))
                fieldHeight = fieldWidth * signatureRatio
                if fieldHeight < 30 {
                    fieldHeight = 30
                }
            case .email:
                fieldWidth = 0.23 * frame.size.width
            case .initial:
                fieldWidth = 0.05 * frame.size.width
                let initialRatio = SignatureManager.initialImageRatio((currentEditingType != .sentDoc_selfSign))
                fieldHeight = fieldWidth * initialRatio
                if fieldHeight < 20 {
                    fieldHeight = 20
                }
            default:
                break
        }
        
        let fontName = UIFont(name: "Roboto", size: 8) != nil ? "Roboto" : "Helvetica"
        
        var textMinSize: CGSize = .zero
        if let font = UIFont(name: fontName, size: 8) {
            if let textVal = field.textValue, !textVal.isEmpty{
                textMinSize = textVal.SizeOf_String(font: font)
            }
        }
        
        var textminWidth = fieldWidth
        var textminHeight = fieldHeight
        
        if currentEditingType == .sentDoc_Create {
            
            if let font = UIFont(name: fontName, size: 8) {
                if let textVal = field.textValue, !textVal.isEmpty{
                    textMinSize = textVal.SizeOf_String(font: font)
                }
            }
            textminWidth = (fieldWidth > (80)) ? fieldWidth : (80)
        }
        if ((field.type == .signature) || (field.type == .initial)) {
            if let signImg = field.signImage {
                textminWidth  =  (signImg.size.width/signImg.size.height) * fieldHeight;
            }
        }
        else if((currentEditingType != .sentDoc_Create) || (field.type == .textBox))
        {//other fields(texts)
            textminWidth    =   ((self.frame.size.width - 10) > (textMinSize.width + 5)) ? (textMinSize.width + 5) : (self.frame.size.width - 10);
        }
        
        if (field.type == .checkBox) {
            textminWidth      =  0.013 * self.frame.size.width;
            textminHeight     =  0.013 * self.frame.size.height;
        }
        
        field.minWidth      =   textminWidth;
        field.minHeight     =   textminHeight;
    }
    
    func hideGridLines()  {
        horizontalGridLinesLayer.opacity = 0
        verticalGridLinesLayer.opacity = 0
    }
    
    func focusSubView(_ resizeView : ResizableView)  {
        selectedField   =   resizeView.signField;
        if (selectedField?.isOtherRecipientField ?? false) {return;}
           
           isEditingMode = true;
        for subview in subviews {
            if let subview = subview as? ZResizeView {
                if subview == resizeView {
                    bringSubviewToFront(resizeView)
                    subview.showEditingHandles()
                }else{
                    subview.hideEditingHandles()
                }
            } else if let radioGroupView = subview as? ZRadioGroupView {
                if radioGroupView == resizeView {
                    bringSubviewToFront(resizeView)
                } else {
                    radioGroupView.setFocused(false)
                }
            }
        }
        
        resizeView.becomeFirstResponder()
        delegate?.editorBaseViewBeginEditing(editorView: self, field: selectedField!)
        currentSelectedView = resizeView
    }
 
    public func endEditing()  {
        endEditing(true)
        isEditingMode = false
        isUserInteractionEnabled = false
        currentSelectedView?.hideEditingHandles()
        currentSelectedView = nil;
        isUserInteractionEnabled = true
//        if (_delegate && [_delegate respondsToSelector:@selector(imageEditorViewEndEditing:)]) {
//            [_delegate imageEditorViewEndEditing:self];
//        }//TODO: change

        if let presented = superVC?.presentedViewController{
            presented.dismiss(animated: false, completion: nil)
        }
    }
    
    //MARK:- Compose popup menu delagate
    func textPopupDoneWithText(_ inputText : String, fieldType : ZSignFieldType?)  {
        guard let fieldType = fieldType else {
            return
        }
        if (currentEditingType == .receivedDoc_sign) {
            
            guard let resizeView = currentSelectedView as? ZResizeView else {
                return
            }
            let inputText = inputText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

            resizeView.signField.textValue = inputText

            var fieldname: String? =  ""
            updateCurrentUserDetail(fieldType: resizeView.signField.type!, inputText: inputText, fieldName: &fieldname)
            resizeView.setFieldValue(inputText)
            valueChangeHandling(field: &resizeView.signField, markComplete: !inputText.isEmpty)
            
            resizeView.resizeViewMode = .receivedDoc_completed

            if (CGFloat(resizeView.textFieldRatio ?? 1) > 1.000) {resizeView.adjustFrames()}
        }
        else if (currentEditingType == .sentDoc_Create)
        {
            addSignField(fieldType: .textBox, at: lastTouchPoint, name: inputText, fieldValue: nil, dropDownOptions: nil)

        }
        else if (currentEditingType == .sentDoc_selfSign)
        {
            var fieldname: String? = "sign.mobile.field.text".ZSLString
            updateCurrentUserDetail(fieldType: fieldType, inputText: inputText, fieldName: &fieldname)
            addSignField(fieldType: fieldType, at: lastTouchPoint, name: fieldname, fieldValue: inputText, dropDownOptions: nil)
        }
        
        superVC?.presentedViewController?.dismiss(animated: false, completion: nil)
    }
    
    func updateCurrentUserDetail(fieldType : ZSignFieldType, inputText : String,  fieldName :inout  String?)  {
        if (IsInpersonSigning) {
            if (fieldType == .email) {
                currentInpersonSigner?.inPersonEmail = inputText;
            }
            return;
        }
        switch (fieldType) {
            
            case .email:
                if (!SwiftUtils.isValidEmail(inputText, showAlertIfFails: false, rootViewController: self.superVC)) {
                    return;
                }
                
                fieldName =  "sign.mobile.field.email".ZSLString
                UserManager.shared.currentUser?.email = inputText
                UserManager.shared.saveUser(updateInServer: true)
                break;
            case .initial:
                fieldName =  "sign.mobile.field.initial".ZSLString
            case .name:
                fieldName =  "sign.mobile.field.name".ZSLString
                UserManager.shared.currentUser?.fullName = inputText
                UserManager.shared.saveUser(updateInServer: true)

            case .company:
                fieldName =  "sign.mobile.field.company".ZSLString
                UserManager.shared.currentUser?.company = inputText
                UserManager.shared.saveUser(updateInServer: true)
            case .jobTitle:
                fieldName =  "sign.mobile.field.jobtitle".ZSLString
                UserManager.shared.currentUser?.jobTitle = inputText
                UserManager.shared.saveUser(updateInServer: true)

            default:
                break;
            }
    }
    
    //MARK: - Signing delegate
    func valueChangeHandling(field : inout SignField, markComplete : Bool)  {
        field.isCompleted =   markComplete
        delegate?.editorBaseViewFieldValueChanged(editorView: self, field: field)
    }
}

//MARK:- Extns
extension DocumentEditorBaseView : SignatureManagerDelegate{
    public func didSaveSignature(signImage: UIImage) {
        currentInpersonSigner?.inPersonSignature = signImage
        
        perform({[weak self] in
            self?.addSignField(fieldType: .signature, at: self?.lastTouchPoint ?? CGPoint.zero, name: "", fieldValue: signImage, dropDownOptions: nil)
            }, afterDelay: 0.1)
    }
    
    public func didSaveIntial(initialImage: UIImage) {
        currentInpersonSigner?.inPersonInitial = initialImage
        
        perform({[weak self] in
            self?.addSignField(fieldType: .initial, at: self?.lastTouchPoint ?? CGPoint.zero, name: "", fieldValue: initialImage, dropDownOptions: nil)
            }, afterDelay: 0.1)
    }
    
   
}



//MARK: - ZResizeViewDelegate

extension DocumentEditorBaseView : ZResizeViewDelegate{
    public func resizeViewDidChangeTextProperties(_ resizeView: ZResizeView) {
        delegate?.editorBaseViewFieldDetailsUpdated(editorView: self, field: resizeView.signField)
    }
    
   
    public func resizeViewDidChangeSubtype(type: ZSignFieldSubType, resizeView: ZResizeView) {
        
        resizeView.signField.subType = type
        
        if (resizeView.signField.subType == .firstName) {
            resizeView.signField.textValue =  (isSampleSigning) ? currentInpersonSigner?.inpersonFirstName() : UserManager.shared.currentUser?.firstName
            resizeView.signField.fieldName =   "sign.mobile.field.firstname".ZSLString
        }else if (resizeView.signField.subType == .lastName){
            resizeView.signField.textValue =   (isSampleSigning) ? currentInpersonSigner?.inpersonLastName() : UserManager.shared.currentUser?.lastName
            resizeView.signField.fieldName =   "sign.mobile.field.lastname".ZSLString
        }else{
            resizeView.signField.textValue = (isSampleSigning) ? currentInpersonSigner?.name : UserManager.shared.currentUser?.fullName
            resizeView.signField.fieldName =   "sign.mobile.field.fullname".ZSLString
        }
        resizeView.updateFieldName()
        resizeView.adjustFrames()
    }
    
    public func resizeViewDidBeginEditing(_ resizeView: ZResizeView) {
        focusSubView(resizeView)
    }
    
    public func resizeViewDidChange(_ resizeView: ZResizeView) {
        horizontalGridLinesLayer.path = UIBezierPath(rect: CGRect(x: -1, y: resizeView.frame.origin.y + resizeableView_OuterMargin, width: self.frame.width + 2, height: resizeView.frame.height - resizeableView_TotalMargin)).cgPath
        
        horizontalGridLinesLayer.opacity = 1;
        
        verticalGridLinesLayer.path = UIBezierPath(rect: CGRect(x: resizeView.frame.origin.x + resizeableView_OuterMargin, y: -1, width: resizeView.frame.width - resizeableView_TotalMargin , height: self.frame.height + 1)).cgPath
            
        verticalGridLinesLayer.opacity = 1;
    }
    
    
    public func resizeViewDidEndEditing(_ resizeView: ZResizeView) {
        hideGridLines()
        
        guard let field  =   resizeView.signField else {
            return
        }
        
        resizeView.signField?.frame = CGRect(x: resizeView.frame.origin.x + resizeableView_OuterMargin, y: resizeView.frame.origin.y + resizeableView_OuterMargin, width: resizeView.frame.width - resizeableView_TotalMargin, height: resizeView.frame.height - resizeableView_TotalMargin)
        
        
        resizeView.signField?.xValue        =   (field.frame.origin.x / self.frame.width) * 100;
        resizeView.signField?.yValue        =   (field.frame.origin.y / self.frame.height) * 100;
        resizeView.signField?.width         =   (field.frame.width / self.frame.width) * 100;
        resizeView.signField?.height        =   (field.frame.height / self.frame.height) * 100;
        
        
        delegate?.editorBaseViewFieldDetailsUpdated(editorView: self, field: resizeView.signField)
    }
    
    public func resizeViewDeleteButtonTapped(_ resizeView: ZResizeView) {
        delegate?.editorBaseViewFieldDeleted(editorView: self, field: resizeView.signField)
        endEditing()
        resizeView.removeFromSuperview()
    }
    
    public func resizeViewDidTapped(_ resizeView: ZResizeView) {
        switch (resizeView.signField.type) {
            case .signature:
                signButtonClicked(resizeView, type: .signature)
                break;
            case .initial:
                signButtonClicked(resizeView, type: .initial)
                break;
            case .signDate:
                signDateButtonClicked(resizeView)
                break;
            case .date:
                dateButtonClicked(resizeView)
                break;
            case .dropDown:
                dropDownFieldClicked(resizeView)
                break;
            case .checkBox:
                checkboxButtonClicked(resizeView)
                break;
            case .attachment:
                delegate?.editorBaseViewAttachmentFieldTapped(editorView: self, signView: resizeView)
                break;
            default:
                textFieldButtonClicked(resizeView)
                break;
        }
    }
    
    public func resizeViewDidEditButtonTapped(_ resizeView: ZResizeView) {
        guard let field  =   resizeView.signField else {
            return
        }
        
        if field.type == .dropDown {
            let dropDown = ZSDropdownController(style: UITableView.Style.grouped)
            if field.defaultValue != nil {
                dropDown.selectedValue = field.defaultValue ?? ""
            }
            dropDown.fieldName = field.fieldName ?? ""
            var optArr: [DropdownOption] = []
            if let dropdownFields = field.dropdownFields {
                for dict in dropdownFields {
                    guard let dict = dict as? [AnyHashable : Any] else {
                        continue
                    }
                    var opt = DropdownOption()
                    opt.dropdownOptionId = dict[keyPath:"dropdown_value_id"]
                    opt.dropdownValue = dict[keyPath:"dropdown_value"]
                    opt.dropdownOrder = dict[keyPath:"dropdown_order"]
                    ZInfo("dd %i", opt.dropdownOrder)
                    
                    optArr.append(opt)
                }
            }
            dropDown.optionsArray = optArr
            dropDown.completionHandler = { fieldName, options, selectedValue in
                resizeView.signField?.fieldName = fieldName
                resizeView.signField?.defaultValue = selectedValue
                
                var optionsArr: [AnyHashable] = []
                for opt in options {
                    
                    if let optionId = opt.dropdownOptionId{
                        optionsArr.append([
                            //                            "dropdown_order": NSNumber(value: opt.dropdownOrder),
                            "dropdown_order": opt.dropdownOrder ,
                            "dropdown_value": opt.dropdownValue,
                            "dropdown_value_id": optionId
                        ])
                    }else{
                        optionsArr.append([
                            //                            "dropdown_order": NSNumber(value: opt.dropdownOrder),
                            "dropdown_order": opt.dropdownOrder,
                            "dropdown_value": opt.dropdownValue
                        ])
                    }
                }
                resizeView.signField?.dropdownFields = optionsArr
                resizeView.signField = field
                
            }
            let nav = UINavigationController(rootViewController: dropDown)
            
            if DeviceType.isIpad {
                nav.modalPresentationStyle = .popover
                nav.popoverPresentationController?.sourceRect = resizeView.frame
                nav.popoverPresentationController?.sourceView = self
                nav.popoverPresentationController?.permittedArrowDirections = [.down, .up]
            } else if !SwiftUtils.isIOS13AndAbove {
                nav.modalPresentationStyle = .custom
                nav.transitioningDelegate = transitionManager
            }
            superVC?.present(nav, animated: true)
        }
    }
    
}

extension DocumentEditorBaseView: ZRadioGroupViewDelegate {
    
    public func radioGroupBeginEditing(onRadioGroupView radioGroupView: ZRadioGroupView) {
        endEditing()
        focusSubView(radioGroupView)
    }
    
    public func radioGroupEditing(onRadioGroupView radioGroupView: ZRadioGroupView) {
        
    }
    
    public func radioGroupEndEditing(_ radioGroupView: ZRadioGroupView,onRadioItem radioItem: ZRadioGroupItem) {
        selectedField = radioGroupView.signField
        delegate?.editorBaseViewFieldDetailsUpdated(editorView: self, field: selectedField!)
    }
    
    public func radioGroupDidTappedEditButton(_ radioGroupView: ZRadioGroupView,onRadioItem radioItem: ZRadioGroupItem) {
        radioGroupBeginEditing(onRadioGroupView: radioGroupView)
        addRadioGroupField(withRadioGroupView: radioGroupView)
    }
    
    public func radioGroupDidTapped(onRadioGroupView radioGroupView: ZRadioGroupView) {
        if radioGroupView.resizeViewMode == .receivedDoc_completed {
            valueChangeHandling(field: &radioGroupView.signField, markComplete: true)
        }
    }
}

extension DocumentEditorBaseView: ZRadioGroupControllerdelegate {
    func radioGroupUpdated(associatedWith view: ZRadioGroupView) {
        selectedField = view.signField
        delegate?.editorBaseViewFieldDetailsUpdated(editorView: self, field: selectedField!)
    }
    
    func radioGroupCreated(withValue radiogroup: RadioGroup) {
        addSignField(fieldType: .radioGroup, at: lastTouchPoint, name: radiogroup.name, fieldValue: nil, dropDownOptions: nil, radioGroup: radiogroup)
    }
    
    func deleteRadiogroup(associatedWith view: ZRadioGroupView) {
        delegate?.editorBaseViewFieldDeleted(editorView: self, field: view.signField)
        endEditing()
        view.removeFromSuperview()
    }
}
