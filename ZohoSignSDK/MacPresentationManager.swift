//
//  MacPresentationManager.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 21/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit
public enum MacPresentationDirection {
    case formSheet
    case pageSheet
    case top
    case topWithCustomHeight
}

public class MacPresentationManager: NSObject {
    public var direction: MacPresentationDirection = .pageSheet
    public var isBlurredBackground = false
    public var popupHeight: CGFloat = 420
    var presentationController: MacPresentationController!
}

extension  MacPresentationManager: UIViewControllerTransitioningDelegate {
    
    public func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        presentationController = MacPresentationController(presentedViewController: presented, presentingViewController: presenting, direction: direction, isBlurredBackground: isBlurredBackground)
        presentationController.popupHeight = popupHeight
        return presentationController
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return MacPresentationAnimator(direction: direction, isPresentation: false)
    }
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return MacPresentationAnimator(direction: direction, isPresentation: true)
    }
}
