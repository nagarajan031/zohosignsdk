//
//  ZSPopTip.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 29/04/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit
import AMPopTip

public class ZSPopTip: NSObject {

    static var  poptip : PopTip = {
        let popTip = PopTip()
        popTip.font = ZSFont.bodyFont()
        popTip.shouldDismissOnTap = false
        popTip.shouldDismissOnTapOutside = true
        popTip.bubbleColor = ZColor.notifyBgColor
        popTip.arrowSize = CGSize(width: 15, height: 10)
        popTip.cornerRadius = 10
        popTip.layer.zPosition = 9999
        return popTip
    }()
   
    @objc public static func dismissPoptip(){
        poptip.hide()
        SwiftUtils.dismissPoptip()
    }
    
    @objc public static func showInfoTip(withMessage message:String, inView view:UIView, onRect rect:CGRect, direction : Int = 0) {//0 = none , 1 = up, 2 = down
        switch direction {
        case 0:
            SwiftUtils.showInfoTip(withMessage: message, inView: view, onRect: rect, poptipDirection: .none)
            break
        case 1:
            SwiftUtils.showInfoTip(withMessage: message, inView: view, onRect: rect, poptipDirection: .up)
            break
        default:
            break
        }
    }
    
    @objc public static func showMessage(_ message:String, inView view:UIView, onRect rect:CGRect, direction : Int = 0) {//0 = none , 1 = up, 2 = down
        switch direction {
        case 0:
            SwiftUtils.showPopTip(withMessage: message, inView: view, onRect: rect, poptipDirection: .none)
            break
        case 1:
            SwiftUtils.showPopTip(withMessage: message, inView: view, onRect: rect, poptipDirection: .up)
            break
        default:
            break
        }
    }
    
    @objc public static func showCustomView(_ customView:UIView, inView view:UIView, onRect rect:CGRect, direction : Int = 0) {//0 = none , 1 = up, 2 = down
        poptip.show(customView: customView, direction: (direction == 1) ? .up : .down, in: view, from: rect)
    }
}
