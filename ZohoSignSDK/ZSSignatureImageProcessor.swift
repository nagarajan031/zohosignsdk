//
//  ZSSignatureImageProcessor.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 23/09/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit
import CoreImage

struct Color {
    let red: UInt8
    let green: UInt8
    let blue: UInt8
    var alpha: UInt8 = 255
    
    init(red: UInt8, green: UInt8, blue: UInt8) {
        self.red = red
        self.green = green
        self.blue = blue
    }
    
    init(red: UInt8, green: UInt8, blue: UInt8, alpha: UInt8) {
        self.init(red: red,green: green,blue: blue)
        self.alpha = alpha
    }
}

public class ZSSignatureImageProcessor: NSObject {
    public var image: UIImage!
    let bytesPerPixel = 4
    let bitsPerComponent = 8
    let contrastFactor: Float = 1
    var dataPointer: UnsafeMutableRawPointer!
    public typealias ImageBackgroundRemovalCompletionHandler = (UIImage) -> Void
    
    var initialCGImage: CGImage {
        return image.cgImage!
    }
    
    var imageWidth: Int {
        return initialCGImage.width
    }
    
    var imageHeight: Int {
        return initialCGImage.height
    }
    
    var bytesPerRow: Int {
        return bytesPerPixel * initialCGImage.width
    }
    
    var bitmapByteCount: Int {
        return bytesPerRow * initialCGImage.height
    }
    
    let colorSpace = CGColorSpaceCreateDeviceRGB()
    
    /// It replace the given UIImage's backgroung with the given color
    /// - Parameter color: The color to be applied to the background
    /// - Parameter tolerance: It indicated the amount of color intensity that needs to applied. The higher the value the more intense it will be.
    @discardableResult
    public func replaceBackground(with color: UIColor, tolerance: Int = 127, completion: @escaping ImageBackgroundRemovalCompletionHandler){
        guard let pointer: UnsafeMutableRawPointer = malloc(bitmapByteCount) else {
            completion(image)
            return
        }
        self.dataPointer = pointer
        
        let context = getCGContext(dataPointer: dataPointer)
        context?.draw(initialCGImage, in: CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight))
        
        let toCGColor = color.cgColor
        let toComponents = toCGColor.components!
        
        var strongSelf = self
        globalUserInitiatedQueue {[weak self] in
            self?.runLoopForReplacebackground(tolerance: tolerance, toComponents: toComponents)
            mainQueue {
                completion(strongSelf.createUIImage(from: context?.makeImage()))
            }
        }
    }
    
    func runLoopForReplacebackground(tolerance: Int, toComponents: [CGFloat]) {
        var byteIndex = 0
        
        while byteIndex < bitmapByteCount {
            let color = createColor(dataPointer: dataPointer, byteIndex: byteIndex)
            
            let dr = abs(Int(color.red) - 255)
            let dg = abs(Int(color.green) - 255)
            let db = abs(Int(color.blue) - 255)
            
            if dr < tolerance, dg < tolerance, db < tolerance {
                storeBytes(dataPointer: dataPointer, byteIndex: byteIndex, red: UInt8(toComponents[0] * 255.0), green: UInt8(toComponents[1] * 255.0), blue: UInt8(toComponents[2] * 255.0), alpha: UInt8(toComponents[3] * 255.0))
            }
            byteIndex += 4
        }
    }
    
    /// It apllies brightness effect to the given UIImage
    /// - Parameter brightNess: value ranges from 0.0 to 1.0
    @discardableResult
    public func image(with brightNess: Float, completion: @escaping ImageBackgroundRemovalCompletionHandler){
        guard let pointer: UnsafeMutableRawPointer = malloc(bitmapByteCount) else {
            completion(image)
            return
        }
        dataPointer = pointer
        
        let context = CGContext(data: dataPointer, width: imageWidth, height: imageHeight, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue|CGBitmapInfo.byteOrder32Big.rawValue)
        context?.draw(initialCGImage, in: CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight))
        
        var strongSelf = self
        globalUserInitiatedQueue {[weak self] in
            self?.runLoopForBrightness(brightNess: brightNess)
            mainQueue {
                completion(strongSelf.createUIImage(from: context?.makeImage()))
            }
        }
    }
    
    func runLoopForBrightness(brightNess: Float) {
        var byteIndex = 0
        while byteIndex < bitmapByteCount {
            let color = createColor(dataPointer: dataPointer, byteIndex: byteIndex)
            var red = color.red
            var green = color.green
            var blue = color.blue
            
            red = UInt8(min(255, max(0, roundf(Float(red) + Float(red) * brightNess))))
            green = UInt8(min(255, max(0, roundf(Float(green) + Float(green) * brightNess))))
            blue = UInt8(min(255, max(0, roundf(Float(blue) + Float(blue) * brightNess))))
            
            red = UInt8(min(255, max(0, roundf(1*(Float(red) - 127.5)) + 128)))
            green = UInt8(min(255, max(0, roundf(1*(Float(green) - 127.5)) + 128)))
            blue = UInt8(min(255, max(0, roundf(1*(Float(blue) - 127.5)) + 128)))
            
            storeBytes(dataPointer: dataPointer, byteIndex: byteIndex, red: red, green: green, blue: blue, alpha: color.alpha)
            
            dataPointer.storeBytes(of: red, toByteOffset: byteIndex, as: UInt8.self)
            dataPointer.storeBytes(of: green, toByteOffset: byteIndex+1, as: UInt8.self)
            dataPointer.storeBytes(of: blue, toByteOffset: byteIndex+2, as: UInt8.self)
            byteIndex += 4
        }
    }
    
    /// Applies grayscale effect to the given UIImage.
    /// It aplies formula of RED * 0.299 + GREEN * 0.587 + BLUE * 0.114. The resulting value will be stored in the respective pixel.
    @discardableResult
    public func imageWithGrayScale(completion: @escaping ImageBackgroundRemovalCompletionHandler) {
        guard let pointer: UnsafeMutableRawPointer = malloc(bitmapByteCount) else {
            completion(image)
            return
        }
        dataPointer = pointer
        
        let context = CGContext(data: dataPointer, width: imageWidth, height: imageHeight, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue|CGBitmapInfo.byteOrder32Big.rawValue)
        context?.draw(initialCGImage, in: CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight))
        
        var strongSelf = self
        globalUserInitiatedQueue {[weak self] in
            self?.runLoopForGrayScale()
            mainQueue {
                completion(strongSelf.createUIImage(from: context?.makeImage()))
            }
        }
    }
    
    func runLoopForGrayScale(){
        var byteIndex = 0
        while byteIndex < bitmapByteCount {
            let color = createColor(dataPointer: dataPointer, byteIndex: byteIndex)
            
            let average = UInt8(Float(color.red) * 0.299 + Float(color.green) * 0.587 + Float(color.blue) * 0.114)
            storeBytes(dataPointer: dataPointer, byteIndex: byteIndex, red: average, green: average, blue: average, alpha: color.alpha)
            byteIndex += 4
        }
    }
    
    /// It removes the background of the image by applying brightness adjustment, grayscale and turning of the alpha channel of the pixel contains value more than the thresshold
    /// - Parameter brightnessFactor: determines how much amount of brightness needs to be applied (ranges from 0.0 to 1.0)
    /// - Parameter thresshold: determines how much accurately the background has to be processed. higher the value more pixels will be removed. (ranges from 0 to 255)
    /// - Parameter completion: gives result in UIImage
    @discardableResult
    public func removeBackground(brightnessFactor: Float, thresshold: Int, completion: @escaping ImageBackgroundRemovalCompletionHandler) {
        guard let pointer: UnsafeMutableRawPointer = malloc(bitmapByteCount) else {
            completion(image)
            return
        }
        self.dataPointer = pointer
        
        let context = CGContext(data: dataPointer, width: imageWidth, height: imageHeight, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue|CGBitmapInfo.byteOrder32Big.rawValue)
        context?.draw(initialCGImage, in: CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight))
        
        let toCGColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0).cgColor
        let toComponents = toCGColor.components!
        
        var strongSelf = self
        globalUserInitiatedQueue {[weak self] in
            self?.runLoopForRemoveBacground(thresshold: thresshold, brightnessFactor: brightnessFactor, toComponents: toComponents)
            mainQueue {
                completion(strongSelf.createUIImage(from: context?.makeImage()))
            }
        }
    }
    
    func runLoopForRemoveBacground(thresshold: Int, brightnessFactor: Float, toComponents: [CGFloat]) {
        var byteIndex = 0
        while byteIndex < bitmapByteCount {
            let color = createColor(dataPointer: dataPointer, byteIndex: byteIndex)
            var red = color.red
            var green = color.green
            var blue = color.blue
            
            /*
            //adjust brightness to separate out the signature from the background
            red = UInt8(min(255, max(0, roundf(Float(red) + Float(red) * brightnessFactor))))
            green = UInt8(min(255, max(0, roundf(Float(green) + Float(green) * brightnessFactor))))
            blue = UInt8(min(255, max(0, roundf(Float(blue) + Float(blue) * brightnessFactor))))
            
            red = UInt8(min(255, max(0, roundf(contrastFactor*(Float(red) - 127.5)) + 128)))
            green = UInt8(min(255, max(0, roundf(contrastFactor*(Float(green) - 127.5)) + 128)))
            blue = UInt8(min(255, max(0, roundf(contrastFactor*(Float(blue) - 127.5)) + 128)))
            
            storeBytes(dataPointer: dataPointer, byteIndex: byteIndex, red: red, green: green, blue: blue, alpha: color.alpha)
            
            //apply grayScale effect to invert colors to shades of black
            
            let average = UInt8(Float(red) * 0.299 + Float(green) * 0.587 + Float(blue) * 0.114)
            storeBytes(dataPointer: dataPointer, byteIndex: byteIndex, red: average, green: average, blue: average, alpha: color.alpha)
            */
            
            //now turn off the alpha channels of color codes having values less than the thresshold
            let dr = abs(Int(red) - 255)
            let dg = abs(Int(green) - 255)
            let db = abs(Int(blue) - 255)
            
            if dr < thresshold, dg < thresshold, db < thresshold {
                storeBytes(dataPointer: dataPointer, byteIndex: byteIndex, red: UInt8(toComponents[0] * 255.0), green: UInt8(toComponents[1] * 255.0), blue: UInt8(toComponents[2] * 255.0), alpha: UInt8(toComponents[3] * 255.0))
            } else {
                storeBytes(dataPointer: dataPointer, byteIndex: byteIndex, red: 0, green: 0, blue: 0, alpha: 255)
            }
            
            byteIndex += 4
        }
    }
}

//MARK: UTIL METHODS
extension ZSSignatureImageProcessor {
    func getCGContext(dataPointer: UnsafeMutableRawPointer) -> CGContext? {
        return CGContext(data: dataPointer, width: imageWidth, height: imageHeight, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue|CGBitmapInfo.byteOrder32Big.rawValue)
    }
    
    func storeBytes(dataPointer: UnsafeMutableRawPointer, byteIndex: Int,red: UInt8, green: UInt8, blue: UInt8, alpha: UInt8){
        dataPointer.storeBytes(of: red, toByteOffset: byteIndex, as: UInt8.self)
        dataPointer.storeBytes(of: green, toByteOffset: byteIndex+1, as: UInt8.self)
        dataPointer.storeBytes(of: blue, toByteOffset: byteIndex+2, as: UInt8.self)
        dataPointer.storeBytes(of: alpha, toByteOffset: byteIndex+3, as: UInt8.self)
    }
    
    func createColor(dataPointer: UnsafeMutableRawPointer, byteIndex: Int) -> Color {
        let red = loadByte(dataPointer: dataPointer, byteIndex: byteIndex)
        let green = loadByte(dataPointer: dataPointer, byteIndex: byteIndex+1)
        let blue = loadByte(dataPointer: dataPointer, byteIndex: byteIndex+2)
        let alpha = loadByte(dataPointer: dataPointer, byteIndex: byteIndex+3)
        let color = Color(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func loadByte(dataPointer: UnsafeMutableRawPointer, byteIndex: Int) -> UInt8{
        return dataPointer.load(fromByteOffset: byteIndex, as: UInt8.self)
    }
    
    func createUIImage(from cgImage: CGImage?) -> UIImage {
        if let finalCGImage = cgImage {
            let finalImage = UIImage(cgImage: finalCGImage)
            self.image = finalImage
        }
        free(dataPointer)
        return self.image
    }
}



