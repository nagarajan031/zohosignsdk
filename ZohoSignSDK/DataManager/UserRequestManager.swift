//
//  UserRequestManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 16/08/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class UserRequestManager: URLRequestManager {

    public func addAccount(orgName: String, adKeyword: String?, adGroup: String?, adCampaign : String?, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler) {
        var params = ["org_name":orgName]
        if !(adKeyword ?? "").isEmpty {
            params["ad_keyword"] = adKeyword
        }
        if !(adGroup ?? "").isEmpty {
            params["ad_group"] = adGroup
        }
        if !(adCampaign ?? "").isEmpty {
            params["ad_campaign"] = adCampaign
        }
        
        request(url: accountsApi, method: .post, parameters: params, success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .userRequest(.accountCreated))
            var dict = Dictionary<AnyHashable, Any>()
            success(dict["accounts"])
        }, failure: failure)
        
    }
    
    public func getAccountDetails(success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        request(url: accountsApi, method: .get, parameters: nil, success: { (responseObj) in
            success(responseObj["accounts"])
        }, failure: failure)
    }
    
    public func getCurrentUserDetails(success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        request(url: getCurrentUserDetailApi, method: .get, parameters: nil, success: { (responseObj) in
            success(responseObj["users"])

        }, failure: failure)
    }
    
    public func getUserAccountSettings(success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        request(url: getAccountSettingsApi, method: .get, parameters: nil, success: { (responseObj) in
            success(responseObj["settings"])
        }, failure: failure)
    }
    
    public func updateUserProfileDetails(params: [String : String], progress: @escaping DataManagerProgressHandler, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        

        
        let paramstring    =   ["profile_details" : params].jsonString()
        
        var signatureImageData : Data?
        var  initialImageData  : Data?
        
        if  let signImage  =  UserManager.shared.currentUser?.getSignatureImage() {
            signatureImageData = signImage.compressSignatureData()
        }
        if  let initialImage  = UserManager.shared.currentUser?.getInitialImage() {
            initialImageData = initialImage.compressSignatureData()
        }
        
        upload(url: profileUpdateApi, method: .put, multiPartData: { (multipartFormData) in
            multipartFormData.append(paramstring.data(using: .utf8) ?? Data(), withName: "data")

            if let signData =  signatureImageData{
                multipartFormData.append(signData, withName: "signature", fileName: "signature", mimeType: "image/png")
            }
            if  let initialData =  initialImageData{
                multipartFormData.append(initialData, withName: "initial", fileName: "initial", mimeType: "image/png")
            }

        }, parameters: params, progress: {progress in
            
        }, success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .userRequest(.userUpdated))
            success(responseObj)
        }, failure: failure)
    }
    
    
    
    //MARK:- Notifications
    
    public func getInstallationId(success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        request(url: getInstallationIDApi, method: .post, parameters: nil, success: { (responseObj) in
            if  let deviceSpec = responseObj["device_specs"]  as? [String: String], let installationId = deviceSpec["INSTALLATIONID"]{
                success(installationId)
            }else{
                failure(self.dataParsingError)
            }
        }, failure: failure)
    }
    
    public func registerNotification(devToken : String, installationID: String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler) {
        // to ignore annoying error msg for inhouse distribution
        if Bundle.main.bundleIdentifier ==  "com.zoho.inhouse.sign" {
            return;
        }
        
        guard let bundleId = Bundle.main.bundleIdentifier?.replacingOccurrences(of:  "maccatalyst.", with: "") else{
            return
        }
        
        let values = ["OSCODE" : "IOS",
                      "APPID" : bundleId,
                      "NFID" : devToken,
                      "INSID" : installationID,
                      "SINFO" : UIDevice.current.systemVersion,
                      "DINFO" : UIDevice.current.model]
        
        let outString = ["mobile_details":values].jsonString()
        
        
        request(url: registerForPushNotificationApi, method: .post, parameters: ["data":outString], success: { (responseObject) in
            success(responseObject)
        }, failure: failure)
    }

    public func unregisterNotification(devToken : String, installationID: String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler) {
        // to ignore annoying error msg for inhouse distribution
        if Bundle.main.bundleIdentifier ==  "com.zoho.inhouse.sign" {
            return;
        }
        
        guard let bundleId = Bundle.main.bundleIdentifier?.replacingOccurrences(of:  "maccatalyst.", with: "") else{
            return
        }
        
        let values = ["OSCODE" : "IOS",
                      "APPID" : Bundle.main.bundleIdentifier,
                      "NFID" : devToken,
                      "INSID" : installationID,
                      "SINFO" : UIDevice.current.systemVersion,
                      "DINFO" : UIDevice.current.model]
        
        let outString = ["mobile_details":values].jsonString()
        
        request(url: unregisterForPushNotificationApi, method: .post, parameters: ["data":outString], success: { (responseObject) in
            success(responseObject)
        }, failure: failure)
    }
}
