//
//  SummaryRequestManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 27/11/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class SummaryRequestManager: URLRequestManager {
    public func getRequestCount(success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        request(url: getRequestsCountApi, method: .get, parameters: nil, success: { (responseObj) in
            success(responseObj["request_count"])
        }, failure: failure)
    }
}
