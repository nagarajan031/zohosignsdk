//
//  TemplateRequestManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 26/09/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class TemplateRequestManager: URLRequestManager {

    
    //MARK:- Actions
    public func createNewTemplate(params : [String : Any], success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        
        let dict =  ["templates" :params].jsonString()
        
        request(url: templatesApi, method: .post, parameters: ["data":dict], success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .templateRequest(.created))

            success(responseObj["templates"])
        }, failure: failure)
    }
    
    public func updateTemplate(id : String,params : [String : Any], success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        let dict =  ["templates" :params].jsonString()
        
        request(url: String(format: updateTemplateApi, id), method: .put, parameters: ["data":dict], success: { (responseObj) in
            if let dict = responseObj["templates"] as? [AnyHashable : Any]{
                ZSCoreDataManager.shared.updateTemplate(withId: id, fromDict: dict)
                ZSAnalytics.shared.track(forEvent: .templateRequest(.updated))
                success(responseObj)
            }else{
                failure(self.dataParsingError)
            }
        }, failure: failure)
    }
    
    public func getTemplateDetail(id : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        request(url: String(format: templateDetailApi, id), method: .get, parameters: nil, success: { (responseObj) in
            if let dict = responseObj["templates"] as? [AnyHashable : Any]{
                ZSCoreDataManager.shared.updateTemplate(withId: id, fromDict: dict)
                success(responseObj)
            }else{
                failure(self.dataParsingError)
            }
        }, failure: failure)
    }
    
    
    
    public func createRequest(templateId : String, isQuickSend : Bool, params: [String : Any], success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        let dict =  ["templates" :params].jsonString()

        request(url: String(format: requestFromTemplateApi , templateId, (isQuickSend ? "true" : "false")), method: .post, parameters: ["data" : dict], success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: isQuickSend ? .templateRequest(.quickSend) : .templateRequest(.use))
            success(responseObj["requests"])
        }, failure: failure)
    }
    
    public func deleteTemplate(id : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        request(url: String(format: deleteTemplateApi, id), method: .put, parameters: nil, success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .templateRequest(.deleted))
            ZSCoreDataManager.shared.deleteTemplate(withIds: id)
            success(responseObj)
        }, failure: failure)
    }
    
}
