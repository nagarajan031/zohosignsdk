//
//  DocumentsListRequestManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 28/08/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class DocumentsListRequestManager: URLRequestManager {

    //    func addSentRequestsList(filterName: String = STATUS_ALL, startIndex: Int = 0, needThumbnail : Bool = false, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler) {

    
    public func getSentRequestsList(filterName: String = STATUS_ALL,searchStr : String = "", startIndex: Int = 0,isThumbnailNeeded : Bool = true,isExtension : Bool = false, row_count: Int =  LIST_ROW_COUNT, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler) {
        
        let sortColumn =  "modified_time";
        let sortOrder  =  "DESC";
        var rowCount = row_count
        
        var dict : [String : Any] = [ "sort_column" : sortColumn,
             "sort_order":  sortOrder,
             "start_index": NSNumber(value: startIndex),
             "row_count"  : rowCount,
             "search_columns" : ["request_name" : searchStr]
            ]
        
        if(searchStr.isEmpty){
            dict.removeValue(forKey: "search_columns")
        }
        
        let dictStr =  ["page_context" : dict].jsonString()
        
        var params =  ["data":dictStr]
        if isThumbnailNeeded {
            params["is_thumbnail"] = "true"
        }

        if ((filterName != STATUS_ALL) && (filterName != REQUEST_FILTER_SEARCH) ) {
            params["request_status"] = filterName
        }
        
        request(url: getSentRequestsApi, method: .get, parameters: params, success: { (responseObj) in
//            if let parseDict = responseObj["requests"] as? [[String : Any]]{
//                ZSCoreDataManager.shared.addSignRequests(objects: parseDict, filterKey: filterName)
//            }
//            NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: "root")
            ZSCoreDataManager.shared.addSignRequests(fromResponseObject: responseObj["requests"] , forFilter: filterName, shouldRemovePreviousElements: startIndex <= 1)
            success(responseObj)
        }, failure: failure)
        
    }
    
    public func getReceivedRequestsList(filterName: String = STATUS_ALL,searchStr : String = "", startIndex: Int = 0, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler) {
        
        let sortColumn =  "requested_time";
        let sortOrder  =  "DESC";
        
        var dict : [String:Any] = [ "sort_column" : sortColumn,
                                    "sort_order":  sortOrder,
                                    "search_columns" : ["my_request_name" : searchStr],
                                    "start_index": NSNumber(value: startIndex),
                                    "row_count"  :(isExtension) ? NSNumber(value: 5) : NSNumber(value: LIST_ROW_COUNT)
        ]
        
        if(searchStr.isEmpty){
            dict.removeValue(forKey: "search_columns")
        }
        
        let dictStr =  ["page_context" : dict].jsonString()
        
        var params =  ["data":dictStr]
        
        if ((filterName != STATUS_ALL) && (filterName != REQUEST_FILTER_SEARCH) ) {
            params["request_status"] = filterName
        }
        
        request(url: getReceivedRequestsApi, method: .get, parameters: params, success: { (responseObj) in
            ZSCoreDataManager.shared.addMyRequests(fromResponseObject: responseObj["my_requests"], forFilter: filterName, shouldRemovePreviousElement: startIndex <= 1)
            success(responseObj)
        }, failure: failure)
        
    }
    
    public func getTemplatesList(startIndex: Int = 0,searchparams : [String : String] , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler) {
        
        let sortColumn =  "modified_time";
        let sortOrder  =  "DESC";
        
        let dict =  ["page_context" :
            ["sort_column" : sortColumn,
             "sort_order":  sortOrder,
             "search_columns":searchparams,
             "start_index": NSNumber(value: startIndex),
             "row_count"  : NSNumber(value: LIST_ROW_COUNT)]].jsonString()
        
        var params =  ["data":dict]
        params["is_thumbnail"] = "true"

        request(url: templatesApi, method: .get, parameters: params, success: { (responseObj) in
            if (searchparams.count < 1){
                ZSCoreDataManager.shared.addTemplatesList(fromResponseObject: responseObj["templates"], shouldRemovePreviousElements: startIndex <= 1)
            }else{
                ZSCoreDataManager.shared.addSearchTemplates(fromResponseObject: responseObj["templates"])
            }
            success(responseObj)
        }, failure: failure)
        
    }
}

