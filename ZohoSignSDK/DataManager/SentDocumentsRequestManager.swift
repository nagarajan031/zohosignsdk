//
//  DocumentsRequestManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 27/08/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class SentDocumentsRequestManager: URLRequestManager {


    //MARK:- Create Req
   public func createNewRequest(params : [String : Any], success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        
        let dict =  ["requests" :params].jsonString()
        
        request(url: addNewRequestApi, method: .post, parameters: ["data":dict], success: { (responseObj) in
            success(responseObj["requests"])
        }, failure: failure)
    }

   public func updateDraftRequest(reqID: String ,params : [String : Any], success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        
        let dict =  ["requests" :params].jsonString()
   
        request(url: String(format:updateDraftRequestApi, reqID) , method: .put, parameters: ["data":dict], success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .sentDocuments(.requestUpdated))

            success(responseObj["requests"])
        }, failure: failure)
    }
    
    //MARK: - Submit Request
    
   public func submitSelfSignRequest(reqID : String, params : [String : Any], success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        let dict =  ["requests" :params].jsonString()
        
        var signatureImageData : Data?
        var  initialImageData  : Data?

        if (UserManager.shared.currentUser?.isUserProfileEdited == true){
            if  let signImage  =  UserManager.shared.currentUser?.getSignatureImage() {
                signatureImageData = signImage.compressSignatureData()
            }
            if  let initialImage  =  UserManager.shared.currentUser?.getInitialImage() {
                initialImageData = initialImage.compressSignatureData()
            }
        }
        
        upload(url: String(format: submitSelfSignRequestApi, reqID), method: .post, multiPartData: { (multipartFormData) in
            multipartFormData.append(dict.data(using: .utf8) ?? Data(), withName: "data")

            if let signData =  signatureImageData{
                multipartFormData.append(signData, withName: "signature", fileName: "signature", mimeType: "image/png")
            }
            if  let initialData =  initialImageData{
                multipartFormData.append(initialData, withName: "initial", fileName: "initial", mimeType: "image/png")
            }
            
        }, parameters: params as? [String : String], progress: {progress in
            
        }, success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .sentDocuments(.requestCreatedSelfSign))

            success(responseObj)
        }, failure: failure)
    }
    
    public func submitRequest(reqID : String, params : [String : Any], success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        let dict =  ["requests" :params].jsonString()
        
        request(url: String(format: submitRequestApi, reqID), method: .post, parameters: ["data" : dict], success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .sentDocuments(.requestCreatedSendForSign))

            success(responseObj)
        }, failure: failure)
    }
    
    public func cloneRequest(reqID : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
                
        request(url: String(format: cloneRequestApi, reqID), method: .post, parameters:nil, success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .sentDocuments(.cloneDocument))

            success(responseObj["requests"])
        }, failure: failure)
    }

    public func getRequestFieldTypes(success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        request(url: getFieldTypesApi, method: .get, parameters: nil, success: { (responseObj) in
            
            var array  : [ZSFieldType] = []
            if let responseDict  = responseObj["field_types"] as? [[String : AnyObject]]{
                for dict in responseDict {
                    let fieldType = ZSFieldType()
                    fieldType.update(withDict: dict)
                    array.append(fieldType)
                }
                
                let jsonEncoder = JSONEncoder()
                do{
                    let archievedData = try jsonEncoder.encode(array)
                    UserDefaults.standard.set(archievedData, forKey: kUserDefaultkeyFieldType)
                    UserDefaults.standard.synchronize()
                }catch{
                    print(error)
                }
            }
            success(array)
            
        }, failure: failure)
        
    }
    
    

    
    public func getAllDocumentTypes(success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        request(url: requestTypesApi, method: .get, parameters: nil, success: { (responseObj) in
            
            var array  : [ZSRequestType] = []
            if let responseDict  = responseObj["request_types"] as? [[String : AnyObject]]{
                for dict in responseDict {
                    let type = ZSRequestType(typeId: dict[keyPath:"request_type_id"] ?? "", name: dict[keyPath:"request_type_name"] ?? "")
                    array.append(type)
                }
            }
            success(array)
            
        }, failure: failure)
        
    }
    
    public func addNewDocType(name: String,  success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        let dict =  ["request_types" :["request_type_name":name]].jsonString()
        
        request(url: requestTypesApi, method: .post, parameters: ["data" : dict], success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .sentDocuments(.newDocumentTypeCreated))

            if let arr  = responseObj["request_types"] as? [[String : String]], let dict = arr.first{
                let type = ZSRequestType(typeId: dict[keyPath:"request_type_id"] ?? "", name: dict[keyPath:"request_type_name"] ?? "")
                success(type)
            }
            else{
                failure(nil)
            }
            
        }, failure: failure)
    }
    
    
    public func getAllFolders(success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        request(url: foldersApi, method: .get, parameters: nil, success: { (responseObj) in
            
            var array  : [ZSFolder] = []
            if let responseDict  = responseObj["folders"] as? [[String : AnyObject]]{
                for dict in responseDict {
                    let folder = ZSFolder(folderId: dict[keyPath: "folder_id"] ?? "", name: dict[keyPath: "folder_name"] ?? "")
//                    folder.folderId = dict[keyPath: "folder_id"]
//                    folder.folderName = dict[keyPath: "folder_name"]
                    array.append(folder)
                }
            }
            success(array)
            
        }, failure: failure)
        
    }
    
   public func addNewFolder(name: String,  success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        let dict =  ["folders" :["folder_name":name]].jsonString()

        request(url: foldersApi, method: .post, parameters: ["data" : dict], success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .sentDocuments(.newFolderCreated))

            if let dict  = responseObj["folders"] as? [String : String]{
                let folder = ZSFolder(folderId: dict[keyPath: "folder_id"] ?? "", name: dict[keyPath: "folder_name"] ?? "")

//                let folder = ZSFolder()
//                folder.folderId = dict["folder_id"]
//                folder.folderName = dict["folder_name"]
                success(folder)
            }
            else{
                failure(nil)
            }
            
        }, failure: failure)
    }
    
    //MARK:- Details
    public func getSignRequestDetails(_ reqID : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        request(url: String(format: getSignRequestDetailsApi, reqID), method: .get, parameters: nil, success: { (responseObj) in
            success(responseObj["requests"])
        }, failure: failure)
    }
    
    public func getRequestActivities(_ reqID : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        request(url: String(format: getSignRequestAuditApi, reqID), method: .get, parameters: nil, success: { (responseObj) in
            
            success(responseObj["account_activity_report"])
        }, failure: failure)
    }
    
    public func getRequestActivitiesLocation(_ reqID : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        request(url: String(format: getSignRequestMapsAuditApi, reqID), method: .get, parameters: nil, success: { (responseObj) in
            
            success(responseObj["requests"])
        }, failure: failure)
    }
    
    //MARK:- Actions
    
    public func deleteRequest(reqID : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        request(url: String(format: deleteRequestApi, reqID), method: .put, parameters: nil, success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .sentDocuments(.sentRequestDeleted))
            ZSCoreDataManager.shared.deleteSignRequests(withIds: reqID)
            success(responseObj)
        }, failure: failure)
    }
    
    public func restoreRequest(reqID : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        request(url: String(format: restoreRequestApi, reqID), method: .put, parameters: nil, success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .sentDocuments(.sentRequestRestored))
            ZSCoreDataManager.shared.deleteSignRequests(withIds: reqID)
            success(responseObj)
        }, failure: failure)
    }
    
    public func recallRequest(reqID : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        request(url: String(format: recallRequestApi, reqID), method: .post, parameters: nil, success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .sentDocuments(.sentRequestRecalled))
            success(responseObj)
        }, failure: failure)
    }
    
    public func remindRequest(reqID : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        request(url: String(format: remindRequestApi, reqID), method: .post, parameters: nil, success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .sentDocuments(.sentRequestReminded))
            success(responseObj)
        }, failure: failure)
    }
    
    public func mailRequest(reqID : String, toMailIDs : [String], success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        let mailIDStr = String(format: "[%@]" , toMailIDs.joined(separator: ","))
        
        request(url: String(format: emailRequestApi, reqID), method: .post, parameters: ["email_id":mailIDStr], success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .sentDocuments(.sentRequestMailed))
            success(responseObj)
        }, failure: failure)
    }
    
    public func unblockRequest(reqID : String, actionId : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        
        request(url: String(format: resetForActionIDApi, reqID,actionId), method: .put, parameters: nil, success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .sentDocuments(.sentRequestUnblocked))
            success(responseObj["requests"])
        }, failure: failure)
    }
    
    //MARK: - download request
    func downloadRequestAsZip(reqID : String, docId : String,progress: @escaping DataManagerProgressHandler, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler )  {
        download(url: String(format: downloadSignRequestDocAsZipApi, reqID,docId), method: .get, parameters: nil, progress: progress, success: { (response, mimeType)  in
            ZInfo(response ?? "")
            if let data = response as? Data{
                self.saveZipData(docId: docId, data: data)
            }
            success(nil)


        }, failure: failure)
    }
    
    func downloadRequestAsPDF(reqID : String, saveCache: Bool, docId : String,progress: @escaping DataManagerProgressHandler, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler )  {
        download(url: String(format: downloadSignRequestDocAsPDFApi, reqID), method: .get, parameters: nil, progress: progress, success: { (response, mimeType) in
            ZInfo(response ?? "")
            if let data = response as? Data{
                ZSFileManager.shared.saveFileAfterEncryption(forRequestId: reqID, isSentRequests: true, fileData: data, mimeType: mimeType, saveCache: saveCache)
                let filePathArr = ZSFileManager.shared.getSavedFilesPathAfterDecryption(forRequestId: reqID, isSentRequests: true, saveCache: saveCache)
                success(filePathArr)
            }
            success(nil)
            
        }, failure: failure)
    }
    

}
