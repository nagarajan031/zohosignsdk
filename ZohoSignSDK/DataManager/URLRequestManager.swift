//
//  URLRequestManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 16/08/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit
import SSZipArchive
import Alamofire
import Reachability
import SSOKit

public class ZSError : NSObject, Error {
    public var localizedDescription : String = ""
    public var errorType : ZSErrorType = .generalError
    public var errorCode : Int = 0

    public class func getDataParsingError() -> ZSError{
        let error = ZSError()
        error.errorType = .dataParsingError
        error.localizedDescription = "sign.mobile.error.dataError".ZSLString
        return error
    }
}

@objc public enum ZSErrorType : Int{
    case generalError
    case noDataError
    case timeOut
    case cancel
    case IAMExpiryError
    case dataParsingError
    case noNetworkError
}
//
//struct MultiPartData {
//    var name : String
//    var fileName : String
//    var data : Data
//    var mimeType : String
//}

//enum HTTPMethod : String{
//    case GET = "GET"
//    case PUT = "PUT"
//    case POST = "POST"
//    case MULTPARTPOST = "MULTPARTPOST"
//    case UPDATE = "UPDATE"
//    case DELETE = "DELETE"
//}

public typealias RequestSuccessHandler = (_ response : Dictionary<AnyHashable, Any>) -> ()
public typealias DownloadSuccessHandler = (_ response : Any?,_ mimeType : String?) -> ()
//public typealias DownloadProgressHandler = (_ progress : CGFloat) -> ()
public typealias DataManagerSuccessHandler = (_ response : Any?) -> ()
public typealias DataManagerProgressHandler = (_ progress : Progress?) -> ()
public typealias DataManagerErrorHandler = (_ error : ZSError?) -> ()

public let accountsApi                            =           "/api/v1/accounts"
public let getCurrentUserDetailApi                =           "/api/v1/currentuser"
public let profileUpdateApi                       =           "/api/v1/profile"
public let getGuestUserProfileApi                 =           "/api/guest/v1/requests/%@/actions/%@/profile"
public let getRequestsCountApi                    =           "/api/v1/requests/count";
public let getReportsCountApi                     =           "/api/v1/reports/count";
public let getReportChartApi                      =           "/api/v1/charts";
public let getRecentActivityApi                   =           "/api/v1/recentactivity"
public let searchRequestApi                       =           "/api/v1/search"
public let getSentRequestsApi                     =           "/api/v1/requests";
public let getReceivedRequestsApi                 =           "/api/v1/myrequests"
public let downloadSignRequestDocAsPDFApi               =           "/api/v1/requests/%@/pdf";
public let fileDownloadAsPdfApi                   =           "/api/v1/documents/%@/pdf";
public let templateFileDownloadAsPdfApi           =           "/api/v1/templates/%@/documents/%@/pdf";
public let getSignRequestAuditApi                 =           "/api/v1/requests/%@/audit";
public let getSignRequestMapsAuditApi             =           "/api/v1/requests/%@/mapsaudit";
public let downloadSignRequestDetailPreviewApi    =           "/api/v1/documents/%@"
public let hostDownloadDocApi                     =           "/api/v1/myrequests/%@/pdf";
public let guestGetVerificationCodeApi            =           "/api/v1/guest/actions";
public let guestSendVerificationCodeApi           =           "/api/v1/guest/actions/sendverifycode";
public let guestGetStatelessAuthCodeApi           =           "/api/v1/guest/actions/%@/verify?sign_id=%@&set_cookie=false";
public let guestGetRequestDetailApi               =           "/api/v1/guest/requests/%@/actions/%@";
public let downloadMyRequestDocAsZipApi           =           "/api/v1/guest/requests/%@/actions/%@/documents/%@/zip";
public let downloadMyRequestFileAsPdfApi           =           "/api/v1/guest/requests/%@/actions/%@/documents/%@/cdpdf";
public let downloadMyRequestDocAsPdfApi           =           "/api/v1/guest/requests/%@/actions/%@/pdf";
public let emailGuestDocApi                       =           "/api/v1/myrequests/%@/email"
public let deleteMyRequestApi                     =           "/api/v1/myrequests/%@/delete";
public let restoreMyRequestApi                    =           "/api/v1/myrequests/%@/restore"
public let downloadSignRequestDocAsZipApi         =           "/api/v1/requests/%@/documents/%@/zip"
public let recipientsApi                          =           "/api/v1/contacts";
public let getHostsApi                            =           "/api/v1/signhosts";
public let validateHostEmailApi                   =           "/api/v1/signhosts/validate"
public let getSignRequestDetailsApi               =           "/api/v1/requests/%@"
public let templatesApi                            =           "/api/v1/templates";
public let templateDetailApi                      =           "/api/v1/templates/%@";
public let deleteTemplateApi                      =           "/api/v1/templates/%@/delete";
public let updateTemplateApi                      =           "/api/v1/templates/%@";
public let downloadTemplateDocAsZipApi            =           "/api/v1/templates/%@/documents/%@/zip";
public let requestFromTemplateApi                 =           "/api/v1/templates/%@/createdocument?is_quicksend=%@";
//    documentTemplateDownloadAsPdf       =           "/api/v1/templates/%@/pdf";
public let documentTemplateDownloadAsPdfApi       =           "/api/v1/templates/%@/documents/%@/pdf"
public let requestTypesApi                        =           "/api/v1/requesttypes";
public let getFieldTypesApi                       =           "/api/v1/fieldtypes"
public let foldersApi                             =           "/api/v1/folders"
public let addNewDocumentApi                      =           "/api/v1/documents";
public let rotateDocumentPageApi                      =           "/api/v1/requests/%@/documents/%@/page/%i/rotate";
public let importFileFromCloudServicesApi         =           "/api/v1/documents/import?file_id=%@&source=%@";
public let deleteFileApi                      =           "/api/v1/documents/%@/delete"
public let addNewRequestApi                       =           "/api/v1/requests";
public let updateDraftRequestApi                  =           "/api/v1/requests/%@";
public let submitRequestApi                       =           "/api/v1/requests/%@/submit";
public let cloneRequestApi                        =           "/api/v1/requests/%@/clone";

public let submitSelfSignRequestApi               =           "/api/v1/requests/%@/sign"
public let declineDocumentApi                      =           "/api/v1/guest/requests/%@/actions/%@/reject";
public let forwardDocumentApi                     =           "/api/v1/guest/requests/%@/actions/%@/forward"
public let signDocumentApi                        =           "/api/v1/guest/requests/%@/actions/%@/sign"
public let deleteRequestApi                       =           "/api/v1/requests/%@/delete";
public let recallRequestApi                       =           "/api/v1/requests/%@/recall";
public let restoreRequestApi                      =           "/api/v1/requests/%@/restore";
public let emailRequestApi                        =           "/api/v1/requests/%@/email";
public let remindRequestApi                       =           "/api/v1/requests/%@/remind"
public let feedbackApi                            =           "/api/v1/users/feedback"
public let getInstallationIDApi                   =           "/api/v1/mobile/getinstallationid";
public let registerForPushNotificationApi         =           "/api/v1/mobile/register";
public let unregisterForPushNotificationApi       =           "/api/v1/mobile/unregister"
public let legalTermsApi                          =           "/api/v1/accounts/legalterms"
public let legalTermsForGuestApi                  =           "/api/v1/guest/actions/legalterms";
public let legalTermsForHostApi                   =           "/api/v1/guest/actions/legalterms?sign_id=%@"
public let resetForActionIDApi                    =           "/api/v1/requests/%@/actions/%@/reset"
public let completionCertificateApi               =           "/api/v1/requests/%@/completioncertificate"
public let getAccountSettingsApi                  =           "/api/v1/users/settings"


//attachments
public let  downloadReceivedRequestsAttachmentApi            =      "/api/v1/guest/requests/%@/actions/%@/attachments/%@/pdf"
public let  deleteReceivedRequestsAttachmentApi            =      "/api/v1/guest/requests/%@/actions/%@/attachments/%@/delete"
public let  uploadReceivedRequestsAttachmentApi            =      "/api/v1/guest/requests/%@/actions/%@/fields/%@/addAttachment"


public class ZSURLSession : NSObject {
    
    @objc public static let shared = ZSURLSession()
 
//    lazy var requestURLSession : URLSession = {
//        let configuration = URLSessionConfiguration.default
//        configuration.timeoutIntervalForResource = 30
//        return URLSession(configuration: configuration)
//    }()
    
    lazy var requestManager : SessionManager = {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        return manager
    }()
    
    lazy var requestBackgroundManager : SessionManager = {
        
        let manager =  Alamofire.SessionManager(configuration: URLSessionConfiguration.background(withIdentifier:  "com.zoho.sign.background"))
        manager.session.configuration.timeoutIntervalForRequest = 30
        manager.session.configuration.httpMaximumConnectionsPerHost = 1
        manager.session.configuration.shouldUseExtendedBackgroundIdleMode = true
        return manager
    }()
    
    
    
    lazy var guestRequestManager : SessionManager = {
        let manager = Alamofire.SessionManager()
        manager.session.configuration.timeoutIntervalForRequest = 30
        return manager
    }()
    
    public lazy var reachability : Reachability = {
        let networkReachbility = Reachability.forInternetConnection()
        networkReachbility?.startNotifier()
        return networkReachbility!
    }()
    
    public var guestSignURL = "https://sign.zoho.com";
    
    @objc public func changeBaseDomainForGuest(baseDomain : String?, isUserLoggedIn: Bool) {
 
        switch(ZSignKit.shared.signKitType){
        case .live:
            if isUserLoggedIn{
                guestSignURL = ZIAMManager.transformedURLString("https://sign.zoho.com")
            }else if let  _baseDomain  = baseDomain{
                guestSignURL = "https://sign." + _baseDomain
            }
            else{
                guestSignURL = "https://sign.zoho.com"
            }
            break
        case .localZoho:
//            guestSignURL = "https://signtest.localzoho.com"
            guestSignURL = "https://sign.localzoho.com"
            break
        case .pre:
            guestSignURL = "https://presign.zoho.com"
            break
        case .CSEZ:
            guestSignURL = "https://meod-lin1.csez.zohocorpin.com:8443"
            break
        default:  break
        }
        
    }
}

class Track {
    
    let previewURL: URL
    let index: Int
    var downloaded = false
    
    init(previewURL: URL, index: Int) {
        self.previewURL = previewURL
        self.index = index
    }
    
}


class Download {
    
    var url: String
    init(url: String) {
        self.url = url
    }
    
    // Download service sets these values:
    var task: URLSessionDownloadTask?
    var isDownloading = false
    var resumeData: Data?
    
    // Download delegate sets this value:
    var progress: Float = 0
    
}



public class URLRequestManager :  NSObject {
    
//    fileprivate var dataTasks : [URLSessionDataTask] = []
    var dataTasks : [DataRequest] = []
    var downloadTasks : [DownloadRequest] = []

    private var downloadProgessHandler : DataManagerProgressHandler?
    private var downloadSuccessHandler : DownloadSuccessHandler?
    private var downloadFailureHandler : DataManagerErrorHandler?

    public var isExtension = false
    
    let fileManager = FileManager.default
    //MARK: - Inits
    
    public lazy var guestRequestHeader : [String : String] = {
        
        let infoDict = Bundle.main.infoDictionary
        
        return ["buildNumber" : infoDict!["CFBundleVersion"] as! String,
                "Accept-Language" : SwiftUtils.getDeviceLanguageID(),
                "requestFrom" : "zohosignmobilenative",
                "User-Agent" : ZSignKit.shared.userAgent]
        
    }()

    
    public lazy var header : [String : String] = {
        
        let infoDict = Bundle.main.infoDictionary
        if let key = ZSignKit.shared.apiKey{
            return ["buildNumber" : infoDict!["CFBundleVersion"] as! String,
                    "Accept-Language" : SwiftUtils.getDeviceLanguageID(),
                    "requestFrom" : "zohosignmobilenative",
                    "requestFromSDK" : key,
                    "User-Agent" : ZSignKit.shared.userAgent]
        }
   
        return ["buildNumber" : infoDict!["CFBundleVersion"] as! String,
                "Accept-Language" : SwiftUtils.getDeviceLanguageID(),
                "requestFrom" : "zohosignmobilenative",
                "User-Agent" : ZSignKit.shared.userAgent]
        
    }()
    
    
    internal lazy var noNetworkError : ZSError = {
        let error = ZSError()
        error.errorType = .noNetworkError
        error.localizedDescription = "sign.mobile.alert.noInternetConnection".ZSLString
        return error
    }()
    
    internal lazy var dataParsingError : ZSError = {
        let error = ZSError()
        error.errorType = .dataParsingError
        error.localizedDescription = "sign.mobile.error.dataError".ZSLString
        return error
    }()
    
    
    private func getBaseUrl() -> String  {
        if let user = UserManager.shared.currentUser, let url = user.baseUrl{
            return url
        }
        
        var url = "";
        switch(ZSignKit.shared.signKitType){
            case .live:
                url = ZIAMManager.transformedURLString("https://sign.zoho.com")
                break
            case .localZoho:
                //                url = "https://signtest.localzoho.com"
                url = "https://sign.localzoho.com"
                break
            case .pre:
                url = "https://presign.zoho.com"
                break
            case .CSEZ:
                url = "https://meod-lin1.csez.zohocorpin.com:8443"//"https://vagee-4413.csez.zohocorpin.com:7443"
                break
            default:  break
        }
        
        UserManager.shared.currentUser?.baseUrl = url
   
        return url
    }
     
    @objc public func isConnectedToInternet() ->Bool {
        return ZSURLSession.shared.reachability.isReachable()
    }
    
    @objc public func currentNetworkStatus() ->NetworkStatus {
        return ZSURLSession.shared.reachability.currentReachabilityStatus()
    }
    
    public func getFullUrl(_ url : String) -> String{
        if (url.contains("zoho.com") || url.contains("zoho.eu")) { return url }
        return getBaseUrl() + url
    }
    
    public func getBaseGuestUrl(_ url : String, isHost : Bool = false) -> String{
        if url.contains("zoho.com") { return url }
        if(isHost){
           let url1 = url.replacingOccurrences(of: "api/v1/guest", with: "api/v1/host")
           return ZSURLSession.shared.guestSignURL + url1
        }
        return ZSURLSession.shared.guestSignURL + url
    }
    
   
    func clearFileCaches()  {
        if let directoryURL = fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first?.path{
            do{
                let directoryContents = try fileManager.contentsOfDirectory(atPath: directoryURL)

                for path in directoryContents {
                    if(path .contains(".pdf") || path .contains(".zip") || path .contains(".png") || path .contains(".tmp") || path .contains(".txt")){
                        let filePath = String(format: "%@/%@", directoryURL,path)
                        try fileManager.removeItem(atPath: filePath)
                    }
                }
            }catch{
                
            }
            
        }

        
    }
    //MARK: - requests
    
    internal func request(url: String,method : HTTPMethod , parameters : [String : Any]?,  skipStatusCheck: Bool = false, isBackgroundThread : Bool = false , success: @escaping RequestSuccessHandler, failure : @escaping DataManagerErrorHandler) {
        if !isConnectedToInternet() {
            ZSNotify.sharedNotify.showNoNetworkAlert()
            perform({
                failure(self.noNetworkError)
            }, afterDelay: 0.3)
            
            return
        }
                
        ZIAMManager.getIAMAccessTokenForExtn({ (accessToken) in
            
            
            self.header["Authorization"] =  "Zoho-oauthtoken " + accessToken!
            
           /* let request = self.createRequest(urlStr: url, parameters: parameters, method: method)
            
            let task = ZSURLSession.shared.requestURLSession.dataTask(with: request, completionHandler: { (data, response, error) in
                if error != nil {
                    let err = ZSError.getDataParsingError()
                    err.localizedDescription = error?.localizedDescription ?? ""
                    DispatchQueue.main.async {
                        failure(err)
                    }
                    return;
                }
                
                self.validateJsonData(data ?? Data(),skipStatusCheck: skipStatusCheck, success: success, failure: failure)
            })
            self.dataTasks.append(task)
            task.resume()*/
 
            if (isBackgroundThread){
                let task =  ZSURLSession.shared.requestBackgroundManager.request(self.getFullUrl(url),
                                                                       method: method,
                                                                       parameters: parameters,
                                                                       headers: self.header)
                    .responseJSON { response in
                        self.validateJsonResponse(response: response,skipStatusCheck : skipStatusCheck,success: success, failure: failure)
                }
                self.dataTasks.append(task)
            }
            else{
                let task =  ZSURLSession.shared.requestManager.request(self.getFullUrl(url),
                                                                       method: method,
                                                                       parameters: parameters,
                                                                       headers: self.header)
                    .responseJSON { response in
                        self.validateJsonResponse(response: response,skipStatusCheck : skipStatusCheck,success: success, failure: failure)
                }
                self.dataTasks.append(task)
            }
          
           
            
        }) { (iamError) in
            
            let err = ZSError()
            err.errorType = .IAMExpiryError
            err.localizedDescription = iamError?.localizedDescription ?? ""
            ZError(err.localizedDescription)

            
            if let _iamError = iamError as NSError?{
                if (_iamError.code == k_SSONoAccessToken || _iamError.code == k_SSOOneAuthAccountMismatch || _iamError.code == k_SSONoUsersFound){
                        ZSignKit.shared.zSignHandleOAuthExpiration()
                }
                else{
                    DispatchQueue.main.async {
                        failure(err)
                    }
                }
            }

            
        }
        
    }
    
    internal func upload(url: String,method : HTTPMethod ,multiPartData :@escaping (MultipartFormData) -> Void, parameters : [String : String]?, progress: @escaping DataManagerProgressHandler , success: @escaping RequestSuccessHandler, failure : @escaping DataManagerErrorHandler) {
        if !isConnectedToInternet() {
            perform({
                failure(self.noNetworkError)
            }, afterDelay: 0.3)
            return
        }
        
        
        ZIAMManager.getIAMAccessTokenForExtn({ (accessToken) in
            
           
            self.header["Authorization"] =  "Zoho-oauthtoken " + accessToken!
            
           /* let request = self.createMultipartRequest(urlStr: url, parameters: parameters, partdata: multiPartData , method: method)
            
            let task = ZSURLSession.shared.requestURLSession.uploadTask(with: request, from: nil, completionHandler: { (data, response, error) in
                if error != nil {
                    let err = ZSError.getDataParsingError()
                    err.localizedDescription = error?.localizedDescription ?? ""
                    failure(err)
                    return;
                }
                
                self.validateJsonData(data ?? Data(), success: success, failure: failure)
            })
            self.dataTasks.append(task)
            
            task.resume()*/
           
            weak var weakself = self
    
             ZSURLSession.shared.requestManager.upload(multipartFormData: multiPartData, to: self.getFullUrl(url), method: method, headers: self.header, encodingCompletion: {
                (result) in
                switch result {
                case .success(let upload, _, _):
                    DispatchQueue.main.async {
                        upload.uploadProgress(closure: { (prog) in
                           progress(prog)
                        })
                        upload.responseJSON { response in
                            weakself?.validateJsonResponse(response: response, success: success, failure: failure)
                        }
                    }
                    break
                    
                case .failure(let encodingError):
                    let error = self.getZSError(error: encodingError)
                    failure(error)
                    break
                }
             })
            
            
        }) { (iamError) in
            let err = ZSError()
            err.errorType = .IAMExpiryError
            err.localizedDescription = iamError?.localizedDescription ?? ""
            ZError(err.localizedDescription)
            
            
            if let _iamError = iamError as NSError?{
                if (_iamError.code == k_SSONoAccessToken || _iamError.code == k_SSOOneAuthAccountMismatch || _iamError.code == k_SSONoUsersFound){
                    ZSignKit.shared.zSignHandleOAuthExpiration()
                }
                else{
                    DispatchQueue.main.async {
                        failure(err)
                    }
                }
            }

        }
        
    }
    
    internal func download(url: String,method : HTTPMethod , parameters : [String : Any]?,  progress: @escaping DataManagerProgressHandler, success: @escaping DownloadSuccessHandler,  failure : @escaping DataManagerErrorHandler) {
        if !isConnectedToInternet() {
            perform({
                failure(self.noNetworkError)
            }, afterDelay: 0.3)
            return
        }
        
        
        weak var weakself = self

        ZIAMManager.getIAMAccessTokenForExtn({ (accessToken) in
            
            weakself?.downloadProgessHandler = progress
            weakself?.downloadSuccessHandler = success
            weakself?.downloadFailureHandler = failure
            
            weakself?.header["Authorization"] =  "Zoho-oauthtoken " + accessToken!
            
           
            /*let request = self.createRequest(urlStr: url, parameters: parameters, method: .get)

            DispatchQueue.main.async {
                self.downloadProgessHandler?(0.0)
            }
            let task = self.downloadURLSession.downloadTask(with: request)*/
            
          /*  let task = self.downloadURLSession.downloadTask(with: request, completionHandler: { (url, response, error) in
                self.downloadProgessHandler = nil

                do{
                    let data = try Data(contentsOf: url!)
                    success(data)
                }
                catch{
                    failure(self.dataParsingError)
                }

            })
            self.downlaodTasks.append(task)
            task.resume()*/
//            let destination = DownloadRequest.suggestedDownloadDestination(for: .cachesDirectory)
  
            
//            let destination: DownloadRequest.DownloadFileDestination =  { tempUrl, _ in
//                let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
////                let fileURL = documentsURL.appendingPathComponent("dest.pdf")
//
//                return (documentsURL, [.removePreviousFile, .createIntermediateDirectories])
//            }

            weakself?.clearFileCaches()

            let destination: DownloadRequest.DownloadFileDestination = { temporaryURL, response in
                let directoryURLs = weakself?.fileManager.urls(for: .cachesDirectory, in: .userDomainMask)
                
                if let directoryURL = directoryURLs?.first {
                    var name = response.suggestedFilename ?? "1.pdf"
                    if (name.count > 100){
                        name = String(name.dropFirst(name.count - 95))
                    }
                    return (directoryURL.appendingPathComponent(name), [])
                }
                
                return (temporaryURL, [.removePreviousFile, .createIntermediateDirectories])
            }
           
            
            let task = ZSURLSession.shared.requestManager.download(self.getFullUrl(url), method: method, parameters: parameters, encoding: JSONEncoding.default, headers: self.header, to:destination).downloadProgress(closure: { (progresss) in
                DispatchQueue.main.async {
                    progress(progresss)
                }
            }).responseData(completionHandler: { (response) in
                DispatchQueue.main.async {
                    weakself?.validateDataResponse(response: response, success: success, failure: failure)
                }
            })
            weakself?.downloadTasks.append(task)

        }) { (iamError) in
            let err = ZSError()
            err.errorType = .IAMExpiryError
            err.localizedDescription = iamError?.localizedDescription ?? ""
            ZError(err.localizedDescription)
            
            
            if let _iamError = iamError as NSError?{
                if (_iamError.code == k_SSONoAccessToken || _iamError.code == k_SSOOneAuthAccountMismatch || _iamError.code == k_SSONoUsersFound){
                    ZSignKit.shared.zSignHandleOAuthExpiration()
                }
                else{
                    DispatchQueue.main.async {
                        failure(err)
                    }
                }
            }

        }
    }
    
    //MARK: - validates and helpers
    
    public func validateJsonResponse(response : DataResponse<Any>,skipStatusCheck: Bool = false , success: @escaping RequestSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        guard response.result.isSuccess,
            let value = response.result.value else {
                let error = response.error
                let err = self.getZSError(error: error)
                failure(err)
                return
        }
        ZInfo(value);

        ZInfo(response.request?.url?.absoluteURL ?? "");

        guard let json = value as? [String:AnyObject]  else {
            failure(dataParsingError)
            return
        }
        
        if(skipStatusCheck){
            success(json)
            return;
        }
        
        var message  = ""
        var status   =  "failure"
        if let _status = json["status"] as? String{
            status = _status
        }
        if let _msg = json["message"] as? String{
            message = _msg
        }
        
        if status == "success"{
            success(json)
        }
        else {
            if message == "Invalid Oauth token"{
                ZSignKit.shared.zSignHandleOAuthExpiration()
                return
            }
//            ZInfo(response.request?.url?.absoluteURL ?? "");
            let error = ZSError.getDataParsingError()
            if (!message.isEmpty){
                error.localizedDescription =  message
            }
            error.errorCode = json["code"] as? Int ?? 0
            ZError(response.request?.url?.absoluteURL ?? "")
            ZError(error.localizedDescription)
            failure(error)
        }
    }
    
  
    public func validateDataResponse(response : DownloadResponse<Data>, success: @escaping DownloadSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        guard response.result.isSuccess,
            let destUrl = response.destinationURL,
                let data = try? Data(contentsOf: destUrl) else {
                let error = getZSError(error: response.error)
                failure(error)
                return
        }
        
        ZInfo(destUrl)

         
        
        if destUrl.pathExtension == "pdf" {
            success(data, "application/pdf")
        }
        else if destUrl.pathExtension == "zip" {
            success(data, "application/zip")
        }
        else if let _jsonResult = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves) {
            
            if let jsonResult = _jsonResult as? Dictionary<String, AnyObject>, let message = jsonResult["message"] as? String {
                let err = ZSError()
                err.errorType  = .generalError
                err.errorCode = jsonResult["code"] as? Int ?? 0
                err.localizedDescription = message
                failure(err)
            }
            else{
                failure(dataParsingError)
            }
        }
        
        try? fileManager.removeItem(at: destUrl)

    }
    
    public func getZSError(error : Error? , code : ZSErrorType = .generalError ) -> ZSError {
        let err = ZSError()
        err.errorType  = code
        err.localizedDescription = error?.localizedDescription ?? ""
        err.errorCode = error?._code ?? 0
        
        switch (error?._code) {
        case NSURLErrorTimedOut:
            err.errorType = .timeOut
            break;
        case NSURLErrorCancelled:
            err.errorType = .cancel
            break;
        case NSURLErrorNotConnectedToInternet:
            err.errorType = .noNetworkError
            break;
        default:
            break
            
        }
        
        ZError(err.localizedDescription)
        return err;
    }
    

    internal func createMultipartBody(parameters: [String: String],
                             boundary: String,
                    data: Data,
                    mimeType: String,
                    filename: String) -> Data {
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        
        for (key, value) in parameters {
            body.appendString(boundaryPrefix)
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        
        body.appendString(boundaryPrefix)
        body.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimeType)\r\n\r\n")
        body.append(data)
        body.appendString("\r\n")
        body.appendString("--".appending(boundary.appending("--")))
        
        return body as Data
    }
    
    internal func saveZipData(docId : String, data: Data){
       
        
        do{
            let folderPath = ZSFileManager.shared.getFolderPathDirectory(withName: "downloads", isCacheDirectory: false)
            let filePath  = String(format: "%@/%@",folderPath!, docId)
            
            let zipPath = String(format: "%@.zip", filePath)
            
            try data.write(to: URL(fileURLWithPath: zipPath))
       
        
            if !fileManager.fileExists(atPath: filePath) {
               try  fileManager.createDirectory(atPath: filePath, withIntermediateDirectories: false, attributes: nil)
            }
            let pagePath = String(format: "%@/pages", filePath)

            if (fileManager.fileExists(atPath: pagePath, isDirectory: nil)){
                try fileManager.removeItem(atPath: pagePath)
            }
            
            if (SSZipArchive.unzipFile(atPath: zipPath, toDestination: pagePath)){
                try fileManager.removeItem(atPath: zipPath)
            }
            

//            let fileList = try fileManager.contentsOfDirectory(atPath: pagePath)
//            for i in 0...fileList.count-1 {
//                let path = String(format: "%@/%i.jpg", pagePath,i)
//                let orgImg = UIImage(contentsOfFile: path)
//                let compressedImg = orgImg?.resizeImage(200, opaque: true)
//                let thumpPath = String(format: "%@/%i_thumbnail.jpg", pagePath,i)
//                try compressedImg!.pngData()?.write(to: URL(fileURLWithPath: thumpPath))
//            }
        }catch{
            
        }
        
    }
   

    
    @objc public func cancelAllTasks(){
//        ZInfo("cancelAllTasks")
        dataTasks.forEach { (task) in
            task.cancel()
        }
        downloadTasks.forEach { (task) in
            task.cancel()
        }
      
    }
    
    @objc public func cancelAllUploadTasks(){
        ZSURLSession.shared.requestManager.session.getTasksWithCompletionHandler { (datatasks, uploadtasks, downloadTasks) in
           
            uploadtasks.forEach({ (task) in
                if task.currentRequest?.url?.absoluteString.contains("/api/v1/profile") ?? false{
                    //skip
                    
                }else{
                    task.cancel()
                }
                
            })
            
        }
    }
    
    //MARK: - cancel
    deinit {
       cancelAllTasks()
    }
}


extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}

extension Dictionary{
    public func jsonString() -> String  {
        var jsonString: String? = nil
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            jsonString = String(data: jsonData, encoding: .utf8)

        } catch {
        }
        
        guard let outString = jsonString?.replacingOccurrences(of: "\\/", with: "/")
            else { return "" }

        return outString

    }
}
