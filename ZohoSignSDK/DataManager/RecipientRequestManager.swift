//
//  RecipientRequestManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 29/11/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class RecipientRequestManager: URLRequestManager {

    //MARK: -

    public func getRecipientsList(startsText : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        let dict =  ["page_context" :["starts_with":["name":startsText.lowercased(),"email":startsText.lowercased()]]].jsonString()
        
        request(url: recipientsApi, method: .get, parameters: ["data" : dict], success: { (responseObj) in
            success(responseObj["contacts"])
        }, failure: failure)
    }
    
    public func addNewRecipient(name : String, emailId : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        let dict =  ["recipients" :["recipient_name":["name":name,"recipient_email":emailId]]].jsonString()
        
        request(url: recipientsApi, method: .post, parameters: ["data" : dict], success: { (responseObj) in
            ZSAnalytics.shared.track(forEvent: .recipientRequest(.newRecipientAdded))

            success(responseObj["contacts"])
        }, failure: failure)
    }
    
//    public func updateRecipient(id :String, name : String, emailId : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
//
//        let dict =  ["recipients" :["recipient_name":["name":name,"recipient_email":emailId]]].jsonString()
//
//        request(url: recipientsApi, method: .post, parameters: ["data" : dict], success: { (responseObj) in
//            success(responseObj["contacts"])
//        }, failure: failure)
//    }
    
    public func getHostsList(startsText : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        let dict =  ["page_context" :["starts_with":["name":startsText.lowercased(),"email":startsText.lowercased()]]].jsonString()

        request(url: getHostsApi, method: .get, parameters: ["data" : dict], success: { (responseObj) in
            success(responseObj["hosts"])
        }, failure: failure)
    }
 
    public func validateHostsEmail(email : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        request(url: validateHostEmailApi, method: .get, parameters: ["email_id"   : "[\(email)]"], success: { (responseObj) in
            success(responseObj["hosts"])
        }, failure: failure)
    }
    

}
