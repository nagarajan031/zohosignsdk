//
//  ReceivedDocumentSigningRequestManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 21/11/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class ReceivedDocumentSigningRequestManager: GuestURLRequestManager {

    
    //MARK: - Get detail
    public func getVerificationCode(signId : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)   {
        isHostApi = false
        
        var param = ["sign_id" : signId]
        if let zuid = UserManager.shared.currentUser?.ZUID{
            param["ZUID"] = zuid
        }
        self.request(url: guestGetVerificationCodeApi, method: .get, parameters: param, success: { (response) in
            success(response["actions"])
        }, failure: failure)
       
    }
    
    public func sendVerificationCodeToMail(signId : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)   {
        isHostApi = false

        request(url: guestSendVerificationCodeApi, method: .get, parameters: ["sign_id" : signId], success: { (response) in
            success(response["actions"])
        }, failure: failure)
    }
    
    public func getStatelessAuthCode(actionId : String,signId : String,verifyCode : String?, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)   {
        isHostApi = false
        var dict : [String:String]  = [:]
        if let code = verifyCode {
            dict = ["verification_code":code]
        }
        
        request(url: String(format: guestGetStatelessAuthCodeApi,actionId,signId), method: .post, parameters: dict, success: { (response) in
            success(response["stateless_signature"])
        }, failure: failure)
    }
    
    public func getReceivedRequestDetails(actionType : String?,requestId : String, actionId : String,statelessAuth : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)   {
        isHostApi = (actionType == ZS_ActionType_InPerson)
        
        setHeaderValue(statelessAuth: statelessAuth, isInpersonSigning: isHostApi, completion: {
            self.request(url: String(format: guestGetRequestDetailApi,requestId,actionId), method: .get, parameters: nil, success: { (response) in
                success(response["requests"])
            }, failure: failure)
        }, failure: failure)
        
    }
    
    //MARK: - Sign/ reject request
    public func signRequest(req: MyRequest, inpersonSigner : ZSRecipient?,  params : [String : Any], success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
    
        isHostApi = (req.actionType == ZS_ActionType_InPerson)

        let paramstring    =   ["field_data" : params].jsonString();
        
        var signatureImageData : Data?
        var  initialImageData  : Data?
        
        if req.actionType == ZS_ActionType_InPerson {
            if  let signImage  =  inpersonSigner?.inPersonSignature {
                signatureImageData = signImage.compressSignatureData()
            }
            if  let initialImage  =  inpersonSigner?.inPersonInitial {
                initialImageData = initialImage.compressSignatureData()
            }
        }else{
            if  let signImage  =  UserManager.shared.currentUser?.getSignatureImage() {
                signatureImageData = signImage.compressSignatureData()
            }
            if  let initialImage  = UserManager.shared.currentUser?.getInitialImage() {
                initialImageData = initialImage.compressSignatureData()
            }
        }
       
  
        setHeaderValue(statelessAuth: req.statelessAuthCode, isInpersonSigning: isHostApi, completion: {

            self.upload(url: String(format: signDocumentApi, req.requestId!,req.actionId!), method: .post, multiPartData: { (multipartFormData) in
                
                multipartFormData.append(paramstring.data(using: .utf8) ?? Data(), withName: "data")

                if let signData =  signatureImageData{
                    multipartFormData.append(signData, withName: "signature", fileName: "signature", mimeType: "image/png")
                }
                if  let initialData =  initialImageData{
                    multipartFormData.append(initialData, withName: "initial", fileName: "initial", mimeType: "image/png")
                }
                if let inpersonEmail = inpersonSigner?.inPersonEmail{
                    multipartFormData.append(inpersonEmail.data(using: .utf8) ?? Data(), withName: "in_person_email")
                }

            }, parameters: nil, progress: { (progress) in
                
            }, success: { (responseObj) in
                if self.isHostApi{
                    ZSAnalytics.shared.track(forEvent: .guestSign(.successInperson))
                }else{
                    ZSAnalytics.shared.track(forEvent: .guestSign(.successRemote))
                }

                success(responseObj["my_requests"])
            }, failure: failure)
        }, failure: failure)
        
    }
    
    public func assignRequest(reqId : String,actionId : String, statelessAuth : String, recipientName : String, recipientEmail : String, reason : String,  success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        setHeaderValue(statelessAuth: statelessAuth)

        let outString = ["forward_details": ["reason":reason,"recipient_name":recipientName,"recipient_email":recipientEmail]].jsonString()
        request(url: String(format: forwardDocumentApi, reqId,actionId), method: .post, parameters: ["data" : outString], success: { (response) in
            ZSAnalytics.shared.track(forEvent: .guestSign(.Assigned))
            success(response)
        }, failure: failure)
    }
    
    public func declineRequest(reqId : String,actionId : String, statelessAuth : String,  reason : String,  success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        setHeaderValue(statelessAuth: statelessAuth)

        request(url: String(format: declineDocumentApi, reqId,actionId), method: .post, parameters:["reason":reason], success: { (response) in
            ZSAnalytics.shared.track(forEvent: .guestSign(.declined))

            success(response)
        }, failure: failure)
    }

    //MARK: - Actions
    public func mailRequest(reqId : String,actionId : String, statelessAuth : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        setHeaderValue(statelessAuth: statelessAuth)
        
        request(url: String(format: emailGuestDocApi, reqId,actionId), method: .post, parameters:nil, success: { (response) in
            ZSAnalytics.shared.track(forEvent: .guestSign(.mailed))

            success(response)
        }, failure: failure)
    }
    
    
    //MARK: - Download
    public func downloadRequestFile(request : MyRequest,fileId : String, returnAsPDF: Bool = false, progress: @escaping DataManagerProgressHandler , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        isHostApi = (request.actionType == ZS_ActionType_InPerson)

        
        setHeaderValue(statelessAuth: request.statelessAuthCode!, isInpersonSigning: isHostApi, completion: {
            self.download(url: String(format: downloadMyRequestFileAsPdfApi,request.requestId!,request.actionId!,fileId), method: .get, parameters: nil, progress:  { (_progress) in
                progress(_progress)
            }, success: { (responseObj,mimeType)  in
                if returnAsPDF {
                    success(responseObj)
                }else if let data = responseObj as? Data{
                    let fileManager = ZSFileManager()
                    if (fileManager.writeFile(withFileId: fileId, fileData: data, saveCache: false)){
                        let filePath = fileManager.getFile(withFileId: fileId, isSharedLocation: false)
                        success(filePath)
                    }
                    else{
                        failure(self.dataParsingError)
                    }
                }
            }, failure: failure)
        }, failure: failure)
    }
    
    public func downloadAllFiles(request : MyRequest, progress: @escaping DataManagerProgressHandler , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        isHostApi = (request.actionType == ZS_ActionType_InPerson)


        setHeaderValue(statelessAuth: request.statelessAuthCode!, isInpersonSigning: isHostApi, completion: {

            self.download(url: String(format: downloadMyRequestDocAsPdfApi,request.requestId!,request.actionId!), method: .get, parameters: nil, progress:  { (_progress) in
                progress(_progress)
            }, success: { (responseObj,mimeType)  in
                if let data = responseObj as? Data{

                    if (ZSFileManager.shared.saveFileAfterEncryption(forRequestId: request.myRequestId, isSentRequests: false, fileData: data, mimeType: mimeType, saveCache: true)){
                        let filePaths = ZSFileManager.shared.getSavedFilesPathAfterDecryption(forRequestId: request.myRequestId, isSentRequests: false, saveCache: true)
                        success(filePaths)
                    }
                    else{
                        failure(self.dataParsingError)
                    }
                }
            }, failure: failure)
        }, failure: failure)
    }

    //MARK: - Terms
    public func getLegalTerms(signId : String, statelessAuthCode : String,success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler )  {
        setHeaderValue(statelessAuth: statelessAuthCode)
        request(url: legalTermsForGuestApi, method: .get, parameters: ["sign_id" : signId], success: { (response) in
            if let legalDict = response["accounts"] as? [String: Any], let legalTerms = legalDict["legal_agreement"] as? String{
                success(legalTerms)
            }else{
                failure(self.dataParsingError)
            }
        }, failure: failure)
        
    }
    
    public func getHostLegalTerms(signId : String, isHost : Bool ,success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler )  {
        let dict = isHost ?  ["sign_id" : signId] : ["sign_id" : signId, "ishost" : "true"]
        request(url: legalTermsForGuestApi, method: .get, parameters: dict, success: { (response) in
            if let legalDict = response["accounts"] as? [String: Any], let legalTerms = legalDict["legal_agreement"] as? String{
                success(legalTerms)
            }else{
                failure(self.dataParsingError)
            }
        }, failure: failure)
    }
   
    
    //MARK: - Attachments
    public func uploadAttachment(request : MyRequest, fileItem : ZFileItem,fieldId : String, progress: @escaping DataManagerProgressHandler , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        isHostApi = (request.actionType == ZS_ActionType_InPerson)
        
        guard let fileUrl = fileItem.fileUrl, let data = try? Data(contentsOf: fileUrl) else {
            failure(dataParsingError)
            return
        }

        setHeaderValue(statelessAuth: request.statelessAuthCode, isInpersonSigning: isHostApi, completion: {
                
            self.upload(url: String(format: uploadReceivedRequestsAttachmentApi, request.requestId!,request.actionId!,fieldId), method: .post, multiPartData: { (multiPartData) in
                multiPartData.append(data, withName: "file", fileName: fileItem.fileNameWithExtn() , mimeType: fileItem.mimeType)
            }, parameters: nil, progress: { (_progress) in
                progress(_progress)
            }, success: { (responseObj) in
                success(responseObj["attachments"])
            }, failure: failure)
        }, failure: failure)
    }
    
    public func downloadAttachment(request : MyRequest, id : String,progress: @escaping DataManagerProgressHandler , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        
        isHostApi = (request.actionType == ZS_ActionType_InPerson)
        
        
        setHeaderValue(statelessAuth: request.statelessAuthCode, isInpersonSigning: isHostApi, completion: {
            self.download(url: String(format: downloadReceivedRequestsAttachmentApi, request.requestId!,request.actionId!,id), method: .get, parameters: nil, progress:  { (_progress) in
                progress(_progress)
            }, success: { (responseObj,mimeType)  in
                if let data = responseObj as? Data{
                    let fm = ZSFileManager()
                    let attachment = fm.saveAttachment(attachmentId: id, data: data)
                    if attachment.fileAlreadyAvailable{
                        success(attachment.filePath)
                    }else{
                        failure(self.dataParsingError)
                    }
                }else{
                    failure(self.dataParsingError)
                }
            }, failure: failure)
        }, failure: failure)
    }
    
    
    public func deleteAttachment(request : MyRequest, attachmentId : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        isHostApi = (request.actionType == ZS_ActionType_InPerson)

        setHeaderValue(statelessAuth: request.statelessAuthCode, isInpersonSigning: isHostApi, completion: {
                
            self.request(url: String(format: deleteReceivedRequestsAttachmentApi, request.requestId!,request.actionId!,attachmentId), method: .delete, parameters: nil, success: { (responseObj) in
                success(responseObj)
            }, failure: failure)
        }, failure: failure)
    }
    
}
