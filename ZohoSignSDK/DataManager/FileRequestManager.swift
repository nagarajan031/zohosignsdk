//
//  FileRequestManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 28/08/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class FileRequestManager: URLRequestManager {

    let zfileManager = ZSFileManager()

    //MARK: - Upload file

    @objc public func uploadFile(fileItem : ZFileItem, progress: @escaping DataManagerProgressHandler , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        guard let fileUrl = fileItem.fileUrl, let data = try? Data(contentsOf: fileUrl) else {
            failure(dataParsingError)
            return
        }
        
        upload(url: addNewDocumentApi, method: .post, multiPartData: { (multiPartData) in
            multiPartData.append(data, withName: "file", fileName: fileItem.fileNameWithExtn() , mimeType: fileItem.mimeType)
            
        }, parameters: nil, progress: { (progresss) in
            progress(progresss)
        }, success: { (responseObj) in
            success(responseObj["documents"])
        }, failure: failure)
        
        
    }
    
    public func uploadFileFromCloud(fileItem : ZFileItem, progress: @escaping DataManagerProgressHandler , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        ZSAnalytics.shared.track(forEvent: .fileRequest(.uploadFromZohoDocs))

        var urlStr  = String(format: importFileFromCloudServicesApi, fileItem.cloudServiceDocId,fileItem.cloudServiceName)
        if let password = fileItem.filePassword{
            urlStr = urlStr.appendingFormat("&user_password=%@", password)
        }
        request(url: urlStr, method: .post, parameters: nil, success: { (responseObj) in
            success(responseObj["documents"])
        }, failure: failure)
        
    }
    
    public func loadFilesFromZoho(startIndex : Int, limit : Int,  success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        let zohoDocsURL = ZIAMManager.transformedURLString("https://apidocs.zoho.com/files")
        
        let url = String(format: "%@/v1/files", zohoDocsURL)
        
        request(url: url, method: .get, parameters: ["start": startIndex, "limit": limit],skipStatusCheck : true, success: { (response) in
            success(response)
        }, failure: failure)
        
    }
    
    public func rotateFilePage(reqId: String, fileId : String, pageNo : Int,  success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
//        ZSAnalytics.shared.track(forEvent: .fileRequest(.uploadFromZohoDocs))

         var urlStr  = String(format: rotateDocumentPageApi,reqId, fileId, pageNo)

          
        request(url: urlStr, method: .put, parameters: ["rotation":-90],skipStatusCheck : true, success: { (response) in
              success(response)
          }, failure: failure)
          
      }
    
    /*public func getZohoDocsDocumentDetails(docId: String, fileType: String , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler) {
        guard let zohoDocsURL = ZSIAMManager.transformedURLString("https://apidocs.zoho.com/files") else {
            return
        }
        
        let url = String(format: "%@/v1/revision/details", zohoDocsURL)
        request(url: url, method: .get, parameters: ["docid": docId, "type": fileType], success: success, failure: failure)
    }*/
    
    //MARK: - Download file
    public func downloadPreviewImage(fileID  : String, thumbnail : Bool, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler) {
        
        request(url: String(format: downloadSignRequestDetailPreviewApi, fileID), method: .get, parameters: nil,skipStatusCheck : true ,isBackgroundThread : true,success: { (responseObj) in
//            success(responseObj)
            if let pagesArr = responseObj["pages"] as? [Any], let page = pagesArr.first as? Dictionary<AnyHashable, Any>{
                do {
                    let image = SwiftUtils.decodeBase64ToImage(encodeStr: page[keyPath: "image_string"])
                    let data = image!.pngData()
                    let folderPath  = self.zfileManager.getFolderPathDirectory(withName: "downloads/thumbnail", isCacheDirectory: true)
                    let filePath =  String(format: "%@/%@.jpg", folderPath!,fileID)
                    try data?.write(to: URL(fileURLWithPath: filePath))
                    mainQueue {
                        success(filePath)
                    }
                }catch{
                    failure(self.dataParsingError)}
            }
            else{failure(self.dataParsingError) }
            
        }, failure: failure)
    }

    @objc public func downloadFile(fileID : String, returnAsPDF: Bool = false , progress: @escaping DataManagerProgressHandler , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        
        download(url: String(format: fileDownloadAsPdfApi, fileID), method: .get, parameters: nil,
                 progress: { (progresss) in
                    progress(progresss)
        },
                 success: { (responseObj, mimeType) in
                    if let data = responseObj as? Data{
                        if(returnAsPDF){
                            success(data)
                        }
                        else{

                            if (self.zfileManager.writeFile(withFileId: fileID, fileData: data, saveCache: false)){
                                let filePath = self.zfileManager.getFile(withFileId:fileID, isSharedLocation: false)
                                success(filePath)
                            }
                            else{
                                failure(self.dataParsingError)
                            }
                        }
                    }
        }, failure: failure)
    }
    
    @objc public func downloadTemplateFile(templateID: String, fileID : String, returnAsPDF: Bool = false , progress: @escaping DataManagerProgressHandler , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
           
        download(url: String(format: templateFileDownloadAsPdfApi,templateID, fileID), method: .get, parameters: nil,
                 progress: { (progresss) in
                    progress(progresss)
        },
                 success: { (responseObj, mimeType) in
                    if let data = responseObj as? Data{
                        if(returnAsPDF){
                            success(data)
                        }
                        else{

                            if (self.zfileManager.writeFile(withFileId: fileID, fileData: data, saveCache: false)){
                                let filePath = self.zfileManager.getFile(withFileId:fileID, isSharedLocation: false)
                                success(filePath)
                            }
                            else{
                                failure(self.dataParsingError)
                            }
                        }
                    }
        }, failure: failure)
    }
    
    @objc public func downloadFilesForSentRequest(requestID : String,saveChace : Bool, returnAsPDF: Bool = false , progress: @escaping DataManagerProgressHandler , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        
        download(url: String(format: downloadSignRequestDocAsPDFApi, requestID), method: .get, parameters: nil,
                 progress: { (progresss) in
                    progress(progresss)
        },
                 success: { (responseObj, mimeType) in
                    if let data = responseObj as? Data{
                        if(returnAsPDF){
                            success(data)
                        }
                        else{
                            
                            if (ZSFileManager.shared.saveFileAfterEncryption(forRequestId: requestID, isSentRequests: true, fileData: data, mimeType: mimeType, saveCache: saveChace)){
                                
                                let filePaths = ZSFileManager.shared.getSavedFilesPathAfterDecryption(forRequestId: requestID, isSentRequests: true, saveCache: saveChace)
                                success(filePaths)
                            }
                            else{
                                failure(self.dataParsingError)
                            }
                        }
                    }
        }, failure: failure)
    }
    
    @objc public func downloadHostedFilesForRequest(requestID : String, saveCache : Bool, progress: @escaping DataManagerProgressHandler , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        ZSAnalytics.shared.track(forEvent: .fileRequest(.viewHostedFiles))

        download(url: String(format: hostDownloadDocApi, requestID), method: .get, parameters: nil,
                 progress: { (progresss) in
                    progress(progresss)
        },
                 success: { (responseObj, mimeType) in
                    if let data = responseObj as? Data{

                        if(ZSFileManager.shared.saveFileAfterEncryption(forRequestId: requestID, isSentRequests: false, fileData: data, mimeType: mimeType, saveCache: saveCache)){
                            let filePaths = ZSFileManager.shared.getSavedFilesPathAfterDecryption(forRequestId: requestID, isSentRequests: false, saveCache: saveCache)
                                success(filePaths)
                            }
                            else{
                                failure(self.dataParsingError)
                            }
                    }
        }, failure: failure)
    }
    
    @objc public func downloadCompletionCertificate(requestId : String, progress: @escaping DataManagerProgressHandler , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        ZSAnalytics.shared.track(forEvent: .fileRequest(.completionCertDownload))
        download(url: String(format: completionCertificateApi, requestId), method: .get, parameters: nil, progress:progress,
                 success: {[weak self] (responseObj, mimeType) in
                    if let data = responseObj as? Data{
                       
                        
                        if (self?.zfileManager.saveCompletionCertificate(forRequestId: requestId, fileData: data, mimeType: mimeType) ?? false){
                            let filePath = self?.zfileManager.getCompletionCertificateFilePath(forRequestId: requestId)
                            success(filePath)
                        }
                        else{
                            failure(self?.dataParsingError)
                        }
                    }else{failure(self?.dataParsingError)}
        }, failure: failure)
    }
  
    //MARK: - Delete file
    public func deleteFile(fileID : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        request(url: String(format: deleteFileApi, fileID), method: .delete, parameters: nil, success: { (responseObj) in
            ZSignKit.shared.trackEvent("File deleted")
            ZSAnalytics.shared.track(forEvent: .fileRequest(.fileDeleted))
            success(responseObj)
        }, failure: failure)
    }
    
}
