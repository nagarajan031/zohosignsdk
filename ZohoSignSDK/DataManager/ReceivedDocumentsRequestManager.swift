//
//  ReceivedDocumentsRequestManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 14/09/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class ReceivedDocumentsRequestManager: URLRequestManager {

    public func mailDocument(reqId : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        request(url: String(format: emailGuestDocApi, reqId), method: .post, parameters: nil, success: { (response) in
            ZSAnalytics.shared.track(forEvent: .myRequest(.mailed))
            success(response)
        }, failure: failure)
    }
    
    public func deleteRequest(reqId : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        request(url: String(format: deleteMyRequestApi, reqId), method: .put, parameters: nil, success: { (response) in
            ZSAnalytics.shared.track(forEvent: .myRequest(.deleted))

            ZSCoreDataManager.shared.deleteMyRequests(withRequestIds: reqId)
            success(response)
        }, failure: failure)
    }
    
    public func restoreRequest(reqId : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        request(url: String(format: restoreMyRequestApi, reqId), method: .put, parameters: nil, success: { (response) in
            ZSAnalytics.shared.track(forEvent: .myRequest(.restored))
            ZSCoreDataManager.shared.deleteMyRequests(withRequestIds: reqId)
            success(response)
        }, failure: failure)
    }
    
    public func getLegalTerms(success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        request(url: legalTermsApi, method: .get, parameters: nil, success: { (response) in
            if let legalDict = response["accounts"] as? [String: Any], let legalTerms = legalDict["legal_agreement"] as? String{
                success(legalTerms)
            }else{
                failure(self.dataParsingError)
            }
        }, failure: failure)
    }

    
}
