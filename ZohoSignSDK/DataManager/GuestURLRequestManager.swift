//
//  GuestURLRequestManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 21/11/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit
import Alamofire
import SSOKit

public class GuestURLRequestManager: URLRequestManager {
    
    var isHostApi = false
    
    public func setHeaderValue(statelessAuth : String){
        self.guestRequestHeader["Authorization"] = nil
        self.guestRequestHeader["stateless_auth"] = statelessAuth
        
        isHostApi = false
    }
    
    public func setHeaderValue(statelessAuth : String?, isInpersonSigning : Bool = false ,  completion: @escaping () -> (), failure : @escaping DataManagerErrorHandler){
        self.guestRequestHeader["stateless_auth"] = statelessAuth

        if isInpersonSigning{
            
            ZIAMManager.getIAMAccessToken({ (accessToken) in
                self.guestRequestHeader["Authorization"] =  "Zoho-oauthtoken " + accessToken!
                completion()
            }) { (iamError) in
                let err = ZSError()
                err.errorType = .IAMExpiryError
                err.localizedDescription = iamError?.localizedDescription ?? ""
                ZError(err.localizedDescription)
                
                
                if let _iamError = iamError as NSError?{
                    if (_iamError.code == k_SSONoAccessToken || _iamError.code == k_SSOOneAuthAccountMismatch || _iamError.code == k_SSONoUsersFound){
                        ZSignKit.shared.zSignHandleOAuthExpiration()
                    }
                    else{
                        DispatchQueue.main.async {
                            failure(err)
                        }
                    }
                }

            }
        }
        else{
            self.guestRequestHeader["Authorization"] = nil
            completion()
        }
    }
    
    //MARK: - requests
    
    internal override func request(url: String,method : HTTPMethod , parameters : [String : Any]?,  skipStatusCheck: Bool = false, isBackgroundThread : Bool = false , success: @escaping RequestSuccessHandler, failure : @escaping DataManagerErrorHandler) {
        if !isConnectedToInternet() {
            perform({
                failure(self.noNetworkError)
            }, afterDelay: 0.3)
            return
        }
        
        
        let task =  ZSURLSession.shared.guestRequestManager.request(self.getBaseGuestUrl(url, isHost: isHostApi),
                                                               method: method,
                                                               parameters: parameters,
                                                               headers: self.guestRequestHeader)
            .responseJSON { response in
                self.validateJsonResponse(response: response,skipStatusCheck : skipStatusCheck,success: success, failure: failure)
        }
        self.dataTasks.append(task)
            
    
    }
    
    
    
    internal override func download(url: String,method : HTTPMethod , parameters : [String : Any]?,  progress: @escaping DataManagerProgressHandler, success: @escaping DownloadSuccessHandler,  failure : @escaping DataManagerErrorHandler) {
        if !isConnectedToInternet() {
            perform({
                failure(self.noNetworkError)
            }, afterDelay: 0.3)
            return
        }
        
        
        weak var weakself = self
        
        
        let destination: DownloadRequest.DownloadFileDestination = { temporaryURL, response in
            let directoryURLs = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
            
            if !directoryURLs.isEmpty {
                return (directoryURLs[0].appendingPathComponent(response.suggestedFilename!), [.removePreviousFile])
            }
            
            return (temporaryURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        
        let task = ZSURLSession.shared.guestRequestManager.download(self.getBaseGuestUrl(url, isHost: isHostApi), method: method, parameters: parameters, encoding: JSONEncoding.default, headers: self.guestRequestHeader, to:destination).downloadProgress(closure: { (progresss) in
            DispatchQueue.main.async {
                ZInfo(progresss.fractionCompleted)
                progress(progresss)
            }
        }).responseData(completionHandler: { (response) in
            DispatchQueue.main.async {
                weakself?.validateDataResponse(response: response, success: success, failure: failure)
            }
        })
        weakself?.downloadTasks.append(task)
        
    }
    
    internal override func upload(url: String,method : HTTPMethod ,multiPartData :@escaping (MultipartFormData) -> Void, parameters : [String : String]?, progress: @escaping DataManagerProgressHandler , success: @escaping RequestSuccessHandler, failure : @escaping DataManagerErrorHandler) {
        if !isConnectedToInternet() {
            perform({
                failure(self.noNetworkError)
            }, afterDelay: 0.3)
            return
        }
        
        weak var weakself = self
        
        
        ZSURLSession.shared.guestRequestManager.upload(multipartFormData: multiPartData, to: self.getBaseGuestUrl(url, isHost: isHostApi), method: method, headers: self.guestRequestHeader, encodingCompletion: {
            (result) in
            switch result {
            case .success(let upload, _, _):
                DispatchQueue.main.async {
                    upload.uploadProgress(closure: { (prog) in
                        progress(prog)
                    })
                    upload.responseJSON { response in
                        weakself?.validateJsonResponse(response: response, success: success, failure: failure)
                    }
                }
                break
                
            case .failure(let encodingError):
                let error = self.getZSError(error: encodingError)
                failure(error)
                break
            }
        })
    }
    
    
    
}
