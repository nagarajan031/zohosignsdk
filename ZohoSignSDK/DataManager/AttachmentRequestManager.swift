//
//  AttachmentRequestManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 25/09/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

public class AttachmentRequestManager: GuestURLRequestManager {

    //MARK: - GET files
    public func downloadAttachment(request : MyRequest, id : String,progress: @escaping DataManagerProgressHandler , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        
        isHostApi = (request.actionType == ZS_ActionType_InPerson)
        
        
        setHeaderValue(statelessAuth: request.statelessAuthCode, isInpersonSigning: isHostApi, completion: {
            self.download(url: String(format: downloadReceivedRequestsAttachmentApi, request.requestId!,request.actionId!,id), method: .get, parameters: nil, progress:  { (_progress) in
                progress(_progress)
            }, success: { (responseObj,mimeType)  in
                if let data = responseObj as? Data{
                    let fm = ZSFileManager()
                    let attachment = fm.saveAttachment(attachmentId: id, data: data)
                    if attachment.fileAlreadyAvailable{
                        success(attachment.filePath)
                    }else{
                        failure(self.dataParsingError)
                    }
                }else{
                    failure(self.dataParsingError)
                }
            }, failure: failure)
        }, failure: failure)
    }
    
//    //MARK: - Upload Attachment
   /* public func uploadAttachment(request : MyRequest, fileItem : ZFileItem,signField : SignField, progress: @escaping DataManagerProgressHandler , success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler)  {
        
        isHostApi = (request.actionType == ZS_ActionType_InPerson)
        
        guard let data = try? Data(contentsOf: fileItem.fileUrl) else {
            failure(dataParsingError)
            return
        }

        setHeaderValue(statelessAuth: request.statelessAuthCode, isInpersonSigning: isHostApi, completion: {
                
            self.upload(url: String(format: uploadReceivedRequestsAttachmentApi, request.requestId!,request.actionId!,signField.fieldId!), method: .post, multiPartData: { (multiPartData) in
                multiPartData.append(data, withName: "file", fileName: fileItem.fileNameWithExtn() , mimeType: fileItem.mimeType)
            }, parameters: nil, progress: { (_progress) in
                progress(_progress)
            }, success: { (responseObj) in
                success(responseObj["attachments"])
            }, failure: failure)
        }, failure: failure)
    }


    //MARK: - Delete file
    public func deleteFile(fileID : String, success: @escaping DataManagerSuccessHandler, failure : @escaping DataManagerErrorHandler){
        request(url: String(format: deleteFileApi, fileID), method: .delete, parameters: nil, success: { (responseObj) in
            ZSignKit.shared.trackEvent("File deleted")

            success(responseObj)
        }, failure: failure)
    }
    */
}
