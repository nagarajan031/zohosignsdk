//
//  ZSFileManager+CompletionCertificate.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 03/02/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import Foundation

extension ZSFileManager{
    
     public func saveCompletionCertificate(forRequestId requestId: String?, fileData: Data?, mimeType: String?) -> Bool {
        guard let requestId = requestId else {
            return false
        }
        
        
        let folderPath = getSharedDirectory(withRequestId: requestId, isSentRequests: true)

        var fileName = "completion certificate.pdf"
        if (mimeType == "application/zip") {
            fileName = "\(requestId ?? "").zip"
        }

        let fileURL = URL(fileURLWithPath: folderPath).appendingPathComponent(fileName)
        
        return ((try? fileData?.write(to: fileURL)) != nil)
    }

     public func getCompletionCertificateFilePath(forRequestId requestId: String?) -> String? {
        guard let requestId = requestId else {
            return nil
        }
        
        let folderPath = self.getSharedDirectory(withRequestId: requestId, isSentRequests: true)

        let filePath = URL(fileURLWithPath: folderPath).appendingPathComponent("completion certificate.pdf").path

        if fileManager.fileExists(atPath: filePath) {
            return filePath
        }
        return nil
    }
    
}
