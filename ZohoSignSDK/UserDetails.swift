//
//  UserDetails.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 31/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public struct UserDetails : Codable {

    public var userID : String?
    public var accountID : String?
    public var ZUID : String?
    public var customId : String?//for iAP
    public var userName : String?
    public var fullName : String?
    public var firstName : String?
    public var lastName : String?
    public var email : String?
    public var company : String?
    public var jobTitle : String?
    public var timeZone : String?
    public var dateFormat : String?

    public var passcode : String?
    public var installationID : String? // push notification
    public var deviceToken : String? // push notification

    var temporaryEditedRequestFields : Data? // Saved Temporary fields

    public var isAdmin : Bool = false
    public var isPasscodeEnabled : Bool = false
    public var isTouchIDEnabled : Bool = false
    public var isProtectDocumentSigning : Bool = false

    public var isRetainSigningOrderByDefault : Bool = false

    public var isInPersonSigningAvailable : Bool = false
    public var isTemplatesAvailable : Bool = false
    public var isBulkSendAvailable : Bool = false
    public var isReportsAvailable : Bool = false
    public var isApproverFeatureAvailable : Bool = false
    public var isAttachmentFeatureAvailable : Bool = false

    public var isUserProfileEdited : Bool = false// to be updated in server

    public var isConfirmedToContinueAsGuest : Bool = false
    public var isGuestUserAlreadyHaveZohoAccount : Bool = false
    
    public var addMeAcionType : String?

    //Permissions& lincense
    public var licenseType : String? //license Type
    public var urlBaseDomain : String? //zoho.com or zoho.eu
    public var baseUrl : String? //zoho.com or zoho.eu

    
    public var selectedIndexpath : IndexPath?

    public var accountSetting : AccountSettings?


//    //Docs Count
    public var numberOfTotalDocumentsQouta : Int?
    public var numberOfUsedDocuments : Int?

    // images
    var signImageData : Data?
    var initialImageData : Data?
//    var profileImage : UIImage?
//    var signImage : Image? // product dimension 560px X 140px
//    var initialImage : Image? // product dimension 560px X 140px
//
    // enum
    public var userType : ZSUserType?

    
    public func getSignatureImage() -> UIImage? {
        guard let imgData = signImageData else {
            return nil
        }
        return UIImage(data:imgData)
    }
    
    public func getInitialImage() -> UIImage? {
        guard let imgData = initialImageData else {
            return nil
        }
        return UIImage(data:imgData)
    }
  
    public mutating func saveSignatureImage(_ img : UIImage)  {
        signImageData = img.pngData()
    }
    
    public mutating func saveinitialImage(_ img : UIImage)  {
        initialImageData = img.pngData()
    }
    
//    func encodeWithCoder(_ aCoder: NSCoder) {
//        // Serialize your object here
//
//    }

//    init(coder aDecoder: NSCoder) {
//        // Deserialize your object here
//
//    }
    
}

public struct AccountSettings: Codable {
    public var isAuthenticationModeEmailEnabled : Bool = true
    public var isAuthenticationModeOfflineEnabled : Bool = true
    public var isAuthenticationModeSmsEnabled : Bool = true
    public var isAuthenticationModeMandatory : Bool = false
    public var expirationDays : String = "30"
    public var isAutomaticReminderOn: Bool = true
    public var reminderTimePeriod: Int = 2
}



public enum ZSUserType  : String, Codable
{
    case loggedInSignUser
    case guestUser
}
