//
//  PrintingManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 04/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation
import PDFKit

public class PrintingManager  {
    
    
    public static func printDocument(filePathArray:[String],name : String , from parentVC: UIViewController) {
        
        let printInfo = UIPrintInfo(dictionary:nil)
        printInfo.outputType = UIPrintInfo.OutputType.general
        printInfo.jobName = name
        
    
        let printController = UIPrintInteractionController.shared
        printController.printInfo = printInfo
        
        
        var docsArr : [ZSPDFDocument] = []
//        var dataArray = [URL]()
        for filePath in filePathArray {
            if let doc =  ZSPDFDocument(url: URL(fileURLWithPath: filePath)){
                docsArr.append(doc)
            }
        }
        
        let imagesArr = convertPDFToImages(documents: docsArr)
        
        printController.printingItems = imagesArr
        printController.present(from: parentVC.view.frame, in: parentVC.view, animated: true, completionHandler: nil)
    }

    static func convertPDFToImages(documents:[ZSPDFDocument]) -> [UIImage] {
        var imagesArray: [UIImage] = []

        documents.forEach { (pdfdocument) in
            for index in 0...(pdfdocument.pageCount-1){
                let page = pdfdocument.page(at: index)
                let pageRect = page?.bounds(for: .artBox)
                let pdfImgSize  = CGSize(width: pageRect!.size.width * 2, height: pageRect!.size.height * 2)

                if var image = page?.thumbnail(of: pdfImgSize, for: .artBox){
                   /*if let rotation = page?.rotation, rotation > 0, SwiftUtils.isIOS13AndAbove, !DeviceType.isMac {
                        image = image.rotate(radians: (Float(rotation) * (.pi / 180)))
                    }*/
                    imagesArray.append(image)
                }
            }
        }
        
        return imagesArray
    }
}
