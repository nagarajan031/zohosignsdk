//
//  FieldSelectionView.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 12/04/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

struct SignFieldMenuItem {
    var title = ""
    var icon  = UIImage()
    var signFieldType : ZSignFieldType = .signature
}

public class IphoneEditorFieldSelectionView: UIView  {

    var menuItemArray : [SignFieldMenuItem]   =  []
    public var isSignAndSend = false
//    public var isPrefillByYou = false

    let scrollView = UIScrollView()
    public var selectedFieldType  : ZSignFieldType = .others
    
    public typealias fieldDidSelectBlock = (_ field : ZSignFieldType) ->Void
    
    public var selectionBlock : fieldDidSelectBlock?

    let cellWidth = 75
    @objc public func initateSetup() {
        self.backgroundColor  = UIColor(red:0.16, green:0.16, blue:0.16, alpha:1.00)

        
        
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        if (CGFloat(menuItemArray.count * cellWidth) < bounds.size.width){
            scrollView.frame = CGRect(x: 0, y: 0, width: menuItemArray.count * cellWidth, height: 60)
             scrollView.center  = CGPoint(x: self.center.x, y: 30);
        }else{
            scrollView.frame = bounds
        }
    }
    
    deinit {
        ZInfo("deinit")
    }
    
    func updateFieldsListForReceipient(_ recep : ZSRecipient)  {
        subviews.forEach { (subview) in
            subview.removeFromSuperview()
        }
        scrollView.subviews.forEach { (subview) in
            subview.removeFromSuperview()
        }
        
        let signatureField = SignFieldMenuItem(title: "sign.mobile.field.signature".ZSLString, icon: UIImage(named:"menu_sign")!, signFieldType: .signature)
        let initialField = SignFieldMenuItem(title: "sign.mobile.field.initial".ZSLString, icon: UIImage(named:"menu_initial")!, signFieldType: .initial)
        let emailField = SignFieldMenuItem(title: "sign.mobile.field.email".ZSLString, icon: UIImage(named:"menu_email")!, signFieldType: .email)
        let nameField = SignFieldMenuItem(title: "sign.mobile.field.name".ZSLString, icon: UIImage(named:"menu_name")!, signFieldType: .name)
        let companyField = SignFieldMenuItem(title: "sign.mobile.field.company".ZSLString, icon: UIImage(named:"menu_companyname")!, signFieldType: .company)
        let signDateField = SignFieldMenuItem(title: "sign.mobile.field.signDate".ZSLString, icon: UIImage(named:"menu_date")!, signFieldType: .signDate)
        let jobField = SignFieldMenuItem(title: "sign.mobile.field.jobtitle".ZSLString, icon: UIImage(named:"menu_jobtitle")!, signFieldType: .jobTitle)
        let checkboxField = SignFieldMenuItem(title: "sign.mobile.field.checkbox".ZSLString, icon: UIImage(named:"menu_checkbox")!, signFieldType: .checkBox)
        let textField = SignFieldMenuItem(title: "sign.mobile.field.textfield".ZSLString, icon: UIImage(named:"menu_textfield")!, signFieldType: .textBox)
        let dateField = SignFieldMenuItem(title: "sign.mobile.field.date".ZSLString, icon: UIImage(named:"menu_date")!, signFieldType: .date)
        let dropDownField = SignFieldMenuItem(title: "sign.mobile.field.dropDown".ZSLString, icon: UIImage(named:"menu_dropdown")!, signFieldType: .dropDown)
        let attachementField = SignFieldMenuItem(title: "sign.mobile.field.attachment".ZSLString, icon: UIImage(named:"menu_attachment")!, signFieldType: .attachment)
        let radioField = SignFieldMenuItem(title: "sign.mobile.field.radio".ZSLString, icon: UIImage(named:"menu_checkbox")!, signFieldType: .radioGroup)
        
        menuItemArray.removeAll()
        menuItemArray.append(contentsOf: [signatureField,initialField,emailField,nameField,signDateField,companyField,jobField,checkboxField,textField,dateField,dropDownField,radioField])
        if isSignAndSend {
            menuItemArray.removeLast()//datefield
            menuItemArray.removeLast()//dropdownfield
            menuItemArray.removeLast()//radioField
        }
        
        if (UserManager.shared.currentUser?.isAttachmentFeatureAvailable ?? false) && !isSignAndSend && !recep.isTemplateUser{
            menuItemArray.append(attachementField)
        }
        
        if (CGFloat(menuItemArray.count * cellWidth) < bounds.size.width){
            scrollView.frame = CGRect(x: 0, y: 0, width: menuItemArray.count * cellWidth, height: 60)
        }else{
            scrollView.frame = bounds
        }
        
        
        scrollView.autoresizingMask =  [.flexibleWidth , .flexibleHeight]
        scrollView.backgroundColor  = UIColor(red:0.16, green:0.16, blue:0.16, alpha:1.00)
        scrollView.showsHorizontalScrollIndicator = false;
        self.addSubview(scrollView)
        
        var cellIndex = 0
        
        let separatorLine =  UIView()
        separatorLine.backgroundColor = ZColor.seperatorColor
        separatorLine.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: 0.8)
        separatorLine.autoresizingMask = .flexibleWidth
        self.addSubview(separatorLine)
        
        for field in menuItemArray{
            let button      =  ZSButton()
            button.frame    =  CGRect(x: cellWidth * cellIndex, y: 0, width: cellWidth, height: 60)
            //            button.autoresizingMask = .flexibleHeight
            button.setImage(ZSImage.tintColorImage(field.icon, col: ZColor.whiteColor), for: .normal)
            //            button.setImage(ZSImage.tintColorImage(field.icon, col: ZColor.secondaryTextColorDark), for: .normal)
            button.addTarget(self, action: #selector(fieldButtonTapped(_:)), for: .touchUpInside)
            button.imageEdgeInsets = UIEdgeInsets.init(top: 10, left: 10, bottom: 24, right: 10)
            button.tag = cellIndex
            button.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
            cellIndex       =  cellIndex + 1
            scrollView.addSubview(button)
            
            let titleLabel = UILabel()
            titleLabel.frame    =   CGRect(x: 3, y: 33, width: 69, height: 18)
            titleLabel.textColor     =    ZColor.whiteColor
            //            titleLabel.font          =    ZSFont.mediumFont(withSize: 12)
            titleLabel.font           =     ZSFont.bodyFont()
            titleLabel.adjustsFontForContentSizeCategory = true
            titleLabel.textAlignment  =   .center
            titleLabel.adjustsFontSizeToFitWidth = true
            titleLabel.numberOfLines = 2
            titleLabel.text    = field.title
            button.addSubview(titleLabel)
        }
        
        scrollView.contentSize = CGSize(width: cellWidth * cellIndex, height: 60)
    }
    
    @objc func fieldButtonTapped(_ sender : UIButton){
        let menuItem = menuItemArray[sender.tag]
        selectionBlock?(menuItem.signFieldType)
    }
    
    @objc public func viewInTouchLocation(_ point : CGPoint) -> UIView? {
        let adjustedPointX = point.x - scrollView.frame.origin.x
        
        let pointX = Int((adjustedPointX + scrollView.contentOffset.x) / CGFloat(cellWidth))
        
        if pointX >= menuItemArray.count {
            return nil;
        }
        let view = scrollView.subviews[pointX]

        SwiftUtils.generateImpactFeedBack()
        
        
        let menuItem = menuItemArray[pointX]
        selectedFieldType = menuItem.signFieldType
        let imgView  = UIImageView()
        imgView.backgroundColor =   scrollView.backgroundColor
        imgView.image = view.asImage()
        imgView.frame = view.frame
        return imgView
    }
    
  
}
