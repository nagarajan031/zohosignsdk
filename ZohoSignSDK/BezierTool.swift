//
//  BezierTool.swift
//  SignatureView
//
//  Created by somesh-8758 on 13/02/20.
//  Copyright © 2020 Somesh-8758. All rights reserved.
//

import UIKit

class BezierTool: NSObject {
    static func bezierSmooth(_ points: [CGPoint]) -> [CGPoint]{
        var smoothPoints = [CGPoint]()
        if points.count > 2 {
            var p2Point = points[0]
            var p1Point = points[1]
            
            for point in points[2..<points.count] {
                let bezierPoints = self.bezierPoints(p2Point, p1Point, point)
                smoothPoints.append(contentsOf: bezierPoints)
                (p2Point,p1Point) = (p1Point,point)
            }
        }
        return smoothPoints
    }
    
    private static func bezierPoints(_ p1Point: CGPoint,_ p2Point: CGPoint,_ p3Point: CGPoint) -> [CGPoint]{
        var points = [CGPoint]()
        let mid1 = p2Point / p1Point
        let mid2 = p3Point / p2Point
        
        let distance: Int = 10
        
        for i in 0..<distance {
            let t = CGFloat(i/distance)
            let newPoint = normalisedPoint(mid1, p2Point, mid2, withConstant: t)
            points.append(newPoint)
        }
        
        return points
    }
    
    static func normalisedPoint(_ point1: CGPoint,_ point2: CGPoint,_ point3: CGPoint,withConstant t: CGFloat) -> CGPoint {
        return (point1 * pow(1-t, 2.0)) +
        (point2 * (2.0*(1-t) * t)) +
        (point3 * (t*t))
    }
}
