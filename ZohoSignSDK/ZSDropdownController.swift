//
//  ZSDropdownController.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 11/12/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZSDropdownController: UITableViewController {
    
    let noneValue = "sign.mobile.field.dropdown.noneValue".ZSLString
    public var fieldName : String = ""
//    public var optionsArray : [ZDropdownOption] = []
    public var optionsArray : [DropdownOption] = []
    public var selectedValue  : String = "sign.mobile.field.dropdown.noneValue".ZSLString
//    public var completionHandler : ((_ fieldName :String,_ options : [ZDropdownOption], _ selectedValue :String?) -> Void)?
    public var completionHandler : ((_ fieldName :String,_ options : [DropdownOption], _ selectedValue :String?) -> Void)?


    override public func viewDidLoad() {
        super.viewDidLoad()
        title = "sign.mobile.field.dropDown".ZSLString
        view.backgroundColor = ZColor.bgColorGray
        navigationController?.view.backgroundColor = ZColor.bgColorGray
        navigationController?.navigationBar.barTintColor = ZColor.bgColorGray
        
        tableView.rowHeight = TableviewCellSize.normal
        if DeviceType.isIpad {
            preferredContentSize = CGSize(width: 340, height: 500)
        }
        if !DeviceType.isIpad {
            navigationItem.leftBarButtonItem = ZSBarButtonItem.barButton(type: .cancel, target: self, action: #selector(cancelButtonPressed))
        }

        navigationItem.rightBarButtonItem = ZSBarButtonItem.barButton(type: .done, target: self, action: #selector(doneButtonPressed))
        navigationItem.rightBarButtonItem?.isEnabled = false
        navigationItem.rightBarButtonItem?.isEnabled = (optionsArray.count > 0)


        tableView.register(ZTextFieldCell.self, forCellReuseIdentifier: "TextCell")
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")

    }
    
    @objc func cancelButtonPressed(){
        dismiss(animated: true, completion: nil)
    }

    @objc func doneButtonPressed(){
        var _selectedvalue : String?
        if selectedValue != noneValue{
            _selectedvalue = selectedValue
        }
        fieldName = fieldName.trimmingCharacters(in: .whitespacesAndNewlines)
        completionHandler?(fieldName,optionsArray,_selectedvalue)
        dismiss(animated: true, completion: nil)
    }
    
    @objc func addOptionButtonPressed()  {
        var option = DropdownOption()
        option.dropdownValue = ""
        option.dropdownOrder = String(optionsArray.count)
        optionsArray.append(option)
        
        let indexPath = IndexPath(row: optionsArray.count-1, section: 1)
        tableView.insertRows(at: [indexPath], with: .bottom)
        
        let cell = tableView.cellForRow(at: indexPath) as? ZTextFieldCell
        cell?.textField.becomeFirstResponder()
        
        validateRequiredFields()
    }
    
    @objc func deleteOption( index : Int)  {
        tableView.endEditing(true)
        view.endEditing(true)
        let opt = optionsArray[index]
        if opt.dropdownValue == selectedValue{
            selectedValue = "sign.mobile.field.dropdown.noneValue".ZSLString
            tableView.reloadRows(at: [IndexPath(row: 0, section: 2)], with: .none)
        }
        optionsArray.remove(at: index)
        
        tableView.reloadSections(IndexSet(integer: 1), with: .fade)
        validateRequiredFields()
    }
    
    func validateRequiredFields()  {
        if fieldName.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            navigationItem.rightBarButtonItem?.isEnabled = false
            return
        }
        if optionsArray.count <= 0 {
            navigationItem.rightBarButtonItem?.isEnabled = false
            return
        }
        
        let allMatch = optionsArray.allSatisfy{ ($0.dropdownValue.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == false) }
        
        if navigationItem.rightBarButtonItem?.isEnabled != allMatch {
            navigationItem.rightBarButtonItem?.isEnabled = allMatch
            tableView.reloadRows(at: [IndexPath(row: 0, section: 2)], with: .none)
        }
    }
    
    @objc func changeOption(value : String, index : Int)  {

        optionsArray[index].dropdownValue = value
        validateRequiredFields()
        
//        let indexPath = IndexPath(row: index, section: 1)
//        tableView.reloadRows(at: [indexPath], with: .fade)
    }
    
    // MARK: - UITableViewDataSource
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "sign.mobile.field.name".ZSLString
        case 1:
            return "sign.mobile.field.dropdown.dropdownOptions".ZSLString
        case 2:
            return "sign.mobile.field.defaultValue".ZSLString
        default:
            return ""
        }
    }
    
    public override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return (section == 1) ?  70 : 25
    }
    
    override public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (section == 1) {
            let sectionFooterView  =   UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
            sectionFooterView.backgroundColor = .clear
//            sectionFooterView.layer.borderWidth = 0.3
//            sectionFooterView.layer.borderColor = ZColor.seperatorColor.cgColor
            
            let subView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
            subView.backgroundColor = ZColor.bgColorWhite
            sectionFooterView.addSubview(subView)
            
            let bottomBorder = UIView()
            bottomBorder.frame =   CGRect(x: 0, y: 49.7, width: tableView.frame.width, height: 0.3)
            bottomBorder.backgroundColor = ZColor.seperatorColor
            sectionFooterView.addSubview(bottomBorder)
            
            let addOptionButton = ZSButton()
            addOptionButton.frame = CGRect(x: 18, y: 0, width: tableView.frame.height - 32, height: 50)
            addOptionButton.backgroundColor = .clear
            addOptionButton.tag = 1
            addOptionButton.titleLabel?.font = ZSFont.mediumFontLarge()
            addOptionButton.contentHorizontalAlignment = .left
            addOptionButton.addTarget(self, action: #selector(addOptionButtonPressed), for: UIControl.Event.touchUpInside)
            addOptionButton.setImage(ZSImage.tintColorImage(withNamed: "plus_icon", col: ZColor.buttonColor), for: UIControl.State.normal)
            addOptionButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
            addOptionButton.setTitleColor(ZColor.buttonColor, for: UIControl.State.normal)
            addOptionButton.setTitle("sign.mobile.field.dropdown.addOption".ZSLString, for: .normal)
            sectionFooterView.addSubview(addOptionButton)
            
            return sectionFooterView
        }
        else {
            return nil
        }
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return optionsArray.count
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            
            cell.accessoryType = .disclosureIndicator
            cell.textLabel?.text         =   selectedValue
            cell.textLabel?.font        =   ZSFont.regularFontLarge()

            //Selected bgview
            let selectedView = UIView()
            selectedView.backgroundColor = ZColor.listSelectionColor;
            cell.selectedBackgroundView = selectedView;
            
//            let navEnabled = navigationItem.rightBarButtonItem?.isEnabled ?? true
//            cell.isUserInteractionEnabled = navEnabled
//            cell.textLabel?.textColor = navEnabled ? ZColor.primaryTextColor : ZColor.secondaryTextColorDark

            cell.textLabel?.textColor =  ZColor.primaryTextColor
            cell.backgroundColor = ZColor.bgColorWhite

            return cell
        }
        
        weak var _self = self

        let cell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath) as! ZTextFieldCell
        if indexPath.section == 0 {
            cell.setTextField(placeholder: "sign.mobile.request.enterYourText".ZSLString, value: fieldName, editable: true)
            cell.textFieldShouldChangeCharactersIn = {(_ , inputText) in
                _self?.fieldName = inputText
                _self?.validateRequiredFields()
            }
            cell.showDeleteButton = false
        }
        else if indexPath.section == 1 {
            cell.setTextField(placeholder: String(format: "sign.mobile.field.dropdown.option".ZSLString, (indexPath.row+1)), value: optionsArray[indexPath.row].dropdownValue, editable: true)
            cell.showDeleteButton = true
//            cell.showDeleteButton = (indexPath.section == 1 && indexPath.row >= 2)
            cell.deleteButtonTapHandler = {
                _self?.deleteOption(index: indexPath.row)
            }
            cell.textFieldShouldChangeCharactersIn = {(_ , inputText) in
                _self?.changeOption(value: inputText, index: indexPath.row)
            }
        }
        
        cell.selectionStyle = .none
        
        cell.maxLimit = kSINGLELINE_MAX_COUNT    // 255
        cell.checkForTextFieldRegex(regex: ZS_Regex_ForName, showPopInView: self.view)
        
        
//        cell.textFieldEndEditing = { (textStr) -> Void in
//            _self?.changeOption(value: textStr, index: indexPath.row)
//        }
        
        return cell
    }
    

    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            tableView.endEditing(true)
            let selectionController = ZSSingleSelectionContoller(style : .grouped)
            selectionController.title = "sign.mobile.field.defaultValue".ZSLString
            var tempArray : [String] = []
            for option in optionsArray {
                tempArray.append(option.dropdownValue)
            }
            
            tempArray.insert(noneValue, at: 0)
            selectionController.selectedValue = selectedValue
            selectionController.optionsArray  = tempArray
            selectionController.selectedValueDidChange = {[weak self] index in
                if(index == 0){self?.selectedValue =  "sign.mobile.field.dropdown.noneValue".ZSLString;}
                else{
                    self?.selectedValue = self?.optionsArray[index-1].dropdownValue ?? "sign.mobile.field.dropdown.noneValue".ZSLString
                }
                tableView.reloadRows(at: [IndexPath(row: 0, section: 2)], with: .fade)
            }
            navigationController?.pushViewController(selectionController, animated: true)
        }
    }
    
    deinit {
        ZInfo("deinit")
    }
}

extension ZSDropdownController: ZPresentable {
    //should implement this protocol to have custom transition for below iOS 13.0
}
