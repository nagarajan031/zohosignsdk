//
//  ZSFieldType.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 19/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation


class ZSFieldType : Codable {
    
    var typeId: String?
    var name: String?
    var category: String?
    var isMandatory = false
//    var type: signFieldType?
    var fieldType : ZSignFieldType = .others
    
    
   
    enum CodingKeys: String, CodingKey {
        case typeId
        case name
        case category
        case isMandatory
        case fieldType
    }

    func update(withDict dict: [String : AnyObject]) {
        if let fieldTypeName = dict["field_type_name"] as? String {
            switch fieldTypeName {
                case "Signature":
                    fieldType = .signature
                case "Initial":
                    fieldType = .initial
                case "Company":
                    fieldType = .company
                case "Name":
                    fieldType = .name
                case "Email":
                    fieldType = .email
                case "Date":
                    fieldType = .signDate
                case "CustomDate":
                    fieldType = .date
                case "Jobtitle":
                    fieldType = .jobTitle
                case "Checkbox":
                    fieldType = .checkBox
                case "Dropdown":
                    fieldType = .dropDown
                case "Attachment":
                    fieldType = .attachment
                case "Radiogroup":
                    fieldType = .radioGroup
                case "Textfield":
                    fieldType = .textBox
                default:
                    fieldType = .others
            }
        }
        else{
            fieldType = .others
        }
        
         self.typeId           =   dict[keyPath: "field_type_id"]
         self.name             =   dict[keyPath: "field_type_name"]
         self.category         =   dict[keyPath: "field_category"]
         self.isMandatory      =   dict[keyPath: "is_mandatory"] ?? false
    }
    

    
}
