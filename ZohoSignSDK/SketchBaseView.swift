//
//  SketchBaseView.swift
//  SignatureView
//
//  Created by somesh-8758 on 14/02/20.
//  Copyright © 2020 Somesh-8758. All rights reserved.
//

import UIKit

class SketchTool: UIBezierPath {
    var lineColor: UIColor = SketchDefaults.lineColor
    var lineAlpha: CGFloat = SketchDefaults.lineAlpha
    var _lineWidth: CGFloat = SketchDefaults.lineWidth
    var path: CGMutablePath = .init()
    
    var subPaths: [SketchPath]
    var controlPoints: [CGPoint]
    
    var counter: Int = 0
    
    override init() {
        path = CGMutablePath()
        subPaths = [SketchPath]()
        controlPoints = Array(repeating: CGPoint.zero, count: 3)
        super.init()
        lineCapStyle = .round
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func draw() {}
    func touchesBegin(_ point: CGPoint, with force: CGFloat) { }
    @discardableResult
    func touchesMoved(_ point: CGPoint, with force: CGFloat) -> CGRect { return .zero}
    @discardableResult
    func touchesEnded(_ point: CGPoint, with force: CGFloat) -> CGRect { return .zero}
    func showPreview(_ points: [CGPoint]) { }
}
