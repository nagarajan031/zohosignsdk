//
//  ZRequestDetailsConfirmationController.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 09/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

class ZRequestDetailsConfirmationController : UIViewController, UITableViewDelegate, UITableViewDataSource{
    var menuItemArray : [ZSRecipient] = []
    var confirmDetailsHandler : ((_ isSignNow : Bool) -> Void)?
    var menuTableView : UITableView = UITableView()
    let footerView = PopupToolbar()

    
    init(menuItems: [ZSRecipient]) {
        super.init(nibName: nil, bundle: nil)
        self.menuItemArray  =   menuItems
        initateSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor   =   ZColor.bgColorGray
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return [.portrait]
    }
    
    override var shouldAutorotate: Bool
    {
        return true
    }
    
    var isOwnerPartOfSigners = false
    
    var tableHeight : CGFloat = 0
    // MARK: - Private
    
    func initateSetup()  {
        
        self.title = "sign.mobile.request.confirmDetails".ZSLString
        
        
        for recep in menuItemArray
        {
            if (recep.email == UserManager.shared.currentUser?.email && recep.signingOrder <= 1){
                isOwnerPartOfSigners = true
                break
            }
        }
        
        let confirmationStr = "sign.mobile.request.verifyNumberOfFields".ZSLString

        let size = confirmationStr.textSize(withConstrainedSize: CGSize(width: 340, height: 100), font: ZSFont.regularFont(withSize: 15))

        
        let headerView = UIView()
        
        let infoLabel = UILabel()
        infoLabel.frame = CGRect(x: 16, y: 20, width: size.width, height: size.height)
        infoLabel.text = confirmationStr
        infoLabel.font = ZSFont.regularFont(withSize: 15)
        infoLabel.textColor = ZColor.secondaryTextColorDark
        infoLabel.numberOfLines = 0
        headerView.addSubview(infoLabel)
        headerView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: infoLabel.frame.size.height + 34)
        
      
        tableHeight = (TableviewCellSize.receipientList *  CGFloat(menuItemArray.count) ) + headerView.frame.size.height
        
        menuTableView.frame                             =    view.bounds
        menuTableView.autoresizingMask                  =   [.flexibleWidth,.flexibleHeight]
        menuTableView.delegate                          =    self
        menuTableView.dataSource                        =    self
        menuTableView.rowHeight                         =    TableviewCellSize.receipientList
        menuTableView.setSeparatorStyle()
        menuTableView.showsVerticalScrollIndicator      =    false
        menuTableView.backgroundColor                   =   ZColor.bgColorWhite;
        view.addSubview(menuTableView)
        
        createFooterButtons()

        menuTableView.tableHeaderView = headerView
        
        var bottomSafeArea = footerView.getHeight() + (DeviceType.isIpad ? 14 :  (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0) + 0)
        if DeviceType.isIpad, (footerView.secondaryActionButton.frame.maxY > 1) {
            bottomSafeArea += bottomMargin
        }
        menuTableView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-bottomSafeArea)
        }
        
        if(DeviceType.isIpad){
            preferredContentSize = CGSize(width: 375, height: tableHeight + bottomSafeArea)
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: {[weak self] (transition) in
            if UIApplication.shared.isSplitOrSlideOver{
                self?.navigationItem.leftBarButtonItem    =    ZSBarButtonItem.barButton(type: .cancel, target: self, action: #selector(self?.cancelBtnPressed))
            }else{
                self?.navigationItem.leftBarButtonItem    =    nil
            }
            
        }) {(context) in
            
        }
    }
    
    func createFooterButtons() {
        
        
      /*  let footerView  = UIView()
        footerView.backgroundColor = ZColor.bgColorWhite
        self.view.addSubview(footerView)
        
        let borderView = UIView(frame: CGRect(x: 0, y: 0, width:footerView.frame.size.width , height: 0.4))
        borderView.backgroundColor = ZColor.seperatorColorLight
        footerView.addSubview(borderView)
        
        let sendLaterBtn = ZSButton()
        sendLaterBtn.layer.cornerRadius =   4
        sendLaterBtn.layer.borderWidth  =   1
        sendLaterBtn.layer.borderColor  =   ZColor.buttonColor.cgColor
        sendLaterBtn.autoresizingMask   =   [.flexibleWidth]
        sendLaterBtn.backgroundColor    =   ZColor.bgColorWhite
        sendLaterBtn.titleLabel?.font   =   ZSFont.buttonTitleFont()
        sendLaterBtn.titleLabel?.textAlignment = .center
        sendLaterBtn.addTarget(self, action: #selector(signLaterBtnTapped), for: UIControl.Event.touchUpInside)
        sendLaterBtn.setTitleColor(ZColor.buttonColor, for: UIControl.State())
        sendLaterBtn.setTitle("Sign Later".ZSLString, for: UIControl.State());
        
        let confirmBtn = ZSButton()
        confirmBtn.layer.cornerRadius =   4
        confirmBtn.autoresizingMask   =   [.flexibleWidth]
        confirmBtn.backgroundColor    =   ZColor.buttonColor
        confirmBtn.titleLabel?.font   =   ZSFont.buttonTitleFont()
        confirmBtn.titleLabel?.textAlignment = .center
        confirmBtn.addTarget(self, action: #selector(signNowBtnTapped), for: UIControl.Event.touchUpInside)
        confirmBtn.setTitleColor(ZColor.whiteColor, for: UIControl.State())
        confirmBtn.setTitle("Sign Now".ZSLString, for: UIControl.State());

   
        footerView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(menuTableView.snp.bottom)
            make.bottom.equalToSuperview()
        }
        
        borderView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(0.5)
            make.top.equalToSuperview()
        }
        
        footerView.addSubview(sendLaterBtn)
        footerView.addSubview(confirmBtn)
        
        if isOwnerPartOfSigners {
            confirmBtn.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(12)
                make.width.equalTo(280).priority(.high)
                make.left.equalTo(footerView.snp.centerX).offset(15).priority(.high)
                make.right.equalToSuperview().inset(cellValueLabelX).priority(.high)
                make.height.equalTo(38)
            }
            sendLaterBtn.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(12)
                make.width.equalTo(280).priority(.high)
                make.right.equalTo(footerView.snp.centerX).inset(15).priority(.high)
                make.left.equalToSuperview().offset(cellValueLabelX).priority(.high)
                make.height.equalTo(38)
            }
        }
        else{
            sendLaterBtn.isHidden = true
            confirmBtn.removeTarget(self, action:  #selector(signNowBtnTapped), for: .touchUpInside)
            confirmBtn.setTitle("sign.mobile.request.confirmAndSend".ZSLString, for: UIControl.State());
            confirmBtn.addTarget(self, action: #selector(confirmBtnPressed), for: .touchUpInside)

            confirmBtn.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(12)
                make.right.equalToSuperview().offset(-cellValueLabelX)
                make.left.equalTo(cellValueLabelX)
                make.height.equalTo(38)
            }
            
        }
        
        
        
        self.view.updateConstraints()
        self.view.layoutIfNeeded()*/
        
        
        if isOwnerPartOfSigners {
            footerView.titleLabel.text = "sign.mobile.request.confirmNumberOfFields".ZSLString
            footerView.primaryActionButton.setTitle("sign.mobile.request.signNow".ZSLString, for: .normal)
            footerView.secondaryActionButton.setTitle("sign.mobile.request.signLater".ZSLString, for: .normal)
            footerView.primaryActionButton.addTarget(self, action: #selector(signNowBtnTapped), for: .touchUpInside)
            footerView.secondaryActionButton.addTarget(self, action: #selector(signLaterBtnTapped), for: .touchUpInside)
        }else{
            footerView.primaryActionButton.setTitle("sign.mobile.request.confirmAndSend".ZSLString, for: .normal)
            footerView.primaryActionButton.addTarget(self, action: #selector(confirmBtnPressed), for: .touchUpInside)
        }
        view.addSubview(footerView)
        
        footerView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(menuTableView.snp.bottom)
            make.bottom.equalToSuperview()
        }
        footerView.initateSetup()

        self.view.updateConstraints()
        self.view.layoutIfNeeded()
        
    
    }
    
    
    @objc func cancelBtnPressed()
    {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func signLaterBtnTapped()  {
        confirmDetailsHandler?(false)
        dismiss(animated: true, completion: nil)
    }
    
    @objc func signNowBtnTapped()  {
        confirmDetailsHandler?(true)
        dismiss(animated: true, completion: nil)
    }
    
    func getViewControllerHeight() -> CGFloat {
        let bottomSafeArea =  DeviceType.isIpad ? 14 :  (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0) + 0

        let footerHeight = footerView.getHeight() + bottomSafeArea

        menuTableView.snp.removeConstraints()
        menuTableView.snp.updateConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-footerHeight)
        }
        
        return tableHeight + footerView.getHeight()
    }
    
    @objc func confirmBtnPressed()
    {
        confirmDetailsHandler?(false)
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - TableView Data Source
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return menuItemArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let recipient =  menuItemArray[indexPath.row]
        if (recipient.isTemplateUser) { return 60;}
        
        return TableviewCellSize.receipientList;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let identifier = "Cell"
        
        var cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        let recipient =  menuItemArray[indexPath.row]
        
        if cell == nil {
            cell =  UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: identifier)
            
            //Selected bgview
            let selectedView = UIView()
            selectedView.backgroundColor = ZColor.listSelectionColor;
            cell.selectedBackgroundView = selectedView;
        }
        var recipientName  = recipient.userRole
        if ((recipientName == nil) || (recipientName == "")){
            recipientName = String(format: "%@ (%@)", recipient.name ?? "",recipient.email ?? "")
        }
        
        
        cell.textLabel?.text            =    recipientName
        cell.textLabel?.font            =    ZSFont.listTitleFont()
        cell.detailTextLabel?.font      =    ZSFont.listSubTitleFont()
        
        cell.textLabel?.textColor    =       ZColor.primaryTextColor
        cell.detailTextLabel?.textColor =    ZColor.secondaryTextColorDark
        cell.backgroundColor            =   ZColor.bgColorWhite
        
        switch recipient.actionType {
        case ZS_ActionType_Sign:
            cell.detailTextLabel?.text =  "sign.mobile.recipient.signer".ZSLString
        case ZS_ActionType_Approver:
            cell.detailTextLabel?.text =  "sign.mobile.recipient.approver".ZSLString
        case ZS_ActionType_InPerson:
            cell.detailTextLabel?.text =  "sign.mobile.common.inpersonSigner".ZSLString
            var inpersonName  =  recipient.userRole
            if ((inpersonName == nil) || (inpersonName == "")){
                inpersonName = recipient.inPersonName
            }
            if ((inpersonName == nil) || (inpersonName == "")){
                inpersonName = recipient.inPersonEmail
            }
            if ((inpersonName == nil) || (inpersonName == "")){
                inpersonName = String(format:"%@: %@","sign.mobile.recipient.host".ZSLString,recipient.name ?? "")
            }
            if ((inpersonName == nil) || (inpersonName == String(format:"%@: ","sign.mobile.recipient.host".ZSLString))){
                inpersonName = String(format:"%@: %@","sign.mobile.recipient.host".ZSLString,recipient.email ?? "")
            }
            cell.textLabel?.text          =    inpersonName
        default:
            cell.detailTextLabel?.text =  "sign.mobile.recipient.getsACopy".ZSLString
            
            break
        }
        
        
        let accessView = UILabel()
        accessView.frame = CGRect(x: 0, y: 0, width: 40, height: 20)
        accessView.text = String(recipient.draftFieldListArray.count)
        accessView.textAlignment = .right
        accessView.textColor = ZColor.secondaryTextColorDark
        accessView.numberOfLines = 1
        accessView.adjustsFontSizeToFitWidth = true
        //        accessView.backgroundColor = ZColor.listSelectionColor
        accessView.font = ZSFont.regularFontLarge()
        cell.accessoryView = accessView
        
        cell.isUserInteractionEnabled =  false
        
        
        return cell
    }
    
       
}

extension ZRequestDetailsConfirmationController: ZPresentable {
    var navBar: PresentationNavBar {
        PresentationNavBar(size: PresentationBarSize.withTitleBig, title: title, titleAlignment: .center)
    }
    
    var popupHeight: CGFloat {
        return getViewControllerHeight() + navBar.size + bottomMargin
    }
    
    var direction: PresentationDirection {
        return .bottomWithCustomHeight
    }
}
