//
//  UIColor+.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 24/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public extension UIColor {
    @available(iOS 13, *)
    public static func setAppearance(dark: UIColor,light: UIColor) -> UIColor {
        return UIColor {(trait) -> UIColor in
            if trait.userInterfaceStyle == .dark {
                return dark
            } else {
                return light
            }
        }
    }
    
    public static func setColor(dark: UIColor,light: UIColor) -> UIColor {
        if #available(iOS 13, *){
            return .setAppearance(dark: dark, light: light)
        } else {
            return light
        }
    }
    
    public func resolveDynamicColor(with traitCollection: UITraitCollection) -> UIColor{
        if #available(iOS 13, *){
            return resolvedColor(with: traitCollection)
        } else {
            return self
        }
    }
    
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }
    
    convenience init(rgb: Int, a: CGFloat = 1.0) {
        self.init(
            red: rgb >> 16 & 0xFF,
            green: rgb >> 8 & 0xFF,
            blue: rgb & 0xFF,
            a: a
        )
    }
}
