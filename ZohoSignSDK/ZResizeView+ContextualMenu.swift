//
//  ZResizeView+ContextualMenu.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 10/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

@available(iOS 13,*)
extension ZResizeView : UIContextMenuInteractionDelegate{
    public func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
            return UIContextMenuConfiguration(identifier: nil, previewProvider: { () -> UIViewController? in
                return nil
            }) { (menu) -> UIMenu? in
                var menuChildren : [UIMenuElement] = []
                
                let fontAction = UIAction(title: "sign.mobile.fields.font".ZSLString, image: nil ,identifier: nil){ action in
                    self.showFontPropertiesView()
                }
                
                let dateAction = UIAction(title: "sign.mobile.profile.dateFormat".ZSLString, image: nil ,identifier: nil){ action in
                    self.showDatePropertiesView()
                }

                let editAction = UIAction(title: "sign.mobile.common.edit".ZSLString, image: nil ,identifier: nil){ action in
                    self.delegate?.resizeViewDidEditButtonTapped(self)
                }
                
                let firstNameAction = UIAction(title: "sign.mobile.field.firstname".ZSLString, image: nil ,identifier: nil){ action in
                    self.delegate?.resizeViewDidChangeSubtype(type: .firstName, resizeView: self)
                }
                
                let lastNameAction = UIAction(title: "sign.mobile.field.lastname".ZSLString, image: nil ,identifier: nil){ action in
                    self.delegate?.resizeViewDidChangeSubtype(type: .lastName, resizeView: self)
                }
                
                let fullNameAction = UIAction(title: "sign.mobile.field.fullname".ZSLString, image: nil ,identifier: nil){ action in
                    self.delegate?.resizeViewDidChangeSubtype(type: .fullName, resizeView: self)
                }
                
                let optionalAction = UIAction(title: "sign.mobile.common.optional".ZSLString, image: nil ,identifier: nil){ action in
                    self.signField.isMandatory = false;
                }
                
                let mandatoryAction = UIAction(title: "sign.mobile.common.mandatory".ZSLString, image: nil ,identifier: nil){ action in
                    self.signField.isMandatory = true;
                }
                
                let checkAction = UIAction(title: "sign.mobile.fields.checkbox.checked".ZSLString, image: nil ,identifier: nil){ action in
                    self.setFieldCheckboxValue(true)
                }
            
                
                let uncheckAction = UIAction(title: "sign.mobile.fields.checkbox.unchecked".ZSLString, image: nil ,identifier: nil){ action in
                    self.setFieldCheckboxValue(false)
                }
                
                let deleteAction = UIAction(title: "sign.mobile.common.delete".ZSLString, image: nil ,identifier: nil){ action in
                    self.deleteButtonClicked()
                }
                let readonlyAction = UIAction(title: "sign.mobile.common.readonly".ZSLString, image: nil ,identifier: nil){ action in
                    self.signField.isReadOnly = true;
                    if (self.signField.type == .checkBox) {
                        self.setFieldCheckboxValue(true)
                    }
                }
                
                let readwriteAction = UIAction(title: "sign.mobile.common.readAndWrite".ZSLString, image: nil ,identifier: nil){ action in
                    self.signField.isReadOnly = false;
                }
               
                switch (self.signField.type) {
                    case .signature,.initial,.attachment:
                        if (self.resizeViewMode == .sentDoc_Create) {
                            menuChildren.append(((self.signField.isMandatory) ? optionalAction : mandatoryAction))
                        }
                        break
                    case .email,.jobTitle, .company:
                        menuChildren.append(contentsOf: [fontAction])
                        break;
                    case .name:
                        var isLastNameAvailable =  true;
                        if (self.isSampleSigning) {
                            isLastNameAvailable = true;
                        }
                        else if ((self.resizeViewMode == .sentDoc_selfSign) && ((UserManager.shared.currentUser?.lastName?.count == 0) || (UserManager.shared.currentUser?.lastName == " "))) {
                            isLastNameAvailable = false;
                        }
                        
                    
                        
                        
                        if (self.signField.subType == .firstName) {
                            menuChildren.append(contentsOf: [fullNameAction,fontAction])


                            if (isLastNameAvailable) {
                                menuChildren.insert(lastNameAction, at: 1)
                            }
                            
                        }else if (self.signField.subType == .lastName) {
                            menuChildren.append(contentsOf: [fullNameAction,firstNameAction,fontAction])
                        }else{
                            menuChildren.append(contentsOf: [firstNameAction,fontAction])

                            if (isLastNameAvailable) {
                                menuChildren.insert(lastNameAction, at: 1)
                            }
                        }
                        break;
                    case .signDate:
                        menuChildren.append(contentsOf: [fontAction,dateAction])
                        break;
                    case .date:
                        menuChildren.append(contentsOf: [fontAction,dateAction,((self.signField.isMandatory) ? optionalAction : mandatoryAction)])
                        break;
                    case .checkBox:
                        if (self.resizeViewMode == .sentDoc_Create) {
                            menuChildren.append(contentsOf:
                                [((self.signField.isCheckboxTicked) ? uncheckAction : checkAction),
                                 ((self.signField.isMandatory) ? optionalAction : mandatoryAction),
                                 ((self.signField.isReadOnly) ? readwriteAction : readonlyAction)])
                        }
                        break;
                    case .textBox:

                        if (self.resizeViewMode == .sentDoc_Create) {
                            menuChildren.append(contentsOf:
                                        [fontAction,
                                         ((self.signField.isMandatory) ? optionalAction : mandatoryAction),
                                         ((self.signField.isReadOnly) ? readwriteAction : readonlyAction)])
                        }else{menuChildren.append(contentsOf:[fontAction])}
                        
                        
                        break;
                    case .dropDown:
                        menuChildren.append(contentsOf:
                                            [editAction,fontAction,
                                             ((self.signField.isMandatory) ? optionalAction : mandatoryAction),
                                             ((self.signField.isReadOnly) ? readwriteAction : readonlyAction)])
                        break;
                    default:
                        menuChildren.append(contentsOf: [fontAction])
                        break;
                }
                if UIFont(name: "Roboto", size: 14) == nil {
                    if let index = menuChildren.index(of: fontAction) {
                        menuChildren.remove(at: index)
                    }
                }
                
                menuChildren.append(deleteAction)
                
                return UIMenu(__title: "", image: nil, identifier: nil, children: menuChildren)
                
        }

    }
    
    #if targetEnvironment(macCatalyst)
    open override var keyCommands: [UIKeyCommand]?{
       let deleteCmd = UIKeyCommand(input: "\u{08}", modifierFlags: [], action: #selector(deleteButtonClicked))
       
       return [deleteCmd]
   }
   #endif
    
}
