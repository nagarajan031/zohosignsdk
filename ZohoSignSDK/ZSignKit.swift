//
//  ZSignKit.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 07/11/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation
import SSOKit

public enum SignSDKType{
    case localZoho
    case live
    case pre
    case CSEZ
}

public class ZSignKit {
    public static let shared = ZSignKit()
    
    public var signKitType : SignSDKType = .live
    public var userAgent = ""
    var apiKey : String?
    
    public weak var superVC : UIViewController?
    var selectedDocument :  ZFileItem?
    
    public var isAppExtension  = false
    public var isDynamicFontSupportNeeded : Bool = false
    
    var personalDetailController  : ZSPersonalDetailController?
    
    var requestDatamanager : SentDocumentsRequestManager?
    var userDatamanager  :  UserRequestManager?
    var fileDatamanager : FileRequestManager?
    
    public var OAuthTokenExpirationHandler : (() -> Void)?
    public var displayExceptionHandler : ((_ exception : String) -> Void)?
    
    public var trackErrorHandler : ((Error?) -> Void)?
    public var trackEventHandler : ((String, [AnyHashable : Any]?) -> Void)?
    public var trackExtnHandler : ((Bool) -> Void)?
    
    
    public typealias ZSignKitProfileDetailsBlock = (Bool, UserDetails?) -> Void
    public typealias ZSignKitSigningSuccessBlock = (ZFileItem) -> Void
    public typealias ZSignKitFailureBlock = (Error?) -> Void
    public typealias ZSignKitDocumentDownloadSuccessBlock = (Data) -> Void
    public typealias ZSignKitDocumentDownloadProgressBlock = (Progress?) -> Void
    
    var profileDetailsSuccessBlk : ZSignKitProfileDetailsBlock?
    var signingSuccessBlock : ZSignKitSigningSuccessBlock?
    var dataFailureBlock : ZSignKitFailureBlock?
    var downloadSuccessBlock : ZSignKitDocumentDownloadSuccessBlock?
    var downloadProgressBlock : ZSignKitDocumentDownloadProgressBlock?
    
    
    
    public class func initate(withType type : SignSDKType, isAppExtension : Bool = false){
        shared.signKitType = type
        shared.isAppExtension = isAppExtension
        
        let userAgent_string = String(format: "Zoho Sign/%@(%@)/%@(%@)", Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String, Bundle.main.infoDictionary?["CFBundleVersion"] as! String, UIDevice.current.model, UIDevice.current.systemVersion)
        shared.userAgent = userAgent_string
    }
    
    func getCurrentLoggedInUser() -> UserDetails? {
        return UserManager.shared.currentUser
    }
    
    func clearLoggedInUserDetails(){
        if UserManager.shared.isUserExists{
            UserManager.shared.deleteCurrentUser()
        }
    }
    
    func createNewUser() -> Bool {
        if !ZSSOKit.isUserSignedIn() {return false}
        if !UserManager.shared.isUserExists {
            let domain = ZIAMManager.loggedInUserBaseDomain()
            UserManager.shared.createSignUser(withBaseDomain: domain)
        }
        return true
    }
    
    public func signDocument(filePath: String, rootViewController rootVc: UIViewController, success successBlock: @escaping ZSignKitSigningSuccessBlock, failure failureBlock: @escaping ZSignKitFailureBlock) {
        if !createNewUser() {
            failureBlock(nil)
            return
        }
        
        superVC = rootVc
        ZSProgressHUD.show(inView: superVC!.view, status: "sign.mobile.activityIndicator.preparingDocument".ZSLString)
        
        selectedDocument = nil
        
        selectedDocument = ZFileItem()
        selectedDocument?.filePath = filePath
        selectedDocument?.fileUrl = URL(fileURLWithPath: filePath ?? "")
        selectedDocument?.fileName = selectedDocument?.fileUrl?.lastPathComponent
        
        signingSuccessBlock = successBlock
        dataFailureBlock = failureBlock
        
        
        getloggedinZSignUser(successBlk: {[weak self] (isUserAvailableinZohoSign, user) in
            if !isUserAvailableinZohoSign {
                self?.showAccountCreationPage(from: self?.superVC, successBlock: nil)
            } else {
                self?.uploadDocumentToServer()
            }
            }, failureBlk: failureBlock)
        
    }
    
    fileprivate func getloggedinZSignUser(successBlk:@escaping ZSignKitProfileDetailsBlock, failureBlk: @escaping ZSignKitFailureBlock) {
        if !createNewUser() {
            failureBlk(nil)
            return
        }
        
        if userDatamanager == nil{
            userDatamanager = UserRequestManager()
        }
        dataFailureBlock = failureBlk
        
        
        userDatamanager?.getCurrentUserDetails(success: { responseData in
            guard let responseData = responseData as? [String : AnyObject], responseData["account_id"] == nil else{
                return successBlk(false, nil)
            }
            
            UserManager.shared.updateCurrentUser(dict: responseData)
            successBlk(true, UserManager.shared.currentUser)
            
        }, failure: { error in
            self.dataManagerErrorhandling(error)
        })
    }
    
    fileprivate func uploadDocumentToServer() {
        
        guard let selectedDocument = selectedDocument else{
            dataFailureBlock?(ZSError.getDataParsingError())
            return
        }
        
        if fileDatamanager == nil {
            fileDatamanager = FileRequestManager()
        }
        ZInfo("fileUrl : %@", selectedDocument.fileUrl)
        
        weak var weakSelf = self
        
        fileDatamanager?.uploadFile(fileItem: selectedDocument, progress: { progress in
            
        }, success: { resposnseData in
            weakSelf?.documentUploadSuccessHandling(resposnseData)
            
        }, failure: { error in
            weakSelf?.dataManagerErrorhandling(error)
        })
        
    }
    
    func documentUploadSuccessHandling(_ resposnseData: Any?) {
        guard let resposnseDict = resposnseData as? [AnyHashable : Any], let idDict = resposnseDict["document_ids"] as? [AnyHashable : Any],let tempDict =  idDict.first as? [AnyHashable : Any] else {
            dataManagerErrorhandling(ZSError.getDataParsingError())
            return
        }
        
        selectedDocument?.fileID = tempDict[keyPath:"document_id"]
        selectedDocument?.createdTime = tempDict[keyPath:"created_time"]
        uploadTempRequest()
    }
    
    func documentSignedDocument(_ docID: String, progress progressBlock: @escaping ZSignKitDocumentDownloadProgressBlock, success successBlock:@escaping ZSignKitDocumentDownloadSuccessBlock, failure failureBlock:@escaping ZSignKitFailureBlock) {
        if fileDatamanager == nil {
            fileDatamanager = FileRequestManager()
        }
        fileDatamanager?.downloadFile(fileID: docID, returnAsPDF: true, progress: { progress in
            progressBlock(progress)
        }, success: { data in
            guard let data = data as? Data else {
                failureBlock(ZSError.getDataParsingError())
                return
            }
            successBlock(data)
            
        }, failure: { error in
            
            var err: Error? = nil
            if let localizedDescription = error?.localizedDescription {
                err = NSError(domain: NSURLErrorDomain, code: error?.errorCode ?? 0, userInfo: [
                    NSLocalizedDescriptionKey: localizedDescription
                ])
            }
            failureBlock(err)
        })
    }
    
    func getPersonalDetailsController(withUpgradeHandler callback: @escaping () -> Void) -> UIViewController? {
        if !createNewUser() {
            return nil
        }
        
        if personalDetailController == nil {
            personalDetailController = ZSPersonalDetailController(style: UITableView.Style.grouped)
        } else {
            personalDetailController?.refreshListData()
        }
        personalDetailController?.upgradeButtonTapHandler = callback
        personalDetailController?.isThirdPartySDK = true
        return personalDetailController
    }
    
    func showAccountCreationPage(from rootVC: UIViewController?, successBlock callback: (() -> Void)?) {
        
        if !createNewUser() {
            return
        }
        
        let create = AccountCreationViewController()
        if (selectedDocument != nil) {
            create.proceedButtonTapHandler = {
                self.uploadDocumentToServer()
            }
        }
        if callback != nil {
            create.proceedButtonTapHandler = callback
        }
        let nav = UINavigationController(rootViewController: create)
        nav.navigationBar.shadowImage = UIImage()
        nav.navigationBar.isTranslucent = true
        if DeviceType.isIpad {
            nav.modalPresentationStyle = .formSheet
        } else {
            nav.modalPresentationStyle = .fullScreen
        }
        rootVC?.present(nav, animated: true)
        
    }
    
    
    
    fileprivate func uploadTempRequest(){
        var requestDict: [String : Any] = [:]
        requestDict["request_name"] = selectedDocument?.trimmedFileName
        requestDict["document_ids"] = [[
            "document_id": selectedDocument?.fileID,
            "document_order": NSNumber(value: 0)
            ]]
        requestDict["self_sign"] = "true"
        if requestDatamanager == nil {
            requestDatamanager = SentDocumentsRequestManager()
        }
        
        weak var weakself = self
        requestDatamanager?.createNewRequest(params: requestDict, success: { responseObj in
            if let responseObj = responseObj as?  [AnyHashable : Any]{
                weakself?.createRequestSuccessHandling(responseObj)
            }
        }, failure: { error in
            weakself?.dataManagerErrorhandling(error)
        })
    }
    
    
    //MARK: - DataManager Delegate
    func dataManagerErrorhandling(_ error: ZSError?) {
        ZSProgressHUD.dismiss()
        
        superVC?.showAlert(withMessage: error?.localizedDescription ?? "")
        superVC = nil
        
        dataFailureBlock?(error)
    }
    
    func createRequestSuccessHandling(_ dict: [AnyHashable : Any]) {
        ZSProgressHUD.dismiss()
        
        guard let currentReq = ZSCoreDataManager.shared.addDraftSignRequest(fromDict: dict) else {
            dataFailureBlock?(ZSError.getDataParsingError())
            return
        }
        
        if DeviceType.isIpad {
            
            let vc: iPadDocumentEditor = iPadDocumentEditor.init(request: currentReq)
            vc.isThirdPartySignSDK = true
            vc.isFromCreatePage = true
            let nav = UINavigationController(rootViewController: vc)
            nav.navigationBar.isTranslucent = false
            nav.modalPresentationStyle = .fullScreen
            superVC?.present(nav, animated: true)
            
        } else {
            let vc = IPhoneDocumentEditor()
            vc.refRequest = currentReq
            vc.isFromCreatePage = true
            vc.createReqType = ZSDocumentCreatorType.SIGN_AND_SEND
            vc.isThirdPartySignSDK = true
            
            let nav = UINavigationController(rootViewController: vc)
            nav.navigationBar.shadowImage = UIImage()
            nav.navigationBar.isTranslucent = true
            superVC?.present(nav, animated: true)
        }
        
    }
    
    public func zSignSuccessHandler(withDocID docId : String)
    {
        guard let selectedDocument = selectedDocument else {
            return
        }
        selectedDocument.fileID = docId
        signingSuccessBlock?(selectedDocument)
    }

    public func zSignFailureHandler() {
        dataFailureBlock?(nil)
    }
    
    //MARK: - Ananlytics Handling
    public func trackEvent(_ eventName: String,withProperties properties: [AnyHashable : Any]? = nil ) {
        trackEventHandler?(eventName, properties)
    }
    
    
    public func trackError(_ error: Error?) {
        trackErrorHandler?(error)
    }
    
    public func trackExtnSession(isStopped stopped: Bool) {
        trackExtnHandler?(stopped)
    }
    
    public func zSignHandleOAuthExpiration() {
        OAuthTokenExpirationHandler?()
    }
    
    func setNightMode(_ nightMode: Bool) {
        ZColor.changeTheme(isNightMode: nightMode)
        personalDetailController = nil
    }

}
