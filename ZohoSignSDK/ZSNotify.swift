//
//  ZSNotify.swift
//  ZohoSign
//
//  Created by Haridoss R on 29/12/16.
//  Copyright © 2016 Zoho Corporation. All rights reserved.
//

import UIKit

public enum ZSNotifyType
{
    case success
    case error
    case warning
    case info
    case infoWithLoader
}


public protocol ZSNotifyDelegate {
    func didNotificationPressed()
}


public class ZSNotify: NSObject {
    
    //MARK: In View Notification
    
    // color
    
    let NOTIFY_SUCCESS_COLOR       =      ZColor.greenColor
    let NOTIFY_ERROR_COLOR         =      ZColor.redColor
    let NOTIFY_WARNING_COLOR       =       UIColor.orange;
    let NOTIFY_INFO_COLOR          =       ZColor.bgColorDark
    
    public static let sharedNotify = ZSNotify()
    
    
    var tapToDismiss: Bool!         =   true
    var delayTimer: TimeInterval!   =   1.0
    var bannerType: ZSNotifyType!   =   .success;
    var isTopBanner = true
    var paddingY  : CGFloat  = 0
    var maskView:UIView?
    
    fileprivate var bannerView: UIView?
    fileprivate var tapGesture: UITapGestureRecognizer?
    fileprivate var viewTimer:Timer?
    
    public var delegate: ZSNotifyDelegate?
    
    var isTappedForClose:Bool = false;
    
    
    public func showError(msg:String! ,inView: UIView, fromYPosition: CGFloat, isDirectionTop: Bool) {
        showNotify(inView: inView, notifyType: .error, fromTop: isDirectionTop, fromHeight: fromYPosition, title: nil , subtitle: msg, isIcon: false, iconImage: nil)
    }
    
    public func showInfo(msg:String! ,inView: UIView, fromYPosition: CGFloat, isDirectionTop: Bool)
    {
        showNotify(inView: inView,
                   notifyType: .info,
                   fromTop: isDirectionTop,
                   fromHeight: fromYPosition,
                   title: nil,
                   subtitle: msg,
                   isIcon: false,
                   iconImage: nil
        )
    }
    
    public func showInfoWithLoader(msg:String! ,inView: UIView, fromYPosition: CGFloat, isDirectionTop: Bool)
    {
        showNotify(inView: inView,
                   notifyType: .infoWithLoader,
                   fromTop: isDirectionTop,
                   fromHeight: fromYPosition,
                   title: nil,
                   subtitle: msg,
                   isIcon: false,
                   iconImage: nil
        )
    }
    
    public func showNotificationInfo(title: String!, subtitle: String!, iconImage:UIImage!, inView view: UIView? = nil)
    {
        if  let statusBarWindow     =  view ?? UIApplication.shared.keyWindow?.rootViewController?.view{
            let isIconExists:Bool   =   (iconImage == nil) ? false : true;
            
            showNotify(inView: statusBarWindow,
                       notifyType: .info,
                       fromTop: true,
                       fromHeight: 0,
                       title: title,
                       subtitle: subtitle,
                       isIcon: isIconExists,
                       iconImage: iconImage
            )
        }
    }
    
    public func showNotify(inView: UIView, notifyType: ZSNotifyType,fromTop : Bool, fromHeight : CGFloat, title: String!, subtitle:String!, isIcon:Bool, iconImage:UIImage!)
    {
        dismissStatusBarNotify()
        dismissNotify()
        // remove previous instances
        
        isTopBanner  = fromTop
        
        bannerView?.removeFromSuperview()
        maskView?.removeFromSuperview()

        maskView = nil
        bannerView = nil
        tapGesture = nil
        viewTimer?.invalidate()
        barTimer?.invalidate()
        
        // set background color
        var bannerBackgroundColor: UIColor! = NOTIFY_INFO_COLOR

        switch notifyType
        {
            case .success:                  bannerBackgroundColor   =   NOTIFY_SUCCESS_COLOR;
            case .error:                    bannerBackgroundColor   =   NOTIFY_ERROR_COLOR;
            case .warning:                  bannerBackgroundColor   =   NOTIFY_WARNING_COLOR;
            case .info:                     bannerBackgroundColor   =   NOTIFY_INFO_COLOR;
            case .infoWithLoader:           bannerBackgroundColor   =   NOTIFY_INFO_COLOR;
        }
        
        self.tapToDismiss   =   true;
        
        bannerView                          =   UIView()
        bannerView?.autoresizingMask        =   .flexibleWidth
        bannerView?.backgroundColor         =   bannerBackgroundColor
        
        let _defScreen:CGRect           =   inView.frame;
        
        //icon
        
        var title_x:CGFloat                  =   15.0;
        var title_width:CGFloat              =   _defScreen.size.width - 30;
        
        if(isIcon || (notifyType == .infoWithLoader))
        {
            if isIcon {
                let displayIcon                 =   UIImageView();
                displayIcon.frame               =   CGRect(x: 15, y: 15, width: 30, height: 30);
                displayIcon.backgroundColor     =   UIColor.clear;
                displayIcon.image               =   iconImage;
                bannerView?.addSubview(displayIcon);
                
                title_x  =   60;
                title_width = _defScreen.size.width - 75
            }
            else{
                let activityIndicator           =   UIActivityIndicatorView(style: .white);
                activityIndicator.frame               =   CGRect(x: 13, y: 8, width: 28, height: 30);
                activityIndicator.transform = CGAffineTransform(scaleX: 0.9, y: 0.9);

                bannerView?.addSubview(activityIndicator);
                activityIndicator .startAnimating()
                
                title_x  =   48;
                title_width = _defScreen.size.width - 65
            }
        }
        
        // title
        
        let titleLabel                  =   UILabel();
        titleLabel.frame                =   CGRect(x: title_x, y: 15, width: title_width, height: 17);
        titleLabel.text                 =   title;
        titleLabel.textColor            =   ZColor.whiteColor
//        titleLabel.autoresizingMask     =    [.flexibleWidth , .flexibleTopMargin]
        titleLabel.backgroundColor      =   UIColor.clear
        titleLabel.font                 =   ZSFont.boldFont(withSize: 16)
        bannerView?.addSubview(titleLabel)
        
        // description
        
        let descLabel                  =   UILabel();
        descLabel.frame                =    CGRect(x: title_x, y: 15, width: (inView.frame.size.width) - title_x - 15, height: 30);
        descLabel.numberOfLines        =   0;
//        descLabel.autoresizingMask     =    [.flexibleWidth , .flexibleTopMargin]
        descLabel.backgroundColor      =   UIColor.clear;
        descLabel.textColor            =   ZColor.whiteColor
        descLabel.text                 =   subtitle;
        descLabel.font                 =   ZSFont.bodyFont()
        descLabel.autoresize()
        
        bannerView?.addSubview(descLabel);
        if (notifyType != .infoWithLoader) {
            addTapToDismissGesture()
        }
        
      

        
        var adjustedHeight:CGFloat = 60;
        
        if subtitle != nil && subtitle.count != 0
        {
            adjustedHeight  =    descLabel.frame.size.height + ((title == nil) ?  15 : 38);
        }
        
//        if !isTopBanner {
            adjustedHeight += 13
//        }
        
        maskView                       =   UIView();
        maskView?.frame                 =   CGRect(x: 0, y: fromHeight, width: inView.frame.size.width, height: adjustedHeight);
        maskView?.clipsToBounds         =   true;
//        maskView.autoresizingMask      =   [.flexibleTopMargin,.flexibleWidth];
        inView.addSubview(maskView!);
        bannerView?.autoresizingMask   =    [.flexibleWidth,.flexibleHeight];
        bannerView?.frame              =    maskView!.bounds
        maskView?.addSubview(bannerView!)
//        inView.addSubview(bannerView!)

        
        addConstraints(view: maskView!, superView: inView,fromHeight: fromHeight, adjustedHeight: adjustedHeight, isFromTop: isTopBanner)
        
        let diff  =  abs(((bannerView?.frame.size.height)!+8) - adjustedHeight)
        
        let startingY  =  ((isTopBanner) ?   fromHeight : (fromHeight-maskView!.frame.size.height))

        
        if (diff < 5)  {
            maskView?.frame = CGRect(x: 0, y: startingY, width: maskView!.frame.size.width, height:  maskView!.frame.size.height + 18)
        }
        else{
            maskView?.frame = CGRect(x: 0, y: startingY, width: maskView!.frame.size.width, height:  maskView!.frame.size.height)
        }
//
        self.bannerView?.frame = CGRect(x: 0, y: isTopBanner ? ( -(bannerView?.frame.size.height)!) : (bannerView?.frame.size.height)!, width: (self.bannerView?.frame.size.width)!, height:  (self.bannerView?.frame.size.height)!)

//        self.bannerView?.frame = CGRect(x: 0, y: 0, width: (maskView.frame.size.width), height:  (maskView.frame.size.height))


         if title != nil {
            let titleY = (isTopBanner) ? ((bannerView?.frame.size.height)! - adjustedHeight  + 2) : 13;
            let descY = (isTopBanner) ? ((bannerView?.frame.size.height)! - adjustedHeight  + 24) : 35;
            
            titleLabel.frame                =   CGRect(x: title_x, y: titleY , width: (bannerView?.frame.size.width)! - title_x - 15, height: 17);
            
            descLabel.frame                =   CGRect(x: title_x, y: descY, width: (bannerView?.frame.size.width)! - title_x - 15, height: descLabel.frame.size.height);
        }
        else{
            let descY = (isTopBanner) ? ((bannerView?.frame.size.height)! - adjustedHeight  + 2) : 13;

            descLabel.frame                =   CGRect(x: title_x, y: descY, width: (bannerView?.frame.size.width)! - title_x - 15, height: descLabel.frame.size.height);

        }
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: .curveEaseIn, animations: {

            self.bannerView?.frame = CGRect(x: 0, y: 0, width: (self.bannerView?.frame.size.width)!, height:  (self.bannerView?.frame.size.height)!)

        }, completion: { (Bool) -> Void in


        })
        
       
        
        // dismiss
        
        viewTimer    =   Timer.scheduledTimer(timeInterval: (notifyType == .infoWithLoader) ? 15.0 : 4.0, target: self, selector: #selector(self.dismissNotify), userInfo: nil, repeats: false);
    }
    
    
    func addConstraints(view: UIView, superView:  UIView, fromHeight: CGFloat, adjustedHeight :  CGFloat,  isFromTop : Bool)
    {
        view.translatesAutoresizingMaskIntoConstraints = false
        let trailing = NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: superView, attribute: .trailing, multiplier: 1, constant: 0)
        
        let leading = NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: superView, attribute: .leading, multiplier: 1, constant: 0)
        
        var bottom = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: superView, attribute: .top, multiplier: 1, constant: adjustedHeight + 18)
        
        var top = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: superView, attribute: .top, multiplier: 1, constant: 0)
        
        
        bottom = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: superView.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant: adjustedHeight - 8)

        
        if !isTopBanner {
            bottom = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: superView, attribute: .bottom, multiplier: 1, constant: 0 )
            
            top = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: superView, attribute: .bottom, multiplier: 1, constant: -adjustedHeight)
            
            
            if(fromHeight >= superView.frame.size.height)
            {
                top = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: superView.safeAreaLayoutGuide, attribute: .bottom, multiplier: 1, constant: -adjustedHeight)
            }
        }
        
        superView.addConstraints([trailing,leading,bottom,top])
        superView.layoutIfNeeded()
        superView.updateConstraints()
    }
    
  
    
    @objc public func dismissNotify() {
        
        if self.bannerView != nil {
            let curFrame = self.bannerView?.frame
            
            let endY : CGFloat = isTopBanner ? (-curFrame!.height) : (curFrame!.height)
            
            
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 4, initialSpringVelocity: 10, options: .curveEaseIn, animations: {
                
                self.bannerView?.frame = CGRect(x: 0, y: endY, width: (curFrame?.width)!, height: (curFrame?.height)!)
                
            }, completion: { (cpl) -> Void in
//                if (cpl == true) {
                
                    
                    if (self.isTappedForClose == true)
                    {
                        self.delegate?.didNotificationPressed();
                        self.delegate = nil;
                        self.isTappedForClose = false;
                    }
                    
                    self.bannerView?.removeFromSuperview()
                    self.maskView?.removeFromSuperview()

                    self.maskView?.translatesAutoresizingMaskIntoConstraints = true

                    self.maskView = nil
                    self.bannerView = nil
//                }
            })
        }else{
            self.bannerView?.removeFromSuperview()
            self.maskView?.removeFromSuperview()
            
            self.maskView = nil
            self.bannerView = nil
        }
    }
    
    func addTapToDismissGesture()
    {
        if self.tapGesture != nil {
            self.tapGesture = nil
        }
        if tapToDismiss == true {
            self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(ZSNotify.notifyTapped));
            self.bannerView?.addGestureRecognizer(tapGesture!)
        }
    }
    
    @objc func notifyTapped()
    {
        isTappedForClose = true;
        self.dismissNotify();
    }
    
  
    
    //MARK: Status Bar Notification
    
    
    public func showNoNetworkAlert(_ msg : String = "sign.mobile.alert.noInternetConnection".ZSLString) {
        if ZSignKit.shared.isAppExtension {
            return
        }
     
        let notif = ZNotificationToastView(title: msg)
        notif.showErrorMsg()
        
//        if DeviceType.isIpad {
//            let notif = ZNotificationToastView(title: msg ?? "sign.mobile.alert.noInternetConnection".ZSLString)
//            notif.showSuccessMsg()
//        }
//        else{
//            showStatusBarNotify(title: "sign.mobile.alert.noInternetConnection".ZSLString)
//        }
    }
    
    var statusBarImgView: UIImageView?
    fileprivate var offlineLabelView:UIView?
    fileprivate var barTimer:Timer?

    private var statusBarHeight = 20
    
    func showStatusBarNotify(title:String){
        

        dismissNotify()
        
        barTimer?.invalidate();
        viewTimer?.invalidate()

        statusBarImgView?.removeFromSuperview()
        offlineLabelView?.removeFromSuperview();
        offlineLabelView = nil;
        statusBarImgView = nil;
        
        guard let statusBarWindow     =   UIApplication.shared.keyWindow else {
            return
        }
        
//        let statusBarWindow     =   UIApplication.shared.value(forKey:"statusBarWindow") as! UIView
        statusBarWindow.clipsToBounds   =   true;
        
        
        let maskLabel                   =   UIView();
        maskLabel.frame                 =   CGRect(x: 0, y: 0, width: statusBarWindow.frame.size.width, height: 20);
//        maskLabel.backgroundColor   =   UIColor.red
        maskLabel.clipsToBounds         =   true;
        statusBarWindow.addSubview(maskLabel);
        
        
        maskLabel.translatesAutoresizingMaskIntoConstraints = false
        let trailing = NSLayoutConstraint(item: maskLabel, attribute: .trailing, relatedBy: .equal, toItem: statusBarWindow, attribute: .trailing, multiplier: 1, constant: 0)
        
        let leading = NSLayoutConstraint(item: maskLabel, attribute: .leading, relatedBy: .equal, toItem: statusBarWindow, attribute: .leading, multiplier: 1, constant: 0)
        
        var bottom = NSLayoutConstraint(item: maskLabel, attribute: .bottom, relatedBy: .equal, toItem: statusBarWindow, attribute: .top, multiplier: 1, constant: 20)
        
        let top = NSLayoutConstraint(item: maskLabel, attribute: .top, relatedBy: .equal, toItem: statusBarWindow, attribute: .top, multiplier: 1, constant: 0)
        
        
        bottom = NSLayoutConstraint(item: maskLabel, attribute: .bottom, relatedBy: .equal, toItem: statusBarWindow.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant: 0)

        statusBarWindow.addConstraints([trailing,leading,bottom,top])
        statusBarWindow.layoutIfNeeded()
        statusBarWindow.updateConstraints()
        if(maskLabel.frame.size.height < 20)
        {
            maskLabel.frame = CGRect(x: 0, y: 0, width: statusBarWindow.frame.size.width, height: 20)
        }
        
        statusBarImgView                   =   UIImageView();
        statusBarImgView?.frame             =   CGRect(x: 0, y: 0, width: maskLabel.frame.size.width, height: maskLabel.frame.size.height)
        statusBarImgView?.autoresizingMask  =   [.flexibleWidth,.flexibleHeight]
        statusBarImgView?.image             =   screenShotMethod(view: statusBarWindow)
        statusBarImgView?.clipsToBounds     =   true;
        statusBarImgView?.backgroundColor   =   UIColor.clear;// ZColor.whiteColor
        maskLabel.addSubview(statusBarImgView!);
        
        
        offlineLabelView                 =   UIView();
        offlineLabelView?.frame              =   CGRect(x: 0, y: -maskLabel.frame.size.height, width: maskLabel.frame.size.width, height: maskLabel.frame.size.height)
        offlineLabelView?.autoresizingMask  =   [.flexibleWidth,.flexibleHeight]
        offlineLabelView?.backgroundColor    =  UIColor(white: 0.1, alpha: 1.0)
        maskLabel.addSubview(offlineLabelView!);
        
        let offlineLabel                    =   UILabel();
        offlineLabel.frame              =   CGRect(x: 0, y: (offlineLabelView?.frame.size.height)! - 18, width: maskLabel.frame.size.width, height: 18)
        offlineLabel.autoresizingMask  =   [.flexibleWidth,.flexibleTopMargin]
        offlineLabel.textAlignment      =   NSTextAlignment.center;
        offlineLabel.font               =   ZSFont.mediumFont(withSize: 13)
        offlineLabel.textColor          =   ZColor.whiteColor;
        offlineLabel.text               =   title
        offlineLabelView?.addSubview(offlineLabel);
        
       
        
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            
            self.statusBarImgView?.frame             =   CGRect(x: 0, y: maskLabel.frame.size.height, width: maskLabel.frame.size.width, height: 0);
            self.offlineLabelView?.frame     =   CGRect(x: 0, y: 0, width: maskLabel.frame.size.width, height: maskLabel.frame.size.height);
        })
        
        
        
        barTimer    =   Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.dismissStatusBarNotify), userInfo: nil, repeats: false);
    }
    
    @objc func dismissStatusBarNotify() {
        
        if  (self.statusBarImgView == nil  ||  self.offlineLabelView == nil)  {return}
        
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 4, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.offlineLabelView?.frame  =   CGRect(x:0, y:-20, width:(self.offlineLabelView?.frame.size.width)!, height:20);
            self.statusBarImgView?.frame             =   CGRect(x: 0, y: 0, width: (self.statusBarImgView?.frame.size.width)!, height: (self.statusBarImgView?.superview?.frame.size.height)!);
            
        }, completion: { (Bool) -> Void in
            
            if  (self.statusBarImgView == nil  ||  self.offlineLabelView == nil)  {return}
            
            
            self.statusBarImgView?.removeFromSuperview()
            self.offlineLabelView?.superview?.removeFromSuperview()
            self.offlineLabelView?.removeFromSuperview();
            self.offlineLabelView = nil;
            self.statusBarImgView = nil;
        })
    }

    func screenShotMethod(view: UIView) -> UIImage {
        //Create the UIImage
        UIGraphicsBeginImageContextWithOptions(CGSize(width: view.frame.size.width, height: (statusBarImgView?.frame.size.height)!), false, UIScreen.main.scale)
        //        UIGraphicsBeginImageContext(CGSize(width: view.frame.size.width, height: 20) , fas)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}
