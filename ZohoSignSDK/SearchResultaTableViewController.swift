//
//  SearchResultaTableViewController.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 15/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit
 
open class SearchResultsTableViewController: UITableViewController {
    
    open lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "sign.mobile.common.search".ZSLString
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.searchBarStyle = .prominent
        self.definesPresentationContext = true
        return searchController
    }()
    
    open var isSearching: Bool {
        return searchController.isActive && !(searchController.searchBar.text?.isEmpty ?? true)
    }
    
    public override init(style: UITableView.Style) {
        super.init(style: style)
    }
    
    open override func viewDidLoad() {
        tableView.backgroundColor                   =   ZColor.bgColorGray
        tableView.setSeparatorStyle()
        tableView.tableFooterView = UIView()
        tableView.showsVerticalScrollIndicator      =    false
        tableView.alwaysBounceVertical = true
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationController?.view.backgroundColor = ZColor.bgColorGray
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SearchResultsTableViewController: UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
    open func updateSearchResults(for searchController: UISearchController) {
        
    }
    
}
