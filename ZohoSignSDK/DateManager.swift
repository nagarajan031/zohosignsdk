//
//  FontManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 14/11/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public let DATE_ONLY_FORMAT_DATE_FIRST          =   "dd MMM yyyy"
public let TWELVEHOUR_FORMAT_WITHOUT_MERIDIAN   =   "dd MMM yyyy, HH:mm"
public let TWENTYFOUR_FORMAT_WITHSECONDS        =   "dd MMM yyyy, HH:mm:ss"
public let TWELVEHOUR_FORMAT_WITH_MERIDIAN      =   "dd MMM yyyy, hh:mm a"
public let DATE_ONLY_FORMAT_MONTH_FIRST         =   "MMM dd, yyyy"
public let DATE_MONTH_ONLY_FORMAT               =   "dd MMM"
public let TIME_ONLY_WITHOUT_SEC_FORMAT         =   "hh:mm a"

public class DateManager {
    static let globalDateFormatter = DateFormatter()
    static func availableDateFormats() -> [String]{
        return ["MMM dd yyyy HH:mm:ss",
                "MMM dd yyyy HH:mm z",
                "dd MMM yyyy",
                "MMM dd yyyy",
                "dd MMMM yyyy",
                "MMMM dd yyyy",
                "dd/MM/yyyy",
                "MM/dd/yyyy",
                "dd/MM/yy",
                "MM/dd/yy",
                "MM.dd.yyyy",
                "MM.dd.yy",
                "dd.MM.yyyy",
                "dd.MM.yy",
                "MMM dd,yyyy",
                "dd MMMM,yyyy",
                "dd-MMMM-yy",
                "dd MMM yyyy",
                "MMM dd yyyy"]
    }
    
    static func customDateFormats() -> [String]{
        return ["MMM dd yyyy HH:mm z",
                "MMM dd yyyy HH:mm:ss",
                "dd MMM yyyy",
                "MMM dd yyyy",
                "dd MMMM yyyy",
                "MMMM dd yyyy",
                "dd/MM/yyyy",
                "MM/dd/yyyy"]
    }
    
    
    public class func convertStringToLong(stringValue: String?, dateFormat format: String?) -> String {
        guard let stringValue = stringValue else {
            return "0"
        }
        
        globalDateFormatter.dateFormat = format

        let dateCon = globalDateFormatter.date(from: stringValue)

        let timeInterval = dateCon?.timeIntervalSince1970 ?? 0.0
        let number = NSNumber(value: Int64(timeInterval * 1000))
        let longValue = number.stringValue

        return longValue
    }

    public class func convertLongToDateString(longValue: String?, dateFormat format: String?) -> String? {
        guard let longValue = longValue else {
            return nil
        }

        globalDateFormatter.dateFormat = format

        let timeInterval = TimeInterval((Int64(longValue) ?? 1) / 1000)

        let stringValue = globalDateFormatter.string(from: Date(timeIntervalSince1970: (timeInterval)))

        return stringValue
    }
    
    //MARK: - Date Formatter
    class func formatTableviewHeader(_ inputTime: NSNumber?, setTodayasNil isTodayNil: Bool) -> NSNumber? {
        var headerTime: NSNumber?

        let calendar = Calendar.current
        let date = Date(timeIntervalSince1970: TimeInterval((Int64(inputTime ?? 1) / 1000)))
        let components = calendar.dateComponents([.month, .year], from: date)
         let diffComponents = calendar.dateComponents([.day, .month, .year], from: Date(), to: date)

        if (diffComponents.month == 0) && (diffComponents.year == 0) {
            if diffComponents.day == 0 {
                headerTime = isTodayNil ? nil : NSNumber(value: 9999999999)
            } else if (diffComponents.day ?? 0) <= 6 {
                headerTime = NSNumber(value: 8888888888)
            } else {
                headerTime = NSNumber(value: Double(((components.year ?? 1) * 1000) + (components.month ?? 1)))
            }
        } else {
            headerTime = NSNumber(value: Double(((components.year ?? 1) * 1000) + (components.month ?? 1)))
        }
        return headerTime
    }
    
    public static func getDateString(for time: NSNumber?) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM YYYY"
        let date = Date(timeIntervalSince1970: TimeInterval((time?.int64Value ?? 1) / 1000))
        
        return dateFormatter.string(from: date)
    }

}
