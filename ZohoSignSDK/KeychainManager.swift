//
//  ZSKeychainManager.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 11/12/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

class KeychainManager {
    
    class func save(key: String, data: Data) -> OSStatus {
        let serviceName =  CommonUtils.signAppSharedKeychainGroupID()

        let query = [
            kSecClass as String       : kSecClassGenericPassword as String,
            kSecAttrAccount as String : key,
            kSecValueData as String   : data,
            kSecAttrAccessGroup as String : serviceName] as [String : Any]

        SecItemDelete(query as CFDictionary)

        return SecItemAdd(query as CFDictionary, nil)
    }

    class func load(key: String) -> Data? {
        let serviceName =  CommonUtils.signAppSharedKeychainGroupID()

        let query = [
            kSecClass as String       : kSecClassGenericPassword,
            kSecAttrAccount as String : key,
            kSecReturnData as String  : kCFBooleanTrue!,
            kSecMatchLimit as String  : kSecMatchLimitOne,
            kSecAttrAccessGroup as String : serviceName] as [String : Any]

        var dataTypeRef: AnyObject? = nil

        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)

        if status == noErr {
            return dataTypeRef as! Data?
        } else {
            return nil
        }
    }
    
    class func remove(key: String) -> OSStatus {
        let serviceName =  CommonUtils.signAppSharedKeychainGroupID()

         let query = [
                   kSecClass as String       : kSecClassGenericPassword,
                   kSecAttrAccount as String : key,
                   kSecReturnData as String  : kCFBooleanTrue!,
                   kSecMatchLimit as String  : kSecMatchLimitOne,
                   kSecAttrAccessGroup as String : serviceName] as [String : Any]

        return SecItemDelete(query as CFDictionary)
    }

    class func createUniqueID() -> String {
        let uuid: CFUUID = CFUUIDCreate(nil)
        let cfStr: CFString = CFUUIDCreateString(nil, uuid)

        let swiftString: String = cfStr as String
        return swiftString
    }
}
