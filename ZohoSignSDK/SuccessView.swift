//
//  SuccessView.swift
//  ZohoSign
//
//  Created by somesh-8758 on 07/11/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

/*
import UIKit
import Lottie

public class SuccessView: UIView {

    private var dimmingView: UIView!
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = ZColor.bgColorWhite
        view.layer.cornerRadius = 5
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var successtickMark: LOTAnimationView = {
        let view = LOTAnimationView(name: "success_tick")
        view.loopAnimation = false
        return view
    }()
    
    private lazy var successMessage: UILabel = {
        let view = UILabel()
        view.text = "Signed Successfully"
        view.font = ZSFont.mediumFont(withSize: 16)
        view.textColor = ZColor.primaryTextColor
        view.textAlignment = .center
        return view
    }()
    
    @objc public override init(frame: CGRect) {
        super.init(frame: frame)
        dimmingView = UIView()
        dimmingView.backgroundColor = ZColor.bgColorDullBlack
        addSubviews([dimmingView, containerView])
        containerView.addSubviews([successtickMark, successMessage])
        dimmingView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        containerView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(250)
            make.height.equalTo(120)
        }
        
        successtickMark.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(46)
        }
        
        successMessage.snp.makeConstraints { (make) in
            make.top.equalTo(successtickMark.snp.bottom).offset(16)
            make.centerX.width.equalToSuperview()
        }
    }
    
    override public func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.dimmingView.alpha = 0
        successMessage.text = ""
        containerView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.3, animations: {
            self.dimmingView.alpha = 1
            self.containerView.transform = .identity
        }) { (finished) in
            self.successtickMark.play { (finished) in
                UIView.transition(with: self.successMessage, duration: 0.3, options: [.curveEaseIn, .transitionCrossDissolve], animations: {
                    self.successMessage.text = "Signed Successfully"
                }, completion: nil)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
*/
