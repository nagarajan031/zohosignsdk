//
//  MyRequest+SignerSettings.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 11/02/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import Foundation

public struct SignerSettings : Codable{
    public var allowGuestSuggestionTooltip = true
    public var allowRedirection = false
    public var disableDownloadPdf = false
    public var disableForward = false
    public var disableSendEmail = false
    public var disableSignLater = false

    public init(){
        
    }
    public init( signerSettingDict : [String : AnyHashable]) {
        allowGuestSuggestionTooltip = signerSettingDict[keyPath: "allow_guest_suggestion_tooltip"] ?? true
        allowRedirection = signerSettingDict[keyPath: "allow_redirection"] ?? false
        disableDownloadPdf = signerSettingDict[keyPath: "disable_download_pdf"] ?? false
        disableForward = signerSettingDict[keyPath: "disable_forward"] ?? false
        disableSendEmail = signerSettingDict[keyPath: "disable_send_email"] ?? false
        disableSignLater = signerSettingDict[keyPath: "disable_sign_later"] ?? false
    }
}
