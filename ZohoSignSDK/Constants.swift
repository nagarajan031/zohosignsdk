//
//  Constants.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 05/02/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//


let ZSignAppBundleIDs       =   "ZSignAppBundleIDs"
let ZSSharedGroupID         =    "ZSSharedGroupID"
let ZSMessageExtensionID    =    "ZSMessageExtensionID"
let ZSTodayWidgetID         =    "ZSTodayWidgetID"
let ZSActionExtensionID     =    "ZSActionExtensionID"
let ZSSSOGroupID            =    "ZSSSOGroupID"
let ZSSharedKeychainGroupID =     "ZSSharedKeychainGroupID"


let ZSFileEncryptionKey          =      "ZSFileEncryptionKey"
let UnCachedFileSaveLocation     =      "NonCacheFiles"
let FolderNameTemplates          =      "TemplateFiles"
let FolderNameAllFiles           =      "AllFiles"
public let FolderNameImport      =      "ImportedFiles" //for handling imported files
let FolderNameCompletionCertificates  = "CompletionCertificates"
let FolderNameSentFiles          =      "SentFiles"
let FolderNameReceivedFiles      =      "ReceivedFiles"

let FolderNameCachedReceivedReq  =      "CacheRecievedReq"
let FolderNameCachedSignReq      =      "CacheSignReq"



public let  REQUEST_FILTER_SEARCH               =     "Search"

//------------------------------------------------------//
/*            Sample users                              */
//------------------------------------------------------//
let ZS_DEFAULT_SAMPLE_SIGN_USERNAME         =     "Charles Stone"
let ZS_DEFAULT_GUEST_USERNAME             =     "Guest"

//------------------------------------------------------//
/*            Request  Delivery Type                    */
//------------------------------------------------------//
public let ZS_DeliverType_sms                =   "SMS"
public let ZS_DeliverType_email              =   "EMAIL"
public let ZS_DeliverType_offline            =   "OFFLINE"

//------------------------------------------------------//
/*           Request Action Type                      */
//------------------------------------------------------//

public let ZS_ActionType_Sign                =      "SIGN"
public let ZS_ActionType_View                =      "VIEW"
public let ZS_ActionType_InPerson            =       "INPERSONSIGN"
public let ZS_ActionType_Declined            =       "DECLINED"
public let ZS_ActionType_Approver            =       "APPROVER"

//------------------------------------------------------//
/*           Request Action Status                      */
//------------------------------------------------------//

public let ZS_ActionStatus_noAction            =      "NOACTION"
public let ZS_ActionStatus_unopened            =      "UNOPENED"
public let ZS_ActionStatus_viewed              =      "VIEWED"
public let ZS_ActionStatus_signed              =      "SIGNED"
public let ZS_ActionStatus_declined            =      "DECLINED"
public let ZS_ActionStatus_commented           =      "COMMENTED"
public let ZS_ActionStatus_forwarded           =      "FORWARDED"
public let ZS_ActionStatus_approved            =      "APPROVED"


public let STATUS_ALL                  =    "all"
public let STATUS_EXPIRING             =    "expiring"
public let STATUS_EXPIRED              =    "expired"
public let STATUS_DECLINED             =    "declined"
public let STATUS_ONHOLD               =    "onhold"
public let STATUS_COMPLETED            =    "completed"
public let STATUS_RECALLED             =    "recalled"
public let STATUS_INPROGRESS           =    "inprogress"
public let STATUS_TOBEHOSTED           =    "to_be_hosted"
public let STATUS_HOSTED               =    "hosted"
public let STATUS_DRAFT                =    "draft"
public let STATUS_DELETED              =    "deleted"
public let STATUS_ARCHIVED             =    "archived"
public let STATUS_SHREDED              =    "shredded"

public let STATUS_SIGN_EXPIRING        =    "expiring"
public let STATUS_SIGN_EXPIRED         =    "expired"
public let STATUS_SIGN_FORWARDED       =    "forwarded"
public let STATUS_SIGN_ASSIGNED        =    "assigned"
public let STATUS_SIGN_COMMENTED       =    "commented"
public let STATUS_SIGN_SIGNED          =    "signed"
public let STATUS_SIGN_VIEWED          =    "viewed"
public let STATUS_SIGN_DECLINED        =    "declined"
public let STATUS_SIGN_PENDING         =    "my_pending"
public let STATUS_SIGN_DELETED         =    "deleted"
public let STATUS_SIGN_ARCHIVED        =    "archived"
public let STATUS_SIGN_SHREDED         =    "shredded"
public let STATUS_SIGN_APPROVED        =    "approved"



public let  ENTITY_SIGN_REQUEST                 =    "SignRequest"
public let  ENTITY_MY_REQUEST                   =    "MyRequest"
public let  ENTITY_FILTER                       =    "Filter"
public let  ENTITY_DOCUMENTS                    =    "Documents"
public let  ENTITY_TEMPLATE                     =    "Template"
public let  ENTITY_TEMPLATE_FILTER              =    "TemplateFilter"
public let  ENTITY_ATTACHMNENT                  =    "Attachment"


public let  EMPTY_FOLDER_ID     =   ""
public let  EMPTY_TYPE_ID       =   ""


public let LIST_ROW_COUNT          =     25
public let kNAME_COUNT             =    100
public let kSINGLELINE_MAX_COUNT   =     255
public let kMULTILINE_MAX_COUNT    =    1024


public let  ZS_Regex_ForName  = "^[a-zA-Z0-9\\s\\#\\+\\?\\!,()\\$\\'@%\\.\\-\\:_\\*/\\=\\[\\]&\\P{InBasicLatin}]+$"


//Userdefaults

public let  kUserDefaultkeyFieldType                        =  "fieldTypesArrayNew"

public let  kUserDefaultNotifyModuleOperation               =  "notifyModuleOperation"
public let  kUserDefaultNotifyModuleFilterName              =  "notifyModuleFilterName"
public let  kUserDefaultNotifyModuleSentReqId              =  "notifyModuleSentReqId"
public let  kUserDefaultNotifyModuleReceivedReqId              =  "notifyModuleReceivedReqId"
public let  kUserDefaultHomeSegmentControl = "HomePage Segment Control"


public let  kMyRequestListRefreshNotification       =  "MyRequestListRefreshNotification"
public let  kSignRequestListRefreshNotification     =  "SignRequestListRefreshNotification"
public let  kUserProfileUpdationNotification        =  "UserProfileUpdationNotification"
public let  kTemplateListRefreshNotification      =  "TemplateListRefreshNotification"

public let  kLocalNotificationInpersonSigningComplted        =  "LocalNotificationInpersonSigningComplted"

public let  kNotificationUserInfoSignRequest         =  "UserInfoSignRequest"
public let  kNotificationUserInfoRecievedRequest     =  "UserInfoRecievedRequest"



//Notification Names
// Sign Request

public let  ZS_Notification_Request_Forwarded               =  "RequestForwarded"
public let  ZS_Notification_Request_Declined                =  "RequestDeclined"  // no notif_type
public let  ZS_Notification_Request_Completed               =  "RequestCompleted" // no notif_type
public let  ZS_Notification_Request_Signed                  =  "RequestSigned"    // no notif_type

// My Request

public let  ZS_Notification_Request_SignRequestReceived     =  "NewSignRequestReceived"
public let  ZS_Notification_Request_Reminded                =  "RequestReminded"
public let  ZS_Notification_Request_Recalled                =  "MyRequestRecalled"


public let  ZS_Notification_RequestInpersonSignRequestReceived    =  "NewInpersonSignRequestReceived"
public let  ZS_Notification_RequestSignedInperson   =  "RequestSignedInperson"
public let  ZS_Notification_RequestInpersonAccessFailure   =  "RequestInpersonAccessFailure"
public let  ZS_Notification_RequestRemindedForInpersonSigning  =  "RequestRemindedForInpersonSigning"
public let  ZS_Notification_RequestHosted          =  "RequestHosted"


public let   ZS_Event_ForceUpdate              =   "ForceUpdate"


public enum ZSignFieldType  : String, Codable
{
    case signature
    case initial

    case email
    case name
    case company
    case jobTitle
    case signDate

    case textBox
    case checkBox
    case date
    case dropDown
    case attachment

    case radioGroup
    case others
}


public enum ZSignFieldSubType  : String, Codable
{
    case fullName
    case firstName
    case lastName
}


public enum ZSignFieldSignatureType  : String, Codable
{
    case signature
    case initial
}



public enum ZSAlertType : Int
{
    case SentReq_Created
    case SentReq_Drafted
    case SentReq_Updated
    case SentReq_Mailed
    case SentReq_Deleted
    case SentReq_Restored
    case SentReq_Reminded
    case SentReq_Recalled

    case RecievedReq_Declined
    case RecievedReq_Signed
    case RecievedReq_Assigned
    case RecievedReq_Saved
    
    case Template_Saved

    case Inperson_Signed

    case LocalData_Cleared
    case Loader_Reminding
    case Loader_Mail

    case Template_Deleted
}



public enum ZSDocumentCreatorType
{
    case SIGN_AND_SEND
    case SEND_FOR_SIGNATURE
    case TEMPLATE
}


public enum ZSDocumentViewMode
{
    case sentDoc_Create//Template create too
    case sentDoc_selfSign
    case receivedDoc_view
    case receivedDoc_sign
}


public enum ZResizeViewMode {
    case sentDoc_Create
    case sentDoc_selfSign
    case receivedDoc_preview
    case receivedDoc_pending
    case receivedDoc_completed
}



public enum ZSAPIType
{
//    case GetCurrentUserDetails = 1
//    case GetGuestUserProfile
//
//    case GetAccountsDetail
//    case AddAccount
//
//    case UpdateUserProfile
//
//    case GetSignRequestsListForFilter
//    case GetSignRequestsListForSearch
//    case GetSignRequestDetails
//    case GetSignRequestAudit
//    case ResetForActionID
//    case DownloadCompletionCertificate
//
//    case SearchSentRequests
//    case SearchReceivedRequests
//
//    case GetMyRequestsListForFilter
    case GetMyRequestVerificationCode
    case SendVerificationCodeToMail
    case GetMyRequestStatelessAuthCode
    case GetMyRequestDetails
//    case DeleteMyRequest
//    case RestoreMyRequest
//
//    case GetRequestsCount
//    case GetReportsCount, // unused
//    case GetExpiringSoonCount
//
    case GetRecipientsList
//    case AddRecipient
//    case UpdateRecipient
    case GetHostsList
    case ValidateHost

    case GetRequestTypes
    case AddRequestType
//    case GetFieldTypes
    case GetAllFolders
    case AddNewFolder
//
//    case GetTemplates
    case GetTemplateDetail
    case CreateTemplate
    case UpdateTemplate
//    case DeleteTemplate
//    case RequestFromTemplate
//
//    case AddNewDocument
//    case UploadDocFromCloudServices
//    case DeleteDocument
//    case DownloadDocAsZip
//    case DocumentDownloadAsPDF
//    case DownloadGuestDocAsPDF
    case MailGuestDoc
    
    case DownloadSignRequestDetailPreviewImage
    case CreateNewRequest
    case UpdateRequest
    case SubmitDraftRequest
    case SubmitSelfSignRequest
    
    case RejectSignDocument
//    case ForwardSignDocument
    case SignDocument
//
//    case DeleteRequest
//    case RecallRequest
//    case RestoreRequest
//    case EmailRequest
//    case RemindRequest
//
//    case Feedback
//
//    case GetInstallationID
//    case RegisterForPushNotification
//    case UnregisterForPushNotification
//
//    case GetZohoDocsList
//    case DownloadZohoDoc
//
//    case LegalTermsAccount
//    case LegalTermsGuest
//    case LegalTermsHost
//
//    case GetUserAccountSettings
    
}
