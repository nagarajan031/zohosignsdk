//
//  ZSButtonFieldCell.swift
//  ZohoSign
//
//  Created by Rajdurai on 11/10/17.
//  Copyright © 2017 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZSButtonIconCell: UITableViewCell {
    
    let placeHolderLabel =  UILabel()
    let iconImgView     =   UIImageView()
    let valueLabel      =   UILabel()
    public let separatorLine   =   UIView()
    var addButton       =   ZSButton()

//    var floatingImageView : UIImageView!
//    var titleLabel : UILabel!
//    var popUpButton : ZSButton!
//    var addButton : ZSButton!
//    var separatorLine : UIView!
    
    public var addButtonPressed : (() -> Void)?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        //fatalError("init(coder:) has not been implemented")
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func initialSetup(){
        
        self.contentView.addSubview(iconImgView)
        
        placeHolderLabel.font = ZSFont.createListKeyFontSmall()
        placeHolderLabel.textColor = ZColor.secondaryTextColorDark
//        self.contentView.addSubview(placeHolderLabel)
        
        valueLabel.font = ZSFont.createListValueFont()
        valueLabel.textAlignment = .left
        valueLabel.textColor = ZColor.primaryTextColor
        self.contentView.addSubview(valueLabel)
        
        backgroundColor = ZColor.bgColorWhite

//        popUpButton = ZSButton()
//        popUpButton.backgroundColor = .clear
//        popUpButton.titleLabel?.font = ZSFont.createListValueFont()
//        popUpButton.titleLabel?.textAlignment = .left
//        popUpButton.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        popUpButton.contentHorizontalAlignment = .left
//        popUpButton.addTarget(self, action: #selector(popupBtnPressed), for: UIControlEvents.touchUpInside)
//        popUpButton.setTitleColor(ZColor.primaryTextColor, for: UIControlState.normal)
//        self.contentView.addSubview(popUpButton)
//        popUpButton.contentEdgeInsets = UIEdgeInsets(top: 20, left: 25, bottom: 0, right: 0)
        
        addButton.backgroundColor = .clear
        addButton.autoresizingMask = [.flexibleLeftMargin, .flexibleHeight]
        addButton.setImage(UIImage(named: "plus_icon")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        addButton.tintColor = ZColor.buttonColor
        addButton.addTarget(self, action: #selector(addBtnPressed), for: UIControl.Event.touchUpInside)
        self.contentView.addSubview(addButton)
        addButton.isHidden = true
        
//        separatorLine.backgroundColor = ZColor.seperatorColor
//        self.contentView.addSubview(separatorLine)
        
        
        let selectedView = UIView()
        selectedView.backgroundColor = ZColor.listSelectionColor;
        self.selectedBackgroundView = selectedView;
        
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
//        placeHolderLabel.frame  =  CGRect(x: cellValueLabelX, y: cellPlaceHolderLabelY, width: self.bounds.size.width-30, height: 18)
//        valueLabel.frame = CGRect(x: cellValueLabelX, y: cellValueLabelY, width: self.bounds.size.width - 50, height: 40)
//        separatorLine.frame = CGRect(x: cellValueLabelX, y: self.bounds.size.height - Constants.ZS_SeparatorLine_Height, width: self.bounds.size.width-30, height: Constants.ZS_SeparatorLine_Height)
        iconImgView.frame = cellIconFrame
        
        valueLabel.frame = CGRect(x: cellValueLabelX, y: 0, width: self.bounds.size.width - 50, height: self.bounds.size.height)
    }
    
    
    @objc func addBtnPressed() {
        if (self.addButtonPressed != nil) {
            self.addButtonPressed!()
        }
    }
    
    public func isEditable(_ editable: Bool)  {
        isUserInteractionEnabled = editable
        valueLabel.alpha    =   editable ? 1 : 0.4
        placeHolderLabel.alpha    =   editable ? 1 : 0.4
        addButton.isEnabled     =   editable
        
    }
    
    public func setButtonValue(key placeholder:String, iconName imageName:String?, value valueText:String, addButton isAdd:Bool)
    {
        valueLabel.text = valueText
        if let icon = imageName {
            iconImgView.image = UIImage(named: icon)?.withRenderingMode(.alwaysTemplate)
            iconImgView.tintColor = ZColor.secondaryTextColorDark
        }
        placeHolderLabel.text = placeholder
//        popUpButton.setTitle(valueText, for: UIControlState.normal)
        addButton.isHidden = (isAdd) ? false : true
    }
}
