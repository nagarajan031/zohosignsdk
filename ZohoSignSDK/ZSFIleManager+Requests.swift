//
//  ZSFIleManager+SentRequests.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 31/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import Foundation
import SSZipArchive

extension ZSFileManager{
    
    func getSharedDirectory(withRequestId requestId: String, isSentRequests: Bool) -> String {
        #if targetEnvironment(macCatalyst)
            
            let folder =  createSubFolder(parentFilePath: getSubFolder(type: .cache, name: (isSentRequests ? FolderNameSentFiles : FolderNameReceivedFiles)), subFolder: requestId)

            return folder.folderPath

        #else

            let folder =  createSubFolder(parentFilePath: getSubFolder(type: .shared, name: (isSentRequests ? FolderNameCachedSignReq : FolderNameCachedReceivedReq)), subFolder: requestId)

            return folder.folderPath

        #endif
    }

    
    
    public func isFileAvailableForRequest(requestId: String?, isSentRequests: Bool, isCache: Bool) -> Bool {
        guard let requestId = requestId, isCache  else {
            return false
        }

        let fm = FileManager.default

        
        let folderPath = self.getSharedDirectory(withRequestId: requestId, isSentRequests: isSentRequests)
        let filePath = URL(fileURLWithPath: folderPath).appendingPathComponent("\(requestId ?? "").pdf").path

        if fm.fileExists(atPath: filePath) {
            return true
        }

        let zipFilePath = URL(fileURLWithPath: folderPath).appendingPathComponent("\(requestId ?? "").zip").path

        //    NSString* encryptedFilePath     =   [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.zip",requestId]];

        return fm.fileExists(atPath: zipFilePath)
    }

    
    func saveFileAfterEncryption(forRequestId requestId: String?, isSentRequests: Bool, fileData: Data,  mimeType: String?, saveCache: Bool) -> Bool
    {
        guard let requestId = requestId  else {
            return false
        }
        
        let folderPath  = saveCache ? getSharedDirectory(withRequestId: requestId, isSentRequests: isSentRequests) : getSubFolder(type: .cache, name:UnCachedFileSaveLocation)
        
      
        var fileName =  String(format: "%@.pdf", requestId)
        if (mimeType == "application/zip") {
            fileName =  String(format: "%@.zip", requestId)
        }
        
        let filePath     =  folderPath.appendingFormat("/%@", fileName)
        
        return ((try? fileData.write(to: URL(fileURLWithPath: filePath))) != nil)
    }
    
    
    public func getSavedFilesPathAfterDecryption(forRequestId requestId: String?, isSentRequests: Bool, saveCache: Bool) -> [String]?
    {
        guard let requestId = requestId  else {
            return nil
        }
        
        let folderPath  = saveCache ? getSharedDirectory(withRequestId: requestId, isSentRequests: isSentRequests) : getSubFolder(type: .cache, name:UnCachedFileSaveLocation)

        let filePath     =  folderPath.appendingFormat("/%@.pdf", requestId)

        if (fileManager.fileExists(atPath: filePath)) {
            return [filePath];
        }
        
        //No pdf files saved
        
        let zipFilePath     =  folderPath.appendingFormat("/%@.zip", requestId)
        
        if (!fileManager.fileExists(atPath: zipFilePath)) {
            return nil;
        }
        
        return getFilesFromZipPath(zipFilePath)
    }

    
    fileprivate func getFilesFromZipPath(_ zipFilePath: String?) -> [String]? {
        let fileData = NSData(contentsOfFile: zipFilePath ?? "") as Data?

        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)

        let documentsDirectory = paths[0] // Get documents folder

        let tempZipPath = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("temp.zip").path
        fileManager.createFile(atPath: tempZipPath, contents: fileData, attributes: nil)

        let unzipPath = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("temp").path
        try? fileManager.removeItem(atPath: unzipPath)

        SSZipArchive.unzipFile(atPath: tempZipPath, toDestination: unzipPath, overwrite: true, password: nil, progressHandler: { entry, zipInfo, entryNumber, total in

        }, completionHandler: {[weak self] path, succeeded, error in
            try? self?.fileManager.removeItem(atPath: tempZipPath)
        })

        guard var directoryContents: [String] = try? fileManager.contentsOfDirectory(atPath: unzipPath), directoryContents.count > 0 else{
            return nil
        }
        
            var array: [String] = []
            for path in directoryContents ?? [] {
                array.append(URL(fileURLWithPath: unzipPath).appendingPathComponent(path).path)
            }

        let sortedArrayOfString =  array.sorted()
//        let sortedArrayOfString = array.sort { (obj1, obj2) -> Bool in
//            return obj1.compare(obj2, options: [.caseInsensitive, .numeric], range: nil, locale: .current)
//        }
//            let sortedArrayOfString = (array as NSArray).sortedArray(comparator: { obj1, obj2 in
//                    return ((obj1 as? String)?.compare(obj2 as? String ?? "", options: [.caseInsensitive, .numeric], range: nil, locale: .current))!
//                })

            return sortedArrayOfString

//        return [unzipPath]
    }

    
}
