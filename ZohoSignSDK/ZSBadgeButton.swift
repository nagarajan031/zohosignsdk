//
//  ZSBadgeButton.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 04/02/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZSBadgeButton: UIButton {
    fileprivate let fieldsCountLabel = UILabel()
    //MARK: - inits
   public convenience init() {
        self.init(frame: .zero)
        self.initateSetup()
    }

    func initateSetup() {
        fieldsCountLabel.backgroundColor  =       ZColor.redColor;
        fieldsCountLabel.font             =       ZSFont.mediumFont(withSize: 10);
        fieldsCountLabel.textColor        =       ZColor.whiteColor
        fieldsCountLabel.layer.cornerRadius  =     7.5
        fieldsCountLabel.textAlignment      =   .center;
        fieldsCountLabel.clipsToBounds      =   true;
        fieldsCountLabel.adjustsFontSizeToFitWidth = true
        addSubview(fieldsCountLabel)
        
        fieldsCountLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(8)
            make.top.equalTo(-4)
            make.width.equalTo(28)
            make.height.equalTo(16)
        }
    }

    public func setBadge(_ badge : String){
        fieldsCountLabel.text = badge
    }

}
