//
//  PDFDocument+.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 17/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import Foundation

public extension ZSPDFDocument{
    func thumbnailImage(size : CGSize) -> UIImage?  {
        let pdfPage = page(at: 0)
        return pdfPage?.thumbnail(of: size, for: .artBox)
    }
}

