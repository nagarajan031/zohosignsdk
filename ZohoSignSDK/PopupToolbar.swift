//
//  PopupToolbar.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 23/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

public class PopupToolbar: UIView {

    lazy var titleLabel : UILabel = {
        let label = UILabel()
        label.font = ZSFont.regularFont(withSize: 15)
        label.textColor = ZColor.secondaryTextColorDark
        label.numberOfLines = 0
        return label
    }()
    
    public lazy var primaryActionButton : ZSButton = {
        let btn = ZSButton()
        btn.layer.cornerRadius = 5
        btn.backgroundColor = ZColor.buttonColor
        btn.setTitleColor(ZColor.whiteColor, for: .normal)
        btn.titleLabel?.font = ZSFont.buttonTitleFont()
        return btn
    }()
    
    public lazy var secondaryActionButton : ZSButton = {
        let btn = ZSButton()
        btn.backgroundColor = ZColor.bgColorWhite
        btn.setTitleColor(ZColor.buttonColor, for: .normal)
        btn.titleLabel?.font = ZSFont.mediumFont(withSize: 15)
        return btn
    }()
    
    
    public func initateSetup(){
        backgroundColor = ZColor.bgColorWhite
        
//        let borderView = UIView()
//        borderView.backgroundColor = ZColor.seperatorColor
//        addSubview(borderView)
//        borderView.snp.makeConstraints { (make) in
//            make.leading.trailing.equalToSuperview()
//            make.top.equalToSuperview()
//            make.height.equalTo(0.3)
//        }
        
        addSubview(titleLabel)
    
        if !(titleLabel.text?.isEmpty ?? true){
            
            titleLabel.snp.makeConstraints { (make) in
                make.leading.equalTo(16)
                make.trailing.equalToSuperview().inset(16)
                make.top.equalTo(16)
            }
            
            titleLabel.setNeedsLayout()
            titleLabel.sizeToFit()
        }
                
        
        addSubview(primaryActionButton)
        
        primaryActionButton.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom).offset(16)
            make.height.equalTo(45)
            make.width.equalTo(320).priority(.high)
            make.left.equalTo(22).priority(.high)
            make.right.equalTo(-22).priority(.high)
        }
        
        addSubview(secondaryActionButton)

        if !(secondaryActionButton.titleLabel?.text?.isEmpty ?? true) {
            secondaryActionButton.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.top.equalTo(primaryActionButton.snp.bottom).offset(0)
                make.height.equalTo(40)
                make.width.equalTo(primaryActionButton.snp.width)
            }
        }
        
        updateConstraints()
        layoutIfNeeded()
        
    }
    
    public func getHeight() -> CGFloat {
        return (secondaryActionButton.frame.maxY < 1) ? (primaryActionButton.frame.maxY + 8 ): secondaryActionButton.frame.maxY
    }
    
}
