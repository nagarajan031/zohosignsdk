//
//  SketchView.swift
//  SignatureView
//
//  Created by somesh-8758 on 13/02/20.
//  Copyright © 2020 Somesh-8758. All rights reserved.
//

import UIKit

public protocol SketchViewDelegate: class {
    func willBeginDrawing(_ view: SketchView)
    func didEndDrawing(_ view: SketchView)
    func didPerformedAction(_ action: SketchView.Actions)
    func imageforDrawnSignature(_ image: UIImage)
}

public extension SketchViewDelegate {
    func willBeginDrawing(_ view: SketchView) {}
    func didEndDrawing(_ view: SketchView) {}
}

public class SketchView: UIView {
    var style: Sketch.Style = .pen
    public var lineColor: UIColor
    var lineWidth: CGFloat
    var lineAlpha: CGFloat
    
    private var image: UIImage?
    private var backgroundImage: UIImage?
    
    private var parentScrollView: UIScrollView
    private var toolIndices: [SketchToolIndex]
    
    private var shouldRemovePlayBack: Bool = false
    private var canRedo: Bool {
        return redoArray.count > 0
    }
    private var canUndo: Bool {
        return undoArray.count > 0
    }
    
    private var currentPoint, previousPoint1, previousPoint2: CGPoint
    private var undoArray: [Sketch]
    private var redoArray: [Sketch]
    private var currentTool: SketchTool?
    private var baseImage: UIImage?
    private var totalObjects: Int = 0
    private var lastUndoType: Sketch.UndoType = .clear
    private var playbackCounter: Int = 0
    private var playbackPoints: [CGPoint]
    private var displayLink: CADisplayLink?
    
    public weak var delegate: SketchViewDelegate?
    
    private lazy var placeHolderLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: frame.size.width-30, height: 80))
        label.autoresizingMask = [.flexibleWidth]
        label.text = "sign.mobile.profile.signHere".ZSLString
        label.textColor =  ZColor.seperatorColor
        label.font = ZSFont.boldFont(withSize: 44)
        label.textAlignment = .center
        return label
    }()
    
    init() {
        undoArray = .init()
        redoArray = .init()
        toolIndices = .init()
        lineWidth = SketchDefaults.lineWidth
        lineColor = SketchDefaults.lineColor
        lineAlpha = SketchDefaults.lineAlpha
        parentScrollView = UIScrollView()
        currentPoint = .zero
        previousPoint1 = .zero
        previousPoint2 = .zero
        playbackPoints = .init()
        super.init(frame: .zero)
        configure()
    }
    
    private func configure() {
        addSubview(placeHolderLabel)
        backgroundColor = .clear
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        placeHolderLabel.center = center
    }
    
    override public func draw(_ rect: CGRect) {
        image?.draw(at: .zero)
        currentTool?.draw()
    }
    
    
    private func updateCacheImage(_ shouldRedraw: Bool) {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0.0)
        if shouldRedraw {
            image = nil
            (backgroundImage?.copy() as? UIImage)?.draw(in: bounds)
            updateBaseImage()
        } else {
            image?.draw(at: .zero)
            currentTool?.draw()
        }
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
    private func updateBaseImage() {
        var index = 0
        if let sketchIndex = toolIndices.last {
            index = sketchIndex.index
        }
        if index == 0 && baseImage != nil {
            baseImage?.draw(at: .zero)
        }
        
        for sketch in undoArray {
            switch sketch.type {
            case .clear:
                let context = UIGraphicsGetCurrentContext()
                context?.clear(bounds)
            case .image:
                sketch.image?.draw(at: .zero)
            case .sketchTool:
                sketch.tool?.draw()
            }
        }
    }
    
    private func changeBaseImage() {
        let firstTool = undoArray[0]
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0.0)
        
        if firstTool.type != .clear {
            if let baseImage = baseImage {
                baseImage.draw(at: .zero)
            }
            
            if firstTool.type == .sketchTool {
                firstTool.tool?.draw()
            } else {
                firstTool.image?.draw(at: .zero)
            }
        }
        
        baseImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
    private func finishDrawing() {
        updateCacheImage(false)
        redoArray.removeAll()
        
        delegate?.didEndDrawing(self)
        
        if let pencilTool = currentTool as? PencilTool {
            pencilTool.removePoints()
        }
        
        finishCurrentDrawing()
        currentTool = nil
    }
    
    
    private func finishCurrentDrawing() {
        if totalObjects%SketchDefaults.imageCacheLimit == 0 {
            
            let currentSketchTool = undoArray.last
            currentSketchTool?.image = self.image
            currentSketchTool?.type = .image
            
            let toolIndex = SketchToolIndex(index: undoArray.count-1, undoType: .image)
            toolIndices.append(toolIndex)
            
            lastUndoType = .image
            
        } else {
            lastUndoType = .sketchTool
        }
        
    }
    
    private func removeFirstObjectIfNeeded() {
        if undoArray.count > SketchDefaults.undoLimit {
            changeBaseImage()
            undoArray.remove(at: 0)
            
            if toolIndices.count > 0 {
                let firstObject = toolIndices[0]
                if firstObject.index == 0 {
                    toolIndices.remove(at: 0)
                }
                
                for toolIndex in toolIndices {
                    toolIndex.index -= 1
                }
            }
        }
    }
    
    private func toolWithCurrentSettings() -> SketchTool {
        switch style {
        case .pencil:
            return PencilTool()
        case .pen:
            return PenTool()
        case .brush:
            return BrushTool()
        case .eraser:
            return EraserTool()
        }
    }
    
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        placeHolderLabel.isHidden = true
        
        if shouldRemovePlayBack {
            shouldRemovePlayBack = false
            setNeedsDisplay()
        }
        
        guard let touch = touches.first else {return}
        previousPoint1 = touch.previousLocation(in: self)
        currentPoint = touch.location(in: self)
        
        currentTool = toolWithCurrentSettings()
        currentTool?.lineAlpha = lineAlpha
        currentTool?.lineColor = lineColor
        currentTool?._lineWidth = lineWidth
        
        let sketchObject = Sketch.sketchObject(with: currentTool!)
        undoArray.append(sketchObject)
        
        totalObjects += 1
        
        delegate?.willBeginDrawing(self)
        
        let force: CGFloat = touch.force
        let forcePercentage = (force != 0 && touch.maximumPossibleForce > 0) ? force/touch.maximumPossibleForce : 0
        
        self.currentTool?.touchesBegin(self.currentPoint, with: forcePercentage)
    }
    
    override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        guard let touch = touches.first else {return}
        
        previousPoint2 = previousPoint1
        previousPoint1 = touch.previousLocation(in: self)
        currentPoint = touch.location(in: self)
        
        if let eraserTool = currentTool as? EraserTool {
            var bounds = eraserTool.addPath(previousPreviousPoint: previousPoint2, previousPoint: previousPoint1, currentPoint: currentPoint)
            updateDrawBox(&bounds)
            setNeedsDisplay(bounds)
        } else {
            
            //special case only applicable to pencil tool
            if let pencilTool = currentTool as? PencilTool,
                pencilTool.points.count > 2000 {
                updateCacheImage(false)
                pencilTool.resetPoints()
            }
            
            //applicable to all tools
            let force = touch.force
            let forcePercentage = force != 0 && touch.maximumPossibleForce != 0 ? force/touch.maximumPossibleForce : 0
            if var bounds = currentTool?.touchesMoved(currentPoint, with: forcePercentage) {
                updateDrawBox(&bounds)
                setNeedsDisplay(bounds)
            }
        }
        
    }
    
    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        guard let touch = touches.first else {return}
        let currentPoint = touch.location(in: self)
        
        let force = touch.force
        let forcePencertage = force != 0 && touch.maximumPossibleForce != 0 ? force/touch.maximumPossibleForce : 0
        
        switch currentTool {
        case is BrushTool:
            currentTool?.touchesEnded(currentPoint, with: forcePencertage)
            touchesMoved(touches, with: event)
        case is EraserTool:
            touchesMoved(touches, with: event)
        default:
            if var bounds = currentTool?.touchesEnded(currentPoint, with: forcePencertage) {
                updateDrawBox(&bounds)
                setNeedsDisplay(bounds)
            }

        }
        
        finishDrawing()
    }
    
    override public func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        if parentScrollView.isZooming || parentScrollView.isDragging {
            undoArray.removeLast()
            currentTool = nil
            setNeedsDisplay()
        } else {
            touchesEnded(touches, with: event)
        }
    }
    
    private func updateDrawBox(_ bounds: inout CGRect) {
        bounds.origin.x -= lineWidth * 2.0
        bounds.origin.y -= lineWidth * 2.0
        bounds.size.width += lineWidth * 4.0
        bounds.size.height += lineWidth * 4.0
    }
    
    
    func loadImage(_ image: UIImage) {
        self.image = image
        self.backgroundImage = image.copy() as? UIImage
        redoArray.removeAll()
        undoArray.removeAll()
        updateCacheImage(true)
        setNeedsDisplay()
    }
    
    private func resetTool() {
        currentTool = nil
    }
    
    @objc func clear() {
        delegate?.didPerformedAction(.clear)
        placeHolderLabel.isHidden = false
        
        if lastUndoType != .clear {
            resetTool()
            
            image = nil
            
            let toolIndex = SketchToolIndex.clearTool(at: undoArray.count)
            toolIndices.append(toolIndex)
            
            let sketchObject = Sketch.clear()
            undoArray.append(sketchObject)
            
            removeFirstObjectIfNeeded()
            
            totalObjects += 1
            lastUndoType = .clear
            redoArray.removeAll()
            
            setNeedsDisplay()
            delegate?.didEndDrawing(self)
        }
    }
    
    private func remove() {
        resetTool()
        undoArray.removeAll()
        redoArray.removeAll()
        backgroundImage = nil
        updateCacheImage(true)
        setNeedsDisplay()
    }
    
    @objc func redo() {
        if canRedo {
            resetTool()
            
            let sketch = redoArray.last!
            if sketch.type == .clear {
                let toolIndex = SketchToolIndex.clearTool(at: undoArray.count)
                toolIndices.append(toolIndex)
            } else if sketch.type == .image {
                let toolIndex = SketchToolIndex.imageTool(at: undoArray.count)
                toolIndices.append(toolIndex)
            }
            
            lastUndoType = sketch.type
            
            undoArray.append(sketch)
            redoArray.removeLast()
            updateCacheImage(true)
            setNeedsDisplay()
            delegate?.didPerformedAction(.redo(canRedo))
        }
    }
    
    @objc func undo() {
        if canUndo {
            resetTool()
            let sketch = undoArray.last!
            if sketch.type == .clear || sketch.type == .image {
                toolIndices.removeLast()
            }
            
            redoArray.append(sketch)
            undoArray.removeLast()
            
            if let lastSketch = undoArray.last {
                lastUndoType = lastSketch.type
            }
            
            updateCacheImage(true)
            setNeedsDisplay()
            delegate?.didPerformedAction(.undo(canUndo))
        }
    }
    
    private func playBack() {
        perform(#selector(startDisplayLink), with: nil, afterDelay: 0.2)
        shouldRemovePlayBack = true
    }
    
    private func playBackPoints() -> [CGPoint]{
        let pointDistance: CGFloat = 50
        let totalWidth = bounds.size.width - 20
        let numberOfPoints = totalWidth/pointDistance
        
        let sketchWidth = numberOfPoints * pointDistance
        let pointOrigin = (bounds.size.width - sketchWidth) / 2
        
        let centerY = bounds.size.height/2
        var points: [CGPoint] = []
        
        
        for i in 0...Int(numberOfPoints) {
            let x,y: CGFloat
            if i%2 == 0 {
                x = pointOrigin + (CGFloat(i) * pointDistance)
                y = centerY + 30
            } else {
                x = pointOrigin + (CGFloat(i) * pointDistance)
                y = centerY - 30
            }
            points.append(CGPoint(x: x, y: y))
        }
        
        return points
    }
    
    @objc private func startDisplayLink() {
        currentTool = toolWithCurrentSettings()
        currentTool?._lineWidth = lineWidth
        currentTool?.lineColor = lineColor
        currentTool?.lineAlpha = lineAlpha
        
        playbackCounter = 0
        playbackPoints = BezierTool.bezierSmooth(playbackPoints)
        
        displayLink = CADisplayLink(target: self, selector: #selector(onDisplaLink))
        displayLink?.add(to: RunLoop.current, forMode: .default)
    }
    
    @objc private func onDisplaLink() {
        guard playbackCounter < playbackPoints.count else {
            stopDisplayLink()
            return
        }
        
        let currentPoint = playbackPoints[playbackCounter]
        
        if playbackCounter == 0 {
            onDisplayLinkBegin(from: currentPoint)
        } else {
            onDisplayLinkMove(to: currentPoint)
        }
        
        setNeedsDisplay()
        playbackCounter += 1
    }
    
    private func onDisplayLinkBegin(from point: CGPoint) {
        if !(currentTool is EraserTool) {
            currentTool?.touchesBegin(point, with: 0.0)
        }
    }
    
    private func onDisplayLinkMove(to point: CGPoint) {
        if !(currentTool is EraserTool) {
            currentTool?.touchesMoved(point, with: 0.0)
        }
    }
    
    private func stopDisplayLink() {
        playbackCounter = 0
        playbackPoints.removeAll()
        displayLink?.invalidate()
        displayLink = nil
    }
    
    private func convertViewToImage() -> UIImage? {
        UIGraphicsBeginImageContext(bounds.size)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func redraw(with color: UIColor) {
        resetTool()
        lineColor = color
        for sketch in undoArray {
            sketch.tool?.lineColor = color
        }
        updateCacheImage(true)
        setNeedsDisplay()
    }
    
    func getSignatureImage() {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 1)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let viewWidth = frame.width
        let viewHeight = frame.height
        
        globalUserInitiatedQueue {
            var x: CGFloat = .greatestFiniteMagnitude
            var y: CGFloat = .greatestFiniteMagnitude
            var width: CGFloat = 0
            var height: CGFloat = 0
            
            self.undoArray.reverse()
            
            for sketch in self.undoArray {
                if sketch.type == .clear {
                    break
                }
                sketch.tool.safelyUnwrap { (skechTool) in
                    skechTool.subPaths.forEach { (sketchPath) in
                        let rect = sketchPath.path.boundingBox
                        x = min(x,max(0, rect.origin.x))
                        y = min(y,max(0, rect.origin.y))
                        width = max(width, min(viewWidth, rect.maxX))
                        height = max(height, min(viewHeight, rect.maxY))
                    }
                }
            }
            
            mainQueue {
                x = max(0, x-10)
                y = max(0, y-10)
                width = min(viewWidth,width+10)
                height = min(viewHeight,height+10)
                image?.crop(CGRect(x: x, y: y, width: width-x, height: height-y)).safelyUnwrap{
                    self.delegate?.imageforDrawnSignature($0)
                }
            }
        }
        
        /*
        globalUserInitiatedQueue {
            var x: CGFloat = .greatestFiniteMagnitude
            var y: CGFloat = .greatestFiniteMagnitude
            var width: CGFloat = 0
            var height: CGFloat = 0
            self.pointsArray.forEach { (point) in
                x = min(x,max(0, point.x))
                y = min(y,max(0, point.y))
                width = max(width, min(viewWidth, point.x))
                height = max(height, min(viewHeight, point.y))
            }
            mainQueue {
                x = max(0, x-10)
                y = max(0, y-10)
                width = min(viewWidth,width+10)
                height = min(viewHeight,height+10)
                image?.crop(CGRect(x: x, y: y, width: width-x, height: height-y)).safelyUnwrap{
                    self.delegate?.imageforDrawnSignature($0)
                }
            }
        }
        */
    }
}


extension SketchView {
    public enum Actions {
        case clear
        case undo(_ isSignatureAvailable: Bool)
        case redo(_ isSignatureAvailable: Bool)
    }
}
