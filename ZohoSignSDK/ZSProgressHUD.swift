//
//  ZSProgressHUD.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 08/05/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit
import SVProgressHUD

enum loadingType {
    case general
    case activity
    case success
    case error
    case info
    case progress
}


public class ZSProgressHUD: UIView {

  
    public static let shared = ZSProgressHUD()
    public static let sharedProgressLoader = SVProgressHUD()

    let activityView = UIView()

    //MARK: - inits
    public convenience init() {
        self.init(frame: .zero)
        self.initateSetup()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.initateSetup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initateSetup()
    }

    //MARK: - Class methods
    public class func showActivity(inView : UIView? = nil, status : String = "sign.mobile.common.loading".ZSLString) {
        var superview = inView
        if superview == nil {
            superview = UIApplication.shared.keyWindow
        }
        guard let view = superview else {
            return
        }
        shared.showHUD(inView: view, message:((status.isEmpty) ? nil : status) ,type: .activity)

    }
    
    public class func show(inView : UIView, status : String = "sign.mobile.common.loading".ZSLString) {
        DispatchQueue.main.async {
            shared.showHUD(inView: inView,message: status)
        }
    }
    
  
    
    public class func showSuccess(inView : UIView, status : String) {
        DispatchQueue.main.async {
            shared.showHUD(inView: inView, message:  status, type: .success)
        }
    }
    
    
    //MARK:- Local methods
    internal func initateSetup()  {
        self.backgroundColor       =    UIColor.clear
        
        layer.zPosition     =   999

        let hudView = SVIndefiniteAnimatedView()
        hudView.radius = 28
        hudView.strokeThickness = 3
        hudView.strokeColor = ZColor.whiteColor
        
        activityView.addSubview(hudView)
        
        hudView.translatesAutoresizingMaskIntoConstraints = false
    
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setDefaultAnimationType(.flat)
        SVProgressHUD.setCornerRadius(10)
//        if ZSignKit.shared.isDynamicFontSupportNeeded {
        SVProgressHUD.setFont(ZSFont.bodyFont())
//        }else{
//            SVProgressHUD.setFont(ZSFont.regularFontLarge())
//        }
        SVProgressHUD.setMotionEffectEnabled(false)
//        SVProgressHUD.setMaximumDismissTimeInterval(2)
    }
    
    public class func dismiss()  {
        shared.dismiss()
    }
    public override func layoutSubviews() {
        super.layoutSubviews()
        frame = superview?.bounds ?? CGRect.zero
    }
    
    //MARK: -  activity HUD
    internal func showHUD(inView: UIView, message : String? = "sign.mobile.common.loading".ZSLString, type : loadingType = .general)  {
        
        
        backgroundColor = UIColor.clear
        autoresizesSubviews = true
        
        if !self.isDescendant(of: inView) {
            layer.opacity    =   0
            inView.addSubview(self)
        }
        else{
            inView.bringSubviewToFront(self)
        }
        self.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview()
        }
        inView.layoutIfNeeded()
        
        SVProgressHUD.setContainerView(self)
        switch type {
        case .general:
            SVProgressHUD.show(withStatus: message)
            break
        case .success:
            SVProgressHUD.showSuccess(withStatus: message)
            perform({
                self.dismiss()
            }, afterDelay: 1)
            break
        case .error:
            SVProgressHUD.showError(withStatus: message)
            break
        case .info:
            SVProgressHUD.showInfo(withStatus: message)
            break
        case .activity:
            SVProgressHUD.show(withStatus: message)
        case .progress:
            SVProgressHUD.showProgress(0, status: message)
        }
        
     
        layer.opacity = 1
      
    }
    

    public func dismiss(){
        SVProgressHUD.dismiss {
            self.layer.opacity     =   0;
            if(self.layer.opacity == 0){self.removeFromSuperview()}
        }
    }

    
    //MARK: - upload progress for attachments
    public class func showUploadingProgress(inView : UIView, msg : String = "sign.mobile.document.uploadMsg".ZSLString) {
      DispatchQueue.main.async {
//        inView: inView, message:  "sign.mobile.document.uploadMsg".ZSLString
        shared.showHUD(inView: inView, message: msg, type: .progress)
      }
    }
    
    public class func updateProgress(_ progress : Float, msg : String = "sign.mobile.document.uploadMsg".ZSLString) {
      DispatchQueue.main.async {
        SVProgressHUD.showProgress(progress, status: msg)
      }
    }
    
}
