//
//  IPhoneDocumentEditor.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 25/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//
//
//                       _oo0oo_
//                      o8888888o
//                      88" . "88
//                      (| -_- |)
//                      0\  =  /0
//                    ___/`---'\___
//                  .' \\|     |// '.
//                 / \\|||  :  |||// \
//                / _||||| -:- |||||- \
//               |   | \\\  -  /// |   |
//               | \_|  ''\---/''  |_/ |
//               \  .-\__  '-'  ___/-. /
//             ___'. .'  /--.--\  `. .'___
//          ."" '<  `.___\_<|>_/___.' >' "".
//         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
//         \  \ `_.   \_ __\ /__ _/   .-` /  /
//     =====`-.____`.___ \_____/___.-`___.-'=====
//                       `=---='
//
//
//     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


import UIKit
//import snap

public class IPhoneDocumentEditor: UIViewController {
    fileprivate let contentScrollView = UIScrollView()
    fileprivate var bgScrollView  = UIScrollView()
      
    let dataParser = EditorDataParser()
    
    public var isSampleSigning = false
    public var isFromCreatePage = false
    public var isFromActionExtn  = false
    public var isViewMode = false
    fileprivate var ignoreDidScrollHandling = false
    
    public var isThirdPartySignSDK = false
    
    public var extenstionContext : NSExtensionContext?

    var currentBottomBarHeight = 60
    let fieldSelectionMenu = IphoneEditorFieldSelectionView()


    public var createReqType : ZSDocumentCreatorType = .SIGN_AND_SEND

    var tempPanView   : UIView?

    private lazy var layout : UICollectionViewFlowLayout = {
        return SwiftUtils.pdfThumbanailLayout(showHeader: true)
    }()
    
    public var refRequest : SignRequest?
    public var refTemplate : Template?
    
    var beginningPoint = CGPoint.zero
    var beginningCenter = CGPoint.zero
    var lastScrollPoint = CGPoint.zero
    var totalEditorHeight : CGFloat = 0

    let yPadding :CGFloat = 10
    
    fileprivate let minimumZoomScale : CGFloat = 1
    
    var successMsgView : ZSSuccessMsgView!
    
    let toRecipButton  = ZSButton()

    let transitionManager = PresentationManager()
    private lazy var thumbnailView : ZSPDFThumbnailView = {
        let thumbnailView = ZSPDFThumbnailView(frame: CGRect.zero, collectionViewLayout: layout)
        thumbnailView.translatesAutoresizingMaskIntoConstraints = false
        thumbnailView.backgroundColor   =   ZColor.bgColorGray
        thumbnailView.clipsToBounds    =   false;
        thumbnailView.delegate = self
        thumbnailView.thumbnailDelegate = self
        return thumbnailView
    }()
    
    public var sampleSigningSuccessHandler : (() -> Void)?
    
    //MARK: - Inits

    deinit {
        ZInfo("deinit")
    }

    override public func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = ZColor.bgColorGray
        self.navigationItem.leftBarButtonItem = ZSBarButtonItem.barButton(type: .cancel, target: self, action: #selector(cancelButtonTapped))
        
        

        
        dataParser.delegate   = self
        dataParser.refRequest = refRequest
        dataParser.refTemplate = refTemplate
        dataParser.isFromCreatePage = isFromCreatePage
        dataParser.createReqType = createReqType
        dataParser.isSelfSigningRequest = refRequest?.isSelfSign == 1

        bgScrollView.backgroundColor = ZColor.bgColorGray
        bgScrollView.maximumZoomScale    =   4.0;
        bgScrollView.minimumZoomScale    =   minimumZoomScale;
        bgScrollView.delegate        =   self
        view.addSubview(bgScrollView)
        bgScrollView.snp.makeConstraints { (make) in
            make.leading.trailing.top.equalToSuperview()
            make.bottomMargin.equalToSuperview().inset(currentBottomBarHeight)
        }
        
        bgScrollView.addSubview(contentScrollView)
        bgScrollView.layoutIfNeeded()
        
        fieldSelectionMenu.isSignAndSend = (createReqType == .SIGN_AND_SEND)
        fieldSelectionMenu.initateSetup()
        weak var _self = self
        fieldSelectionMenu.selectionBlock = { type in
            _self?.addQuickSignField(type)
        }
     
        
        dataParser.pageWidth = bgScrollView.frame.width

        if isSampleSigning {
            self.navigationItem.rightBarButtonItem    =  ZSBarButtonItem.barButton(type: .finish, target: self, action: #selector(doneButtonClicked))
            self.title = "sign.mobile.request.sampleDocument".ZSLString
            
            dataParser.parseSampleDocFiles(thumbnailView: &thumbnailView)
            dataParser.currentPage = dataParser.pagesArray.first
            
            addFieldSelectionMenu()
            
        }else{
            self.navigationItem.rightBarButtonItem   = ZSBarButtonItem.barButton(type: (dataParser.isSelfSigningRequest ? .finish : .send), target: self, action: #selector(doneButtonClicked))
            if refTemplate != nil{
                self.navigationItem.rightBarButtonItem    = ZSBarButtonItem.barButton(type: .save, target: self, action: #selector(saveDraft))
            }
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            
            dataParser.getRequestTypes()
            dataParser.downloadDocument()
        }
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        
        let panGesture = UIPanGestureRecognizer()
        panGesture.addTarget(self, action: #selector(handlePan(gesture:)))
        panGesture.delegate  = self
        view.addGestureRecognizer(panGesture)
    }
    
    //MARK: - Local methods
    func addFieldSelectionMenu()  {
        view.addSubview(fieldSelectionMenu)
        fieldSelectionMenu.updateFieldsListForReceipient(dataParser.selectedRecipient)
        fieldSelectionMenu.translatesAutoresizingMaskIntoConstraints = false
        fieldSelectionMenu.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
//            make.top.equalTo(bgScrollView.snp.bottom)
            make.top.equalTo(view.snp.bottomMargin).inset(currentBottomBarHeight)
            make.bottom.equalToSuperview()
        }
    }
    
    func createRecipientSelectionForMultipleRecipients()  {
        bgScrollView.snp.removeConstraints()
        bgScrollView.snp.makeConstraints { (make) in
            make.top.equalTo(40)
            make.leading.trailing.equalToSuperview()
            make.bottomMargin.equalToSuperview().inset(currentBottomBarHeight)
        }
        
        let recipientSelectionView = UIView()
        recipientSelectionView.backgroundColor = ZColor.navBarColor
        view.addSubview(recipientSelectionView)

        recipientSelectionView.snp.makeConstraints { (make) in
            make.width.equalToSuperview().offset(2)
            make.left.equalTo(-1)
            make.height.equalTo(40)
        }
        recipientSelectionView.layer.borderColor  = ZColor.seperatorColor.cgColor
//        recipientSelectionView.layer.borderWidth  = 1
        
        toRecipButton.contentHorizontalAlignment = .left
        recipientSelectionView.addSubview(toRecipButton)
        toRecipButton.setImage(ZSImage.tintColorImage(withNamed: "nav_dropdown_icon", col: ZColor.buttonColor), for: .normal)
        toRecipButton.imageEdgeInsets = UIEdgeInsets(top: 4, left: 1, bottom: 0, right: 0)
        toRecipButton.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        toRecipButton.addTarget(self, action: #selector(showAvailableReceipients), for: .touchUpInside)
        toRecipButton.snp.makeConstraints { (make) in
            make.left.equalTo(cellValueLabelX)
            make.right.equalToSuperview().inset(cellValueLabelX)
            make.height.equalToSuperview()
            make.top.equalTo(0)
        }
        
    }
    
    @objc func showAvailableReceipients(){
        bgScrollView.setContentOffset(bgScrollView.contentOffset, animated: false)
        
        
        let popup = RecipientSelectionController(title: "sign.mobile.request.recipients".ZSLString, menuItems: dataParser.recipientsArray, selectedItem: dataParser.selectedRecipient)
        
        popup.didChangeItemHandler = {[weak self](index) in
            if let recep = self?.dataParser.recipientsArray[index]{
                self?.selectSiginingReceipient(recep)
            }
        }
            
        popup.modalPresentationStyle = .custom
        popup.transitioningDelegate = transitionManager

        present(popup, animated: true, completion: nil)
    }
    
    func addContentViews(){
        
        dataParser.pagesArray.forEach { (page) in
            
            var originY : CGFloat = 0
            if(page.originalPageNumber > 1){
                let editorView = dataParser.contentViews[page.originalPageNumber-1]
                originY = (editorView?.frame.maxY ?? 0) + yPadding
            }
            let viewWidth =  contentScrollView.frame.width
            let viewRect = CGRect(x: 0.0, y: originY, width: viewWidth, height: page.calibratedPageHeight(viewWidth))
            let contentView = DocumentEditorBaseView(frame: viewRect)
            contentView.isThirdPartySDK =   isThirdPartySignSDK
            let pdfPageImg = UIImage(contentsOfFile: page.filePath!)
            let downsampledImg = pdfPageImg?.downsampleImage(size: viewRect.size)
//            let modifyImg = UIImage(contentsOfFile: page.filePath!)?.downsampleImage(withSize: viewRect.size)
//            contentView.image           =   modifyImg
//            contentView.image           =   UIImage(contentsOfFile: page.filePath!)
            contentView.image           =   downsampledImg
            contentView.pdfPageSize     =   pdfPageImg?.size ?? view.frame.size
            contentView.delegate        =   dataParser
            contentView.docId           =   page.docId
            if(isSampleSigning){ contentView.currentInpersonSigner   =   dataParser.sampleSigner;}
            contentView.isTemplateDocumentFields    =   dataParser.selectedRecipient.isTemplateUser;
            contentView.docOrder        =  page.docOrder
            contentView.pageIndex       =  page.pageNumber - 1;
            //    contentView.pageNumber      =   (int)pageNumber - 1;
            contentView.currentEditingType =  ((createReqType == .SEND_FOR_SIGNATURE) || (createReqType == .TEMPLATE)) ?  .sentDoc_Create : .sentDoc_selfSign;
//            contentView.fieldsListArray =  NSMutableArray(array: fieldsListArray)
//            contentView.fieldsListArray =  dataParser.fieldsListArray
            contentView.isPreviewMode   =   (!contentScrollView.isUserInteractionEnabled || isViewMode);
            contentView.isFinalPreviewMode  =   dataParser.isSigningCompleted;
            contentView.superVC       =   self.navigationController;
//            contentView.deletedFieldsListArray = dataParser.deletedFieldsListArray
            contentView.isUserInteractionEnabled  =   true;
            contentView.isSampleSigning      =  isSampleSigning;
            
            contentView.layer.borderColor = ZColor.seperatorColor.cgColor
            contentView.layer.borderWidth = 0.5
            
            
            dataParser.contentViews[page.originalPageNumber] = contentView
            contentScrollView.addSubview(contentView)
            
            var filteredResults = dataParser.fieldsListArray.filter { (field) -> Bool in
                return ((field.pageIndex == contentView.pageIndex) && (field.docId == contentView.docId))
            }
            
            if (filteredResults.count > 0){
                contentView.preFillSignFields(filteredResults)
            }
        }
    }

 
    func updateContentViews() {
        
    }
    
    func  adjustViewFrames()  {
        updateTotalHeight(&totalEditorHeight)
        
//        contentScrollView.snp.makeConstraints { (make) in
//            make.top.leading.trailing.equalToSuperview()
//            make.height.equalTo(totalEditorHeight)
//        }
        contentScrollView.frame = CGRect(x: 0, y: 0, width: bgScrollView.frame.width , height: totalEditorHeight)
        bgScrollView.contentSize = CGSize(width: bgScrollView.frame.width, height: totalEditorHeight + yPadding)
    }
    
    func updateTotalHeight(_ totalEditorHeight : inout CGFloat){
        var height = -yPadding
        dataParser.pagesArray.forEach { (page) in
            height += page.calibratedPageHeight(bgScrollView.frame.width)
            height += yPadding
        }
        
        totalEditorHeight = height
    }
        
    func updateZoomScale()
    {
        bgScrollView.minimumZoomScale = minimumZoomScale
        bgScrollView.setZoomScale(minimumZoomScale, animated: true)
    }

    
    
    @objc func cancelButtonTapped()  {
        dataParser.cancelBtnTapped()
//        if !isThirdPartySignSDK {
//            documentDownloader.cancelDownload()
//        }

//        if !isSigningCompleted && isThirdPartySignSDK {
//            ZSignKit.shared.zSignFailureHandler()
//        }

        if isFromCreatePage {

            if isFromActionExtn {

                ZSignKit.shared.trackExtnSession(isStopped: true)
                extenstionContext?.completeRequest(returningItems: nil, completionHandler: nil)
            } else if dataParser.isSigningCompleted {
                dismiss(animated: true)
            } else {
                saveDraft()
            }

            return
        }
        if isSampleSigning || dataParser.isSigningCompleted || (refTemplate != nil) {
            dismiss(animated: true)

            return
        }
        
        if dataParser.isFieldsEdited {
            let changesActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let saveAction = UIAlertAction(title: "sign.mobile.request.saveChanges".ZSLString, style: .default, handler: { action in
                self.saveDraft()
                })
            let discardAction = UIAlertAction(title: "sign.mobile.request.list.discardChanges".ZSLString, style: .destructive, handler: { action in
                    ZSAnalytics.shared.track(forEvent: .documentEditor( .discardChanges))

                    self.dismissVC()
                })
            let cancelAction = UIAlertAction(title: "sign.mobile.common.cancel".ZSLString, style: .cancel, handler: { action in
                })
            if changesActionSheet.popoverPresentationController != nil {
                changesActionSheet.popoverPresentationController?.barButtonItem = navigationItem.leftBarButtonItem
            }


            changesActionSheet.addAction(saveAction)
            changesActionSheet.addAction(discardAction)
            changesActionSheet.addAction(cancelAction)
            present(changesActionSheet, animated: true)
        } else {
            dismiss(animated: true)
        }

    }
    
    @objc func dismissVC()  {
        dataParser.cancelAllTasks()
        dismiss(animated: true)
    }
    
    func endEditing()  {
        ZSPopTip.dismissPoptip()
        dataParser.contentViews.forEach { (key, value) in
              value.endEditing()
        }
    }
    
    @objc func doneButtonClicked() {
        ZSPopTip.dismissPoptip()

        endEditing()


        if isSampleSigning {
            contentScrollView.isUserInteractionEnabled = false
            dataParser.showFinalPreview()
            
        
            ZSProgressHUD.show(inView: navigationController!.view , status: "sign.mobile.activityIndicator.signingDoc".ZSLString)

            ZSAnalytics.shared.track(forEvent: .documentEditor(.sampleSigning))
            weak var weakSelf = self
            perform({
                    weakSelf?.updateZoomScale()
                    ZSProgressHUD.showSuccess(inView: weakSelf?.navigationController?.view ?? UIView(), status: "sign.mobile.request.signedSuccessfully".ZSLString)

                    weakSelf?.sampleSigningSuccessHandling()
            }, afterDelay: 2)

//            navigationItem.rightBarButtonItem = nil
            return
        }
        
        dataParser.submitRequestToServer()
//        updateZoomScale()


//        dataParser.selectedRecipient.draftFieldListArray.removeAll()
//        let tempArray = dataParser.fieldsListArray.filter { (field) -> Bool in
//            return field.isOtherRecipientField == false
//        }

        
//        if let tempArray = tempArray as? [AnyHashable] {
//            dataParser.selectedRecipient.draftFieldListArray.append(contentsOf: tempArray)
//        }
//
//        let dict = dataParser.validateSignFieldValues(isCompleted: true)
//
//        if dict == nil {
//            return
//        }
//        weak var _self = self
//        if isSelfSigningRequest {
//            if isFromActionExtn {
//                navigationController?.view.isUserInteractionEnabled = false
//            }
//
//            ZSProgressHUD.show(in: navigationController?.view, status: ZSLString("sign.mobile.activityIndicator.signingDoc"))
//
//
//            requestDatamanager.submitSelfSignRequest(withReqID: refRequest.requestId(), params: dict, success: { response in
//                _self?.dataManager(withRequriedData: response, apiType: ZS_SubmitSelfSignRequest)
//
//            }, failure: { error in
//                _self?.requestUpdateErrorHandling(error)
//            })
//        }else{
//            var signersArray: [Recipient] = []
//            for recep in recipientsArray {
//                if recep.isNeedToSign() || (recep.actionType == ZS_ActionType_Approver) {
//                    signersArray.append(recep)
//                }
//            }
//
//
//            let popup = RequestDetailsConfirmationController(title: ZSLString("sign.mobile.request.confirmDetails"), menuItems: signersArray)
//            let nav = UINavigationController(rootViewController: popup)
//            nav.modalPresentationStyle = .custom
//            presentationManager.popupHeight = popup.totalHeight
//            presentationManager.isBottomPopupWithCustomHeight = true
//            nav.transitioningDelegate = presentationManager
//            present(nav, animated: true)
//
//            popup.confirmButtonTapHandler = {
//                ZSProgressHUD.show(in: self.navigationController?.view, status: ZSLString("sign.mobile.activityIndicator.submittingDoc"))
//
//
//                self.requestDatamanager.submitRequest(withReqID: self.refRequest.requestId(), params: dict, success: { response in
//                    self.dataManager(withRequriedData: response, apiType: ZS_SubmitDraftRequest)
//                }, failure: { error in
//                    self.requestUpdateErrorHandling(error)
//                })
//            }
//
//            navigationItem?.rightBarButtonItem?.isEnabled = true
//        }
    }
    
    @objc func saveDraft()
    {
        dataParser.pageWidth = bgScrollView.frame.width
        dataParser.saveDraft()
    }
    
    func sampleSigningSuccessHandling()  {
        ZSPopTip.dismissPoptip()
        
        navigationItem.leftBarButtonItem  = nil
        navigationItem.rightBarButtonItem = ZSBarButtonItem.barButton(type: .done, target: self, action: #selector(cancelButtonTapped))

        
        self.view.gestureRecognizers = nil;
        let successMsgView = ZSSuccessMsgView.sharedInstance()
        successMsgView.showSuccessMsg(inView: view, type: .SampleSigning)
            
        successMsgView.didActionButtonPressed = {[weak self] in
            self?.sampleSigningSuccessHandler?()
        }
//        [successMsgView setDidActionButtonPressed:^{
//            if (sampleSuccessCallback) {
//                sampleSuccessCallback();
//            }
//        }];
        currentBottomBarHeight = successMsgView.viewHeight;
        
        bgScrollView.contentSize   =  CGSize(width: contentScrollView.frame.width, height: contentScrollView.frame.height)
    }
    
    
    func addQuickSignField(_ type : ZSignFieldType)  {
        if let editorView = dataParser.contentViews[dataParser.currentPage.originalPageNumber]{
            editorView.addSignField(point: CGPoint(x: editorView.bounds.width/2, y: editorView.bounds.height/2), fieldType: type)
        }
        
    }
//    func addSignField(tapPoint: CGPoint, fieldType: signFieldType, isQuickAdd: Bool, pangesture: UIPanGestureRecognizer?)  {
//          GlobalFunctions.makeDeviceVibrate()
//          SwiftUtils.dismissPoptip()
//
//        dataParser.contentViews.forEach { (key, value) in
//              if( value.isEditingMode){
//                  value.endEditing()
//                  bgScrollView.isScrollEnabled = true
//              }
//          }
//
//          dataParser.contentViews.forEach { (key, editorView) in
//              if(editorView.frame.contains(tapPoint)){
//                  var point = CGPoint(x: editorView.bounds.width/2, y: editorView.bounds.height/2)
//                 editorView.isUserInteractionEnabled = true
//                  if(isQuickAdd){
//                    editorView.addSignField(point: point, fieldType: fieldType)
//                      return
//                  }else if let gesture = pangesture{
//                      point = gesture.location(in: editorView)
//                  }else{
//                      point = bgScrollView.convert(tapPoint, to: editorView)
//                  }
//                editorView.addSignField(point: point, fieldType: fieldType)
//              }
//          }
//      }

    
     @objc func handlePan(gesture : UIPanGestureRecognizer) {
        if (dataParser.isSigningCompleted) {return;}

            let touchLocation = gesture.location(in: self.view)
            switch gesture.state {
            case .began:
                tempPanView?.removeFromSuperview()

                beginningPoint = touchLocation;
                beginningCenter = touchLocation;
                bgScrollView.isUserInteractionEnabled = false;
                let convertedPoint = view.convert(touchLocation, to: fieldSelectionMenu)
                tempPanView = fieldSelectionMenu.viewInTouchLocation(convertedPoint)
                tempPanView?.alpha = 0.8
                if tempPanView != nil{
                    view.addSubview(tempPanView!)
                    tempPanView!.center = beginningPoint;
                }
                fieldSelectionMenu.isUserInteractionEnabled = false;
                break;
            case .changed:
                tempPanView?.center = touchLocation;
                break;
            case .ended:
                bgScrollView.isUserInteractionEnabled = true;
                fieldSelectionMenu.isUserInteractionEnabled = true;

                if tempPanView == nil{
                    tempPanView?.removeFromSuperview()
                    return
                }


                tempPanView?.center = touchLocation;

                let point  = gesture.location(in: contentScrollView)
                var zoomedPoint = point
                if (bgScrollView.zoomScale < 1){
                    zoomedPoint =  CGPoint(x: point.x * bgScrollView.zoomScale, y: point.y * bgScrollView.zoomScale)
                    zoomedPoint.x +=  contentScrollView.frame.origin.x
                }

                if(contentScrollView.frame.contains(zoomedPoint)){
                    dataParser.addSignField(tapPoint: point, fieldType: fieldSelectionMenu.selectedFieldType, isQuickAdd: false, pangesture: gesture, inView: &bgScrollView)
                }

                UIView.animate(withDuration: 0.1, animations: {
                    self.tempPanView?.transform = CGAffineTransform.init(scaleX: 0.3, y: 0.3)
                }) { (finished) in
                    self.tempPanView?.removeFromSuperview()
    //                self.tempPanView = nil;
                }

                break;
            default:
                break;
            }
        }

   
}

extension IPhoneDocumentEditor : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        self.goToPage(docIndex: indexPath.section, pageIndex: indexPath.row)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
        let width : CGFloat = 120//120
        var a4Ratio : CGFloat = 1.414
        var height = width*a4Ratio + 55
        
           
        let filteredResults = dataParser.pagesArray.filter { (page) -> Bool in
            return ((page.pageNumber == indexPath.row+1) && (page.docOrder == indexPath.section))
           }
           guard let page = filteredResults.first else {
               return CGSize(width: width, height: height)
           }
        
        a4Ratio = page.sizeRatio
        height = width*a4Ratio + 55
        return CGSize(width: width, height: height)
    }
}

extension IPhoneDocumentEditor : ZSPDFThumbnailViewDelegate{
    public func pdfThumbnailViewDidSelectHeader(section: Int) {
//        goToPage(docIndex: section, pageIndex: 0)
    }
}

//MARK: - Dataparser Delegate

extension IPhoneDocumentEditor : DocumentCreateEditorDelegate{
    
    func editorDataParserParsingCompleted() {
        dataParser.resetAllFields()
        if dataParser.recipientsArray.count > 1 {
            createRecipientSelectionForMultipleRecipients()
        }
    
        for receipient in dataParser.recipientsArray {
            if(receipient.isNeedToSign){
                selectSiginingReceipient(receipient)
                break;
            }
        }
        
        
        self.navigationItem.rightBarButtonItem?.isEnabled  = ((dataParser.fieldsListArray.count > 0) || (dataParser.selectedRecipient.actionType == ZS_ActionType_Approver))
        
        adjustViewFrames()
        addFieldSelectionMenu()
        addContentViews()
        thumbnailView.goToPage(indexPath: IndexPath(item: 0, section: 0), animated: false)
        //        fieldCountBtn.setBadge(String(format: "%i", dataParser.fieldsListArray.count))
        
        //        contentScrollView.isHidden = true
        perform({ [weak self] in
            self?.updateContentViews()
            }, afterDelay: 0)
    }
    
    func editorDataParserParsingFailed() {
//        dismissVC()
    }
    
    func editorDataParserSaveDraftCompleted() {
        dismissVC()
    }
    
    func editorDataParserRequestCreated() {
        if !dataParser.isSelfSigningRequest {
            dismissVC()
        }
        
        updateZoomScale()
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItems = nil
        self.navigationItem.rightBarButtonItem = ZSBarButtonItem.barButton(type: .done, target: self, action: #selector(dismissVC))
        
        
        if (isThirdPartySignSDK) {
            ZSProgressHUD.showSuccess(inView: self.navigationController!.view, status: "sign.mobile.request.signedSuccessfully".ZSLString)
            
            let notif = ZNotificationToastView(title: "sign.mobile.tooltip.extnSuccessmsg".ZSLString)
            notif.showSuccessMsg()
            
        }else{
            showSuccessNotification()
            
            
            ZSProgressHUD.dismiss()
        }
        
        dataParser.currentPage = dataParser.pagesArray.first
        bgScrollView.setContentOffset(CGPoint.zero, animated: true)
        dataParser.showFinalPreview()
                
        self.perform({
            UIView.animate(withDuration: 0.3, animations: {
                self.contentScrollView.center = CGPoint(x: (self.bgScrollView.frame.width / 2), y: self.contentScrollView.center.y)
            }, completion: { (finish) in
                self.fieldSelectionMenu.isHidden = true
            })
            
        }, afterDelay: 0)
    }
    
    func editorDataParserTemplateCreated(_ template: Template?) {
        dismissVC()
    }
    
    func editorDataParserRequestUpdateFailed() {
//        dismissVC()
        
        
    }
    
    func shareButtonClicked()  {
        dataParser.shareDocument(fromButton: successMsgView.actionButton)
    }
    
    func showSuccessNotification() {

        if !isThirdPartySignSDK {
            successMsgView = ZSSuccessMsgView.sharedInstance()
            successMsgView.documentName = title ?? ""
            successMsgView.showSuccessMsg(inView: view, type: (isFromActionExtn ? .ExtnSigning : .SelfSigning))

            weak var _self = self
            successMsgView.didActionButtonPressed = {
                _self?.shareButtonClicked()
            }

            currentBottomBarHeight = Int(successMsgView.viewHeight)
        } else {
            currentBottomBarHeight = 0
        }
        bgScrollView.contentSize = CGSize(width: contentScrollView.frame.size.width, height: contentScrollView.frame.size.height + CGFloat(currentBottomBarHeight))
    }

    
    
    func editorDataParserFieldsUpdated(_ fieldsListArray: [SignField]) {
        navigationItem.rightBarButtonItem?.isEnabled = (fieldsListArray.count > 0)
    }
       

    
    func selectSiginingReceipient(_ recep : ZSRecipient) {
        let myAttribute = [ NSAttributedString.Key.foregroundColor: ZColor.buttonColor,
                            NSAttributedString.Key.font : ZSFont.mediumFont(withSize: 15)]

        let str = String(format: "%@ %@", "sign.mobile.common.mailTo".ZSLString,recep.displayRecipientName())
        let attr = NSMutableAttributedString(string: str, attributes: myAttribute)
        attr.addAttribute(.foregroundColor, value: ZColor.secondaryTextColorDark, range: NSRange(location: 0, length: "sign.mobile.common.mailTo".ZSLString.count))

        toRecipButton.setAttributedTitle(attr, for: .normal)
        
        dataParser.changeCurrentRecipient(recep)
    
    }
    

}

//MARK: - Gesture Delegate
extension IPhoneDocumentEditor : UIGestureRecognizerDelegate{
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
            if isViewMode { return false}
        else if let panGesture = gestureRecognizer as? UIPanGestureRecognizer{
            let touchPoint = gestureRecognizer.location(in: fieldSelectionMenu)
            return ((touchPoint.x > 0) && (touchPoint.y > 0) && panGesture.view != bgScrollView)
        }
        return !bgScrollView.isHidden
    }
}

extension IPhoneDocumentEditor : UIScrollViewDelegate{
    public func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        if scale  < minimumZoomScale {
            bgScrollView.setZoomScale(minimumZoomScale, animated: true)
        }
    }
    
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        if (scrollView == bgScrollView) {
            return contentScrollView;
        }else{
            return nil;
        }
    }
    
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        guard let currentEditorView = dataParser.getCurrentEditorView() else {
            return
        }

        let touchLocation = scrollView.panGestureRecognizer.location(in: contentScrollView)
        if let selectedFieldView = currentEditorView.currentSelectedView as? ZResizeView{
            let newPoint = contentScrollView.convert(touchLocation, to: currentEditorView)
            let touched = selectedFieldView.frame.contains(newPoint )
            if(touched){
                scrollView.isScrollEnabled = false
                scrollView.isScrollEnabled = true
                ZInfo("touched")
                return;
            }
        }
        
        lastScrollPoint = scrollView.contentOffset
    }
    
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if ignoreDidScrollHandling {return}
        if (scrollView != bgScrollView) {return;}
        
        let yVal = scrollView.contentOffset.y
        
        guard let currentPage = dataParser.currentPage else {
            return
        }
        if (lastScrollPoint.y > scrollView.contentOffset.y) { //    // move down
            if (yVal >= (totalEditorHeight - bgScrollView.frame.size.height)) {
                dataParser.currentPage = dataParser.pagesArray.last;
            }
            else if (dataParser.currentPage.originalPageNumber > 1){
                let prevEditorView = dataParser.getCurrentEditorView(-1);
                
                let diff = ((yVal / bgScrollView.zoomScale) - prevEditorView!.frame.origin.y);
                
                if (diff < 0) {
                    dataParser.currentPage = dataParser.pagesArray[currentPage.originalPageNumber-2]
                }
            }
        }
        else if (lastScrollPoint.y < scrollView.contentOffset.y) {
            
            // move up
            if (yVal <= 30) { dataParser.currentPage = dataParser.pagesArray.first
            }
            else if (dataParser.currentPage.originalPageNumber < dataParser.pagesArray.count) {
                let nextEditorView = dataParser.getCurrentEditorView(1)
                
                let diff = (nextEditorView!.frame.origin.y - (yVal / bgScrollView.zoomScale))
                
                if (diff < (bgScrollView.frame.size.height/2)) {
                    dataParser.currentPage = dataParser.pagesArray[currentPage.originalPageNumber];
                }
                
            }
        }
        thumbnailView.goToPage(indexPath: IndexPath(row: dataParser.currentPage.pageNumber-1, section: currentPage.docOrder), animated: true)
        lastScrollPoint = scrollView.contentOffset;
    }

}


