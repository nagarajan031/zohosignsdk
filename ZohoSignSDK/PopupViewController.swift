//
//  PopupViewController.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 15/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

public struct PopupDataModel: Equatable {
    public var title: String
    public var subtitle: String? = nil
    public var image: UIImage? = nil
    
    public init(title: String, subtitle: String? = nil, image: UIImage? = nil) {
        self.title = title
        self.subtitle = subtitle
        self.image = image
    }
    
    public static func getDataModel(forStrings strings: [String], forImages image: [UIImage]? = nil) -> [PopupDataModel] {
        return strings.enumeratedMap { return PopupDataModel(title: $1, image: image?[$0]) }
    }
    
    public static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.title == rhs.title
    }
}

public class PopupViewController: UITableViewController {
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        if #available(iOS 13, *){
            view.style =  .large;
        } else {
            view.style = .whiteLarge
        }
        view.backgroundColor  = ZColor.bgColorWhite
        view.hidesWhenStopped = true
        view.color = ZColor.secondaryTextColorDark
        view.startAnimating()
        return view
    }()
    
    var menuItems: [PopupDataModel] = [] {
        willSet {
            activityIndicator.stopAnimating()
            if newValue.count == 0 {
                showNoDataView()
            }
            tableView.reloadData()
        }
    }
    public var shouldEnableScroll: Bool = true
    public var shouldAddFooterView: Bool = true
    public var imageCornerRadius: CGFloat?
    public var shouldAddCloseButton: Bool = false
    public var vcDirection: PresentationDirection = .bottomWithCustomHeight
    public var tableViewCellSize: CGFloat = TableviewCellSize.normal
    var selectedIndex: Int = -1
    public var completionHandler: ((Int) -> (Void))?
    
    //MARK: - View lifecycle
    
    public init(title: String, menuItems: [PopupDataModel], selectedItem: PopupDataModel) {
        super.init(style: .plain)
        self.title = title
        self.menuItems = menuItems
        self.selectedIndex = menuItems.firstIndex(of: selectedItem) ?? 0
    }
    
    public init(title: String, menuItems: [PopupDataModel], selectedIndex: Int) {
        super.init(style: .plain)
        self.title = title
        self.menuItems = menuItems
        self.selectedIndex = selectedIndex
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ZColor.bgColorWhite
        tableView.backgroundColor = ZColor.bgColorWhite
        if shouldAddFooterView {
            tableView.tableFooterView = UIView()
        }
        tableView.rowHeight = tableViewCellSize
        tableView.showsVerticalScrollIndicator = false
        tableView.setSeparatorStyle()
        tableView.isScrollEnabled = shouldEnableScroll
        
        view.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { (make) in
            make.center.size.equalToSuperview()
        }
        if shouldAddCloseButton {
            navigationItem.rightBarButtonItem = ZSBarButtonItem.barButton(type: .close, target: self, action: #selector(closeButtonClicked))
        }
    }
    
    @objc func closeButtonClicked() {
        dismiss(animated: true, completion: nil)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if menuItems.count > 0 {
            activityIndicator.stopAnimating()
        }
        tableView.scrollToRow(at: IndexPath(row: selectedIndex, section: 0), at: .middle, animated: false)
    }
    
    
    //MARK: - Local methods
    @objc public static func getViewControllerHeight(numberOfItems: CGFloat) -> CGFloat{
        let maxNumRows: CGFloat = (DeviceType.isIphone5Orless) ? 6 : 8
        let numberOfRows = numberOfItems > maxNumRows ? maxNumRows : numberOfItems
        let tableHeight = (TableviewCellSize.normal *  numberOfRows )
        return tableHeight
    }
    
    
    public func updateMenuItems(_ menuItems: [PopupDataModel]) {
        activityIndicator.stopAnimating()
        self.menuItems = menuItems
    }
    
    func showNoDataView(){
        
    }
    
    
    //MARK: - table delegate, datasource
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = String(describing: UITableViewCell.self)
        var cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        if cell.isNull {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: identifier)
            let selectedView = UIView()
            selectedView.backgroundColor = ZColor.listSelectionColor;
            cell.selectedBackgroundView = selectedView;
        }
        
        let menuItem = menuItems[indexPath.row]
        
        if let image = menuItem.image {
            let imageView = cell.imageView
            imageView?.image = image
            imageView?.clipsToBounds = true
            imageView?.layer.cornerRadius = imageCornerRadius ?? image.size.height / 2
        }
        
        if let subtitle = menuItem.subtitle {
            cell.detailTextLabel?.text = subtitle
            cell.detailTextLabel?.textColor = ZColor.secondaryTextColorLight
            cell.detailTextLabel?.font = ZSFont.listSubTitleFont()
        }
        
        cell.backgroundColor = ZColor.bgColorWhite
        cell.textLabel?.text = menuItem.title
        cell.textLabel?.font = ZSFont.regularFontLarge()
        cell.textLabel?.textColor = ZColor.primaryTextColor
        cell.accessoryType = .none

        if (indexPath.row == selectedIndex){
            cell.accessoryType = .checkmark
            cell.textLabel?.textColor = ZColor.buttonColor
        }
        
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true) {
            self.completionHandler?(indexPath.row)
        }
    }
  
    
}

extension PopupViewController: ZPresentable {
    public var direction: PresentationDirection {
        return vcDirection
    }
    
    public var navBar: PresentationNavBar{
        return PresentationNavBar(size: PresentationBarSize.withTitleBig, title: title, titleAlignment: .center)
    }
    
    public var popupHeight: CGFloat{
        return PopupViewController.getViewControllerHeight(numberOfItems: CGFloat(menuItems.count)) + bottomMargin + navBar.size
    }
}
