//
//  ZSPopupMenuViewController.swift
//  ZohoSignSDK
//
//  Created by somesh-pt2434 on 01/02/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit
let kNavbarHeight : CGFloat =   44

public class ZSPopupMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate{
    
    private lazy var semiTransparentView: UIView = {
        let view = UIView()
        view.backgroundColor = ZColor.bgColorDullBlack
        view.alpha = 0
        view.isUserInteractionEnabled = false
        return view
    }()
    
    private lazy var popupBackgroundView: UIView = {       
        let view = UIView()
        view.backgroundColor      =       ZColor.bgColorWhite
        view.clipsToBounds        =       true
        view.layer.cornerRadius   =     10
        return view
    }()
    
    private lazy var headerView: UIView = {
       let view = UIView()
        view.addBorders(edges: [.bottom], color: ZColor.seperatorColor, inset: 0, thickness: 0.4)
        return view
    }()
    
    private lazy var menuTableView: UITableView = {
       let view = UITableView()
        view.delegate = self
        view.dataSource = self
        view.clipsToBounds        =       true
        view.rowHeight                         =   ZSignKit.shared.isDynamicFontSupportNeeded ? UITableView.automaticDimension : TableviewCellSize.normal
        view.setSeparatorStyle(color : (DeviceType.isIphone ? ZColor.seperatorColor : ZColor.seperatorColorLight))
        view.showsVerticalScrollIndicator      =    false
        view.backgroundColor                   =   ZColor.bgColorWhite
        view.isHidden = true
        view.alwaysBounceVertical = true
        return view
    }()

    private lazy var activityIndicatorView: UIActivityIndicatorView = {
       let view = UIActivityIndicatorView()
        view.style =   .whiteLarge;
        view.backgroundColor  = ZColor.bgColorWhite
        view.hidesWhenStopped = true
        view.color = ZColor.secondaryTextColorDark
        view.startAnimating()
        return view
    }()
    
    private lazy var headerTitlelabel: UILabel = {
       let view = UILabel()
        view.text             =  self.title
        view.textAlignment    =   .center;
        view.textColor        =   ZColor.primaryTextColor
        view.font             =   ZSFont.navTitleFont()
        return view
    }()
    
    private lazy var noDataLabel: UILabel = {
       let view = UILabel()
        view.numberOfLines       =   0
        view.textAlignment       =   .center
        view.text                =   "sign.mobile.emptyScreen.noDataAvailable".ZSLString
        view.textColor           =   ZColor.secondaryTextColorLight
        view.font                =   ZSFont.listTitleFontSmall()
        view.autoresizingMask    =   [.flexibleWidth]
        view.layer.zPosition     =   100;
        return view
    }()
    
    var menuItemArray : NSArray?
    public var imagesArray: [UIImage]?
    
    var selectedIndex = -1
    public var isShowLoader = false
    public var selectedStr : String? = ""
    
    var maxNumRows: CGFloat = 0
    var numberOfRows: CGFloat = 0
    var tableHeight: CGFloat = 0
    

    public typealias completedBlock = (Int) -> Void
    public var didChangeItemHandler : completedBlock?
    var isCompleted = false
    public var isPresentedAsFormSheet: Bool = false
    public var isSelectAlreadyChosenValue : Bool = false

    private var superView = UIView()
    
    @objc public init(title: String , menuItems: NSArray , selectedItem: String) {
        super.init(nibName: nil, bundle: nil)
        self.title = title
        self.menuItemArray  =   menuItems
        self.selectedStr    =   selectedItem
        
        selectedIndex = menuItems.index(of: selectedItem)
        
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
    }
    
    @objc public init(title: String , menuItems: NSArray , selectedIndex: Int) {
        super.init(nibName: nil, bundle: nil)
        self.title = title
        self.menuItemArray  =   menuItems
        
        if (selectedIndex < (menuItems.count)) {
            self.selectedIndex  =   selectedIndex
            self.selectedStr    =   menuItems[selectedIndex] as? String
        }
        
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(white: 0, alpha: 0)
        superView = (isPresentedAsFormSheet ? presentingViewController?.view : self.view)!
        createView()
        if DeviceType.isIphone {
            SwiftUtils.generateImpactFeedBack(for: .light)
        }
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addConstraints()
        if !isShowLoader{
            activityIndicatorView.stopAnimating()
            menuTableView.isHidden = false
        }

        isCompleted = false
        
        
        if !DeviceType.isIpad || isPresentedAsFormSheet{
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: [.curveEaseIn], animations: {
                self.semiTransparentView.alpha = 1
                self.popupBackgroundView.frame.origin.y = self.superView.frame.height - self.tableHeight - kNavbarHeight
            }) { (bool) in
                self.semiTransparentView.isUserInteractionEnabled = true
            }
        }
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        menuTableView.scrollToRow(at: IndexPath(row: selectedIndex, section: 0), at: .middle, animated: true)
    }
    
    func createView() {
        let tapGesture = UITapGestureRecognizer()
        tapGesture.numberOfTapsRequired = 1
        tapGesture.delegate    =    self
        tapGesture.addTarget(self, action: #selector(hideView))
        self.semiTransparentView.addGestureRecognizer(tapGesture)
        maxNumRows = (DeviceType.isIphone5Orless) ? 6 : 8
        numberOfRows = ((menuItemArray!.count > Int(maxNumRows)) ? maxNumRows : CGFloat(menuItemArray!.count))
        tableHeight = (TableviewCellSize.normal *  numberOfRows ) + kNavbarHeight + ((menuItemArray!.count > 5) ? 15 : 0)
        //        var tableHeight : CGFloat = (kMenuRowHeight *  numberOfRows ) + ((menuItemArray!.count > 5) ? 15 : 0)
        
        if isShowLoader { tableHeight = 150}
        else if (menuItemArray?.count == 0){
            tableHeight = 150;
            showEmptyDataView()
        }
        
        self.view.addSubview(semiTransparentView)
        self.view.addSubview(popupBackgroundView)
        
        headerView.addSubview(headerTitlelabel)
        popupBackgroundView.addSubview(menuTableView)
        popupBackgroundView.addSubview(headerView)
        popupBackgroundView.addSubview(activityIndicatorView)
        
    }
    
    func addConstraints()  {
        if DeviceType.isIpad && !isPresentedAsFormSheet{
            preferredContentSize = CGSize(width: 375, height: tableHeight + kNavbarHeight)
            popupBackgroundView.frame =  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: tableHeight + kNavbarHeight)
            
        } else {
            popupBackgroundView.frame =  CGRect(x: 0, y: self.superView.frame.size.height, width: self.superView.frame.size.width, height: tableHeight + kNavbarHeight)
        }
        
        semiTransparentView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        headerView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(kNavbarHeight)
        }
        
        headerTitlelabel.snp.makeConstraints { (make) in
            make.center.height.equalToSuperview()
            make.width.equalToSuperview().offset(-32)
        }
        
         self.menuTableView.frame = CGRect(x: 0, y: kNavbarHeight, width: self.popupBackgroundView.frame.size.width, height: self.tableHeight)
        
        activityIndicatorView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.width.equalTo(100)
        }
    }
    
    @objc public func updateMenu(menuItemsArray: NSArray)  {
        activityIndicatorView.stopAnimating()
//        popupBackgroundView.frame = CGRect.zero
        
        self.menuItemArray  =    menuItemsArray
        
        selectedIndex = menuItemsArray.index(of: selectedStr!)
        
        numberOfRows = ((menuItemArray!.count > 6) ? 6 : CGFloat(menuItemArray!.count))
        tableHeight = (TableviewCellSize.normal *  numberOfRows ) + kNavbarHeight + ((menuItemArray!.count > 5) ? 15 : 0)
        //        var tableHeight : CGFloat = (kMenuRowHeight *  numberOfRows ) + ((menuItemArray!.count > 5) ? 15 : 0)
        
        if (menuItemsArray.count == 0){
            tableHeight = 150;
            showEmptyDataView()
        } else {
            menuTableView.isHidden = false
        }
        self.menuTableView.reloadData()

        if DeviceType.isIpad && !isPresentedAsFormSheet {
            
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                self.preferredContentSize = CGSize(width: 375, height: kNavbarHeight + self.tableHeight)
                self.popupBackgroundView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: kNavbarHeight + self.tableHeight)
                self.menuTableView.frame = CGRect(x: 0, y: kNavbarHeight, width: self.popupBackgroundView.bounds.size.width, height: self.tableHeight)
                
            }, completion: { (bool) in
//                self.menuTableView.reloadData()

            })
        } else {
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                self.popupBackgroundView.frame = CGRect(x: 0, y: self.superView.frame.height - self.tableHeight - kNavbarHeight, width: self.superView.frame.width, height: kNavbarHeight + self.tableHeight)
                self.menuTableView.frame = CGRect(x: 0, y: kNavbarHeight, width: self.popupBackgroundView.bounds.size.width, height: self.tableHeight)
                
            }, completion: { (bool) in
//                self.menuTableView.reloadData()
            })
        }
        
        
        
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (menuItemArray == nil) ? 0 : menuItemArray!.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell"
        
        var cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        if cell == nil {
            cell =  UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: identifier)
            let selectedView = UIView()
            selectedView.backgroundColor = ZColor.listSelectionColor;
            cell.selectedBackgroundView = selectedView;
        }
        
        if let imagesArray = imagesArray {
            let image = imagesArray[indexPath.row]
            let imageView = cell.imageView
            imageView?.image = image
            imageView?.clipsToBounds = true
            imageView?.layer.cornerRadius = image.size.height/2
        }
        
        cell.backgroundColor = ZColor.bgColorWhite
        cell.textLabel?.text = menuItemArray?.object(at: indexPath.row) as? String
        if !ZSignKit.shared.isDynamicFontSupportNeeded{
            cell.textLabel?.font = ZSFont.regularFontLarge()
            cell.textLabel?.textColor = ZColor.primaryTextColor
        }
        cell.accessoryType = .none

        if (indexPath.row == selectedIndex){
            cell.accessoryType = .checkmark
            cell.textLabel?.textColor = ZColor.buttonColor
        }
        
     
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndex != indexPath.row || isSelectAlreadyChosenValue{
            isCompleted = true
            selectedIndex = indexPath.row
        }
        hideView()
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y < 0){
            scrollView.contentOffset    = CGPoint(x: 0, y: 0);
        }
    }

    
    func showEmptyDataView()  {
        popupBackgroundView.addSubview(noDataLabel)
        noDataLabel.snp.makeConstraints { (make) in
            make.width.equalToSuperview().offset(-30)
            make.height.equalTo(80)
            make.center.equalToSuperview()
        }
        menuTableView.isHidden = true
    }
    
    //MARK: ONCLICK FUNCTIONS
    @objc func hideView(){
        if !DeviceType.isIpad || isPresentedAsFormSheet{
//            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
//                self.semiTransparentView.alpha = 0
//                self.popupBackgroundView.frame.origin.y = self.view.frame.height
//            }) { (finished) in
//                self.dismiss(animated: false, completion: {
//                    if self.isCompleted {
//                        self.didChangeItemHandler?(self.selectedIndex)
//                    }
//                })
//            }
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: [.curveEaseOut], animations: {
                self.semiTransparentView.alpha = 0
                self.popupBackgroundView.frame.origin.y = self.view.frame.height
            }, completion: { (bool) in
                self.dismiss(animated: false, completion: {
                    if self.isCompleted {
                        self.didChangeItemHandler?(self.selectedIndex)
                    }
                })
            })
//            perform({
//                UIView.animate(withDuration: 0.3, animations: {
//                })
//            }, afterDelay: 0.1)
        } else {
            self.dismiss(animated: true, completion: {
                if self.isCompleted {
                    self.didChangeItemHandler?(self.selectedIndex)
                }
            })
        }
        
    }

}
