//
//  ZSAnalytics.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 26/09/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation
public class ZSAnalytics: NSObject {
    public static let shared = ZSAnalytics()
    public var eventHandler: ((ZSAnalyticsEventType,ResponseDictionary?) -> Void)?
    public var eventHandlerForName: ((String) -> Void)?
    public var errorHandler: ((Error) -> Void)?
    public var extensionHandler: ((Bool) -> Void)?
    
    private override init() {
        super.init()
    }
    
    public func track(forName name: String) {
        eventHandlerForName?(name)
    }
    
    public func track(forEvent event: ZSAnalyticsEventType, properties: ResponseDictionary? = nil) {
        eventHandler?(event,properties)
    }
    
    public func track(forError error: Error) {
        errorHandler?(error)
    }
    
    public func track(isExtensionStopped: Bool) {
        extensionHandler?(isExtensionStopped)
    }
    
    deinit {
        eventHandler = nil
        errorHandler = nil
        extensionHandler = nil
    }
}
