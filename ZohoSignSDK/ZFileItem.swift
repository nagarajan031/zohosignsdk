//
//  ZFileItem.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 13/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import UIKit
import PDFKit
import MobileCoreServices

public class ZFileItem: NSObject {

    public var fileName: String?
    public var fileSize : Int32 = 0
    public var lastModifiedDate: Date?
    var isFolder = false
    public var filePath: String?
    public var fileUrl: URL?
    public var mimeType: String = "application/octet-stream"
    public var fileID: String!
    public var createdTime: String?
    public var filePassword: String?
    public var thumbnailImage: UIImage?
    //* A pre-generated description of the item that is displayed in the picker view
//    private(set) var localizedMetadata: String?
    public var isFromCloudServices = false
    public var cloudServiceName: String!
    public var cloudServiceDocId: String!
    public var isFromCameraUpload = false
    public var isUploadingInProgress = false
    public var isUploadFailed = false
    
    public var filePasscodeSuccessHandling  : ((String?) -> Void)?
    
    private var passwordPopup : ZSTextPopupController!
    // Whether the file uploaded failed or not
    public func isPasswordProtecedPdf() -> Bool {
        guard let fileurl = fileUrl, let pdfDocument = PDFDocument(url: fileurl) else {
            return false
        }
        
        return pdfDocument.isLocked || pdfDocument.isEncrypted
    }
    
    public func thumbnailImage(size : CGSize) -> UIImage?{
        if let thumbnailImg = thumbnailImage {
            return thumbnailImg.downsampleImage(size: size)
        }
        
        if isImageType() {
            return UIImage(contentsOfFile: filePath!)?.downsampleImage(size: size)
        }
      
        //pdf  pdfDocument.thumbnailImage(size: size)
        if let url = fileUrl , let pdfDocument = ZSPDFDocument(url: url), let thumnailImg = pdfDocument.thumbnailImage(size: size) {
            return thumnailImg
        }
       
        
        return UIImage(named: "list_bg")?.downsampleImage(size: size)
    }
    

    public func unlockPdf(password : String) {
        guard let fileurl = fileUrl, let pdfDocument = PDFDocument(url: fileurl) else {
            return
        }
        
        pdfDocument.unlock(withPassword: password)
        let inboxFolderPath  = ZSFileManager.shared.getFolderPathDirectory(withName: nil, isCacheDirectory: false)
        filePath = inboxFolderPath?.appending("/" + "\(self.fileName)")
        pdfDocument.write(toFile: filePath!)
        
        filePasscodeSuccessHandling?(password)
        
        passwordPopup.dismiss(animated: true, completion: nil)
    }

    public func unlockPdf(withBaseVC baseVC: UIViewController?, success successBlock: @escaping (String?) -> Void, failure failureBlock: @escaping () -> Void) {
        
        filePasscodeSuccessHandling = successBlock
        
        if passwordPopup  == nil{
            passwordPopup = ZSTextPopupController()
        }
        passwordPopup.shouldIgnoreViewDismiss = true
        passwordPopup.isTextFieldCanbeEmpty = false
        passwordPopup.title = "sign.mobile.common.password".ZSLString
        passwordPopup.showFooterMsg("sign.mobile.alert.documentPasswordProtected".ZSLString)
        weak var _file = self
        weak var weakVC = passwordPopup
        passwordPopup.didCancelled = {
            if failureBlock != nil {
                failureBlock()
            }
        }
        passwordPopup.didEndEditingWithText = { password in
            if _file?.isFromCloudServices != nil && successBlock != nil {
                successBlock(password)
                weakVC?.dismiss(animated: true)
            } else {
                _file?.unlockPdf(password: password)
            }
        }

        baseVC?.present(passwordPopup, animated: false)
    }

    public func trimmedFileName() -> String {
        guard let fileName = fileName as? NSString else {
            return ""
        }
        
        
        var trimmedName = fileName.deletingPathExtension

        let regexPredicate = NSPredicate(format: "SELF MATCHES %@", ZS_Regex_ForName)
        if !regexPredicate.evaluate(with: trimmedName) {
            trimmedName = trimmedName.components(separatedBy: CharacterSet.letters.inverted).joined(separator: "")
        }

        return trimmedName
    }

    public func fileNameWithExtn() -> String {
        guard let fileName = fileName, let extn = fileExtn(forMimeType: fileName) else {
            return "temp.pdf" //failure case
        }

        let fileUrl = URL(fileURLWithPath: fileName)
        
        if (fileUrl.pathExtension).contains(extn) {
            return fileName
        }
        return "\(fileName).\(extn)"
    }

    public func fileNameWithMimeType(name: String) -> String? {
        let mimeTypeStr = mimeType

        if (mimeTypeStr == "image/jpeg") {
            return "\(name).jpg"
        } else if (mimeTypeStr == "image/png") {
            return "\(name).png"
        } else if (mimeTypeStr == "image/gif") {
            return "\(name).gif"
        } else if (mimeTypeStr == "image/tiff") {
            return "\(name).tiff"
        } else if (mimeTypeStr == "application/pdf") {
            return "\(name).pdf"
        } else if (mimeTypeStr == "text/plain") {
            return "\(name).txt"
        }
        return name
    }

    

    func fileExtn(forMimeType fileName: String?) -> String? {
        let mimeTypeStr = mimeType

        if (mimeTypeStr == "image/jpeg") {
            return "jpg"
        } else if (mimeTypeStr == "image/png") {
            return "png"
        } else if (mimeTypeStr == "image/gif") {
            return "gif"
        } else if (mimeTypeStr == "image/tiff") {
            return "tiff"
        } else if (mimeTypeStr == "application/pdf") {
            return "pdf"
        } else if (mimeTypeStr == "text/plain") {
            return "txt"
        }
        return URL(fileURLWithPath: fileName ?? "").pathExtension
    }

    func fileUploadIdentifier() -> String? {
        if let fielid = fileID {
            return fielid
        } else if let filePath = filePath {
            return filePath
        } else if let fileUrl = fileUrl {
            return fileUrl.absoluteString
        } else if let fileName = fileName {
            return fileName
        }
        return "File" //rarecase
    }
    
    func isImageType() -> Bool {
        let fileExtension = URL(fileURLWithPath: filePath ?? "").pathExtension
        guard let fileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension as CFString, nil)?.takeRetainedValue()  else{
            return false
        }
        return UTTypeConformsTo(fileUTI, kUTTypeImage)
    }
    
    

}
