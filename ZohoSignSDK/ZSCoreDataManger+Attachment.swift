//
//  ZSCoreDataManger+Attachment.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 09/10/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import CoreData

extension ZSCoreDataManager{
    
    @objc public func updateAttachment(attachment : Attachment, dict: [AnyHashable : Any]) {
        attachment.attachmentId = dict[keyPath:"attachment_id"]
        attachment.attachmentName = dict[keyPath:"attachment_name"]
        attachment.totalPages = dict[keyPath:"total_pages"] ?? 0
        attachment.fieldId = dict[keyPath:"field_id"]
        var size : Int64 = dict[keyPath:"attachment_size"] ?? 0
        if size < 0 {size = 0}
        attachment.attachmentSize = size
    }
    
    @discardableResult
    @objc public func addAttachment(withDataDict dict: [AnyHashable : Any]) -> Attachment {
        let aAttachment = Attachment(context: managedObjContext)
        
        aAttachment.attachmentId = dict[keyPath:"attachment_id"]
        aAttachment.attachmentName = dict[keyPath:"attachment_name"]
        aAttachment.totalPages = dict[keyPath:"total_pages"] ?? 0
        aAttachment.fieldId = dict[keyPath:"field_id"]
        var size : Int64 = dict[keyPath:"attachment_size"] ?? 0
        if size < 0 {size = 0}
        aAttachment.attachmentSize = size
        return aAttachment
    }
    
    @objc public func getAttachmentsList(fieldId: String) -> [Attachment] {
        let fetchRequest: NSFetchRequest<Attachment> = Attachment.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "fieldId == %@", fieldId)
        if let fetchedObjects =  try? managedObjContext.fetch(fetchRequest){
            return fetchedObjects
        }else{
            return []
        }
    }
}
