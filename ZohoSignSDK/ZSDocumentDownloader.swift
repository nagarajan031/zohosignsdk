//
//  ZSDocumentDownloader.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 31/10/17.
//  Copyright © 2017 Zoho Corporation. All rights reserved.
//

import UIKit
import PDFKit

public class ZSPage: NSObject{
    public var docId : String?
    public var docName : String?
    public var docOrder : Int = 0
    public var pageNumber  : Int = 1
    public var originalPageNumber  : Int = 1
//    public var thumbnailPath : String?
    public var filePath : String?
    public var width : CGFloat = 0
    public var height : CGFloat = 0
    public var sizeRatio : CGFloat = 0
    public var rotation : Int = 0

//    public var thumbnailImage : UIImage?{
//        didSet{
//            width = (thumbnailImage?.size.width)!
//            height = (thumbnailImage?.size.height)!
//            sizeRatio = (height / width)
//        }
//    }

    public func calculatePageSize()  {
        let image = UIImage(contentsOfFile: filePath!)
        let divRatio : CGFloat = DeviceType.isIphone ? 1 :  2
//        width = (image?.size.width ?? 1) / divRatio
//        height = (image?.size.height ?? 1) / divRatio
        width = image?.size.width ?? 1
        height = image?.size.height ?? 1
        sizeRatio = (height / width)
    }
    
    public func calibratedPageHeight(_ width : CGFloat) ->  CGFloat{
        return width * sizeRatio
    }
    
    
}

@available(iOS 11.0, *)
public class ZSDocumentDownloader: NSObject  {
    
    weak var sentRequest : SignRequest?
    weak var receivedRequest : MyRequest?
    weak var template : Template?
    let sentRequestDataManager = SentDocumentsRequestManager()
    let fileDownloadDataManager = FileRequestManager()
    let guestDownloadDataManager = ReceivedDocumentSigningRequestManager()

    let paths          =   NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
    var currentDownloadDocIndex = 0
    var documentsArray : [Documents] = []
    var orgPageIndex = 1
    var isRequestCancelled = false
    
    var attachmentsArray : [Attachment] = []
    let fileManager = ZSFileManager()

    public  var completionHandler    : (([ZSPage]) -> Void)!
//    public var downloadProgress    : ((_ currentDocIndex : Int,_ totalDocs : Int, _ progess : Progress) -> Void)!
    public var downloadProgress    : ((_ currentDocIndex : Int,_ totalDocs : Int, _ progess : CGFloat) -> Void)!
    public var downloadErrorHandler    : ((_ error : ZSError?) -> Void)!
    
    var pagesList : [ZSPage]  = []
    
    public init(sentRequest request: SignRequest) {
        super.init()
        self.sentRequest    =   request
    }
    
    public init(template refTemplate: Template) {
        super.init()
        self.template    =   refTemplate
    }
    
    public init(receivedRequest request: MyRequest) {
        super.init()
        self.receivedRequest    =   request
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        ZInfo("deint")
    }
    
    //MARK:- download
    public func startDownloadingDocument()
    {
        if (sentRequest != nil)
        {
            for doc in (sentRequest?.documents)! {
                documentsArray.append(doc)
            }
            
            documentsArray =  documentsArray.sorted { $0.documnetOrder < $1.documnetOrder }
            downloadNextSentDocInQueue()
        }
        else if (receivedRequest != nil) {
            for doc in (receivedRequest?.documents)! {
                documentsArray.append(doc)
            }
            
            documentsArray =  documentsArray.sorted { $0.documnetOrder < $1.documnetOrder }
            downloadNextReceivedDocInQueue()
        }
        else if (template != nil)
        {
            for doc in (template?.documents)! {
                documentsArray.append(doc)
            }
            
            documentsArray =  documentsArray.sorted { $0.documnetOrder < $1.documnetOrder }
            downloadNextTemplatesDocInQueue()
        }
    }
    
    
    //MARK:- local methods

    func parseDocumentPagesForRequest()  {
        
        for i in 0..<documentsArray.count {

            let doc : Documents = documentsArray[i]
            
            ZInfo(doc.documentId ?? "empty")
            let dataPath        =  paths[0] + String(format:"/downloads/%@/pages",doc.documentId!)
            
    
            for index in 0..<doc.totalPages {
                let page  = ZSPage()
                page.docId = doc.documentId
                page.docName = doc.documentName
                page.docOrder = Int(doc.documnetOrder)
                page.pageNumber  = Int(index+1)
                page.originalPageNumber =   Int(orgPageIndex)
//                page.thumbnailPath =  String(format: "%@/%i_thumbnail.jpg",dataPath, index)
                //                page.thumbnailPath  = String(format: "%@/%i.jpg",dataPath, index)
                page.filePath  = String(format: "%@/%i.jpg",dataPath, index)
                page.calculatePageSize()

                pagesList.append(page)
                orgPageIndex = orgPageIndex+1
                
            }
//            receivedRequest?.attachments?.forEach({ (attach) in
//                attachmentsArray.append(attach)
//            })
        }
        currentDownloadDocIndex = 0
        
        
        downloadNextReceivedDocumentAttachment()
        
//        DispatchQueue.main.async {
//            ZInfo(self.pagesList)
//            self.completionHandler?(self.pagesList)
//        }
    }
    

    
    func downloadNextSentDocInQueue()
    {
        if  (documentsArray.count > currentDownloadDocIndex) {
            let doc : Documents =  documentsArray[currentDownloadDocIndex]
            let dataPath        =  paths[0] + String(format:"/downloads/%@/pages",doc.documentId!)
            let filePathStr     =  String(format: "%@/0.jpg",dataPath);
            
            if (FileManager.default.fileExists(atPath: filePathStr))
            {
                currentDownloadDocIndex = currentDownloadDocIndex+1
                downloadNextSentDocInQueue()
            }
            else{

                weak var weakself = self
                fileDownloadDataManager.downloadFile(fileID: doc.documentId!, returnAsPDF: false, progress: { (progress) in
                    weakself?.downloadProgress(self.currentDownloadDocIndex+1,self.documentsArray.count,CGFloat(progress?.fractionCompleted ?? 0.0))
                }, success: { (data) in
                    weakself?.savePDFToFile(doc.documentId!)
                }) { (error) in
                    weakself?.downloadErrorHandling(error)
                }
                
            
                //Below ios 10
               /* sentRequestDataManager.downloadRequestAsZip(reqID: sentRequest!.requestId!, docId: doc.documentId!, progress: { (progress) in
                    self.downloadProgress(self.currentDownloadDocIndex+1,self.documentsArray.count,CGFloat(progress?.fractionCompleted ?? 0.0))
                    
                }, success: { (data) in
                    self.downloadNextDocument()
                }) { (error) in
                    if !self.isRequestCancelled {
                        self.downloadErrorHandler()
                    }
                }*/
            }
        }
        else{
            parseDocumentPagesForRequest()
        }
    }
    
    func downloadNextReceivedDocInQueue()  {
        if  (documentsArray.count > currentDownloadDocIndex) {
            let doc : Documents =  documentsArray[currentDownloadDocIndex]
            let dataPath        =  paths[0] + String(format:"/downloads/%@/pages",doc.documentId!)
            let filePathStr     =  String(format: "%@/0.jpg",dataPath);

            if (FileManager.default.fileExists(atPath: filePathStr))
            {
                currentDownloadDocIndex = currentDownloadDocIndex+1
                downloadNextReceivedDocInQueue()
            }
            else{
                weak var weakself = self
                guestDownloadDataManager.downloadRequestFile(request: receivedRequest!, fileId: doc.documentId!, progress: { (progress) in
                    weakself?.downlodProgressHandling(progress, docsArray:  weakself?.documentsArray)
                }, success: { (filePath) in
                    
                    weakself?.receivedRequestFilesDownloadSuccessHandling(filePath: filePath as? String, document: doc)
                }) { (error) in
                    weakself?.downloadErrorHandling(error)
                }
            }
        
        }
        else{
            
            parseDocumentPagesForRequest()
        }
    }
    
    func downloadNextReceivedDocumentAttachment(){
        
        if  (attachmentsArray.count > currentDownloadDocIndex) {
            let attachment = attachmentsArray[currentDownloadDocIndex]
            let attachmentStatus = fileManager.attachmentFilePath(attachmentId: attachment.attachmentId!)
            if attachmentStatus.fileAlreadyAvailable {
                currentDownloadDocIndex += 1
                downloadNextReceivedDocumentAttachment()
            }
            else{
                weak var weakself = self

                guestDownloadDataManager.downloadAttachment(request: receivedRequest!, id: attachment.attachmentId!, progress: { (progress) in
                    weakself?.downlodProgressHandling(progress, docsArray:  weakself?.attachmentsArray)
                }, success: { (filePath) in
                    weakself?.currentDownloadDocIndex += 1
                    weakself?.downloadNextReceivedDocumentAttachment()
                }) { (error) in
                    weakself?.downloadErrorHandling(error)
                }
            }
            
        }
        else{
            DispatchQueue.main.async {
                ZInfo(self.pagesList)
                self.completionHandler?(self.pagesList)
            }
        }
    }
    
    func downloadNextTemplatesDocInQueue()  {
        if  (documentsArray.count > currentDownloadDocIndex) {
            let doc : Documents =  documentsArray[currentDownloadDocIndex]
            let dataPath        =  paths[0] + String(format:"/downloads/%@/pages",doc.documentId!)
            let filePathStr     =  String(format: "%@/0.jpg",dataPath);
            
            if (FileManager.default.fileExists(atPath: filePathStr))
            {
                currentDownloadDocIndex = currentDownloadDocIndex+1
                downloadNextTemplatesDocInQueue()
            }
            else{
                guard let tempId = template?.templateId,let docId = doc.documentId  else {
                    downloadErrorHandling(fileDownloadDataManager.dataParsingError)
                    return
                }
                weak var weakself = self
                fileDownloadDataManager .downloadTemplateFile(templateID: tempId, fileID: docId, progress: { (progress) in
                    weakself?.downlodProgressHandling(progress, docsArray:  weakself?.documentsArray)
                }, success: { (filePath) in
                    weakself?.savePDFToFile(docId)
                }) { (error) in
                    weakself?.downloadErrorHandling(error)
                }
            }
            
        }
        else{
            
            parseDocumentPagesForRequest()
        }
    }
    
    @available(iOS 11, *)
    func savePDFToFile(_ fileID: String){
        /*var filePath : String?
        if (template != nil) {
            filePath = ZSCommonUtils.getDocumentForTemplate(withDocumentId: fileID)
        }
        else{
            filePath = ZSCommonUtils.getFileWithFileId(fileID, fromCache: false)
        }*/
        let fileManager = ZSFileManager()
        let filePath  = fileManager.getFile(withFileId:fileID, isSharedLocation: false)
        let doc : Documents =  documentsArray[currentDownloadDocIndex]
        let dataPath        =  paths[0] + String(format:"/downloads/%@/pages",doc.documentId!)
        if let filterPath = filePath{
                convertPDFToImages(filePath: filterPath , toFolder: dataPath)
            }
    
        downloadNextDocument()
    }

    
    
    func downloadNextDocument(){
        currentDownloadDocIndex = currentDownloadDocIndex+1
        if (sentRequest != nil) { downloadNextSentDocInQueue() }
        else if (receivedRequest != nil) {downloadNextReceivedDocInQueue() }
        else if (template != nil) {downloadNextTemplatesDocInQueue() }
    }
    
    //MARK: - PDF download
    fileprivate func downloadDocAsPDF(){
        weak var weakself = self

        if let request = sentRequest
        {
            for doc in (request.documents)! {
                documentsArray.append(doc)
            }
            
            documentsArray =  documentsArray.sorted { $0.documnetOrder < $1.documnetOrder }
            fileDownloadDataManager.downloadFilesForSentRequest(requestID: request.requestId!, saveChace: false, progress: { (progress) in
                weakself?.downlodProgressHandling(progress, docsArray: self.documentsArray)
            }, success: { (response) in
                weakself?.sentRequestFilesDownloadSuccessHandling(data: response)
            }) { (error) in
                weakself?.downloadErrorHandling(error)
            }
        }
        else if (receivedRequest != nil) {
            for doc in (receivedRequest?.documents)! {
                documentsArray.append(doc)
            }
            
            documentsArray =  documentsArray.sorted { $0.documnetOrder < $1.documnetOrder }
            downloadNextReceivedDocInQueue()
        }
        else if (template != nil)
        {
            for doc in (template?.documents)! {
                documentsArray.append(doc)
            }
            
            documentsArray =  documentsArray.sorted { $0.documnetOrder < $1.documnetOrder }
            downloadNextTemplatesDocInQueue()
        }
    }

    
    @objc public func cancelDownload()  {
        isRequestCancelled = true
        sentRequestDataManager.cancelAllTasks()
        fileDownloadDataManager.cancelAllTasks()
        guestDownloadDataManager.cancelAllTasks()
    }
    
      func convertPDFToImages(filePath:String, toFolder: String) {
            let fileUrl  = URL(fileURLWithPath: filePath)
          //  var imagesArray : [UIImage]  = []
            if !FileManager.default.fileExists(atPath: toFolder) {
                  try? FileManager.default.createDirectory(atPath: toFolder, withIntermediateDirectories: true, attributes: nil)
            }
            
            do{
                guard let pdfdocument =  PDFDocument(url: fileUrl) else {
                    return
                }
               
                for index in 0...(pdfdocument.pageCount-1){
                    let page = pdfdocument.page(at: index)
                    let pageRect = page?.bounds(for: .artBox)
                    

//                    let pdfImgSize  = DeviceType.isIphone ? pageRect!.size : CGSize(width: pageRect!.size.width * 2, height: pageRect!.size.height * 2)
                    let pdfImgSize  = pageRect!.size
                    let imgFilePath =  String(format: "%@/%i.jpg",toFolder, index)
                    try autoreleasepool { () -> Void in
                        if var image = page?.thumbnail(of: pdfImgSize, for: .artBox){
                            //imagesArray.append(image)
                            /*if let rotation = page?.rotation, rotation > 0, SwiftUtils.isIOS13AndAbove, !DeviceType.isMac {
                                image = image.rotate(radians: (Float(rotation) * (.pi / 180)))
                            }*/
                            let imgData = image.pngData()
                            
                            try imgData?.write(to: URL(fileURLWithPath: imgFilePath), options: .atomic)
                        }
                    }
                }
            }
            catch(let ex) {
                ZInfo(ex.localizedDescription)
                //Fail
            }
            
        }
    
    //MARK: - Data manager delegate
    
    func sentRequestFilesDownloadSuccessHandling(data : Any?)  {
        guard let filePathArray : [String] = data as? [String] else {
            return;
        }
        let doc : Documents =  documentsArray[currentDownloadDocIndex]
        let dataPath        =  paths[0] + String(format:"/downloads/%@/pages",doc.documentId!)
        let filteredPath = filePathArray.filter { $0.contains(doc.documentId!) }.first
        if (filteredPath != nil){
            convertPDFToImages(filePath: filteredPath! , toFolder: dataPath)
        }
        
        currentDownloadDocIndex = currentDownloadDocIndex+1
        if (sentRequest != nil) { downloadNextSentDocInQueue() }
        if (receivedRequest != nil) {downloadNextReceivedDocInQueue() }
        else if (template != nil) {downloadNextTemplatesDocInQueue() }
    }
    
    func receivedRequestFilesDownloadSuccessHandling(filePath : String?, document :  Documents)  {
        let dataPath        =  paths[0] + String(format:"/downloads/%@/pages",document.documentId!)
        
        
        if let filterPath = filePath{
            convertPDFToImages(filePath: filterPath , toFolder: dataPath)
        }
        
        currentDownloadDocIndex = currentDownloadDocIndex+1
        downloadNextReceivedDocInQueue()
    }
    
    func downloadErrorHandling(_ error : ZSError?)  {
        if !isRequestCancelled {
            downloadErrorHandler(error)
        }
    }
    
    func downlodProgressHandling(_ progress: Progress!, docsArray : [Any]!)  {
        DispatchQueue.main.async {
            let calibratedPercentage = (((CGFloat(self.currentDownloadDocIndex) + CGFloat(progress.fractionCompleted))) / CGFloat(docsArray.count))
            
            ZInfo(calibratedPercentage)
            self.downloadProgress(self.currentDownloadDocIndex+1,docsArray.count,calibratedPercentage)
        }
    }
}

