//
//  ZSContact.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 16/11/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit

public class ZSContact: NSObject {
    public var contactName : String = ""
    public var contactEmail : String = ""
    public var phoneNumber: String?
    public var countryCode: String?

    public var isLocal = false

}
