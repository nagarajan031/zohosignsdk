//
//  ZSCoreDataManager+MyRequest.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 02/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import CoreData

extension ZSCoreDataManager {
    
    //MARK: CREATE
    public func addMyRequest(with dict: ResponseDictionary) -> MyRequest? {
        let receivedReq = MyRequest(context: managedObjContext)
        updateMyRequest(withRequestDetail: receivedReq, withDict: dict, shouldSave: false)
        return receivedReq
    }
    
    public func addMyRequests(fromResponseObject object: Any?, forFilter requestFilter: String, shouldRemovePreviousElement shouldRemove: Bool) {
        
        guard let dictArray = object as? [ResponseDictionary] else {return}
        
        managedObjContext.performAndWait {
            let filterFetchRequest: NSFetchRequest<Filter> = Filter.fetchRequest()
            filterFetchRequest.predicate = requestFilter == REQUEST_FILTER_SEARCH ?
                NSPredicate(format: "filterKey=%@", requestFilter) :
                NSPredicate(format: "isMyReqFilter == %@ && filterKey == %@", NSNumber(booleanLiteral: true),requestFilter)
            
            do {
                let filter = try managedObjContext.fetch(filterFetchRequest).first
                
                if shouldRemove {
                    filter?.myRequests = nil
                    if SwiftUtils.isIOS11 {
                        save()
                    }
                }
                
                dictArray.forEach { (dict) in
                    guard let myRequestId: String = dict[keyPath: "my_request_id"] else {return}
                    let myRequestFetchRequest: NSFetchRequest<MyRequest> = MyRequest.fetchRequest()
                    myRequestFetchRequest.predicate = NSPredicate(format: "myRequestId == %@", myRequestId)
                    
                    var myRequest: MyRequest
                    if let request = try? managedObjContext.fetch(myRequestFetchRequest).first {
                        myRequest = request
                    } else {
                        myRequest = MyRequest(context: managedObjContext)
                    }
                    
                    updateMyRequest(withRequest: myRequest, fromDict: dict)
                    filter?.addToMyRequests(myRequest)
                }
                save()
            } catch {
                ZError(error.localizedDescription)
            }
        }
    }
    
    public func addTemporaryMyRequest(withSignId signId: String,forFilter filter: Filter? = nil) -> MyRequest? {
        var myRequest: MyRequest? = nil
        
        managedObjContext.performAndWait {
            let fetchRequest: NSFetchRequest<MyRequest> = MyRequest.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "signId == %@", signId)
            if let request = try? managedObjContext.fetch(fetchRequest).first {
                myRequest = request
            } else {
                myRequest = MyRequest(context: managedObjContext)
            }
            
            myRequest?.signId = signId
            filter?.addToMyRequests(myRequest!)
            save()
        }
        
        return myRequest
    }
    
    //MARK: UPDATE
    
    private func updateMyRequest(withRequest myRequest: MyRequest, fromDict dict: ResponseDictionary) {
        myRequest.actionType = dict[keyPath: "action_type"]
        myRequest.expireByTime = dict[keyPath: "expire_by"]
        myRequest.isExpired = dict[keyPath: "is_expired"]
        myRequest.isExpiring = dict[keyPath: "is_expiring"]
        myRequest.myRequestId = dict[keyPath: "my_request_id"]
        myRequest.name = dict[keyPath: "request_name"]
        myRequest.isDeletedRequest = dict[keyPath: "is_deleted"] ?? false
        myRequest.myRequestStatus = dict[keyPath: "my_status"]
        myRequest.requestStatus = dict[keyPath: "request_status"]
        myRequest.requestedTime = dict[keyPath: "requested_time"]
        myRequest.requesterEmail = dict[keyPath: "requester_email"]
        myRequest.requesterName = dict[keyPath: "requester_name"]
        myRequest.signId = dict[keyPath: "sign_id"]
        myRequest.typeName = dict[keyPath: "request_type_name"]
        myRequest.actionStatus = myRequest.myRequestStatus
        
        DateManager.formatTableviewHeader(myRequest.requestedTime, setTodayasNil: false).safelyUnwrap { (num) in
            if myRequest.sectionIdentifierRequestedTime != num.int64Value {
                myRequest.sectionIdentifierRequestedTime = num.int64Value
            }
        }
        
        myRequest.documents = nil
        
        var totalPages: Int16 = 0
        dict[keyPath: "document_ids"].safelyUnwrap { (docDicts: [ResponseDictionary]) in
            
            docDicts.forEach { (docDict) in
                if let documentId: String = docDict[keyPath: "document_id"] {
                    let document = addDocument(withDocumentId: documentId, andDataDict: docDict)
                    myRequest.addToDocuments(document)
                    totalPages += document.totalPages
                    if let imageString = document.thumbnailStr, document.documnetOrder == 0{
                        myRequest.thumbnailPath = String(format: "%@/%@.jpg", thumbnailFolderPath, documentId)
                        myRequest.thumbnailStr = imageString
                    }
                }
            }
            
        }
        
        
        
        dict[keyPath: "sign_ids"].safelyUnwrap { (signIds: [ResponseDictionary]) in
            myRequest.inpersonSigners = NSKeyedArchiver.archivedData(withRootObject: signIds)
        }
        
        myRequest.totalPages = NSNumber(value: totalPages)
    }
    
    public func updateMyRequest(withRequestDetail receivedRequest: MyRequest, withDict dict: ResponseDictionary, shouldSave: Bool) {
        receivedRequest.totalPages = dict[keyPath: "total_pages"]
        receivedRequest.ownerName = String(format: "%@ %@", dict[keyPath: "owner_first_name"] ?? "", dict[keyPath: "owner_last_name"] ?? "")
        receivedRequest.requestStatus = dict[keyPath: "request_status"]
        receivedRequest.requestId = dict[keyPath: "request_id"]
        
        dict[keyPath: "recipient_email"].safelyUnwrap({ (email: String) in
            receivedRequest.recipientEmail = email
        })
        
        dict[keyPath: "recipient_name"].safelyUnwrap { (name: String) in
            receivedRequest.recipientName = name
        }
        
        receivedRequest.expireByTime = dict[keyPath: "expire_by"]
        receivedRequest.name = dict[keyPath: "request_name"]
        receivedRequest.notes = dict[keyPath: "notes"]
        receivedRequest.desc = dict[keyPath: "description"]
        receivedRequest.validity = dict[keyPath: "validity"] ?? 0
        
        if receivedRequest.requesterName.isNull {
            receivedRequest.requesterName = receivedRequest.ownerName
        }
        
        dict[keyPath: "created_time"].safelyUnwrap { (createdTime: NSNumber) in
            receivedRequest.requestedTime = createdTime
        }
        
        DateManager.formatTableviewHeader(receivedRequest.requestedTime, setTodayasNil: false).safelyUnwrap { (num) in
            if receivedRequest.sectionIdentifierRequestedTime != num.int64Value {
                receivedRequest.sectionIdentifierRequestedTime = num.int64Value
            }
        }
        
        receivedRequest.documents = nil
        var pages: Int16 = 0
        if let documents: [ResponseDictionary] = dict[keyPath: "document_ids"] {
            documents.forEach { (documentDict) in
                
                if let documentId: String = documentDict[keyPath: "document_id"] {
                    let document = addDocument(withDocumentId: documentId, andDataDict: documentDict)
                    receivedRequest.addToDocuments(document)
                    if let imageString = document.thumbnailStr,
                        document.documnetOrder == 0{
                        receivedRequest.thumbnailPath = String(format: "%@/%@.jpg", thumbnailFolderPath, documentId)
                        receivedRequest.thumbnailStr = imageString
                    }
                    pages += document.totalPages
                }
                
            }
        }
        
        self.deletAllObjects(forEntities: Attachment.self, shouldSave: false)
        
        dict[keyPath: "attachments"].safelyUnwrap { (dictArray: [ResponseDictionary]) in
            dictArray.forEach { (dict) in
                let _ = ZSCoreDataManager.shared.addAttachment(withDataDict: dict)
            }
        }
        
        receivedRequest.totalPages = NSNumber(value: pages)
        
        if let actions: [ResponseDictionary] = dict[keyPath: "actions"] {
            receivedRequest.actions = NSKeyedArchiver.archivedData(withRootObject: actions)
            if let lastAction = actions.last {
                receivedRequest.actionType = lastAction[keyPath: "action_type"]
                receivedRequest.actionStatus = lastAction[keyPath: "action_status"]
            }
        }
        
        dict[keyPath: "my_status"].safelyUnwrap { (status: String) in
            receivedRequest.myRequestStatus = status
        }
        
        dict[keyPath: "sign_id"].safelyUnwrap { (signId: String) in
            receivedRequest.signId = signId
        }
        
        dict[keyPath: "sign_ids"].safelyUnwrap { (signIds: [ResponseDictionary]) in
            receivedRequest.inpersonSigners = NSKeyedArchiver.archivedData(withRootObject: signIds)
        }
        
        let dict1 = dict["signer_settings"]
        
        if let signerSettingDict : [String : AnyHashable] = dict[keyPath: "signer_settings"]{
            let signerSetting = SignerSettings(signerSettingDict: signerSettingDict)
            let encoder = JSONEncoder()

            if let signerSettingData = try? encoder.encode(signerSetting){
                receivedRequest.signerSettings = signerSettingData
            }

        }
        
        if shouldSave {
            managedObjContext.performAndWait {
                save()
            }
        }
    }
    
    //MARK: GET
    public func getMyRequest(with requestId: String?,or signId : String?) -> MyRequest? {
        var receivedReq : MyRequest?
        do {
            let fetchRequest : NSFetchRequest<MyRequest> = MyRequest.fetchRequest()
            if let reqId = requestId {
                fetchRequest.predicate = NSPredicate(format: "myRequestId == %@", reqId)
                let fetchedResults = try managedObjContext.fetch(fetchRequest)
                if let aReq = fetchedResults.first {
                    receivedReq = aReq
                }
            }
            
            if let request = receivedReq {
                return request
            }
            
            if let _signId = signId {
                fetchRequest.predicate = NSPredicate(format: "signId == %@", _signId)
                let fetchedResults = try managedObjContext.fetch(fetchRequest)
                if let aReq = fetchedResults.first {
                    receivedReq = aReq
                }
            }
            
            if let request = receivedReq {
                return request
            }
            else{
                receivedReq = MyRequest(context: managedObjContext)
                receivedReq?.myRequestId = requestId
                receivedReq?.signId = signId
            }
        }
        catch{ }
        
        return receivedReq
    }
    
    public func getMyRequest(requestId: String) -> MyRequest? {
        var receivedReq : MyRequest?
        do {
            let fetchRequest : NSFetchRequest<MyRequest> = MyRequest.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "myRequestId == %@", requestId)
            
            let fetchedResults = try managedObjContext.fetch(fetchRequest)
            if let aDoc = fetchedResults.first {
                receivedReq = aDoc
            }else{
                receivedReq = MyRequest(context: managedObjContext)
            }
            receivedReq?.myRequestId = requestId
        }
        catch{ }
        
        return receivedReq
    }
    
    public func getMyRequest(signId: String) -> MyRequest? {
        var receivedReq : MyRequest?
        do {
            let fetchRequest : NSFetchRequest<MyRequest> = MyRequest.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "signId == %@", signId)
            
            let fetchedResults = try managedObjContext.fetch(fetchRequest)
            if let aDoc = fetchedResults.first {
                receivedReq = aDoc
            }else{
                receivedReq = MyRequest(context: managedObjContext)
            }
            receivedReq?.signId = signId
        }
        catch{ }
        
        return receivedReq!
    }
    
    //MARK: DELETE
    func deleteMyRequests(withRequestIds reqIds : String...)  {
        reqIds.forEach { (reqId) in
            let fetchRequest : NSFetchRequest<MyRequest> = MyRequest.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "myRequestId == %@", reqId)
            
            do{
                let fetchedResults = try managedObjContext.fetch(fetchRequest)
                for object in fetchedResults {
                    managedObjContext.delete(object)
                }
            }
            catch{ }
        }
        
        coreDataDefault.saveContext()
    }
}
