//
//  ZSignPreviewController.swift
//  ZohoSignSDK
//
//  Created by sudhan-6859 on 08/10/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit
import SnapKit

public class ZSignPreviewController: UIViewController {

    private let imageView = UIImageView()
    public var signatureManager : SignatureManager?{
        didSet{
            signatureManager?.rootViewController = self
        }
    }
    
    private lazy var cancelButton : UIButton  = {
        let cancelBtn = UIButton(type: .system)
        cancelBtn.setTitle("sign.mobile.common.close".ZSLString, for: .normal)
        cancelBtn.tintColor = ZColor.whiteColor
        cancelBtn.titleLabel?.font = ZSFont.buttonTitleFont()
        cancelBtn.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
        return cancelBtn
    }()
    
    public convenience init(_ signType : ZSignFieldSignatureType){
        self.init()
        self.signType = signType
    }
    
    private var signType : ZSignFieldSignatureType?
    
    @objc private func cancelButtonPressed(){
         let presentingVC = (self.presentingViewController as? UINavigationController)?.topViewController ?? self.presentingViewController
         self.dismiss(animated: true, completion: {
            self.signatureManager?.rootViewController = presentingVC
         })
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (signType == .initial){
            
            if let img = UserManager.shared.currentUser?.getInitialImage(){
                imageView.image = img
            }
        }else{
            if let img = UserManager.shared.currentUser?.getSignatureImage(){
                imageView.image = img
            }
        }
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)

        self.view.addSubview(imageView)
        imageView.layer.cornerRadius = 10
        imageView.backgroundColor = .white
        imageView.layer.borderColor = UIColor.black.cgColor
        imageView.contentMode = .scaleAspectFit
        imageView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.centerX.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(view.snp.width).dividedBy(278/78)
        }
        
        self.view.addSubview(cancelButton)
        cancelButton.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *){
                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(10)
            }else{
                make.top.equalToSuperview().offset(30)
            }
            make.right.equalToSuperview().offset(-20)
        }
        
        weak var weakself =  self
        
        let subView1 = ZSignPreviewImageView(frame: .zero)
        subView1.setTitle("sign.mobile.profile.signatureWizardDraw".ZSLString, andImage: UIImage(named: "form_attachment"))
        subView1.completionHandler = {
            weakself?.signatureManager?.drawSignature()
        }
        let subView2 = ZSignPreviewImageView(frame: .zero)
        subView2.setTitle("sign.mobile.request.type".ZSLString, andImage: UIImage(named: "form_docName"))
        subView2.completionHandler = {
            weakself?.signatureManager?.selectSignature()
        }
        let subView3 = ZSignPreviewImageView(frame: .zero)
        subView3.setTitle("sign.mobile.common.capture".ZSLString, andImage: UIImage(named: "form_initial"))
        subView3.completionHandler = {
            weakself?.signatureManager?.captureSignature()
        }
        
        let fontPath = Bundle.main.path(forResource: "Fancy-Signature", ofType: "otf")
        
        
        let stackView = UIStackView(arrangedSubviews: [subView1,subView3])
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.view.addSubview(stackView)
    
        if FileManager.default.fileExists(atPath: fontPath!) {
            stackView.insertArrangedSubview(subView2, at: 1)
        }

        
        stackView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-30)
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
            make.height.equalTo(100)
        }
    }
    
}

class ZSignPreviewImageView : UIView{
    
    private lazy var imageView = {
        return UIImageView()
    }()
    
    private lazy var label : UILabel = {
        let lbl =  UILabel()
        lbl.textAlignment = .center
        lbl.textColor = .white
        return lbl
    }()
    
    public var completionHandler : (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialSetup(){
        addSubview(imageView)
        imageView.backgroundColor = UIColor(red: 0.12, green: 0.12, blue: 0.12, alpha: 1)
        imageView.snp.makeConstraints { (make) in
            make.height.equalTo(50)
            make.width.equalTo(50)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview()
        }
        
        addSubview(label)
        label.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(12)
            make.width.equalToSuperview()
            make.centerX.equalToSuperview()
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandle))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc private func tapGestureHandle(){
        completionHandler?()
    }
    
    public func setTitle(_ title : String,andImage image : UIImage?){
        label.text = title
        imageView.image = image
        self.perform({ [weak self] in
            self?.imageView.makeCircular()
        }, afterDelay: 0.2)
    }
    
}

extension UIView {
    func makeCircular() {
        self.layer.cornerRadius = min(self.frame.size.height, self.frame.size.width) / 2.0
        self.clipsToBounds = true
    }
}
