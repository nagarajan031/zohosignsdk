//
//  SignatureFiltersView.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 29/11/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

enum SignatureFilters {
    case original
    case filtered
    case backgroundRemoved
}

protocol SignatureFiltersViewDelegate: NSObjectProtocol {
    func imageDidSelected(_ image: UIImage,withFilteredType type: SignatureFilters)
}

class SignatureFiltersView: UIView {
    private var originalImage: UIImage {
        willSet {
            filteredImage = SwiftUtils.generateContrastImage(5.0, newValue.image(withBrightness: 0.4) ?? newValue)// SwiftUtils.generateContrastImage(5.0, newValue.withContrast(1.0, brightness: 0.4))
            
            let imageProcessor = ZSSignatureImageProcessor()
            imageProcessor.image = self.filteredImage
            imageProcessor.removeBackground(brightnessFactor: 1.0, thresshold: 100) {[weak self] image in
                self?.backgroundRemovedImage = image
            }
        }
    }

    private var filteredImage: UIImage!
    private var backgroundRemovedImage: UIImage!
    
    private lazy var stackView: UIStackView = {
       let view = UIStackView()
        view.axis = DeviceType.isMac ? .horizontal : .vertical
        view.distribution = .fillEqually
        view.spacing = 20
        return view
    }()
    
    private lazy var stackViewItemBackgroundColors: [(UIColor,UIImage?)] = {
        return [
            (ZColor.bgColorWhite, originalImage),
            (ZColor.bgColorWhite, filteredImage),
            (UIColor(patternImage: UIImage(named: "empty_background")!), backgroundRemovedImage)
        ]
    }()
    
    private let filterNames: [String] = [
        "Original",
        "Black & White",
        "Transparent"
    ]
    
    private var selectedImageButton: SignatureImageFiltersButton?
    
    public weak var delegate: SignatureFiltersViewDelegate?
    
    init(withOriginalImage originalImage: UIImage,delegate: SignatureFiltersViewDelegate) {
        self.originalImage = originalImage
        self.delegate = delegate
        super.init(frame: .zero)
        
        self.filteredImage = SwiftUtils.generateContrastImage(5.0, originalImage.image(withBrightness: 0.4) ?? originalImage)//SwiftUtils.generateContrastImage(5.0, originalImage.withContrast(1.0, brightness: 0.4))
        let imageProcessor = ZSSignatureImageProcessor()
        imageProcessor.image = self.filteredImage
        imageProcessor.removeBackground(brightnessFactor: 1.0, thresshold: 100) {[weak self] image in
            self?.backgroundRemovedImage = image
            (self?.stackView.arrangedSubviews.last?.subviews.first(where: {$0 is SignatureImageFiltersButton}) as? SignatureImageFiltersButton)?.setBackgroundImage(image, for: .normal)
        }
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        for (index,color) in stackViewItemBackgroundColors.enumerated() {
            
            let imageButton = SignatureImageFiltersButton(filterType: index == 0 ? .original : index == 1 ? .filtered : .backgroundRemoved)
            imageButton.backgroundColor = color.0
            imageButton.setBackgroundImage(color.1, for: .normal)
            imageButton.addTarget(self, action: #selector(imageClicked(_:)), for: .touchUpInside)
            
            let filterNameLabel = UILabel()
            filterNameLabel.text = filterNames[index]
            filterNameLabel.font = ZSFont.mediumFont(withSize: 14)
            filterNameLabel.textColor = ZColor.whiteColor
            filterNameLabel.backgroundColor = ZColor.bgColorDullBlack
            filterNameLabel.adjustsFontSizeToFitWidth = true
            filterNameLabel.textAlignment = .center
            
            let containerView = UIView()
            containerView.addSubviews(imageButton, filterNameLabel)
            
            imageButton.snp.makeConstraints { (make) in
                make.top.leading.trailing.equalToSuperview()
                make.height.equalToSuperview().multipliedBy(0.75)
            }
            
            filterNameLabel.snp.makeConstraints { (make) in
                make.bottom.leading.trailing.equalToSuperview()
                make.height.equalToSuperview().multipliedBy(0.25)
            }
            
            stackView.addArrangedSubview(containerView)
            if index == 0 {
                imageButton.setSelected(isSelected: true)
                selectedImageButton = imageButton
            }
        }
        
        addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    @objc func imageClicked(_ sender: SignatureImageFiltersButton) {
        selectedImageButton?.setSelected(isSelected: false)
        sender.setSelected(isSelected: true)
        selectedImageButton = sender
        var image = UIImage()
        switch sender.filterType {
        case .original:
            image = originalImage
        case .filtered:
            image = filteredImage
        case .backgroundRemoved:
            image = backgroundRemovedImage
        default:
            break
        }
        delegate?.imageDidSelected(image,withFilteredType: sender.filterType)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class SignatureImageFiltersButton: ZSButton {
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        if #available(iOS 13, *){
            view.style = .large
            view.color = ZColor.whiteColor
        } else {
            view.style = .whiteLarge
        }
        view.startAnimating()
        
        view.hidesWhenStopped = true
        return view
    }()
    
    private lazy var tickImageView: UIImageView = {
        let view = UIImageView(image: ZSImage.tintColorImage(withNamed: "check_tick", col: ZColor.whiteColor))
        view.contentMode = .scaleAspectFit
        view.alpha = 0
        view.backgroundColor = .clear
        return view
    }()
    
    private var overLayView: UIView!
    
    public var filterType: SignatureFilters = .original
    
    init(filterType: SignatureFilters) {
        self.filterType = filterType
        super.init(frame: .zero)
        overLayView = UIView()
        overLayView.backgroundColor = ZColor.bgColorDullBlack
        overLayView.addSubviews(tickImageView,activityIndicator)
        
        tickImageView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalTo(DeviceType.isMac ? 50 : 30)
        }
        
        activityIndicator.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        
        addSubview(overLayView)
        overLayView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        isUserInteractionEnabled = false
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setBackgroundImage(_ image: UIImage?, for state: UIControl.State) {
        super.setBackgroundImage(image, for: .normal)
        if image != nil {
            activityIndicator.stopAnimating()
            setSelected(isSelected: false)
            isUserInteractionEnabled = true
        }
    }
    
    func setSelected(isSelected: Bool){
        self.overLayView.alpha = isSelected.toNumber()
        self.tickImageView.alpha = isSelected.toNumber()
    }
}
