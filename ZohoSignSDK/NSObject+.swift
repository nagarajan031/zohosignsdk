//
//  NSObject+.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 24/12/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import Foundation

public extension NSObject {
    func pushGraphicsContextIntoStack(forSize size: CGSize) -> CGContext{
        let width = Int(size.width)
        let height = Int(size.height)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bytesPerPixel = 4
        let bytesPerRow = bytesPerPixel * width
        let bitsPerComponent = 8
        let bitmapByteCount = bytesPerRow * height
        let dataPointer:UnsafeMutableRawPointer = calloc(bitmapByteCount, 1)!
        
        let context = CGContext(data: dataPointer, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue|CGBitmapInfo.byteOrder32Big.rawValue)
        UIGraphicsPushContext(context!)
        return context!
    }
    
    @available(iOS 13,*)
    func dismissWindow(with windowDismissalAnimation: UIWindowScene.DismissalAnimation,session : UISceneSession){
        
        let options = UIWindowSceneDestructionRequestOptions()
        options.windowDismissalAnimation = windowDismissalAnimation
        UIApplication.shared.requestSceneSessionDestruction(session, options: options) { (error) in
            ZError( "Drag and Drop window dismissal error" + error.localizedDescription)
        }
    }
    
    
    func perform(_ block: @escaping () -> Void, afterDelay delay: TimeInterval) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            block()
        }
    }
}
