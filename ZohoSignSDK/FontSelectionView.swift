//
//  FontSelectionView.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 28/11/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit


public protocol FontSelectionViewDelegate: NSObjectProtocol {
    func fontFamilyDidChanged(withFontFamily fontFamily: String)
}

public class FontSelectionView: UIControl {
    public let fontsNameArray = [
        "Roboto",
        "Liberation Serif",
        "DejaVu Sans",
        "Heuristica",
        "TexGyreCursor"
    ]
    
    private lazy var fontsArray: [UIFont] = {
        return TextFonts.allCases.map({UIFont(name: $0.rawValue, size: 17)}).compactMap({$0})
    }()
    
    public lazy var fontsScrollview: UIScrollView = {
        let view = UIScrollView()
        view.alwaysBounceHorizontal = true
        view.alwaysBounceVertical = false
        view.clipsToBounds = true
        view.backgroundColor = .clear
        view.delegate = self
        view.showsHorizontalScrollIndicator = false
        return view
    }()
    
    private lazy var fontsScrollViewSelectionView:  UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        view.backgroundColor = ZColor.buttonColor
        view.clipsToBounds = true
        return view
    }()
    
    private var showdowColor: UIColor {
        if #available(iOS 13, *){
            return .setAppearance(dark: .secondarySystemBackground, light: .systemGroupedBackground)
        } else {
            return ZColor.bgColorGray
        }
    }
    
    private var selectedFontFamily: String{
        willSet {
            delegate?.fontFamilyDidChanged(withFontFamily: newValue)
        }
    }
    
    private var selectedLabelColor: UIColor{
        return .setColor(dark: ZColor.primaryTextColor, light: ZColor.whiteColor)
    }
    
    private var unSelectedLabelColor: UIColor {
        return ZColor.primaryTextColor
    }
    
    private var selectedButton: ZSButton?
    private var scrollViewLeftShadow: UIView!
    private var scrollViewRightShadow: UIView!
    
    private let scrollViewContentheight: CGFloat = 55
    private let scrollViewItemSpacing: CGFloat = 10
    private let scrollViewItemPadding: CGFloat = 20
    public var isViewAppeared: Bool = false
    
    public weak var delegate: FontSelectionViewDelegate?
    
    init(selectedFontFamily: String) {
        self.selectedFontFamily = selectedFontFamily
        super.init(frame: .zero)
        scrollViewLeftShadow = UIView()
        scrollViewLeftShadow.isUserInteractionEnabled = false
        scrollViewRightShadow = UIView()
        scrollViewRightShadow.isUserInteractionEnabled = false
    }
    
    override public func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        var scrollViewContentWidth: CGFloat = 0
        for (index,font) in fontsArray.enumerated() {
            let fontButton = ZSButton()
            fontButton.setTitle(fontsNameArray[index], for: .normal)
            fontButton.titleLabel?.font = font
            fontButton.titleLabel?.textAlignment = .center
            fontButton.tag = index
            fontButton.setTitleColor(unSelectedLabelColor, for: .normal)
            
//            let labelWidth1 = GlobalFunctions.size(forText: fontButton.titleLabel?.text!, sizeWith: font, constrainedTo: CGSize(width: frame.width, height: scrollViewContentheight)).width + scrollViewItemPadding
            let labelWidth = (fontButton.titleLabel?.text?.textSize(withConstrainedSize: CGSize(width: frame.width, height: scrollViewContentheight), font: font).width ?? 0) + scrollViewItemPadding
            
            fontButton.frame = CGRect(x: scrollViewContentWidth, y: 0, width: labelWidth, height: scrollViewContentheight)
            fontButton.layer.cornerRadius = 5
            fontButton.addTarget(self, action: #selector(fontButtonClicked(_:)), for: .touchUpInside)
            scrollViewContentWidth += labelWidth + scrollViewItemSpacing
            fontsScrollview.addSubview(fontButton)
            
            if selectedFontFamily == fontsNameArray[index] {
                selectedButton = fontButton
                fontButtonClicked(fontButton)
            }
        }
        
        fontsScrollview.addSubviews(fontsScrollViewSelectionView)
        fontsScrollview.sendSubviewToBack(fontsScrollViewSelectionView)
        fontsScrollview.contentSize = CGSize(width: scrollViewContentWidth, height: scrollViewContentheight)
        
        addSubviews(fontsScrollview,scrollViewLeftShadow,scrollViewRightShadow)
        
        fontsScrollview.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        scrollViewLeftShadow.snp.makeConstraints { (make) in
            make.leading.top.bottom.equalToSuperview()
            make.width.equalTo(30)
        }
        
        scrollViewRightShadow.snp.makeConstraints { (make) in
            make.trailing.top.equalToSuperview()
            make.size.equalTo(scrollViewLeftShadow)
        }
    }
    
    @objc public func fontButtonClicked(_ sender: ZSButton){
        let senderFrame = sender.frame
        /*
        let senderFrameRelativeToScrollview = senderFrame.origin.x - fontsScrollview.contentOffset.x
        let centerX = fontsScrollview.frame.width/2 - senderFrame.width/2
        */
        let pointToMove = senderFrame.origin.x - 60
        
        ///change content offset if subview can be moved manually.
        if pointToMove > 0 && abs(pointToMove - fontsScrollview.contentSize.width) >= fontsScrollview.frame.width{
            self.fontsScrollview.setContentOffset(CGPoint(x: pointToMove, y: 0), animated: true)
        } else {
            ///else let scrollview handle it.
            self.fontsScrollview.scrollRectToVisible(senderFrame, animated: true)
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.4, options: [.curveEaseOut,.beginFromCurrentState], animations: {
            self.fontsScrollViewSelectionView.frame = senderFrame
            self.selectedButton?.setTitleColor(self.unSelectedLabelColor, for: .normal)
        }) {_ in
            self.selectedFontFamily = sender.titleLabel!.text!
        }
        
        if sender.tag == 0 {
            removeSubLayers(forView: scrollViewLeftShadow)
        } else if sender.tag == fontsArray.count - 1 {
            perform({
                self.removeSubLayers(forView: self.scrollViewRightShadow)
            }, afterDelay: 0.3)
        }
        sender.setTitleColor(selectedLabelColor, for: .normal)
        selectedButton = sender
    }
    
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection){
            removeSubLayers(forView: scrollViewLeftShadow)
            removeSubLayers(forView: scrollViewRightShadow)
            perform({
                self.applyfadeViewToScrollView(forContentOffSet: self.fontsScrollview.contentOffset)
            }, afterDelay: traitCollection.userInterfaceStyle == .dark ? 0.4 : 0.0)
            fontsScrollview.subviews.forEach({ button in
                if let button = button as? ZSButton {
                    button.setTitleColor(selectedFontFamily == TextFonts.allCasesRawValue[button.tag] ? selectedLabelColor : unSelectedLabelColor, for: .normal)
                }
            })
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension FontSelectionView: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if isViewAppeared {
            applyfadeViewToScrollView(forContentOffSet: scrollView.contentOffset)
        }
    }
    
    func applyfadeViewToScrollView(forContentOffSet contentOffset: CGPoint){
        let gradientLayer = CAGradientLayer()
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        if contentOffset.x > 0 {
            if scrollViewLeftShadow.layer.sublayers == nil {
                gradientLayer.frame = CGRect(x: 0, y: 0, width: 30, height: scrollViewContentheight)
                gradientLayer.colors = [showdowColor.cgColor,showdowColor.cgColor,showdowColor.withAlphaComponent(0.4).cgColor, showdowColor.withAlphaComponent(0.0).cgColor]
                scrollViewLeftShadow.layer.addSublayer(gradientLayer)
                scrollViewLeftShadow.backgroundColor = .clear
            }
        } else {
            removeSubLayers(forView: scrollViewLeftShadow)
        }
        
        if contentOffset.x + fontsScrollview.frame.width < fontsScrollview.contentSize.width {
            if scrollViewRightShadow.layer.sublayers == nil, scrollViewRightShadow.bounds != .zero {
                gradientLayer.frame = scrollViewRightShadow.bounds
                gradientLayer.colors = [showdowColor.withAlphaComponent(0.0).cgColor, showdowColor.withAlphaComponent(0.4).cgColor, showdowColor.cgColor, showdowColor.cgColor]
                scrollViewRightShadow.layer.addSublayer(gradientLayer)
                scrollViewRightShadow.backgroundColor = .clear
            }
        } else {
            removeSubLayers(forView: scrollViewRightShadow)
        }
    }
    
    func removeSubLayers(forView view: UIView) {
        view.layer.sublayers?.forEach({$0.removeFromSuperlayer()})
        view.layer.sublayers = nil
    }
}
