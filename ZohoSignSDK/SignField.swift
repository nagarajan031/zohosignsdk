////
////  SignField.swift
////  ZohoSignSDK
////
////  Created by Nagarajan S on 13/11/19.
////  Copyright © 2019 Zoho Corporation. All rights reserved.
////
//
import Foundation


public class SignField : Equatable{
    public static func == (lhs: SignField, rhs: SignField) -> Bool {
        if lhs.fieldId != nil && lhs.fieldId == rhs.fieldId{
            return true
        }
        if lhs.addedTimeStamp > 0 &&  lhs.addedTimeStamp == rhs.addedTimeStamp{
            return true
        }
        return false
    }
    
    public var type: ZSignFieldType?
    public var subType: ZSignFieldSubType?
    
    public var addedTimeStamp: Int64 = 0 //To validate fields

    //TextField
//    var fontFamily: String?
//    var isBold = false
//    var isItalic = false
//    var isUnderline = false
    public var currentTextProperty = TextProperty(fontColor: .black, fontFamily: TextFonts.roboto.rawValue , fontSize: 8, isBold: false, isItalics: false, isUnderline: false)

    public var fieldId: String?
    public var fieldName: String?
    public var fieldLabel: String?
    public var fieldTypeId: String?
    public var fieldTypeName: String?
    
  
    public var docId: String?

    public var recipientName: String?
    public var recipientIndex = 0

    public var frame = CGRect.zero
    
    public var isCompleted = false
    public var isMandatory = false
    public var isReadOnly = false
    public var isOtherRecipientField = false
//    var fontSize = 0
    public var pageIndex = 0
    public var serverFontSize = 0
    public var xValue: CGFloat = 0.0
    public var yValue: CGFloat = 0.0
    public var width: CGFloat = 0.0
    public var height: CGFloat = 0.0
    public var minWidth: CGFloat = 0.0
    public var minHeight: CGFloat = 0.0
    public var signImage: UIImage?
    public var textValue: String?
    public var dateFormat: String?
    public var dateValue: String?
    public var isCheckboxTicked = false
    public var dropdownFields: [AnyHashable]?
    public var defaultValue: String?
    public var radioGroup: RadioGroup?{
        willSet {
            fieldName = newValue?.name
            isMandatory = !(newValue?.isOptional ?? false)
            isReadOnly = newValue?.isReadOnly ?? false
        }
    }
 //dropdown default value and textfield default value
    
    public weak var parentView: ResizableView?

    
    func nameFormat() -> String {
        if subType == .firstName {
            return "FIRST_NAME"
        } else if subType == .lastName {
            return "LAST_NAME"
        } else {
            return "FULL_NAME"
        }
    }

 
    //weak parent reference
    func getTextWithFieldParams(for str: String?) -> NSAttributedString {
        
        let fontName : String =  currentTextProperty.fontFamily
        
        let fontSize = currentTextProperty.fontSize
        let textColor =  currentTextProperty.fontColor
        
        var attributesDict: [NSAttributedString.Key : Any] = [:]
        attributesDict[.font] =  UIFont(name: FontManager.shared.getFontName(withFontFamily: currentTextProperty.fontFamily, boldTrait: currentTextProperty.isBold, italicTrait: currentTextProperty.isItalics), size: CGFloat(fontSize))

        if currentTextProperty.isUnderline ?? false {
            attributesDict[.underlineStyle] = NSNumber(value: NSUnderlineStyle.single.rawValue)
        }
        
        
        attributesDict[.foregroundColor] = textColor
        
        
        let attrString = NSAttributedString(string: ((str == nil) ? "" : str) ?? "", attributes: attributesDict)
        
        return attrString
    }

    func adjustedAttributesString(_ str: String, toFillWidth width: CGFloat) -> NSAttributedString {

        var fontRatio : CGFloat = 1
        if let imgView = (parentView?.superview) as? DocumentEditorBaseView{
            fontRatio = FontManager.fieldFontRatio(imgView.frame.size.width , imageWidth: imgView.pdfPageSize.width)
        }
        
        let fontName = FontManager.shared.getFontName(withFontFamily: currentTextProperty.fontFamily, boldTrait: currentTextProperty.isBold, italicTrait: currentTextProperty.isItalics)

        var largestFontSize = Int(roundf(Float(99 * fontRatio)))

        if let font = UIFont(name: fontName, size: CGFloat(largestFontSize)) {
            while (str.size(withAttributes: [
            NSAttributedString.Key.font: font
            ]).width ?? 0.0) > (width - 5) {
                largestFontSize -= 1
                if largestFontSize == 8 {
                    break
                }
            }
        }

        currentTextProperty.fontSize = largestFontSize

        let imgView1 = (parentView?.superview) as? UIImageView

        serverFontSize = FontManager.calibaratedServerFontSize(forDeviceFontSize: largestFontSize, pageWidth: imgView1?.frame.width ?? 1, imageWidth: (parentView as? ZResizeView)?.pdfPageSize.width ?? 1)

        ZInfo("serverFontSize %d", serverFontSize)
        return getTextWithFieldParams(for: str)
    }

    func adjustedAttributedString(_ str: String?, toFillRect newBounds: CGRect) -> NSAttributedString {
        guard let str = str else {
              return getTextWithFieldParams(for: "")
        }
        
        let imgView = (parentView?.superview) as? UIImageView
        
        var fontRatio : CGFloat = 1
        if let imgView = (parentView?.superview) as? DocumentEditorBaseView{
            fontRatio = FontManager.fieldFontRatio(imgView.frame.size.width , imageWidth: imgView.pdfPageSize.width)
        }
        
        let fontName = FontManager.shared.getFontName(withFontFamily: currentTextProperty.fontFamily, boldTrait: currentTextProperty.isBold, italicTrait: currentTextProperty.isItalics)

        var largestFontSize = Int(roundf(Float(99 * fontRatio)))

        var i = largestFontSize
        while i >= 8 {

            largestFontSize = i
            var labelSize: CGSize? = nil
            if let font = UIFont(name: fontName, size: CGFloat(i)) {
                labelSize = str.size(withAttributes: [
                NSAttributedString.Key.font: font
            ])
            }
            if ((labelSize?.height ?? 0.0) < (newBounds.size.height - 5)) && ((labelSize?.width ?? 0.0) < (newBounds.size.width - 5)) {
                break
            }
            i -= 1
        }
        currentTextProperty.fontSize = largestFontSize

        return getTextWithFieldParams(for: str)
    }

    public init() {}
}

public class ResizableView: UIView {
    public var currentEditingType : ZSDocumentViewMode = .receivedDoc_view
    public var resizeViewMode: ZResizeViewMode = .sentDoc_selfSign
    public var isDisableResizing = false
    
    public var signField: SignField!
    
    public var  outerMargin  : CGFloat   =    12
    public var  totalMargin  : CGFloat   =    24
    
    let grayFieldBorderColor = UIColor(red: 0.68, green: 0.70, blue: 0.71, alpha: 1)
    let grayFieldBgColor = UIColor(red: 0.93, green: 0.93, blue: 0.94, alpha: 1.00)


    let greenFieldBorderColor = UIColor(red: 0.28, green: 0.79, blue: 0.55, alpha: 1)
    let greenFieldBgColor = UIColor(red: 0.89, green: 0.97, blue: 0.93, alpha: 0.6)

    let redFieldBgColor = UIColor(red: 0.99, green: 0.92, blue: 0.91, alpha: 0.85)
    let redFieldBorderColor = UIColor(red: 0.91, green: 0.42, blue: 0.36, alpha: 1)
    let redFieldDarkBgColor = UIColor(red: 0.95, green: 0.67, blue: 0.63, alpha: 1)
    
    public func setIsHighlighted(_ isHighlighted: Bool) { }
    @objc public func hideEditingHandles() { }
    
    func setResizeViewMode(isPreviewMode: Bool,isFinalPreviewMode: Bool) {
        if currentEditingType == .sentDoc_Create {
            if isPreviewMode {
                resizeViewMode = .receivedDoc_preview
            } else {
                resizeViewMode = .sentDoc_Create
            }
        } else if (currentEditingType == .sentDoc_selfSign) && !isFinalPreviewMode {
            resizeViewMode = .sentDoc_selfSign
        } else if isFinalPreviewMode || (currentEditingType == .receivedDoc_view) {
            resizeViewMode = .receivedDoc_completed
        } else if currentEditingType == .receivedDoc_sign {
            if isPreviewMode {
                resizeViewMode = .receivedDoc_preview
            } else if signField.isCompleted {
                resizeViewMode = .receivedDoc_completed
            } else {
                resizeViewMode = .receivedDoc_pending
            }
        }
    }
}
