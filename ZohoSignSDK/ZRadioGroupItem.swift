//
//  ZRadioGroupItem.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 24/01/20.
//  Copyright © 2020 Zoho Corporation. All rights reserved.
//

import UIKit

public protocol ZRadioGroupItemDelegate: class {
    func didBeginDragging(onItem radioItem: ZRadioGroupItem)
    func didChangeDragging(onItem radioItem: ZRadioGroupItem)
    func didEndDragging(onItem radioItem: ZRadioGroupItem)
    func didTapped(onItem radioItem: ZRadioGroupItem)
    func didTappedEditButton(onItem radioItem: ZRadioGroupItem)
}

public extension ZRadioGroupViewDelegate {
    func didBeginDragging(onItem radioItem: ZRadioGroupItem) {}
    func didChangeDragging(onItem radioItem: ZRadioGroupItem) {}
    func didEndDragging(onItem radioItem: ZRadioGroupItem) {}
    func didTapped(onItem radioItem: ZRadioGroupItem) {}
    func didTappedEditButton(onItem radioItem: ZRadioGroupItem) {}
}

public struct RadioValues: Equatable {
    public var name: String
    public var rect: CGRect?
    public var xValue: CGFloat = 0
    public var yValue: CGFloat = 0
    public var width: CGFloat = 0
    public var height: CGFloat = 0
    public var pageNo: Int?
    public var subFieldId: String?
    public var defaultValue: Bool = false
    public init(name: String) {
        self.name = name
    }
    public static func == (lhs: Self, rhs:Self) -> Bool{
        return lhs.name == rhs.name
    }
    
    public var normalisedFrame: CGRect {
        return CGRect(x: xValue, y: yValue, width: width, height: height)
    }
    
    public func getActualFrame(inBounds bounds: CGRect) -> CGRect {
        return CGRect(x: ((xValue / 100) * bounds.width), y: (yValue / 100) * bounds.height, width: (width / 100) * (bounds.width), height: (height / 100) * bounds.height)
    }
    
    public func actualViewFrameFromRect(outerMargin: CGFloat, size: CGFloat) -> CGRect {
        return CGRect(x: rect!.origin.x-outerMargin, y: rect!.origin.y-outerMargin, width: size, height: size)
    }
    
    mutating public func setRect(fromActualViewFrame frame: CGRect, outerMargin: CGFloat) {
        self.rect = CGRect(x: frame.origin.x+outerMargin, y: frame.origin.y+outerMargin, width: frame.width-2*outerMargin, height: frame.height-2*outerMargin)
    }
    
    mutating func setRect(inBounds bounds: CGRect) {
        self.rect = getActualFrame(inBounds: bounds)
    }
}

public class ZRadioGroupItem : UIView{
    
    var radioValue: RadioValues
    var resizeMode: ZResizeViewMode {
        willSet {
            if newValue == .sentDoc_Create {
                radioButton.isSelected = radioValue.defaultValue
            }
        }
    }
    private var totalMargin: CGFloat = 24
    lazy var radioButton: ZRadioButton = {
        let view  = ZRadioButton()
        view.outerCircleViewBackgroundColor = ZColor.whiteColor
        view.clipsToBounds = true
        view.isUserInteractionEnabled = false
        return view
    }()
    
    private lazy var normalizedOrigin: CGPoint = {
        let point = superview?.convert(CGPoint.zero, to: superview?.superview) ?? .zero
        return CGPoint(x: -point.x, y: -point.y)
    }()
    
    var dragView: UIView!
    public weak var delegate: ZRadioGroupItemDelegate?
    
    public init(frame: CGRect, value: RadioValues,outerMargin: CGFloat,resizeMode: ZResizeViewMode) {
        self.radioValue = value
        self.totalMargin = outerMargin*2
        self.resizeMode = resizeMode
        super.init(frame: frame)
        setupDefaultAttributes()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    deinit {
        ZInfo("deinit")
    }
    
    public override var frame: CGRect{
        willSet {
            radioValue.setRect(fromActualViewFrame: newValue, outerMargin: totalMargin/2)
        }
    }
    
    private func setupDefaultAttributes() {
        isUserInteractionEnabled = true
        clipsToBounds = false
        layer.cornerRadius = frame.height*0.1
        
        dragView = UIView()
        dragView.isUserInteractionEnabled = true
        addSubview(dragView)
        
        dragView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.width.equalToSuperview().multipliedBy(2)
        }
        
        addSubview(radioButton)
        radioButton.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.width.equalTo(self).offset(-totalMargin)
        }
        
        
        let menuController = UIMenuController.shared
        if #available(iOS 13.0, *) {
            menuController.hideMenu()
            if (SwiftUtils.isMac){
                addInteraction(UIContextMenuInteraction(delegate: self))
            }
        } else {
            menuController.setMenuVisible(true, animated: false)
            menuController.setMenuVisible(false, animated: false)
        }
        
        let moveGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureTriggered(_:)))
        moveGesture.delegate = self
        dragView.addGestureRecognizer(moveGesture)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureTriggered))
        tapGesture.delegate = self
        dragView.addGestureRecognizer(tapGesture)
    }
    
    public override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return dragView.frame.contains(point)
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if resizeMode == .sentDoc_Create {
            delegate?.didBeginDragging(onItem: self)
        }
    }
    
    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        radioButton.layer.cornerRadius = radioButton.frame.height/2
    }

    private func showMenu() {
        if SwiftUtils.isMac, resizeMode != .sentDoc_Create {
            return
        }
        
        weak var weakSelf = self
        let deleteMenu = UIMenuItem(title: "sign.mobile.common.delete".ZSLString, image: ZSImage.tintColorImage(withNamed: "field_delete_icon", col: ZColor.whiteColor))  { (item) in
            weakSelf?.deleteButtonClicked()
        }
        
        let editMenu = UIMenuItem(title: "sign.mobile.common.edit".ZSLString, image: nil) { (item) in
            weakSelf?.editButtonClicked()
        }
        hideMenu()
        UIMenuController.shared.menuItems = [editMenu]
        UIMenuController.shared.update()
        
        if #available(iOS 13.0, *) {
            UIMenuController.shared.showMenu(from: self, rect: bounds)
        } else {
            UIMenuController.shared.setTargetRect(bounds, in: self)
            UIMenuController.shared.setMenuVisible(true, animated: true)
        }
    }
    
    private func hideMenu() {
        if #available(iOS 13.0, *) {
            UIMenuController.shared.hideMenu()
        } else {
            // Fallback on earlier versions
            UIMenuController.shared.setMenuVisible(false, animated: true)
        }
    }
    
    private func deleteButtonClicked() {
        //TODO: implement delete
    }
    
    private func editButtonClicked() {
        delegate?.didTappedEditButton(onItem: self)
    }
    
    public func setSelected(_ isSelected: Bool) {
        radioValue.defaultValue = isSelected
        radioButton.isSelected = isSelected
    }
}

extension ZRadioGroupItem: UIGestureRecognizerDelegate {
    
    public override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if resizeMode == .receivedDoc_preview {
            return false
        }
        
        if gestureRecognizer is UIPanGestureRecognizer {
            return resizeMode == .sentDoc_Create
        } else if gestureRecognizer is UITapGestureRecognizer {
            return resizeMode != .receivedDoc_completed
        }
        
        return true
    }
    
    @objc private func tapGestureTriggered() {
        guard resizeMode != .receivedDoc_preview else {
            return
        }
        
        if let parentView = delegate as? UIView, let rootView = parentView.superview {
            if let parentViewController = rootView.parentViewController {
                NSObject.cancelPreviousPerformRequests(withTarget: parentViewController)
            }
        }
        
        delegate?.didTapped(onItem: self)
        
        if resizeMode == .sentDoc_Create {
            showMenu()
        }
    }
    
    @objc private func panGestureTriggered(_ sender: UIPanGestureRecognizer) {
        guard let superViewFrame = superview?.superview?.frame else {return}
        
        switch sender.state {
        case .began:
            delegate?.didBeginDragging(onItem: self)
        case .changed:
            let newCenter: CGPoint = sender.location(in: superview)
            let centerX = min( max(newCenter.x, normalizedOrigin.x + frame.width/2), superViewFrame.maxX - frame.width/2 + normalizedOrigin.x )
            
            let centerY = min( max(newCenter.y, normalizedOrigin.y + frame.height/2), superViewFrame.maxY - frame.height/2 + normalizedOrigin.y )
            center = CGPoint(x: centerX, y: centerY)
            delegate?.didChangeDragging(onItem: self)
        case .ended,.cancelled,.failed:
            delegate?.didEndDragging(onItem: self)
        default:
            break
        }
        
    }
}

@available(iOS 13, *)
extension ZRadioGroupItem: UIContextMenuInteractionDelegate {
    @available(iOS 13.0, *)
    public func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { (menu) -> UIMenu? in
            weak var weakSelf = self

            guard weakSelf?.resizeMode == .sentDoc_Create else {
                return nil
            }
            let editAction = UIAction(title: "sign.mobile.common.edit".ZSLString, image: nil ,identifier: nil){ action in
                weakSelf?.editButtonClicked()
            }
            let deleteAction = UIAction(title: "sign.mobile.common.delete".ZSLString, image: nil ,identifier: nil){ action in
                weakSelf?.deleteButtonClicked()
            }
            return UIMenu(__title: "", image: nil, identifier: nil, children: [editAction])
        }
    }
}
