//
//  ZSDownloadProgressView.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 04/04/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit
let _sharedDownloadProgressView = ZSDownloadProgressView()

public class ZSDownloadProgressView: UIView {

    let progessIndicatorView      = UIView()//indicator bg
    let progessIndicatorValueView = UIView()//indicator top
    public let titleLabel                = UILabel()

    let progressLabelWidth = 280
    // the sharedInstance class method can be reached from ObjC. (From OP's answer.)
    public class func sharedInstance() -> ZSDownloadProgressView {
        return _sharedDownloadProgressView
    }
    
    // MARK: - Private
    @objc public init() {
        super.init(frame: CGRect.zero)
        initateSetup()
        return;
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initateSetup()  {
        self.backgroundColor       =    ZColor.bgColorGray
        
        titleLabel.numberOfLines = 0
        titleLabel.frame =   CGRect(x: 16, y: 16, width: self.frame.size.width - 32, height: 22)
        titleLabel.autoresizingMask = [.flexibleWidth]
        titleLabel.font = ZSFont.createListValueFont()
        titleLabel.textAlignment = .center
        titleLabel.text = "sign.mobile.common.loading".ZSLString
        titleLabel.textColor = ZColor.secondaryTextColorDark
        titleLabel.clipsToBounds  = false;
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(titleLabel)
        
        let centerConstraint = NSLayoutConstraint(item:  titleLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 7)
        
        let leftConstraint = NSLayoutConstraint(item: titleLabel, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 16)
        
        let rightConstraint = NSLayoutConstraint(item:  titleLabel, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -16)
        let heightConstraint = NSLayoutConstraint(item:  titleLabel, attribute: .height, relatedBy: .equal, toItem: nil , attribute: .notAnAttribute, multiplier: 1, constant: 20)
        
        self.addConstraints([centerConstraint, leftConstraint, rightConstraint,heightConstraint])
        

        
        progessIndicatorView.layer.cornerRadius = 1.2
        progessIndicatorView.clipsToBounds  =   true
        progessIndicatorView.backgroundColor    =   ZColor.seperatorColor
        progessIndicatorValueView.backgroundColor = ZColor.buttonColor
        progessIndicatorValueView.layer.cornerRadius = 1.2
        self.addSubview(progessIndicatorView)
        progessIndicatorView.addSubview(progessIndicatorValueView)
        
        progessIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        let centerConstraint1 = NSLayoutConstraint(item:  progessIndicatorView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: -17)
        
        let centerXConstraint1 = NSLayoutConstraint(item: progessIndicatorView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        
        let widthConstraint1 = NSLayoutConstraint(item:  progessIndicatorView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 260)
        let heightConstraint1 = NSLayoutConstraint(item:  progessIndicatorView, attribute: .height, relatedBy: .equal, toItem: nil , attribute: .notAnAttribute, multiplier: 1, constant: 2.4)
        
        self.addConstraints([centerConstraint1, centerXConstraint1, widthConstraint1,heightConstraint1])
    }
    
    @objc public func show(inView: UIView, progress : CGFloat) {
        if (self.superview != nil) {self.removeFromSuperview()}
        self.alpha                =     1

        titleLabel.text = "sign.mobile.common.loading".ZSLString
        self.frame = inView.bounds
        self.autoresizingMask      =   [.flexibleWidth, .flexibleHeight]

        inView.addSubview(self)
        
        self.updateConstraints()
        self.layoutIfNeeded()
        self.progessIndicatorValueView.frame = CGRect(x: 0, y: 0, width:  0, height: 3)
    }
    
    @objc public func dismiss()
    {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.alpha                =     0
        }) { (completed) in
            self.removeFromSuperview()
        }
    }
    
    @objc public func setProgress(_ progress : CGFloat)
    {
        DispatchQueue.main.async {
            UIView.animateKeyframes(withDuration: 0.1, delay: 0, options: .beginFromCurrentState, animations: {
                self.progessIndicatorValueView.frame = CGRect(x: 0, y: 0, width:  self.progessIndicatorView.frame.size.width * progress, height: 3)
            }, completion: nil)
        }
    }
}
