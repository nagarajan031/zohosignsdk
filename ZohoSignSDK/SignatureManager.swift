//
//  SignatureManager.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 06/11/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit
import MobileCoreServices

@objc public protocol SignatureManagerDelegate: NSObjectProtocol {
    func didSaveIntial(initialImage: UIImage)
    func didSaveSignature(signImage: UIImage)
    @objc optional func didCancelled()
}

@objc public protocol SignatureCreatorDelegate: NSObjectProtocol {
    @objc func signatureCreated(signImage: UIImage)
}

//public enum SignatureType {
//    case initial
//    case signature
//}

public let defaultSignatureHeight =  78.0
public let defaultSignatureWidth  =  278.0

public class SignatureManager: NSObject {
    
    public weak var rootViewController: UIViewController?
    public var signatureType: ZSignFieldSignatureType
    
    public var isIncludeSelectSignatureStyle: Bool = false
    public var isTemporary: Bool = false
    public var isThirdPartySDK: Bool = false
    public var signatureString: String?
    public weak var delegate: SignatureManagerDelegate?
    public var actionSheetPresentationRect: CGRect?
    public var actionSheetPresentationView: UIView?
    
    class func signatureImageRatio(_ isDefault: Bool) -> CGFloat {
        guard let signatureImg  = UserManager.shared.currentUser?.getSignatureImage(), !isDefault else {
            return CGFloat(defaultSignatureHeight / defaultSignatureWidth)
        }
        
        return signatureImg.size.height / signatureImg.size.width
    }

    class func initialImageRatio(_ isDefault: Bool) -> CGFloat {
        guard let initailImg  = UserManager.shared.currentUser?.getInitialImage(), !isDefault else {
            return CGFloat(defaultSignatureHeight / defaultSignatureWidth)
        }
        
        return initailImg.size.height / initailImg.size.width
    }


    public init(rootViewController: UIViewController?, signatureType: ZSignFieldSignatureType) {
        self.rootViewController = rootViewController
        self.signatureType = signatureType
        super.init()
    }
    
    public func showSignatureWizard(fromRect rect: CGRect) {
        actionSheetPresentationRect = rect
        showSignatureWizard()
    }
    
    public func showSignatureWizard(fromView view: UIView) {
        actionSheetPresentationView = view
        showSignatureWizard()
    }
    
    public func showSignatureWizard(){
        if let fontPath = Bundle.main.path(forResource: "Fancy-Signature", ofType: "otf") {
            isIncludeSelectSignatureStyle = FileManager.default.fileExists(atPath: fontPath)
        }
        
        rootViewController?.view.endEditing(true)
        
        let actionSheet = UIAlertController(title: nil, message: "sign.mobile.profile.signatureWizard".ZSLString, preferredStyle: .actionSheet)
        
        ///draw signature action
        let drawAction = UIAlertAction(title: "sign.mobile.profile.signatureWizardDraw".ZSLString, style: .default) { (action) in
            self.drawSignature()
        }
        actionSheet.addAction(drawAction)
        
        ///selection signature action
        if isIncludeSelectSignatureStyle {
            let selectAction = UIAlertAction(title: "sign.mobile.profile.selectSignature".ZSLString, style: .default) { (action) in
                self.selectSignature()
            }
            actionSheet.addAction(selectAction)
        }
        
        ///capture signature action
        if UIDevice.isCameraAvailable() {
            let photoAction = UIAlertAction(title: "sign.mobile.profile.photoSignature".ZSLString, style: .default) { (action) in
                self.captureSignature()
            }
            actionSheet.addAction(photoAction)
        }
        
        ///browse image for signature
        if DeviceType.isMac {
            let browseAction = UIAlertAction(title: "sign.mobile.common.browse".ZSLString, style: .default) { (action) in
                self.openFiles()
            }
            actionSheet.addAction(browseAction)
        }
        
        ///cancel action
        let cancelAction = UIAlertAction(title: "sign.mobile.common.cancel".ZSLString, style: .cancel) { (action) in
            self.delegate?.didCancelled?()
        }
        actionSheet.addAction(cancelAction)
        
        if DeviceType.isIpad {
            actionSheet.modalPresentationStyle = .popover
            if let view = actionSheetPresentationView {
                actionSheet.popoverPresentationController?.sourceView = view
                actionSheet.popoverPresentationController?.sourceRect = view.bounds
            } else if let rect = actionSheetPresentationRect {
                actionSheet.popoverPresentationController?.sourceView = rootViewController?.view
                actionSheet.popoverPresentationController?.sourceRect = rect
            }
        }
        
        if actionSheet.actions.count == 1 {
            self.drawSignature()
        } else {
            rootViewController?.present(actionSheet, animated: true)
        }
    }

    func showPreview(){
        let previewController = ZSignPreviewController(signatureType)
        let nav = ZNavigationController(rootViewController: previewController, supportedOrient: .portrait)
        nav.isNavigationBarHidden = true
        nav.modalPresentationStyle = .overCurrentContext
        rootViewController?.present(nav, animated: true)
    }
    
    func drawSignature(){
        let nav: ZNavigationController
        weak var weakSelf = self
        let drawViewController = ZSDrawSignatureController()
        drawViewController.delegate = self
        drawViewController.cancelBlock = {
            weakSelf?.rootViewController?.becomeFirstResponder()
        }
        nav = ZNavigationController(rootViewController: drawViewController, supportedOrient: .landscape)
        nav.modalPresentationStyle = .fullScreen
        rootViewController?.present(nav, animated: true)
    }
    
    func selectSignature(){
        let signSelector = ZSignSelectController(signType: signatureType)
        signSelector.delegate = self
        signSelector.cancelBlock = {[weak self] in
            self?.rootViewController?.becomeFirstResponder()
        }
        signSelector.signatureText = signatureString ?? ""
        let nav = ZNavigationController(rootViewController: signSelector, supportedOrient: .portrait)
        rootViewController?.present(nav, animated: true)
    }
    
    func captureSignature(){
        #if !targetEnvironment(macCatalyst)
        weak var weakSelf = self
        CommonUtils.hasUserGrantAccessForCamera(successBlock: {
            let signatureCapture = ZSSignCaptureController()
            signatureCapture.delegate = self
            let nav = ZNavigationController(rootViewController: signatureCapture, supportedOrient: DeviceType.isIpad ? .all : .landscape)
            nav.modalPresentationStyle = .fullScreen
            weakSelf?.rootViewController?.present(nav, animated: true)
        }) {
            let alertController = UIAlertController(title: "sign.mobile.alert.noCameraAccess".ZSLString, message: "sign.mobile.alert.enablePrivacySettings".ZSLString, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "sign.mobile.common.ok", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            weakSelf?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
        #endif
    }
    
    @objc private func openFiles(){
        let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypeImage as String], in: .import)
        documentPicker.allowsMultipleSelection = false
        documentPicker.delegate = self
        rootViewController?.present(documentPicker, animated: true, completion: nil)
    }
}

extension SignatureManager: SignatureCreatorDelegate {
    public func signatureCreated(signImage: UIImage) {
        switch signatureType {
        case .initial:
            if !isTemporary {
                ZSAnalytics.shared.track(forEvent: .signatureManager(.initial))
                UserManager.shared.currentUser?.initialImageData = signImage.pngData()
                UserManager.shared.saveUser(updateInServer: true)
            }
            delegate?.didSaveIntial(initialImage: signImage)
        case .signature:
            if !isTemporary {
                ZSAnalytics.shared.track(forEvent: .signatureManager(.signature))
                UserManager.shared.currentUser?.signImageData = signImage.pngData()
                UserManager.shared.saveUser(updateInServer: true)
            }
            delegate?.didSaveSignature(signImage: signImage)
            break
        default:
            break
        }
    }
}


extension  SignatureManager: UIDocumentPickerDelegate {
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        controller.dismiss(animated: true, completion: nil)
        guard let imageUrl = urls.first,
            let imageData = try? Data(contentsOf: imageUrl),
            let image = UIImage(data: imageData)
             else {return}
        
        let signatureEditorController = SignatureEditorViewController(image: image.getOrientationFixedUIImage())
        signatureEditorController.isModalyPresented = true
        signatureEditorController.delegate = self
        let nav = UINavigationController(rootViewController: signatureEditorController)
        nav.modalPresentationStyle = .fullScreen
        rootViewController?.present(nav, animated: true, completion: nil)
    }
    
    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
