//
//  SignatureDrawViewController.swift
//  ZohoSignSDK
//
//  Created by somesh-8758 on 21/06/19.
//  Copyright © 2019 Zoho Corporation. All rights reserved.
//

import UIKit

/*

#if !targetEnvironment(macCatalyst)
import PencilKit

@available(iOS 13,*)
@available(macCatalyst, unavailable)
public class SignatureDrawViewController: UIViewController {

    private lazy var canvasView = PKCanvasView()

    private var undoBarButtonItem: UIBarButtonItem!
    private var redoBarButtonItem: UIBarButtonItem!
    private var cancelBarButtonItem: UIBarButtonItem!

    private let originSpace: CGFloat = 10
    private let sizeSpace: CGFloat = 20
    public var delegate: ZSSignCreatorDelegate?

    public override var prefersStatusBarHidden: Bool{
        return DeviceType.isIphone
    }

    #if !targetEnvironment(macCatalyst)
    public override var prefersHomeIndicatorAutoHidden: Bool{
        return true
    }
    #endif
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ZColor.bgColorWhite
        view.addSubview(canvasView)
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.title  =   "sign.mobile.profile.drawSignature".ZSLString

        undoBarButtonItem = UIBarButtonItem(barButtonSystemItem: .undo, target: self, action: #selector(undoButtonClicked))
        undoBarButtonItem.isEnabled = false
        redoBarButtonItem = UIBarButtonItem(barButtonSystemItem: .redo, target: self, action: #selector(redoButtonClicked))
        redoBarButtonItem.isEnabled = false
        cancelBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonClicked))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonClicked))
        
//        undoBarButtonItem.publisher(for: \.isEnabled)
//            .receive(on: DispatchQueue.main)
//            .assign(to: \.isEnabled, on: navigationItem.rightBarButtonItem!)
        
     /*   canvasView.frame = view.bounds
        canvasView.delegate = self
        canvasView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        canvasView.alwaysBounceVertical = true

        becomeFirstResponder()
        
        
//        perform({
            if let window = self.parent?.view.window, let toolPicker = PKToolPicker.shared(for: window) {
               
                toolPicker.setVisible(true, forFirstResponder: self.canvasView)
                toolPicker.addObserver(self.canvasView)
                       toolPicker.addObserver(self)
                self.updateLayout(toolPicker: toolPicker)
                self.canvasView.becomeFirstResponder()
                   }
//        }, afterDelay: 2)*/
        
      
        canvasView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        view.setNeedsLayout()
        view.layoutIfNeeded()

    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        canvasView.tool = PKInkingTool(.pen, color: UIColor(red: 0, green: 0, blue: 0, alpha: 1), width: 4)
//        canvasView.becomeFirstResponder()
        if let window = self.parent?.view.window, let toolPicker = PKToolPicker.shared(for: window) {
        
         toolPicker.setVisible(true, forFirstResponder: self.canvasView)
         toolPicker.addObserver(self.canvasView)
//                toolPicker.addObserver(self)
//         self.updateLayout(toolPicker: toolPicker)
         self.canvasView.becomeFirstResponder()
            }
    }
    
//    override public var canBecomeFirstResponder: Bool{
//        return true
//    }
    public override var canBecomeFirstResponder: Bool{
        return true
    }

    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        let toolPicker = PKToolPicker.shared(for: self.parent!.view.window!)
        toolPicker?.setVisible(true, forFirstResponder: self.canvasView)
        toolPicker?.addObserver(self.canvasView)
        
//        let canvasScale = canvasView.bounds.width / 768
//        canvasView.minimumZoomScale = canvasScale
//        canvasView.maximumZoomScale = canvasScale
//        canvasView.zoomScale = canvasScale
//
//        // Scroll to the top.
//        updateContentSizeForDrawing()
//        canvasView.contentOffset = CGPoint(x: 0, y: -canvasView.adjustedContentInset.top)
    }

    func updateContentSizeForDrawing() {
        // Update the content size to match the drawing.
        let drawing = canvasView.drawing
        let contentHeight: CGFloat

        // Adjust the content size to always be bigger than the drawing height.
        if !drawing.bounds.isNull {
            contentHeight = max(canvasView.bounds.height, (drawing.bounds.maxY + 500) * canvasView.zoomScale)
        } else {
            contentHeight = canvasView.bounds.height
        }
        canvasView.contentSize = CGSize(width: 768 * canvasView.zoomScale, height: contentHeight)
    }

    @objc func undoButtonClicked(){
        guard let undoManager = canvasView.undoManager else {return}
        if undoManager.canUndo, !undoManager.isUndoing {
            undoManager.undo()
            updateBarButtonItems(canvasView: canvasView)
        }
    }

    @objc func redoButtonClicked(){
        guard let undoManager = canvasView.undoManager else {return}
        if undoManager.canRedo, !undoManager.isRedoing {
            undoManager.redo()
            updateBarButtonItems(canvasView: canvasView)
        }
    }

    @objc func doneButtonClicked(){
        let drawingBounds = canvasView.drawing.bounds

        let originX = drawingBounds.origin.x - originSpace > canvasView.frame.origin.x ? drawingBounds.origin.x - originSpace : canvasView.frame.origin.x
        let originY = drawingBounds.origin.y - originSpace > canvasView.frame.origin.y ? drawingBounds.origin.y - originSpace : canvasView.frame.origin.y
        let width = drawingBounds.width + sizeSpace < canvasView.frame.width ? drawingBounds.width + sizeSpace : canvasView.frame.width
        let height = drawingBounds.height + sizeSpace < canvasView.frame.height ? drawingBounds.height + sizeSpace : canvasView.frame.height

        let imageBounds = CGRect(x: originX, y: originY, width: width, height: height)
        let image = canvasView.drawing.image(from: imageBounds, scale: 1.0)
        delegate?.zsSinatureCreated(image)
        cancelButtonClicked()
    }

    @objc func cancelButtonClicked(){
        dismiss(animated: true, completion: nil)
    }
}

//MARK: CanvasView Delegate
@available(iOS 13, *)
@available(macCatalyst, unavailable)
extension SignatureDrawViewController: PKCanvasViewDelegate {
    public func canvasViewDrawingDidChange(_ canvasView: PKCanvasView) {
        updateBarButtonItems(canvasView: canvasView)
    }

    func updateBarButtonItems(canvasView: PKCanvasView) {
        guard let undoManager = canvasView.undoManager else {return}
        redoBarButtonItem.isEnabled = undoManager.canRedo
        undoBarButtonItem.isEnabled = undoManager.canUndo
    }
}


//MARK: ToolPicker Delegate
@available(iOS 13, *)
@available(macCatalyst, unavailable)
extension SignatureDrawViewController: PKToolPickerObserver {
    public func toolPickerFramesObscuredDidChange(_ toolPicker: PKToolPicker) {
        updateLayout(toolPicker: toolPicker)
    }

    public func toolPickerVisibilityDidChange(_ toolPicker: PKToolPicker) {
        updateLayout(toolPicker: toolPicker)
    }

    private func updateLayout(toolPicker: PKToolPicker){
        let obscurredFRame = toolPicker.frameObscured(in: view)

        if obscurredFRame.isNull {
            canvasView.contentInset = .zero
            navigationItem.leftBarButtonItems = [cancelBarButtonItem]
        } else {
            canvasView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: view.bounds.maxY - obscurredFRame.minY, right: 0)
            navigationItem.leftBarButtonItems = [cancelBarButtonItem,undoBarButtonItem,redoBarButtonItem]
        }
    }

}

extension UIImage {
    func replaceColorWithTransparent(fromColor: UIColor,toColor: UIColor, tolerance: CGFloat = 127)-> UIImage?{

        let initialCGImage = cgImage!
        let width = initialCGImage.width
        let height = initialCGImage.height
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bytesPerPixel = 4
        let bytesPerRow = bytesPerPixel * width
        let bitsPerComponent = 8
        let bitmapByteCount = bytesPerRow * height
        let dataPointer:UnsafeMutableRawPointer = calloc(bitmapByteCount, 1)!

        let context = CGContext(data: dataPointer, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue|CGBitmapInfo.byteOrder32Big.rawValue)
        context?.draw(initialCGImage, in: CGRect(x: 0, y: 0, width: width, height: height))

        let fromCgColor = fromColor.cgColor
        let fromComponents = fromCgColor.components!

        let toCGColor = toColor.cgColor
        let toComponents = toCGColor.components!

        let r = fromComponents[0]*255.0
        let g = fromComponents[1]*255.0
        let b = fromComponents[2]*255.0

        let redRange = [max(r - (tolerance/2.0), 0.0), min(r + (tolerance/2.0), 255.0)]
        let greenRange = [max(g - (tolerance/2.0), 0.0), min(g + (tolerance/2.0), 255.0)]
        let blueRange = [max(b - (tolerance/2.0), 0.0),min(g + (tolerance/2.0), 255.0)]

        var byteIndex = 0

        while byteIndex < bitmapByteCount {
            let red = dataPointer.load(fromByteOffset: byteIndex, as: UInt8.self)
            let green = dataPointer.load(fromByteOffset: byteIndex+1, as: UInt8.self)
            let blue = dataPointer.load(fromByteOffset: byteIndex+2, as: UInt8.self)
            if red>=UInt8(redRange[0]) && red<=UInt8(redRange[1]) && green>=UInt8(greenRange[0]) && green<=UInt8(greenRange[1]) && blue>=UInt8(blueRange[0]) && blue<=UInt8(blueRange[1]) {
                dataPointer.storeBytes(of: UInt8(toComponents[0] * 255.0), toByteOffset: byteIndex, as: UInt8.self)
                dataPointer.storeBytes(of: UInt8(toComponents[1] * 255.0), toByteOffset: byteIndex+1, as: UInt8.self)
                dataPointer.storeBytes(of: UInt8(toComponents[2] * 255.0), toByteOffset: byteIndex+2, as: UInt8.self)
                dataPointer.storeBytes(of: UInt8(toComponents[3] * 255.0), toByteOffset: byteIndex+3, as: UInt8.self)
            }
            byteIndex += 4
        }

        let finalCgImage = context?.makeImage()
        let finalImage = UIImage(cgImage: finalCgImage!)
        free(dataPointer)
        return finalImage
    }
}

#endif

*/
