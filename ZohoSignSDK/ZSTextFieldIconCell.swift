//
//  ZSTextFieldIconCell.swift
//  ZohoSign
//
//  Created by Rajdurai on 10/10/17.
//  Copyright © 2017 Zoho Corporation. All rights reserved.
//

import UIKit
public extension UITableViewCell {
    var tableView: UITableView? {
        var view = self.superview
        while (view != nil && view!.isKind(of: UITableView.self) == false) {
            view = view!.superview
        }
        return view as? UITableView
    }
}


public class ZSTextFieldIconCell: UITableViewCell, UITextFieldDelegate {

    public var iconTextField = ZSTextField()
    let separatorLine = UIView()
    let iconImgView = UIImageView()

    public var isCheckForRegex : Bool = false
    var regexPattern : String?
    public var maxLimit : Int?
    var popupShowInView : UIView?
    var keyString : String = ""
    public var isMandatory  = false
    
    public var textFieldBeginEditing : (() -> Void)?
    public var textFieldEndEditing : ((String) -> Void)?
    public var textFieldShouldChangeCharactersIn : ((UITextField, String) -> Void)?
    public var textFieldShouldReturn : (() -> Void)?
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Initial setup
        initialSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        //fatalError("init(coder:) has not been implemented")
    }
    
    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initialSetup(){
        
        self.contentView.backgroundColor = ZColor.bgColorWhite
        
        iconTextField.backgroundColor = UIColor.clear
        iconTextField.font = ZSFont.createListValueFont()
        iconTextField.textColor = ZColor.primaryTextColor
        iconTextField.returnKeyType = .done
        iconTextField.clearButtonMode = .whileEditing
        iconTextField.autocorrectionType = .no
        iconTextField.delegate = self
        self.contentView.addSubview(iconTextField)
       
        contentView.addSubview(iconImgView)
        
        separatorLine.backgroundColor = ZColor.seperatorColor
        self.contentView.addSubview(separatorLine)
        
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
//        iconTextField.frame = CGRect(x: cellValueLabelX, y: cellValueLabelY, width: self.bounds.size.width - 50, height: 40)
//        iconTextField.backgroundColor = ZColor.redColor
        iconTextField.frame = CGRect(x: cellValueLabelX, y: 0, width: self.bounds.size.width - 32, height: 60)
        separatorLine.frame = CGRect(x: cellValueLabelX, y: self.bounds.size.height - Constants.ZS_SeparatorLine_Height, width: self.bounds.size.width-30, height: Constants.ZS_SeparatorLine_Height)
        iconImgView.frame = cellIconFrame
    }
    
    // MARK: -
    
    @objc public  func setTextField(key placeholder:String, iconName imageName:String?, value valueText:String?, mandatory:Bool, editable isEdit:Bool)
    {
        keyString = placeholder
        isMandatory = mandatory
//        iconTextField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedStringKey.foregroundColor : ZColor.secondaryTextColorLight])
        iconTextField.setPlaceHolder(placeholder, isMandatory: mandatory)
        
        if let icon = imageName {
            iconImgView.image = ZSImage.tintColorImage(withNamed: icon, col: ZColor.secondaryTextColorDark)
        }
        iconTextField.text = valueText
        
        isUserInteractionEnabled    = isEdit

        if (isEdit) {
            iconTextField.alpha = 1.0
        }else {
            iconTextField.alpha = 0.5
        }
        self.setNeedsLayout()
    }
    
    public func checkForTextFieldRegex(regex pattern:String, showPopInView view:UIView){
        isCheckForRegex = true
        regexPattern = pattern
        popupShowInView = view
    }
    
    public func setTextMaxLimit(_ limit:Int){
        maxLimit = limit
    }
    
    func dismissPopTip(){
        SwiftUtils.dismissPoptip()
    }
    
   
    func getRectOfCellInSuperview(_ indexPath:IndexPath) -> CGRect {
        var rectOfCellInSuperview = CGRect.zero
        if let rectOfCell = tableView?.rectForRow(at: indexPath) {
            rectOfCellInSuperview = (tableView?.convert(rectOfCell, to: tableView?.superview))!
            
            var rect = rectOfCellInSuperview
            rect.origin.y += 20
            rectOfCellInSuperview = rect
        }
        return rectOfCellInSuperview
    }
    
    // MARK: -
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if (self.textFieldBeginEditing != nil) {
            self.textFieldBeginEditing!()
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        dismissPopTip()
        if (self.textFieldEndEditing != nil) {
            self.textFieldEndEditing!(textField.text!)
        }
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        dismissPopTip()

        let textFieldText: NSString = (textField.text ?? "") as NSString
        let newString = textFieldText.replacingCharacters(in: range, with: string)

        var isAllowChar : Bool = true
        if let limit = maxLimit {
            // If existing text is greater than limit, allow delete character
            if ((textField.text?.count)! <= limit) {
                isAllowChar = (newString.count <= limit)
            }
        }
        if (isCheckForRegex && newString.count > 0) {
            if let regex = self.regexPattern {
                let regexPredicate = NSPredicate(format:"SELF MATCHES %@", regex)

                if (!regexPredicate.evaluate(with: string)) {
                    // Invalid char
                    // Delete chars, but not add chars
                    if ((textField.text?.count)! < newString.count) {
                        isAllowChar = false
                        if let shownInView = self.popupShowInView {
                            let message = "\(self.keyString ) should not contain '\(string)'"
                            if let indexPath = tableView?.indexPath(for: self) {
                                let rect = getRectOfCellInSuperview(indexPath)
                                SwiftUtils.showPopTip(withMessage: message, inView: shownInView, onRect: rect)
                            }
                        }
                    }
                }
            }
        }

        if (isAllowChar) {
            if (self.textFieldShouldChangeCharactersIn != nil) {
                self.textFieldShouldChangeCharactersIn!(textField, newString)
            }
        }
        return isAllowChar
        
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if (self.textFieldShouldChangeCharactersIn != nil) {
            self.textFieldShouldChangeCharactersIn!(textField, "")
        }
        return true
    }
}
