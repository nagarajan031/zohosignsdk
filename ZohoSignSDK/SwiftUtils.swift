//
//  SwiftConstants.swift
//  ZohoSignSDK
//
//  Created by Nagarajan S on 06/01/18.
//  Copyright © 2018 Zoho Corporation. All rights reserved.
//

import UIKit
import AMPopTip
import AVFoundation
import SafariServices
import MobileCoreServices

struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

public struct TableviewCellSize
{
    public static let normal  : CGFloat = 50
    public static let extraLarge  : CGFloat = 91
    public static let signature : CGFloat = 72
    public static let subtitle  : CGFloat = 60
    public static let textView  : CGFloat = 220
    public static let receipientList  : CGFloat = 75
    public static let photoAlbumList  : CGFloat = 84
}



public struct DeviceType{
    
    public static let isIphone = UIDevice.current.userInterfaceIdiom == .phone
    public static let isIpad  = UIDevice.current.userInterfaceIdiom == .pad // valid for both ipad and mac
    public static let isMac  = SwiftUtils.isMac

    
    public static let isIphone5Orless = (isIphone && ScreenSize.SCREEN_MAX_LENGTH <= 568.0)
    public static let isIphone6OrAbove = (isIphone) && (ScreenSize.SCREEN_MAX_LENGTH >= 667.0)
    public static let isIphone6       = (isIphone) && (ScreenSize.SCREEN_MAX_LENGTH == 667.0)
    
    
    internal static let isIphone6PlusStandard = isIphone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    internal static let isIphone6PlusZoomed = (isIphone) && (ScreenSize.SCREEN_MAX_LENGTH == 667.0) && (UIScreen.main.nativeScale < UIScreen.main.scale)
    
    public static let isIphone6Plus  = isIphone6PlusZoomed || isIphone6PlusStandard
    
    public static let isIphoneXAndAbove  = isIphone && ScreenSize.SCREEN_MAX_LENGTH >= 812.0
    
    public static func isIpadOnly() -> Bool{
        if isIphone || isMac {
            return false
        }
        return isIpad
    }
}
/*public let IS_IPHONE_5         = IS_IPHONE && ScreenSize.SCREEN_MAX_LENGTH == 568.0
//let IS_IPHONE_6_7       = (UIDevice.current.userInterfaceIdiom == .phone) && (ScreenSize.SCREEN_MAX_LENGTH == 667.0) && (UIScreen.main.nativeScale >= UIScreen.main.scale)
public let IS_IPHONE_6_7       = (IS_IPHONE) && (ScreenSize.SCREEN_MAX_LENGTH == 667.0)
public let IS_STANDARD_IPHONE_6P_7P = IS_IPHONE && ScreenSize.SCREEN_MAX_LENGTH == 736.0
public let IS_ZOOMED_IPHONE_6P_7P = (IS_IPHONE) && (ScreenSize.SCREEN_MAX_LENGTH == 667.0) && (UIScreen.main.nativeScale < UIScreen.main.scale)
public let IS_IPHONE_6P_7P     = IS_STANDARD_IPHONE_6P_7P || IS_ZOOMED_IPHONE_6P_7P
public let IS_IPHONE_X         = IS_IPHONE && ScreenSize.SCREEN_MAX_LENGTH == 812.0
public let IS_IPHONE_X_Above         = IS_IPHONE && ScreenSize.SCREEN_MAX_LENGTH >= 812.0

public let IS_IPHONE              = UIDevice.current.userInterfaceIdiom == .phone
*/
public let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad

public let IS_IPAD_9_7 = IS_IPAD && ScreenSize.SCREEN_MAX_LENGTH == 1024.0 && ScreenSize.SCREEN_MIN_LENGTH == 768.0

public let IS_IPAD_10_5 = IS_IPAD && ScreenSize.SCREEN_MAX_LENGTH == 1112.0 && ScreenSize.SCREEN_MIN_LENGTH == 834.0

public let IS_IPAD_11 = IS_IPAD && ScreenSize.SCREEN_MAX_LENGTH == 1194.0 && ScreenSize.SCREEN_MIN_LENGTH == 834.0

public let IS_IPAD_12 = IS_IPAD && ScreenSize.SCREEN_MAX_LENGTH == 1366 && ScreenSize.SCREEN_MIN_LENGTH == 1024

//let DeviceType.isIphone6OrAbove = (UIDevice.current.userInterfaceIdiom == .phone) && (ScreenSize.SCREEN_MAX_LENGTH >= 667.0) && (UIScreen.main.nativeScale >= UIScreen.main.scale)
//public let DeviceType.isIphone6OrAbove = (IS_IPHONE) && (ScreenSize.SCREEN_MAX_LENGTH >= 667.0)

//public let iPhoneXCurveRadius : CGFloat = ((IS_IPHONE_X) ? 10 : 0)
public let iPhoneXCurveRadius : CGFloat =  10

public let ZS_NavbarHeight : CGFloat = 44

public let cellIconFrame : CGRect = CGRect.zero
public let cellPlaceHolderLabelY : CGFloat = 10
public let cellValueLabelY : CGFloat = 22
public let cellValueLabelX : CGFloat = 18

//public let kAlert_width  : CGFloat =    (IS_IPAD) ? 540 : 300
public let kAlert_width  : CGFloat =    (IS_IPAD) ? 540 : 300
public let kTextPopup_width  : CGFloat =    (IS_IPAD || DeviceType.isIphone5Orless) ? 280 : 320


//EditorView
public let  ipadThumbnailViewWidth : CGFloat     =   170//160
public let  editorFieldSelectionViewWidth : CGFloat =  270//220 // right view
public let  viewerDetailViewWidth    : CGFloat     =   300 // right view




public class SwiftUtils: NSObject {
    
    public static let isIOS13AndAbove: Bool  = {
        if #available(iOS 13, *){
            return true
        } else {
            return false
        }
    }()

    public static let isIOS11: Bool  = {
        if #available(iOS 12, *){
            return false
        } else {
            return true
        }
    }()


    public static let isMac: Bool  = {
    #if targetEnvironment(macCatalyst)
        return true
    #else
        return false
    #endif
    }()

    public static let isIpad: Bool  = {
        return UIDevice.current.userInterfaceIdiom == .pad
    }()

    public static let isIphone: Bool  = {
           return UIDevice.current.userInterfaceIdiom == .phone
       }()
    
    static let popTip = PopTip()

    public static func dismissPoptip(){
        popTip.hide()
    }
    
    public static let appstoreAppId: String  = {
        if DeviceType.isMac{
            return "1476226908"
        }else {
            return "1236146442"
        }
    }()
    
    
    //MARK: - Poptip
    public static func showInfoTip(withMessage message:String, inView view:UIView, onRect rect:CGRect, poptipDirection : PopTipDirection = .up) {
    

        popTip.font = ZSFont.regularFont(withSize: 15)
        popTip.shouldDismissOnTap = true
        popTip.shouldDismissOnTapOutside = true
        popTip.bubbleColor = ZColor.bgColorDark
        popTip.arrowSize = CGSize(width: 15, height: 10)
        let padding : CGFloat = DeviceType.isIphone ? 5 : 8
        popTip.edgeInsets = UIEdgeInsets.init(top: padding, left: padding, bottom: padding, right: padding)
        self.popTip.show(text: message, direction: poptipDirection, maxWidth: 250, in: view, from: rect, duration: 5)
    
    }
    
    
    public static func showPopTip(withMessage message:String, inView view:UIView, onRect rect:CGRect, poptipDirection : PopTipDirection = .up, dismissOnTap: Bool = false) {
        
        popTip.font = ZSFont.regularFont(withSize: 15)
        popTip.shouldDismissOnTap = dismissOnTap
        popTip.shouldDismissOnTapOutside = dismissOnTap
        popTip.bubbleColor = ZColor.redColor
        popTip.layer.zPosition = 9999
        popTip.arrowSize = CGSize(width: 15, height: 10)
        popTip.edgeInsets = UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
        self.popTip.show(text: message, direction: poptipDirection, maxWidth: 250, in: view, from: rect, duration: 3)
    }
    
    public static var sharedDataFormatter : DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        return dateFormatter
    }()
//     public static func generatePDFFile(_ pathForPDF :  String, _ imagesToConvertPDF: [UIImage])
//    {
//        let printedImageView:UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 720, height: 1019))
//        printedImageView.contentMode = .scaleAspectFit
//        UIGraphicsBeginPDFContextToFile(pathForPDF, .zero, nil)
//        
//        for image in imagesToConvertPDF {
//            printedImageView.image = image.reducedFileSize()
//            UIGraphicsBeginPDFPageWithInfo(printedImageView.frame, nil)
//            let pdfContext:CGContext = UIGraphicsGetCurrentContext()!
//            printedImageView.layer.render(in: pdfContext)
//        }
//        UIGraphicsEndPDFContext();
//    }
    
    //MARK: - Helpers
    public class func isValidEmail(_ checkString: String, showAlertIfFails show: Bool, rootViewController vc: UIViewController?) -> Bool {
        var errorMsg: String?
        var isvalid = false
        if checkString.isEmpty {
            errorMsg = "sign.mobile.alert.enterEmailId".ZSLString
        } else {
            let stricterFilter = false
            let stricterFilterString = "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$"
            let laxString = "^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$"
            let emailRegex = stricterFilter ? stricterFilterString : laxString
            let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)

            isvalid = emailTest.evaluate(with: checkString)
            errorMsg = "sign.mobile.recipient.invalidEmail".ZSLString
        }

        if !isvalid {

            if !show || (vc == nil) {
                return false
            }
            DispatchQueue.main.async(execute: {
                vc?.showAlert(withMessage: errorMsg!)
            })

            return false
        }

        return true
    }

    
//    class func encodeImageToBase64String(image: UIImage) -> String? {
//
//        let imagedata = image.pngData()
//        let str = imagedata?.base64EncodedString(options: .endLineWithCarriageReturn)
//        return str
//    }

    public class func decodeBase64ToImage(encodeStr: String?) -> UIImage? {
        guard let encodeStr = encodeStr else {
            return nil
        }
       
        if let data = Data(base64Encoded: encodeStr, options: .ignoreUnknownCharacters) {
            return UIImage(data: data)
        }
        return nil
    }
    
    public class func getDeviceLanguageID() -> String {
        let languageID = Bundle.main.preferredLocalizations.first

        if languageID == nil {
            return "en-US"
        } else if (languageID == "fr") {
            return "fr-FR"
        } else if (languageID == "es") {
            return "es-ES"
        } else if (languageID == "pt-BR") {
            return "pt-BR"
        } else if (languageID == "pt-PT") {
            return "pt-PT"
        } else if (languageID == "it") {
            return "it-IT"
        } else if (languageID == "de") {
            return "de-DE"
        } else if (languageID == "zh-Hans") {
            return "zh-CN"
        } else if (languageID == "nl") {
            return "nl-NL"
        } else if (languageID == "ja") {
            return "ja-JP"
        } else if (languageID == "ru") {
            return "ru-RU"
        } else if (languageID == "sv") {
            return "sv-SE"
        } else if (languageID == "pl") {
            return "pl-PL"
        }

        return "en-US"
    }

    public class func supportedUTITypes() -> [String] {
        return [
        kUTTypePDF as String,
        "public.png",
        "public.jpeg",
        "com.microsoft.word.doc",
        "public.rtf",
        "public.plain-text"
        ]
    }


    //MARK: - Image Handling
    class func getCompressedSignatureData(image: UIImage) -> Data {
        var image = image
        guard var imgData = image.pngData() else {
            return Data()
        }
        ZInfo("Size of Image(bytes):" + String(imgData.count))

        var scalledQuality = 1
        while (imgData.count) > 400000 {
            if let scaledImage = image.downsampleImage(size: CGSize(width: image.size.width / 2, height: image.size.height / 2)) {
                image = scaledImage
            }
            imgData = image.pngData() ?? Data()
            ZInfo("Size of Image(bytes):" + String(imgData.count))
            scalledQuality += 1
        }

        return imgData
    }

    
    //MARK: - Thumbnail

    public static func pdfThumbanailLayout(showHeader : Bool) -> UICollectionViewFlowLayout{
        let layout = UICollectionViewFlowLayout()
        //        layout.minimumInteritemSpacing  =   10
//        layout.minimumLineSpacing = 20
        let width : CGFloat =  DeviceType.isIphone ? 100 : 120//120
        let a4Ratio : CGFloat = 1.414
        let height = width*a4Ratio + 55
        var horizontalInset: CGFloat = 0
        layout.itemSize = CGSize(width: width, height: height)
        layout.minimumLineSpacing = 0
        layout.sectionHeadersPinToVisibleBounds = true
        
        if DeviceType.isIphone {
            let totalWidth = UIScreen.main.bounds.size.width
            let rowCount: CGFloat = 3
            let totalCellWidth:CGFloat = width * rowCount
            horizontalInset = (totalWidth - totalCellWidth) / (rowCount + 1)
            layout.minimumInteritemSpacing = horizontalInset
        } else {
            horizontalInset = showHeader ? 0 : 40
            layout.sectionInset = UIEdgeInsets(top: showHeader ? 0 : cellValueLabelY, left: 0, bottom: showHeader ? 0 : cellValueLabelY, right: 0)
        }
        
        if showHeader {
            layout.headerReferenceSize = CGSize(width: width, height: tablePlainHeaderHeightLarge)
            layout.sectionInset = UIEdgeInsets(top: 0, left: horizontalInset, bottom: 0, right: horizontalInset)
        } else{
            layout.sectionInset = UIEdgeInsets(top: cellValueLabelY, left: horizontalInset, bottom: cellValueLabelY, right: horizontalInset)
        }
        return layout
    }
    
    public static func thumbnailPageSize(pagesArray : [ZSPage], indexPath : IndexPath) -> CGSize{
        let width : CGFloat = 120//120
        var a4Ratio : CGFloat = 1.414
        var height = width*a4Ratio + 55
        
           
           let filteredResults = pagesArray.filter { (page) -> Bool in
            return ((page.pageNumber == indexPath.row+1) && (page.docOrder == indexPath.section))
           }
           guard let page = filteredResults.first else {
               return CGSize(width: width, height: height)
           }
        
//        if page.rotation > 0, SwiftUtils.isIOS13AndAbove {
//            return CGSize(width: width, height: height)
//        }
        
        a4Ratio = page.sizeRatio
        height = width*a4Ratio + 55
        return CGSize(width: width, height: height)
    }
    
    
    public static func createButtonWithLeftIcon() -> UIButton{
        let btn = UIButton()
        btn.titleLabel?.font = ZSFont.navTitleFont()
        btn.contentHorizontalAlignment = .center
        btn.imageEdgeInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 10)
        btn.titleEdgeInsets = UIEdgeInsets(top: 4, left: 10, bottom: 4, right: 4)
        btn.setTitleColor(ZColor.primaryTextColor, for: .normal)
        btn.titleLabel?.numberOfLines = 1
        btn.isUserInteractionEnabled = false
        return btn
    }
    
   

    public static func generateNotificationFeedback(for type: UINotificationFeedbackGenerator.FeedbackType){
        #if !targetEnvironment(macCatalyst)
        checkForFeedBackGeneratorSupportLevel { (feedBackGen) in
            switch feedBackGen {
            case 2:
                var notificationFeedbackGenerator: UINotificationFeedbackGenerator? = nil
                notificationFeedbackGenerator = UINotificationFeedbackGenerator()
                notificationFeedbackGenerator?.prepare()
                notificationFeedbackGenerator?.notificationOccurred(type)
                notificationFeedbackGenerator = nil
            case 1:
                var type1 = SystemSoundID(HapticEngineGenOneCode.peek.rawValue)
                switch type {
                case .success:
                    type1 = SystemSoundID(HapticEngineGenOneCode.peek.rawValue)
                case .error:
                    type1 = SystemSoundID(HapticEngineGenOneCode.falied.rawValue)
                case .warning:
                    type1 = SystemSoundID(HapticEngineGenOneCode.tryAgain.rawValue)
                @unknown default:
                    type1 = SystemSoundID(HapticEngineGenOneCode.falied.rawValue)
                }
                AudioServicesPlaySystemSound(type1)
            case 0:
                if type == .error {
                    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                }
                break
            default:
                break
            }
        }
        #endif
    }
    
    public static func generateSelectionFeedBack() {
        checkForFeedBackGeneratorSupportLevel { (feedBackGen) in
            switch feedBackGen {
            case 2:
                var selectionFeedBack: UISelectionFeedbackGenerator? = UISelectionFeedbackGenerator()
                selectionFeedBack?.prepare()
                selectionFeedBack?.selectionChanged()
                selectionFeedBack = nil
            case 1:
                AudioServicesPlaySystemSound(SystemSoundID(HapticEngineGenOneCode.peek.rawValue))
            default:
                break
            }
        }
        
    }
    
    public static func generateImpactFeedBack(for style: UIImpactFeedbackGenerator.FeedbackStyle = .medium){
        #if !targetEnvironment(macCatalyst)
        checkForFeedBackGeneratorSupportLevel { (feedBackGen) in
            switch feedBackGen {
            case 2:
                var lightImpactFeedbackGenerator: UIImpactFeedbackGenerator? = UIImpactFeedbackGenerator(style: style)
                lightImpactFeedbackGenerator?.prepare()
                lightImpactFeedbackGenerator?.impactOccurred()
                lightImpactFeedbackGenerator = nil
            case 1:
                let peek = SystemSoundID(HapticEngineGenOneCode.peek.rawValue)
                AudioServicesPlaySystemSound(peek)
            default:
                break
            }
        }
        #endif
    }
    

    public static func generateContrastImage(_ value: Float = 5,_ image: UIImage) -> UIImage{
//        let openGLContext = EAGLContext(api: .openGLES2)
//        let context = CIContext(eaglContext: openGLContext!)
//        let openGLContext = EAGLContext(api: .openGLES2)
        let context = CIContext(options: nil)
        
        let beginImage = CIImage(image: image)
        let filter = CIFilter(name: "CIColorControls")
        filter?.setValue(beginImage, forKey: kCIInputImageKey)
        filter?.setValue(0, forKey: kCIInputSaturationKey)
        filter?.setValue(value, forKey: kCIInputContrastKey)
       // filter?.setValue(0.3 * value, forKey: kCIInputBrightnessKey)
        if let outputImage = filter?.outputImage {
            return UIImage(cgImage: context.createCGImage(outputImage, from: outputImage.extent) ?? image.cgImage!)
        }
        return image
    }
    
    private static func checkForFeedBackGeneratorSupportLevel(completion: (Int) -> Void){
        completion(UIDevice.current.value(forKey: "_feedbackSupportLevel") as? Int ?? -1)
    }
    
    public static func actionSheetStyle() -> UIAlertController.Style {
        return isMac ? UIAlertController.Style.alert : UIAlertController.Style.actionSheet
    }
    
    public static func modalPresentationStyle(style : UIModalPresentationStyle) -> UIModalPresentationStyle {
        return isMac ? UIModalPresentationStyle.fullScreen : style
    }
    
    public static func defaultFieldFontSize(pageWidth : CGFloat) -> CGFloat{
        if DeviceType.isIphone{
            return 8
        }
        return (pageWidth / 60).rounded(.toNearestOrAwayFromZero)
    }
    
    public static func openSafariController(url: URL, in viewController: UIViewController?) {
        let safariController = SFSafariViewController(url: url)
        viewController?.present(safariController, animated: true, completion: nil)
    }
    
 
   /* public static let defaultFieldFontSize: CGFloat  = {
        if DeviceType.isMac{
            return 13
        }
        else if DeviceType.isIpad{
            return 13
        }
        
        return 8
   }()*/
}

enum HapticEngineGenOneCode: Float{
    case peek = 1519
    case pop = 1520
    case cancelled = 1521
    case falied = 1102
    case tryAgain = 1107
}



public struct Constants {
    public static let ZS_SeparatorLine_Height : CGFloat = 0.4
}

public extension UIEdgeInsets {
    init(withInset inset: CGFloat) {
        self.init(top: inset, left: inset, bottom: inset, right: inset)
    }
}

public extension UIBarButtonItem {
    static var flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
}

public enum OverLappingSide {
    case top
    case bottom
    case left
    case right
    case topLeft
    case topRight
    case bottomLeft
    case bottomRight
    case all
    case none
}

